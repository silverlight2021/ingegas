<?php

require('libraries/fpdf/fpdf.php');


class PDF_Invoice extends FPDF
{
	// private variables
	var $colonnes;
	var $format;
	var $angle=0;

	// private functions
	function RoundedRect($x, $y, $w, $h, $r, $style = '') //Rectángulo con bordes redondeados 
	{
		$k = $this->k;
		$hp = $this->h;
		if($style=='F')
			$op='f';
		elseif($style=='FD' || $style=='DF')
			$op='B';
		else
			$op='S';
		$MyArc = 4/3 * (sqrt(2) - 1);
		$this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));
		$xc = $x+$w-$r ;
		$yc = $y+$r;
		$this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));

		$this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);
		$xc = $x+$w-$r ;
		$yc = $y+$h-$r;
		$this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
		$this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);
		$xc = $x+$r ;
		$yc = $y+$h-$r;
		$this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
		$this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);
		$xc = $x+$r ;
		$yc = $y+$r;
		$this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
		$this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
		$this->_out($op);
	}

	function _Arc($x1, $y1, $x2, $y2, $x3, $y3) //Curvatura del rectángulo
	{
		$h = $this->h;
		$this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
							$x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
	}

	function Rotate($angle, $x=-1, $y=-1) //Rotar un texto dentro de la pantalla
	{
		if($x==-1)
			$x=$this->x;
		if($y==-1)
			$y=$this->y;
		if($this->angle!=0)
			$this->_out('Q');
		$this->angle=$angle;
		if($angle!=0)
		{
			$angle*=M_PI/180;
			$c=cos($angle);
			$s=sin($angle);
			$cx=$x*$this->k;
			$cy=($this->h-$y)*$this->k;
			$this->_out(sprintf('q %.5F %.5F %.5F %.5F %.2F %.2F cm 1 0 0 1 %.2F %.2F cm',$c,$s,-$s,$c,$cx,$cy,-$cx,-$cy));
		}
	}

	function _endpage()
	{
		if($this->angle!=0)
		{
			$this->angle=0;
			$this->_out('Q');
		}
		parent::_endpage();
	}

	// public functions
	function sizeOfText( $texte, $largeur )
	{
		$index    = 0;
		$nb_lines = 0;
		$loop     = TRUE;
		while ( $loop )
		{
			$pos = strpos($texte, "\n");
			if (!$pos)
			{
				$loop  = FALSE;
				$ligne = $texte;
			}
			else
			{
				$ligne  = substr( $texte, $index, $pos);
				$texte = substr( $texte, $pos+1 );
			}
			$length = floor( $this->GetStringWidth($ligne));
			$res = 1 + floor( $length / $largeur) ;
			$nb_lines += $res;
		}
		return $nb_lines;
	}

	// Company Data
	function addDatosEmpresa( $nom, $adresse )
	{
		$x1 = 5;
		$y1 = 42;
		//Positionnement en bas
		$this->SetXY( $x1, $y1 );
		$this->SetFont('Arial','B',12);
		$length = $this->GetStringWidth( $nom );
		$this->Cell( $length, 2, $nom);
		$this->SetXY( $x1, $y1 + 4 );
		$this->SetFont('Arial','',10);
		$length = $this->GetStringWidth( $adresse );
		//Coordonnées de la société
		$lignes = $this->sizeOfText( $adresse, $length) ;
		$this->MultiCell($length, 4, $adresse);
	}

	// Label and number of invoice/estimate


	function fact_dev1( $nombre_empresa_ajustado )//Numero Comprobante
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 75;
		$y1  = 3; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;		
		

		$this->SetXY( $r1 + ($r2-$r1)/2 -10 , $y1 + 5 );
		$this->SetFont( "Arial", "", 10);
		$this->Cell(100,7,$nombre_empresa_ajustado, 0,0, "l");

	}
	function fact_dev2( $nombre_empresa_ajustado2 )//Numero Comprobante
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 75;
		$y1  = 8; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;		
				
		$this->SetXY( $r1 + ($r2-$r1)/2 -10 , $y1 + 5 );
		$this->SetFont( "Arial", "", 10);
		$this->Cell(100,7,$nombre_empresa_ajustado2, 0,0, "l");

	}

	Function fact_dev21( $nombre_formato )//Numero Comprobante
	{
		$r1  = $this->w - 100;
		$r2  = $r1 + 75;
		$y1  = 8; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;		
				
		$this->SetXY( $r1 + ($r2-$r1)/2 -10 , $y1 + 5 );
		$this->SetFont( "Arial", "", 10);
		$this->Cell(100,7,$nombre_formato, 0,0, "l");

	}
	function fact_dev3( $titulo )//Numero Comprobante
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 250;
		$y1  = 20; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;		
		
		$this->SetXY( $r1 + ($r2-$r1)/2 -10 , $y1 + 5 );
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,5,utf8_decode($titulo), 0,0, "C");

	}
	function fact_dev4( $registro )//Numero Comprobante
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 75;
		$y1  = 35; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;		
		
		$this->SetXY( $r1 + ($r2-$r1)/2 -10 , $y1 + 5 );
		$this->SetFont( "Arial", "", 8);
		$this->Cell(10,5,utf8_decode($registro), 0,0, "l");

	}
	function fact_dev5( $mezcla )//Numero Comprobante
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 75;
		$y1  = 30; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;		
		
		$this->SetXY( $r1 + ($r2-$r1)/2 -10 , $y1 + 5 );
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,5,utf8_decode($mezcla), 0,0, "l");

	}
	function fact_dev6( $numero )//Numero Comprobante
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 325;
		$y1  = 30; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;		
		
		$this->SetXY( $r1 + ($r2-$r1)/2 -10 , $y1 + 5 );
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,5,utf8_decode($numero), 0,0, "l");

	}
	function fact_dev7( $numero_orden )//Numero Comprobante
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 380;
		$y1  = 30; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;		
		
		$this->SetXY( $r1 + ($r2-$r1)/2 -10 , $y1 + 5 );
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($numero_orden), 0,0, "C");
	}	
	function fact_dev8( $codigo_producto )
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 104;
		$y1  = 40; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2 -2 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("Código Producto :"), 0, 0, "R");	
		
		$this->SetXY( $r1 + ($r2-$r1)-43  , $y1+5);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($codigo_producto), 0,0, "l");
	}
	function fact_dev88( $num_orden )
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 104;
		$y1  = 45; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2 -2 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("Número Orden :"), 0, 0, "R");	
		
		$this->SetXY( $r1 + ($r2-$r1)-43  , $y1+5);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($num_orden), 0,0, "l");
	}
	function fact_dev99( $num_lote )
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 200;
		$y1  = 45; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2+60 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("Número Lote :"), 0, 0, "R");	
		
		$this->SetXY( $r1 + ($r2-$r1)/2+70  , $y1+5);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($num_lote), 0,0, "l");
	}
	function fact_dev9( $num_lote )
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 200;
		$y1  = 40; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2+60 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("Número Lote :"), 0, 0, "R");	
		
		$this->SetXY( $r1 + ($r2-$r1)/2+70  , $y1+5);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($num_lote), 0,0, "l");
	}
	function fact_dev10( $codigo_producto )
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 104;
		$y1  = 45; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2 -2 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("Tipo Cilindro :"), 0, 0, "R");	
		
		$this->SetXY( $r1 + ($r2-$r1)-43  , $y1+5);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($codigo_producto), 0,0, "l");
	}
	function fact_dev11( $num_lote )
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 200;
		$y1  = 45; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2+60 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("Conexión Válvula :"), 0, 0, "R");	
		
		$this->SetXY( $r1 + ($r2-$r1)/2+70  , $y1+5);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($num_lote), 0,0, "l");
	}
	function fact_dev12( $codigo_producto )
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 104;
		$y1  = 150; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2 -2 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("	Presión Cilindros :"), 0, 0, "R");	
		
		$this->SetXY( $r1 + ($r2-$r1)-43  , $y1+5);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($codigo_producto), 0,0, "l");
	}
	function fech_llenado( $fech_crea )
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 200;
		$y1  = 150; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2+60 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("Fecha de Llenado :"), 0, 0, "R");	
		
		$this->SetXY( $r1 + ($r2-$r1)/2+70  , $y1+5);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($fech_crea), 0,0, "l");
	}
	function metodo_llenado( $llenado )
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 104;
		$y1  = 155; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2 -2 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("Método de Llenado :"), 0, 0, "R");	
		
		$this->SetXY( $r1 + ($r2-$r1)-43  , $y1+5);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($llenado), 0,0, "l");
	}
	function fecha_vencimiento( $fech_ven )
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 200;
		$y1  = 155; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2+60 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("Fecha de Expiración :"), 0, 0, "R");	
		
		$this->SetXY( $r1 + ($r2-$r1)/2+70  , $y1+5);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($fech_ven), 0,0, "l");
	}
	function fact_dev16( $oxi_eti )
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 104;
		$y1  = 197; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2 -2 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("	Óxido de Etileno :"), 0, 0, "R");	
		
		$this->SetXY( $r1 + ($r2-$r1)-43  , $y1+5);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($oxi_eti), 0,0, "l");
	}
	function fact_dev17( $num_lote )
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 200;
		$y1  = 197; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2+60 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("Dioxido de Carbono :"), 0, 0, "R");	
		
		$this->SetXY( $r1 + ($r2-$r1)/2+70  , $y1+5);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($num_lote), 0,0, "l");
	}
	function fact_dev18( $num_lote_eto )
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 104;
		$y1  = 202; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2 -2 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("No. Cilindro ETO :"), 0, 0, "R");	
		
		$this->SetXY( $r1 + ($r2-$r1)-43  , $y1+5);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($num_lote_eto), 0,0, "l");
	}
	function fact_dev19( $num_lote_co2 )
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 200;
		$y1  = 202; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2+60 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("No. LOTE CO2 :"), 0, 0, "R");	
		
		$this->SetXY( $r1 + ($r2-$r1)/2+70  , $y1+5);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($num_lote_co2), 0,0, "l");
	}
	function oficina( $oficina ,$bogota)
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 100;
		$y1  = 255; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2-30 , $y1+5);
		$this->SetFont( "Arial", "", 8);
		$this->Cell(10,4, utf8_decode("OFICINA TOBERÍN "), 0, 0, "L");

		$this->SetXY( $r1 + ($r2-$r1)/2-30 , $y1+15);
		$this->SetFont( "Arial", "", 8);
		$this->Cell(10,5,utf8_decode($bogota), 0,0, "L");

		$this->SetXY( $r1 + ($r2-$r1)/2-30 , $y1+10);
		$this->SetFont( "Arial", "", 8);
		$this->Cell(10,5,utf8_decode($oficina), 0,0, "L");
	}
	function firma( $firma )
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 150;
		$y1  = 235; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->Line(130, 254, 205, 254);

		$this->SetXY( $r1 + ($r2-$r1)/2+60 , $y1+15);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("Firmado :"), 0, 0, "R");	
		
		$this->SetXY( $r1 + ($r2-$r1)/2+70  , $y1+20);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($firma), 0,0, "l");
	}
	function firma1( $firma1)
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 150;
		$y1  = 250; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->Line(130, 269, 205, 269);

		$this->SetXY( $r1 + ($r2-$r1)/2+60 , $y1+15);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("Firmado :"), 0, 0, "R");	
		
		$this->SetXY( $r1 + ($r2-$r1)/2+70  , $y1+20);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($firma1), 0,0, "l");
	}

	function tab_mezcla( $mezcla_eto,$exactitud )
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 300;
		$y1  = 155; //top
		$y2  = 12;
		
		$this->RoundedRect(10, 170, 190, 20, '', 'c');
		$this->SetXY( 35 , $y1+16);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode($mezcla_eto), 0, 0, "C");
		$this->SetXY( 95 , $y1+16);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("ESPECIFICACIONES "), 0, 0, "C");
		$this->SetXY( 160 , $y1+16);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("RESULTADO "), 0, 0, "C");
		$this->SetXY( 35 , $y1+21);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,4, utf8_decode("EXACTITUD"), 0, 0, "C");
		$this->SetXY( 95 , $y1+21);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,4, utf8_decode("99,0% Min"), 0, 0, "C");
		$this->SetXY( 160 , $y1+21);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,4, utf8_decode($exactitud." %"), 0, 0, "C");
		$this->SetXY( 35 , $y1+26);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,4, utf8_decode("PUREZA"), 0, 0, "C");
		$this->SetXY( 95 , $y1+26);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,4, utf8_decode("99,0% Min"), 0, 0, "C");
		$this->SetXY( 160 , $y1+26);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,4, utf8_decode("99,9 %"), 0, 0, "C");
		$this->SetXY( 35 , $y1+31);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,4, utf8_decode("HUMEDAD"), 0, 0, "C");
		$this->SetXY( 95 , $y1+31);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,4, utf8_decode("MAX 100 ppm"), 0, 0, "C");
		$this->SetXY( 160 , $y1+31);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,4, utf8_decode("100 ppm"), 0, 0, "C");

		$this->Line(10, 175, 200, 175);
		$this->Line(10, 180, 200, 180);
		$this->Line(10, 185, 200, 185);
		$this->Line(66,170,66,190 );
		$this->Line(132,170,132,190 );		
	}
	function compuestos( $pureza_eto,$pureza_co2,$humedad_eto,$humedad_co2 )
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 300;
		$y1  = 200; //top
		$y2  = 12;
		
		$this->RoundedRect(5, 200, 200, 45, '', 'c');
		$this->RoundedRect(10, 215, 190, 25, '', 'c');
		
		$this->SetXY( 95 , $y1+16);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("ESPECIFICACIONES "), 0, 0, "C");
		$this->SetXY( 160 , $y1+16);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("RESULTADO "), 0, 0, "C");

		$this->SetXY( 35 , $y1+21);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,4, utf8_decode("PUREZA ETO"), 0, 0, "C");
		$this->SetXY( 95 , $y1+21);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,4, utf8_decode("99,0% Min"), 0, 0, "C");
		$this->SetXY( 160 , $y1+21);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,4, utf8_decode($pureza_eto." %"), 0, 0, "C");

		$this->SetXY( 35 , $y1+26);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,4, utf8_decode("HUMEDAD ETO"), 0, 0, "C");
		$this->SetXY( 95 , $y1+26);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,4, utf8_decode("MAX 100 ppm"), 0, 0, "C");
		$this->SetXY( 160 , $y1+26);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,4, utf8_decode($humedad_eto." ppm"), 0, 0, "C");
		
		$this->SetXY( 35 , $y1+31);		
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,4, utf8_decode("PUREZA CO2"), 0, 0, "C");
		$this->SetXY( 95 , $y1+31);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,4, utf8_decode("99,00% Min"), 0, 0, "C");
		$this->SetXY( 160 , $y1+31);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,4, utf8_decode($pureza_co2." %"), 0, 0, "C");

		$this->SetXY( 35 , $y1+36);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,4, utf8_decode("HUMEDAD CO2"), 0, 0, "C");
		$this->SetXY( 95 , $y1+36);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,4, utf8_decode("MAX 20 ppm"), 0, 0, "C");
		$this->SetXY( 160 , $y1+36);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,4, utf8_decode($humedad_co2." ppm"), 0, 0, "C");


		
		$this->Line(10, 220, 200, 220);
		$this->Line(10, 225, 200, 225);
		$this->Line(10, 230, 200, 230);
		$this->Line(10, 235, 200, 235);
		$this->Line(66,215,66,240);
		$this->Line(132,215,132,240 );		
	}	

	function addCols( $tab )
	{
		global $colonnes;
		
		$r1  = 10;
		$r2  = $this->w - ($r1 * 1) ;
		$y1  = 45;
		$y2  = $this->h - 85 - $y1;
		$this->SetXY( $r1, $y1 );
		$this->Rect( 5, $y1, $r2, 150, "D");
		//$this->Line( $r1, $y1+6, $r1+$r2, $y1+6);
		$colX = $r1;
		$colonnes = $tab;
		while ( list( $lib, $pos ) = each ($tab) )
		{
			$this->SetXY( $colX, $y1+2 );
			$this->Cell( $pos, 30, $lib, 0, 0, "C");
			$colX += $pos;
			//$this->Line( $colX, $y1, $colX, $y1+$y2);
		}
	}
	function addLineFormat( $tab )
	{
		global $format, $colonnes;
		
		while ( list( $lib, $pos ) = each ($colonnes) )
		{
			if ( isset( $tab["$lib"] ) )
				$format[ $lib ] = $tab["$lib"];
		}
	}
	function addLine( $ligne, $tab )
	{
		global $colonnes, $format;

		$ordonnee     = 21;
		$maxSize      = $ligne;

		reset( $colonnes );
		while ( list( $lib, $pos ) = each ($colonnes) )
		{
			$longCell  = $pos -10;
			$texte     = $tab[ $lib ];
			$length    = $this->GetStringWidth( $texte );
			$tailleTexte = $this->sizeOfText( $texte, $length );
			$formText  = $format[ $lib ];
			$this->SetXY( $ordonnee, $ligne-1);
			$this->MultiCell( $longCell, 4 , $texte, 0, $formText);
			if ( $maxSize < ($this->GetY()  ) )
				$maxSize = $this->GetY() ;
			$ordonnee += $pos;
		}
		return ( $maxSize - $ligne );
	}

	
}
?>