<?php
date_default_timezone_set("America/Bogota");
require ("libraries/conexion.php");
session_start();

if(@$_SESSION['logged']== 'yes')
{ 
  $acc = $_SESSION['acc'];    

  require_once("inc/init.php");
  require_once("inc/config.ui.php");
  $page_title = "Cargue de archivos de Cod - UN";
  $page_css[] = "your_style.css";
  include("inc/header.php");
  $page_nav["comisiones"]["sub"]["vendedores"]["sub"]["cod_un"]["active"] = true;
  include("inc/nav.php");

  ?>
  <style type="text/css">
    h2 {display:inline}
  </style>
  <style type="text/css">
    .center-row {
      display:table;
    }
    .center {
      display:table-cell;
      vertical-align:middle;
      float:none;
    }
  </style>
  <div id="main" role="main">
    <div id="content">
      <div class="row">
        <div class="" align="center">
          <h1  class="page-title txt-color-blueDark"> <?php echo $page_title; ?></h1>
        </div>	      	
      </div>	
      <section id="widget-grid" class="">
        <div class="row">
          <article class="col-md-6 col-md-offset-3">		
            <div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">			
              <header><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
                <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                <h2>Archivos a cargar</h2>											
              </header>
              <div>
                <div class="jarviswidget-editbox"></div>	
                <div class="widget-body no-padding">
                  <form id="upload_csv" class="smart-form" method="POST" name="upload_csv" enctype="multipart/form-data">
                    <fieldset>
                      <div class="row" align="center">
                        <section style="padding: 15px">
                          <label class="font-lg">Archivo de Cod - UN</label>
                          <input type="file" name="file" class="file" style="display:none">
                          <div class="input-group">
                            <label class="input"><input type="text" class="form-control input-lg" disabled placeholder="Subir Excel">
                            </label>
                            <span class="input-group-btn">
                              <button type="button" class="btn1 btn-warning btn-lg">Seleccionar</button>
                            </span>
                          </div>
                        </section>
                      </div>
                    </fieldset>
                    <footer>
                      <p id="cargando" style="font-size: 20px; color: green"></p>  
                      <input type="submit" name="Import" id="submit" class="btn btn-primary" value="SUBIR ARCHIVOS">      
                    </footer>
                  </form>
                </div>
              </div>				
            </div>	
          </article>				
        </div>
      </section>
      
      
    </div>
    
  </div>
  <?php
    include("inc/footer.php");
    include("inc/scripts.php"); 
  ?>
  <script>
      function mostrarId(id){
          document.getElementById("id_cliente").value = id;
          valor = document.getElementById("id_cliente").value;
          
          fecha_inicial = document.getElementById("fecha_inicial").value;
          fecha_final = document.getElementById("fecha_final").value;
          
          if(valor != "" && fecha_inicial != "" && fecha_final != ""){
              document.getElementById("descargar_excel").disabled = false;
          }
      }
      
      function validarCampos(){
          valor = document.getElementById("id_cliente").value;
          
          fecha_inicial = document.getElementById("fecha_inicial").value;
          fecha_final = document.getElementById("fecha_final").value;
          
          if(valor != "" && fecha_inicial != "" && fecha_final != ""){
              document.getElementById("descargar_excel").disabled = false;
          }
      }
  </script>
  <script type="text/javascript">
    $(document).on('click', '.btn1', function(){
    var file = $(this).parent().parent().parent().find('.file');
      file.trigger('click');
    });
    $(document).on('change', '.file', function(){
      $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
    });
  </script>

  <script type="text/javascript">
    $(document).ready(function(){
      $('#upload_csv').on("submit", function(e){
        e.preventDefault();
        $.ajax({
          url: "comisiones_vendedores/importar_cod_un.php",
          method: "POST",
          data: new FormData(this),
          contentType: false, 
          cache: false, 
          processData: false,
          beforeSend: function(){
            $('#cargando').html("Importando archivo, espere por favor...")
          }, 
          success: function(data){
            if(data=="1"){
              $('#cargando').html("Cargue de Cod - UN realizado");
            }else if(data == "2"){
              $('#cargando').html("Ha ocurrido un error, por favor vuelva a intentarlo");
            }else{
              $('#cargando').html(data);
            }
          }
        })
      });
    });
  </script>


  <script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
  <script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
  <script src="js/plugin/flot/jquery.flot.time.min.js"></script>
  <script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>
  <script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
  <script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
  <script src="js/plugin/moment/moment.min.js"></script>
  <script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
  <script src="js/plugin/datatables/jquery.dataTables.min.js"></script>
  <script src="js/plugin/datatables/dataTables.colVis.min.js"></script>
  <script src="js/plugin/datatables/dataTables.tableTools.min.js"></script>
  <script src="js/plugin/datatables/dataTables.bootstrap.min.js"></script>
  <script src="js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
  <script type="text/javascript">
    // DO NOT REMOVE : GLOBAL FUNCTIONS!

    $(document).ready(function() {
      
      /* // DOM Position key index //
        
      l - Length changing (dropdown)
      f - Filtering input (search)
      t - The Table! (datatable)
      i - Information (records)
      p - Pagination (paging)
      r - pRocessing 
      < and > - div elements
      <"#id" and > - div with an id
      <"class" and > - div with a class
      <"#id.class" and > - div with an id and class
      
      Also see: http://legacy.datatables.net/usage/features
      */	

      /* BASIC ;*/
        var responsiveHelper_dt_basic = undefined;
        var responsiveHelper_datatable_fixed_column = undefined;
        var responsiveHelper_datatable_col_reorder = undefined;
        var responsiveHelper_datatable_tabletools = undefined;
        
        var breakpointDefinition = {
          tablet : 1024,
          phone : 480
        };

        $('#dt_basic').dataTable({
          "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
            "t"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
          "autoWidth" : true,
          "preDrawCallback" : function() {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper_dt_basic) {
              responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
            }
          },
          "rowCallback" : function(nRow) {
            responsiveHelper_dt_basic.createExpandIcon(nRow);
          },
          "drawCallback" : function(oSettings) {
            responsiveHelper_dt_basic.respond();
          }
        });

      /* END BASIC */
      
      /* COLUMN FILTER  */
        var otable = $('#datatable_fixed_column').DataTable({
          //"bFilter": false,
          //"bInfo": false,
          //"bLengthChange": false
          //"bAutoWidth": false,
          //"bPaginate": false,
          //"bStateSave": true // saves sort state using localStorage
        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
            "t"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        "autoWidth" : true,
        "preDrawCallback" : function() {
          // Initialize the responsive datatables helper once.
          if (!responsiveHelper_datatable_fixed_column) {
            responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
          }
        },
        "rowCallback" : function(nRow) {
          responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
        },
        "drawCallback" : function(oSettings) {
          responsiveHelper_datatable_fixed_column.respond();
        }		
      
        });
        
        // custom toolbar
        $("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');
            
        // Apply the filter
        $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {
          
            otable
                .column( $(this).parent().index()+':visible' )
                .search( this.value )
                .draw();
                
        } );
        /* END COLUMN FILTER */   

      /* COLUMN SHOW - HIDE */
      $('#datatable_col_reorder').dataTable({
        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
            "t"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        "autoWidth" : true,
        "preDrawCallback" : function() {
          // Initialize the responsive datatables helper once.
          if (!responsiveHelper_datatable_col_reorder) {
            responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
          }
        },
        "rowCallback" : function(nRow) {
          responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
        },
        "drawCallback" : function(oSettings) {
          responsiveHelper_datatable_col_reorder.respond();
        }			
      });
      
      /* END COLUMN SHOW - HIDE */

      /* TABLETOOLS */
      $('#datatable_tabletools').dataTable({
        
        // Tabletools options: 
        //   https://datatables.net/extensions/tabletools/button_options
        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
            "t"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
            "oTableTools": {
              "aButtons": [
                "copy",
                "csv",
                "xls",
                    {
                        "sExtends": "pdf",
                        "sTitle": "SmartAdmin_PDF",
                        "sPdfMessage": "SmartAdmin PDF Export",
                        "sPdfSize": "letter"
                    },
                  {
                      "sExtends": "print",
                      "sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>"
                  }
                ],
                "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
            },
        "autoWidth" : true,
        "preDrawCallback" : function() {
          // Initialize the responsive datatables helper once.
          if (!responsiveHelper_datatable_tabletools) {
            responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
          }
        },
        "rowCallback" : function(nRow) {
          responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
        },
        "drawCallback" : function(oSettings) {
          responsiveHelper_datatable_tabletools.respond();
        }
      });
      
      /* END TABLETOOLS */

    })

  </script>
  <script type="text/javascript">
      
  </script>

  <?php 

	include("inc/google-analytics.php"); 
}
else
{
  header("Location:index.php");
}
?>