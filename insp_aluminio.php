<?php
session_start();
require("libraries/conexion.php");

if($_SESSION['logged'] == 'yes')
{
	$id_has_movimiento_cilindro_pev1 = isset($_REQUEST['id_cili']) ? $_REQUEST['id_cili'] : NULL;

	$consulta3 = "SELECT num_cili, id_tipo_gas_pev, id_transporte_pev
                  FROM has_movimiento_cilindro_pev WHERE id_has_movimiento_cilindro_pev = '".$id_has_movimiento_cilindro_pev1."'";
    $resultado3 = mysqli_query($con,$consulta3);
    if(mysqli_num_rows($resultado3) > 0)
    {
        $linea3 = mysqli_fetch_assoc($resultado3);
        $num_cili = $linea3['num_cili'];
        $id_tipo_gas_pev = $linea3['id_tipo_gas_pev'];
        $id_transporte_pev = $linea3['id_transporte_pev'];

        $consulta4 = "SELECT fecha FROM transporte_pev WHERE id_transporte_pev = '".$id_transporte_pev."'";
	    $resultado4 = mysqli_query($con,$consulta4);
	    if(mysqli_num_rows($resultado4) > 0)
	    {
	        $linea4 = mysqli_fetch_assoc($resultado4);
	        $fecha_ingreso = $linea4['fecha'];
	    }
	    $consulta5 = "SELECT nombre FROM tipo_gas_pev WHERE id_tipo_gas_pev = '".$id_tipo_gas_pev."'";
	    $resultado5 = mysqli_query($con,$consulta5);
	    if(mysqli_num_rows($resultado5) > 0)
	    {
	        $linea5 = mysqli_fetch_assoc($resultado5);
	        $nombre_gas = $linea5['nombre'];
	    }
	    $consulta6 = "SELECT id_especi_eto, especi_eto, num_cili_eto_2 FROM cilindro_eto WHERE num_cili_eto = '".$num_cili."'";
	    $resultado6 = mysqli_query($con,$consulta6);
	    if(mysqli_num_rows($resultado6) > 0)
	    {
	        $linea6 = mysqli_fetch_assoc($resultado6);
	        $id_especi_eto = $linea6['id_especi_eto'];
	        $especi_eto = $linea6['especi_eto'];
	        $num_cili_eto_2 = $linea6['num_cili_eto_2'];
	        $consulta7 = "SELECT especificacion FROM especificacion WHERE id_especificacion = '".$id_especi_eto."'";
	        $resultado7 = mysqli_query($con,$consulta7);
	        if(mysqli_num_rows($resultado7))
	        {
	        	$linea7 = mysqli_fetch_assoc($resultado7);
	        	$especificacion = $linea7['especificacion'];
	        }
	    }
	    $returnData = $num_cili.",".$fecha_ingreso.",".$nombre_gas.",".$especificacion.",".$especi_eto.",".$num_cili_eto_2;
	    echo $returnData;
    }    
}
else
{
	?>
	<script type="text/javascript">document.location = "index.php"</script>
	<?php
}
?>