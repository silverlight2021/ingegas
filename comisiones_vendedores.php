<?php
date_default_timezone_set("America/Bogota");
require ("libraries/conexion.php");
session_start();

if(@$_SESSION['logged']== 'yes')
{ 
	$valor ="";
  $acc = $_SESSION['acc'];    
    
  if(isset($_POST['descargar_excel'])){
    $archivo = $_POST["archivo"];

		if($archivo == "1"){
			header("Location: descargar_excel/comisiones_vendedores/resumenFinalizado.php");
		}else if($archivo == "2"){
			header("Location: descargar_excel/comisiones_vendedores/recaudoGeneral.php");
		}else if($archivo == "3"){
			header("Location: descargar_excel/comisiones_vendedores/recaudo_TOBE_SUR.php");
		}else if($archivo == "4"){
			header("Location: descargar_excel/comisiones_vendedores/recaudo_MANIZ_MED.php");
		}else if($archivo == "5"){
			header("Location: descargar_excel/comisiones_vendedores/recaudo_TV.php");
		}else if($archivo == "6"){
			header("Location: descargar_excel/comisiones_vendedores/recaudo_MED_MANI.php");
		}else if($archivo == "7"){
			header("Location: descargar_excel/comisiones_vendedores/carteraToberin.php");
		}else if($archivo == "8"){
			header("Location: descargar_excel/comisiones_vendedores/carteraSur.php");
		}elseif ($archivo == "9") {
			header("Location: descargar_excel/comisiones_vendedores/carteraManizales.php");
		}elseif ($archivo == "10") {
			header("Location: descargar_excel/comisiones_vendedores/carteraMedellin.php");
		}elseif ($archivo == "11") {
			header("Location: descargar_excel/comisiones_vendedores/comisionesIngegas.php");
		}elseif ($archivo == "12") {
			header("Location: descargar_excel/comisiones_vendedores/cobranzaIng.php");
		}
		
		
  }
    

require_once("inc/init.php");
require_once("inc/config.ui.php");
$page_title = "Consolidado de Comisiones Vendedores";
$page_css[] = "your_style.css";
include("inc/header.php");
include("inc/nav.php");

?>
<style type="text/css">
  h2 {display:inline}
</style>
<style type="text/css">
	.center-row {
	display:table;
	}
	.center {
		display:table-cell;
	    vertical-align:middle;
	    float:none;
	}
</style>	
<div id="main" role="main">
	<div id="content">
		<div class="row">
			<div class="" align="center">
				<h1  class="page-title txt-color-blueDark"> <?php echo $page_title; ?></h1>
			</div>	      	
		</div>	
		<section id="widget-grid" class="">
			<div class="row">
				<article class="col-md-6 col-md-offset-3">		
					<div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">			
						<header><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h2>Indique el Archivo</h2>											
						</header>
						<div>
							<div class="jarviswidget-editbox"></div>	
							<div class="widget-body no-padding">								
								<form id="checkout-form" name="form1" class="smart-form" novalidate="novalidate" action="comisiones_vendedores.php" method="POST">
								<input type="hidden" name="valor" id="valor" value="1">
									<fieldset>
										<div class="row">
											<section class="col col-4">
												<label class="input"> 
													<select class="input-lg" name="archivo">
														<option value="0">Seleccione...</option>
														<option value="1">Resumen Comisiones</option>
														<option value="2">D Ms</option>
														<option value="3">CA Ms</option>
														<option value="4">JJ Ms</option>
														<option value="5">TV Ms</option>
														<option value="6">VM Ms</option>
														<option value="7">Cartera Toberin</option>
														<option value="8">Cartera Sur</option>
														<option value="9">Cartera Manizales</option>
														<option value="10">Cartera Itagui</option>
														<option value="11">Com Ing</option>
														<option value="12">Cr Ing</option>
													</select>
												</label>
											</section>							
										</div>
										
									</fieldset>	
									<?php
									//if (in_array(41, $acc))
									//{
									?>
									<footer>
											<input type="submit" value="Generar" name="descargar_excel" id="descargar_excel" class="btn btn-primary" />
											</form>
											
									</footer>
									<?php
									//}										
									?>
							</div>						
						</div>				
					</div>	
				</article>				
			</div>

			<div class="row" style="display: none" id="cargue">
				<article class="col-md-6 col-md-offset-3">		
					<div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">			
						<header><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h2>Archivos a cargar</h2>											
						</header>
						<div>
							<div class="jarviswidget-editbox"></div>	
							<div class="widget-body no-padding">
                  <form id="upload_csv" class="smart-form" method="POST" name="upload_csv" enctype="multipart/form-data">
                    <fieldset>
                      <div class="row" align="center">
                        <section style="padding: 15px">
                          <label class="font-lg">Archivo</label>
                          <input type="file" name="file" class="file" style="display:none">
                          <div class="input-group">
                            <label class="input"><input type="text" class="form-control input-lg" disabled placeholder="Subir Excel">
                            </label>
                            <span class="input-group-btn">
                              <button type="button" class="btn1 btn-warning btn-lg">Seleccionar</button>
                            </span>
                          </div>
                        </section>
                      </div>
                    </fieldset>
                    <footer>
                      <p id="cargando" style="font-size: 20px; color: green"></p>  
                      <input type="submit" name="Import" id="submit" class="btn btn-primary" value="SUBIR ARCHIVOS">      
                    </footer>
                  </form>
                </div>						
						</div>				
					</div>	
				</article>				
			</div>
		</section>

		
	</div>
	
</div>
<?php
	include("inc/footer.php");
	include("inc/scripts.php"); 
?>

<script type="text/javascript">
    $(document).on('click', '.btn1', function(){
    var file = $(this).parent().parent().parent().find('.file');
      file.trigger('click');
    });
    $(document).on('change', '.file', function(){
      $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
    });
  </script>

<script type="text/javascript">
	function habilitar_cargue(){
		document.getElementById("cargue").style.display = "block";
	}
</script>
<script>
    function mostrarId(id){
        document.getElementById("id_cliente").value = id;
        valor = document.getElementById("id_cliente").value;
        
        fecha_inicial = document.getElementById("fecha_inicial").value;
        fecha_final = document.getElementById("fecha_final").value;
        
        if(valor != "" && fecha_inicial != "" && fecha_final != ""){
            document.getElementById("descargar_excel").disabled = false;
        }
    }
    
    function validarCampos(){
        valor = document.getElementById("id_cliente").value;
        
        fecha_inicial = document.getElementById("fecha_inicial").value;
        fecha_final = document.getElementById("fecha_final").value;
        
        if(valor != "" && fecha_inicial != "" && fecha_final != ""){
            document.getElementById("descargar_excel").disabled = false;
        }
    }
</script>
<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<script type="text/javascript">
// DO NOT REMOVE : GLOBAL FUNCTIONS!

$(document).ready(function() {
	
	/* // DOM Position key index //
		
	l - Length changing (dropdown)
	f - Filtering input (search)
	t - The Table! (datatable)
	i - Information (records)
	p - Pagination (paging)
	r - pRocessing 
	< and > - div elements
	<"#id" and > - div with an id
	<"class" and > - div with a class
	<"#id.class" and > - div with an id and class
	
	Also see: http://legacy.datatables.net/usage/features
	*/	

	/* BASIC ;*/
		var responsiveHelper_dt_basic = undefined;
		var responsiveHelper_datatable_fixed_column = undefined;
		var responsiveHelper_datatable_col_reorder = undefined;
		var responsiveHelper_datatable_tabletools = undefined;
		
		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};

		$('#dt_basic').dataTable({
			"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
			"autoWidth" : true,
			"preDrawCallback" : function() {
				// Initialize the responsive datatables helper once.
				if (!responsiveHelper_dt_basic) {
					responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
				}
			},
			"rowCallback" : function(nRow) {
				responsiveHelper_dt_basic.createExpandIcon(nRow);
			},
			"drawCallback" : function(oSettings) {
				responsiveHelper_dt_basic.respond();
			}
		});

	/* END BASIC */
	
	/* COLUMN FILTER  */
    var otable = $('#datatable_fixed_column').DataTable({
    	//"bFilter": false,
    	//"bInfo": false,
    	//"bLengthChange": false
    	//"bAutoWidth": false,
    	//"bPaginate": false,
    	//"bStateSave": true // saves sort state using localStorage
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_fixed_column) {
				responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_fixed_column.respond();
		}		
	
    });
    
    // custom toolbar
    $("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');
    	   
    // Apply the filter
    $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {
    	
        otable
            .column( $(this).parent().index()+':visible' )
            .search( this.value )
            .draw();
            
    } );
    /* END COLUMN FILTER */   

	/* COLUMN SHOW - HIDE */
	$('#datatable_col_reorder').dataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_col_reorder) {
				responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_col_reorder.respond();
		}			
	});
	
	/* END COLUMN SHOW - HIDE */

	/* TABLETOOLS */
	$('#datatable_tabletools').dataTable({
		
		// Tabletools options: 
		//   https://datatables.net/extensions/tabletools/button_options
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        "oTableTools": {
        	 "aButtons": [
             "copy",
             "csv",
             "xls",
                {
                    "sExtends": "pdf",
                    "sTitle": "SmartAdmin_PDF",
                    "sPdfMessage": "SmartAdmin PDF Export",
                    "sPdfSize": "letter"
                },
             	{
                	"sExtends": "print",
                	"sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>"
            	}
             ],
            "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
        },
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_tabletools) {
				responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_tabletools.respond();
		}
	});
	
	/* END TABLETOOLS */

})

</script>
<script type="text/javascript">
    
</script>

<?php 

	include("inc/google-analytics.php"); 
}
else
{
    header("Location:index.php");
}
?>