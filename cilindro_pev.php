<?php
session_start();
date_default_timezone_set("America/Bogota");

require ("libraries/conexion.php");
require ("libraries/seguimiento_pev.php");


if(@$_SESSION['logged']== 'yes')
{ 
  	$id_cilindro_pev = isset($_REQUEST['id_cilindro_pev']) ? $_REQUEST['id_cilindro_pev'] : NULL;
    $id_user =$_SESSION['su'];
    $acc = $_SESSION['acc'];
    $tipo_eto_pev = isset($_REQUEST['tipo_eto_pev']) ? $_REQUEST['tipo_eto_pev'] : NULL;

	if(isset($_POST['g_cilindro_pev']))
	{
		$num_cili_pev1 = $_POST['num_cili_pev'];  
		$num_cili_pev_2 = $_POST['num_cili_pev_2'];
		$especi_pev1 = $_POST['especi_pev'];
		$pre_tra_pev1 = $_POST['pre_tra_pev'];
		$pre_pru_pev1 = $_POST['pre_pru_pev'];
		$fecha_fab_pev1 = $_POST['fecha_fab_pev'];  
		$fecha_ult_pev1 = $_POST['fecha_ult_pev'];
		$volumen_pev1 = $_POST['volumen_pev'];
		$volumen_actu1 = $_POST['volumen_actu'];
		$tara_esta_pev1 = $_POST['tara_esta_pev'];
		$tara_actu_pev1 = $_POST['tara_actu_pev'];
		$esp_fab_pev1 = $_POST['esp_fab_pev'];
		$esp_actu_pev1 = $_POST['esp_actu_pev'];
		$cone_val_pev1 = $_POST['id_conexion1'];
		$fecha_lav_pev1 = $_POST['fecha_lav_pev'];
		$id_propiedad_cilindro2 = $_POST['id_propiedad_cilindro1'];
		$tipo_cili_pev2 = $_POST['id_tipo_cili1'];
		$id_tipo_cilindro2 = $_POST['id_tipo_cilindro1'];
		$id_tipo_envace2 = $_POST['id_tipo_envace'];		
		$id_cliente1 = $_POST['id_cliente'];
		$id_especi_pev2 = $_POST['id_especificacion1'];
		$id_conversion2 = $_POST['id_conversion1'];
		$pre_trabajo1 = $_POST['pre_tra_pev_1'];
		$id_estado2 = $_POST['id_estado1'];	
		$tipo_eto_pev = $_POST['tipo_eto_pev'];	
		$hilos_rosca = $_POST['hilos_rosca'];
		$costura = isset($_POST["costura"]) ? $_POST["costura"] : 0;


		if($cone_val_pev1==9){
			$otr_valvula = $_POST['otr_valvula'];
		}else{
			$otr_valvula = NULL;
		}

		if(strlen($id_cilindro_pev) > 0)
		{
			if(($id_estado2 == 2)||($id_estado2==1)){
				$id_estado3 = 3;
			}else{
				$id_estado3 = $id_estado2; 
			}

	    	$consulta= "UPDATE cilindro_pev SET 
	                num_cili_pev = '$num_cili_pev1',
	                num_cili_pev_2 = '$num_cili_pev_2',
	                id_especi_pev = '$id_especi_pev2',
	                especi_pev = '$especi_pev1' ,
	                id_conversion = '$id_conversion2' ,
	                pre_trabajo = '$pre_trabajo1' ,
	                pre_tra_pev = '$pre_tra_pev1' ,
	                pre_pru_pev = '$pre_pru_pev1' ,
	                fecha_fab_pev = '$fecha_fab_pev1' ,
	                fecha_ult_pev = '$fecha_ult_pev1' ,
	                volumen_pev = '$volumen_pev1' ,
	                volumen_actu = '$volumen_actu1' ,
	                tara_esta_pev = '$tara_esta_pev1' ,
	                tara_actu_pev = '$tara_actu_pev1' ,
	                esp_fab_pev = '$esp_fab_pev1' ,
	                esp_actu_pev = '$esp_actu_pev1' ,
	                cone_val_pev = '$cone_val_pev1' ,
	                fecha_lav_pev = '$fecha_lav_pev1' ,
	                propiedad_pev = '$id_propiedad_cilindro2',
	                id_tipo_cilindro = '$id_tipo_cilindro2',
	                id_tipo_envace = '$id_tipo_envace2',
	                tipo_cili_pev = '$tipo_cili_pev2',
	                id_cliente = '$id_cliente1', 
	                id_estado = '$id_estado3',
	                otra_valvula = '$otr_valvula',
	                hilos_rosca = $hilos_rosca,
	                costura = $costura
	                WHERE id_cilindro_pev = $id_cilindro_pev ";
	    	$resultado = mysqli_query($con,$consulta) ;

		    if ($resultado == FALSE)
		    {
		      	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		      	echo $consulta;
		    }
		    else
		    {
		    	seguimiento_pev(1,$id_cilindro_pev,$id_user,1,0);//id_seguimiento,id_cilindro_pev,id_user,id_proceso,id_has_proceso
		      	
			    header('Location: busqueda_cilindros_pev.php');	  
		    }
	  	}
	    else
	    {
	    	$consulta2  = "SELECT num_cili_pev FROM cilindro_pev WHERE  num_cili_pev like '".$num_cili_pev1."' ";
		  	$resultado2 = mysqli_query($con,$consulta2) ;
		  	$linea2 = mysqli_fetch_array($resultado2);
		  	$num_cili_pev2 = isset($linea2["num_cili_pev"]) ? $linea2["num_cili_pev"] : NULL;	
		  	if ($num_cili_pev2==$num_cili_pev1) 
		  	{
		  		?>
		      	<script type="text/javascript">
		         	alert("Ya existe este Cilindro ");
		         	 header('Location: busqueda_cilindros_pev.php');
		     	</script>
		     	<?php
		  	}
		  	else		  		# code...
		  	{ 
			  	$id_estado=1;
			    $consulta = "INSERT INTO cilindro_pev
			          		(num_cili_pev,
			          		num_cili_pev_2,
			          		id_especi_pev,
			          		especi_pev,
			          		id_conversion,
			          		pre_trabajo, 
			          		pre_tra_pev, 
			          		pre_pru_pev, 
			          		fecha_fab_pev, 
			          		fecha_ult_pev, 
			          		volumen_pev, 
			          		tara_esta_pev, 
			          		tara_actu_pev, 
			          		esp_fab_pev, 
			          		esp_actu_pev, 
			          		cone_val_pev, 
			          		fecha_lav_pev, 
			          		propiedad_pev, 
			          		tipo_cili_pev,
			          		id_tipo_cilindro,
			          		id_tipo_envace,
			          		volumen_actu,
			          		id_estado,
			          		id_cliente,
			          		fech_crea,
			          		tipo) 
			          		VALUES ('".$num_cili_pev1."',
			          				'".$num_cili_pev_2."',
			          				'".$id_especi_pev2."',
					          		'".$especi_pev1."', 
					          		'".$id_conversion2."', 
					          		'".$pre_trabajo1."', 
					          		'".$pre_tra_pev1."', 
					          		'".$pre_pru_pev1."', 
					          		'".$fecha_fab_pev1."', 
					          		'".$fecha_ult_pev1."', 
					          		'".$volumen_pev1."', 
					          		'".$tara_esta_pev1."', 
					          		'".$tara_actu_pev1."', 
					          		'".$esp_fab_pev1."', 
					          		'".$esp_actu_pev1."', 
					          		'".$cone_val_pev1."', 
					          		'".$fecha_lav_pev1."' , 
					          		'".$id_propiedad_cilindro2."' , 
					          		'".$tipo_cili_pev2."' ,
					          		'".$id_tipo_cilindro2."',
					          		'".$id_tipo_envace2."',
					          		'".$volumen_actu1."',
					          		'".$id_estado."',
					          		'".$id_cliente1."',
					          		'".date("Y/m/d")."',
					          		'".$tipo_eto_pev."')";
			    $resultado = mysqli_query($con,$consulta) ;
			    if ($resultado == FALSE)
			    {
			      	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
			    }
			    else
			    {
				    $id_cilindro_pev1 = mysqli_insert_id($con);
				    seguimiento_pev(2,$id_cilindro_pev1,$id_user,1,0);//id_seguimiento,id_cilindro_pev,id_user,id_proceso,id_has_proceso
				      
				    header('Location: busqueda_cilindros_pev.php');	
			      
			    }
			}
	  	}	    
	}
	if(strlen($id_cilindro_pev) > 0)
	{ 
	  	$consulta  = "SELECT * FROM cilindro_pev WHERE id_cilindro_pev= $id_cilindro_pev";
	  	$resultado = mysqli_query($con,$consulta) ;
	  	$linea = mysqli_fetch_array($resultado);
		$num_cili_pev = isset($linea["num_cili_pev"]) ? $linea["num_cili_pev"] : NULL;  
		$num_cili_pev_2 = isset($linea["num_cili_pev_2"]) ? $linea["num_cili_pev_2"] : NULL;
		$especi_pev = isset($linea["especi_pev"]) ? $linea["especi_pev"] : NULL;
		$pre_tra_pev = isset($linea["pre_tra_pev"]) ? $linea["pre_tra_pev"] : NULL;
		$pre_pru_pev = isset($linea["pre_pru_pev"]) ? $linea["pre_pru_pev"] : NULL;
		$fecha_fab_pev = isset($linea["fecha_fab_pev"]) ? $linea["fecha_fab_pev"] : NULL;  
		$fecha_ult_pev = isset($linea["fecha_ult_pev"]) ? $linea["fecha_ult_pev"] : NULL;
		$volumen_pev = isset($linea["volumen_pev"]) ? $linea["volumen_pev"] : NULL;
		$tara_esta_pev = isset($linea["tara_esta_pev"]) ? $linea["tara_esta_pev"] : NULL;
		$tara_actu_pev = isset($linea["tara_actu_pev"]) ? $linea["tara_actu_pev"] : NULL;
		$esp_fab_pev = isset($linea["esp_fab_pev"]) ? $linea["esp_fab_pev"] : NULL;
		$esp_actu_pev = isset($linea["esp_actu_pev"]) ? $linea["esp_actu_pev"] : NULL;
		$id_conexion1 = isset($linea["cone_val_pev"]) ? $linea["cone_val_pev"] : NULL;
		$fecha_lav_pev = isset($linea["fecha_lav_pev"]) ? $linea["fecha_lav_pev"] : NULL;
		$id_propiedad_cilindro1 = isset($linea["propiedad_pev"]) ? $linea["propiedad_pev"] : NULL;
		$tipo_cili_pev1 = isset($linea["tipo_cili_pev"]) ? $linea["tipo_cili_pev"] : NULL;
		$id_tipo_cilindro1 = isset($linea["id_tipo_cilindro"]) ? $linea["id_tipo_cilindro"] : NULL;
		$id_tipo_envace = isset($linea["id_tipo_envace"]) ? $linea["id_tipo_envace"] : NULL;
		$id_tipo_cili1 = isset($linea["tipo_cili_pev"]) ? $linea["tipo_cili_pev"] : NULL;
		$volumen_actu = isset($linea["volumen_actu"]) ? $linea["volumen_actu"] : NULL;
		$id_estado1 = isset($linea["id_estado"]) ? $linea["id_estado"] : NULL;
		$id_cliente1 = isset($linea["id_cliente"]) ? $linea["id_cliente"] : NULL;
		$id_especificacion1 = isset($linea["id_especi_pev"]) ? $linea["id_especi_pev"] : NULL;
		$id_conversion1 = isset($linea["id_conversion"]) ? $linea["id_conversion"] : NULL;
		$tipo_eto_pev = isset($linea["tipo"]) ? $linea["tipo"] : NULL;
		$id_tipo_gas_pev1 = isset($linea["id_tipo_gas_pev"]) ? $linea["id_tipo_gas_pev"] : NULL;
		$pre_trabajo = isset($linea["pre_trabajo"]) ? $linea["pre_trabajo"] : NULL;
		$fech_crea = isset($linea["fech_crea"]) ? $linea["fech_crea"] : NULL;
		$hilos_rosca = isset($linea["hilos_rosca"]) ? $linea["hilos_rosca"] : NULL;
		$costura = isset($linea["costura"]) ? $linea["costura"] : NULL;
		if($id_conexion1 == 9){
			$otra_valvula = isset($linea["otra_valvula"]) ? $linea["otra_valvula"] : NULL;
		}
		mysqli_free_result($resultado);
		if ($resultado == true)
	    {
	      	$consulta1  = "SELECT * FROM estados_pev WHERE id_estado_pev = $id_estado1";
		  	$resultado1 = mysqli_query($con,$consulta1) ;
		  	$linea1 = mysqli_fetch_array($resultado1);
		  	$id_estado2 = isset($linea1["estado_pev"]) ? $linea1["estado_pev"] : NULL;
		  	mysqli_free_result($resultado1);

		  	$consulta2  = "SELECT * FROM clientes WHERE id_cliente = $id_cliente1";
		  	$resultado2 = mysqli_query($con,$consulta2) ;
		  	$linea2 = mysqli_fetch_array($resultado2);
		  	$id_cliente3 = isset($linea2["nombre"]) ? $linea2["nombre"] : NULL;
		  	mysqli_free_result($resultado2);

		  	$consulta3  = "SELECT * FROM tipo_envace WHERE id_tipo_envace = ".$id_tipo_envace;
		  	$resultado3 = mysqli_query($con,$consulta3) ;
		  	$linea3 = mysqli_fetch_array($resultado3);
		  	$tipo = isset($linea3["tipo"]) ? $linea3["tipo"] : NULL;

		  	mysqli_free_result($resultado3);	

		  	$consulta7 = "SELECT * FROM has_movimiento_cilindro_pev WHERE num_cili LIKE '%$num_cili_pev%' ORDER BY id_has_movimiento_cilindro_pev DESC LIMIT 1";
			$resultado7 = mysqli_query($con,$consulta7);

			$linea7 = mysqli_fetch_array($resultado7);

			$material_cilindro = isset($linea7["material_cilindro_u"]) ? $linea7["material_cilindro_u"] : NULL;

	    }		
	    
	}

require_once("inc/init.php");
require_once("inc/config.ui.php");

if($tipo_eto_pev == 1)
{
	$page_title = "Cilindros-ETO";	
}
else
{
	$page_title = "Cilindros-PEV";
}


$page_css[] = "your_style.css";
include("inc/header.php");
$page_nav[""][""][""][""] = true;
include("inc/nav.php");
?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
	<div id="content">
		<section id="widget-grid" class="">
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-6">
					<div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h6>Datos Básicos Cilindro </h6>			
						</header>
						<div>
							<div class="jarviswidget-editbox"></div>
							<div class="widget-body no-padding">
								<form id="form1" class="smart-form" novalidate="novalidate" action="" method="POST" name="form1">
									<input type="hidden" name="id_cilindro_pev" id="id_cilindro_pev" value="<?php echo $id_cilindro_pev; ?>">
									<input type="hidden" name="id_cliente" id="id_cliente" value="<?php echo $id_cliente; ?>">
									<input type="hidden" name="tipo_eto_pev" id="tipo_eto_pev" value="<?php echo $tipo_eto_pev; ?>">
									<fieldset>
										<div class="row">												
											<section class="col col-5">
												<label class="font-lg">Número Cilindro :</label>
												<label class="input"> 
													<input type="text" class="input-lg" name="num_cili_pev" placeholder="Número Cilindro" title="Ingrese un Número Cilindro" required value="<?php echo isset($num_cili_pev) ? $num_cili_pev : NULL; ?>" >
												</label>
											</section>
											<?php
											if($tipo_eto_pev == 2)
											{
												?>
												<section class="col col-7">
													<label class="font-lg">Número Cilindro Confirmado:</label>
													<label class="input"> 
														<input type="text" class="input-lg" name="num_cili_pev_2" placeholder="Número Cilindro Confirmado" title="Ingrese un Número Cilindro" required value="<?php echo isset($num_cili_pev_2) ? $num_cili_pev_2 : NULL; ?>" >
													</label>
												</section>
												<?php
											}
											?>
											<section class="col col-3">	
												<?php
													$consulta6 ="SELECT * 
						                                      FROM especificacion ORDER BY especificacion ";
						                            $resultado6 = mysqli_query($con,$consulta6) ;	
													echo "<section>";
													echo "<label class='font-lg'>Especificación:</label>";
													echo"<label class='select'>";
													echo "<select name='id_especificacion1' class='input-lg'  onChange='conversion()'  >";
													echo "<option value='0'>Seleccione...</option>";
													while($linea6 = mysqli_fetch_array($resultado6))
					                                    {
					                                      $id_especificacion = $linea6['id_especificacion'];
					                                      $especificacion = $linea6['especificacion'];
					                                      if ($id_especificacion==$id_especificacion1)
					                                      {
					                                          echo "<option value='$id_especificacion' selected >$especificacion</option>"; 
					                                      }
					                                      else 
					                                      {
					                                          echo "<option value='$id_especificacion'>$especificacion</option>"; 
					                                      } 
					                                    }//fin while
													echo "</select>";
													echo "<i></i>";
													echo "</label>";
													echo "</section>";																
												?>
											</section>												
											<section class="col col-3">
												<label class="font-lg">&nbsp  </label>
												<label class="input"> 
													<input type="text" class="input-lg" title="Especificación" class="input-lg" name="especi_pev"  id="especi_pev" placeholder="Especificación " value="<?php echo isset($especi_pev) ? $especi_pev : NULL; ?>" required>
												</label>
											</section>
								
										</div>
										<div class="row">												
											<section class="col col-6">	
												<section>
													<label class="font-lg">Tipo Gas :</label>
													<label class='select'>
														<select name='id_tipo_cilindro1' class="input-lg" onChange='cilindro(this.value)' title='Ingrese un Número Cilindro' required>
															<option value='0'>Seleccione...</option>
															<?php
															if($tipo_eto_pev == 1)
															{
																$consulta6 ="SELECT * FROM tipo_cilindro ";
																$resultado6 = mysqli_query($con,$consulta6) ;
															
																while($linea6 = mysqli_fetch_array($resultado6))
								                                {
								                                  $id_tipo_cilindro = $linea6['id_tipo_cilindro'];
								                                  $tipo_cili = $linea6['tipo_cili'];
								                                  if ($id_tipo_cilindro==$id_tipo_cilindro1)
								                                  {
								                                      echo "<option value='$id_tipo_cilindro' selected >$tipo_cili</option>"; 
								                                  }
								                                  else 
								                                  {
								                                      echo "<option value='$id_tipo_cilindro'>$tipo_cili</option>"; 
								                                  } 
								                                }//fin while
								                            }
								                            if($tipo_eto_pev == 2)
								                            {
								                            	$consulta6 ="SELECT * FROM tipo_gas_pev ORDER BY nombre ASC";
																$resultado6 = mysqli_query($con,$consulta6) ;
															
																while($linea6 = mysqli_fetch_array($resultado6))
								                                {
								                                  $id_tipo_gas_pev = $linea6['id_tipo_gas_pev'];
								                                  $nombre = $linea6['nombre'];
								                                  if ($id_tipo_gas_pev==$id_tipo_gas_pev1)
								                                  {
								                                      echo "<option value='$id_tipo_gas_pev' selected >$nombre</option>"; 
								                                  }
								                                  else 
								                                  {
								                                      echo "<option value='$id_tipo_gas_pev'>$nombre</option>"; 
								                                  } 
								                                }//fin while
								                            }
							                                ?> 
														</select>
														<i></i>
													</label>
												</section>		
											</section>
											<?php
												if($material_cilindro == 3)
												{
													?>
													<section class="col col-6">
														<section>
														<label class="font-lg">Costura :</label>
														<label class='select'>
														<select name="costura" class="input-lg">
															<option value="0">--Seleccione--</option>
															<option value="1" <?php if($costura==1) echo "selected" ?>>Con Costura</option>
															<option value="2" <?php if($costura==2) echo "selected" ?>>Sin Costura</option>
														</select>
														<i></i>
														</label>
														</section>	
													</section>													
													<?php
												}
											?>
																				
										</div>
										<div class="row">
											<section class="col col-6">	
												<?php
													$consulta6 ="SELECT * 
						                                      FROM conversion";
						                            $resultado6 = mysqli_query($con,$consulta6) ;	
													echo "<section>";
													echo "<label class='font-lg'>Unidad :</label>";
													echo"<label class='select'>";
													echo "<select name='id_conversion1' class='input-lg' onChange='conversion()'  >";
													echo "<option value='0'>Seleccione...</option>";
													while($linea6 = mysqli_fetch_array($resultado6))
					                                    {
					                                      $id_conversion = $linea6['id_conversion'];
					                                      $conversion = $linea6['conversion'];
					                                      if ($id_conversion==$id_conversion1)
					                                      {
					                                          echo "<option value='$id_conversion' selected >$conversion</option>"; 
					                                      }
					                                      else 
					                                      {
					                                          echo "<option value='$id_conversion'>$conversion</option>"; 
					                                      } 
					                                    }//fin while
													echo "</select>";
													echo "<i></i>";
													echo "</label>";
													echo "</section>";																
												?>
											</section>												
											<section class="col col-6">
												<label class="font-lg">Pre Trabajo :</label>
												<label class="input"> 
													<input type="text" name="pre_tra_pev_1" id="" class="input-lg"  placeholder="Presión Trabajo" onKeyUp='conversion()' onkeypress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" value="<?php echo isset($pre_trabajo) ? $pre_trabajo : NULL; ?>" required>
												</label>
											</section>
										</div>
										<div class="row">												
											<section class="col col-6">
												<label class="font-lg">Presión Trabajo (Psi) :</label>
												<label class="input"> 
													<input type="text" class="input-lg" name="pre_tra_pev" readonly placeholder="Presión Trabajo" id="pre_tra_pev" value="<?php echo isset($pre_tra_pev) ? $pre_tra_pev : NULL; ?>" onkeypress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" required>
												</label>
											</section>
											<section class="col col-6">
												<label class="font-lg">Presión Prueba (Psi) :</label>
												<label class="input"> 
													<input type="text" class="input-lg" name="pre_pru_pev" readonly placeholder="Presión Prueba" id="pre_pru_pev"  value="<?php echo isset($pre_pru_pev) ? $pre_pru_pev : NULL; ?>" onkeypress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" required>
												</label>
											</section>										
										</div>
										<div class="row">												
											<section class="col col-6">
												<label class="font-lg">Fecha Fabricación :</label>
												<label class="input"> 
													<input type="date" class="input-lg" name="fecha_fab_pev" placeholder="Fecha Fabricación"  title="Ingrese una Fecha de Fabricación" required value="<?php echo isset($fecha_fab_pev) ? $fecha_fab_pev : NULL; ?>">
												</label>
											</section>
											<section class="col col-6">
												<label class="font-lg">Fecha Última Revisión (PH):</label>
												<label class="input"> 
													<input type="date" class="input-lg" name="fecha_ult_pev" placeholder="Fecha Última Revisión"  title="Ingrese una Fecha de Última Revisión" required value="<?php echo isset($fecha_ult_pev) ? $fecha_ult_pev : NULL; ?>">
												</label>
											</section>																						
										</div>
										<div class="row">												
											<section class="col col-6">
												<?php
												if($tipo_eto_pev == 1)
												{
													?>
													<label class="font-lg">Volumen Actual (Lt):</label>	
													<?php
												}
												else
												{
													?>
													<label class="font-lg">Volumen Actual (Lt):</label>	
													<?php	
												}
												?>
												<label class="input"> 
													<input type="text" class="input-lg" name="volumen_actu" placeholder="Volumen"  title="Ingrese el Volumen Actual" required value="<?php echo isset($volumen_actu) ? $volumen_actu : NULL; ?>" onkeypress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;">
												</label>
											</section>
											<section class="col col-6">
												<label class="font-lg">Volumen Revisado(Lt):</label>
												<label class="input"> 
													<input type="text" class="input-lg" name="volumen_pev" placeholder="Volumen"  title="Ingrese el Volumen" required value="<?php echo isset($volumen_pev) ? $volumen_pev : NULL; ?>" onkeypress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;">
												</label>
											</section>									
										</div>
										<div class="row">												
											<section class="col col-6">
												<?php
												if($tipo_eto_pev == 1)
												{
													?>
													<label class="font-lg">Tara Actual (Kg):</label>
													<?php
												}
												else
												{
													?>
													<label class="font-lg">Tara Actual (Kg):</label>
													<?php	
												}
												?>
												<label class="input"> 
													<input type="text" class="input-lg" name="tara_esta_pev" placeholder="Tara Actual"  title="Ingrese la Tara Estampada" required value="<?php echo isset($tara_esta_pev) ? $tara_esta_pev : NULL; ?>" onkeypress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;">
												</label>
											</section>
											<section class="col col-6">
												<label class="font-lg">Tara Verificada (Kg):</label>
												<label class="input"> 
													<input type="text" class="input-lg" name="tara_actu_pev" placeholder="Tara Verificada"  title="Ingrese la Tara Actual" required value="<?php echo isset($tara_actu_pev) ? $tara_actu_pev : NULL; ?>" onkeypress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;">
												</label>
											</section>										
										</div>
										<div class="row">												
											<section class="col col-6">
												<label class="font-lg">Espesor Fabricación (mm):</label>
												<label class="input"> 
													<input type="text" class="input-lg" name="esp_fab_pev" placeholder="Espesor Fabricación"  title="Ingrese el Espesor Fabricación" required value="<?php echo isset($esp_fab_pev) ? $esp_fab_pev : NULL; ?>" onkeypress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;">
												</label>
											</section>
											<section class="col col-6">
												<label class="font-lg">Espesor Actual (mm):</label>
												<label class="input"> 
													<input type="text" class="input-lg" name="esp_actu_pev" placeholder="Espesor Actual"  title="Ingrese el Espesor Actual" required value="<?php echo isset($esp_actu_pev) ? $esp_actu_pev : NULL; ?>" onkeypress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;">
												</label>
											</section>										
										</div>
										<div class="row">												
											<section class="col col-6">
												<label class='font-lg'>Tipo de Cilindro :</label>
												<label class='select'>
													<select class='input-lg' name='id_tipo_cili1'>
														<option value='0'>Seleccione...</option>
														<?php
															$consulta6 ="SELECT * FROM tipo_cili ORDER BY tipo ASC";
															$resultado6 = mysqli_query($con,$consulta6) ;
															if(mysqli_num_rows($resultado6) > 0)
															{
																while($linea6 = mysqli_fetch_assoc($resultado6))
																{
																	$id_tipo_cili = $linea6['id_tipo_cili'];
																	$tipo = $linea6['tipo']."-".$linea6['observacion'];
																	$tipo = utf8_encode($tipo);
																	if ($id_tipo_cili==$id_tipo_cili1)
																	{
																			echo "<option value='$id_tipo_cili' selected >$tipo</option>"; 
																	}
																	else 
																	{
																			echo "<option value='$id_tipo_cili'>$tipo</option>"; 
																	} 
																}
															}
														?>
													</select><i></i>
												</label>												
											</section>
											<section class="col col-6">	
												<?php

												if($tipo_eto_pev == 1)
												{
													$consulta6 ="SELECT * FROM conexion_valvula ORDER BY tip_cone ASC";
													$resultado6 = mysqli_query($con,$consulta6) ;
													echo "<section>";
													echo "<label class='font-lg'>Conexión Valvula :</label>";
													echo"<label class='select'>";
													echo "<select class='input-lg' name='id_conexion1'>";
													echo "<option value='0'>Seleccione...</option>";
													while($linea6 = mysqli_fetch_array($resultado6))
													{
														$id_conexion = $linea6['id_conexion'];
														$tip_cone = $linea6['tip_cone'];
														if ($id_conexion==$id_conexion1)
														{
																echo "<option value='$id_conexion' selected >$tip_cone</option>"; 
														}
														else 
														{
																echo "<option value='$id_conexion'>$tip_cone</option>"; 
														} 
													}//fin while 
													echo "</select>";
													echo "<i></i>";
													echo "</label>";
													echo "</section>";	
												}
												elseif ($tipo_eto_pev == 2) 
												{
													?>
													<section>
														<label class="font-lg">Conexión Válvula:</label>
														<label class="select">
															<select class="input-lg" name="id_conexion1" onchange="validar_valvula(this.value)">
															<option value='0' selected>Seleccione...</option>
																<?php
																	$consulta = "SELECT * FROM conexion_valvula_pev";
																	$resultado = mysqli_query($con,$consulta);
																	if(mysqli_num_rows($resultado) > 0)
																	{
																		while($linea = mysqli_fetch_assoc($resultado))
																		{
																			$id_valvula_pev = $linea['id_valvula_pev'];
																			$tipo_valvula = $linea['tipo_valvula'];
																			if($id_valvula_pev == $id_conexion1)
																			{
																				echo "<option value='$id_valvula_pev' selected>$tipo_valvula</option>";
																			}
																			else
																			{
																				echo "<option value='$id_valvula_pev'>$tipo_valvula</option>"; 
																			}
																		}
																	}
																?>
															</select><i></i>
														</label>
													</section>
													<?php
												}															
												?>
											</section>												
										</div>
										<div class="row">												
											<section class="col col-6">
												<label class="font-lg">Hilos de la Rosca:</label>
												<label class="input"> 
													<input type="text" class="input-lg" name="hilos_rosca" placeholder="Numero de Hilos"  title="Ingrese el numero de hilos de la Rosca" required value="<?php echo isset($hilos_rosca) ? $hilos_rosca : NULL; ?>">
												</label>
											</section>
											<section class="col col-6">
												<label class="font-lg">Válvula:</label>
												<label class="input"> 
													<input type="text" class="input-lg" name="otr_valvula" placeholder="Valvula" id="otra_valvula" disabled title="Ingrese la información de la Valvula" required value="<?php echo isset($otra_valvula) ? $otra_valvula : NULL; ?>">
												</label>
											</section>
										</div>
										<div class="row">
											<section class="col col-6">
												<label class="font-lg">Fecha De Lavado :</label>
												<label class="input"> 
													<input type="date" class="input-lg" name="fecha_lav_pev" placeholder="Conexión Válvula"  value="<?php echo isset($fecha_lav_pev) ? $fecha_lav_pev : NULL; ?>" title="Ingrese la fecha de lavado" required>
												</label>
											</section>												
											<section class="col col-6">	
												<?php
												$consulta6 ="SELECT * FROM propiedad_cilindro ORDER BY propiedad ASC";
												$resultado6 = mysqli_query($con,$consulta6) ;
												echo "<section>";
												echo "<label class='font-lg'>Propiedad :</label>";
												echo"<label class='select'>";
												echo "<select class='input-lg' name='id_propiedad_cilindro1' onChange='cliente(this.value)'>";
												echo "<option value='0'>Seleccione...</option>";
												while($linea6 = mysqli_fetch_array($resultado6))
												{
													$id_propiedad_cilindro = $linea6['id_propiedad_cilindro'];
													$propiedad = $linea6['propiedad'];
													if ($id_propiedad_cilindro==$id_propiedad_cilindro1)
													{
															echo "<option value='$id_propiedad_cilindro' selected >$propiedad</option>"; 
													}
													else 
													{
															echo "<option value='$id_propiedad_cilindro'>$propiedad</option>"; 
													} 
												}//fin while 
												echo "</select>";
												echo "<i></i>";
												echo "</label>";
												echo "</section>";
															
												?>
											</section>												
										</div>
										<div class="row" >
											<section class="col col-6" id="cili">
												<label class="font-lg">Cliente:</label>
												<label class="input"> 
													<input type="text" class="input-lg" name="nombre" placeholder="Cliente"  readonly  value="<?php echo isset($id_cliente3) ? $id_cliente3 : NULL; ?>">
												</label>
											</section>
											<section class="col col-6">
												<label class="font-lg">Creación :</label>
												<label class="input"> 
													<input type="date" class="input-lg" name="fech_crea" placeholder="Cliente" readonly  value="<?php echo isset($fech_crea) ? $fech_crea : NULL; ?>">
												</label>
											</section>													
										</div>
										<?php
										if ($id_cilindro_pev>0) 
										{
										?>
											<div class="row" >
												<section class="col col-6">	
													<?php
													$consulta6 ="SELECT * FROM estados_pev ORDER BY id_estado_pev ASC";
													$resultado6 = mysqli_query($con,$consulta6) ;
													echo "<section>";
													echo "<label class='font-lg'>Estado :</label>";
													echo"<label class='select'>";
													echo "<select class='input-lg' name='id_estado1'>";
													echo "<option value='0'>Seleccione...</option>";
													while($linea6 = mysqli_fetch_array($resultado6))
													{
														$id_estado_cilindro = $linea6['id_estado_pev'];
														$estado_cili_pev = $linea6['estado_pev'];
														if ($id_estado_cilindro==$id_estado1)
														{
															echo "<option value='$id_estado_cilindro' selected >$estado_cili_pev</option>"; 
														}
														else 
														{
															echo "<option value='$id_estado_cilindro'>$estado_cili_pev</option>"; 
														} 
													}//fin while 
													echo "</select>";
													echo "<i></i>";
													echo "</label>";
													echo "</section>";
																
													?>
												</section>													
											</div>
										<?php
										}
										?>
									</fieldset>		
									
																
										<footer>                    
					                    	<input type="submit" value="Guardar" name="g_cilindro_pev" id="g_cilindro_pev" class="btn btn-primary" />
					                  	</footer>
				                  	
                				</form>								
							
             				</div>			              
				        </div>            
         			</div> 
        		</article>
     		</div>
   		</section> 
   		    <!-- Modal -->
	    <div class="modal fade" id="modal_cliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">
			<form name="mezcla" method="POST" id="mezcla">
			<input type="hidden" name="id_cilindro_pev"  value="id_cilindro_pev">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							&times;
							</button>
							<h4 class="modal-title" id="myModalLabel">Seleccione el Cliente</h4>
						</div>
						<div class="modal-body">
							<div class="row">
								<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">						
									<form  name="form2" id="form2">							
										<thead>
											<tr>
												<th>#</th>
												<th>Acción</th>
												<th>Cliente</th>
												<th>Nit</th>									
											</tr>
										</thead>
										<tbody>										
											<?php                                
			                        			$contador = "0";
			                        	        $consulta = "SELECT * FROM clientes ";
			                                    $resultado = mysqli_query($con,$consulta) ;
			                                    while ($linea = mysqli_fetch_array($resultado))
			                                    {
			                                        $contador = $contador + 1;
			                                        $id_cliente = $linea["id_cliente"];
			                                        $nombre = $linea["nombre"];
			                                        $telefono_fijo = $linea["telefono_fijo"];
			                                        $telefono_celular = $linea["telefono_celular"];
			                                        $correo = $linea["correo"];
			                                        $persona_contacto = $linea["persona_contacto"];
			                                        $direccion = $linea["direccion"];
			                                        $observacion = $linea["observacion"];
			                                        $nit = $linea["nit"];
			                             
			                                    ?>
			                                        <tr>                                                	
			                                            <td width="5"><?php echo $contador; ?></td>
			                                            <td width="5"><a href="#" onclick="g_cliente(<?php echo $id_cliente; ?>)"><img src="img/chulo.png" width="20" height="20"></a></td>
			                                            <td><?php echo $nombre; ?></td>
			                                            <td><?php echo $nit; ?></td>                                            
			                                    	</tr>   
			                                    <?php
			                                    }mysqli_free_result($resultado);   
			                                	
			                                ?>
										</tbody>
									</form>
								</table>
							</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">	Cancel</button>	
						</div>
					</div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			</form>
		</div>
		<!-- Fin Modal -->
  	</div>
</div>



<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->
<?php
  include("inc/footer.php");
  include("inc/scripts.php"); 
?>
<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/jquery-form/jquery-form.min.js"></script>
<script src="js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<script type="text/javascript">
function confirmar()
{
	//return false;
	fecha_ult_pev=form1.fecha_ult_pev.value;
	fecha_lav_pev=form1.fecha_lav_pev.value;
	var txt;
    var r = confirm("Esta seguro de Crear el Cilindro ?\n Fecha lavado ="+fecha_lav_pev+"\nFecha PH ="+fecha_ult_pev);
    if (r == true) {
        submit.form1();
        //txt = "You pressed OK!";
    } else {
        return false;
    }
}
</script>

<script type="text/javascript">

  function cilindro(id_tipo_cilindro1)
  {
    document.form1.id_tipo_envace.length=0;
    document.form1.id_tipo_envace.options[0] = new Option("Seleccione...","0","defaultSelected","");
    var indice=1;
    <?php
    $consulta = "SELECT * 
          FROM tipo_envace 
          ORDER BY tipo";
    $resultado = mysqli_query($con,$consulta);
    if(mysqli_num_rows($resultado)>0)
    {
      while($linea = mysqli_fetch_assoc($resultado))
      {
        ?>
        if(id_tipo_cilindro1=='<?php echo $linea["id_tipo_cilindro"]; ?>')
        {
          document.form1.id_tipo_envace.options[indice] = new Option("<?php echo $linea["tipo"]; ?>","<?php echo $linea["id_tipo_envace"]?>");
          indice++;
        }
        <?php
      }
    }
    ?>
  }

</script>
<script type="text/javascript">
function cliente(clientes) 
{	
	
	if (clientes==1) 
	{
		//alert(clientes);
		$("#modal_cliente").modal();
		$("#cili").show();
	}
	else
	{
		$("#cili").hide();
	}
}	

</script>
<script type="text/javascript">


function conversion()
{
		id_conversion=form1.id_conversion1.value;	
		id_especificacion=form1.id_especificacion1.value;
		//var id_especificacion = document.getElementById("id_especificacion1").value;
		//alert(id_especificacion);	
		
		if (id_especificacion==1) 
		{
			valor_espe= (5/3);
		}
		if (id_especificacion==2) 
		{
			valor_espe= (5/3);
		}
		if (id_especificacion==3) 
		{
			valor_espe= 1.5;
			document.getElementById('pre_pru_pev').readOnly = false;
			document.getElementById('pre_tra_pev').readOnly = false;
		}
		if (id_especificacion==4) 
		{
			valor_espe= 1.5;
		}
		if (id_conversion==1) 
		{
			valor= 14.5038;
		}
		if (id_conversion==2) 
		{
			valor= 0.000145038;
		}
		if (id_conversion==3) 
		{
			valor= 145.037699722;
		}
		if (id_conversion==4) 
		{
			valor= 14.6959;
		}
		if (id_conversion==5) 
		{
			valor= 1;
		}		
		//alert(valor_espe);
		
	    form1.pre_tra_pev.value = (((parseInt(form1.pre_tra_pev_1.value))*valor)).toFixed(2);
	    form1.pre_pru_pev.value = (((parseInt(form1.pre_tra_pev_1.value))*valor)*valor_espe).toFixed(2);
}
function conversion2()
{
		id_conversion1=form1.id_conversion2.value;

		if (id_conversion1==1) 
		{
			valor= 14.5038;
		}
		if (id_conversion1==2) 
		{
			valor= 0.000145038;
		}
		if (id_conversion1==3) 
		{
			valor= 0.0193368;
		}
		if (id_conversion1==4) 
		{
			valor= 14.6959;
		}
		if (id_conversion1==5) 
		{
			valor= 1;
		}	
	     
	    form1.pre_pru_pev.value = ((parseInt(form1.pre_pru_pev_1.value))*valor).toFixed(2);      
	     

	    
}

function g_cliente(num_cili1) 
{
	//alert("hola");
	//var num_cili1 = $('input:text[name=cliente]').val();

	$('#modal_cliente').modal('hide');
	$('input:hidden[name=id_cliente]').val(num_cili1);

	$.ajax({
		url: 'id_cliente.php',
		type: 'POST',
		data: 'dato='+num_cili1,
	})
	.done(function(data) {
		var objeto = JSON.parse(data);							
		
		$("input[name=nombre]").val(objeto.nombre);							
		
		document.getElementById("form2").reset();

	})
	.fail(function() {
		console.log("error");
	});
	
}

function validar_valvula(dato){
	if(dato==9){
		document.getElementById("otra_valvula").disabled = false;
	}else{
		document.getElementById("otra_valvula").disabled = true;
	}
}

</script>



<?php 
  //include footer
  include("inc/google-analytics.php"); 
}
else
{
    header("Location:index.php");
}
?>
					         