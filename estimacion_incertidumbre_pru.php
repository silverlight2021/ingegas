<?php
date_default_timezone_set("America/Bogota");

require ("libraries/conexion.php");
session_start();
if(@$_SESSION['logged']== 'yes')
{ 
	$id_user=$_SESSION['su'];
    $acc = $_SESSION['acc'];
    $letra = isset($_REQUEST['letra']) ? $_REQUEST['letra'] : NULL;
    //$id_orden = isset($_REQUEST['id_orden']) ? $_REQUEST['id_orden'] : NULL;
    $id_calibracion = isset($_REQUEST['id_calibracion']) ? $_REQUEST['id_calibracion'] : NULL;
    $contador_orden = 0;

	require_once("inc/init.php");
	require_once("inc/config.ui.php");
	$page_title = "Calibración";
	$page_css[] = "your_style.css";
	include("inc/header.php");
	$page_nav[""][""][""][""] = true;
	include("inc/nav.php");	
?>
<?php
	$consulta1 = "SELECT Name, LastName FROM user WHERE idUser = '$id_user'";
	$resultado1 = mysqli_query($con,$consulta1);
	$linea1 = mysqli_fetch_assoc($resultado1);

	$nombre = isset($linea1["Name"]) ? $linea1["Name"] : NULL;
	$apellido = isset($linea1["LastName"]) ? $linea1["LastName"] : NULL;

	$fecha = date("Y-m-d");
	$hora = date("G:i:s",time());

	$consulta2 = "SELECT * FROM verificacion_metodo_pev WHERE id_verificacion = $id_calibracion";
	$resultado2 = mysqli_query($con,$consulta2);
	if(mysqli_num_rows($resultado2)>0){
		$linea2 = mysqli_fetch_assoc($resultado2);

		$temperatura_camisa = $linea2["temperatura_camisa"];
		$temperatura_cilindro = $linea2["temperatura_cilindro"];
		$diferencia_temperatura = $linea2["diferencia_temperatura"];
		$presion_mano1 = $linea2["presion_manometro1"];
		$presion_mano2 = $linea2["presion_manometro2"];
		$presion_mano3 = $linea2["presion_manometro3"];
		$presion_mano4 = $linea2["presion_manometro4"];
		$presion_mano5 = $linea2["presion_manometro5"];
		$expansion1 = $linea2["expansion1"];
		$expansion2 = $linea2["expansion2"];
		$expansion3 = $linea2["expansion3"];
		$expansion4 = $linea2["expansion4"];
		$expansion5 = $linea2["expansion5"];
		$error1 = $linea2["error1"];
		$error2 = $linea2["error2"];
		$error3 = $linea2["error3"];
		$error4 = $linea2["error4"];
		$error5 = $linea2["error5"];
		$estado_calibracion = $linea2["estado_calibracion"];
		$contador_orden++;
	}

	$consulta3 = "SELECT MAX(id_incertidumbre) AS id_incertidumbre FROM incertidumbre";
	$resultado3 = mysqli_query($con,$consulta3);
	$linea3 = mysqli_fetch_assoc($resultado3);

	$id_incertidumbre = isset($linea3['id_incertidumbre']) ? $linea3['id_incertidumbre'] : NULL;

	$consulta4 = "SELECT incertidumbre FROM incertidumbre WHERE id_incertidumbre = '$id_incertidumbre'";
	$resultado4 = mysqli_query($con,$consulta4);
	$linea4 = mysqli_fetch_assoc($resultado4);

	$incertidumbre = isset($linea4['incertidumbre']) ? $linea4['incertidumbre'] : NULL;

	$consulta5 = "SELECT MAX(id_incertidumbre_manometro) AS id_incertidumbre_manometro FROM incertidumbre_manometro";
	$resultado5 = mysqli_query($con,$consulta5);
	$linea5 = mysqli_fetch_assoc($resultado5);

	$id_incertidumbre_manometro = isset($linea5['id_incertidumbre_manometro']) ? $linea5['id_incertidumbre_manometro'] : NULL;

	$consulta6 = "SELECT incertidumbre_manometro FROM incertidumbre_manometro WHERE id_incertidumbre_manometro = '$id_incertidumbre_manometro'";
	$resultado6 = mysqli_query($con,$consulta6);
	$linea6 = mysqli_fetch_assoc($resultado6);

	$incertidumbre_manometro = isset($linea6['incertidumbre_manometro']) ? $linea6['incertidumbre_manometro'] : NULL;

?>
<!-- MAIN PANEL -->
<div id="main" role="main">
<?php
if (in_array(39, $acc))
{
?>
	<div id="content">
		<div class="" align="center">
			<h6  class="page-title txt-color-blueDark">ESTIMACIÓN DE LA INCERTIDUMBRE</h6>			
		</div>
		<div class="" align="center">
			<h7  class="page-title txt-color-blueDark">Incertidumbre debido a la calibración del manómetro, U (pres.), psig</h7>			
		</div>


		<table  class="table table-striped table-bordered table-hover" width="100%">
			<tr colspan = "5">
				<strong>Incertidumbre expandida del manómetro - U (psig)</strong>
			</tr>
			<tr>
				<td>
					U Exp. Manómetro PSIG
				</td>
					
				<td>
					Factor de cobertura (K)
				</td>
				<td>
					Formula
				</td>
				<td>
					u(p), PSIG
				</td>
			</tr>
			<tr>
				<td>
					5.4
				</td>
				<td>
					2
				</td>
				<td>
					u(p): U(p)/K
				</td>
				<td>
					2.7
				</td>
			</tr>						
		</table>
		
		</br>
		</br>

		<table  class="table table-striped table-bordered table-hover" width="100%">
			<tr>
				<strong>Estimación de la incertidumbre por resolución del manómetro en la medición de la expansión</strong>
			</tr>
			<tr>
				<td width="33%">
					Resolución en la escala del manómetro (REM) PSIG
				</td>
					
				<td>
					Formula
				</td>
				<td>
					U(rem):
				</td>
				
			</tr>
			<tr>
				<td>
					10
				</td>
				<td>
					u(rem): REM/√3
				</td>
				<td>
					5,773502692
				</td>
				
			</tr>						
		</table>

		<table  class="table table-striped table-bordered table-hover" width="100%">
			<tr>
				<td>
					<strong>(P1-P2)</strong>
				</td>
					
				<td>
					<strong>(P1-P2)2</strong>
				</td>
				<td>
					<strong>Desviación estándar</strong>
				</td>
				<td>
					<strong>Incertidumbre x repetibilidad</strong>
				</td>
				<td>
					<strong>Incertidumbre estándar combinada x rango de presión</strong>
				</td>
			</tr>
			<tr>
				<td>
					<?php
						echo $va1 = (0-$presion_mano1);
					?>
				</td>
				<td>
					<?php
						echo $va12 = pow($va1,2);
					?>
				</td>
				<td>
					<?php
						echo $va13 = sqrt($va12/1);
					?>
				</td>
				<td>
					<?php
						echo $va14 = ($va13/(sqrt(1)));
					?>
				</td>
				<td>
					<?php
						echo $va15 = sqrt((pow($va14,2))+(pow(2.7,2))+(pow(5.773502692,2)));
					?>
				</td>
			</tr>
			<tr>
				<td>
					<?php
						echo $va2 = (2000-$presion_mano2);
					?>
				</td>
				<td>
					<?php
						echo $va22 = pow($va2,2);
					?>
				</td>
				<td>
					<?php
						echo $va23 = sqrt($va22/1);
					?>
				</td>
				<td>
					<?php
						echo $va24 = ($va23/(sqrt(1)));
					?>
				</td>
				<td>
					<?php
						echo $va25 = sqrt((pow($va24,2))+(pow(2.7,2))+(pow(5.773502692,2)));
					?>
				</td>
			</tr>
			<tr>
				<td>
					<?php
						echo $va3 = (3000-$presion_mano3);
					?>
				</td>
				<td>
					<?php
						echo $va32 = pow($va3,2);
					?>
				</td>
				<td>
					<?php
						echo $va33 = sqrt($va32/1);
					?>
				</td>
				<td>
					<?php
						echo $va34 = ($va33/(sqrt(1)));
					?>
				</td>
				<td>
					<?php
						echo $va35 = sqrt((pow($va34,2))+(pow(2.7,2))+(pow(5.773502692,2)));
					?>
				</td>
			</tr>
			<tr>
				<td>
					<?php
						echo $va4 = (4000-$presion_mano4);
					?>
				</td>
				<td>
					<?php
						echo $va42 = pow($va4,2);
					?>
				</td>
				<td>
					<?php
						echo $va43 = sqrt($va42/1);
					?>
				</td>
				<td>
					<?php
						echo $va44 = ($va43/(sqrt(1)));
					?>
				</td>
				<td>
					<?php
						echo $va45 = sqrt((pow($va44,2))+(pow(2.7,2))+(pow(5.773502692,2)));
					?>
				</td>
			</tr>
			<tr>
				<td>
					<?php
						echo $va5 = (5000-$presion_mano5);
					?>
				</td>
				<td>
					<?php
						echo $va52 = pow($va5,2);
					?>
				</td>
				<td>
					<?php
						echo $va53 = sqrt($va52/1);
					?>
				</td>
				<td>
					<?php
						echo $va54 = ($va53/(sqrt(1)));
					?>
				</td>
				<td>
					<?php
						echo $va55 = sqrt((pow($va54,2))+(pow(2.7,2))+(pow(5.773502692,2)));
					?>
				</td>
			</tr>						
		</table>

		<section id="widget-grid" class="">
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-6">
					<table class="table table-striped table-bordered table-hover" width="100%">
						<tbody>
							<tr>
								<td><strong>Incertidumbre combinada por factores del manómetro en % - U	</strong></td>
								<td><strong><?php echo $incertidumbre_combinada = sqrt((pow($va15,2))+(pow($va25,2))+(pow($va35,2))+(pow($va45,2))+(pow($va55,2))); ?></strong></td>
							</tr>
						</tbody>
					</table>
				</article>
				<article class="col-sm-12 col-md-12 col-lg-6">			
				</article>
			</div>
		</section>
		
		<table  class="table table-striped table-bordered table-hover" width="100%">
			<tr>
				<strong> Incertidumbre debido a la calibración de la balanza , u(exp.); gr/cc</strong>
			</tr>
			<tr>
				<td width="25%">
					<strong>U Exp. Balanza  gr</strong>
				</td>
				<td>
					<strong>Factor de cobertura (K)</strong>
				</td>
				<td>
					<strong>Formula</strong>
				</td>
				<td>
					<strong>u(Exp), gr/cc</strong>
				</td>
			</tr>
			<tr>
				<td>
					10
				</td>
				<td>
					u(rem): REM/√3
				</td>
				<td>
					5,773502692
				</td>
				<td>
					U(rem):
				</td>
			</tr>						
		</table>

		
		<!--<div class="" align="center">
			<h6  class="page-title txt-color-blueDark">DESVIACIÓN MAXIMA  PERMITIDA 1%</h6>			
		</div>		
		<section id="widget-grid" class="">
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-12">					
					<form method="POST" action="calibracion.php">
						<table  class="table table-striped table-bordered table-hover" width="100%">
							<thead>
								<tr>
									<th>PRESIÓN DE REFERENCIA</th>
									<th>EXPANSIÓN DE REFERENCIA</th>
									<th>PRESION DE MANOMETRO</th>
									<th>EXPANSIÓN</th>
									<th>ERROR ABSOLUTO</th>
									<th>GUARDAR</th>
								</tr>
							</thead>	
							<tbody>
								<tr>
									<form id="form1" name="form1" method="post" action="guardarVerificacion.php">
										<td>0</td>
										<td>0,0</td>
										<td><input type="text" name="presion_mano1" class="input" id="presion_mano1" value="<?php echo isset($presion_mano1) ? $presion_mano1 : NULL; ?>"></td>
										<td>
											<input type="text" name="expancion1" class="input" readonly id="expancion1" value="<?php echo isset($expansion1) ? $expansion1 : NULL; ?>">
											<?php
												if($contador_orden==0){
											?>
											<button type="button" name="tom_lectura" id="tom_lectura" class="btn btn-success" onclick="validar(1)">LECTURA</button> 
											<?php
												}
											?>
										</td>
										<td>
											<input type="text" name="error_absoluto1" class="input" id="error_absoluto1" readonly value="<?php echo isset($error1) ? $error1 : NULL; ?>"> 
											<input type="hidden" name="idPresion" value="1">
										</td>
										<td>
											<?php
												if($contador_orden==0){
											?>
												<input type="hidden" name="id_user" value="<?php echo $id_user; ?>">
												<input type="hidden" name="id_calibra" value="<?php echo $id_calibracion; ?>">
												<input type="hidden" name="temperatura_camisa" id="temperatura_camisa">
												<input type="hidden" name="temperatura_cilindro" id="temperatura_cilindro" >
												<input type="hidden" name="diferencia_temperatura" id="diferencia_temperatura">
												<input type="hidden" name="idPresion" value="1">
												<input type="submit" value="Guardar" name="guardar1" id="guardar1">
											<?php
												}
											?>
										</td>
									</form>
								</tr><!-- ----------------------------1000----------------------------- 
								<tr>
									<form id="form2" name="form2" method="post" action="guardarVerificacion.php">
										<td>2000</td>
										<td>86.5</td>
										<td>
											<input type="text" class="input" name="presion_mano2" value="<?php echo isset($presion_mano2) ? $presion_mano2 : NULL; ?>" id="presion_mano2"></td>
										<td>
											<input type="text" class="input"  name="expancion2" readonly id="expancion2" value="<?php echo isset($expansion2) ? $expansion2 : NULL; ?>">
											<?php
												if(($expansion1!=NULL)&&($expansion2==NULL)){
											?>
												<button type="button" name="tom_lectura" id="tom_lectura" class="btn btn-success" onclick="validar(2)">TOMAR LECTURA</button>
											<?php
												}
											?>
										</td>
										<td>
											<input type="text" class="input" name="error_absoluto2" value="<?php echo isset($error2) ? $error2 : NULL; ?>" id="error_absoluto2">
										</td>
										<td>
											<?php
												if(($expansion1!=NULL)&&($expansion2==NULL)){
											?>
											<input type="hidden" name="idPresion" value="2">
											<input type="hidden" name="id_calibra" value="<?php echo $id_calibracion; ?>">
											<input type="submit" value="Guardar" name="guardar2" id="guardar2">
											<?php
												}
											?>
										</td>
									</form>
								</tr><!-- ----------------------------2000----------------------------- 
								<tr>
									<form id="form3" name="form3" method="post" action="guardarVerificacion.php">
										<td>3000</td>
										<td>130,0</td>
										<td><input type="text" class="input" name="presion_mano3" value="<?php echo isset($presion_mano3) ? $presion_mano3 : NULL; ?>" id="presion_mano3"></td>
										<td>
											<input type="text" class="input" name="expancion3" readonly id="expancion3" value="<?php echo isset($expansion3) ? $expansion3 : NULL; ?>">
											<?php
												if(($expansion2!=NULL)&&($expansion3==NULL)){
											?>
											<button type="button" name="tom_lectura" id="tom_lectura" class="btn btn-success" onclick="validar(3)">TOMAR LECTURA</button>
											<?php
												}
											?>
										</td>
										<td><input type="text" class="input" name="error_absoluto3" value="<?php echo isset($error3) ? $error3 : NULL; ?>" id="error_absoluto3" ></td>
										<td>
											<?php
												if(($expansion2!=NULL)&&($expansion3==NULL)){
											?>
											<input type="hidden" name="idPresion" value="3">
											<input type="hidden" name="id_calibra" value="<?php echo $id_calibracion; ?>">
											<input type="submit" value="Guardar" name="guardar3" id="guardar3">
											<?php
												}
											?>
										</td>
									</form>
								</tr><!-- ----------------------------3000----------------------------- 
								<tr>
									<form id="form4" name="form4" method="post" action="guardarVerificacion.php">
										<td>4000</td>
										<td>173,5</td>
										<td><input type="text" class="input" name="presion_mano4" value="<?php echo isset($presion_mano4) ? $presion_mano4 : NULL; ?>" id="presion_mano4"></td>
										<td>
											<input type="text" class="input" name="expancion4" readonly id="expancion4" value="<?php echo isset($expansion4) ? $expansion4 : NULL; ?>">
											<?php
												if(($expansion3!=NULL)&&($expansion4==NULL)){
											?>
											<button type="button" name="tom_lectura" id="tom_lectura" class="btn btn-success" onclick="validar(4)">TOMAR LECTURA</button>
											<?php
												}
											?>
										</td>
										<td><input type="text" class="input" name="error_absoluto4" value="<?php echo isset($error4) ? $error4 : NULL; ?>" id="error_absoluto4" ></td>
										<td>
											<?php
												if(($expansion3!=NULL)&&($expansion4==NULL)){
											?>
											<input type="hidden" name="idPresion" value="4">
											<input type="hidden" name="id_calibra" value="<?php echo $id_calibracion; ?>">
											<input type="submit" value="Guardar" name="guardar4" id="guardar4">
											<?php
												}
											?>
										</td>
									</form>
								</tr><!-- ----------------------------4000----------------------------- 
								<tr>
									<form id="form5" name="form5" method="post" action="guardarVerificacion.php">
										<td>5000</td>
										<td>217,0</td>
										<td><input type="text" class="input" name="presion_mano5" value="<?php echo isset($presion_mano5) ? $presion_mano5 : NULL; ?>" id="presion_mano5"></td>
										<td>
											<input type="text" class="input" name="expancion5" readonly id="expancion5" value="<?php echo isset($expansion5) ? $expansion5 : NULL; ?>">
											<?php
												if(($expansion4!=NULL)&&($expansion5==NULL)){
											?>
											<button type="button" name="tom_lectura" id="tom_lectura" class="btn btn-success" onclick="validar(5)">TOMAR LECTURA</button>
											<?php
												}
											?>
										</td>
										<td><input type="text" class="input" name="error_absoluto5" value="<?php echo isset($error5) ? $error5 : NULL; ?>" id="error_absoluto5" ></td>
										<td>
											<?php
												if(($expansion4!=NULL)&&($expansion5==NULL)){
											?>
											<input type="hidden" name="idPresion" value="5">
											<input type="hidden" name="id_calibra" value="<?php echo $id_calibracion; ?>">
											<input type="submit" value="Guardar" name="guardar5" id="guardar5">
											<?php
												}
											?>
										</td>
									</form>
								</tr><!-- ----------------------------5000----------------------------- 
							</tbody>
						</table>
					<!--</form>
				</article>
			</div>
		</section>-->
		<!--<form id="form9" name="form9" method="post" action="guardarVerificacion.php">
				
		<section id="widget-grid" class="">
			<div class="row">						
				<article class="col-sm-12 col-md-12 col-lg-6">
					<table class="table table-striped table-bordered table-hover" width="100%">
						<tr>
							<input type="hidden" name="error_total" id="error_total">
							<input type="hidden" value="6" name="idPresion">
							<input type="hidden" value='<?php echo $id_calibracion; ?>' name="id_calibra">
							<?php
								if($expansion5!=NULL){
							?>
								<td style="text-align: center;"><input type="submit" name="guardarCalibracion" id="guardarCalibracion" value="Guardar"></td>
								<div style="text-align: center;background: red;cursor: pointer;" id="calculoErrores" onclick="calcularTotalErrores()">Calcular Errores Absolutos</div>
							<?php
								}
							?>
						</tr>		
					</table>
				</article>
			</div>
		</section>
		</form>-->
	</div>			
<?php
}										
?>

</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->
<?php
	include("inc/footer.php");
	include("inc/scripts.php"); 
?>
<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<script>
	var t = document.getElementById("tom_lectura");
	t.disabled = true;
	t.addEventListener("mousemove", setTimeout('desbloquear()',6000),false);

	function desbloquear(){
		t.disabled = false;
	}



	function validar(campo){
		var tmp;
		var newcad;
		var cadena = "";
		var arrayData = new Array();
		var archivoTxt = new XMLHttpRequest();
		var fileRuta = 'Uploads/ArchivoNvo.txt';
		archivoTxt.open("GET",fileRuta,false);
		archivoTxt.send(null);
		var txt = archivoTxt.responseText;
		for (var i = 0; i<txt.length; i++){
			arrayData.push(txt[i]);
		}
		arrayData.forEach(function(data){
			cadena += data;
		});
		console.log(cadena);
		var lect = document.getElementById('expancion'+campo);
		lect.value=cadena;
		cadena="";

		calcularDesviacion(campo);

	}
</script>
<script type="text/javascript">
	function calcularDesviacion(item){

		var expa = document.getElementById('presion_mano'+item);
		var mano = document.getElementById('expancion'+item);
		var desvia = document.getElementById('error_absoluto'+item);
		var valor_expa = mano.value;
		//var valor_mano = mano.value;
		var valor_desvia = desvia.value;
		var valor = 0;
		var exp_cil_gal;

		if(item==1){
			exp_cil_gal = 0;
		}else if(item==2){
			exp_cil_gal = 86.5;
		}else if(item==3){
			exp_cil_gal = 130.0;
		}else if(item==4){
			exp_cil_gal = 173.5;
		}else if(item==5){
			exp_cil_gal = 217.0;
		}

		valor = ((exp_cil_gal-valor_expa)/exp_cil_gal)*100;

		if(item == 1){
			valor = 0;
		}

		//alert("(("+exp_cil_gal+"-"+valor_expa+")/"+exp_cil_gal+")*100");

		valor = valor.toFixed(2);

		desvia.value = valor;
		

	}

	function calcularTotalErrores(){

		error1 = parseFloat(document.getElementById('error_absoluto1').value);
		error2 = parseFloat(document.getElementById('error_absoluto2').value);
		error3 = parseFloat(document.getElementById('error_absoluto3').value);
		error4 = parseFloat(document.getElementById('error_absoluto4').value);
		error5 = parseFloat(document.getElementById('error_absoluto5').value);

		totalDesviacion = error1+error2+error3+error4+error5;
		totalDesviacion = totalDesviacion.toFixed(2)

		document.getElementById('error_total').value = totalDesviacion;
		document.getElementById('calculoErrores').style.display = "none";


	}
</script>
<script type="text/javascript">
	function digita_desviacion(valor_referencia){

		document.getElementById('expa8').value = valor_referencia;
		document.getElementById('expa7').value = valor_referencia;
		document.getElementById('expa6').value = valor_referencia;
		document.getElementById('expa5').value = valor_referencia;
		document.getElementById('expa4').value = valor_referencia;
		document.getElementById('expa3').value = valor_referencia;
		document.getElementById('expa2').value = valor_referencia;
		document.getElementById('expa1').value = valor_referencia;
		

	}
</script>
<script type="text/javascript">
	function calcularDesviacionMetodo(){
		
		var sumatoria = 0;
		var dato;
		<?php
			if($id_calibracion != 0){
				$consulta7 = "SELECT contador_presion FROM calibracion WHERE id_calibracion = '$id_calibracion'";
				$resultado7 = mysqli_query($con,$consulta7);
				$linea7 = mysqli_fetch_assoc($resultado7);

				$contador_presion = isset($linea7['contador_presion']) ? $linea7['contador_presion'] : NULL;

				$contador_campo = $contador_presion+1;
			}
			
		?>
		
		for(var i=1; i< <?php echo $contador_presion+1; ?>; i++){
			
			dato = parseFloat(document.getElementById('desvia'+i).value);
			sumatoria = sumatoria+dato;
			
		}
		i = i-1;

		var desviacion = sumatoria/i;

		desviacion = desviacion.toFixed(2);
		
		document.getElementById('guardar'+<?php echo $contador_campo; ?>).disabled = true;
		
		document.getElementById('desviacionT').value = desviacion;
	}
</script>
<script type="text/javascript">
	function calcularTemp(){
		var temp_cilindro = document.getElementById('temp_cilindro').value;
		var temp_camisa = document.getElementById('temp_camisa').value;

		var dif = temp_camisa-temp_cilindro;
		if(dif<0){
			dif = dif*-1;
		}

		document.getElementById('diferencia_temp').value = dif;
		document.getElementById('diferencia_temperatura').value = dif;
		document.getElementById('temperatura_camisa').value = temp_camisa;
		document.getElementById('temperatura_cilindro').value = temp_cilindro;
	}
</script>
<script type="text/javascript">
	function calcularTotal() {
		
		var totalDesviacion;
		var Xa = parseFloat(document.getElementById('incertidumbre_manometro').value);
		var Xb = parseFloat(document.getElementById('incertidumbre_bascula').value);
		var Xc = parseFloat(document.getElementById('desviacionT').value);

		var Xa2 = Math.pow(Xa,2);
		var Xb2 = Math.pow(Xb,2);
		var Xc2 = Math.pow(Xc,2);

		var X = Xa2+Xb2+Xc2;
		var totalDesviacion = Math.sqrt(X);
		totalDesviacion = totalDesviacion.toFixed(2);

		document.getElementById('incertidumbre_total').value = totalDesviacion;
		
	}
</script>
<script type="text/javascript">
// DO NOT REMOVE : GLOBAL FUNCTIONS!

$(document).ready(function() {
	
	/* // DOM Position key index //
		
	l - Length changing (dropdown)
	f - Filtering input (search)
	t - The Table! (datatable)
	i - Information (records)
	p - Pagination (paging)
	r - pRocessing 
	< and > - div elements
	<"#id" and > - div with an id
	<"class" and > - div with a class
	<"#id.class" and > - div with an id and class
	
	Also see: http://legacy.datatables.net/usage/features
	*/	

	/* BASIC ;*/
		var responsiveHelper_dt_basic = undefined;
		var responsiveHelper_datatable_fixed_column = undefined;
		var responsiveHelper_datatable_col_reorder = undefined;
		var responsiveHelper_datatable_tabletools = undefined;
		
		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};

		$('#dt_basic').dataTable({
			"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
			"autoWidth" : true,
			"preDrawCallback" : function() {
				// Initialize the responsive datatables helper once.
				if (!responsiveHelper_dt_basic) {
					responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
				}
			},
			"rowCallback" : function(nRow) {
				responsiveHelper_dt_basic.createExpandIcon(nRow);
			},
			"drawCallback" : function(oSettings) {
				responsiveHelper_dt_basic.respond();
			}
		});

	/* END BASIC */
	
	/* COLUMN FILTER  */
    var otable = $('#datatable_fixed_column').DataTable({
    	//"bFilter": false,
    	//"bInfo": false,
    	//"bLengthChange": false
    	//"bAutoWidth": false,
    	//"bPaginate": false,
    	//"bStateSave": true // saves sort state using localStorage
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_fixed_column) {
				responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_fixed_column.respond();
		}		
	
    });
    
    // custom toolbar
    $("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');
    	   
    // Apply the filter
    $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {
    	
        otable
            .column( $(this).parent().index()+':visible' )
            .search( this.value )
            .draw();
            
    } );
    /* END COLUMN FILTER */   

	/* COLUMN SHOW - HIDE */
	$('#datatable_col_reorder').dataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_col_reorder) {
				responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_col_reorder.respond();
		}			
	});
	
	/* END COLUMN SHOW - HIDE */

	/* TABLETOOLS */
	$('#datatable_tabletools').dataTable({
		
		// Tabletools options: 
		//   https://datatables.net/extensions/tabletools/button_options
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        "oTableTools": {
        	 "aButtons": [
             "copy",
             "csv",
             "xls",
                {
                    "sExtends": "pdf",
                    "sTitle": "SmartAdmin_PDF",
                    "sPdfMessage": "SmartAdmin PDF Export",
                    "sPdfSize": "letter"
                },
             	{
                	"sExtends": "print",
                	"sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>"
            	}
             ],
            "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
        },
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_tabletools) {
				responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_tabletools.respond();
		}
	});
	
	/* END TABLETOOLS */

})

</script>
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script type="text/javascript">

 
		
		$(document).ready(function() {
			
			// menu
			$("#menu").menu();
		
			/*
			 * AUTO COMPLETE AJAX
			 */
		
			function log(message) {
				$("<div>").text(message).prependTo("#log");
				$("#log").scrollTop(0);
			}
		
			$("#city").autocomplete({
				source : function(request, response) {
					$.ajax({
						url : "http://ws.geonames.org/searchJSON",
						dataType : "jsonp",
						data : {
							featureClass : "P",
							style : "full",
							maxRows : 12,
							name_startsWith : request.term
						},
						success : function(data) {
							response($.map(data.geonames, function(item) {
								return {
									label : item.name + (item.adminName1 ? ", " + item.adminName1 : "") + ", " + item.countryName,
									value : item.name
								}
							}));
						}
					});
				},
				minLength : 2,
				select : function(event, ui) {
					log(ui.item ? "Selected: " + ui.item.label : "Nothing selected, input was " + this.value);
				}
			});
		
			/*
			 * Spinners
			 */
			$("#spinner").spinner();
			$("#spinner-decimal").spinner({
				step : 0.01,
				numberFormat : "n"
			});
		
			$("#spinner-currency").spinner({
				min : 5,
				max : 2500,
				step : 25,
				start : 1000,
				numberFormat : "C"
			});
		
			/*
			 * CONVERT DIALOG TITLE TO HTML
			 * REF: http://stackoverflow.com/questions/14488774/using-html-in-a-dialogs-title-in-jquery-ui-1-10
			 */
			$.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
				_title : function(title) {
					if (!this.options.title) {
						title.html("&#160;");
					} else {
						title.html(this.options.title);
					}
				}
			}));
		
		
			/*
			* DIALOG SIMPLE
			*/
		
			// Dialog click
			$('#dialog_link').click(function() {
				$('#dialog_simple').dialog('open');
				return false;
		
			});
		
			$('#dialog_simple').dialog({
				autoOpen : false,
				width : 600,
				resizable : false,
				modal : true,
				title : "<div class='widget-header'><h4><i class='fa fa-warning'></i> Empty the recycle bin?</h4></div>",
				buttons : [{
					html : "<i class='fa fa-trash-o'></i>&nbsp; Delete all items",
					"class" : "btn btn-danger",
					click : function() {
						$(this).dialog("close");
					}
				}, {
					html : "<i class='fa fa-times'></i>&nbsp; Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
					}
				}]
			});
		
			/*
			* DIALOG HEADER ICON
			*/
		
			// Modal Link
			$('#modal_link').click(function() {
				$('#dialog-message').dialog('open');
				return false;
			});
		
			$("#dialog-message").dialog({
				autoOpen : false,
				modal : true,
				title : "<div class='widget-header'><h4><i class='icon-ok'></i> jQuery UI Dialog</h4></div>",
				buttons : [{
					html : "Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
					}
				}]
		
			});
		
			/*
			 * Remove focus from buttons
			 */
			$('.ui-dialog :button').blur();
		
			/*
			 * Just Tabs
			 */
		
			$('#tabs').tabs();
		
			/*
			 *  Simple tabs adding and removing
			 */
		
			$('#tabs2').tabs();
		
			// Dynamic tabs
			var tabTitle = $("#tab_title"), tabContent = $("#tab_content"), tabTemplate = "<li style='position:relative;'> <span class='air air-top-left delete-tab' style='top:7px; left:7px;'><button class='btn btn-xs font-xs btn-default hover-transparent'><i class='fa fa-times'></i></button></span></span><a href='#{href}'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; #{label}</a></li>", tabCounter = 2;
		
			var tabs = $("#tabs2").tabs();
		
			// modal dialog init: custom buttons and a "close" callback reseting the form inside
			var dialog = $("#addtab").dialog({
				autoOpen : false,
				width : 600,
				resizable : false,
				modal : true,
				buttons : [{
					html : "<i class='fa fa-times'></i>&nbsp; Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
		
					}
				}]
			});
		
			// addTab form: calls addTab function on submit and closes the dialog
			var form = dialog.find("form").submit(function(event) {
				addTab();
				dialog.dialog("close");
				
			});
		
			// actual addTab function: adds new tab using the input from the form above

			
			function addTab() 
			{
				
				var label = tabTitle.val() || "Tab " + tabCounter, id = "tabs-" + tabCounter, li = $(tabTemplate.replace(/#\{href\}/g, "#" + id).replace(/#\{label\}/g, label)), tabContentHtml = tabContent.val() || "Tab " + tabCounter + " content.";
		
				tabs.find(".ui-tabs-nav").append(li);
				tabs.append("<div id='" + id + "'><p>" + tabContentHtml + "</p></div>");
				tabs.tabs("refresh");
				tabCounter++;
		
				// clear fields
				$("#tab_title").val("");
				$("#tab_content").val("");
			}
		
			// addTab button: just opens the dialog
			$("#add_tab").button().click(function() {
				var idd = document.getElementById('num').firstChild.nodeValue;

				
				dialog.dialog("open");
			});
		
			// close icon: removing the tab on click
					
			/*
			* ACCORDION
			*/
			//jquery accordion
			
		     var accordionIcons = {
		         header: "fa fa-plus",    // custom icon class
		         activeHeader: "fa fa-minus" // custom icon class
		     };
		     
			$("#accordion").accordion({
				autoHeight : false,
				heightStyle : "content",
				collapsible : true,
				animate : 300,
				icons: accordionIcons,
				header : "h4",
			})
		
			/*
			 * PROGRESS BAR
			 */
			$("#progressbar").progressbar({
		     	value: 25,
		     	create: function( event, ui ) {
		     		$(this).removeClass("ui-corner-all").addClass('progress').find(">:first-child").removeClass("ui-corner-left").addClass('progress-bar progress-bar-success');
				}
			});			

		})

	</script>
	


<?php 
	//include footer
	include("inc/google-analytics.php"); 
}
else
{
    header("Location:index.php");
}
?>