<?php
date_default_timezone_set("America/Bogota");
require ("libraries/conexion.php");
session_start();

if(@$_SESSION['logged']== 'yes')
{ 
    $acc = $_SESSION['acc'];
    $letra = isset($_REQUEST['letra']) ? $_REQUEST['letra'] : NULL;

require_once("inc/init.php");
require_once("inc/config.ui.php");
$page_title = "Menú Principal";
$page_css[] = "your_style.css";
include("inc/header.php");
$page_nav[""][""][""][""] = true;
include("inc/nav.php");

?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">

	<style type="text/css">
	  h2 {display:inline}
	</style>
	<!-- MAIN CONTENT -->
	<div id="content">	
		<div class="row">
			<div class="" align="center">
				<h1  class="page-title txt-color-blueDark"> <?php echo $page_title; ?>: Servicio ETO - PEV</h1>
			</div>	      	
		</div>		
		<div>
			<div>
				<?php
				if (in_array(39, $acc))
				{
				?>
				<a href="busqueda_clientes.php"><img src="img/iconos/clientes.png" alt="Clientes" width="200" height="200"></a>	
				<?php
				}
				if (in_array(11, $acc))
				{
				?>
				<img src="img/iconos/eto.png" alt="eto" width="200" height="200" id="eto">
				<?php
				}
				if (in_array(42, $acc))
				{
				?>
				<img src="img/iconos/lote_insumo.png" alt="lote_insumo" width="200" height="200" id="insumo">
				<?php
				}
				if (in_array(1, $acc))
				{
				?>
				<img src="img/iconos/configuracion.png" alt="eto" width="200" height="200" id="confi">
				<?php
				}
				if (in_array(46, $acc))
				{
				?>
				<img src="img/iconos/pev.png" alt="PEV" width="200" height="200" id="pev">
				<?php
				}
				?>									
			</div>
		</div>		
	</div>
	<!-- END MAIN CONTENT -->
</div>
<div class="modal fade" id="modal_eto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">	
	<div class="modal-dialog">
		<div class="modal-content">			
			<div class="modal-body">
				<?php				
				if (in_array(12, $acc))
				{
				?>
				<a href="busqueda_cilindros_eto.php"><img src="img/iconos/cilindros_eto.png" alt="Cilindros_ETO" width="180" height="180"></a>
				
				<?php
				}				
				if (in_array(15, $acc))
				{
				?>
				<a href="busqueda_ordenes.php"><img src="img/iconos/ordenes_produccion.png" alt="Ordenes" width="180" height="180"></a>
				
				<?php
				}				
				if (in_array(18, $acc))
				{
				?>
				<a href="revision_preliminar.php"><img src="img/iconos/revision_preliminar.png" alt="Revision_Preliminar" width="180" height="180"></a>
				<?php
				}				
				if (in_array(43, $acc))
				{
				?>
				<a href="busqueda_mezclas.php"><img src="img/iconos/produccion_mezclas.png" alt="Mezclas" width="180" height="180"></a>
				<?php
				}				
				if (in_array(23, $acc))
				{
				?>
				<a href="busqueda_calidad.php"><img src="img/iconos/calidad.png" alt="Calidad" width="180" height="180"></a>
				<?php				
				}
				if (in_array(25, $acc))
				{
				?>
				<a href="busqueda_logistica.php"><img src="img/iconos/logistica.png" alt="Logística" width="180" height="180"></a>				
				<?php
				}				
				if (in_array(28, $acc))
				{
				?>
				<a href="busqueda_transporte_eto.php"><img src="img/iconos/transporte.png" alt="Transporte" width="180" height="180"></a>	
				<?php
				}
				?>			
			</div>
		</div>	
	</div>	
</div><!-- /.modal -->
<div class="modal fade" id="modal_confi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">	
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<a href="parametros.php"><img src="img/iconos/parametros.png" alt="Parametros" width="180" height="180"></a>
				<a href="busqueda_perfil.php"><img src="img/iconos/perfil.png" alt="Perfil" width="180" height="180"></a>
				<a href="busqueda_usuario.php"><img src="img/iconos/usuario.png" alt="Usuario" width="180" height="180"></a>								
			</div>
		</div>	
	</div>	
</div><!-- /.modal -->
<div class="modal fade" id="modal_insumo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">	
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<div aling="center">
					<a href="busqueda_eto.php"><img src="img/iconos/insumo_eto.png" alt="Insumo_ETO" width="180" height="180"></a>
					<a href="busqueda_co2.php"><img src="img/iconos/insumo_co2.png" alt="Insumo_CO2" width="180" height="180"></a>		
				</div>							
			</div>
		</div>	
	</div>	
</div><!-- /.modal --> 
<! --Modal PEV -->
<div class="modal fade" id="modal_pev" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">	
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<div aling="center">
					<img src="img/iconos/transporte.png" alt="Transporte_PEV" width="200" height="200" id="transporte_pev">
					<a href="busqueda_ordenes_pev.php"><img src="img/iconos/asignacion_servicios.png" alt="Insumo_CO2" width="180" height="180"></a>	
					<a href="busqueda_ordenes_pev.php"><img src="img/iconos/inspeccion_visual.png" alt="Insumo_CO2" width="180" height="180"></a>		
				</div>							
			</div>
		</div>	
	</div>	
</div><!-- /.modal -->
<! --Modal Transporte PEV -->
<div class="modal fade" id="modal_transporte_pev" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">	
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<div aling="center">
					<a href="transporte_pev.php"><img src="img/iconos/crear_transporte_pev.png" alt="Crear_Transporte" width="200" height="200"></a>
					<a href="guia_transporte_pev.php"><img src="img/iconos/guia_transporte_pev.png" alt="Guia_Transporte" width="180" height="180"></a>
				</div>							
			</div>
		</div>	
	</div>	
</div><!-- /.modal -->
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->
<!-- PAGE FOOTER -->


<script type="text/javascript">
$(document).ready(function(){
    $("#eto").click(function(){
        //alert("The paragraph was clicked.");
        $("#modal_eto").modal();
    });
});
</script>

<script type="text/javascript">
$(document).ready(function(){
    $("#info").click(function(){
        alert("No disponible");
        //$("#modal_confi").modal();
    });
});
</script>

<script type="text/javascript">
$(document).ready(function(){
    $("#insumo").click(function(){
        //alert("No disponible");
        $("#modal_insumo").modal();
    });
});
</script>

<script type="text/javascript">
$(document).ready(function(){
    $("#confi").click(function(){
        //alert("The paragraph was clicked.");
        $("#modal_confi").modal();
    });
});
</script>
<!-- Se lanza modal PEV -->
<script type="text/javascript">
$(document).ready(function(){
    $("#pev").click(function(){
        //alert("The paragraph was clicked.");
        $("#modal_pev").modal();
    });
});
</script>
<!-- Se lanza modal Transporte PEV -->
<script type="text/javascript">
$(document).ready(function(){
    $("#transporte_pev").click(function(){
        //alert("The paragraph was clicked.");
        $('#modal_pev').modal('hide');
        $("#modal_transporte_pev").modal();
    });
});
</script>

<?php
	include("inc/footer.php");
	include("inc/scripts.php"); 
?>
<script src="/js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="/js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="/js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="/js/plugin/flot/jquery.flot.tooltip.min.js"></script>
<script src="/js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="/js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="/js/plugin/moment/moment.min.js"></script>
<script src="/js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<?php 

	include("inc/google-analytics.php"); 
}
else
{
    header("Location:index.php");
}
?>