<?php
session_start();

date_default_timezone_set('America/Bogota');
$fecha_actual = date("H:i:s (Y-m-d)");



if($_SESSION['logged']== 'yes')
{
	require ("libraries/conexion.php"); 
	$id_user = $_SESSION['su'];
    $acc = $_SESSION['acc'];

	require_once("inc/init.php");
	require_once("inc/config.ui.php");
	$page_title = "Búsqueda Hojas-Ruta";
	$page_css[] = "your_style.css";
	include("inc/header.php");
	$page_nav["rutero"]["sub"]["hoja_ruta"]["active"] = true;
	include("inc/nav.php");	
?>
<!-- MAIN PANEL -->
<div id="main" role="main">
<?php
if (in_array(57, $acc))
{
?>
	<div id="content">	
		<div class="" align="center">
			<h1  class="page-title txt-color-blueDark"> <?php echo $page_title; ?>
				<?php
				if (in_array(58, $acc))
				{
					$consulta_prev = "SELECT User_idUser_conductor, estado FROM hoja_ruta WHERE User_idUser_conductor = $id_user AND estado <> 3 AND estado <> 2";
					$resulado_prev = mysqli_query($con,$consulta_prev);
					if(mysqli_num_rows($resulado_prev) <= 0){

					?>	
					<div class="" align="left">
						<a class="btn btn-success" href="hoja_ruta.php?idHoja_ruta=0">CREAR HOJA DE RUTA</a>
					</div>
					<?php
					}
				}										
				?>
			</h1>			
		</div>		
		<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">		
			<header><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
				<span class="widget-icon"> <i class="fa fa-table"></i> </span>
				<h2>HOJAS DE RUTA REGISTRADAS</h2>				
			</header>
			<div>
				<div class="jarviswidget-editbox"></div>
				<div class="widget-body no-padding">
					<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
						<form action="busqueda_cilindros_eto.php" method="POST" name="form1">
							<thead>
								<tr>
									<th>#</th>
									<th >EDITAR</th>
									<?php
										if(in_array(57, $acc))
										{
											?>
											<th>EXCEL</th>
											<?php
										}
									?>
									<?php
										if(in_array(57, $acc))
										{
											?>
											<th>PDF</th>
											<?php
										}
									?>
									<th>FECHA</th>
									<th>PLACA</th>
									<th>CONDUCTOR</th>
									<th>HORA SALIDA</th>
									<th>HORA LLEGADA</th>
									<th>ODÓMETRO<br>SALIDA</th>
									<th>ODÓMETRO<br>LLEGADA</th>
									<th>RECORRIDO</th>
									<th>ESTADO</th>
								</tr>
							</thead>
							<tbody>
								<?php
                                    
	                    			$contador = "0";
	                    			if(in_array(84, $acc)){
	                    				$consulta_1 = "SELECT h.idHoja_ruta, h.fecha, h.salida, h.llegada, h.odometro_salida, h.odometro_llegada, h.recorrido, h.estado, u.Name, u.LastName, p.placa_camion FROM hoja_ruta h INNER JOIN user u ON h.User_idUser_conductor = u.idUser INNER JOIN placa_camion p ON h.placa = p.id_placa_camion ORDER BY h.idHoja_ruta DESC LIMIT 1000";

	                    			}else{
	                    				$consulta_1 = "SELECT h.idHoja_ruta, h.fecha, h.salida, h.llegada, h.odometro_salida, h.odometro_llegada, h.recorrido, h.estado, u.Name, u.LastName, p.placa_camion FROM hoja_ruta h INNER JOIN user u ON h.User_idUser_conductor = u.idUser INNER JOIN placa_camion p ON h.placa = p.id_placa_camion WHERE h.User_idUser_conductor = $id_user ORDER BY h.idHoja_ruta DESC LIMIT 1000";
	                    				//$consulta_1 = "SELECT * FROM hoja_ruta WHERE User_idUser_conductor = $id_user ORDER BY idHoja_ruta DESC";

	                    			}
	                    	        $resultado_1 = mysqli_query($con,$consulta_1);
	                                if(mysqli_num_rows($resultado_1) > 0)
	                                {
	                                	while ($linea_1 = mysqli_fetch_assoc($resultado_1))
	                                	{
	                                		$contador++;
	                                		$idHoja_ruta = $linea_1['idHoja_ruta'];
	                                		$fecha = $linea_1['fecha'];
	                                		$placa_camion = $linea_1['placa_camion'];
	                                		$salida = $linea_1['salida'];
	                                		$llegada = $linea_1['llegada'];
	                                		$odometro_salida = $linea_1['odometro_salida'];
	                                		$odometro_llegada = $linea_1['odometro_llegada'];
	                                		$recorrido = $linea_1['recorrido'];
	                                		$estado = $linea_1['estado'];

	                                		if($estado == 1)
	                                		{
	                                			$estado = "Asignando recorridos";
	                                		}
	                                		elseif ($estado == 2)
	                                		{
	                                			$estado = "En Revisión";
	                                		}
	                                		elseif ($estado == 3)
	                                		{
	                                			$estado = "Finalizada";
	                                		}

	                                		$conductor = $linea_1['Name']." ".$linea_1['LastName'];
	                                		?>
	                                		<tr>
		                                        <td width="5"><?php echo $contador; ?></td>
												<td width="5" align="center">
					                              	<a href="hoja_ruta.php?idHoja_ruta=<?php echo $idHoja_ruta; ?>"><img src="img/edit.png" width="30" height="30"></a>
					                            </td>
														
												<?php
													if(in_array(57, $acc))
													{
														?>
														<td width="5" align="center">
				                                        	<a href="descargar_excel/hoja_ruta/descargar_hoja_ruta.php?idHoja_ruta=<?php echo $idHoja_ruta; ?>"><img src="img/excel_icon.png" width="30" height="30"></a>
				                                        </td>
														<?php
													}
												?>	
												<?php
													if(in_array(57, $acc))
													{
														?>
														<td width="5" align="center">
				                                        	<a href="pdf_hoja_ruta.php?idHoja_ruta=<?php echo $idHoja_ruta; ?>"><img src="img/pdf_icon.png" width="30" height="30"></a>
				                                        </td>
														<?php
													}
												?>			                                        
		                                        <td><?php echo $fecha; ?></td>
		                                        <td><?php echo $placa_camion; ?></td>
		                                        <td><?php echo $conductor; ?></td>
		                                        <td><?php echo $salida; ?></td>
		                                        <td><?php echo $llegada; ?></td>
		                                        <td><?php echo $odometro_salida; ?></td>	
		                                        <td><?php echo $odometro_llegada; ?></td>	
		                                        <td><?php echo $recorrido; ?></td>	                                                        
		                                        <td><?php echo $estado; ?></td>
		                                	</tr> 
		                                	<?php
	                                	}
	                                	mysqli_free_result($resultado_1);
	                                }
	                            ?>                                      
							</tbody>
						</form>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- END MAIN CONTENT -->
<?php
}										
?>

</div>
<!-- ==========================CONTENT ENDS HERE ========================== -->
<?php
	include("inc/footer.php");
	include("inc/scripts.php"); 
?>

<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<script type="text/javascript">

// DO NOT REMOVE : GLOBAL FUNCTIONS!

$(document).ready(function() {
	
	/* // DOM Position key index //
		
	l - Length changing (dropdown)
	f - Filtering input (search)
	t - The Table! (datatable)
	i - Information (records)
	p - Pagination (paging)
	r - pRocessing 
	< and > - div elements
	<"#id" and > - div with an id
	<"class" and > - div with a class
	<"#id.class" and > - div with an id and class
	
	Also see: http://legacy.datatables.net/usage/features
	*/	

	/* BASIC ;*/
		var responsiveHelper_dt_basic = undefined;
		var responsiveHelper_datatable_fixed_column = undefined;
		var responsiveHelper_datatable_col_reorder = undefined;
		var responsiveHelper_datatable_tabletools = undefined;
		
		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};

		$('#dt_basic').dataTable({
			"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
			"autoWidth" : true,
			"preDrawCallback" : function() {
				// Initialize the responsive datatables helper once.
				if (!responsiveHelper_dt_basic) {
					responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
				}
			},
			"rowCallback" : function(nRow) {
				responsiveHelper_dt_basic.createExpandIcon(nRow);
			},
			"drawCallback" : function(oSettings) {
				responsiveHelper_dt_basic.respond();
			}
		});

	/* END BASIC */
	
	/* COLUMN FILTER  */
    var otable = $('#datatable_fixed_column').DataTable({
    	//"bFilter": false,
    	//"bInfo": false,
    	//"bLengthChange": false
    	//"bAutoWidth": false,
    	//"bPaginate": false,
    	//"bStateSave": true // saves sort state using localStorage
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_fixed_column) {
				responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_fixed_column.respond();
		}		
	
    });
    
    // custom toolbar
    $("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');
    	   
    // Apply the filter
    $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {
    	
        otable
            .column( $(this).parent().index()+':visible' )
            .search( this.value )
            .draw();
            
    } );
    /* END COLUMN FILTER */   	

})
</script>

<?php 

	include("inc/google-analytics.php"); 

}
else
{
    header("Location:index.php");
}
?>