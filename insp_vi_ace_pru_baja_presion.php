<?php
session_start();
date_default_timezone_set("America/Bogota");
$Fecha = date("Y-m-d");
require ("libraries/conexion.php");

$motivo_condena = "NINGUNO";
$estado_prueba_i = "APROBADO";
$prueba_realizada = 0;
if($_SESSION['logged']== 'yes')
{ 
	$id_has_movimiento_cilindro_pev = isset($_REQUEST['id_has_movimiento_cilindro_pev']) ? $_REQUEST['id_has_movimiento_cilindro_pev'] : NULL;
	$idUser =$_SESSION['su'];
	$acc = $_SESSION['acc'];

	require_once("inc/init.php");
	require_once("inc/config.ui.php");
	$page_title = "Inspección Visual Acero";
	$page_css[] = "your_style.css";
	include("inc/header.php");
	$page_nav[""][""][""][""] = true;
	include("inc/nav.php");

	$consulta0 = "SELECT * FROM has_movimiento_cilindro_pev WHERE id_has_movimiento_cilindro_pev = '$id_has_movimiento_cilindro_pev'";
	$resultado0 = mysqli_query($con,$consulta0);

	$linea0 = mysqli_fetch_assoc($resultado0);
	$num_cili = $linea0["num_cili"];

	$nva_consul = "SELECT * FROM cilindro_pev WHERE num_cili_pev = '$num_cili'";
	$resul_consul = mysqli_query($con,$nva_consul);
	$linea_consul = mysqli_fetch_assoc($resul_consul);

	$id_cilindro = $linea_consul["id_cilindro_pev"];



	if (isset($_POST['g_insp_visual_acero'])) //guardar información de inspección visual de acero
	{

		$estado_prueba = $_POST['estado_prueba'];
		

		if($estado_prueba == "CONDENADO"){
			$consulta_u = "UPDATE cilindro_pev SET id_estado = 15 WHERE id_cilindro_pev = $id_cilindro";
			
		}else if($estado_prueba == "RECHAZADO"){
			$consulta_u = "UPDATE cilindro_pev SET id_estado = 14 WHERE id_cilindro_pev = $id_cilindro";
			
		}else if($estado_prueba == "APROBADO"){
			$consulta_u = "UPDATE cilindro_pev SET id_estado = 5 WHERE id_cilindro_pev = $id_cilindro";
			
		}
		echo $consulta_u;

		$resultado_u = mysqli_query($con,$consulta_u);

		$id_has_movimiento_cilindro_pev = $_POST['id_has_movimiento_cilindro_pev'];
		$estado_prueba = $_POST['estado_prueba'];
		$protuberancia = $_POST['protuberancia_h'];
		$abolladuras = $_POST['abolladuras_h'];
		$abombamiento = $_POST['abombamiento_h'];
		$indentacion = $_POST['indentacion_h'];
		$grieta = $_POST['grieta_h'];
		$arco_antorcha = $_POST['arco_antorcha_h'];
		$dano_fuego = $_POST['dano_fuego_h'];
		$tapon_cuello = $_POST['tapon_cuello_h'];
		$defecto_cuello = $_POST['defecto_cuello_h'];
		$estampacion = $_POST['estampacion_h'];
		$marcas_sospechosas = $_POST['marcas_sospechosas_h'];
		$estabilidad_vertical = $_POST['estabilidad_h'];
		$corrosion = $_POST['corrosion_h'];
		$corrosion_general = $_POST['corrosion_general_h'];
		$cort_ranu_perfo = $_POST['cort_ranu_perfo_h'];
		$picaduras_aisladas = $_POST['picaduras_aisladas_h'];
		$motivo_condena = $_POST['motivo_condena'];
		$observaciones = $_POST["observaciones"];


		$consulta1 = "INSERT INTO insp_vi_ace_baja_pev (
							id_has_movimiento_cilindro_pev , 
							num_cili_pev ,
							estado_prueba ,
							protuberancia ,
							abolladuras ,
							abombamiento ,
							indentacion ,
							grieta ,
							arco_antorcha ,
							fuego ,
							tapon_cuello ,
							defecto_cuello , 
							estampacion ,
							marcas ,
							estabilidad_vertical ,
							corrosion ,
							corrosion_general ,
							cort_ranu_perfo ,
							picaduras_aisladas ,
							motivo_condena,
							observaciones
							) VALUES ($id_has_movimiento_cilindro_pev, 
									'$num_cili',
									'$estado_prueba', 
									$protuberancia, 
									$abolladuras, 
									$abombamiento, 
									$indentacion, 
									$grieta, 
									$arco_antorcha, 
									$dano_fuego, 
									$tapon_cuello, 
									$defecto_cuello, 
									$estampacion, 
									$marcas_sospechosas, 
									$estabilidad_vertical, 
									$corrosion, 
									$corrosion_general,
									$cort_ranu_perfo,  
									$picaduras_aisladas,
									'$motivo_condena',
									'$observaciones')";

		if(mysqli_query($con,$consulta1))
			{
				?>
					<script type="text/javascript">
						var id_orden_pev = '<?php echo $id_orden_pev; ?>';
						alert("Inspección guardada correctamente.")
						window.location = 'insp_vi_ace_pru_baja_presion.php?id_has_movimiento_cilindro_pev='+<?php echo $id_has_movimiento_cilindro_pev; ?>;
					</script>
				<?php
			}
			else
			{
				echo "Error: " . $consulta1 . "<br>" . mysqli_error($con);
			}
	}

	 //Consulta el responsable de la inspección
	$consulta = "SELECT Name, LastName FROM user WHERE idUser = '".$idUser."'";
	$resultado = mysqli_query($con,$consulta);
	if(mysqli_num_rows($resultado) > 0 )
	{
	  	$linea = mysqli_fetch_assoc($resultado);
		$Name = $linea['Name'];
		$LastName = $linea['LastName'];
		$responsable = $Name." ".$LastName;
	}
	 //Si existe el cilindro se consultan datos
	if(strlen($id_has_movimiento_cilindro_pev) > 0)
	{
		$consulta3 = "SELECT num_cili, id_tipo_gas_pev, id_transporte_pev
						FROM has_movimiento_cilindro_pev 
						WHERE id_has_movimiento_cilindro_pev = '".$id_has_movimiento_cilindro_pev."'";
		$resultado3 = mysqli_query($con,$consulta3);
		if(mysqli_num_rows($resultado3) > 0)
		{
			$linea3 = mysqli_fetch_assoc($resultado3);
			$num_cili = $linea3['num_cili'];
			$id_tipo_gas_pev = $linea3['id_tipo_gas_pev'];
			$id_transporte_pev = $linea3['id_transporte_pev'];

			$consulta10 = "SELECT esp_fab_pev, esp_actu_pev FROM cilindro_pev WHERE num_cili_pev = '".$num_cili."'";
			$resultado10 = mysqli_query($con,$consulta10);
			if(mysqli_num_rows($resultado10) > 0)
			{
				$linea10 = mysqli_fetch_assoc($resultado10);
				$esp_fab_pev = isset($linea10["esp_fab_pev"]) ? $linea10["esp_fab_pev"] : NULL;
				$esp_actu_pev = isset($linea10['esp_actu_pev']) ? $linea10["esp_fab_pev"] : NULL;

				if($esp_fab_pev == NULL){
					$esp_fab_pev = "DESCONOCIDO";
				}
				if($esp_actu_pev == NULL){
					$esp_actu_pev = "DESCONOCIDO";
				}
			}
			$consulta4 = "SELECT id_cliente,fecha FROM transporte_pev WHERE id_transporte_pev = '".$id_transporte_pev."'";
			$resultado4 = mysqli_query($con,$consulta4);
			if(mysqli_num_rows($resultado4) > 0)
			{
				$linea4 = mysqli_fetch_assoc($resultado4);
				$fecha = $linea4['fecha'];
				$id_cliente = $linea4['id_cliente'];

				$consulta8 = "SELECT nombre FROM clientes WHERE id_cliente = '".$id_cliente."'";
				$resultado8 = mysqli_query($con,$consulta8);
				if(mysqli_num_rows($resultado8) > 0)
				{
					$linea8 = mysqli_fetch_assoc($resultado8);
					$nombre_cliente = $linea8['nombre'];
				}
			}
			$consulta5 = "SELECT nombre FROM tipo_gas_pev WHERE id_tipo_gas_pev = '".$id_tipo_gas_pev."'";
			$resultado5 = mysqli_query($con,$consulta5);
			if(mysqli_num_rows($resultado5) > 0)
			{
				$linea5 = mysqli_fetch_assoc($resultado5);
				$nombre = $linea5['nombre'];
			}            
				
			$consulta6 = "SELECT id_especi_pev, especi_pev, num_cili_pev_2 FROM cilindro_pev WHERE num_cili_pev = '".$num_cili."'";
			$resultado6 = mysqli_query($con,$consulta6);
			if(mysqli_num_rows($resultado6) > 0)
			{
				$linea6 = mysqli_fetch_assoc($resultado6);
				$id_especi_pev = $linea6['id_especi_pev'];
				$especi_pev = $linea6['especi_pev'];
				$num_cili_pev_2 = $linea6['num_cili_pev_2'];

				$consulta7 = "SELECT especificacion FROM especificacion WHERE id_especificacion = '".$id_especi_pev."'";
				$resultado7 = mysqli_query($con,$consulta7);
				if(mysqli_num_rows($resultado7))
				{
					$linea7 = mysqli_fetch_assoc($resultado7);
					$especificacion = $linea7['especificacion'];
				}
			}
			$consulta11 = "SELECT * FROM insp_vi_ace_baja_pev WHERE id_has_movimiento_cilindro_pev = '".$id_has_movimiento_cilindro_pev."'";
			$resultado11 = mysqli_query($con,$consulta11);
			if(mysqli_num_rows($resultado11) > 0)
			{
				$prueba_realizada = 1;
				while($linea11 = mysqli_fetch_assoc($resultado11)){
					$id_has_movimiento_cilindro_pev_i = $linea11['id_has_movimiento_cilindro_pev'];
					$estado_prueba_i = $linea11['estado_prueba'];
					$protuberancia_i = $linea11['protuberancia'];
					$abolladuras_i = $linea11['abolladuras'];
					$abombamiento_i = $linea11['abombamiento'];
					$indentacion_i = $linea11['indentacion'];
					$corte_estria_i = $linea11['corte_estria'];
					$grieta_i = $linea11['grieta'];
					$arco_antorcha_i = $linea11['arco_antorcha'];
					$dano_fuego_i = $linea11['fuego'];
					$tapon_cuello_i = $linea11['tapon_cuello'];
					$defecto_cuello_i = $linea11['defecto_cuello'];
					$estampacion_i = $linea11['estampacion'];
					$marcas_sospechosas_i = $linea11['marcas'];
					$estabilidad_vertical_i = $linea11['estabilidad_vertical'];
					$corrosion_general_i = $linea11['corrosion_general'];
					$corrosion_i = $linea11['corrosion'];
					$cort_ranu_perfo_i = $linea11['cort_ranu_perfo'];
					$picaduras_aisladas_i = $linea11['picaduras_aisladas'];
					$picaduras_multiples_i = $linea11['picaduras_multiples'];
					$corr_hendidura_i = $linea11['corrosion_hendidura'];
					$motivo_condena_i = $linea11['motivo_condena'];
					$observaciones = $linea11["observaciones"];

					if($estado_prueba_i == "CONDENADO"){

						$prueba_realizada = 1;
						break;
					}else if($estado_prueba_i == "RECHAZADO"){
						$prueba_realizada = 0;
					}
				}
				
			}
		}

	}
?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
	 <div id="content">
		  <section id="widget-grid" class="">
				<div class="row">
					 <article class="col-md-6 col-md-offset-3">
						  <div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
								<header>
									 <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
									 <h2>INSPECCIÓN VISUAL CILINDRO DE ACERO</h2>
								</header>
								<div>
									 <div class="jarviswidget-editbox">
									 </div>
									 <div class="widget-body no-padding">                                
										  <form name="mezcla" action="insp_vi_ace_pru_baja_presion.php" method="POST" id="mezcla">
												<input type="hidden" name="id_has_movimiento_cilindro_pev" value="<?php echo $id_has_movimiento_cilindro_pev; ?>">
												<input type="hidden" name="motivo_condena" id="motivo_condena" value="<?php echo $motivo_condena; ?>">
												<div class="well well-sm well-primary">
													 <fieldset>
													 <legend><center>IDENTIFICACIÓN DEL CILINDRO</center></legend>
														  <div class="row">
																<div class="col-md-3">
																	 <div class="form-group">
																		  <label for="category">N° Cilindro<br>Ingreso:</label>
																		  <input type="text" class="form-control" placeholder="Numero Cilindro" name="num_cili_al" readonly required value="<?php echo $num_cili; ?>" />
																	 </div>
																</div>
																<div class="col-md-3">
																	 <div class="form-group">
																		  <label for="category">N° Cilindro<br>Verificado:</label>
																		  <input type="text" class="form-control" placeholder="Numero Cilindro" name="num_cili_al_2" readonly required value="<?php echo isset($num_cili_pev_2) ? $num_cili_pev_2 : NULL; ?>"/>
																	 </div>
																</div>
																<div class="col-md-3">
																	 <div class="form-group">
																		  <label for="category">Fecha de<br> Ingreso:</label>
																		  <input type="text" class="form-control" placeholder="Fecha Ingreso" name="fecha_ingreso_al" readonly value="<?php echo isset($fecha) ? $fecha : NULL; ?>"/>                                                    </div>
																</div>
																<div class="col-md-3">
																	 <div class="form-group">
																		  <label for="category">Fecha de<br> Inspección:</label>
																		  <input type="text" class="form-control" placeholder="Fecha Inspección" name="fecha_inspeccion_al" readonly required value="<?php echo $Fecha ?>"/>
																	 </div>
																</div>
														  </div>
														  <div class="row">
																<div class="col-md-6">
																	 <div class="form-group">
																		  <label for="category">GAS:</label>
																		  <input type="text" class="form-control" placeholder="Tipo de GAS" name="gas_al" readonly required value="<?php echo isset($nombre) ? $nombre : NULL; ?>"/>
																	 </div>
																</div>
																<div class="col-md-2">
																	 <div class="form-group">
																		  <label for="category">Especificación:</label>
																		  <input type="text" class="form-control" placeholder="Especificación" name="especificacion_al" readonly value="<?php echo isset($especificacion) ? $especificacion : NULL; ?>"/>
																	 </div>
																</div>
																<div class="col-md-4">
																	 <div class="form-group">
																		<label for="category">Descripción:</label>
																		  <input type="text" class="form-control" placeholder="Especificación" name="especi_pev2" readonly value="<?php echo isset($especi_pev) ? $especi_pev : NULL; ?>"/>
																	 </div>
																</div>
														  </div>
														  <div class="row">
																<div class="col-md-6">
																	 <div class="form-group">
																		<label for="category">Aprobación / Rechazo:</label>
																		  <input type="text" class="form-control" placeholder="ESTADO" name="estado_prueba" id="estado_prueba" readonly value="<?php echo isset($estado_prueba_i) ? $estado_prueba_i : NULL; ?>" />
																	 </div>
																</div>
																<?php
																	if($prueba_realizada == 1){

																		if($estado_prueba_i != "APROBADO"){
																?>
																<div class="col-md-6">
																	 <div class="form-group">
																		  <label for="category">Causa de Condena:</label>
																		  <input type="text" class="form-control" placeholder="Cliente" name="nombre_cliente" readonly value="<?php echo isset($motivo_condena_i) ? $motivo_condena_i : NULL; ?>"/>
																	 </div>
																</div>
																<?php
																	}
																}
																?>
														  </div>
														  <div class="row">
																<div class="col-md-6">
																	 <div class="form-group">
																		<label for="category">Responsable:</label>
																		  <input type="text" class="form-control" placeholder="Responsable" name="responsable" readonly value="<?php echo isset($responsable) ? $responsable : NULL; ?>"/>
																	 </div>
																</div>
																<div class="col-md-6">
																	 <div class="form-group">
																		  <label for="category">Cliente:</label>
																		  <input type="text" class="form-control" placeholder="Cliente" name="nombre_cliente" readonly value="<?php echo isset($nombre_cliente) ? $nombre_cliente : NULL; ?>"/>
																	 </div>
																</div>
														  </div>
														  <?php
														  	if($estado_prueba_i == "CONDENADO"){
														  ?>
														  <div class="row">
																<div class="col-md-6">
																	 <div class="form-group">
																		<label for="category">Certificado Condena:</label>
																		<?php
																		$link = "<a href='pdf_certificado_pev_insp_visual.php?id_has_movimiento_cilindro_pev=".$id_has_movimiento_cilindro_pev."'>Certificado</a>";

																		echo $link;
																		?>
																	 </div>
																</div>
														  </div>
														  <?php
															}
														  ?>
													 </fieldset>
												</div>  <!-- -----------------------DATOS BASICOS------------------------------- -->
												<div class="well well-sm well-primary">
													<legend><center>INSPECCIÓN VISUAL </center></legend>
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Protuberancia: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('protuberancia')"></label>
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="protuberancia" id="protuberancia1" onclick="checkProtuberancia('1');" value="1" <?php if($protuberancia_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="protuberancia" id="protuberancia2" onclick="checkProtuberancia('0');" value="2" <?php if($protuberancia_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="protuberancia_h" id="protuberancia_h">
																	</label>
																</div>
															</div>  
														</div>															
													</div><!-- -----------------------PROTUBERANCIA------------------------------- -->
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Abolladuras: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('abolladuras')"></label>
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="abolladuras" id="abolladuras1" onclick="checkAbolladura('1')" value="1" <?php if($abolladuras_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="abolladuras" id="abolladuras2" onclick="checkAbolladura('0')" value="2" <?php if($abolladuras_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="abolladuras_h" id="abolladuras_h">
																	</label>
																</div>
															</div>  
														</div>
														<div class="row" id="info_abolladura" style="display: none">
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="category"><strong>Información de la Abolladura</strong></label>
																	<label class="input">Profundidad Maxima de la Picadura:
																		<input type="text" name="profundidad_abolladura" id="profundidad_abolladura" size="9" onchange="validar_profundidad_abolladura(this.value)">
																	</label>
																</div>
															</div>
														</div>
													</div><!-- -----------------------ABOLLADURA------------------------------- -->
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Abombamiento: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('abombamiento')"></label>
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="abombamiento" id="abombamiento1" onclick="checkAbombamiento('1');" value="1" <?php if($abombamiento_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="abombamiento" id="abombamiento2" onclick="checkAbombamiento('0');" value="2" <?php if($abombamiento_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="abombamiento_h" id="abombamiento_h">
																	</label>
																</div>
															</div> 
															<div class="row" id="info_ambombamiento" style="display: none">
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="category"><strong>Información del Abombamiento</strong></label>
																	<label class="input">Diametro Normal del Cilindro:
																		<input type="text" name="diam_norm_cilindro" id="diam_norm_cilindro" size="9">
																	</label>
																	<label class="input">Circunferencia del Cilindro:
																		<input type="text" name="circu_cilindro" id="circu_cilindro" onchange="validarAbombamiento(this.value)" size="10">
																	</label>
																</div>
															</div>
														</div> 
														</div>
													</div><!-- -----------------------ABOMBAMIENTO------------------------------- -->
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Indentacion: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('indentacion')">
																</label>                            
															
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="indentacion" id="indentacion1" onclick="checkIndentacion('1');" value="1" <?php if($indentacion_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="indentacion" id="indentacion2" onclick="checkIndentacion('0');" value="2" <?php if($indentacion_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="indentacion_h" id="indentacion_h">
																	</label>
																</div>
															</div>  
														</div>
														<div class="row" id="info_indentacion" style="display: none">
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="category"><strong>Información de la Identacion</strong></label>
																	<label class="input"> Diametro Externo del Cilindro:
																		<input type="text" name="diam_ext_cilindro" id="diam_ext_cilindro" size="9">
																	</label>
																	<label class="input">Profundidad de la Indentacion:
																		<input type="text" name="prof_indentacion" id="prof_indentacion" onchange="validar_prof_indent(this.value)" size="9">
																	</label>
																	<label class="input">Diametro de la Indentancion:
																		<input type="text" name="diam_indentacion" id="diam_indentacion" onchange="validar_diam_indent(this.value)" size="9">
																	</label>
																</div>
															</div>
														</div>
													</div><!-- -----------------------INDENTACION------------------------------- -->
													
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Grieta: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('grieta')"></label>
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="grieta" id="grieta1" onclick="checkGrieta('1');" value="1" <?php if($grieta_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="grieta" id="grieta2" onclick="checkGrieta('0');" value="2" <?php if($grieta_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="grieta_h" id="grieta_h">
																	</label>
																</div>
															</div>  
														</div>															
													</div><!-- -----------------------GRIETA------------------------------- -->
													<div class="row">
														<div class="col-sm-6">
														  	<div class="form-group">
																<label for="category">Quemaduras por Arco y Antorcha: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('arco_antorcha')"></label>                            
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="arco_antorcha" id="arco_antorcha1" onclick="checkArcoAntorcha('1');" value="1" <?php if($arco_antorcha_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si</label>
																	<label class="radio-inline">
																		<input type="radio" name="arco_antorcha" id="arco_antorcha2" onclick="checkArcoAntorcha('0');" value="2" <?php if($arco_antorcha_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No</label>
																		<input type="hidden" name="arco_antorcha_h" id="arco_antorcha_h">
																</div>
															</div>  
														</div>
													</div><!-- -----------------------QUEMADURAS POR ARCO Y ANTORCHA------------------------------- -->
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Daño por Fuego: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('fuego')">
																</label>                            
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="dano_fuego" id="dano_fuego1" onclick="checkDanoFuego('1');" value="1" <?php if($dano_fuego_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="dano_fuego" id="dano_fuego2" onclick="checkDanoFuego('0');" value="2" <?php if($dano_fuego_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="dano_fuego_h" id="dano_fuego_h">
																	</label>
																</div>
															</div>  
														</div>
													</div><!-- -----------------------DAÑO POR FUEGO------------------------------- -->
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Insertos en Tapon o Cuello: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('tapon_cuello')">
																</label>                            
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="tapon_cuello" id="tapon_cuello1" onclick="checkTaponCuello('1');" value="1" <?php if($tapon_cuello_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?> >
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="tapon_cuello" id="tapon_cuello2" onclick="checkTaponCuello('0');" value="2" <?php if($tapon_cuello_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?> >
																		<i></i>No
																		<input type="hidden" name="tapon_cuello_h" id="tapon_cuello_h">
																	</label>
																</div>
															</div>  
														</div>
													</div><!-- -----------------------INSERTOS EN TAPON O CUELLO------------------------------- -->
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Defectos en el Cuello: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('defecto_cuello')"></label>
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="defecto_cuello" id="defecto_cuello1" onclick="checkCuello('1');" value="1" <?php if($defecto_cuello_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="defecto_cuello" id="defecto_cuello2" onclick="checkCuello('0');" value="2" <?php if($defecto_cuello_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="defecto_cuello_h" id="defecto_cuello_h">
																	</label>
																</div>
															</div>  
														</div>
														<div class="row" id="info_defecto_cuello" style="display: none">
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="category"><strong>Información del Defecto del Cuello</strong></label>
																	<label class="input"> Tipo de Rosca:
																		<select name="tipo_rosca" id="tipo_rosca">
																			<option value="0">Seleccione...</option>
																			<option value="conica">Conica</option>
																			<option value="recta">Recta</option>
																		</select>
																	</label>
																	<label class="input">Numero de Hilos:
																		<input type="text" name="no_hilos" id="no_hilos" onchange="validarHilos(this.value)" size="10">
																	</label>
																	<label class="input">Dañó por Corrosión:
																		<input type="checkbox" name="corrosion_cuello_si" value="si_corr_cuello" id="si_corr_cuello" onclick="corr_cuello()">Si
																		<input type="checkbox" name="corrosion_cuello_no" value="no_corr_cuello" id="no_corr_cuello">No
																		
																	</label>
																</div>
															</div>
														</div>
													</div><!-- -----------------------DEFECTOS EN EL CUELLO------------------------------- -->
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Estampación: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('estampacion')"></label>
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="estampacion" id="estampacion1" onclick="checkEstampacion('1');" value="1" <?php if($estampacion_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="estampacion" id="estampacion2" onclick="checkEstampacion('0');" value="2" <?php if($estampacion_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="estampacion_h" id="estampacion_h">
																	</label>
																</div>
															</div>  
														</div>
													</div><!-- -----------------------ESTAMPACION------------------------------- -->
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Marcas Sospechosas: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('marcas')"></label>
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="marcas_sospechosas" id="marcas1" onclick="checkMarcas('1');" value="1" <?php if($marcas_sospechosas_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="marcas_sospechosas" id="marcas2" onclick="checkMarcas('0');" value="2" <?php if($marcas_sospechosas_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="marcas_sospechosas_h" id="marcas_sospechosas_h">
																	</label>
																</div>
															</div>  
														</div>
													</div><!-- -----------------------MARCAS SOSPECHOSAS------------------------------- -->
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Estabilidad Vertical: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('estabilidad')"></label>
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="estabilidad_vertical" id="estabilidad1" onclick="checkEstabilidadVertical('1');" value="1" <?php if($estabilidad_vertical_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="estabilidad_vertical" id="estabilidad2" onclick="checkEstabilidadVertical('0');" value="2" <?php if($estabilidad_vertical_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="estabilidad_h" id="estabilidad_h">
																	</label>
																</div>
															</div>  
														</div>
													</div><!-- -----------------------ESTABILIDAD VERTICAL------------------------------- -->
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Corrosion: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('corrosion')">
																</label>                            
															
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="corrosion" id="corrosion1" onclick="checkCorrosion('1');" value="1" <?php if($corrosion_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="corrosion" id="corrosion2" onclick="checkCorrosion('0');" value="2" <?php if($corrosion_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="corrosion_h" id="corrosion_h">
																	</label>
																</div>
															</div>  
														</div>
														<div class="row" id="info_corrosion" style="display: none">
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="category"><strong>Información de la Corrosion</strong></label>
																	<label class="input">Tara Actual:
																		<input type="text" name="tara_actual" id="tara_actual" size="9">
																	</label>
																	<label class="input">Tara Estampada:
																		<input type="text" name="tara_estampada" id="tara_estampada" size="9" onchange="validarTara(this.value)">
																	</label>
																	<label class="input">Espesor de Pared Restante:
																		<input type="text" name="espesor_res_corr" id="espesor_res_corr" size="9">
																	</label>
																	<label class="input">Espesor Minimo Permisible
																		<input type="text" name="espesor_min_corr" id="espesor_min_corr" onchange="validar_espesor_corrosion(this.value)" size="9">
																	</label>
																	<label class="input">Longitud Defecto
																		<input type="text" name="long_corr" id="long_corr" size="9" onchange="validar_long_corrosion(this.value)">
																	</label>
																</div>
															</div>
														</div>
													</div><!-- -----------------------CORROSION------------------------------ -->		
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Corrosion General con Picaduras: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('corrosion_general')">
																</label>                            
															
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="corrosion_general" id="corrosion_general1" onclick="checkCorrosionGeneral('1');" value="1" <?php if($corrosion_general_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="corrosion_general" id="corrosion_general2" onclick="checkCorrosionGeneral('0');" value="2" <?php if($corrosion_general_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="corrosion_general_h" id="corrosion_general_h">
																	</label>
																</div>
															</div>  
														</div>
														<div class="row" id="info_corrosion_general" style="display: none">
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="category"><strong>Información de la Corrosion General</strong></label>
																	<label class="input">Espesor de Pared Restante:
																		<input type="text" name="espesor_pared_corr_gene" id="espesor_pared_corr_gene" size="9" onchange="validar_espesor_restante(this.value)">
																	</label>
																	<label class="input">Profundidad Maxima de Picadura:
																		<input type="text" name="prof_picadura_corr_gene" id="prof_picadura_corr_gene" size="8" onchange="validar_profundidad_corr_gene(this.value)">
																	</label>
																	<label class="input">Espesor de Pared Original:
																		<input type="text" name="espesor_orig_corr_gene" id="espesor_orig_corr_gene" onchange="validarCorrGral(this.value)" size="9">
																	</label>
																</div>
															</div>
														</div>
													</div><!-- -----------------------CORROSION GENERAL CON PICADURAS------------------------------ -->
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Cortes Ranuras o Perforaciones: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('cortes_perforaciones')">
																</label>                            
															
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="cort_ranu_perfo" id="cort_ranu_perfo1" onclick="checkCortRanuPerfo('1');" value="1" <?php if($cort_ranu_perfo_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="cort_ranu_perfo" id="cort_ranu_perfo2" onclick="checkCortRanuPerfo('0');" value="2" <?php if($cort_ranu_perfo_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="cort_ranu_perfo_h" id="cort_ranu_perfo_h">
																	</label>
																</div>
															</div>  
														</div>
														<div class="row" id="info_cort_ranu_perfo" style="display: none">
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="category"><strong>Información del Defecto</strong></label>
																	<label class="input">Espesor Minimo de Pared de Diseño:
																		<input type="text" name="espesor_min_cort" id="espesor_min_cort" size="9" >
																	</label>
																	<label class="input">Espesor de Pared Original:</br>
																		<input type="text" name="espesor_orig_cort" id="espesor_orig_cort" size="9">
																	</label>
																	<label class="input">Profundidad Maxima del Defecto:
																		<input type="text" name="prof_max_cort" id="prof_max_cort" size="9" onchange="validarProfCort(this.value)">
																	</label>
																	<label class="input">Longitud del Defecto:</br>
																		<input type="text" name="longitud_cort" id="longitud_cort" size="9" onchange="validarLongCort(this.value)">
																	</label>
																</div>
															</div>
														</div>
													</div><!-- -----------------------CORTES RANURAS O PERFORACIONES------------------------------ -->	 
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Picaduras Aisladas: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('picaduras_aisladas')">
																</label>                            
															
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="picaduras_aisladas" id="picaduras_aisladas1" onclick="checkPicadurasAisladas('1');" value="1" <?php if($picaduras_aisladas_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="picaduras_aisladas" id="picaduras_aisladas2" onclick="checkPicadurasAisladas('0');" value="2" <?php if($picaduras_aisladas_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="picaduras_aisladas_h" id="picaduras_aisladas_h">
																	</label>
																</div>
															</div>  
														</div>
														<div class="row" id="info_picaduras_aisladas" style="display: none">
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="category"><strong>Información de las Picaduras Aisladas</strong></label>
																	<label class="input">Espesor de Pared Restante:
																		<input type="text" name="espesor_pared_rest_picaduras" id="espesor_pared_rest_picaduras" size="9" onchange="validarEspesorRestPicaduras(this.value)">
																	</label>
																	<label class="input">Profundidad Maxima de Picaduras:
																		<input type="text" name="prof_max_picadura" id="prof_max_picadura" size="7">
																	</label>
																	<label class="input">Espesor de Pared Original:
																		<input type="text" name="espesor_original_picaduras" id="espesor_original_picaduras" size="9" onchange="validarProfPicadura(this.value)">
																	</label>
																</div>
															</div>
														</div>
													</div><!-- -----------------------PICADURAS AISLADAS------------------------------ -->	 
												</div>
												<div class="well well-sm well-primary">     
													<div class="row">
														<div class="col-sm-12">
															<div class="form-group">
																<label for="category"><strong style="text-decoration: underline; cursor: pointer;" onclick="info('espesores_minimos')">Tabla de Espesores Minimos para cilindros de Baja Presión</strong></label>
															</div>
														</div>
													</div>
												</div> 
												<div class="well well-sm well-primary">     
													<div class="row">
														<div class="form-group">
															<label for="category">Observaciones Generales:</label>
															<textarea class="form-control" name="observaciones"><?php echo $observaciones; ?></textarea>
														</div>
													</div>
												</div> 
												<div class="modal-footer">
													<?php
														if (in_array(22, $acc))
														{
															if($prueba_realizada != 1){
														?>
															<input type="hidden" name="id_cilindro" value="<?php echo $id_cilindro; ?>">
															<input type="submit" value="Guardar" name="g_insp_visual_acero" id="g_insp_visual_acero" class="btn btn-primary" />
														<?php
															}
														}                   
														?>
												</div>
											</form>
									 </div>
								</div>
						  </div>
					 </article>
				</div>
		  </section>
	 </div>
</div>
<!-- END MAIN PANEL -->
<div class="modal fade" id="modal_info_espesores_minimos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">ESPESORES MINIMOS</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
						
						<table border="1" style="text-align: center;">
							<tr>
								<th colspan="2" style="text-align: center;">DIAMETRO NOMINAL DEL CILINDRO</th>
								<th rowspan="2" style="text-align: center;">Especificación DOT marcada</th>
								<th colspan="2" style="text-align: center;">Espesor minimo de pared de diseño permisible</th>

							</tr>
							<tr>
								<td>mm</td>
								<td>Pulgadas</td>
								<td>mm</td>
								<td>Pulgadas</td>
							</tr>
							<tr>
								<td>102</td>
								<td>(4)</td>
								<td>4B500</td>
								<td>2,16</td>
								<td>(0,085)</td>
							</tr>
							<tr>
								<td>127</td>
								<td>(5)</td>
								<td>4B400</td>
								<td>2,77</td>
								<td>(0,109)</td>
							</tr>
							<tr>
								<td>127</td>
								<td>(5)</td>
								<td>4B240</td>
								<td>2,29</td>
								<td>(0,090)</td>
							</tr>
							<tr>
								<td>152</td>
								<td>(6)</td>
								<td>4B400</td>
								<td>2,29</td>
								<td>(0,090)</td>
							</tr>
							<tr>
								<td>152</td>
								<td>(6)</td>
								<td>4BA500</td>
								<td>2,03</td>
								<td>(0,080)</td>
							</tr>
							<tr>
								<td>152</td>
								<td>(6)</td>
								<td>4B500</td>
								<td>2,82</td>
								<td>(0,111)</td>
							</tr>
							<tr>
								<td>152</td>
								<td>(6)</td>
								<td>4B240</td>
								<td>2,29</td>
								<td>(0,090)</td>
							</tr>
							<tr>
								<td>171</td>
								<td>(6,75)</td>
								<td>4B300</td>
								<td>2,29</td>
								<td>(0,090)</td>
							</tr>
							<tr>
								<td>178</td>
								<td>(7)</td>
								<td>4BA300</td>
								<td>2,21</td>
								<td>(0,087)</td>
							</tr>
							<tr>
								<td>203</td>
								<td>(8)</td>
								<td>4B400</td>
								<td>3,18</td>
								<td>(0,125)</td>
							</tr>
							<tr>
								<td>203</td>
								<td>(8)</td>
								<td>2B240</td>
								<td>2,29</td>
								<td>(0,090)</td>
							</tr>
							<tr>
								<td>203</td>
								<td>(8)</td>
								<td>4B300</td>
								<td>2,67</td>
								<td>(0,105)</td>
							</tr>
							<tr>
								<td>203</td>
								<td>(8)</td>
								<td>4BA300</td>
								<td>2,21</td>
								<td>(0,087)</td>
							</tr>
							<tr>
								<td>229</td>
								<td>(9)</td>
								<td>4B240</td>
								<td>2,29</td>
								<td>(0,090)</td>
							</tr>
							<tr>
								<td>229</td>
								<td>(9)</td>
								<td>4BA240, 4BW240, 4BA300, 4BW300</td>
								<td>1,98</td>
								<td>(0,078)</td>
							</tr>
							<tr>
								<td>254</td>
								<td>(10)</td>
								<td>4BA240, 4BW240, 4BA300, 4BW300</td>
								<td>1,98</td>
								<td>(0,078)</td>
							</tr>
							<tr>
								<td>305</td>
								<td>(12)</td>
								<td>4B240</td>
								<td>2,67</td>
								<td>(0,105)</td>
							</tr>
							<tr>
								<td>305</td>
								<td>(12)</td>
								<td>4B240, 4BW240</td>
								<td>1,98</td>
								<td>(0,105)</td>
							</tr>
							<tr>
								<td>368</td>
								<td>(14,5)</td>
								<td>4B240</td>
								<td>3,18</td>
								<td>(0,125</td>
							</tr>
							<tr>
								<td>368</td>
								<td>(14,5)</td>
								<td>4B240, 4BW240</td>
								<td>2,21</td>
								<td>(0,087)</td>
							</tr>
							<tr>
								<td>368</td>
								<td>(14,5)</td>
								<td>4AA480</td>
								<td>4,70</td>
								<td>(0,185)</td>
							</tr>
							<tr>
								<td>368</td>
								<td>(14,5)</td>
								<td>3A480</td>
								<td>5,38</td>
								<td>(0,212)</td>
							</tr>
							<tr>
								<td>559</td>
								<td>(22)</td>
								<td>4B240</td>
								<td>4,85</td>
								<td>(0,191)</td>
							</tr>
							<tr>
								<td>559</td>
								<td>(22)</td>
								<td>4BA240, 4BW240</td>
								<td>3,30</td>
								<td>(0,130)</td>
							</tr>
							<tr>
								<td>610</td>
								<td>(24)</td>
								<td>4B240</td>
								<td>5,28</td>
								<td>(0,208)</td>
							</tr>
							<tr>
								<td>610</td>
								<td>(24)</td>
								<td>4BA240, 4BW240</td>
								<td>3,61</td>
								<td>(0,142)</td>
							</tr>
							<tr>
								<td>762</td>
								<td>(30)</td>
								<td>4B240</td>
								<td>6,38</td>
								<td>(0,251)</td>
							</tr>
							<tr>
								<td>762</td>
								<td>(30)</td>
								<td>4BA240, 4BW240</td>
								<td>4,37</td>
								<td>(0,172)</td>
							</tr>
							<tr>
								<td colspan="5">Algunos cilindros tienen paredes mas gruesas, por las diferencias en métodos de fabricación y en procedimientos de inspección. Los valores indicados son para los minimos espesores absolutos permisible por especificaciones. Se pueden usar valores mas altos, si la información que muestra paredes más gruesas que las enumeradas es obtenida del fabricante del cilindro. La utilización de prueba ultrasónica se sugiere para determinar el espesor de la pared).</td>
							</tr>
						</table>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>
<div class="modal fade" id="modal_info_protuberancia" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">PROTUBERANCIA</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	Hinchazon visible del cilindro.
								</p>
								
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_abolladuras" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">ABOLLADURAS</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	Para un cilindro de 228,6 mm (9 pulgadas) de diámetro x 1295,4 mm (51 pulgadas) de largo, acepta abolladuras hasta de 1,575 mm (0,062 pulgadas) de profundidad, cuando el diámetro mayor de la misma es de 50,8 mm (2 pulgadas) o más.
								</p>
								
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_abombamiento" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">ABOMBAMIENTO</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	Todo cilindro con abombamiento razonablemente visible, como las causadas por incendio.
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_indentacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">INDENTACION</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	Profundidad de la indentación excede el 3% del diámetro externo del cilindro
								</p>
								<p>
									*	Diámetro de indentación sea menor a x 15 su profundidad
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_grieta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">GRIETA</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	Hendidura en el metal.
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_arco_antorcha" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">QUEMADURAS POR ARCO Y ANTORCHA</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	Remoción de metal por biselado o cráteres.
								</p>
								<p>
									*	Biselado o quemadura del metal básico.
								</p>
								<p>
									*	Zona fuertemente afectada por el calor
								</p>
								<p>
									*	Depósito de metal soldado o desplazamiento del metal básico.
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_fuego" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">DAÑO POR FUEGO</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	Fusión parcial del cilindro
								</p>
								<p>
									*	Distorsión del cilindro.
								</p>
								<p>
									*	Carbonización o quemadura de la pintura.
								</p>
								<p>
									*	Daño por fuego en la válvula, derretimiento de la protección plástica o anillo de fecha o tapón fusible, si se encuentran acondicionados.
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_tapon_cuello" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">INSERTOS EN TAPON O CUELLO</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	Todos los cilindros, a menos que se establezca claramente que la adición es parte del diseño aprobado
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_defecto_cuello" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">DEFECTOS EN EL CUELLO</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	El número de hilos efectivos de la rosca del cuello se ha reducido materialmente.
								</p>
								<p>
									*	Menos de cinco (5) hilos completos de rosca para roscas cónicas en cilindros de baja presión.
								</p>
								<p>
									*	Menos de siete (7) hilos completos para rosca cónica en cilindros de alta presión.
								</p>
								<p>
									*	Menos de seis (6) hilos para todos los cilindros con rosca recta.
								</p>
								<p>
									*	Grietas visible en los hilos o áreas adyacentes. 
								</p>
								<p>
									*	Daño visible por la corrosión o de otra clase que pueda afectar la estructura integral de la rosca o de la válvula en el momento de la instalación.
								</p>
								<p>
									*	Cilindros fabricados para una especificación con un número de hilos menor a los mencionados que no mantengan sus hilos completos, continuos y sin daños.
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_estampacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">ESTAMPACIÓN</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	Todos los cilindros con marcaciones ilegibles, modificadas o incorrectas.
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_marcas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">MARCAS</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	Marcas diferentes a las formadas en el proceso de manufactura o la reparación aprobada del cilindro.
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_estabilidad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">ESTABILIDAD VERTICAL</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	La desviación de la verticalidad que pueda presentar riesgo durante el servicio (en especial si está acondicionado con anillo de base).
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_corrosion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">CORROSIÓN</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	El peso de tara es inferior al 90% del peso original estampado.
								</p>
								<p>
									*	La pared restante en un área que tenga picaduras aisladas solamente, sea menos de un tercio (1/3) del espesor mínimo de pared de diseño permisible.
								</p>
								<p>
									*	El peso de tara es inferior al 90% del original estampado.  Un cilindro rechazado puede ser calificado nuevamente por camisa de agua o por expansión directa.
								</p>
								<p>
									*	La línea o grieta de corrosión sea de 76,2 mm (3 pulgadas) de longitud o más y la pared restante es menor a tres cuartos (3/4)  del espesor mínimo de pared de diseño permisible.
								</p>
								<p>
									*	La línea o grieta de corrosión sea inferior a  76,2 mm (3 pulgadas) de longitud  y la pared restante es inferior a la mitad (1/2)  del espesor mínimo de pared de diseño permisible.
								</p>
								<p>
									*	La pared restante, en un área de corrosión general, es inferior a la mitad (1/2) del espesor mínimo de pared de diseño permisible.
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_corrosion_general" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">CORROSIÓN GENERAL CON PICADURAS</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	El espesor de pared puede ser medido y la pared restante es inferior a 1,60 mm (0,063 pulgadas).
								</p>
								<p>
									*	El espesor de pared no puede ser medido , el espesor de pared original no se conoce y la profundidad máxima de la picadura en el área de corrosión general excede 1,067 mm (0,42 pulgadas).
								</p>
								<p>
									*	El espesor actual de la pared no puede ser medido pero se conoce el espesor de pared original, y este menos una y media ( 1 ½) veces la profundidad máxima de la picadura, es inferior a 1,600 mm (0,063 pulgadas).
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_cortes_perforaciones" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">CORTES RANURAS O PERFORACIONES</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	El espesor de pared original no es conocido, el espesor actual no puede ser medido y el defecto excede la mitad del espesor mínimo de pared de diseño permisible.
								</p>
								<p>
									*	El espesor de pared original es conocido, el espesor actual es medido, y el espesor de pared original, menos la profundidad del defecto, es menos de la mitad del espesor mínimo de pared de diseño permisible.
								</p>
								<p>
									*	Los cilindros deben ser condenados a la mitad del límite fijado si la longitud del defecto es de 76,2 mm (3 pulgadas o más).
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_picaduras_aisladas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">PICADURAS AISLADAS</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	El espesor de pared puede ser medido y la pared restante es inferior a 1,067 mm (0,042 pulgadas).
								</p>
								<p>
									*	El espesor de pared no puede ser medido , el espesor de pared original no se conoce y la profundidad de la picadura excede un tercio (1/3) del espesor mínimo de pared de diseño permisible.
								</p>
								<p>
									*	El espesor actual de la pared no puede ser medido pero se conoce el espesor de pared original, y al substraer la profundidad máxima de la picadura del espesor de pared original, es inferior a 1,067 mm (0,042pulgadas).
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>
<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->
<?php
	 include("inc/footer.php");
?>
<!-- END PAGE FOOTER -->

<?php 
	 //include required scripts
	 include("inc/scripts.php"); 
?>


<!-- PAGE RELATED PLUGIN(S) 
<script src="..."></script>-->
<!-- Flot Chart Plugin: Flot Engine, Flot Resizer, Flot Tooltip -->
<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>

<!-- Vector Maps Plugin: Vectormap engine, Vectormap language -->
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>

<!-- Full Calendar -->
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/jquery-form/jquery-form.min.js"></script>

<script type="text/javascript">
function info(ventana){
	 $('#modal_info_'+ventana).modal();
}

function checkProtuberancia(valor){
  	valorCheck = valor;

  	if(valorCheck == 1){
  		
  		if(confirm("Al seleccionar esta opción el cilindro será condenado, ¿Desea continuar?")){
  			if(confirm("¿Desea dejar el cilindro en estado Rechazado para poder continuar despues?")){
  				alert('Cilindro Rechazado!');
  				document.getElementById('protuberancia1').disabled = true;
	  			document.getElementById('protuberancia2').disabled = true;
	  			document.getElementById('estado_prueba').value = 'RECHAZADO';
	  			document.getElementById('motivo_condena'). value = 'Protuberancia';
	  			document.getElementById('protuberancia_h').value = '1';
	  			inhabilitarFormulario('protuberancia1','protuberancia_h');
  			}else{
  				alert('Cilindro Condenado!');
  				document.getElementById('protuberancia1').disabled = true;
	  			document.getElementById('protuberancia2').disabled = true;
	  			document.getElementById('estado_prueba').value = 'CONDENADO';
	  			document.getElementById('motivo_condena'). value = 'Protuberancia';
	  			document.getElementById('protuberancia_h').value = '1';
	  			inhabilitarFormulario('protuberancia1','protuberancia_h');
  			}
  			
  		}else{
  			document.getElementById('protuberancia1').checked = false;
  		}
  		
  	}else if(valorCheck == 0){
  		document.getElementById('protuberancia_h').value = '2';
  	}
}

function validar_profundidad_abolladura(valor){

	var profundidad_abolladura = valor;

	if(profundidad_abolladura > 6.35){
		if(confirm("Con estos datos el cilindro será condenado, ¿Desea continuar?")){
			document.getElementById('abolladuras1').disabled = true;
			document.getElementById('abolladuras2').disabled = true;
			document.getElementById('profundidad_abolladura').readOnly = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
			document.getElementById('motivo_condena'). value = 'Abolladura';
			document.getElementById('abolladuras_h').value = '1';
			inhabilitarFormulario('abolladuras1','abolladuras_h');
		}else{
			document.getElementById('abolladuras1').checked  = false;
		}
	}
}

function checkAbolladura(valor){

	var valorCheck = valor;

	if(valorCheck == 1){
		document.getElementById('info_abolladura').style.display = 'block';
		document.getElementById('abolladuras_h').value = '1';
	}else if(valorCheck == 0){
		
		document.getElementById('info_abolladura').style.display = 'none';
		document.getElementById('abolladuras_h').value = '2';
	}
}

function checkAbombamiento(valor){

	var valorCheck = valor;

	if(valorCheck==1){
		document.getElementById('abombamiento_h').value = '1';
		document.getElementById('info_ambombamiento').style.display = 'block';
	}else if (valorCheck == 0){
		document.getElementById('abombamiento_h').value = '2';
	}
}

function checkIndentacion(valor){

	valorCheck = valor;

	if(valorCheck == 1){
		document.getElementById('indentacion_h').value='1';
		document.getElementById('info_indentacion').style.display = 'block'
	}else if(valorCheck == 0){
		document.getElementById('indentacion_h').value='2';
	}
}

function validar_prof_indent(valor){
	profundidad_indentacion = valor;
	diametro_externo_cili = document.getElementById('diam_ext_cilindro').value;
	porc_indentacion = 0;

	porc_indentacion = profundidad_indentacion * 100 / diametro_externo_cili;

	if(porc_indentacion >= 3){
		if(confirm('Con estos datos, el cilindro será condenado, ¿Desea continuar?')){
			alert('Cilindro Condenado!');
			document.getElementById('prof_indentacion').readOnly = true;
			document.getElementById('diam_ext_cilindro').readOnly = true;
			document.getElementById('diam_indentacion').readOnly = true;
			document.getElementById('indentacion1').disabled = true;
			document.getElementById('indentacion2').disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
			document.getElementById('motivo_condena'). value = 'Indentancion';
			inhabilitarFormulario('indentacion1','indentacion_h');
		}else{
			document.getElementById('prof_indentacion').value = "";
		}
	}
}

function validar_diam_indent(valor){
	diametro_indetacion = valor;
	profundidad_indentacion = document.getElementById('prof_indentacion').value;
	valor_condena = profundidad_indentacion*15;

	if(valor_condena<=diametro_indetacion){
		if(confirm("Con estos valores el cilindro será condenado, ¿Desea continuar?")){
			alert('Cilindro Condenado!');
			document.getElementById('prof_indentacion').readOnly = true;
			document.getElementById('diam_ext_cilindro').readOnly = true;
			document.getElementById('diam_indentacion').readOnly = true;
			document.getElementById('indentacion1').disabled = true;
			document.getElementById('indentacion2').disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
			document.getElementById('motivo_condena'). value = 'Indentancion';
			inhabilitarFormulario('indentacion1','indentacion_h');
		}else{
			document.getElementById('diam_indentacion').value = "";	
		}
	}
}

function checkCortEstria(valor){

	valorCheck = valor;

	if(valorCheck == 1){
		document.getElementById('corte_estria_h').value = '1';
		document.getElementById('info_cort_estria').style.display = 'block';
	}else if(valorCheck == 0){
		document.getElementById('info_cort_estria').style.display = 'none';
		document.getElementById('corte_estria_h').value = '2';
	}
}

function validar_espesor(valor){

	espesor_cili = valor;
	espesor_min_cili = document.getElementById('espesor_min_cili').value;

	if(espesor_cili<espesor_min_cili){
		if(confirm("Con estos valores el cilindro será condenado, ¿Desea continuar?")){
			alert("Cilindro Condenado!");
			document.getElementById("espesor_min_cili").readOnly = true;
			document.getElementById("espesor_cili").readOnly = true;
			document.getElementById("prof_defect").readOnly = true;
			document.getElementById("long_defect").readOnly = true;
			document.getElementById("diamExtCilindro").readOnly = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
  			document.getElementById('motivo_condena'). value = 'Corte o Estria';
  			document.getElementById('corte_estria1').disabled = true;
  			document.getElementById('corte_estria2').disabled = true;
  			inhabilitarFormulario('corte_estria1','corte_estria_h');
		}else{
			document.getElementById('prof_defect').value = "";
		}
	}
}

function validar_prof_corte(valor){
	profundidad_corte = valor;
	espesor_cilindro = document.getElementById('espesor_cili').value;
	porcentaje_profundidad = 0;

	porcentaje_profundidad = profundidad_corte*100/espesor_cilindro;

	if(porcentaje_profundidad>=10){
		if(confirm("Con estos valores el cilindro será condenado, ¿Desea continuar?")){
			alert("Cilindro Condenado!");
			document.getElementById("espesor_min_cili").readOnly = true;
			document.getElementById("espesor_cili").readOnly = true;
			document.getElementById("prof_defect").readOnly = true;
			document.getElementById("long_defect").readOnly = true;
			document.getElementById("diamExtCilindro").readOnly = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
  			document.getElementById('motivo_condena'). value = 'Corte o Estria';
  			document.getElementById('corte_estria1').disabled = true;
  			document.getElementById('corte_estria2').disabled = true;
  			inhabilitarFormulario('corte_estria1','corte_estria_h');
		}else{
			document.getElementById('prof_defect').value = "";
		}
	}
}

function validar_long_corte(valor){
	longitud_corte = valor;
	diametro_externo_cilindro = document.getElementById('diamExtCilindro').value;
	porc_longitud_corte = 0;

	porc_longitud_corte = longitud_corte*100/diametro_externo_cilindro;

	if(porc_longitud_corte >= 25){
		if(confirm("Con estos valores el cilindro será condenado,¿Desea continuar?")){
			alert("Cilindro Condenado!");
			document.getElementById("espesor_min_cili").readOnly = true;
			document.getElementById("espesor_cili").readOnly = true;
			document.getElementById("prof_defect").readOnly = true;
			document.getElementById("long_defect").readOnly = true;
			document.getElementById("diamExtCilindro").readOnly = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
  			document.getElementById('motivo_condena'). value = 'Corte o Estria';
  			document.getElementById('corte_estria1').disabled = true;
  			document.getElementById('corte_estria2').disabled = true;
  			inhabilitarFormulario('corte_estria1','corte_estria_h');
		}else{
			document.getElementById('long_defect').value = "";
		}
	}
}

function checkGrieta(valor){
  	valorCheck = valor;

  	if(valorCheck == 1){
  		
  		if(confirm("Al seleccionar esta opción el cilindro será condenado, ¿Desea continuar?")){
  			if(confirm("¿Desea dejar el cilindro en estado Rechazado para poder continuar despues?")){
  				alert('Cilindro Rechazado!');
  				document.getElementById('grieta1').disabled = true;
	  			document.getElementById('grieta2').disabled = true;
	  			document.getElementById('estado_prueba').value = 'RECHAZADO';
	  			document.getElementById('motivo_condena'). value = 'Grieta';
	  			document.getElementById('grieta_h').value = '1';
	  			inhabilitarFormulario('grieta1','grieta_h');
  			}else{
  				alert('Cilindro Condenado!');
  				document.getElementById('grieta1').disabled = true;
	  			document.getElementById('grieta2').disabled = true;
	  			document.getElementById('estado_prueba').value = 'CONDENADO';
	  			document.getElementById('motivo_condena'). value = 'Grieta';
	  			document.getElementById('grieta_h').value = '1';
	  			inhabilitarFormulario('grieta1','grieta_h');
  			}
  			
  		}else{
  			document.getElementById('grieta1').checked = false;
  		}
  		
  	}else if(valorCheck == 0){
  		document.getElementById('grieta_h').value = '2';
  	}
}

function checkArcoAntorcha(valor){
  	valorCheck = valor;

  	if(valorCheck == 1){
  		
  		if(confirm("Al seleccionar esta opción el cilindro será condenado, ¿Desea continuar?")){
  			if(confirm("¿Desea dejar el cilindro en estado Rechazado para poder continuar despues?")){
  				alert('Cilindro Rechazado!');
  				document.getElementById('arco_antorcha1').disabled = true;
	  			document.getElementById('arco_antorcha2').disabled = true;
	  			document.getElementById('estado_prueba').value = 'RECHAZADO';
	  			document.getElementById('motivo_condena'). value = 'Quemadura por Arco y Antorcha';
	  			document.getElementById('arco_antorcha_h').value = '1';
	  			inhabilitarFormulario('arco_antorcha1','arco_antorcha_h');
  			}else{
  				alert('Cilindro Condenado!');
  				document.getElementById('arco_antorcha1').disabled = true;
	  			document.getElementById('arco_antorcha2').disabled = true;
	  			document.getElementById('estado_prueba').value = 'CONDENADO';
	  			document.getElementById('motivo_condena'). value = 'Quemadura por Arco y Antorcha';
	  			document.getElementById('arco_antorcha_h').value = '1';
	  			inhabilitarFormulario('arco_antorcha1','arco_antorcha_h');
  			}
  			
  		}else{
  			document.getElementById('arco_antorcha1').checked = false;
  		}
  		
  	}else if(valorCheck == 0){
  		document.getElementById('arco_antorcha_h').value = '2';
  	}
}

function checkDanoFuego(valor){
  	valorCheck = valor;

  	if(valorCheck == 1){
  		
  		if(confirm("Al seleccionar esta opción el cilindro será condenado, ¿Desea continuar?")){
  			if(confirm("¿Desea dejar el cilindro en estado Rechazado para poder continuar despues?")){
  				alert('Cilindro Rechazado!');
  				document.getElementById('dano_fuego1').disabled = true;
	  			document.getElementById('dano_fuego2').disabled = true;
	  			document.getElementById('estado_prueba').value = 'RECHAZADO';
	  			document.getElementById('motivo_condena'). value = 'Daño por Fuego';
	  			document.getElementById('dano_fuego_h').value = '1';
	  			inhabilitarFormulario('dano_fuego1','dano_fuego_h');
  			}else{
  				alert('Cilindro Condenado!');
  				document.getElementById('dano_fuego1').disabled = true;
	  			document.getElementById('dano_fuego2').disabled = true;
	  			document.getElementById('estado_prueba').value = 'CONDENADO';
	  			document.getElementById('motivo_condena'). value = 'Daño por Fuego';
	  			document.getElementById('dano_fuego_h').value = '1';
	  			inhabilitarFormulario('dano_fuego1','dano_fuego_h');
  			}
  			
  		}else{
  			document.getElementById('dano_fuego1').checked = false;
  		}
  		
  	}else if(valorCheck == 0){
  		document.getElementById('dano_fuego_h').value = '2';
  	}
}

function checkTaponCuello(valor){
  	valorCheck = valor;

  	if(valorCheck == 1){
  		
  		if(confirm("Al seleccionar esta opción el cilindro será condenado, ¿Desea continuar?")){
  			if(confirm("¿Desea dejar el cilindro en estado Rechazado para poder continuar despues?")){
  				alert('Cilindro Rechazado!');
  				document.getElementById('tapon_cuello1').disabled = true;
	  			document.getElementById('tapon_cuello2').disabled = true;
	  			document.getElementById('estado_prueba').value = 'RECHAZADO';
	  			document.getElementById('motivo_condena'). value = 'Insertos en Tapon o Cuello';
	  			document.getElementById('tapon_cuello_h').value = '1';
	  			inhabilitarFormulario('tapon_cuello1','tapon_cuello_h');
  			}else{
  				alert('Cilindro Condenado!');
  				document.getElementById('tapon_cuello1').disabled = true;
	  			document.getElementById('tapon_cuello2').disabled = true;
	  			document.getElementById('estado_prueba').value = 'CONDENADO';
	  			document.getElementById('motivo_condena'). value = 'Insertos en Tapon o Cuello';
	  			document.getElementById('tapon_cuello_h').value = '1';
	  			inhabilitarFormulario('tapon_cuello1','tapon_cuello_h');
  			}
  			
  		}else{
  			document.getElementById('tapon_cuello1').checked = false;
  		}
  		
  	}else if(valorCheck == 0){
  		document.getElementById('tapon_cuello_h').value = '2';
  	}
}

function checkCuello(valor){
  	valorCheck = valor;

  	if(valorCheck == 1){
  		document.getElementById('defecto_cuello_h').value = '1';
  		document.getElementById('info_defecto_cuello').style.display = 'block';
  	}else if(valorCheck == 0){
  		document.getElementById('defecto_cuello_h').value = '2';
  		document.getElementById('info_defecto_cuello').style.display = 'none';
  	}
}

function validarHilos(valor){
	numero_hilos = valor;
	tipo_rosca =document.getElementById('tipo_rosca').value;

	if(tipo_rosca == 0){
		alert('Por favor seleccione el tipo de rosca');
		document.getElementById('no_hilos').value = "";
	}else if(tipo_rosca == "conica"){
		if(numero_hilos<5){
			if(confirm("Con estos datos el cilindro será condenado, ¿Desea continuar?")){
				document.getElementById('defecto_cuello1').disabled = true;
				document.getElementById('defecto_cuello2').disabled = true;
				document.getElementById('tipo_rosca').disabled = true;
				document.getElementById('no_hilos').readOnly = true;
				document.getElementById('si_corr_cuello').disabled = true;
				document.getElementById('no_corr_cuello').disabled = true;
				document.getElementById('estado_prueba').value = 'CONDENADO';
  				document.getElementById('motivo_condena'). value = 'Defectos del Cuello';
  				inhabilitarFormulario('defecto_cuello1','defecto_cuello_h');
			}else{
				document.getElementById('no_hilos').value = "";
			}
		}
	}else if(tipo_rosca == "recta"){
		if(numero_hilos<6){
			if(confirm("Con estos datos el cilindro será condenado, ¿Desea continuar?")){
				document.getElementById('defecto_cuello1').disabled = true;
				document.getElementById('defecto_cuello2').disabled = true;
				document.getElementById('tipo_rosca').disabled = true;
				document.getElementById('no_hilos').readOnly = true;
				document.getElementById('si_corr_cuello').disabled = true;
				document.getElementById('no_corr_cuello').disabled = true;
				document.getElementById('estado_prueba').value = 'CONDENADO';
  				document.getElementById('motivo_condena'). value = 'Defectos del Cuello';
  				inhabilitarFormulario('defecto_cuello1','defecto_cuello_h');
			}else{
				document.getElementById('no_hilos').value = "";
			}
		}
	}
}

function corr_cuello(){

	if(confirm("Con estos datos el cilindro será condenado, ¿Desea continuar?")){
		document.getElementById('defecto_cuello1').disabled = true;
		document.getElementById('defecto_cuello2').disabled = true;
		document.getElementById('tipo_rosca').disabled = true;
		document.getElementById('no_hilos').readOnly = true;
		document.getElementById('si_corr_cuello').disabled = true;
		document.getElementById('no_corr_cuello').disabled = true;
		document.getElementById('estado_prueba').value = 'CONDENADO';
  		document.getElementById('motivo_condena'). value = 'Defectos del Cuello';
  		inhabilitarFormulario('defecto_cuello1','defecto_cuello_h');
	}else{
		document.getElementById('si_corr_cuello').checked = false;
	}
}

function checkEstampacion(valor){

	var valorCheck = valor;

	if(valorCheck == 1){
		if(confirm("Al seleccionar esta opción el cilindro será condenado, ¿Desea continuar?")){
			if(confirm("¿Desea dejar el cilindro en estado Rechazado para poder continuar despues?")){
  				alert('Cilindro Rechazado!');
  				document.getElementById('estampacion1').disabled = true;
				document.getElementById('estampacion2').disabled = true;
				document.getElementById('estado_prueba').value = 'RECHAZADO';
				document.getElementById('motivo_condena'). value = 'Estampación';
				document.getElementById('estampacion_h').value = '1';
				inhabilitarFormulario('estampacion1','estampacion_h');
  			}else{
  				alert('Cilindro Condenado!');
  				document.getElementById('estampacion1').disabled = true;
				document.getElementById('estampacion2').disabled = true;
				document.getElementById('estado_prueba').value = 'CONDENADO';
				document.getElementById('motivo_condena'). value = 'Estampación';
				document.getElementById('estampacion_h').value = '1';
				inhabilitarFormulario('estampacion1','estampacion_h');
  			}
			
		}else{
			document.getElementById('estampacion1').checked  = false;
		}
	}else if(valorCheck == 0){
		
		document.getElementById('estampacion_h').value = '2';
	}
}

function checkMarcas(valor){

	var valorCheck = valor;

	if(valorCheck == 1){
		if(confirm("Al seleccionar esta opción el cilindro será condenado, ¿Desea continuar?")){
			if(confirm("¿Desea dejar el cilindro en estado Rechazado para poder continuar despues?")){
  				alert('Cilindro Rechazado!');
  				document.getElementById('marcas1').disabled = true;
				document.getElementById('marcas2').disabled = true;
				document.getElementById('estado_prueba').value = 'RECHAZADO';
				document.getElementById('motivo_condena'). value = 'Marcas Sospechosas';
				document.getElementById('marcas_sospechosas_h').value = '1';
				inhabilitarFormulario('marcas1','marcas_sospechosas_h');
  			}else{
  				alert('Cilindro Condenado!');
  				document.getElementById('marcas1').disabled = true;
				document.getElementById('marcas2').disabled = true;
				document.getElementById('estado_prueba').value = 'CONDENADO';
				document.getElementById('motivo_condena'). value = 'Marcas Sospechosas';
				document.getElementById('marcas_sospechosas_h').value = '1';
				inhabilitarFormulario('marcas1','marcas_sospechosas_h');
  			}
			
		}else{
			document.getElementById('marcas1').checked  = false;
		}
	}else if(valorCheck == 0){
		
		document.getElementById('marcas_sospechosas_h').value = '2';
	}
}

function checkEstabilidadVertical(valor){

	var valorCheck = valor;

	if(valorCheck == 1){
		if(confirm("Al seleccionar esta opción el cilindro será condenado, ¿Desea continuar?")){
			if(confirm("¿Desea dejar el cilindro en estado Rechazado para poder continuar despues?")){
  				alert('Cilindro Rechazado!');
  				document.getElementById('estabilidad1').disabled = true;
				document.getElementById('estabilidad2').disabled = true;
				document.getElementById('estado_prueba').value = 'RECHAZADO';
				document.getElementById('motivo_condena'). value = 'Estabilidad Vertical';
				document.getElementById('estabilidad_h').value = '1';
				inhabilitarFormulario('estabilidad1','estabilidad_h');
  			}else{
  				alert('Cilindro Condenado!');
  				document.getElementById('estabilidad1').disabled = true;
				document.getElementById('estabilidad2').disabled = true;
				document.getElementById('estado_prueba').value = 'CONDENADO';
				document.getElementById('motivo_condena'). value = 'Estabilidad Vertical';
				document.getElementById('estabilidad_h').value = '1';
				inhabilitarFormulario('estabilidad1','estabilidad_h');
  			}
			
		}else{
			document.getElementById('estabilidad1').checked  = false;
		}
	}else if(valorCheck == 0){
		
		document.getElementById('estabilidad_h').value = '2';
	}
}

function checkCorrosionGeneral(valor){
	valorCheck = valor;

	if(valorCheck == 1){
		document.getElementById('info_corrosion_general').style.display = "block";
		document.getElementById('corrosion_general_h').value = 1;
	}else if (valorCheck == 0) {
		document.getElementById('info_corrosion_general').style.display = "none";
		document.getElementById('corrosion_general_h').value = 2;
	}
}

function validar_superficie_corr_gene(){

	if(confirm("Con esta opción el cilindro será condenado,¿Desea continuar?")){
		alert("Cilindro Condenado!");
		document.getElementById('corrosion_general1').disabled = true;
		document.getElementById('corrosion_general2').disabled = true;
		document.getElementById('superficie_cil_si').disabled = true;
		document.getElementById('superficie_cil_no').disabled = true;
		document.getElementById('superficie_cil_no').checked = false;
		document.getElementById('espesor_pared1').readOnly = true;
		document.getElementById('espesor_min_gar').readOnly = true;
		document.getElementById('prof_corr_gene').readOnly = true;
		document.getElementById('estado_prueba').value = 'CONDENADO';
		document.getElementById('motivo_condena'). value = 'Corrosion General';
		inhabilitarFormulario('corrosion_general1','corrosion_general_h');
	}else{
		document.getElementById('superficie_cil_si').checked = false;
	}
}

function validar_espesor_corr_gene(valor){
	espesor_minimo_garantizado = valor;
	espesor_cilindro = document.getElementById('espesor_pared1').value;

	if(espesor_cilindro>espesor_minimo_garantizado){
		if(confirm("Con esta opción el cilindro será condenado,¿Desea continuar?")){
			alert("Cilindro Condenado!");
			document.getElementById('espesor_pared1').readOnly = true;
			document.getElementById('espesor_min_gar').readOnly = true;
			document.getElementById('prof_corr_gene').readOnly = true;
			document.getElementById('corrosion_general1').disabled = true;
			document.getElementById('corrosion_general2').disabled = true;
			document.getElementById('superficie_cil_si').disabled = true;
			document.getElementById('superficie_cil_no').disabled = true;
			document.getElementById('superficie_cil_no').checked = false;
			document.getElementById('estado_prueba').value = 'CONDENADO';
			document.getElementById('motivo_condena'). value = 'Corrosion General';
			inhabilitarFormulario('corrosion_general1','corrosion_general_h');
		}else{
			document.getElementById('espesor_pared1').value = "";
			document.getElementById('espesor_min_gar').value = "";
		}
	}
}

function validar_prof_corr_gene(valor){
	profundidad_penetracion = valor;
	espesor_pared = document.getElementById('espesor_pared1').value;
	porcentaje_profundidad = 0;

	porcentaje_profundidad = profundidad_penetracion * 100 / espesor_pared;

	if(porcentaje_profundidad>=10){
		if(confirm("Con esta opción el cilindro será condenado,¿Desea continuar?")){
			alert("Cilindro Condenado!");
			document.getElementById('espesor_pared1').readOnly = true;
			document.getElementById('espesor_min_gar').readOnly = true;
			document.getElementById('prof_corr_gene').readOnly = true;
			document.getElementById('corrosion_general1').disabled = true;
			document.getElementById('corrosion_general2').disabled = true;
			document.getElementById('superficie_cil_si').disabled = true;
			document.getElementById('superficie_cil_no').disabled = true;
			document.getElementById('superficie_cil_no').checked = false;
			document.getElementById('estado_prueba').value = 'CONDENADO';
			document.getElementById('motivo_condena'). value = 'Corrosion General';
			inhabilitarFormulario('corrosion_general1','corrosion_general_h');
		}else{
			document.getElementById('prof_corr_gene').value = "";
			
		}
	}
}

function checkCorrosionLocal(valor){
	valorCheck = valor;

	if(valorCheck == 1){
		document.getElementById('info_corrosion_local').style.display = "block";
		document.getElementById('corrosion_local_h').value = "1";
	}else{
		document.getElementById('info_corrosion_local').style.display = "none";
		document.getElementById('corrosion_local_h').value = "2";
	}
}

function validar_espesor_corr_local(valor){
	espesor_minimo_garantizado = valor;
	espesor_cilindro = document.getElementById('espesor_pared2').value;

	if(espesor_cilindro<espesor_minimo_garantizado){
		if(confirm("Con esta opción el cilindro será condenado,¿Desea continuar?")){
			alert("Cilindro Condenado!");
			document.getElementById('espesor_pared2').readOnly = true;
			document.getElementById('espesor_min_gar2').readOnly = true;
			document.getElementById('prof_corr_local2').readOnly = true;
			document.getElementById('corrosion_local1').disabled = true;
			document.getElementById('corrosion_local2').disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
			document.getElementById('motivo_condena'). value = 'Corrosion Local';
			inhabilitarFormulario('corrosion_local1','corrosion_local_h');			
		}else{
			document.getElementById('espesor_pared2').value = "";
			document.getElementById('espesor_min_gar2').value = "";
		}
	}
}

function validar_prof_corr_local(valor){
	profundidad_penetracion = valor;
	espesor_pared = document.getElementById('espesor_pared2').value;
	porcentaje_profundidad = 0;

	porcentaje_profundidad = profundidad_penetracion * 100 / espesor_pared;

	if(porcentaje_profundidad>=20){
		if(confirm("Con esta opción el cilindro será condenado,¿Desea continuar?")){
			alert("Cilindro Condenado!");
			document.getElementById('espesor_pared2').readOnly = true;
			document.getElementById('espesor_min_gar2').readOnly = true;
			document.getElementById('prof_corr_local2').readOnly = true;
			document.getElementById('corrosion_local1').disabled = true;
			document.getElementById('corrosion_local2').disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
			document.getElementById('motivo_condena'). value = 'Corrosion Local';
			inhabilitarFormulario('corrosion_local1','corrosion_local_h');	
		}else{
			document.getElementById('prof_corr_local2').value = "";
			
		}
	}
}

function checkCorrosionLineal(valor){
	valorCheck = valor;

	if(valorCheck == 1){
		document.getElementById('info_corrosion_lineal').style.display = "block";
		document.getElementById('corrosion_lineal_h').value = "1";
	}else{
		document.getElementById('info_corrosion_lineal').style.display = "none";
		document.getElementById('corrosion_lineal_h').value = "2";
	}
}

function validar_long_corr_lin(valor){
	longitud_corrosion = valor;
	diametro_cilindro = document.getElementById('diam_cili_corr_lin').value;

	if(longitud_corrosion<diametro_cilindro){
		if (confirm("Con esta opción el cilindro será condenado,¿Desea continuar?")) {
			document.getElementById('diam_cili_corr_lin').readOnly = true;
			document.getElementById('long_corr_lin').readOnly = true;
			document.getElementById('prof_corr_lin').readOnly = true;
			document.getElementById('espesor_original').readOnly = true;
			document.getElementById('espesor_actual_cor_lin').readOnly = true;
			document.getElementById('espesor_min_gar_corlin').readOnly = true;
			document.getElementById('corrosion_lineal1').disabled = true;
			document.getElementById('corrosion_lineal2').disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
	  		document.getElementById('motivo_condena'). value = 'Corrosion Lineal';
	  		inhabilitarFormulario('corrosion_lineal1','corrosion_lineal_h');
	  	}else{
			document.getElementById('diam_cili_corr_lin').value = "";
			document.getElementById('long_corr_lin').value = "";
		}
	}
}

function validar_prof_corr_lin(valor){
	espesor_original_cilindro = valor;
	profundidad_corrosion_lineal = document.getElementById('prof_corr_lin').value;
	porcentaje_condena = 0;

	porcentaje_condena = profundidad_corrosion_lineal*100/espesor_original_cilindro;

	if(porcentaje_condena>=10){
		if (confirm("Con esta opción el cilindro será condenado,¿Desea continuar?")) {
			document.getElementById('diam_cili_corr_lin').readOnly = true;
			document.getElementById('long_corr_lin').readOnly = true;
			document.getElementById('prof_corr_lin').readOnly = true;
			document.getElementById('espesor_original').readOnly = true;
			document.getElementById('espesor_actual_cor_lin').readOnly = true;
			document.getElementById('espesor_min_gar_corlin').readOnly = true;
			document.getElementById('corrosion_lineal1').disabled = true;
			document.getElementById('corrosion_lineal2').disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
	  		document.getElementById('motivo_condena'). value = 'Corrosion Lineal';
	  		inhabilitarFormulario('corrosion_lineal1','corrosion_lineal_h');
	  	}else{
			document.getElementById('prof_corr_lin').value = "";
			document.getElementById('espesor_original').value = "";
		}
	}
}

function validar_espesor_corr_lin(valor){

	espesor_minimo_garantizado = valor;
	espesor_pared = document.getElementById('espesor_actual_cor_lin').value;

	if(espesor_pared>espesor_minimo_garantizado){
		if (confirm("Con esta opción el cilindro será condenado,¿Desea continuar?")) {
			alert("Cilindro Condenado!");
			document.getElementById('diam_cili_corr_lin').readOnly = true;
			document.getElementById('long_corr_lin').readOnly = true;
			document.getElementById('prof_corr_lin').readOnly = true;
			document.getElementById('espesor_original').readOnly = true;
			document.getElementById('espesor_actual_cor_lin').readOnly = true;
			document.getElementById('espesor_min_gar_corlin').readOnly = true;
			document.getElementById('corrosion_lineal1').disabled = true;
			document.getElementById('corrosion_lineal2').disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
	  		document.getElementById('motivo_condena'). value = 'Corrosion Lineal';
	  		inhabilitarFormulario('corrosion_lineal1','corrosion_lineal_h');
	  	}else{
			document.getElementById('espesor_actual_cor_lin').value = "";
			document.getElementById('espesor_min_gar_corlin').value = "";
		}
	}
}

function checkPicadurasAisladas(valor){
	valorCheck = valor;

	if(valorCheck == 1){
		document.getElementById('info_picaduras_aisladas').style.display = "block";
		document.getElementById('picaduras_aisladas_h').value = "1";
	}else{
		document.getElementById('info_picaduras_aisladas').style.display = "none";
		document.getElementById('picaduras_aisladas_h').value = "2";
	}
}

function validar_espesor_picaduras(valor){

	espesor_minimo_garantizado = valor;
	espesor_pared = document.getElementById("espesor_picaduras").value;

	if(espesor_pared<espesor_minimo_garantizado){
		if (confirm("Con esta opción el cilindro será condenado,¿Desea continuar?")) {
			alert("Cilindro Condenado!");
			document.getElementById('espesor_picaduras').readOnly = true;
			document.getElementById('espesor_min_picaduras').readOnly = true;
			document.getElementById('diametro_picaduras').readOnly = true;
			document.getElementById('picaduras_aisladas1').disabled = true;
			document.getElementById('picaduras_aisladas2').disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
	  		document.getElementById('motivo_condena'). value = 'Picaduras Aisladas';
	  		inhabilitarFormulario('picaduras_aisladas1','picaduras_aisladas_h');
	  	}else{
			document.getElementById('espesor_actual_cor_lin').value = "";
			document.getElementById('espesor_min_gar_corlin').value = "";
		}
	}
}

function validar_diam_picaduras(valor){

	espesor_minimo = document.getElementById("espesor_min_picaduras").value;
	diametro_picaduras = valor;
	tercio = 0;

	tercio = espesor_minimo / 3;

	if(diametro_picaduras>tercio){
		if (confirm("Con esta opción el cilindro será condenado,¿Desea continuar?")) {
			alert("Cilindro Condenado!");
			document.getElementById('espesor_picaduras').readOnly = true;
			document.getElementById('espesor_min_picaduras').readOnly = true;
			document.getElementById('diametro_picaduras').readOnly = true;
			document.getElementById('picaduras_aisladas1').disabled = true;
			document.getElementById('picaduras_aisladas2').disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
	  		document.getElementById('motivo_condena'). value = 'Picaduras Aisladas';
	  		inhabilitarFormulario('picaduras_aisladas1','picaduras_aisladas_h');
	  	}else{
			document.getElementById('diametro_picaduras').value = "";
		}
	}
}

function checkPicadurasMultiples(valor){

	valorCheck = valor;

	if(valorCheck == 1){
		document.getElementById('info_picaduras_multiples').style.display = "block";
		document.getElementById('picaduras_multiples_h').value = "1";
	}else{
		document.getElementById('info_picaduras_multiples').style.display = "none";
		document.getElementById('picaduras_multiples_h').value = "2";
	}
}

function validar_espesor_picaduras_multi(valor){
	espesor_restante = document.getElementById("espesor_restante").value;
	espesor_minimo = valor;
	porcentaje_condena = 0;

	porcentaje_condena = espesor_restante * 100 / espesor_minimo;


	if(porcentaje_condena >= 5){
		if (confirm("Con esta opción el cilindro será condenado,¿Desea continuar?")) {
			alert("Cilindro Condenado!");
			document.getElementById('espesor_restante').readOnly = true;
			document.getElementById('espesor_min_picaduras_multi').readOnly = true;
			document.getElementById('picaduras_multi1').disabled = true;
			document.getElementById('picaduras_multi2').disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
	  		document.getElementById('motivo_condena'). value = 'Picaduras Multiples';
	  		inhabilitarFormulario('picaduras_multi1','picaduras_multiples_h');
	  	}else{
			document.getElementById('espesor_min_picaduras_multi').value = "";
		}
	}
}

function checkCorrosionHendidura(valor){

	valorCheck = valor;

	if(valorCheck == 1){
		document.getElementById('info_corr_hendidura').style.display = "block";
		document.getElementById('corr_hendidura_h').value = "1";
	}else{
		document.getElementById('info_corr_hendidura').style.display = "none";
		document.getElementById('corr_hendidura_h').value = "2";
	}
}

function validar_espesor_hendidura(valor){

	corrosion_hendidura = valor;
	espesor_original = document.getElementById("espesor_hendidura").value;
	porcentaje_condena = 0;

	porcentaje_condena = corrosion_hendidura * 100 / espesor_original;

	if(porcentaje_condena >= 20){
		if (confirm("Con esta opción el cilindro será condenado,¿Desea continuar?")) {
			alert("Cilindro Condenado!");
			document.getElementById('espesor_hendidura').readOnly = true;
			document.getElementById('profundidad_penetracion').readOnly = true;
			document.getElementById('corr_hendidura1').disabled = true;
			document.getElementById('corr_hendidura2').disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
	  		document.getElementById('motivo_condena'). value = 'Corrosion Hendidura';
	  		inhabilitarFormulario('corr_hendidura1','corr_hendidura_h');
	  	}else{
			document.getElementById('corrosion_hendidura').value = "";
		}
	}
}

function inhabilitarFormulario(check, campo){

	habilitar = check;
	llenar = campo;

	document.getElementById("protuberancia1").disabled = true;
	document.getElementById("protuberancia2").disabled = true;
	document.getElementById("abolladuras1").disabled = true;
	document.getElementById("abolladuras2").disabled = true;
	document.getElementById("abombamiento1").disabled = true;
	document.getElementById("abombamiento2").disabled = true;
	document.getElementById("indentacion1").disabled = true;
	document.getElementById("indentacion2").disabled = true;
	document.getElementById("grieta1").disabled = true;
	document.getElementById("grieta2").disabled = true;
	document.getElementById("arco_antorcha1").disabled = true;
	document.getElementById("arco_antorcha2").disabled = true;
	document.getElementById("dano_fuego1").disabled = true;
	document.getElementById("dano_fuego2").disabled = true;
	document.getElementById("tapon_cuello1").disabled = true;
	document.getElementById("tapon_cuello2").disabled = true;
	document.getElementById("defecto_cuello1").disabled = true;
	document.getElementById("defecto_cuello2").disabled = true;
	document.getElementById("estampacion1").disabled = true;
	document.getElementById("estampacion2").disabled = true;
	document.getElementById("marcas1").disabled = true;
	document.getElementById("marcas2").disabled = true;
	document.getElementById("estabilidad1").disabled = true;
	document.getElementById("estabilidad2").disabled = true;
	document.getElementById("corrosion1").disabled = true;
	document.getElementById("corrosion2").disabled = true;
	document.getElementById("corrosion_general1").disabled = true;
	document.getElementById("corrosion_general2").disabled = true;
	document.getElementById("cort_ranu_perfo1").disabled = true;
	document.getElementById("cort_ranu_perfo2").disabled = true;
	document.getElementById("picaduras_aisladas1").disabled = true;
	document.getElementById("picaduras_aisladas2").disabled = true;

	document.getElementById("protuberancia_h").value = 2;
	document.getElementById("abolladuras_h").value = 2;
	document.getElementById("abombamiento_h").value = 2;
	document.getElementById("indentacion_h").value = 2;
	document.getElementById("grieta_h").value = 2;
	document.getElementById("arco_antorcha_h").value = 2;
	document.getElementById("dano_fuego_h").value = 2;
	document.getElementById("tapon_cuello_h").value = 2;
	document.getElementById("defecto_cuello_h").value = 2;
	document.getElementById("estampacion_h").value = 2;
	document.getElementById("marcas_sospechosas_h").value = 2;
	document.getElementById("estabilidad_h").value = 2;
	document.getElementById("corrosion_h").value = 2;
	document.getElementById("corrosion_general_h").value = 2;
	document.getElementById("cort_ranu_perfo_h").value = 2;
	document.getElementById("picaduras_aisladas_h").value = 2;

	document.getElementById("protuberancia2").checked = true;	
	document.getElementById("abolladuras2").checked = true;
	document.getElementById("abombamiento2").checked = true;
	document.getElementById("indentacion2").checked = true;
	document.getElementById("grieta2").checked = true;
	document.getElementById("arco_antorcha2").checked = true;
	document.getElementById("dano_fuego2").checked = true;
	document.getElementById("tapon_cuello2").checked = true;
	document.getElementById("defecto_cuello2").checked = true;
	document.getElementById("estampacion2").checked = true;
	document.getElementById("marcas2").checked = true;
	document.getElementById("estabilidad2").checked = true;
	document.getElementById("corrosion2").checked = true;
	document.getElementById("corrosion_general2").checked = true;
	document.getElementById("cort_ranu_perfo2").checked = true;
	document.getElementById("picaduras_aisladas2").checked = true;


	document.getElementById(habilitar).checked = true;
	document.getElementById(llenar).value = 1;
}

function checkCorrosion(valor){
	valorCheck = valor;

	if(valorCheck == 1){
		document.getElementById('corrosion_h').value = '1';
		document.getElementById('info_corrosion').style.display = "block";
	}else{
		document.getElementById('corrosion_h').value = '2';
		document.getElementById('info_corrosion').style.display = "none";
	}
}

function validarTara(valor){
	tara_actual = document.getElementById('tara_actual').value;
	tara_estampada = valor;
	porcentaje_tara = 0;

	porcentaje_tara = tara_actual*100/tara_estampada;

	if(porcentaje_tara<90){
		if (confirm("Con estos datos el cilindro será condenado,¿Desea continuar?")) {
			alert("Cilindro Condenado!");
			document.getElementById('tara_actual').readOnly = true;
			document.getElementById('tara_estampada').readOnly = true;
			document.getElementById('espesor_res_corr').readOnly = true;
			document.getElementById('espesor_min_corr').readOnly = true;
			document.getElementById('long_corr').readOnly = true;
			document.getElementById('corrosion1').disabled = true;
			document.getElementById('corrosion2').disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
	  		document.getElementById('motivo_condena'). value = 'Corrosion';
	  		inhabilitarFormulario('corrosion1','corrosion_h');
	  	}else{
			document.getElementById('tara_estampada').value = "";
		}
	}
}

function validar_espesor_corrosion(valor){

	espesor_minimo = valor;
	espesor_restante = document.getElementById('espesor_res_corr').value;
	espesor_min = espesor_minimo/3;

	if(espesor_restante<espesor_min){
		if (confirm("Con estos datos el cilindro será condenado,¿Desea continuar?")) {
			alert("Cilindro Condenado!");
			document.getElementById('tara_actual').readOnly = true;
			document.getElementById('tara_estampada').readOnly = true;
			document.getElementById('espesor_res_corr').readOnly = true;
			document.getElementById('espesor_min_corr').readOnly = true;
			document.getElementById('long_corr').readOnly = true;
			document.getElementById('corrosion1').disabled = true;
			document.getElementById('corrosion2').disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
	  		document.getElementById('motivo_condena'). value = 'Corrosion';
	  		inhabilitarFormulario('corrosion1','corrosion_h');
	  	}else{
			document.getElementById('espesor_res_corr').value = "";
			document.getElementById('espesor_min_corr').value = "";
		}
	}
}

function validar_long_corrosion(valor){
	long_corrosion = valor;
	espesor_restante = document.getElementById('espesor_res_corr').value;
	espesor_minimo = document.getElementById('espesor_min_corr').value;
	espesor_min = espesor_minimo/4;
	espesor34 = espesor_min*3;
	espesor_min_mitad = espesor_minimo/2;

	if(long_corrosion >= 76.2){
		if(espesor_restante < espesor34){
			if (confirm("Con estos datos el cilindro será condenado,¿Desea continuar?")) {
				alert("Cilindro Condenado!");
				document.getElementById('tara_actual').readOnly = true;
				document.getElementById('tara_estampada').readOnly = true;
				document.getElementById('espesor_res_corr').readOnly = true;
				document.getElementById('espesor_min_corr').readOnly = true;
				document.getElementById('long_corr').readOnly = true;
				document.getElementById('corrosion1').disabled = true;
				document.getElementById('corrosion2').disabled = true;
				document.getElementById('estado_prueba').value = 'CONDENADO';
		  		document.getElementById('motivo_condena'). value = 'Corrosion';
		  		inhabilitarFormulario('corrosion1','corrosion_h');
		  	}else{
				document.getElementById('long_corr').value = "";
				
			}
		}
	}else{
		if(espesor_restante < espesor_min_mitad){
			if (confirm("Con estos datos el cilindro será condenado,¿Desea continuar?")) {
				alert("Cilindro Condenado!");
				document.getElementById('tara_actual').readOnly = true;
				document.getElementById('tara_estampada').readOnly = true;
				document.getElementById('espesor_res_corr').readOnly = true;
				document.getElementById('espesor_min_corr').readOnly = true;
				document.getElementById('long_corr').readOnly = true;
				document.getElementById('corrosion1').disabled = true;
				document.getElementById('corrosion2').disabled = true;
				document.getElementById('estado_prueba').value = 'CONDENADO';
		  		document.getElementById('motivo_condena'). value = 'Corrosion';
		  		inhabilitarFormulario('corrosion1','corrosion_h');
		  	}else{
				document.getElementById('long_corr').value = "";
				
			}
		}
	}
}

function validar_espesor_restante(valor){

	espesor_restante = valor;

	if(espesor_restante>1.60){
		if (confirm("Con estos datos el cilindro será condenado,¿Desea continuar?")) {
			alert("Cilindro Condenado!");
			document.getElementById('espesor_pared_corr_gene').readOnly = true;
			document.getElementById('prof_picadura_corr_gene').readOnly = true;
			document.getElementById('espesor_orig_corr_gene').readOnly = true;
			document.getElementById('corrosion_general1').disabled = true;
			document.getElementById('corrosion_general2').disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
	  		document.getElementById('motivo_condena'). value = 'Corrosion General';
	  		inhabilitarFormulario('corrosion_general1','corrosion_general_h');
	  	}else{
			document.getElementById('espesor_pared_corr_gene').value = "";
		}
	}
}

function validar_profundidad_corr_gene(valor){

	profundidad_picadura = valor;

	if(profundidad_picadura>1.067){
		if (confirm("Con estos datos el cilindro será condenado,¿Desea continuar?")) {
			alert("Cilindro Condenado!");
			document.getElementById('espesor_pared_corr_gene').readOnly = true;
			document.getElementById('prof_picadura_corr_gene').readOnly = true;
			document.getElementById('espesor_orig_corr_gene').readOnly = true;
			document.getElementById('corrosion_general1').disabled = true;
			document.getElementById('corrosion_general2').disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
	  		document.getElementById('motivo_condena'). value = 'Corrosion General';
	  		inhabilitarFormulario('corrosion_general1','corrosion_general_h');
	  	}else{
			document.getElementById('prof_picadura_corr_gene').value = "";
		}
	}
}

function validarCorrGral(valor){

	espesor_original = valor;
	profundidad_picadura = document.getElementById('prof_picadura_corr_gene').value;
	espesor_final = 0;
	profundidad_picadura = profundidad_picadura*1.5;
	espesor_final = espesor_original - profundidad_picadura;

	if(espesor_final<1.600){
		if (confirm("Con estos datos el cilindro será condenado,¿Desea continuar?")) {
			alert("Cilindro Condenado!");
			document.getElementById('espesor_pared_corr_gene').readOnly = true;
			document.getElementById('prof_picadura_corr_gene').readOnly = true;
			document.getElementById('espesor_orig_corr_gene').readOnly = true;
			document.getElementById('corrosion_general1').disabled = true;
			document.getElementById('corrosion_general2').disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
	  		document.getElementById('motivo_condena'). value = 'Corrosion General';
	  		inhabilitarFormulario('corrosion_general1','corrosion_general_h');
	  	}else{
			document.getElementById('prof_picadura_corr_gene').value = "";
		}
	}
}

function validarEspesorRestPicaduras(valor){

	valor_espesor_restante = valor;

	if(valor_espesor_restante < 1.067){
		
		if (confirm("Con estos datos el cilindro será condenado,¿Desea continuar?")) {
			alert("Cilindro Condenado!");
			document.getElementById('espesor_pared_rest_picaduras').readOnly = true;
			document.getElementById('prof_max_picadura').readOnly = true;
			document.getElementById('espesor_original_picaduras').readOnly = true;
			document.getElementById('picaduras_aisladas1').disabled = true;
			document.getElementById('picaduras_aisladas2').disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
	  		document.getElementById('motivo_condena'). value = 'Picaduras Aisladas';
	  		inhabilitarFormulario('picaduras_aisladas1','picaduras_aisladas_h');
	  	}else{
			document.getElementById('espesor_pared_rest_picaduras').value = "";
		}
	
	}
}

function validarProfPicadura(valor){
	espesor_pared = valor;
	profundidad_picadura = document.getElementById('prof_max_picadura').value;
	resta_prof = espesor_pared-profundidad_picadura;
	espesor_pared13 = espesor_pared/3;

	if(resta_prof < 1.067){

		if (confirm("Con estos datos el cilindro será condenado,¿Desea continuar?")) {
			alert("Cilindro Condenado!");
			document.getElementById('espesor_pared_rest_picaduras').readOnly = true;
			document.getElementById('prof_max_picadura').readOnly = true;
			document.getElementById('espesor_original_picaduras').readOnly = true;
			document.getElementById('picaduras_aisladas1').disabled = true;
			document.getElementById('picaduras_aisladas2').disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
	  		document.getElementById('motivo_condena'). value = 'Picaduras Aisladas';
	  		inhabilitarFormulario('picaduras_aisladas1','picaduras_aisladas_h');
	  	}else{
			document.getElementById('prof_max_picadura').value = "";
			document.getElementById('espesor_original_picaduras').value = "";
		}
	}

	if(profundidad_picadura>espesor_pared13){
		if (confirm("Con estos datos el cilindro será condenado,¿Desea continuar?")) {
			alert("Cilindro Condenado!");
			document.getElementById('espesor_pared_rest_picaduras').readOnly = true;
			document.getElementById('prof_max_picadura').readOnly = true;
			document.getElementById('espesor_original_picaduras').readOnly = true;
			document.getElementById('picaduras_aisladas1').disabled = true;
			document.getElementById('picaduras_aisladas2').disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
	  		document.getElementById('motivo_condena'). value = 'Picaduras Aisladas';
	  		inhabilitarFormulario('picaduras_aisladas1','picaduras_aisladas_h');
	  	}else{
			document.getElementById('prof_max_picadura').value = "";
			document.getElementById('espesor_original_picaduras').value = "";
		}
	}
}

function checkCortRanuPerfo(valor){

	valorCheck = valor;

	if(valorCheck == 1){
		document.getElementById('cort_ranu_perfo_h').value ='1';
		document.getElementById('info_cort_ranu_perfo').style.display = "block";
	}else{
		document.getElementById('cort_ranu_perfo_h').value ='2';
		document.getElementById('info_cort_ranu_perfo').style.display = "none";
	}
}

function validarProfCort(valor){
	profundidad_corte = valor;
	espesor_minimo = document.getElementById('espesor_min_cort').value;
	espesor_minimo = espesor_minimo / 2;

	if(profundidad_corte>espesor_minimo){
		if (confirm("Con estos datos el cilindro será condenado,¿Desea continuar?")) {
			alert("Cilindro Condenado!");
			document.getElementById('espesor_orig_cort').readOnly = true;
			document.getElementById('espesor_min_cort').readOnly = true;
			document.getElementById('prof_max_cort').readOnly = true;
			document.getElementById('longitud_cort').readOnly = true;
			document.getElementById('cort_ranu_perfo1').disabled = true;
			document.getElementById('cort_ranu_perfo2').disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
	  		document.getElementById('motivo_condena'). value = 'Cortes Perforaciones o Ranuras';
	  		inhabilitarFormulario('cort_ranu_perfo1','cort_ranu_perfo_h');
	  	}else{
			document.getElementById('prof_max_cort').value = "";
		}
	}
}

function validarLongCort(valor){
	longitud_corte = valor;

	if(longitud_corte>76.2){
		if (confirm("Con estos datos el cilindro será condenado,¿Desea continuar?")) {
			alert("Cilindro Condenado!");
			document.getElementById('espesor_orig_cort').readOnly = true;
			document.getElementById('espesor_min_cort').readOnly = true;
			document.getElementById('prof_max_cort').readOnly = true;
			document.getElementById('longitud_cort').readOnly = true;
			document.getElementById('cort_ranu_perfo1').disabled = true;
			document.getElementById('cort_ranu_perfo2').disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
	  		document.getElementById('motivo_condena'). value = 'Cortes Perforaciones o Ranuras';
	  		inhabilitarFormulario('cort_ranu_perfo1','cort_ranu_perfo_h');
	  	}else{
			document.getElementById('longitud_cort').value = "";
		}
	}
}

function validarAbombamiento(valor){
	circunferencia_cilindro = valor;
	diametro_cilindro = document.getElementById('diam_norm_cilindro').value;
	porcentaje_condena = circunferencia_cilindro * 100 / diametro_cilindro;

	if(porcentaje_condena>=1){
		if (confirm("Con estos datos el cilindro será condenado,¿Desea continuar?")) {
			alert("Cilindro Condenado!");
			document.getElementById('diam_norm_cilindro').readOnly = true;
			document.getElementById('circu_cilindro').readOnly = true;
			document.getElementById('abombamiento1').disabled = true;
			document.getElementById('abombamiento2').disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
	  		document.getElementById('motivo_condena'). value = 'Abombamiento';
	  		inhabilitarFormulario('abombamiento1','abombamiento_h');
	  	}else{
			document.getElementById('circu_cilindro').value = "";
		} 
	}

}
</script>
<?php 
	 //include footer
	 include("inc/google-analytics.php"); 
}
else
{
	 header("Location:index.php");
}
?>