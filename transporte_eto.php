<?php
date_default_timezone_set("America/Bogota");
require ("libraries/conexion.php");
require ("libraries/seguimiento.php");
session_start();

if(@$_SESSION['logged']== 'yes')
{ 
  	$id_transporte = isset($_REQUEST['id_transporte']) ? $_REQUEST['id_transporte'] : NULL;
    $id_user =$_SESSION['su'];
    $acc = $_SESSION['acc'];
    $id_estado_transporte=1;/////id transporte
    $id_finalizar=1;
    $id_asignar_cilindros = 0; //si el transporte no se guarda, entonces no se pueden agregar cilindros a la CME. En 1 si se permite. Cero por defecto.
    $time = time();
    $user_agent = $_SERVER['HTTP_USER_AGENT'];
    function getPlatform($user_agent) {
   	$plataformas = array(
    	'Windows 10' => 'Windows NT 10.0+',
      	'Windows 8.1' => 'Windows NT 6.3+',
      	'Windows 8' => 'Windows NT 6.2+',
      	'Windows 7' => 'Windows NT 6.1+',
      	'Windows Vista' => 'Windows NT 6.0+',
      	'Windows XP' => 'Windows NT 5.1+',
      	'Windows 2003' => 'Windows NT 5.2+',
      	'Windows' => 'Windows otros',
      	'iPhone' => 'iPhone',
      	'iPad' => 'iPad',
      	'Mac OS X' => '(Mac OS X+)|(CFNetwork+)',
      	'Mac otros' => 'Macintosh',
      	'Android' => 'Android',
      	'BlackBerry' => 'BlackBerry',
      	'Linux' => 'Linux',
   	);
	   foreach($plataformas as $plataforma=>$pattern){
	      if (preg_match('/'.$pattern.'/', $user_agent))
	         return $plataforma;
	   }
	   return 'Otras';
	}

	$SO = getPlatform($user_agent);

	

if(isset($_GET['id_cliente']))
{
	 $id_cliente = $_GET['id_cliente'] ;    

    
	    $consulta  = "INSERT INTO movimiento
	        (id_cliente,fecha,hora,id_user,id_estado_transporte,id_finalizar,id_asignar_cilindros) 
	        VALUES ('".$id_cliente."', '".date("Y/m/d")."', '".date("H:i:s")."','".$id_user."','".$id_estado_transporte."','".$id_finalizar."','".$id_asignar_cilindros."')";
	    $resultado = mysqli_query($con,$consulta) ;
	    if ($resultado == FALSE)
	    {
	     	 echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
	    }
	    else
	    {
	      	$id_transporte1 = mysqli_insert_id($con);
	     	 header('Location: transporte_eto.php?id_transporte='.$id_transporte1);
	    }
	
} 
if(isset($_POST['guardar_cilindro']))
{
	$contador=0;	
	$id_cilindro1 = $_POST['id_cilindro1'] ;

	$consulta  = "SELECT * FROM has_movimiento_cilindro WHERE id_cilindro= $id_cilindro1 AND id_movimiento = $id_transporte ";
  	$resultado = mysqli_query($con,$consulta) ;
  	$linea = mysqli_fetch_array($resultado);
	$id_cilindro2 = isset($linea["id_cilindro"]) ? $linea["id_cilindro"] : NULL;
	if ($id_cilindro1 ==$id_cilindro2) 
    {
      ?>
      <script type="text/javascript">
         alert("Ya existe este Producto en el Inventario");
     </script>
     <?php
    }
    else
    {        	    
	    $consulta = "INSERT INTO has_movimiento_cilindro
	          		(id_movimiento,id_cilindro) 
	          		VALUES ('".$id_transporte."','".$id_cilindro1."')";
	    $resultado = mysqli_query($con,$consulta) ;
	    if ($resultado == FALSE)
	    {
	      echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
	    }
	    else
	    {	
	    	$id_has_movimiento_cilindro = mysqli_insert_id($con);
		    seguimiento(8,$id_cilindro1,$id_user,6,$id_has_movimiento_cilindro);//id_seguimiento,id_cilindro_eto,id_user,id_proceso,id_has_proceso
			$consulta  = "SELECT * FROM has_movimiento_cilindro WHERE id_movimiento= $id_transporte";
		  	$resultado = mysqli_query($con,$consulta) ;
		  	while ($linea = mysqli_fetch_array($resultado))
	        {	
	        	$id_cilindro = $linea["id_cilindro"];
	        }
	        $consulta7= "UPDATE transporte SET 
		                total = '$contador'              
		                WHERE id_transporte = $id_transporte ";
		    $resultado7 = mysqli_query($con,$consulta7) ;

		    if ($resultado7 == FALSE)
		    {
		      echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    }
		    else
		    {
		    	header('Location: transporte_eto.php?id_transporte='.$id_transporte);
		    }		    
		}
	}  
}
if(isset($_POST['guardar_movimiento']))
{
  $id_placa_camion2 = $_POST['id_placa_camion1'] ;
  $num_trans1 = $_POST['num_trans'] ;
  $obs_trasn1 = $_POST['obs_trasn'] ;
  $id_estado_transporte = 2;// Se pueden agregar cilindros

  if(strlen($id_transporte) > 0)
	{
		$enteroCme = intval($num_trans1);

		if($enteroCme == 0){
			?>
			<script type="text/javascript">
				alert("El numero de la CME no es valido");
				js_id_transporte = <?php echo $id_transporte ?>;
				window.location = ("transporte_eto.php?id_transporte="+js_id_transporte);
			</script>
			<?php
		}else{

			$consulta8 = "SELECT * FROM movimiento WHERE num_trans = '$num_trans1'";
			$resultado8 = mysqli_query($con, $consulta8);

			if(mysqli_num_rows($resultado8)>0){
				?>
				<script type="text/javascript">
					alert("Este numero de CME ya fue registrado");;
					js_id_transporte = <?php echo $id_transporte ?>;
					window.location = ("transporte_eto.php?id_transporte="+js_id_transporte);
				</script>
				<?php
			}else{
				$consulta= "UPDATE movimiento SET 
	              	id_placa_camion = '$id_placa_camion2',
	              	num_trans = '$num_trans1',
	              	obs_trasn = '$obs_trasn1' , 
	              	id_estado_transporte = '$id_estado_transporte'           
	              	WHERE id_movimiento = $id_transporte ";
	  		$resultado = mysqli_query($con,$consulta) ;

	  		if ($resultado == FALSE)
	  		{
	    		echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
	  		}
	  		else
	  		{
	    		//header('Location: transporte_eto.php?id_transporte='.$id_transporte);
	    		?>
	  			<script type="text/javascript">
	  				alert("Transporte guardado con éxito. Ahora puede agregar cilindros.")
	  				var js_id_transporte  = '<?php echo $id_transporte;?>';
	  				window.location = ("transporte_eto.php?id_transporte="+js_id_transporte);
	  			</script>
	  			<?php
	  		}
			}
		}
  }  
}

if(strlen($id_transporte) > 0)
{ 
  	$consulta  = "SELECT * FROM movimiento WHERE id_movimiento= $id_transporte";
  	$resultado = mysqli_query($con,$consulta) ;
  	$linea = mysqli_fetch_array($resultado);

  	$id_user1 = isset($linea["id_user"]) ? $linea["id_user"] : NULL;
  	$sql = "SELECT * FROM user WHERE idUser = $id_user1";
    $sql_res = mysqli_query($con,$sql) ;
    while ($sql_row = mysqli_fetch_array($sql_res))
    {
    	$Name = $sql_row["Name"];
    	$LastName = $sql_row["LastName"];		
    	$usuario = $Name." ".$LastName;                                             	
    }mysqli_free_result($sql_res);

  	$id_cliente1 = isset($linea["id_cliente"]) ? $linea["id_cliente"] : NULL;  
	$fecha = isset($linea["fecha"]) ? $linea["fecha"] : NULL;
	$hora = isset($linea["hora"]) ? $linea["hora"] : NULL;
	$num_trans = isset($linea["num_trans"]) ? $linea["num_trans"] : NULL;
	$obs_trasn = isset($linea["obs_trasn"]) ? $linea["obs_trasn"] : NULL;
	$id_tipo_movimiento1 = isset($linea["id_tipo_movimiento"]) ? $linea["id_tipo_movimiento"] : NULL;
	$id_tipo_transporte1 = isset($linea["id_tipo_transporte"]) ? $linea["id_tipo_transporte"] : NULL;
	$repre_cliente = isset($linea["repre_cliente"]) ? $linea["repre_cliente"] : NULL;
	$num_cedula = isset($linea["num_cedula"]) ? $linea["num_cedula"] : NULL;
	$id_placa_camion1 = isset($linea["id_placa_camion"]) ? $linea["id_placa_camion"] : NULL;	
	$id_finalizar = isset($linea["id_finalizar"]) ? $linea["id_finalizar"] : NULL;
	$id_estado_transporte = isset($linea["id_estado_transporte"]) ? $linea["id_estado_transporte"] : NULL;
    mysqli_free_result($resultado);


  	$sql1 = "SELECT * FROM placa_camion WHERE id_placa_camion = $id_placa_camion1";
    $sql_res1 = mysqli_query($con,$sql1) ;
    while ($sql_row1 = mysqli_fetch_array($sql_res1))
    {
    	//$placa = $sql_row1["placa_camion"];
    	$placa = isset($sql_row1["placa_camion"]) ? $sql_row1["placa_camion"] : NULL;
    }mysqli_free_result($sql_res1);

    $consulta1  = "SELECT id_cliente,nombre,nit FROM clientes WHERE id_cliente= $id_cliente1";
  	$resultado1 = mysqli_query($con,$consulta1) ;
  	$linea1 = mysqli_fetch_array($resultado1);
  	$nombre = isset($linea1["nombre"]) ? $linea1["nombre"] : NULL;
  	$nit1 = isset($linea1["nit"]) ? $linea1["nit"] : NULL;  
  	/*
  	$result = mysqli_query($con,"SELECT COUNT(*) AS count FROM has_movimiento_cilindro WHERE id_movimiento = $id_transporte "); 
	$row = mysqli_fetch_array($result,mysqli_ASSOC); 
	$count = $row['count'];
	if ($count>0) 
	{
		 $id_estado_transporte=3;
	}*/

	
}
require_once("inc/init.php");
require_once("inc/config.ui.php");
$page_title = "Control Transporte";
$page_css[] = "your_style.css";
include("inc/header.php");
$page_nav[""][""][""][""] = true;
include("inc/nav.php");
?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
	<div id="content">	
	<?php
	if ($id_transporte=="") 
	{		
	?>		   
		<div class="">	      	
			<h1  class="page-title txt-color-blueDark"> Seleccione el Cliente :</h1>			
		</div>
		<!-- widget grid -->       
		<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">			
			<header>
				<span class="widget-icon"> <i class="fa fa-table"></i> </span>
				<h6>Clientes</h6>
			</header>	
			<div>
				<div class="jarviswidget-editbox"></div>
				<div class="widget-body no-padding">
					<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">					
						<form  method="POST" name="form1">							
							<thead>
								<tr>
									<th>#</th>
									<th>Acción</th>
									<th>Cliente</th>
									<th>Nit</th>
									<th>Telefono</th>
									<th>Celular</th>
									<th>E-Mail</th>
									<th>Direccion</th>
									<th>Observacion</th>
								</tr>
							</thead>
							<tbody>										
							<?php
                           
                    			$contador = "0";
                    	        $consulta = "SELECT * FROM clientes ";
                                $resultado = mysqli_query($con,$consulta) ;
                                while ($linea = mysqli_fetch_array($resultado))
                                {
                                    $contador = $contador + 1;
                                    $id_cliente = $linea["id_cliente"];
                                    $nombre = $linea["nombre"];
                                    $telefono_fijo = $linea["telefono_fijo"];
                                    $telefono_celular = $linea["telefono_celular"];
                                    $correo = $linea["correo"];
                                    $persona_contacto = $linea["persona_contacto"];
                                    $direccion = $linea["direccion"];
                                    $observacion = $linea["observacion"];
                                    $nit = $linea["nit"];
                         
                                ?>
                                    <tr>                                                	
                                        <td width="5"><?php echo $contador; ?></td>
                                        <td width="5"><a href="transporte_eto.php?id_cliente=<?php echo $id_cliente; ?>"><img src="img/chulo.png" width="20" height="20"></a></td>
                                        <td><?php echo $nombre; ?></td>
                                        <td><?php echo $nit; ?></td>
                                        <td><?php echo $telefono_fijo; ?></td>
                                        <td><?php echo $telefono_celular; ?></td>
                                        <td><?php echo $correo; ?></td>
                                        <td><?php echo $direccion; ?></td>
                                        <td><?php echo $observacion; ?></td>
                                	</tr>   
                                <?php
                                }mysqli_free_result($resultado);   
                            	
                            ?>
							</tbody>
						</form>
					</table>
				</div>
			</div>
		</div>
	<?php
	}
	if ($id_transporte>0) 
	{		
	?>
		<section id="widget-grid" class="">
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-6">
					<div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h6>Cliente </h6>				
						</header>
						<div>				
						<div class="jarviswidget-editbox"></div>
							<div class="widget-body no-padding">
							<form id="checkout-form" class="smart-form" novalidate="novalidate" action="transporte_eto.php" method="POST">						
							<input type="hidden" name="id_transporte" id="id_transporte" value="<?php echo $id_transporte; ?>">
								<fieldset>
								<div class="row">
									<section class="col col-6">
										<label class="font-lg">Cliente :</label>
										<label class="input"> 
											<input type="text" class="input-lg" name="nombre" readonly placeholder="Nombre" value="<?php echo isset($nombre) ? $nombre : NULL; ?>">
										</label>
									</section>
									<section class="col col-6">
										<label class="font-lg">CME :</label>
										<label class="input"> 
											<input type="text" class="input-lg" name="num_trans" placeholder="CME" value="<?php echo isset($num_trans) ? $num_trans : NULL; ?>" required>
										</label>
									</section>
								</div>	
								<div class="row">	
									
									<section class="col col-6">           
			                        <?php
			                        $consulta6 ="SELECT * 
			                              FROM placa_camion";
			                        $resultado6 = mysqli_query($con,$consulta6) ;
			                        echo "<section>";
			                        echo "<label class='font-lg'>Placa Camión :</label>";
			                        echo"<label class='select'>";
			                        echo "<select class='input-lg' name='id_placa_camion1' id='id_placa_camion1'>";
			                        echo "<option value=''>Seleccione...</option>";
			                        while($linea6 = mysqli_fetch_array($resultado6))
			                        {
			                          	$id_placa_camion = $linea6['id_placa_camion'];
			                          	$placa_camion = $linea6['placa_camion'];
			                          	if ($id_placa_camion==$id_placa_camion1)
			                          	{
			                            	echo "<option value='$id_placa_camion' selected >$placa_camion</option>"; 
			                          	}
			                          	else 
			                          	{
			                            	echo "<option value='$id_placa_camion'>$placa_camion</option>"; 
			                          	} 
			                        }//fin while 
			                        echo "</select>";
			                        echo "<i></i>";
			                        echo "</label>";
			                        echo "</section>";      
			                        ?>                          
			                      </section> 
			                      <section class="col col-6">
										<label class="font-lg">Fecha :</label>
										<label class="input"> 
											<input type="text" class="input-lg" name="fecha" readonly placeholder="Fecha" value="<?php echo isset($fecha) ? $fecha : NULL; ?>">
										</label>
									</section> 													
								</div>
								<div class="row">
									<section class="col col-4">
										<label class="font-lg">Hora:</label>
										<label class="input">
											<input type="text" name="hora" class="input-lg" placeholder="Hora" value="<?php echo isset($hora) ? $hora : NULL; ?>">	
										</label>
									</section>
									<section class="col col-8">
										<label class="font-lg">Usuario:</label>
										<label class="input">
											<input type="text" name="usuario" class="input-lg" placeholder="usuario" value="<?php echo isset($usuario) ? $usuario : NULL; ?>">	
										</label>
									</section>
								</div>
								<div class="row">	
									<section class="col col-10">
										<label class="font-lg">Observaciones :</label>
										<label class="input"> 
											<textarea class="form-control" rows="6" name="obs_trasn"><?php echo isset($obs_trasn) ? $obs_trasn : NULL; ?></textarea>
										</label>
									</section>																		
								</div>
								</fieldset>														
								<footer>
										<?php
										if($SO=="Android")
										{
											if($id_estado_transporte == 3)
											{	
											?>
												<a href="com.fidelier.printfromweb://
												$biguhw$INGEGAS-INGENIERIA Y GASES LTDA$intro$
												$small$------------------------------------------$intro$
												$small$Fecha   : <?echo date("d-m-Y (H:i:s)", $time); ?>$intro$
												$small$Cliente : <? echo $nombre;?>$intro$
												$small$Nit     : <? echo $nit1;?>$intro$
												$small$Usuario : <?=@$_SESSION['name']?>$intro$
												$small$Obs     : <? echo $obs_trasn;?>$intro$										
												$small$------------------------------------------$intro$
												$big$#  Cilindro$intro$
												$small$------------------------------------------$intro$
												$small$<?php
												          	$contador = "0";	                
												          	$consulta = "SELECT * FROM has_movimiento_cilindro WHERE id_movimiento= $id_transporte ";
												          	$resultado = mysqli_query($con,$consulta) ;
												          	$aux="";
												          	while ($linea = mysqli_fetch_array($resultado))
												          	{
													            $contador = $contador + 1;
													            $id_cilindro = $linea["id_cilindro"];		                           
													            $id_has_logistica_eto = $linea["id_has_logistica_eto"];		                            

													            $consulta1 = "SELECT * FROM cilindro_eto WHERE id_cilindro_eto = $id_cilindro";
													            $resultado1 = mysqli_query($con,$consulta1) ;
													            while ($linea1 = mysqli_fetch_array($resultado1))
													            {
													            	$num_cili_eto = $linea1["num_cili_eto"];
													            }	                            
													            echo $contador.")  ".$num_cili_eto;?>$intro$$small$<?php
															}mysqli_free_result($resultado);	                                            
														?>$intro$
												$small$------------------------------------------$intro$
												$small$Aprobado Por$intro$
												$intro$
												$intro$
												$intro$
												$small$------------------------------------------$intro$
												$small$Sujeto Auditoria Y Verificacion $intro$
												$cut$$intro$$intro$$intro$"><img src="/img/iconos/printer_black.png"></a>
												<?php
											}
										}
										if ($SO!="Android") 
										{
											if($id_estado_transporte == 3)
											{
											?>
												<a href="javascript:imprSelec('muestra')"><img src="/img/iconos/printer_blue.png"></a>
											<?php
											}
										}
										if($id_estado_transporte != 3)	
										{
										?>														
											<input type="submit" value="Guardar" name="guardar_movimiento" id="guardar_movimiento" class="btn btn-primary" />
										<?php
										}				
										if ($id_estado_transporte == 2) 
										{
														
											?>
												<input type="button" onclick="validar_tr()" value="Cerrar Transporte" class="btn btn-danger btn-lg" >
											<?php														
										}											
										?>	
								</footer>	
							</form>	
							</div>						
						</div>					
					</div>				
				</article>
				<?php				
				if ($id_finalizar==1) 
				{
					if($id_estado_transporte == 2)
					{			
						?>
						<article class="col-sm-12 col-md-12 col-lg-6">
							<div class="jarviswidget" id="wid-id-2" data-widget-editbutton="false" data-widget-custombutton="false">
								<header>
									<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
									<h6>Ingreso de Cilindros </h6>				
								</header>
								<div>				
								<div class="jarviswidget-editbox"></div>
									<div class="widget-body no-padding">												
										<input type="hidden" name="id_transporte"  value="<?php echo $id_transporte; ?>">							
										<div class="row">					
											<div class="col-md-6">
												<div class="form-group">
													<br>
													<label class="font-lg">Num cilindro :</label>											 
													<input type="text" class="input-lg" name="num_cili" id="cilindro" placeholder="Num cilindro" value="" />											
												</div>
											</div>							
										</div>															
									</div>
								</div>					
							</div>				
						</article>
					<?php
					}
									
				}											
				?>
				<article class="col-sm-12 col-md-12 col-lg-6">
					<div class="jarviswidget" id="wi6-id-3" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h6>Cilindros Transportados </h6>				
						</header>
						<div>				
						<div class="jarviswidget-editbox"></div>																	
							<table class="table tabl6-bordered">
								<thead>
									<tr>
										<th>#</th>                                                
			                            <th>Cilindro</th>
			                            <th>Tipo Gas</th>
			                            <th>Conformidad</th>
			                            <?php				
										if ($id_finalizar==1) 
										{			
											?>
				                            <th>Eliminar</th>
				                            <?php				
										}											
										?>
									</tr>
								</thead>
								<tbody>													
								  <?php
		                          $contador = "0";	                
		                          $consulta = "SELECT * FROM has_movimiento_cilindro WHERE id_movimiento=".$id_transporte;
		                          $resultado = mysqli_query($con,$consulta) ;
		                          while ($linea = mysqli_fetch_array($resultado))
		                          {
		                            $contador = $contador + 1;
		                            $id_cilindro = $linea["id_cilindro"];
		                            $id_has_movimiento_cilindro = $linea["id_has_movimiento_cilindro"];
		                            $id_movimiento = $linea["id_movimiento"];
		                            $id_conformidad = $linea["id_conformidad"];
		                            if ($id_conformidad == 1)
		                            {
		                            	$conformidad = "<input type=\"image\" src=\"img/mal.png\" width=\"28\" height=\"35\">";
		                            }
		                            else
		                            {
		                            	$conformidad = "<input type=\"image\" src=\"img/bien.png\" width=\"35\" height=\"35\">";
		                            }	

		                            $consulta1 = "SELECT id_cilindro_eto,num_cili_eto,id_tipo_cilindro FROM cilindro_eto WHERE id_cilindro_eto = $id_cilindro";
		                            $resultado1 = mysqli_query($con,$consulta1) ;
		                            while ($linea1 = mysqli_fetch_array($resultado1))
		                            {
		                            	$num_cili_eto = $linea1["num_cili_eto"];
		                            	$id_cilindro_eto = $linea1["id_cilindro_eto"];
		                            	$id_tipo_cilindro = $linea1["id_tipo_cilindro"];
		                            	$consulta3 = "SELECT * FROM tipo_cilindro WHERE id_tipo_cilindro =".$id_tipo_cilindro;
	                                    $resultado3 = mysqli_query($con,$consulta3) ;
	                                    while ($linea3 = mysqli_fetch_array($resultado3))
	                                    {
	                                    	$tipo_cili = $linea3["tipo_cili"];	                                    	
	                                    }mysqli_free_result($resultado3);
		                            }		                            
		                            ?>
		                            <tr class="odd gradeX">
		                              	<td width="40"><?php echo $contador; ?></td>                                  
		                              	<td><?php echo $num_cili_eto; ?></td> 
		                              	<td><?php echo $tipo_cili; ?></td> 
		                              	<td><?php echo $conformidad; ?></td> 
		                              	<?php				
										if ($id_finalizar==1) 
										{			
										?>
		                              	<td width="100" style="vertical-align:bottom;">			                              	
		                              		<a href="eliminar_has_movi_cili.php?id_has_movimiento_cilindro=<?php echo $id_has_movimiento_cilindro; ?>&id_movimiento=<?php echo $id_movimiento; ?>&id_cilindro_eto=<?php echo $id_cilindro_eto; ?>"><input type="image"  src="img/eliminar.png" width="35" height="35"></a>			                              
		                              	</td> 
		                              	<?php				
										}											
										?>                            
		                           	</tr>                           
		                            <?php
		                            }mysqli_free_result($resultado);	                                            
		                  		    ?>
								</tbody>
							</table>												
						</div>					
					</div>		
				</article>				
			</div>
		</section>
		<div id="muestra" style="display:none">
		<p>INGEGAS-INGENIERIA Y GASES LTDA</p>		
		<p><h2 align="center"><? echo $nombre;?></h2>
		<h2 align="center">NIT :<? echo $nit1;?></h2>
		<h2 align="center">CME: <?php echo $num_trans; ?></h2></p>
		<br>
		<h2 align="center">CILINDROS TRANSPORTADOS</h2>
			<table class="table table-bordered" style="border: 1px solid black;border-collapse: collapse;" align="center">	
				<thead>
					<tr>
						<th style="border: 1px solid black;border-collapse: collapse;">#</th>                                                
		                <th style="border: 1px solid black;border-collapse: collapse;">Cilindro</th>			                            
					</tr>
				</thead>
				<tbody>													
					<?php
		          	$contador = "0";	                
		          	$consulta = "SELECT * FROM has_movimiento_cilindro WHERE id_movimiento=".$id_transporte;
		          	$resultado = mysqli_query($con,$consulta) ;
		          	while ($linea = mysqli_fetch_array($resultado))
		          	{
			            $contador = $contador + 1;
			            $id_cilindro = $linea["id_cilindro"];
		    	        $id_has_movimiento_cilindro = $linea["id_has_movimiento_cilindro"];
		        	    $id_movimiento = $linea["id_movimiento"];

			            $consulta1 = "SELECT id_cilindro_eto,num_cili_eto FROM cilindro_eto WHERE id_cilindro_eto = $id_cilindro";
			            $resultado1 = mysqli_query($con,$consulta1) ;
			            while ($linea1 = mysqli_fetch_array($resultado1))
			            {
			            	$num_cili_eto = $linea1["num_cili_eto"];
			            }		                            
			            ?>
			            <tr class="odd gradeX">
			              	<td width="40" style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $contador; ?></td>                                  
			              	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $num_cili_eto; ?></td> 	                      
			           	</tr>                           
			            <?php
		            }mysqli_free_result($resultado);	                                            
		  		    ?>
				</tbody>
			</table>
			<br>
			<P>Observaciones: <?php echo $obs_trasn; ?></P>
			<br>
			<p>Placa del vehículo:<? echo $placa;?></p>
			<p>Transportador: <?php echo $usuario?></p>
			<br>
			<br>
			<h6>Sujeto Auditoria Y Verificacion</h6>
			<p>Fecha elaboración :<?echo date("d-m-Y (H:i:s)", $time); ?></p>			
		</div>
	<?php
	}
	?>
	</div>
	<!-- END MAIN CONTENT -->
</div>
<!-- Modal -->
<div class="modal fade" id="modal_si_no" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">	
	<div class="modal-dialog">
		<form name="mezcla" action="ph.php" method="POST" id="mezcla" >
			<input type="hidden" name="id_cilindro_eto_1"  id="id_cilindro_eto_1" value="">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					&times;
					</button>
					<h4 class="modal-title" id="myModalLabel">¿ El CILINDRO INGRESADO PRESENTA UNA NO CONFORMIDAD ?</h4>
				</div>
				<div class="modal-body">
					<div class="well well-sm well-primary">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="validar('no')" style="width:70px;"">NO</button>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="validar('si')" style="width:70px;"">SI</button>
								</div>
							</div>														
						</div>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</form>
	</div><!-- /.modal -->
	<!-- Fin Modal -->
</div>

<!-- ==========================CONTENT ENDS HERE ========================== -->
<?php
	include("inc/footer.php");
	include("inc/scripts.php"); 
?>
<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>

<!-- Vector Maps Plugin: Vectormap engine, Vectormap language -->
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>

<!-- Full Calendar -->
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/jquery-form/jquery-form.min.js"></script>
<script type="text/javascript">
function imprSelec(muestra)
{
	var ficha=document.getElementById(muestra);
	var ventimp=window.open(' ','popimpr');
	ventimp.document.write(ficha.innerHTML);
	ventimp.document.close();
	ventimp.print();
	ventimp.close();
}
</script>
<script type="text/javascript">
function validar_tr()
{ 
	var r = confirm("¿Esta seguro de finalizar el Transporte?");
	if (r == true) 
	{
		$(document).ready(function()
        {
			var formulario = $("#checkout-form").serialize();
			var id_transporte = <?php echo $id_transporte; ?>;
        
        	$.ajax({
    		url: 'finalizar_trans.php',
	       	type: 'POST',
	       	data: formulario,
	       	})
    		.done(function(resp){    			
    		  	if(resp == 1)
    		  	{
    		  		alert("Transporte finalizado exitosamente. Ahora puede imprimir el ticket.")
    		  		window.location="/transporte_eto.php?id_transporte="+id_transporte;
    		  	}
    		  	else
    		  	{
    		  		if(resp == 2)
    		  		{
    		  			alert("No se puede cerrar el transporte hasta que agregue al menos un cilindro.");    		  	
    		  			window.location="/transporte_eto.php?id_transporte="+id_transporte;
    		  		}
    		  	} 		  				 			       		
		    })
        });
	} 
}
</script>
<script>
	
    var $registerForm = $("#checkout-form").validate({
    // Rules for form validation
        rules : {
            id_placa_camion1 : {
                required : true
            }
        },
        messages : {
            id_placa_camion1 : {
                required : 'Debe seleccionar un elemento de la lista'
            }
        },

        // Do not change code below
        errorPlacement : function(error, element) {
            error.insertAfter(element.parent());
        }
    });
</script>
<script type="text/javascript">

	
	$("#cilindro").keypress(function(e) {
        if(e.which == 13) 
        {        
	    	var num_cili1 = $('input:text[name=num_cili]').val();
	    	$('input:hidden[name=id_cilindro_eto_1]').val(num_cili1);
	    	//var num_cili1 = document.checkout_form.num_cili.value;	    	
			//validar(num_cili1);     
			$("#modal_si_no").modal();   		 
        }
    });
    
    function validar(si_no)
	{	
		var id_transporte = $('input:hidden[name=id_transporte]').val();
		var l = confirm("¿ ESTA SEGURO ?");		
		if (l == true) 
		{
			var dato = $('input:text[name=num_cili]').val();			
			$.ajax({
				url: 'id_cili4.php',
				type: 'POST',
				data: 'dato='+dato+'&dato1='+id_transporte+'&dato2='+si_no,
			})
			.done(function(data) {
				
				var objeto = JSON.parse(data);							
				var id_cili= objeto.id;
				var estado= objeto.estado;						
				if (estado==1) 
				{												
					alert("Comuníquese con el Jefe de Producción para que él cree o revise el estado del cilindro");
					window.location="transporte_eto.php?id_transporte="+id_transporte;
				}
				if(estado==2)
				{				
					alert("Cilindro Recogido Correctamente. Pendiente para revisión en planta");
					window.location="transporte_eto.php?id_transporte="+id_transporte; 
				}
				if (estado==3) 
				{
					alert("Este cilindro se encuentra en uso");	
				}
				if (estado==4) 
				{
					alert("Este cilindro ya se encuentra en este movimiento");	
				}
				if (estado==5)
				{
					alert("Este cilindro no se encuentra asignado a este cliente");
				}
			})
			.fail(function() {
				console.log("error");
			});
		}
		else
		{
			//window.location="transporte_eto.php?id_transporte="+id_transporte;
		}		
		
	}

</script>
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="js/plugin/datatable-responsive/datatables.responsive.min.js"></script>



<script type="text/javascript">



// DO NOT REMOVE : GLOBAL FUNCTIONS!

$(document).ready(function() {
	
	/* // DOM Position key index //
		
	l - Length changing (dropdown)
	f - Filtering input (search)
	t - The Table! (datatable)
	i - Information (records)
	p - Pagination (paging)
	r - pRocessing 
	< and > - div elements
	<"#id" and > - div with an id
	<"class" and > - div with a class
	<"#id.class" and > - div with an id and class
	
	Also see: http://legacy.datatables.net/usage/features
	*/	

	/* BASIC ;*/
		var responsiveHelper_dt_basic = undefined;
		var responsiveHelper_datatable_fixed_column = undefined;
		var responsiveHelper_datatable_col_reorder = undefined;
		var responsiveHelper_datatable_tabletools = undefined;
		
		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};

		$('#dt_basic').dataTable({
			"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
			"autoWidth" : true,
			"preDrawCallback" : function() {
				// Initialize the responsive datatables helper once.
				if (!responsiveHelper_dt_basic) {
					responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
				}
			},
			"rowCallback" : function(nRow) {
				responsiveHelper_dt_basic.createExpandIcon(nRow);
			},
			"drawCallback" : function(oSettings) {
				responsiveHelper_dt_basic.respond();
			}
		});

	/* END BASIC */
	
	/* COLUMN FILTER  */
    var otable = $('#datatable_fixed_column').DataTable({
    	//"bFilter": false,
    	//"bInfo": false,
    	//"bLengthChange": false
    	//"bAutoWidth": false,
    	//"bPaginate": false,
    	//"bStateSave": true // saves sort state using localStorage
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_fixed_column) {
				responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_fixed_column.respond();
		}		
	
    });
    
    // custom toolbar
    $("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');
    	   
    // Apply the filter
    $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {
    	
        otable
            .column( $(this).parent().index()+':visible' )
            .search( this.value )
            .draw();
            
    } );
    /* END COLUMN FILTER */   

	/* COLUMN SHOW - HIDE */
	$('#datatable_col_reorder').dataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_col_reorder) {
				responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_col_reorder.respond();
		}			
	});
	
	/* END COLUMN SHOW - HIDE */

	/* TABLETOOLS */
	$('#datatable_tabletools').dataTable({
		
		// Tabletools options: 
		//   https://datatables.net/extensions/tabletools/button_options
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        "oTableTools": {
        	 "aButtons": [
             "copy",
             "csv",
             "xls",
                {
                    "sExtends": "pdf",
                    "sTitle": "SmartAdmin_PDF",
                    "sPdfMessage": "SmartAdmin PDF Export",
                    "sPdfSize": "letter"
                },
             	{
                	"sExtends": "print",
                	"sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>"
            	}
             ],
            "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
        },
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_tabletools) {
				responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_tabletools.respond();
		}
	});
	
	/* END TABLETOOLS */

})

</script>

<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script type="text/javascript">

 
		
		$(document).ready(function() {
			
			// menu
			$("#menu").menu();
		
			/*
			 * AUTO COMPLETE AJAX
			 */
		
			function log(message) {
				$("<div>").text(message).prependTo("#log");
				$("#log").scrollTop(0);
			}
		
			$("#city").autocomplete({
				source : function(request, response) {
					$.ajax({
						url : "http://ws.geonames.org/searchJSON",
						dataType : "jsonp",
						data : {
							featureClass : "P",
							style : "full",
							maxRows : 12,
							name_startsWith : request.term
						},
						success : function(data) {
							response($.map(data.geonames, function(item) {
								return {
									label : item.name + (item.adminName1 ? ", " + item.adminName1 : "") + ", " + item.countryName,
									value : item.name
								}
							}));
						}
					});
				},
				minLength : 2,
				select : function(event, ui) {
					log(ui.item ? "Selected: " + ui.item.label : "Nothing selected, input was " + this.value);
				}
			});
		
			/*
			 * Spinners
			 */
			$("#spinner").spinner();
			$("#spinner-decimal").spinner({
				step : 0.01,
				numberFormat : "n"
			});
		
			$("#spinner-currency").spinner({
				min : 5,
				max : 2500,
				step : 25,
				start : 1000,
				numberFormat : "C"
			});
		
			/*
			 * CONVERT DIALOG TITLE TO HTML
			 * REF: http://stackoverflow.com/questions/14488774/using-html-in-a-dialogs-title-in-jquery-ui-1-10
			 */
			$.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
				_title : function(title) {
					if (!this.options.title) {
						title.html("&#160;");
					} else {
						title.html(this.options.title);
					}
				}
			}));
		
		
			/*
			* DIALOG SIMPLE
			*/
		
			// Dialog click
			$('#dialog_link').click(function() {
				$('#dialog_simple').dialog('open');
				return false;
		
			});
		
			$('#dialog_simple').dialog({
				autoOpen : false,
				width : 600,
				resizable : false,
				modal : true,
				title : "<div class='widget-header'><h4><i class='fa fa-warning'></i> Empty the recycle bin?</h4></div>",
				buttons : [{
					html : "<i class='fa fa-trash-o'></i>&nbsp; Delete all items",
					"class" : "btn btn-danger",
					click : function() {
						$(this).dialog("close");
					}
				}, {
					html : "<i class='fa fa-times'></i>&nbsp; Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
					}
				}]
			});
		
			/*
			* DIALOG HEADER ICON
			*/
		
			// Modal Link
			$('#modal_link').click(function() {
				$('#dialog-message').dialog('open');
				return false;
			});
		
			$("#dialog-message").dialog({
				autoOpen : false,
				modal : true,
				title : "<div class='widget-header'><h4><i class='icon-ok'></i> jQuery UI Dialog</h4></div>",
				buttons : [{
					html : "Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
					}
				}]
		
			});
		
			/*
			 * Remove focus from buttons
			 */
			$('.ui-dialog :button').blur();
		
			/*
			 * Just Tabs
			 */
		
			$('#tabs').tabs();
		
			/*
			 *  Simple tabs adding and removing
			 */
		
			$('#tabs2').tabs();
		
			// Dynamic tabs
			var tabTitle = $("#tab_title"), tabContent = $("#tab_content"), tabTemplate = "<li style='position:relative;'> <span class='air air-top-left delete-tab' style='top:7px; left:7px;'><button class='btn btn-xs font-xs btn-default hover-transparent'><i class='fa fa-times'></i></button></span></span><a href='#{href}'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; #{label}</a></li>", tabCounter = 2;
		
			var tabs = $("#tabs2").tabs();
		
			// modal dialog init: custom buttons and a "close" callback reseting the form inside
			var dialog = $("#addtab").dialog({
				autoOpen : false,
				width : 600,
				resizable : false,
				modal : true,
				buttons : [{
					html : "<i class='fa fa-times'></i>&nbsp; Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
		
					}
				}]
			});
		
			// addTab form: calls addTab function on submit and closes the dialog
			var form = dialog.find("form").submit(function(event) {
				addTab();
				dialog.dialog("close");
				
			});
		
			// actual addTab function: adds new tab using the input from the form above

			
			function addTab() 
			{
				
				var label = tabTitle.val() || "Tab " + tabCounter, id = "tabs-" + tabCounter, li = $(tabTemplate.replace(/#\{href\}/g, "#" + id).replace(/#\{label\}/g, label)), tabContentHtml = tabContent.val() || "Tab " + tabCounter + " content.";
		
				tabs.find(".ui-tabs-nav").append(li);
				tabs.append("<div id='" + id + "'><p>" + tabContentHtml + "</p></div>");
				tabs.tabs("refresh");
				tabCounter++;
		
				// clear fields
				$("#tab_title").val("");
				$("#tab_content").val("");
			}
		
			// addTab button: just opens the dialog
			$("#add_tab").button().click(function() {
				var idd = document.getElementById('num').firstChild.nodeValue;

				
				dialog.dialog("open");
			});
		
			// close icon: removing the tab on click
					
			/*
			* ACCORDION
			*/
			//jquery accordion
			
		     var accordionIcons = {
		         header: "fa fa-plus",    // custom icon class
		         activeHeader: "fa fa-minus" // custom icon class
		     };
		     
			$("#accordion").accordion({
				autoHeight : false,
				heightStyle : "content",
				collapsible : true,
				animate : 300,
				icons: accordionIcons,
				header : "h4",
			})
		
			/*
			 * PROGRESS BAR
			 */
			$("#progressbar").progressbar({
		     	value: 25,
		     	create: function( event, ui ) {
		     		$(this).removeClass("ui-corner-all").addClass('progress').find(">:first-child").removeClass("ui-corner-left").addClass('progress-bar progress-bar-success');
				}
			});			

		})

	</script>		







<script type="text/javascript">

	$(document).ready(function() 
	{

		var $checkoutForm = $('#checkout-form').validate(
		{
		// Rules for form validation
			rules : 
			 {
				nombre : 
				{
					required : true
				},
				nit : 
				{
					required : true
				},
				telefono_fijo : 
				{
					required : true				
				},
		        correo : 
		        {
		          required : true,
		          email : true
		          
		        },
		        direccion : 
		        {
		          required : true
		          
		        }
			},

			// Messages for form validation
			messages : {
				nombre : {
					required : 'Ingrese el nombre de la empresa'
				},
				nit : {
					required : 'Ingrese el NIT de la empresa'
				},
		        telefono_fijo : {
		          required : 'Ingrese un numero de telefono fijo'
		        },
				correo : {
					required : 'Ingrese un correo Electronico',
					email : 'Ingrese un correo Electronico valido'
				}
				,
				direccion : {
					required : 'Ingrese una direccion'
				}
			},

			// Do not change code below
			errorPlacement : function(error, element) {
				error.insertAfter(element.parent());
			}
		});
		
		
		
				
	})

</script>


<?php 
	//include footer
	include("inc/google-analytics.php"); 
?>
<?php

}
else
{
    header("Location:index.php");
}
?>	    	