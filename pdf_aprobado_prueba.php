<?php
// William Aguirre Carpeta
// PDF HOJA DE RUTA INGEGAS
header('Content-Type: text/html; charset=UTF-8');
set_time_limit(0);
session_start();
date_default_timezone_set("America/Bogota");
require ("libraries/conexion.php");
require('libraries/fpdf/fpdf.php');
$fecha = date("Y-m-d");
$hora = date("G:i:s",time());

////////////////VARIABLES POST///////////////


	$logotipo_empresa = "img/logo-ingegas.png";
	$id_datos_prueba_pev = isset($_REQUEST['id_datos_prueba_pev']) ? $_REQUEST['id_datos_prueba_pev'] : NULL;
	$num_cili1 = isset($_REQUEST['num_cili']) ? $_REQUEST['num_cili'] : NULL;

	$nombre = "";
	$nit = "";
	$direccion = "";
	$telefono_fijo = "";
	$tabla_inspeccion = "inspeccion_alu";
	$campo_inspeccion = "estado_aluminio";

	$consulta1 = "SELECT * FROM datos_prueba_pev WHERE id_datos_prueba_pev = '$id_datos_prueba_pev'";
	$resultado1 = mysqli_query($con,$consulta1);
	if(mysqli_num_rows($resultado1) > 0){

		$linea1 = mysqli_fetch_assoc($resultado1);

		$id_has_movimiento_cilindro_pev = isset($linea1["id_has_movimiento_cilindro_pev"]) ? $linea1["id_has_movimiento_cilindro_pev"] : NULL;
		$expansion_total_1 = isset($linea1["expansion_total_1"]) ? $linea1["expansion_total_1"] : NULL;
		$expansion_total_2 = isset($linea1["expansion_total_2"]) ? $linea1["expansion_total_2"] : NULL;
		$expansion_total_3 = isset($linea1["expansion_total_3"]) ? $linea1["expansion_total_3"] : NULL;
		$porcentaje_prueba = isset($linea1["porcentaje_prueba"]) ? $linea1["porcentaje_prueba"] : NULL;
		$idUser = isset($linea1["idUser"]) ? $linea1["idUser"] : NULL;
		$fecha_hora = isset($linea1["fecha_hora"]) ? $linea1["fecha_hora"] : NULL;

		$fecha_prueba = substr($fecha_hora, -19, 10);

		$consulta2 = "SELECT id_transporte_pev, num_cili, id_tipo_gas_pev, material_cilindro_u FROM has_movimiento_cilindro_pev WHERE id_has_movimiento_cilindro_pev = $id_has_movimiento_cilindro_pev";
		$resultado2 = mysqli_query($con,$consulta2);

		if(mysqli_num_rows($resultado2) > 0){

			$linea2 = mysqli_fetch_assoc($resultado2);

			$id_transporte_pev = isset($linea2["id_transporte_pev"]) ? $linea2["id_transporte_pev"] : NULL;
			$num_cili = isset($linea2["num_cili"]) ? $linea2["num_cili"] : NULL;
			$id_tipo_gas_pev = isset($linea2["id_tipo_gas_pev"]) ? $linea2["id_tipo_gas_pev"] : NULL;
			$material_cilindro_u = isset($linea2["material_cilindro_u"]) ? $linea2["material_cilindro_u"] : NULL;

			$consulta3 = "SELECT id_cliente FROM transporte_pev WHERE id_transporte_pev = $id_transporte_pev";
			$resultado3 = mysqli_query($con,$consulta3);

			if(mysqli_num_rows($resultado3) > 0){

				$linea3 = mysqli_fetch_assoc($resultado3);

				$id_cliente = isset($linea3["id_cliente"]) ? $linea3["id_cliente"] : NULL;

				$consulta4 = "SELECT nombre, nit, direccion, telefono_fijo FROM clientes WHERE id_cliente = $id_cliente";
				$resultado4 = mysqli_query($con,$consulta4);

				if(mysqli_num_rows($resultado4) > 0){

					$linea4 = mysqli_fetch_assoc($resultado4);

					$nombre = isset($linea4["nombre"]) ? $linea4["nombre"] : NULL;
					$nit = isset($linea4["nit"]) ? $linea4["nit"] : NULL;
					$direccion = isset($linea4["direccion"]) ? $linea4["direccion"] : NULL;
					$telefono_fijo = isset($linea4["telefono_fijo"]) ? $linea4["telefono_fijo"] : NULL;
				}
			}

			$consulta5 = "SELECT nombre FROM tipo_gas_pev WHERE id_tipo_gas_pev = $id_tipo_gas_pev";
			$resultado5 = mysqli_query($con,$consulta5);

			if(mysqli_num_rows($resultado5) > 0){

				$linea5 = mysqli_fetch_assoc($resultado5);

				$nombre_gas = isset($linea5["nombre"]) ? $linea5["nombre"] : NULL;
			}

			$consulta6 = "SELECT id_especi_pev, especi_pev, pre_tra_pev, pre_pru_pev, fecha_fab_pev, fecha_ult_pev, volumen_pev, tara_esta_pev, tara_actu_pev, tipo, fech_crea FROM cilindro_pev WHERE num_cili_pev = '$num_cili'";
			$resultado6 = mysqli_query($con,$consulta6);

			if(mysqli_num_rows($resultado6) > 0){

				$linea6 = mysqli_fetch_assoc($resultado6);

				$id_especi_eto = isset($linea6["id_especi_pev"]) ? $linea6["id_especi_pev"] : NULL;
				$especi_eto = isset($linea6["especi_pev"]) ? $linea6["especi_pev"] : NULL;
				$pre_tra_eto_psi = isset($linea6["pre_tra_pev"]) ? $linea6["pre_tra_pev"] : NULL;
				$pre_pru_eto_psi = isset($linea6["pre_pru_pev"]) ? $linea6["pre_pru_pev"] : NULL;
				$fecha_fab_eto = isset($linea6["fecha_fab_pev"]) ? $linea6["fecha_fab_pev"] : NULL;
				$fecha_ult_eto = isset($linea6["fecha_ult_pev"]) ? $linea6["fecha_ult_pev"] : NULL;
				$volumen_eto = isset($linea6["volumen_pev"]) ? $linea6["volumen_pev"] : NULL;
				$tara_esta_eto = isset($linea6["tara_esta_pev"]) ? $linea6["tara_esta_pev"] : NULL;
				$tara_actu_eto = isset($linea6["tara_actu_pev"]) ? $linea6["tara_actu_pev"] : NULL;
				$tipo = isset($linea6["tipo"]) ? $linea6["tipo"] : NULL;
				$fech_crea = isset($linea6["fech_crea"]) ? $linea6["fech_crea"] : NULL;

				$pre_tra_eto_mpa = $pre_tra_eto_psi/145.038;
				$pre_pru_eto_mpa = $pre_pru_eto_psi/145.038;

				$pre_tra_eto_mpa = round($pre_tra_eto_mpa, 5);
				$pre_pru_eto_mpa = round($pre_pru_eto_mpa, 5);

				$consulta7 = "SELECT especificacion FROM especificacion WHERE id_especificacion = $id_especi_eto";
				$resultado7 = mysqli_query($con,$consulta7);

				if(mysqli_num_rows($resultado7) > 0){

					$linea7 = mysqli_fetch_assoc($resultado7);

					$especificacion = isset($linea7["especificacion"]) ? $linea7["especificacion"] : NULL;
				}
			}

			$consulta8 = "SELECT nombre FROM material_cilindro WHERE id_material = '$material_cilindro_u'";
			$resultado8 = mysqli_query($con,$consulta8);

			if(mysqli_num_rows($resultado8) > 0){

				$linea8 = mysqli_fetch_assoc($resultado8);

				$nombre_material = isset($linea8["nombre"]) ? $linea8["nombre"] : NULL;
			}

			if($material_cilindro_u == 1){
				$tabla_inspeccion = 'inspeccion_ace';
				$campo_inspeccion = 'estado_acero';
			}else if($material_cilindro_u == 2){
				$tabla_inspeccion = 'inspeccion_alu';
				$campo_inspeccion = 'estado_aluminio';
			}

			$consulta9 = "SELECT $campo_inspeccion FROM $tabla_inspeccion WHERE id_has_movimiento_cilindro_pev = $id_has_movimiento_cilindro_pev";
			$resultado9 = mysqli_query($con,$consulta9);

			if(mysqli_num_rows($resultado9) > 0){

				$linea9 = mysqli_fetch_assoc($resultado9);

				$resultado_insp_visual = isset($linea9[$campo_inspeccion]) ? $linea9[$campo_inspeccion] : NULL;


			}
		}
	}
	

	





/**
*  Clase para la maquetación de Factura
*/
class PDF extends FPDF
{
	function _Arc($x1, $y1, $x2, $y2, $x3, $y3) //Curvatura del rectángulo
	{
		$h = $this->h;
		$this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
							$x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
	}
	// private functions
	function RoundedRect($x, $y, $w, $h, $r, $style = '') //Rectángulo con bordes redondeados 
	{
		$k = $this->k;
		$hp = $this->h;
		if($style=='F')
			$op='f';
		elseif($style=='FD' || $style=='DF')
			$op='B';
		else
			$op='S';
		$MyArc = 4/3 * (sqrt(2) - 1);
		$this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));
		$xc = $x+$w-$r ;
		$yc = $y+$r;
		$this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));

		$this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);
		$xc = $x+$w-$r ;
		$yc = $y+$h-$r;
		$this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
		$this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);
		$xc = $x+$r ;
		$yc = $y+$h-$r;
		$this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
		$this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);
		$xc = $x+$r ;
		$yc = $y+$r;
		$this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
		$this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
		$this->_out($op);
	}
	// public functions
	function sizeOfText( $texte, $largeur )
	{
		$index    = 0;
		$nb_lines = 0;
		$loop     = TRUE;
		while ( $loop )
		{
			$pos = strpos($texte, "\n");
			if (!$pos)
			{
				$loop  = FALSE;
				$ligne = $texte;
			}
			else
			{
				$ligne  = substr( $texte, $index, $pos);
				$texte = substr( $texte, $pos+1 );
			}
			$length = floor( $this->GetStringWidth( $ligne ) );
			$res = 1 + floor( $length / $largeur) ;
			$nb_lines += $res;
		}
		return $nb_lines;
	}
	//ENCABEZADO DE PÁGINA
	function Header()
	{
		$x1 = 140;
		$y1 = 6;
		//Variables de encabezado
		global $logotipo_empresa, $nombre_empresa;
		global $reviso, $aprobo,$fecha_reviso,$fecha_aprobo,$codigo,$version,$pagina;

	    $this->SetFont('Arial','B',9);
		$this->SetTextColor(0,0,0);
		$this->SetLineWidth(0);	
		//.....Logo de la compañia		
		$this->Image($logotipo_empresa,30,19,14,22);
		$this->SetXY(60,18);
		
				
		
		$this->SetXY(108,18);
		$this->Cell(100,8, 'INGENIERIA Y GASES LTDA "INGEGAS"', 0,0,'C');
		$this->SetXY(135,26);
		$this->Cell(45,8, 'Calle 163 # 19 A 32 PBX: (1) 6706126 FAX: (1) 6772951', 0,1,'C');
		$this->SetXY(23,40);
		$this->Cell(30,5,'INGEGAS LTDA',0,0,'C');
		$this->SetFont('Arial','B',12);
		$this->SetXY(138,36);
		$this->Cell(40,8,'INFORME DE INSPECCION Y PRUEBA DE CILINDROS',0,0,'C');
		$this->SetFont('Arial', 'B', 7);
		$this->SetXY(138,39);
		$this->Cell(40,8,'IPC-PL-03 (V8)',0,0,'C');
		$this->SetXY(138,41);
		$this->Cell(40,8,utf8_decode('REVISÓ: G.G. 04/12/2014 - APROBÓ: 04/12/2014'),0,0,'C');
		
		
	}
	//PIE DE PÁGINA
	function Footer()
	{
		$this->SetFont('Arial','B',8);
		$this->SetXY(17,175);
		$this->Cell(28,5,'CONVENCIONES:',0,1);
		$this->SetXY(17,180);
		$this->Cell(32,5,utf8_decode('INSPECCIÓN VISUAL:'),0,1);
		$this->SetXY(17,185);
		$this->Cell(20,5,'RESULTADO:',0,1);
		$this->SetFont('Arial','',7);
		$this->SetXY(49,180);
		$this->Cell(120,5,utf8_decode('OK- Indica que el cilindro ha sido sometido a inspección visual interna y externa,y NO presenta defectos'),0,1);
		$this->SetXY(37,185);
		$this->Cell(120,5,utf8_decode('APROBADO: El resultado de la prueba de expansión volumétrica es menor al 10% de la expansión total'),0,1);
		$this->SetXY(37,190);
		$this->Cell(120,5,utf8_decode('RECHAZADO: El resultado de la prueba de expansión volumétrica supera el 10% de la expansión total'),0,1);

	}
}
$pdf = new PDF( 'L', 'mm', 'A4');
$pdf->SetAutoPageBreak(TRUE,40);
$pdf->AliasNbPages();
$pdf->AddPage();

//CONTENIDO DEL CUERPO DE LA HOJA DE RUTA
		
		
		$pdf->SetXY(60,53);
		$pdf->Cell(30,5,"Normas:",0,1);
		$pdf->SetXY(60,61);
		$pdf->Cell(30,5,"Metodo de Ensayo:",0,1);
		
		$pdf->SetXY(90,53);
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(30,5,'NTC 5171:2013 (Num. 5)/5136:2009 (Num. 4,5,6)',0,1);
		$pdf->SetXY(90,61);
		$pdf->Cell(30,5,utf8_decode('INSPECCIÓN VISUAL INTERNA Y EXTERNA Y PRUEBA DE EXPANSIÓN VOLUMÉTRICA EN CAMISA DE AGUA'),0,1);

		$pdf->SetFont('Arial','B',8);
		$pdf->SetXY(25,70);
		$pdf->Cell(30,5,'FECHA INFORME:',0,1);
		$pdf->SetXY(55,70);
		$pdf->Cell(40,5,$fecha,0,1);
		$pdf->SetXY(73,70);
		$pdf->Cell(167,5,'',0,1);
		$pdf->SetXY(240,70);
		$pdf->Cell(40,5,'No. LPH 1865',0,1);
		$pdf->SetXY(240,75);
		$pdf->Cell(40,5,'PAGINA 1 DE 1',0,1);

		$pdf->SetXY(25,78);
		$pdf->Cell(30,5,'CLIENTE:',0,1);
		$pdf->SetXY(25,83);
		$pdf->Cell(30,5,'NIT:',0,1);
		$pdf->SetXY(25,88);
		$pdf->Cell(30,5,'DIRECCION:',0,1);
		$pdf->SetXY(125,88);
		$pdf->Cell(30,5,'TELEFONO:',0,1);

		$pdf->SetFont('Arial','',8);
		$pdf->SetXY(55,78);
		$pdf->Cell(30,5,$nombre,0,1);
		$pdf->SetXY(55,83);
		$pdf->Cell(30,5,$nit,0,1);
		$pdf->SetXY(55,88);
		$pdf->Cell(30,5,$direccion,0,1);
		$pdf->SetXY(155,88);
		$pdf->Cell(30,5,$telefono_fijo,0,1);

		$pdf->SetFont('Arial','B',6);
		$pdf->setFillColor(250,231,115);
		$pdf->SetXY(15,100);
		$pdf->Cell(14,15,'',1,0,'C',1);
		$pdf->SetXY(15,100);
		$pdf->Cell(14,5,'FECHA',0,0,1);
		$pdf->SetXY(15,105);
		$pdf->Cell(14,5,'RECEPCION',0,0,1);
		$pdf->SetXY(15,110);
		$pdf->Cell(14,5,'dd/mm/aa',0,0,1);
		$pdf->SetXY(15,115);
		$pdf->Cell(14,5,$fech_crea,1,1,'C');

		$pdf->SetXY(29,100);
		$pdf->Cell(13,15,'',1,0,'C',1);
		$pdf->SetXY(29,100);
		$pdf->Cell(13,5,'FECHA',0,0);
		$pdf->SetXY(29,105);
		$pdf->Cell(13,5,'PRUEBA',0,0);
		$pdf->SetXY(29,110);
		$pdf->Cell(13,5,'dd/mm/aa',0,0);
		$pdf->SetXY(29,115);
		$pdf->Cell(13,5,$fecha_prueba,1,1);

		$pdf->SetXY(42,100);
		$pdf->Cell(11.5,15,'',1,1,'C',1);
		$pdf->SetXY(42,103);
		$pdf->Cell(11.5,5,'CILINDRO',0,1);
		$pdf->SetXY(42,107);
		$pdf->Cell(11.5,5,utf8_decode('NÚMERO'),0,1);
		$pdf->SetXY(42,115);
		$pdf->Cell(11.5,5,$num_cili,1,1);

		$pdf->SetXY(53.5,100);
		$pdf->Cell(12.5,15,'',1,1,'C',1);
		$pdf->SetXY(53.5,105);
		$pdf->Cell(12.5,5,'GAS',0,1,'C');
		$pdf->SetFont('Arial','B',5);		
		$pdf->SetXY(53.5,115);
		$pdf->Cell(12.5,5,$nombre_gas,1,1,'C');

		$pdf->SetFont('Arial','B',6);
		$pdf->SetXY(66,100);
		$pdf->Cell(18,15,'',1,1,'C',1);
		$pdf->SetXY(66,105);
		$pdf->Cell(18,5,utf8_decode('ESPECIFICACIÓN'),0,1,'C');
		$pdf->SetXY(66,115);
		$pdf->Cell(18,5,$especificacion.'('.$especi_eto.')',1,1,'C');

		
		$pdf->SetXY(84,100);
		$pdf->Cell(52,5,utf8_decode('PRESIÓN'),1,1,'C',1);
		$pdf->SetXY(84,105);
		$pdf->Cell(13,5,'Trabajo',1,1,'C',1);
		$pdf->SetXY(97,105);
		$pdf->Cell(13,5,'Prueba',1,1,'C',1);
		$pdf->SetXY(110,105);
		$pdf->Cell(13,5,'Trabajo',1,1,'C',1);
		$pdf->SetXY(123,105);
		$pdf->Cell(13,5,'Prueba',1,1,'C',1);
		$pdf->SetXY(84,110);
		$pdf->Cell(13,5,'Mpa',1,1,'C',1);
		$pdf->SetXY(97,110);
		$pdf->Cell(13,5,'Mpa',1,1,'C',1);
		$pdf->SetXY(110,110);
		$pdf->Cell(13,5,'Psi',1,1,'C',1);
		$pdf->SetXY(123,110);
		$pdf->Cell(13,5,'Psi',1,1,'C',1);
		$pdf->SetXY(84,115);
		$pdf->Cell(13,5,$pre_tra_eto_mpa,1,1,'C');
		$pdf->SetXY(97,115);
		$pdf->Cell(13,5,$pre_pru_eto_mpa,1,1,'C');
		$pdf->SetXY(110,115);
		$pdf->Cell(13,5,$pre_tra_eto_psi,1,1,'C');
		$pdf->SetXY(123,115);
		$pdf->Cell(13,5,$pre_pru_eto_psi,1,1,'C');


		$pdf->SetXY(136,100);
		$pdf->Cell(26,5,'FECHAS',1,1,'C',1);
		$pdf->SetXY(136,105);
		$pdf->Cell(13,5,utf8_decode('F/cación'),1,1,'C',1);
		$pdf->SetXY(149,105);
		$pdf->Cell(13,5,'Ult. Rev.',1,1,'C',1);
		$pdf->SetXY(136,110);
		$pdf->Cell(13,5,'dd/mm/aa',1,1,'C',1);
		$pdf->SetXY(149,110);
		$pdf->Cell(13,5,'dd/mm/aa',1,1,'C',1);
		$pdf->SetXY(136,115);
		$pdf->Cell(13,5,$fecha_fab_eto,1,1,'C');
		$pdf->SetXY(149,115);
		$pdf->Cell(13,5,$fecha_ult_eto,1,1,'C');



		$pdf->SetXY(162,100);
		$pdf->Cell(13,5,'VOL',1,1,'C',1);
		$pdf->SetXY(162,105);
		$pdf->Cell(13,5,'Litros',1,1,'C',1);
		$pdf->SetXY(162,110);
		$pdf->Cell(13,5,'',1,1,'C',1);
		$pdf->SetXY(162,115);
		$pdf->Cell(13,5,$volumen_eto,1,1,'C');

		$pdf->SetXY(175,100);
		$pdf->Cell(26,5,'TARA',1,1,'C',1);
		$pdf->SetXY(175,105);
		$pdf->Cell(13,5,utf8_decode('F/cación'),1,1,'C',1);
		$pdf->SetXY(188,105);
		$pdf->Cell(13,5,'Actual',1,1,'C',1);
		$pdf->SetXY(175,110);
		$pdf->Cell(13,5,'Kg',1,1,'C',1);
		$pdf->SetXY(188,110);
		$pdf->Cell(13,5,'Kg',1,1,'C',1);
		$pdf->SetXY(175,115);
		$pdf->Cell(13,5,$tara_esta_eto,1,1,'C');
		$pdf->SetXY(188,115);
		$pdf->Cell(13,5,$tara_actu_eto,1,1,'C');

		$pdf->SetXY(201,100);
		$pdf->Cell(14,5,'INSPECC.',1,1,'C',1);
		$pdf->SetXY(201,105);
		$pdf->Cell(14,5,'VISUAL',1,1,'C',1);
		$pdf->SetXY(201,110);
		$pdf->Cell(14,5,'',1,1,'C',1);
		$pdf->SetFont('Arial','B',5);
		$pdf->SetXY(201,115);
		$pdf->Cell(14,5,$resultado_insp_visual,1,1,'C');
		

		$pdf->SetFont('Arial','B',6);
		$pdf->SetXY(215,100);
		$pdf->Cell(39,5,utf8_decode('EXPANS VOLUMÉTRICA'),1,1,'C',1);
		$pdf->SetXY(215,105);
		$pdf->Cell(12,5,'Total',1,1,'C',1);
		$pdf->SetXY(227,105);
		$pdf->Cell(26,5,'Perman.',1,1,'C',1);
		$pdf->SetXY(215,110);
		$pdf->Cell(12,5,'gr',1,1,'C',1);
		$pdf->SetXY(227,110);
		$pdf->Cell(13,5,'gr',1,1,'C',1);
		$pdf->SetXY(240,110);
		$pdf->Cell(13,5,'%',1,1,'C',1);
		$pdf->SetXY(215,115);
		$pdf->Cell(12,5,$expansion_total_2,1,1,'C');
		$pdf->SetXY(227,115);
		$pdf->Cell(13,5,$expansion_total_3,1,1,'C');
		$pdf->SetXY(240,115);
		$pdf->Cell(13,5,$porcentaje_prueba,1,1,'C');

		$pdf->SetXY(253,100);
		$pdf->Cell(13,15,'',1,1,'C',1);
		$pdf->SetXY(253,102);
		$pdf->Cell(13,5,'MATERIAL',0,0,'C');
		$pdf->SetXY(253,105);
		$pdf->Cell(13,5,'DEL',0,0,'C');
		$pdf->SetXY(253,108);
		$pdf->Cell(13,5,'CILINDRO',0,0,'C');
		$pdf->SetXY(253,115);
		$pdf->Cell(13,5,$nombre_material,1,1,'C');
		

		$pdf->SetXY(266,100);
		$pdf->Cell(14,15,'RESULTADO',1,1,'C',1);
		$pdf->SetXY(266,115);
		$pdf->Cell(14,5,'APROBADO',1,1,'C');

		$pdf->SetFont('Arial','B',8);
		$pdf->SetXY(25,125);
		$pdf->Cell(28,5,'TOTAL CILINDROS: ',0,1);
		$pdf->SetXY(53,124);
		$pdf->Cell(22,5,'','B',1);

		$pdf->SetXY(77,130);
		$pdf->Cell(150,5,utf8_decode('La expansión volumétrica permanente respecto a la expansión total debe ser menor al 10% para ser aprobado'),0,1);

		$pdf->SetXY(40,145);
		$pdf->Cell(16,5,'FIRMADO:',0,1);
		$pdf->SetXY(56,144);
		$pdf->Cell(40,5,'','B',1);
		$pdf->SetXY(56,149);
		$pdf->Cell(40,5,'DIRECTO TECNICO',0,1,'C');
		$pdf->SetXY(56,152);
		$pdf->Cell(40,5,'ING. NELSON FORERO',0,1,'C');

		$pdf->SetXY(205,145);
		$pdf->Cell(16,5,'FIRMADO',0,1);
		$pdf->SetXY(221,144);
		$pdf->Cell(40,5,'','B',1);
		$pdf->SetXY(221,149);
		$pdf->Cell(40,5,'OPERARIO INSP. Y ENSAYO',0,1,'C');
		$pdf->SetXY(221,152);
		$pdf->Cell(40,5,'JORGE IVAN AYA',0,1,'C');

		$pdf->SetXY(17,164);
		$pdf->Cell(260,5,'',0,1);
		$pdf->SetXY(17,164);
		$pdf->Cell(11,5,'NOTA:',0,1);
		$pdf->SetFont('Arial','',7);
		$pdf->SetXY(28,164);
		$pdf->Cell(244,5,utf8_decode('La información aquí consignada corresponde únicamente a los ítems ensayados y es de carácter confidencial, por tal motivo su posible reproducción debe ser autorizada por el Laboratorio que lo expide solicitud del interesado'),0,1);

		

//Datos de archivo de descarga
$file_name = str_replace(' ','_',$nombre_empresa_ajustado)."_Factura_No._".$num_factura.".pdf";
$file_destination = $path_pdf.$file_name;
$pdf->Output($file_destination,'F');
$pdf->Output($file_name,'I');
?>