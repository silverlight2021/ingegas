<?php

date_default_timezone_set("America/Bogota");

require ("libraries/conexion.php");
session_start();
if(@$_SESSION['logged']== 'yes')
{ 
     $acc = $_SESSION['acc'];     


$idUser = isset($_REQUEST['idUser']) ? $_REQUEST['idUser'] : NULL;

if(isset($_POST['guardar_usuario']))
{
	$Name = $_POST['Name'] ;
	$LastName = $_POST['LastName'] ;
	$telephone1 = $_POST['telephone1'] ;
	$telephone11 = $_POST['telephone11'] ;
	$cellphone1 = $_POST['cellphone1'] ;
	$address1 = $_POST['address1'] ;
	
	if(strlen($idUser) > 0)
	{
		$consulta  = "UPDATE user
					SET Name = '".$Name."', LastName = '".$LastName."', telephone1 = ".$telephone1.", telephone11 = '".$telephone11."', cellphone1 = ".$cellphone1.", address1 = '".$address1."' 
					WHERE idUser = ".$idUser;
		$resultado = mysqli_query($con,$consulta) ;
		if ($resultado == FALSE)
		{
			echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";	
		}
		else
		{
			header('Location: busqueda_usuario.php');
		}
	}
	else
	{
		$consulta  = "INSERT INTO user
					(Name, LastName, telephone1, cellphone1, address1) 
					VALUES ('".$Name."', '".$LastName."','".$telephone1."','".$cellphone1."', '".$address1."')";
		$resultado = mysqli_query($con,$consulta) ;
		if ($resultado == FALSE)
		{
			echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";	
		}
		else
		{
			$idUser = mysqli_insert_id($con);
			$consulta2  = "INSERT INTO user_has_profile
					(User_idUser) 
					VALUES ('".$idUser."')";
			$resultado2 = mysqli_query($con,$consulta2) ;
			?>
			<script type="text/javascript">
				alert("Usuario Creado Correctamente");
			</script>
			<?php
		}
	}
}
if(isset($_POST['perfil_usuario']))
{
	$idprofile = $_POST['idprofile1'] ;
	$UserLogin = $_POST['UserLogin'] ;
	$PassLogin = $_POST['PassLogin'] ;
	$id_estado_usuario2 = $_POST['id_estado_usuario1'] ;	

	$consulta ="UPDATE user_has_profile	SET 
				User_idUser = '".$idUser."',
				profile_idprofile = '".$idprofile."', 
				UserLogin = '".$UserLogin."', 
				PassLogin = '".md5($PassLogin)."', 
				state = '".$id_estado_usuario2." '
				WHERE User_idUser = ".$idUser;
	$resultado = mysqli_query($con,$consulta) ;
	if ($resultado == FALSE)
	{
		echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";	
	}
	else
	{
		header('Location: busqueda_usuario.php');
	}
		
}


if(strlen($idUser) > 0)
{	
	$consulta  = "SELECT * FROM user WHERE idUser = ".$idUser;
	$resultado = mysqli_query($con,$consulta) ;
	$linea = mysqli_fetch_array($resultado);
	$Name = isset($linea["Name"]) ? $linea["Name"] : NULL;
	if(strlen($Name) > 0)
	{
		$LastName = isset($linea["LastName"]) ? $linea["LastName"] : NULL;
		$telephone1 = isset($linea["telephone1"]) ? $linea["telephone1"] : NULL;
		$telephone11 = isset($linea["telephone11"]) ? $linea["telephone11"] : NULL;
		$cellphone1 = isset($linea["cellphone1"]) ? $linea["cellphone1"] : NULL;
		$address1 = isset($linea["address1"]) ? $linea["address1"] : NULL;
	}
	mysqli_free_result($resultado);
}
if(strlen($idUser) > 0)
{	
	$consulta1  = "SELECT * FROM user_has_profile WHERE User_idUser = ".$idUser;
	$resultado1 = mysqli_query($con,$consulta1) ;
	$linea1 = mysqli_fetch_array($resultado1);
	$idprofile1 = isset($linea1["profile_idprofile"]) ? $linea1["profile_idprofile"] : NULL;
	$UserLogin = isset($linea1["UserLogin"]) ? $linea1["UserLogin"] : NULL;
	//$PassLogin = isset($linea1["PassLogin"]) ? $linea1["PassLogin"] : NULL;
	$id_estado_usuario1 = isset($linea1["state"]) ? $linea1["state"] : NULL;
	
}        
 
require_once("inc/init.php");
require_once("inc/config.ui.php");
$page_title = "Usuario";
$page_css[] = "your_style.css";
include("inc/header.php");
$page_nav[""][""][""][""] = true;
include("inc/nav.php");

?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
	<?php
		include("inc/ribbon.php");
	?>
<style type="text/css">
  h2 {display:inline}
</style>
	<div id="content">
        <section id="widget-grid" class="">
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-6">
					<div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h2>Usuario </h2>
						</header>
						<div>
							<div class="jarviswidget-editbox"></div>
							<div class="widget-body no-padding">								
								<form id="checkout-form" class="smart-form" novalidate="novalidate" action="usuario.php" method="POST">
								<input type="hidden" name="idUser" id="idUser" value="<?php echo $idUser; ?>">
									<fieldset>
										<div class="row">
											<section class="col col-6">
												<label class="label">Nombres :</label>
												<label class="input"> 
													<input type="text" name="Name" placeholder="Nombres" value="<?php echo isset($Name) ? $Name : NULL; ?>">
												</label>
											</section>
											<section class="col col-6">
												<label class="label">Apellidos :</label>
												<label class="input"> 
													<input type="text" name="LastName" placeholder="Apellidos" value="<?php echo isset($LastName) ? $LastName : NULL; ?>">
												</label>
											</section>
										</div>
										</fieldset>
										<fieldset>
										<div class="row">
											<section class="col col-6">
												<label class="label">Teléfono :</label>
												<label class="input"> 
													<input type="text" name="telephone1" placeholder="Telefono" value="<?php echo isset($telephone1) ? $telephone1 : NULL; ?>">
												</label>
											</section>
					                        <section class="col col-6">
						                        <label class="label">Celular :</label>
						                        <label class="input"> 
						                          <input type="text" name="cellphone1" placeholder="Celular" value="<?php echo isset($cellphone1) ? $cellphone1 : NULL; ?>">
						                        </label>
					                        </section>									
										</div>
										</fieldset>
										<fieldset>
										<div class="row">
											<section class="col col-6">
												<label class="label">Correo Electrónico :</label>
												<label class="input"> <i class="icon-append fa fa-lock"></i>
													<input type="email" name="address1" title="Ingrese un correo Electrónico valido" placeholder="Correo Electrónico" value="<?php echo isset($address1) ? $address1 : NULL; ?>">
												</label>
											</section>
					                    </div>
									</fieldset>
									<?php
									if (in_array(10, $acc))
									{
									?>
										<footer>										
											<input type="submit" name="guardar_usuario" id="guardar_usuario" class="btn btn-primary" />
										</footer>
									<?php
									}										
									?>
								</form>
							</div>
							<!-- end widget content -->							
						</div>
						<!-- end widget div -->						
					</div>		
				</article>
			<?php
			if(strlen($idUser) > 0)
			{
			?>
				<article class="col-sm-12 col-md-12 col-lg-6">
					<div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
								<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
								<h2>Perfil Usuario </h2>
						</header>
					<div>
						<div class="jarviswidget-editbox"></div>
							<div class="widget-body no-padding">
							<form id="checkout-form-2" class="smart-form" novalidate="novalidate" action="usuario.php" method="POST">
								<input type="hidden" name="idUser" id="idUser" value="<?php echo $idUser; ?>">
									<fieldset>
										<div class="row">
											<section class="col col-6">	
												<?php
												$consulta6 ="SELECT idprofile, profile FROM profile ORDER BY profile ASC";
												$resultado6 = mysqli_query($con,$consulta6) ;
												echo "<section>";
												echo "<label class='label'>Perfil :</label>";
												echo"<label class='select'>";
												echo "<select name='idprofile1'>";
												echo "<option value='0'>Seleccione...</option>";
												while($linea6 = mysqli_fetch_array($resultado6))
												{
													$idprofile = $linea6['idprofile'];
													$profile = $linea6['profile'];
													if ($idprofile==$idprofile1)
													{
															echo "<option value='$idprofile' selected >$profile</option>"; 
													}
													else 
													{
															echo "<option value='$idprofile'>$profile</option>"; 
													} 
												}//fin while 
												echo "</select>";
												echo "<i></i>";
												echo "</label>";
												echo "</section>";															
												?>
											</section>
											<section class="col col-6">
												<label class="label">Usuario :</label>
												<label class="input"> 
													<input type="text" name="UserLogin"  placeholder="Login Usuario" value="<?php echo isset($UserLogin) ? $UserLogin : NULL; ?>">
												</label>
											</section>										
										</div>
									</fieldset>
									<fieldset>
										<div class="row">
											<section class="col col-6">
						                        <label class="label">Contraseña :</label>
						                        <label class="input"> 
						                          <input type="password" name="PassLogin" placeholder="Contraseña" value="<?php echo isset($PassLogin) ? $PassLogin : NULL; ?>">
						                        </label>
					                        </section>
					                        <section class="col col-6">	
												<?php
												$consulta6 ="SELECT * FROM estado_usuario ORDER BY estado ASC";
												$resultado6 = mysqli_query($con,$consulta6) ;
												echo "<section>";
												echo "<label class='label'>Estado :</label>";
												echo"<label class='select'>";
												echo "<select name='id_estado_usuario1'>";
												echo "<option value='0'>Seleccione...</option>";
												while($linea6 = mysqli_fetch_array($resultado6))
												{
													$id_estado_usuario = $linea6['id_estado_usuario'];
													$estado = $linea6['estado'];
													if ($id_estado_usuario==$id_estado_usuario1)
													{
															echo "<option value='$id_estado_usuario' selected >$estado</option>"; 
													}
													else 
													{
															echo "<option value='$id_estado_usuario'>$estado</option>"; 
													} 
												}//fin while 
												echo "</select>";
												echo "<i></i>";
												echo "</label>";
												echo "</section>";															
												?>
											</section>									
										</div>
									</fieldset>
									<?php
									if (in_array(10, $acc))
									{
									?>
										<footer>										
											<input type="submit" value="Guardar" name="perfil_usuario" id="perfil_usuario" value="Add Profile"  class="btn btn-primary" />
										</footer>
									<?php
									}										
									?>
								</form>
							</div>
							<!-- end widget content -->							
						</div>
						<!-- end widget div -->						
					</div>
				</article>
			<?php
			}
			?>
			</div>
			<!-- END ROW -->
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-6">
					<div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h2>Cargue de Firmas</h2>
						</header>
					<div>
					<div class="jarviswidget-editbox"></div>
					<div class="widget-body no-padding">
						<?php
						$consulta7 = "SELECT * FROM firmasCertificados WHERE idUser = $idUser";
						$resultado7 = mysqli_query($con, $consulta7);

						if(mysqli_num_rows($resultado7)==0){
							?>
							<form id="checkout-form-7" class="smart-form" novalidate="novalidate" action="guardadoFirmas.php" method="POST" enctype="multipart/form-data">
								<input type="hidden" name="idUser" value="<?php echo $idUser; ?>">
								<fieldset>
									<div class="row">
										<section class="col col-6">
											<label class="label">Archivo de Firma:</label>
											<label class="input"> <i class="icon-append fa fa-camera"></i>
												<input type="file" name="archivo_firma" >
											</label>
										</section>
									</div>
									<?php
									if (in_array(6, $acc))
									{
									?>									
									<footer>										
										<input type="submit" value="Enviar" name="g_firma" id="g_firma" class="btn btn-primary" />
									</footer>
									<?php
									}										
									?>
								</fieldset>
							</form>
							<?php
						}else{
							?>
							<form id="checkout-form-7" class="smart-form" novalidate="novalidate" action="guardadoFirmas.php" method="POST" enctype="multipart/form-data">
								<input type="hidden" name="idUser" value="<?php echo $idUser; ?>">
								<fieldset>
									<div class="row">
										<section class="col col-6">
											<section>
												<label class="label">Modulos a Firmar:</label>
												<label class="select">
													<select name="modulo_firma">
														<option value="0">Seleccione...</option>
														<?php

														$consulta8 = "SELECT * FROM firmasCertificados WHERE eto = 1";
														$resultado8 = mysqli_query($con, $consulta8);

														if(mysqli_num_rows($resultado8)!=2){
															$linea7 = mysqli_fetch_array($resultado7);
															$eto = $linea7["eto"];
															if($eto == 0){
																?>
																<option value="1">ETO</option>
																<?php
															}
															
														}

														$consulta8 = "SELECT * FROM firmasCertificados WHERE pev = 1";
														$resultado8 = mysqli_query($con, $consulta8);

														if(mysqli_num_rows($resultado8)!=2){
															$pev = $linea7["pev"];
															if($pev == 0){
																?>
																<option value="2">PEV</option>
																<?php
															}
															
														}
														?>
														
													</select>
													<i></i>
												</label>
											</section>
											
											</label>
										</section>
									</div>
									<?php
									if (in_array(6, $acc))
									{
									?>									
									<footer>										
										<input type="submit" value="Enviar" name="ga_firma" id="ga_firma" class="btn btn-primary" />
									</footer>
									<?php
									}										
									?>
								</fieldset>
							</form>
							<?php
						}
						?>
						
					</div>
				</article>
			</div>
		</section>
		<!-- end widget grid -->
	</div>
	<!-- END MAIN CONTENT -->
</div>
<!-- END MAIN PANEL -->

<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->
<?php
	include("inc/footer.php");
?>
<!-- END PAGE FOOTER -->

<?php 
	//include required scripts
	include("inc/scripts.php"); 
?>
<script>

	$(document).ready(function() {
		// PAGE RELATED SCRIPTS
	})

</script>

<!-- PAGE RELATED PLUGIN(S) 
<script src="..."></script>-->
<!-- Flot Chart Plugin: Flot Engine, Flot Resizer, Flot Tooltip -->
<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>

<!-- Vector Maps Plugin: Vectormap engine, Vectormap language -->
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>

<!-- Full Calendar -->
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script type="text/javascript">

  $(document).ready(function() 
  {

    var $checkoutForm = $('#checkout-form').validate(
    {
    // Rules for form validation
      rules :
      {
        Name : {
          required : true
        },
        LastName : {
          required : true
        },
        telephone1 : {
          required : true
        },
        address1 : {
          required : true
        }
      },

      // Messages for form validation
      messages : {
        Name : {
          required : 'Por favor ingrese los Nombres'
        },
        LastName : {
          required : 'Por favor ingrese los Apellidos'
        },
        telephone1 : {
          required : 'Por favor ingrese un numero de telefono'
        },
        address1 : {
          required : 'Ingrese un correo Electrónico valido'
        }
      },

      // Do not change code below
      errorPlacement : function(error, element) {
        error.insertAfter(element.parent());
      }
    }); 

    }) 
   

</script>
<script type="text/javascript">

  $(document).ready(function() 
  {

    var $checkoutForm = $('#checkout-form-2').validate(
    {
    // Rules for form validation
      rules :
      {
        idprofile1 : {
          required : true
        },
        UserLogin : {
          required : true
        },
        PassLogin : {
          required : true
        },
        id_estado_usuario1 : {
          required : true
        }
      },

      // Messages for form validation
      messages : {
        idprofile1 : {
          required : 'Por favor seleccione un Perfil'
        },
        UserLogin : {
          required : 'Por favor ingrese un Nombre de Usuario'
        },
        PassLogin : {
          required : 'Por favor ingrese una Contraseña'
        },
        id_estado_usuario1 : {
          required : 'Por favor Seleccione un estado'
        }
      },

      // Do not change code below
      errorPlacement : function(error, element) {
        error.insertAfter(element.parent());
      }
    }); 

    }) 
   

</script>



<?php 
	//include footer
	include("inc/google-analytics.php"); 
?>
<?php

}
else
{
    header("Location:index.php");
}
?>