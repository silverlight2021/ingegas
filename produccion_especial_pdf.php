<?php
require ("libraries/conexion.php");
date_default_timezone_set("America/Bogota");

$id_orden_especial = isset($_REQUEST['id_orden_especial']) ? $_REQUEST['id_orden_especial'] : NULL;
//$id_orden=22;
$consulta  = "SELECT * FROM has_orden_especial_cilindro WHERE id_orden_especial= $id_orden_especial";
$resultado = mysqli_query($con,$consulta) ;

$id_tipo_cilindro1 = "";

if(mysqli_num_rows($resultado)>1){
	while ($linea = mysqli_fetch_array($resultado)) {
		
		$id_tipo_cilindro = isset($linea["id_tipo_cilindro"]) ? $linea["id_tipo_cilindro"] : NULL;
		$id_tipo_cilindro1 = $id_tipo_cilindro1.$id_tipo_cilindro.";";
	}
}

$linea = mysqli_fetch_array($resultado);
$id_tipo_cilindro = isset($linea["id_tipo_cilindro"]) ? $linea["id_tipo_cilindro"] : NULL;
$id_tipo_envace = isset($linea["id_tipo_envace"]) ? $linea["id_tipo_envace"] : NULL;

$consulta1 = "SELECT * FROM tipo_cilindro WHERE id_tipo_cilindro =".$id_tipo_cilindro;
$resultado1 = mysqli_query($con,$consulta1) ;
$linea1 = mysqli_fetch_array($resultado1);
$tipo_cili = isset($linea1["tipo_cili"]) ? $linea1["tipo_cili"] : NULL;
$obs_cili = isset($linea1["obs_cili"]) ? $linea1["obs_cili"] : NULL;



$consulta2 = "SELECT * FROM tipo_envace WHERE id_tipo_envace =".$id_tipo_envace;
$resultado2 = mysqli_query($con,$consulta2) ;
$linea2 = mysqli_fetch_array($resultado2);
$tipo1 = isset($linea2["tipo"]) ? $linea2["tipo"] : NULL;

$consulta3  = "SELECT * FROM produccion_especiales WHERE id_orden_especial= $id_orden_especial";
$resultado3 = mysqli_query($con,$consulta3) ;
$linea3 = mysqli_fetch_array($resultado3);
$fech_crea = isset($linea3["fecha_creacion"]) ? $linea3["fecha_creacion"] : NULL;
$lote_co = "";
$id_lote_eto = "";
$lote_pro = "";

$consulta4  = "SELECT * FROM ordenes_especiales WHERE id_orden_especial= $id_orden_especial";
$resultado4 = mysqli_query($con,$consulta4) ;
$linea4 = mysqli_fetch_array($resultado4);
$num_ord = isset($linea4["num_ord"]) ? $linea4["num_ord"] : NULL;
$fecha_ven = isset($linea4["fecha_ven"]) ? $linea4["fecha_ven"] : NULL;

$consulta67 = "SELECT * FROM firmas WHERE id_formas = 1";
$resultado67 = mysqli_query($con,$consulta67) ;
$linea67 = mysqli_fetch_array($resultado67);
$firmas = isset($linea67["firmas"]) ? $linea67["firmas"] : NULL;
$cargo = isset($linea67["cargo"]) ? $linea67["cargo"] : NULL;

$consulta6 = "SELECT * FROM firmas WHERE id_formas = 2";
$resultado6 = mysqli_query($con,$consulta6) ;
$linea6 = mysqli_fetch_array($resultado6);
$firmas1 = isset($linea6["firmas"]) ? $linea6["firmas"] : NULL;
$cargo1 = isset($linea6["cargo"]) ? $linea6["cargo"] : NULL;
$firma_1 = $firmas."-".$cargo;
$firma_2 = $firmas1."-".$cargo1;

mysqli_free_result($resultado6);
mysqli_free_result($resultado4);
mysqli_free_result($resultado3);
mysqli_free_result($resultado2);
mysqli_free_result($resultado1);	
mysqli_free_result($resultado);

if ($tipo_cili=="ETO-10") 
{
	$valor= 10;
	$valvula= 3;
	$tipo="K";
}
if ($tipo_cili=="ETO-20") 
{
	$valor= 20;
	$valvula= 3;
	$tipo="K";
}
if ($tipo_cili=="ETO-90") 
{
	$valor= 90;
	$valvula= 2;

	if ($tipo1=="25") 
	{
		$tipo="K";
	}
	if ($tipo1=="35") 
	{
		$tipo="G";
	}

	//$tipo="K";
}
if ($tipo_cili=="ETO-100") 
{
	$valor= 100;
	$valvula= 1;
	$tipo="G";
}

$valor= 100;
$valvula= 1;
$tipo="G";

$tip_cone = "----";

$consulta77 = "SELECT * FROM manometro_co2 ";
$resultado77 = mysqli_query($con,$consulta77) ;
$linea77 = mysqli_fetch_array($resultado77);
$manometro_co2 = isset($linea77["manometro_co2"]) ? $linea77["manometro_co2"] : NULL;

$consulta88 = "SELECT * FROM manometro_eto ";
$resultado88 = mysqli_query($con,$consulta88) ;
$linea88 = mysqli_fetch_array($resultado88);
$manometro_eto = isset($linea88["manometro_eto"]) ? $linea88["manometro_eto"] : NULL;

$consulta88 = "SELECT * FROM incertidumbre ";
$resultado88 = mysqli_query($con,$consulta88) ;
$linea88 = mysqli_fetch_array($resultado88);
$incertidumbre = isset($linea88["incertidumbre"]) ? $linea88["incertidumbre"] : NULL;




$expo="";
//$id_orden=14;
$consulta33 = "SELECT *  
			FROM produccion_especiales  
			WHERE id_orden_especial = ".$id_orden_especial;
$resultado33 = mysqli_query($con,$consulta33) ;
while ($linea33 = mysqli_fetch_array($resultado33))
{
	$filas += 1;	
	$id_cilindro_eto = $linea33["id_cilindro"];
	$peso_esperado = $linea33["peso_esperado"];
	$pre_final = $linea33["pre_final"];
	$tara_vacio = $linea33["tara_vacio"];
	$desviacion = $linea33["desviacion"];

	$expo=$expo+(pow($desviacion,2));

}
mysqli_free_result($resultado33);

$raiz1= sqrt($expo);
$total_raiz=100-$raiz1-$manometro_co2-$manometro_eto-$incertidumbre."%";


$restante = 100-$valor;
$oxi_eti = $valor."%";
$restante_car = $restante."%";


$path_imagenes = "img/ingegas_pdf.png";
$path_pdf = "Uploads/pdf/certificado/";
 @$num_factura	 = $consecutivo;
$nombre_empresa = utf8_decode("INGEGAS");
$nombre_empresa_ajustado = utf8_decode(substr($nombre_empresa, 0, 47));

$nombre_empresa_1 = utf8_decode("INGENIERIA Y GASES LTDA");
$nombre_empresa_ajustado_1 = utf8_decode(substr($nombre_empresa_1, 0, 47));

@$titulo="DATOS DEL ENVIO Y RECEPCION DE CILINDROS";
@$Registro_Sanitario='REGISTRO SANITARIO No.'.$obs_cili;
@$tipoCilindros='TIPO DE CILINDROS'." ".$tipo_cili;
@$numero="Número";
@$codigo_producto=$tipo_cili;
@$numero_lote=$num_ord;
@$tipo_cili1=$tipo;
@$con_val=$tip_cone;
@$oficina="Calle 163 a N° 19 a - 48 TEL : 670 61 26 FAX :677 29 51 ";
@$bogota="Bogotá D.C - Colombia";
@$llenado="GRAVIMETRÍA";
@$firma1="Hola Hola";
@$firma2="Hola Hola";

require('funcion_pdf.php');

$pdf = new PDF_Invoice( 'P', 'mm', 'A4' );
$pdf->AddPage();
$pdf->Image($path_imagenes,3,3,0,0,'');
$pdf->fact_dev1($nombre_empresa_ajustado);
$pdf->fact_dev2($nombre_empresa_ajustado_1);
$pdf->fact_dev3($titulo);
//$pdf->fact_dev4($Registro_Sanitario);
$pdf->fact_dev5($tipoCilindros);
$pdf->fact_dev88($num_ord);
$pdf->fact_dev99($lote_pro);
//$pdf->fact_dev10($tipo_cili1);
//$pdf->fact_dev11($con_val);
$pdf->fech_llenado($fech_crea);
//$pdf->metodo_llenado($llenado);
$pdf->fecha_vencimiento($fecha_ven);
//$pdf->fact_dev16($oxi_eti);
//$pdf->fact_dev17($restante_car);
//$pdf->fact_dev18($num_cilindro_eto);
//$pdf->fact_dev19($num_cilindro_co2);
//$pdf->oficina($oficina,$bogota);
//$pdf->firma($firma_1);
//$pdf->firma1($firma_2);

//$pdf->tab_mezcla($mezcla_eto, number_format($total_raiz,2,".",","));
//$pdf->compuestos($pureza_eto,$pureza_co2,$humedad_eto,$humedad_co2);


$cols=array( "Numero Cilindro"    => 50,
             "Capacidad Cilindro" => 45,
             "TARA KGS"           => 50,
             "PRESION"           => 50);
$pdf->addCols( $cols);
$cols1=array( "Numero Cilindro"   => "c",
             "Capacidad CiRindro" => "L",
             "TARA KGS"           => "C",
             "PRESION"           => "C");
$pdf->addLineFormat( $cols1);
$filas = 0;
$y    = 65;


$num_cili="Numero Cilindro";
$consulta3 = "SELECT *  
				FROM produccion_especiales  
				WHERE id_orden_especial = ".$id_orden_especial;
$resultado3 = mysqli_query($con,$consulta3) ;
while ($linea3 = mysqli_fetch_array($resultado3))
{
	$filas += 1;	
	$id_cilindro_eto = $linea3["id_cilindro_eto"];
	$peso_esperado = $linea3["peso_esperado"];
	$pre_final = $linea3["pre_final"];
	$tara_vacio = $linea3["tara_vacio"];

	$consulta4 = "SELECT num_cili_eto,id_tipo_envace  
				FROM cilindro_eto  
				WHERE id_cilindro_eto = ".$id_cilindro_eto;
	$resultado4 = mysqli_query($con,$consulta4) ;
	while ($linea4 = mysqli_fetch_array($resultado4))
	{
		$num_cili_eto = $linea4["num_cili_eto"];
		$id_tipo_envace = $linea4["id_tipo_envace"];
	}mysqli_free_result($resultado4);

	$consulta5 = "SELECT *  
				FROM tipo_envace  
				WHERE id_tipo_envace = ".$id_tipo_envace;
	$resultado5 = mysqli_query($con,$consulta5) ;
	while ($linea5 = mysqli_fetch_array($resultado5))
	{
		$tipo = $linea5["tipo"];
	}mysqli_free_result($resultado5);
	/*$line = array( "Numero Cilindro"    => $num_cili_eto,
				   "Capacidad Cilindro" => $tipo,
				   "TARA KGS"           => $tara_vacio,
				   "PRESION"           => $pre_final);
	$size = $pdf->addLine( $y, $line );
	$y   += $size + 2;*/
}
mysqli_free_result($resultado3);

//$pdf->addLetras($letras);
//$pdf->addCLientName($recibido_de);
//$pdf->addCLientAdress($valor_total);
//$pdf->addCLientDoc($ciudad_empresa,$fechaAfiliacion);
//$pdf->addClientConcepto($Concepto);
//$pdf->addPayMode2($No_Cheque,$Sucursal,$Banco);
//$pdf->addPayMode($No_Cheque,$Sucursal);
//$pdf->addSign("");
//


$file_name = str_replace(' ','_',$nombre_empresa_ajustado)."_Certificado_No._".$id_orden.".pdf";
$file_destination = $path_pdf.$file_name;
$pdf->Output($file_destination,'F');
$pdf->Output($file_name,'I');

?>