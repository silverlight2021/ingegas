<?php
date_default_timezone_set("America/Bogota");
require ("libraries/conexion.php");
require ("libraries/seguimiento.php");
session_start();

if(@$_SESSION['logged']== 'yes')
{ 		
	$id_produccion_mezclas = isset($_REQUEST['id_produccion_mezclas']) ? $_REQUEST['id_produccion_mezclas'] : NULL;
	$id_cilindro_eto1 = isset($_REQUEST['id_cilindro_eto']) ? $_REQUEST['id_cilindro_eto'] : NULL;
  	$id_orden = isset($_REQUEST['id_orden']) ? $_REQUEST['id_orden'] : NULL;
    $User_idUser =$_SESSION['su'];
    $acc = $_SESSION['acc'];
    $orden_fini="";
    $id_lote_eto1 = "";
    $carga='';

    if (isset($_GET['abrir'])) 
    {
    	$consulta5  = "UPDATE ordenes 
						SET id_estado='1',id_abrir = 1
    					WHERE id_orden =".$_GET['id_orden'];
	    $resultado5 = mysqli_query($con,$consulta5) ;
	    if ($resultado5 == FALSE)
	    {
	       	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
	    }
	    else
	    {
	    	header('Location: mezcla.php?id_orden='.$id_orden);
	    }
    }
    if (isset($_GET['cerrar'])) 
    {
    	$consulta5  = "UPDATE ordenes 
						SET id_estado='2',id_abrir = 0
    					WHERE id_orden =".$_GET['id_orden'];
	    $resultado5 = mysqli_query($con,$consulta5) ;
	    if ($resultado5 == FALSE)
	    {
	       	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
	    }
	    else
	    {
	    	header('Location: mezcla.php?id_orden='.$id_orden);
	    }
    }
    if (isset($_GET['facturar'])) 
    {
    	$Fecha = date("Y/m/d");
    	$consulta5  = "UPDATE ordenes 
						SET fecha_factura='$Fecha'
    					WHERE id_orden =".$id_orden;
	    $resultado5 = mysqli_query($con,$consulta5) ;
	    if ($resultado5 == FALSE)
	    {
	       	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
	    }
	    else
	    {
	    	header('Location: mezcla.php?id_orden='.$id_orden);
	    }
    }

	if(isset($_POST['g_cilindro']))
	{

	///Recibo todos los datos al hacer clic en el formulario 
		$id_cilindro_eto1 = $_POST['id_cilindro_eto']; //id_principal 
		$prue_hidro1 = $_POST['prue_hidro'];
		$pre_inicial1 = $_POST['pre_inicial'];
		//$gas_eva1 = $_POST['gas_eva'];
		$peso_inicial1 = $_POST['peso_inicial'];  
		$peso_final1 = $_POST['peso_final'];
		$pre_vacio1 = $_POST['pre_vacio'];
		$lote_pro1 = $_POST['lote_pro'];
		//$nombre_mezcla1 = $_POST['nombre_mezcla'];
		$tara_vacio1 = $_POST['tara_vacio'];
		//$peso_esperado1 = $_POST['peso_esperado'];
		$pre_inicial_11 = $_POST['pre_inicial_1'];
		$esperado_eto1 = $_POST['esperado_eto'];
		$real_eto1 = $_POST['real_eto'];
		$esperado_co1 = $_POST['esperado_co'];
		$real_co1 = $_POST['real_co'];
		$peso_final_11 = $_POST['peso_final_1'];
		$desviacion1 = $_POST['desviacion'];
		$pre_final1 = $_POST['pre_final'];
		$lote_co1 = $_POST['id_num_cilindro'];
		$id_lote_eto = $_POST['id_lote_eto1'];
		//$cilindro_eto = $_POST['cilindro_eto'];
		$fecha_ven_mezcla1 = $_POST['fecha_ven_mezcla'];
	///	
	///Obtengo informacion del cilindro donde se verifica el peso esperado.El peso esperado es igual que la capacidad del cilindro 
		$consulta8  = "SELECT * FROM cilindro_eto WHERE id_cilindro_eto =".$id_cilindro_eto1;
	  	$resultado8 = mysqli_query($con,$consulta8) ; 
	  	$linea8= mysqli_fetch_array($resultado8);
	  	$gas_eva1 = isset($linea8["id_tipo_cilindro"]) ? $linea8["id_tipo_cilindro"] : NULL;
	  	$nombre_mezcla1 = isset($linea8["id_tipo_cilindro"]) ? $linea8["id_tipo_cilindro"] : NULL;
	  	$peso_esperado1 = isset($linea8["id_tipo_envace"]) ? $linea8["id_tipo_envace"] : NULL;
	  
	  	if ($resultado8 == FALSE)
	    {
	      echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
	    }
	    else
	    {
	    	//Verifo si la orden fue abierta,si fue abierta el cilidro a agragar o a insertar se le debe poner el lote de produccion correspondiente a esa orden 
	    	$consulta8  = "SELECT * FROM ordenes WHERE id_orden =".$id_orden;
		  	$resultado8 = mysqli_query($con,$consulta8) ; 
		  	$linea8= mysqli_fetch_array($resultado8);
		  	$id_abrir = isset($linea8["id_abrir"]) ? $linea8["id_abrir"] : NULL;
		  	$num_lote = isset($linea8["num_lote"]) ? $linea8["num_lote"] : NULL;
	    	if ($id_abrir==0) 
	    	{
	    		$lote_pro22 =$lote_pro1 ;
	    	}
	    	else
	    	{
	    		$lote_pro22 = $num_lote ;
	    	}	
	    	///

	    	//Se verifica si la el cilindro ya existe en la orden,si es así lo edita
	      	if(strlen($id_produccion_mezclas) > 0)
			{
				$consulta= "UPDATE produccion_mezclas SET 	                
            	id_orden = '".$id_orden."',
          		id_cilindro = '".$id_cilindro_eto1."', 
          		prue_hidro = '".$prue_hidro1."', 
          		pre_inicial = '".$pre_inicial1."', 
          		gas_eva = '".$gas_eva1."',
          		peso_inicial = '".$peso_inicial1."',
          		peso_final = '".$peso_final1."',
          		pre_vacio = '".$pre_vacio1."',
          		lote_pro = '".$lote_pro22."',
          		nombre_mezcla = '".$nombre_mezcla1."', 
          		tara_vacio = '".$tara_vacio1."', 
          		peso_esperado = '".$peso_esperado1."', 
          		pre_inicial_1 = '".$pre_inicial_11."',
          		esperado_eto = '".$esperado_eto1."' , 
          		real_eto = '".$real_eto1."', 
          		esperado_co = '".$esperado_co1."',
          		real_co = '".$real_co1."',
          		peso_final_1 = '".$peso_final_11."',
          		desviacion = '".$desviacion1."',
          		pre_final = '".$pre_final1."',
          		lote_co = '".$lote_co1."',
          		cilindro_eto = '".$cilindro_eto."',
          		fecha_ven_mezcla = '".$fecha_ven_mezcla1."',
          		id_lote_eto = '".$id_lote_eto."'
                WHERE id_produccion_mezclas = $id_produccion_mezclas ";
			    $resultado = mysqli_query($con,$consulta) ;

			    if ($resultado == FALSE)
			    {
			      	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
			    }
			    else
			    {
			    	seguimiento(5,$id_cilindro_eto1,$User_idUser,3,$id_produccion_mezclas);//id_seguimiento,id_cilindro_eto,id_user,id_proceso,id_has_proceso
			    	header('Location: mezcla.php?id_orden='.$id_orden);
			    }
			}
			else
			{ 	
			///
			//Si el cilindro no exite en la orden se inserta		
				$cilindro_eto = 0;				
				$consulta = "INSERT INTO produccion_mezclas(
		    			id_orden,
		          		id_cilindro, 
		          		prue_hidro, 
		          		pre_inicial, 
		          		gas_eva, 
		          		peso_inicial, 
		          		peso_final,
		          		pre_vacio,
		          		lote_pro, 
		          		nombre_mezcla, 
		          		tara_vacio, 
		          		peso_esperado,
		          		pre_inicial_1,
		          		esperado_eto,
		          		real_eto,
		          		esperado_co,
		          		real_co,
		          		peso_final_1,
		          		desviacion,
		          		pre_final,
		          		lote_co,
		          		cilindro_eto,
		          		id_lote_eto,
		          		fecha_ven_mezcla,
		          		fech_crea) 
		          		VALUES(
		          		'".$id_orden."',
		          		'".$id_cilindro_eto1."', 
		          		'".$prue_hidro1."', 
		          		'".$pre_inicial1."', 
		          		'".$gas_eva1."', 
		          		'".$peso_inicial1."', 
		          		'".$peso_final1."',
		          		'".$pre_vacio1."',
		          		'".$lote_pro22."', 
		          		'".$nombre_mezcla1."', 
		          		'".$tara_vacio1."', 
		          		'".$peso_esperado1."', 
		          		'".$pre_inicial_11."',
		          		'".$esperado_eto1."' , 
		          		'".$real_eto1."', 
		          		'".$esperado_co1."',
		          		'".$real_co1."',
		          		'".$peso_final_11."',
		          		'".$desviacion1."',
		          		'".$pre_final1."',
		          		'".$lote_co1."',
		          		'".$cilindro_eto."',
		          		'".$id_lote_eto."',
		          		'".$fecha_ven_mezcla1."',
		          		'".date("Y/m/d")."')";
			    $resultado = mysqli_query($con,$consulta) ;
			    if ($resultado == FALSE)
			    {
			      echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
			    }
			    else
			    {

			    	$id_produccion_mezclas22 = mysqli_insert_id($con);//Se obtiene el ultimo id de la tabla "produccion_mezclas"
			    	seguimiento(6,$id_cilindro_eto1,$User_idUser,3,$id_produccion_mezclas22);//id_seguimiento,id_cilindro_eto,id_user,id_proceso,id_has_proceso

			    	//Se selecciona el uso actual del id del lote de insumo eto
			    	$consulta22  = "SELECT * FROM lote_eto WHERE id_lote_eto = $id_lote_eto";
					$resultado22 = mysqli_query($con,$consulta22) ;
					$linea22 = mysqli_fetch_array($resultado22); 					
					$uso_actual_e = isset($linea22["uso_actual"]) ? $linea22["uso_actual"] : NULL;
					$resta_e=$uso_actual_e-$real_eto1;
					///
					//Se resta el peso real del cilindro al uso actual 
					$consulta33= "UPDATE lote_eto SET 	                
                	uso_actual = '$resta_e'          	
	                WHERE id_lote_eto = $id_lote_eto ";
				    $resultado33 = mysqli_query($con,$consulta33) ;
				    ///				    
				    //Se selecciona el uso actual del id del lote de insumo CO2
					$consulta11  = "SELECT * FROM lote_co2 WHERE id_num_cilindro = $lote_co1";
					$resultado11 = mysqli_query($con,$consulta11) ;
					$linea11 = mysqli_fetch_array($resultado11);
					$uso_actual = isset($linea11["uso_actual"]) ? $linea11["uso_actual"] : NULL;
					///
					$resta=$uso_actual-$real_co1;
					$consulta= "UPDATE lote_co2 SET 	                
                	uso_actual = '$resta'          	
	                WHERE id_num_cilindro = $lote_co1 ";
				    $resultado = mysqli_query($con,$consulta) ;

				    if ($resultado == FALSE)
				    {
				      	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
				    }
				    else
				    {	
				    	$consulta= "UPDATE cilindro_eto SET 	                
	                	id_estado = '3'          	
		                WHERE id_cilindro_eto = $id_cilindro_eto1 ";
					    $resultado = mysqli_query($con,$consulta) ;

					    if ($resultado == FALSE)
					    {
					      	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
					    }
					    else
					    {
					    	$consulta5  = "UPDATE pre_produccion_mezcla SET
					    	id_estado_cilindro='3'

					    	WHERE id_cilindro_eto =".$id_cilindro_eto1;
						    $resultado5 = mysqli_query($con,$consulta5) ;
						    if ($resultado5 == FALSE)
						    {
						       	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
						    }
						    else
						    {
						    	$consulta3  = "SELECT * FROM has_orden_cilindro WHERE id_orden= $id_orden ";
							  	$resultado3 = mysqli_query($con,$consulta3) ;
							  	$linea3 = mysqli_fetch_array($resultado3);
							  	
						  		$total = isset($linea3["cantidad"]) ? $linea3["cantidad"] : NULL;		
							    //echo "SELECT COUNT(*) AS count FROM has_orden_cilindro WHERE id_orden = $id_orden ";
							    $result = mysqli_query($con,"SELECT COUNT(*) AS count FROM produccion_mezclas WHERE id_orden = $id_orden "); 
								$row = mysqli_fetch_array($result,MYSQLI_ASSOC); 
								$count = $row['count'];
								if ($count == $total) 
								{
									$Fecha = date("Y/m/d");
									$consulta5  = "UPDATE ordenes 
													SET id_estado='2' , fecha_ven = '".date("y/m/d", strtotime("$Fecha + 90 days"))."' , fecha_ter = '".date("Y/m/d H:i:s")."'
							    					WHERE id_orden =".$id_orden;
								    $resultado5 = mysqli_query($con,$consulta5) ;
								    if ($resultado5 == FALSE)
								    {
								       	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
								    }
								    else
								    {

								    	$consulta  = "SELECT MAX(num_lote_pro) AS max_lote_pro FROM lote_pro ";
								  		$resultado = mysqli_query($con,$consulta) ;
								  		$linea = mysqli_fetch_array($resultado);
								  		$max_lote = isset($linea["max_lote_pro"]) ? $linea["max_lote_pro"] : NULL;
								  		$lote_pro = $max_lote+1;

								  		if ($id_abrir==0) 
									    {
									    	$consulta55  = "UPDATE lote_pro 
													SET num_lote_pro='$lote_pro'
							    					WHERE id_lote_pro = '1'";
									    	$resultado5 = mysqli_query($con,$consulta55) ;

									    	
									    	$consulta66  = "UPDATE ordenes 
													SET num_lote='$max_lote'
							    					WHERE id_orden =".$id_orden;
										    $resultado6 = mysqli_query($con,$consulta66) ;
										    ?>
								        <!--	<script type="text/javascript">
												alert("entro")
											</script> -->
										<?php
									    }
										else											
										{
											$consulta55  = "UPDATE ordenes 
												SET id_abrir= 0 , num_lote_pro='$lote_pro22'
						    					WHERE id_orden =".$id_orden;
									    	$resultado5 = mysqli_query($con,$consulta55) ;
									    	?>
								        	<!-- <script type="text/javascript">
												alert(" no entro")
											</script> -->
										<?php										       
										}	
										?>
								        	<script type="text/javascript">
												alert("Esta Orden ha sido completada")
											</script>
										<?php
										//header('Location: mezcla.php?id_orden='.$id_orden);
										//$id_estado=2;
									}								
								}								
						    }			    	
					    }
					}    
			    }				 
			} 
		}mysqli_free_result($resultado8);
	}
	if(strlen($id_orden) > 0)
	{ 
	  	$consulta  = "SELECT * FROM has_orden_cilindro WHERE id_orden= '".$id_orden."'";
	  	$resultado = mysqli_query($con,$consulta) ;
	  	$linea = mysqli_fetch_array($resultado);
	  	
		$fecha = isset($linea["fecha"]) ? $linea["fecha"] : NULL;
		$id_orden = isset($linea["id_orden"]) ? $linea["id_orden"] : NULL;
		$id_tipo_cilindro1 = isset($linea["id_tipo_cilindro"]) ? $linea["id_tipo_cilindro"] : NULL;	
		$id_tipo_envace1 = isset($linea["id_tipo_envace"]) ? $linea["id_tipo_envace"] : NULL;
		$id_finalizar = isset($linea["id_finalizar"]) ? $linea["id_finalizar"] : NULL;
		$total = isset($linea["cantidad"]) ? $linea["cantidad"] : NULL;	
		$cantidad = isset($linea["cantidad"]) ? $linea["cantidad"] : NULL;
		mysqli_free_result($resultado);

	  	$consulta2  = "SELECT * FROM tipo_cilindro WHERE id_tipo_cilindro = '".$id_tipo_cilindro1."'";
	  	$resultado2 = mysqli_query($con,$consulta2) ;
	  	$linea2 = mysqli_fetch_array($resultado2);
	  	$id_tipo_cilindro = isset($linea2["tipo_cili"]) ? $linea2["tipo_cili"] : NULL;
	  	mysqli_free_result($resultado2);

	  	$consulta3  = "SELECT * FROM tipo_envace WHERE id_tipo_envace= $id_tipo_envace1";
	  	$resultado3 = mysqli_query($con,$consulta3) ;
	  	$linea3 = mysqli_fetch_array($resultado3);
	  	$id_tipo_envace = isset($linea3["tipo"]) ? $linea3["tipo"] : NULL;		
	    mysqli_free_result($resultado3);	  	
  		
	    $result = mysqli_query($con,"SELECT COUNT(*) AS count FROM produccion_mezclas WHERE id_orden = $id_orden "); 
		$row = mysqli_fetch_array($result,MYSQLI_ASSOC); 
		$count = $row['count'];
		if ($count == $total) 
		{
			$carga=1;
			$consulta  = "SELECT real_eto, SUM(real_eto) FROM produccion_mezclas WHERE id_orden = $id_orden";
			$resultado = mysqli_query($con,$consulta) ;
			$linea = mysqli_fetch_array($resultado);
			$suma_eto = isset($linea["SUM(real_eto)"]) ? $linea["SUM(real_eto)"] : NULL;

			$consulta1  = "SELECT real_co, SUM(real_co) FROM produccion_mezclas WHERE id_orden = $id_orden";
			$resultado1 = mysqli_query($con,$consulta1) ;
			$linea1 = mysqli_fetch_array($resultado1);
			$suma_co22 = isset($linea1["SUM(real_co)"]) ? $linea1["SUM(real_co)"] : NULL;
			$suma_co2 = round($suma_co22, 2);
			$consulta2  = "SELECT peso_final_1, SUM(peso_final_1) FROM produccion_mezclas WHERE id_orden = $id_orden";
			$resultado2 = mysqli_query($con,$consulta2) ;
			$linea2 = mysqli_fetch_array($resultado2);
			$suma_final2 = isset($linea2["SUM(peso_final_1)"]) ? $linea2["SUM(peso_final_1)"] : NULL;	
			$suma_final = round($suma_final2, 2);

			$consulta4  = "SELECT * FROM produccion_mezclas WHERE id_orden = $id_orden ";
		  	$resultado4 = mysqli_query($con,$consulta4) ;
		  	$linea4 = mysqli_fetch_array($resultado4);

			$lote_pro = isset($linea4["lote_pro"]) ? $linea4["lote_pro"] : NULL;
			mysqli_free_result($resultado);
		}
	}
	if(strlen($id_orden) > 0)
	{ 
	  	$consulta  = "SELECT * FROM ordenes WHERE id_orden= $id_orden";
	  	$resultado = mysqli_query($con,$consulta) ;
	  	$linea = mysqli_fetch_array($resultado);
	  	$num_ord = isset($linea["num_ord"]) ? $linea["num_ord"] : NULL;
		$fecha_ven = isset($linea["fecha_ven"]) ? $linea["fecha_ven"] : NULL;
		$id_estado = isset($linea["id_estado"]) ? $linea["id_estado"] : NULL;	
		$fecha_factura = isset($linea["fecha_factura"]) ? $linea["fecha_factura"] : NULL;
		$fecha_factura = isset($linea["fecha_factura"]) ? $linea["fecha_factura"] : NULL;	

			
	    mysqli_free_result($resultado);

	    $consulta1  = "SELECT * FROM lote_co2 WHERE id_estado_co2=1";
	  	$resultado1 = mysqli_query($con,$consulta1) ;
	  	$linea1 = mysqli_fetch_array($resultado1);
		$id_num_cilindro = isset($linea1["id_num_cilindro"]) ? $linea1["id_num_cilindro"] : NULL;	
		$num_cilindro_co2 = isset($linea1["num_cilindro_co2"]) ? $linea1["num_cilindro_co2"] : NULL;
		$alerta = isset($linea1["alerta"]) ? $linea1["alerta"] : NULL;
		$uso_actual = isset($linea1["uso_actual"]) ? $linea1["uso_actual"] : NULL;	
		mysqli_free_result($resultado1);

		$consulta2  = "SELECT * FROM lote_eto WHERE id_estado_eto=1";
	  	$resultado2 = mysqli_query($con,$consulta2) ;
	  	$linea2 = mysqli_fetch_array($resultado2);
		$alerta_eto = isset($linea2["alerta"]) ? $linea2["alerta"] : NULL;
		$uso_actual_eto = isset($linea2["uso_actual"]) ? $linea2["uso_actual"] : NULL;	
			
	    mysqli_free_result($resultado2);
	}  
	require_once("inc/init.php");
	require_once("inc/config.ui.php");
	$page_title = "Produccion de Mezclas";
	$page_css[] = "your_style.css";
	include("inc/header.php");
	$page_nav[""][""][""][""] = true;
	include("inc/nav.php");
?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<!-- Modal -->
<div class="modal fade" id="modal_orden" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">
	<form name="mezcla" action="mezcla.php" method="POST" id="mezcla">
	<input type="hidden" name="id_cilindro_eto"  value="id_cilindro_eto">
	<input type="hidden" name="id_orden" id="id_orden" value="<?php echo $id_orden; ?>">
	<input type="hidden" name="id_produccion_mezclas" id="id_produccion_mezclas" value="<?php echo $id_produccion_mezclas; ?>">
	<input type="hidden" name="id_num_cilindro" id="id_num_cilindro" value="<?php echo $id_num_cilindro; ?>">
	<input type="hidden" name="total" id="total" value="<?php echo $total; ?>">
	<input type="hidden" name="alerta" id="alerta" value="<?php echo $alerta; ?>">
	<input type="hidden" name="uso_actual" id="uso_actual" value="<?php echo $uso_actual; ?>">
	<input type="hidden" name="alerta_eto" id="alerta_eto" value="<?php echo $alerta_eto; ?>">
	<input type="hidden" name="uso_actual_eto" id="uso_actual_eto" value="<?php echo $uso_actual_eto; ?>">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" id ="cerrar"aria-hidden="true">
					&times;
					</button>
					<h4 class="modal-title" id="myModalLabel">Orden</h4>
				</div>
				<div class="modal-body">
				<div class="well well-sm well-primary">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="category">Numero Cilindro :</label>
								<input type="text" class="form-control" placeholder="Numero Cilindro" name="num_cili_eto" readonly required />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="category">Prueba Hidrostatica :</label>
								<input type="date" class="form-control" placeholder="Prueba Hidrostatica" name="prue_hidro" readonly required />
							</div>
						</div>
					</div>
				</div>
				<div class="well well-sm well-primary">	
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label for="category">Presión Inicial :</label>
								<input type="num" class="form-control" placeholder="Presión Inicial" name="pre_inicial" readonly required />
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="category">Gas Evacuado :</label>
								<input type="text" class="form-control" placeholder="Gas Evacuado " name="gas_eva" readonly required />
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="category">Peso Inicial (Kg):</label>
								<input type="num" class="form-control" placeholder="Peso Inicial (Kg)" name="peso_inicial" readonly required />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="category">Peso Final (Kg) :</label>
								<input type="num" class="form-control" placeholder="Peso Final (Kg)" onKeyUp="igualar()" name="peso_final" required />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="category">Presion de Vacio (in Hg):</label>
								<input type="num" class="form-control" placeholder="Presion de Vacio (in Hg)" name="pre_vacio" readonly required />
							</div>
						</div>
					</div>
				</div>
				<div class="well well-sm well-primary">	
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label for="category">Lote Produccion :</label>
								<input type="text" class="form-control" placeholder="Lote Produccion" name="lote_pro" readonly required />
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="category">Nombre Mezcla :</label>
								<input type="text" class="form-control" placeholder="Nombre Mezcla" name="nombre_mezcla" readonly required />
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="category">Tara Vacio (Kg) :</label>
								<input type="text" class="form-control" placeholder="Tara Vacio (Kg)" name="tara_vacio" readonly required />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="category">Peso Esperado (Kg):</label>
								<input type="text" class="form-control" placeholder="Peso Esperado (Kg)" name="peso_esperado" readonly required />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="category">Presion Inicial (in Hg) :</label>
								<input type="text" class="form-control" placeholder="Presion Inicial (in Hg)" name="pre_inicial_1" required />
							</div>
						</div>
					</div>
				</div>
				<div class="well well-sm well-primary">	
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="category">Esperado ETO:</label>
								<input type="text" class="form-control" placeholder="Esperado" name="esperado_eto" readonly required />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="category">Real ETO:</label>
								<input type="text" class="form-control" placeholder="Real" name="real_eto" required />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="category">Esperado CO2:</label>
								<input type="text" class="form-control" placeholder="Esperado" name="esperado_co" readonly required />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="category">Real CO2:</label>
								<input type="text" class="form-control" placeholder="Real" name="real_co" required />
							</div>
						</div>
					</div>
				</div>
				<div class="well well-sm well-primary">	
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label for="category">Peso Final (Kg) :</label>
								<input type="text" class="form-control" placeholder="Peso Final (Kg)" name="peso_final_1" readonly required />
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="category">Desviacion (%) :</label>
								<input type="text" class="form-control" placeholder="Desviacion (%) " name="desviacion" readonly required />
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="category">Presión Final (Psi) :</label>
								<input type="text" class="form-control" placeholder="Presión Final (Psi)" name="pre_final" required />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="category">Lote CO2 :</label>
								<input type="text" class="form-control" placeholder="Lote CO2" name="num_cilindro_co2" readonly  value="<?php echo $num_cilindro_co2; ?>" >
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<?php
									$consulta7 ="SELECT * FROM lote_eto WHERE id_estado_eto = 1 ";
									$resultado7 = mysqli_query($con,$consulta7) ;
								?>	
								<label for="category">Cilindro ETO :</label>
								<select class="form-control" name='id_lote_eto1' >
								<option value='0'>Seleccione...</option>
								<?php
								while($linea7 = mysqli_fetch_array($resultado7))
								{
									$id_lote_eto = $linea7['id_lote_eto'];
									$num_cilindro_eto = $linea7['num_cilindro_eto'];
									if ($id_lote_eto==$id_lote_eto1)
									{
										echo "<option value='$id_lote_eto' selected >$num_cilindro_eto</option>"; 
									}
									else 
									{
										echo "<option value='$id_lote_eto'>$num_cilindro_eto</option>"; 
									} 
								}//fin while 
								?>
								</select>								
							</div>
						</div>						
					</div>
					<div class="row">							
						<div class="col-md-6">
							<div class="form-group">
								<label for="category">Vencimiento Mezcla :</label>
								<input type="date" class="form-control" placeholder="Vencimiento Mezcla" name="fecha_ven_mezcla" readonly required value="<?php echo $fecha_ven; ?>">
							</div>
						</div>						
					</div>					
				</div>			
					
				<div class="modal-footer">
					<button type="button" class="btn btn-default" id="Cancelar" data-dismiss="modal">	Cancelar</button>
					<?php
					if (in_array(22, $acc))
					{
					?>
						<input type="submit" value="Guardar" name="g_cilindro" id="g_cilindro" class="btn btn-primary" />
					<?php
					}										
					?>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</form>
</div><!-- /.modal -->
<!-- Fin Modal -->
 </div>
<div id="main" role="main">
	<div id="content">
		<section id="widget-grid" class="">
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-6">
					<div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h6 id="eg1">Producción Mezclas</h6>				
						</header>
						<div>				
						<div class="jarviswidget-editbox"></div>
							<div class="widget-body no-padding">												
								<input type="hidden" name="id_orden"  value="<?php echo $id_orden; ?>">
								<input type="hidden" name="id_tipo_cilindro1"  value="<?php echo $id_tipo_cilindro1; ?>">
								<input type="hidden" name="id_tipo_envace1"  value="<?php echo $id_tipo_envace1; ?>">							
								<div class="row">					
									<div class="col-md-3">
										<div class="form-group">
											<label for="category">Num Orden :</label>											 
											<input type="text" readonly class="form-control"  placeholder="Num Orden" value="<?php echo isset($num_ord) ? $num_ord : NULL; ?>" />											
										</div>
									</div>	
									<div class="col-md-3">	
										<div class="form-group">
											<label for="category">Tipo Gas :</label>											 
											<input type="text" readonly class="form-control" placeholder="Tipo Gas" value="<?php echo isset($id_tipo_cilindro) ? $id_tipo_cilindro : NULL; ?>" />											
										</div>
									</div>	
									<div class="col-md-3">	
										<div class="form-group">
											<label for="category">Capacidad :</label>											 
											<input type="text" readonly class="form-control" placeholder="Capacidad" value="<?php echo isset($id_tipo_envace) ? $id_tipo_envace : NULL; ?>" />											
										</div>
									</div>
									<div class="col-md-3">	
										<div class="form-group">
											<label for="category">Cantidad :</label>											 
											<input type="text" readonly class="form-control" placeholder="Cantidad" value="<?php echo isset($cantidad) ? $cantidad : NULL; ?>" />											
										</div>
									</div>							
								</div>
								<?php
								if ($id_estado==1) 
								{								
								?>
									<div class="row">
										<?php
										if ($carga==1) 
										{
											if (in_array(45, $acc))
											{
										?>						
											<div class="col-md-3">
												<div class="form-group">
													<a class="btn btn-danger" href="mezcla.php?cerrar=1&id_orden=<?php echo $id_orden; ?>">Cerrar Orden</a>
												</div>										
											</div>
										<?php
											}
										}
										else
										{
											if (in_array(37, $acc))
											{
											?>											
											<div class="col-md-3">
												<div class="form-group">
													<label for="category">Número cilindro :</label>											 
													<input type="text" class="form-control" name="num_cili" id="cilindro" placeholder="Número cilindro" value="" />											
												</div>
											</div>
											<?php
											}
										}																				
										?>
									</div>
								<?php
								}
								if($id_estado==2 || $id_estado==3)
								{ 
								?>
									<div class="row">	
										<div class="col-md-3">
											<div class="form-group">
												<label for="category">Lote :</label>											 
												<input type="text" class="form-control" readonly  placeholder="Consumo ETO" value="<?php echo isset($lote_pro) ? $lote_pro : NULL; ?>" />											
											</div>
										</div>	
										<div class="col-md-3">
											<div class="form-group">
												<label for="category">Consumo ETO (Kg) :</label>											 
												<input type="text" class="form-control" readonly name="num_cili"  placeholder="Consumo ETO" value="<?php echo isset($suma_eto) ? $suma_eto : NULL; ?>" />											
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label for="category">Consumo CO2 (Kg) :</label>											 
												<input type="text" class="form-control" readonly name=""  placeholder="Consumo CO2" value="<?php echo isset($suma_co2) ? $suma_co2 : NULL; ?>" />											
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label for="category">Total (Kg) :</label>											 
												<input type="text" class="form-control" readonly name=""  placeholder="Total Kg" value="<?php echo isset($suma_final) ? $suma_final : NULL; ?>" />											
											</div>
										</div>						
									</div>
									<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label for="category">Orden Completa</label>
											</div>
											<a href="produccion_pdf.php?id_orden=<?php echo $id_orden; ?>" target="_blank"><img src="img/pdf-icon.png" width="50" height="50"></a>
										</div>	
					                    <?php
					                    if ($id_estado==3)
										{
											if ($fecha_factura=="0000-00-00")
											{
											?>
												<div class="col-md-4">
													<div class="form-group">
														<a class="btn btn-success" href="mezcla.php?facturar=1&id_orden=<?php echo $id_orden; ?>">Facturar</a>
													</div>										
												</div>
											<?php
											}
											else
											{	
											?>
												<div class="col-md-4">
													<div class="form-group">
														<label for="category">Facturado :<?php echo $fecha_factura; ?></label> 
													</div>
													<a href="javascript:imprSelec('muestra')"><img src="/img/iconos/printer_blue.png"></a>
												</div>
											<?php
											}

										}

										if (in_array(44, $acc)) 
										{									
										?>
											<div class="col-md-3">
												<div class="form-group">
													<a class="btn btn-success" href="mezcla.php?abrir=1&id_orden=<?php echo $id_orden; ?>">Abrir Orden</a>
												</div>										
											</div>
										<?php
										}							
										?>
									</div>
								<?php
								}
								?>
							</div>
						</div>					
					</div>
					<div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h6>Cilindros Llenos</h6>			
						</header>
						<div>				
						<div class="jarviswidget-editbox"></div>																	
							<table class="table table-bordered">							
								<thead>
									<tr>
										<th>#</th>                                                
			                            <th>Cilindro</th>
			                            <th>Tara</th>
			                            <th>F Vencimiento</th>
			                            <?php
										if ($id_estado==1) 
										{										
										?>
			                            	<th>Acción</th>			
			                            <?php
										}
										?>                            
									</tr>
								</thead>
								<tbody>													
								  <?php
			                          $contador = "0";	                
			                          $consulta = "SELECT * FROM produccion_mezclas WHERE id_orden=".$id_orden;
			                          $resultado = mysqli_query($con,$consulta) ;
			                          while ($linea = mysqli_fetch_array($resultado))
			                          {
			                            $contador = $contador + 1;
			                            $id_orden = $linea["id_orden"];
			                            $id_cilindro = $linea["id_cilindro"];
			                            $id_produccion_mezclas = $linea["id_produccion_mezclas"];
										$tara_vacio = $linea["tara_vacio"];
										$fecha_ven_mezcla = $linea["fecha_ven_mezcla"];				                   		                            

			                            $consulta1 = "SELECT * FROM  cilindro_eto WHERE id_cilindro_eto = $id_cilindro";
			                            $resultado1 = mysqli_query($con,$consulta1) ;
			                            while ($linea1 = mysqli_fetch_array($resultado1))
			                            {
			                            	$num_cili_eto = $linea1["num_cili_eto"];
			                            }
			                            $consulta2 = "SELECT fecha_ven FROM  ordenes WHERE id_orden = $id_orden";
			                            $resultado2 = mysqli_query($con,$consulta2) ;
			                            while ($linea2 = mysqli_fetch_array($resultado2))
			                            {
			                            	$fecha_ven = $linea2["fecha_ven"];
			                            }			                            		                            
			                            ?>
			                            <tr class="odd gradeX">
			                              	<td width="5"><?php echo $contador; ?></td>                                  
			                              	<td><?php echo $num_cili_eto; ?></td>
			                              	<td><?php echo $tara_vacio; ?></td>
			                              	<td><?php echo $fecha_ven; ?></td>
			                              	<?php
											if ($id_estado==1) 
											{
											
											?>
			                              	<td width="100" style="vertical-align:bottom;">			                              	
			                              		<input type="image" onclick="editar(<?php echo $id_produccion_mezclas; ?>)" src="img/edit.png" width="30" height="30">
			                              		<?php
												if (in_array(38, $acc))
												{
												?>
			                              			<a href="eliminar_cili.php?id_produccion_mezclas=<?php echo $id_produccion_mezclas; ?>&id_cilindro_eto=<?php echo $id_cilindro; ?>&id_orden=<?php echo $id_orden; ?>"><input type="image"  src="img/eliminar.png" width="35" height="35"></a>
			                              		<?php
												}
												?>			                              
			                              	</td>
			                              	<?php
											}											
											?>                       
			                            </tr>                           
			                            <?php
			                            }mysqli_free_result($resultado);	                                            
		                  		    ?>
								</tbody>							
							</table>												
						</div>           
         			</div>
         		</article>
     		 </div>
   		</section> 
    </div>
</div>
<div id="muestra" style="display:none">
		<p>INGEGAS-INGENIERIA Y GASES LTDA</p>
		<h2 align="center">Informe Producción de Mezclas</h2>
		<table class="table table-bordered" style="border: 1px solid black;border-collapse: collapse;">							
			<thead>
				<tr>                                               
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Num Orden </th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Tipo Gas</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Capacidad</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Cantidad</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Lote</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Consumo ETO (kG)</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Consumo CO2 (kG)</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Total (kG)</th>             
				</tr>
			</thead>
			<tbody>
                <tr class="odd gradeX">                                
                  	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $num_ord; ?></td>
                  	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $id_tipo_cilindro; ?></td>
                  	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $id_tipo_envace; ?></td>  
                  	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $cantidad; ?></td>  
                  	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo isset($lote_pro) ? $lote_pro : NULL; ?></td>  
                  	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo isset($suma_eto) ? $suma_eto : NULL; ?></td>     
                  	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo isset($suma_co2) ? $suma_co2 : NULL; ?></td>  
                  	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo isset($suma_final) ? $suma_final : NULL; ?></td>  
                </tr>
			</tbody>							
		</table>		
		<br>
		<br>		
		<br>
		<br>			
	</div>
<?php
  include("inc/footer.php");
  include("inc/scripts.php"); 
?>
<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/jquery-form/jquery-form.min.js"></script>
<script src="js/plugin/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<script type="text/javascript">
	function imprSelec(muestra)
	{
		var ficha=document.getElementById(muestra);
		var ventimp=window.open(' ','popimpr');
		ventimp.document.write(ficha.innerHTML);
		ventimp.document.close();
		ventimp.print();
		ventimp.close();
	}
</script>
<script>
	$('#mezcla').on('keyup keypress', function(e) {
	  	var keyCode = e.keyCode || e.which;
	  	if (keyCode === 13) 
	  	{ 
	    	e.preventDefault();
	    	return false;
	  	}
	});
</script>
<script>
$(document).ready(function(){
    $("#g_cilindro").click(function(){
        $('#g_cilindro').hide();
        alert("Por Favor Espere,su informacion se esta transmitiendo");
    });
});
</script>
<script>
$(document).ready(function(){
    $("#cancelar").click(function(){
        $('#g_cilindro').show();
    });
});
</script>
<script type="text/javascript">
	function igualar()
	{
		mezcla.tara_vacio.value = (parseFloat(mezcla.peso_final.value));
	}
</script>

<script type="text/javascript">
	$(document).ready(function() 
	{

	  	var uso = '<?php echo $uso_actual_eto; ?>';
	  	var alerta = '<?php echo $alerta_eto; ?>';
	    
	    if(parseInt(uso)<=parseInt(alerta)) 
	    {
	      $.bigBox({
	        title : "Alerta Insumo ETO",
	        content : "El contenido del Cilindro se está agotando. Por favor tenga en cuenta el restante para las siguientes órdenes de producción.",
	        color : "#C46A69",
	        //timeout: 6000,
	        icon : "fa fa-warning shake animated",
	        number : "1",
	        timeout : 60000
	      });
	    }
	    else
	    {
	      $.bigBox({
	        title : "Alerta Insumo ETO",
	        content : "Contenido disponible.",
	        color : "#28B463",
	        //timeout: 6000,
	        icon : "fa fa-check-square-o shake animated",
	        number : "1",
	        timeout : 60000
	      });
	    }
	});
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#cilindro").keypress(function(e) {
	       if(e.which == 13) 
	       {        
		    	document.getElementById("mezcla").reset();
		    	var num_cili1 = $('input:text[name=num_cili]').val();
		    	//var num_cili1 = document.checkout_form.num_cili.value;	    	
				validar(num_cili1);	          
	       }
	    });		    
	    function validar(dato)
		{	
			//alert(dato);
			var id_tipo_cilindro1 = $('input:hidden[name=id_tipo_cilindro1]').val();
			var id_tipo_envace1 = $('input:hidden[name=id_tipo_envace1]').val();
			var id_orden = $('input:hidden[name=id_orden]').val();
			//alert(id_orden);
			$.ajax({
				url: 'id_cili2.php',
				type: 'POST',
				data: 'dato='+dato+'&dato1='+id_orden,
			})
			.done(function(data) {
				var objeto = JSON.parse(data);							
				var id_cili= objeto.id;
				var tipo_cili= objeto.tipo_cili;
				var tipo_envace= objeto.tipo_envace;
				var variable= objeto.variable;
				//alert(id_tipo_cilindro1); 			
				if (variable==2) 
				{	
					if (tipo_cili==id_tipo_cilindro1&tipo_envace==id_tipo_envace1) 
					{							
						form_cili(id_cili);
						$("#modal_orden").modal(); 
					}
					else
					{
						alert("Por Favor seleccione un cilindro de otro tipo de mezcla ");
					}	
				}
				if(variable==3)
				{				
					alert("Por Favor seleccione un cilindro valido para la mezcla");
				}
				if (variable==1) 
				{
					alert("Orden ya esta terminada ");
				}
				if (variable==4) 
				{
					alert("Orden se termino");
				}
							
			})
			.fail(function() {
				console.log("error");
			});
		}

		function form_cili(id_cili)
		{	
			//alert(tipo_envace);
			$.ajax({
				url: 'id_cili3.php',
				type: 'POST',
				data: 'dato='+id_cili,
			})
			.done(function(data) {
				var objeto = JSON.parse(data);							
				$("input[name=id_cilindro_eto]").val(objeto.id);
				$("input[name=num_cili_eto]").val(objeto.num_cili_eto);
				$("input[name=prue_hidro]").val(objeto.fecha_ult_eto);
				$("input[name=gas_eva]").val(objeto.tipo_cili);
				$("input[name=nombre_mezcla]").val(objeto.tipo_cili);
				$("input[name=peso_esperado]").val(objeto.tipo_envace);
				$("input[name=pre_inicial]").val(objeto.pre_inicial);
				$("input[name=peso_inicial]").val(objeto.peso_inicial);
				$("input[name=peso_final]").val(objeto.peso_final);
				$("input[name=pre_vacio]").val(objeto.pre_vacio);
				$("input[name=lote_pro]").val(objeto.lote_pro);
				$("input[name=tara_vacio]").val(objeto.peso_final);

				campo=objeto.tipo_cili;
				if (campo=="ETO-10") 
				{
					valor= 10;
				}
				if (campo=="ETO-20") 
				{
					valor= 20;
				}
				if (campo=="ETO-90") 
				{
					valor= 90;
				}
				if (campo=="ETO-100") 
				{
					valor= 100;
				}
				if (campo=="ETO-30") 
				{
					valor= 30;
				}

				var restante = 100-valor;
				var por_valor=valor*(0.01);
				var por_res=restante*(0.01);

			    var form = document.forms.mezcla;
			    form.oninput = function() 
			    {  
			        form.esperado_eto.value = ((parseFloat(form.peso_esperado.value))*por_valor).toFixed(2);
			        form.esperado_co.value = ((parseFloat(form.peso_esperado.value))*por_res).toFixed(2);
			        // form.tara_vacio.value = form.peso_final.value;
			        form.peso_final_1.value = ((parseFloat(form.real_eto.value))+(parseFloat(form.real_co.value))).toFixed(2);
			        form.desviacion.value = (Math.abs(((parseFloat(form.peso_esperado.value))-(parseFloat(form.peso_final_1.value)))/(parseFloat(form.peso_esperado.value)) *(100))).toFixed(2);
			        
			    }			
			})
			.fail(function() {
				console.log("error");
			});
		}		
	});

	function editar(cili)
	{
		//var cili=1;
		//alert(cili);
		$("#modal_orden").modal();
		$.ajax({
			url: 'edit_cili.php',
			type: 'POST',
			data: 'dato='+cili,
		})
		.done(function(data) {
			var objeto = JSON.parse(data);							
			$("input[name=id_produccion_mezclas]").val(objeto.id);
			$("input[name=id_cilindro_eto]").val(objeto.id_cilindro_eto);
			$("input[name=id_orden]").val(objeto.id_orden);
			$("input[name=num_cili_eto]").val(objeto.id_cilindro);
			$("input[name=prue_hidro]").val(objeto.prue_hidro);
			$("input[name=pre_inicial]").val(objeto.pre_inicial);
			$("input[name=gas_eva]").val(objeto.gas_eva);
			$("input[name=peso_inicial]").val(objeto.peso_inicial);
			$("input[name=peso_final]").val(objeto.peso_final);
			$("input[name=pre_vacio]").val(objeto.pre_vacio);
			$("input[name=lote_pro]").val(objeto.lote_pro);
			$("input[name=nombre_mezcla]").val(objeto.nombre_mezcla);
			$("input[name=tara_vacio]").val(objeto.tara_vacio);
			$("input[name=peso_esperado]").val(objeto.peso_esperado);
			$("input[name=pre_inicial_1]").val(objeto.pre_inicial_1);
			$("input[name=esperado_eto]").val(objeto.esperado_eto);
			$("input[name=real_eto]").val(objeto.real_eto);
			$("input[name=esperado_co]").val(objeto.esperado_co);
			$("input[name=real_co]").val(objeto.real_co);
			$("input[name=peso_final_1]").val(objeto.peso_final_1);
			$("input[name=desviacion]").val(objeto.desviacion);
			$("input[name=pre_final]").val(objeto.pre_final);
			$("input[name=lote_co]").val(objeto.lote_co);
			$("select[name=id_lote_eto1]").val(objeto.cilindro_eto);
			$("input[name=fecha_ven_mezcla]").val(objeto.fecha_ven_mezcla);	

			campo=objeto.gas_eva;
			if (campo=="ETO-10") 
			{
				valor= 10;
			}
			if (campo=="ETO-20") 
			{
				valor= 20;
			}
			if (campo=="ETO-90") 
			{
				valor= 90;
			}
			if (campo=="ETO-100") 
			{
				valor= 100;
			}

			var restante = 100-valor;
			var por_valor=valor*(0.01);
			var por_res=restante*(0.01);

		    var form = document.forms.mezcla;
		    form.oninput = function() 
		    {  
		        form.esperado_eto.value = ((parseInt(form.peso_esperado.value))*por_valor).toFixed(2);
		        form.esperado_co.value = ((parseInt(form.peso_esperado.value))*por_res).toFixed(2);
		       // form.tara_vacio.value = form.peso_final.value;
		        form.peso_final_1.value = ((parseFloat(form.real_eto.value))+(parseFloat(form.real_co.value))).toFixed(2);
		        form.desviacion.value = (Math.abs(((parseFloat(form.peso_esperado.value))-(parseFloat(form.peso_final_1.value)))/(parseFloat(form.peso_esperado.value)) *(100))).toFixed(2);
		    }
		})
		.fail(function() {
			console.log("error");
		});
	}
</script>

<?php 
include("inc/google-analytics.php"); 
}
else
{
    header("Location:index.php");
}
?>
					                       
										