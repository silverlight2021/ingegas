<?php
session_start();
date_default_timezone_set('America/Bogota');
$Fecha = date("Y-m-d");
$Hora = date("G:i:s", time());

if($_SESSION['logged'] == 'yes'){
  require("../libraries/conexion.php");

  $consulta = "SELECT * FROM cargue_comisiones WHERE estado_cargue < 2 ORDER BY id_cargue DESC LIMIT 1";
  $resultado = mysqli_query($con, $consulta);

  if(mysqli_num_rows($resultado)>0){
    $linea = mysqli_fetch_array($resultado);

    $cantidad_archivos = $linea["cantidad_archivos"];
    $id_cargue = $linea["id_cargue"];

    $cantidad_archivos = $cantidad_archivos+1;

    $consulta2 = "UPDATE cargue_comisiones SET cantidad_archivos = $cantidad_archivos WHERE id_cargue = $id_cargue";
    $resultado2 = mysqli_query($con, $consulta2);
  }else{
    $consulta2 = "INSERT INTO cargue_comisiones(cantidad_archivos, estado_cargue) VALUES (1,1)";
    $resultado2 = mysqli_query($con, $consulta2);

    $consulta3 = "TRUNCATE TABLE comisiones";
    $resultado3 = mysqli_query($con, $consulta3);

    $consulta4 = "TRUNCATE TABLE comisiones1";
    $resultado4 = mysqli_query($con, $consulta4);

    $consulta5 = "TRUNCATE TABLE comisiones2";
    $resultado5 = mysqli_query($con, $consulta5);
  }

  $filename = $_FILES["file"]["tmp_name"];
  $nombre = $_FILES["file"]["name"];
  
  if($_FILES["file"]["size"] > 0){

    $file = fopen($filename, "r");
    $idRegistro = 1;
    $error = 0;

    $sucursal = substr($nombre, 0, 3);

    $consulta6 = "SELECT * FROM comisiones WHERE sucursal LIKE '%$sucursal%'";
    $resultado6 = mysqli_query($con, $consulta6);
    
    if(mysqli_num_rows($resultado6)>0){
      while(($emapData = fgetcsv($file, 50000, ";")) !== FALSE){

        /*$caracter = "Ñ";
        $sNombre_cliente = $emapData[1];

        $pos = strpos($sNombre_cliente, $caracter);
        if($pos === true){
          $sNombre_cliente = str_replace('Ñ', 'N', $sNombre_cliente);
        }*/
        
        $sql = "INSERT INTO comisiones1 VALUES (NULL, '$emapData[0]', '$emapData[1]', '$emapData[2]', '$emapData[3]', '$emapData[4]', '$emapData[5]', 
              '$emapData[6]', '$emapData[7]', '$emapData[8]', '$emapData[9]', '$emapData[10]', '$emapData[11]', '$emapData[12]', '$emapData[13]', 
              '$emapData[14]', '$emapData[15]', '$emapData[16]', '$emapData[17]', '$emapData[18]', '$emapData[19]', '$emapData[20]', '$emapData[21]', 
              '$emapData[22]', '$emapData[23]', '$emapData[24]', '$emapData[25]', '$emapData[26]', '$emapData[27]', '$emapData[28]', '$emapData[29]', 
              '$emapData[30]', '$emapData[31]', '$emapData[32]', '$emapData[33]', '$emapData[34]', '$emapData[35]', '$emapData[36]', '$emapData[37]',
              '$emapData[38]', '$emapData[39]', '$emapData[40]', '$emapData[41]', '$emapData[42]', '$emapData[43]', '$emapData[44]', '$emapData[45]',
              '$emapData[46]', '$emapData[47]', '$emapData[48]', '$emapData[49]', '$emapData[50]', '$emapData[51]', '$emapData[52]', '$emapData[53]',
              '$emapData[54]', '$emapData[55]', '$emapData[56]', '$emapData[57]', '$emapData[58]', '$emapData[59]', '$emapData[60]', '$emapData[61]',
              '$emapData[62]', '$emapData[63]', '$emapData[64]', '$emapData[65]', '$emapData[66]', '$emapData[67]', '$emapData[68]', '$emapData[69]',
              '$emapData[70]', '$sucursal')";
        $result = mysqli_query($con, $sql);
        if($result == FALSE){
          $error = $error+1;
        }else{
          $idRegistro++;
        }
      }
      fclose($file);
      if($error > 0){
        echo "Exito";
      }else{
        echo "Exito";
      }
    }else{
      while(($emapData = fgetcsv($file, 50000, ";")) !== FALSE){
       /* $caracter = "Ñ";
        $sNombre_cliente = $emapData[1];

        $pos = strpos($sNombre_cliente, $caracter);
        if($pos === true){
          $sNombre_cliente = str_replace('Ñ', 'N', $sNombre_cliente);
        }*/
        $sql = "INSERT INTO comisiones VALUES (NULL, '$emapData[0]', '$emapData[1]', '$emapData[2]', '$emapData[3]', '$emapData[4]', '$emapData[5]', 
              '$emapData[6]', '$emapData[7]', '$emapData[8]', '$emapData[9]', '$emapData[10]', '$emapData[11]', '$emapData[12]', '$emapData[13]', 
              '$emapData[14]', '$emapData[15]', '$emapData[16]', '$emapData[17]', '$emapData[18]', '$emapData[19]', '$emapData[20]', '$emapData[21]', 
              '$emapData[22]', '$emapData[23]', '$emapData[24]', '$emapData[25]', '$emapData[26]', '$emapData[27]', '$emapData[28]', '$emapData[29]', 
              '$emapData[30]', '$emapData[31]', '$emapData[32]', '$emapData[33]', '$emapData[34]', '$emapData[35]', '$emapData[36]', '$emapData[37]',
              '$emapData[38]', '$emapData[39]', '$emapData[40]', '$emapData[41]', '$emapData[42]', '$emapData[43]', '$emapData[44]', '$emapData[45]',
              '$emapData[46]', '$emapData[47]', '$emapData[48]', '$emapData[49]', '$emapData[50]', '$emapData[51]', '$emapData[52]', '$emapData[53]',
              '$emapData[54]', '$emapData[55]', '$emapData[56]', '$emapData[57]', '$emapData[58]', '$emapData[59]', '$emapData[60]', '$emapData[61]',
              '$emapData[62]', '$emapData[63]', '$emapData[64]', '$emapData[65]', '$emapData[66]', '$emapData[67]', '$emapData[68]', '$emapData[69]',
              '$emapData[70]', '$sucursal')";
        $result = mysqli_query($con, $sql);
        if($result == FALSE){
          $error = $error+1;
        }else{
          $idRegistro++;
        }
      }
      fclose($file);
      if($error > 0){
        echo "Exito";
      }else{
        echo "Exito";
      }
    }
    mysqli_close($con);
  }else{
    echo "Error2";
  }

  
}
?>