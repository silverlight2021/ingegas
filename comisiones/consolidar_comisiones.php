<?php
session_start();
$Fecha = date("Y-m-d");
$Hora = date("G:i:s", time());

$contErrores = 0;

if($_SESSION['logged'] == 'yes'){
  require("../libraries/conexion.php");

  $consulta = "SELECT * FROM comisiones1 ORDER BY sNumero_documento";
  $resultado = mysqli_query($con, $consulta);

  if(mysqli_num_rows($resultado)>0){

    while($linea = mysqli_fetch_array($resultado)){
      $sNumero_documento = $linea["sNumero_documento"];
      $sCodigo_documento_pago = $linea["sCodigo_documento_pago"];

      $consulta1 = "SELECT * FROM comisiones WHERE sNumero_documento = '$sNumero_documento' AND sCodigo_documento_pago = '$sCodigo_documento_pago'";
      $resultado1 = mysqli_query($con, $consulta1);

      if(mysqli_num_rows($resultado1)==0){
        
        $sCliente=$linea['sCliente'];
        $sNombre_cliente=$linea['sNombre_cliente'];
        $nOrden1=$linea['nOrden1'];
        $nOrden2=$linea['nOrden2'];
        $sTipo_documento=$linea['sTipo_documento'];
        $sNumero_documento=$linea['sNumero_documento'];
        $sCodigo_documento_pago=$linea['sCodigo_documento_pago'];
        $sNro_recibo_manual=$linea['sNro_recibo_manual'];
        $sNumero_documento_pago=$linea['sNumero_documento_pago'];
        $nItem_documento_pago=$linea['nItem_documento_pago'];
        $dFecha_emision_documento=$linea['dFecha_emision_documento'];
        $dFecha_vencimiento_documento=$linea['dFecha_vencimiento_documento'];
        $dFecha_aplicacion_documento=$linea['dFecha_aplicacion_documento'];
        $sMoneda_aplicacion=$linea['sMoneda_aplicacion'];
        $sMoneda_origen_documento=$linea['sMoneda_origen_documento'];
        $nTipo_de_cambio=$linea['nTipo_de_cambio'];
        $nDebe=$linea['nDebe'];
        $nHaber=$linea['nHaber'];
        $s2DoCorte_control=$linea['s2DoCorte_control'];
        $dFecha_des=$linea['dFecha_des'];
        $sSucursal_destinataria=$linea['sSucursal_destinataria'];
        $nSaldoOrigen=$linea['nSaldoOrigen'];
        $nwSaldoPorBloque=$linea['nwSaldoPorBloque'];
        $dFecha_rechazo=$linea['dFecha_rechazo'];
        $sClienteYMoneda=$linea['sClienteYMoneda'];
        $sImprimoLineaDetalle=$linea['sImprimoLineaDetalle'];
        $sSub_business_area=$linea['sSub_business_area'];
        $sDomicilio_legal=$linea['sDomicilio_legal'];
        $sTelefono_legal=$linea['sTelefono_legal'];
        $sBanco_en_guarda=$linea['sBanco_en_guarda'];
        $sTipo_recibo_que_canjea=$linea['sTipo_recibo_que_canjea'];
        $sNumero_recibo_que_canjea=$linea['sNumero_recibo_que_canjea'];
        $nItem_recibo_que_canjea=$linea['nItem_recibo_que_canjea'];
        $sTipo_pago_que_canjea=$linea['sTipo_pago_que_canjea'];
        $nMonto_det_recibo_que_canjea=$linea['nMonto_det_recibo_que_canjea'];
        $nMonto_aplicado_que_canjea=$linea['nMonto_aplicado_que_canjea'];
        $dFecha_cobranza_que_canjea=$linea['dFecha_cobranza_que_canjea'];
        $sTipo_recibo_canjeado=$linea['sTipo_recibo_canjeado'];
        $sNumero_recibo_canjeado=$linea['sNumero_recibo_canjeado'];
        $nItem_recibo_canjeado=$linea['nItem_recibo_canjeado'];
        $sTipo_pago_canjeado=$linea['sTipo_pago_canjeado'];
        $sMotivo_rechazo_canjeado=$linea['sMotivo_rechazo_canjeado'];
        $nMonto_det_recibo_canjeado=$linea['nMonto_det_recibo_canjeado'];
        $nMonto_aplicado_canjeado=$linea['nMonto_aplicado_canjeado'];
        $dFecha_cobranza_canjeado=$linea['dFecha_cobranza_canjeado'];
        $sImprimoLineaCanje=$linea['sImprimoLineaCanje'];
        $sNumero_cheque_canjeado=$linea['sNumero_cheque_canjeado'];
        $dFecha_rechazo_canjeado=$linea['dFecha_rechazo_canjeado'];
        $dFecha_canje_canjeado=$linea['dFecha_canje_canjeado'];
        $sBanco_del_cheque_canjeado=$linea['sBanco_del_cheque_canjeado'];
        $dFecha_pago_cheque_canjeado=$linea['dFecha_pago_cheque_canjeado'];
        $sNombre_moneda=$linea['sNombre_moneda'];
        $sNumero_cheque_que_canjea=$linea['sNumero_cheque_que_canjea'];
        $sBanco_del_cheque_que_canjea=$linea['sBanco_del_cheque_que_canjea'];
        $sObservaciones_aux=$linea['sObservaciones_aux'];
        $sTipo_pago=$linea['sTipo_pago'];
        $sTipo_documento_pago=$linea['sTipo_documento_pago'];
        $sDescripcion_tipo_pago=$linea['sDescripcion_tipo_pago'];
        $sPorcentaje_tipo_pago=$linea['sPorcentaje_tipo_pago'];
        $sNombre_tarjeta=$linea['sNombre_tarjeta'];
        $sNumero_tarjeta=$linea['sNumero_tarjeta'];
        $sNro_boleta_deposito=$linea['sNro_boleta_deposito'];
        $sNumero_cuenta_dep=$linea['sNumero_cuenta_dep'];
        $sNombre_banco_dep=$linea['sNombre_banco_dep'];
        $dFecha_deposito=$linea['dFecha_deposito'];
        $sNumero_cheque=$linea['sNumero_cheque'];
        $sNombre_banco_cheque=$linea['sNombre_banco_cheque'];
        $dFecha_pago_cheque=$linea['dFecha_pago_cheque'];
        $dFecha_emision_pago=$linea['dFecha_emision_pago'];
        $dFecha_vencim_pago=$linea['dFecha_vencim_pago'];
        $dFecha_de_pago=$linea['dFecha_de_pago'];

        $consulta2 = "INSERT INTO comisiones VALUES (NULL, '$sCliente', '$sNombre_cliente', '$nOrden1', '$nOrden2', '$sTipo_documento',
                    '$sNumero_documento', '$sCodigo_documento_pago', '$sNro_recibo_manual', '$sNumero_documento_pago', '$nItem_documento_pago',
                    '$dFecha_emision_documento', '$dFecha_vencimiento_documento', '$dFecha_aplicacion_documento', '$sMoneda_aplicacion',
                    '$sMoneda_origen_documento', '$nTipo_de_cambio', '$nDebe', '$nHaber', '$s2DoCorte_control', '$dFecha_des', '$sSucursal_destinataria',
                    '$nSaldoOrigen', '$nwSaldoPorBloque', '$dFecha_rechazo', '$sClienteYMoneda', '$sImprimoLineaDetalle', '$sSub_business_area',
                    '$sDomicilio_legal', '$sTelefono_legal', '$sBanco_en_guarda', '$sTipo_recibo_que_canjea', '$sNumero_recibo_que_canjea', 
                    '$nItem_recibo_que_canjea', '$sTipo_pago_que_canjea', '$nMonto_det_recibo_que_canjea', '$nMonto_aplicado_que_canjea',
                    '$dFecha_cobranza_que_canjea', '$sTipo_recibo_canjeado', '$sNumero_recibo_canjeado', '$nItem_recibo_canjeado', '$sTipo_pago_canjeado',
                    '$sMotivo_rechazo_canjeado', '$nMonto_det_recibo_canjeado', '$nMonto_aplicado_canjeado', '$dFecha_cobranza_canjeado', 
                    '$sImprimoLineaCanje', '$sNumero_cheque_canjeado', '$dFecha_rechazo_canjeado', '$dFecha_canje_canjeado', '$sBanco_del_cheque_canjeado',
                    '$dFecha_pago_cheque_canjeado', '$sNombre_moneda', '$sNumero_cheque_que_canjea', '$sBanco_del_cheque_que_canjea', '$sObservaciones_aux',
                    '$sTipo_pago', '$sTipo_documento_pago', '$sDescripcion_tipo_pago', '$sPorcentaje_tipo_pago', '$sNombre_tarjeta', '$sNumero_tarjeta',
                    '$sNro_boleta_deposito', '$sNumero_cuenta_dep', '$sNombre_banco_dep', '$dFecha_deposito', '$sNumero_cheque', '$sNombre_banco_cheque',
                    '$dFecha_pago_cheque', '$dFecha_emision_pago', '$dFecha_vencim_pago', '$dFecha_de_pago' )";

        $resultado3 = mysqli_query($con, $consulta2);

        if($resultado3 == FALSE){
          $contErrores++;
        }


      }

    }
  }else{
    echo "1";
  }

  if($contErrores > 0){
    echo "2";
  }else{
    echo "3";
  }

}

?> 