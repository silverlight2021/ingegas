<?php
date_default_timezone_set("America/Bogota");
require ("libraries/conexion.php");
session_start();

if(@$_SESSION['logged']== 'yes')
{ 
	$valor ="";
    $acc = $_SESSION['acc'];    
    if (isset($_POST['guardar_insumo'])) 
    {
    	$fecha_inicial = isset($_REQUEST['fecha_inicial']) ? $_REQUEST['fecha_inicial'] : NULL;
    	$fecha_final = isset($_REQUEST['fecha_final']) ? $_REQUEST['fecha_final'] : "00/00/00";
    	$tipo_insumo = isset($_REQUEST['tipo_insumo']) ? $_REQUEST['tipo_insumo'] : NULL;
    	if ($tipo_insumo== "real_eto") {
    		$consumo="ETO";
    	}
    	if ($tipo_insumo== "real_co") {
    		$consumo="C02";
    	}

    	function cambiar_formato($fecha_a_cambiar)
    	{
    		$fecha=$fecha_a_cambiar;
    		$fechass = explode('/', $fecha);
    		$dia = $fechass[0];
    		$mes = $fechass[1];    		
    		$año = $fechass[2];
    		$fecha_cambiada = $año."-".$mes."-".$dia;
    		return $fecha_cambiada;
    	}	
    	
    	$fecha_inicial1= cambiar_formato($fecha_inicial); 
    	$fecha_final1= cambiar_formato($fecha_final);	

    	$fecha_inicial12 = str_replace(".", "", $fecha_inicial1);

    	$consulta  = "SELECT $tipo_insumo, SUM($tipo_insumo) FROM produccion_mezclas WHERE fech_crea >= '$fecha_inicial12' AND fech_crea <= '$fecha_final1'";
  		$resultado = mysqli_query($con,$consulta) ;
  		$linea = mysqli_fetch_array($resultado);
  		$suma1 = isset($linea["SUM($tipo_insumo)"]) ? $linea["SUM($tipo_insumo)"] : NULL;
  		$suma = round($suma1, 2);
    	$valor="<div align=\"center\"><p><strong>Informe de consumo comprendido entre las fechas ".$fecha_inicial." y ".$fecha_final."</strong></p></div>"; 
    }

require_once("inc/init.php");
require_once("inc/config.ui.php");
$page_title = "Informe Consumo ";
$page_css[] = "your_style.css";
include("inc/header.php");
$page_nav["informes"]["sub"]["consumo"]["active"] = true;
include("inc/nav.php");

?>
<style type="text/css">
  h2 {display:inline}
</style>
<style type="text/css">
	.center-row {
	display:table;
	}
	.center {
		display:table-cell;
	    vertical-align:middle;
	    float:none;
	}
</style>	
<div id="main" role="main">
	<div id="content">
		<div class="row">
			<div class="" align="center">
				<h1  class="page-title txt-color-blueDark"> <?php echo $page_title; ?></h1>
			</div>	      	
		</div>	
		<section id="widget-grid" class="">
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-6">			
					<div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">			
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h2>Informe </h2>											
						</header>
						<div>
							<div class="jarviswidget-editbox"></div>	
							<div class="widget-body no-padding">								
								<form id="checkout-form" name="form1" class="smart-form" novalidate="novalidate" action="informe_consumo.php" method="POST">
								<input type="hidden" name="valor" id="valor" value="1">
									<fieldset>
										<div class="row">
											<section class="col col-4">
												<label class="label">Fecha Inicial :</label>
												<label class="input"> 
													<input type="text" name="fecha_inicial" id="fecha_inicial "  placeholder="dd-mm-yyyy" class="form-control datepicker"  data-dateformat="dd/mm/yy" >
												</label>
											</section>
											<section class="col col-4">
												<label class="label">Fecha Final :</label>
												<label class="input"> 
													<input type="text" name="fecha_final" id="fecha_final"  placeholder="dd-mm-yyyy" class="form-control datepicker"  data-dateformat="dd/mm/yy" >
												</label>
											</section>
											<section class="col col-4">
					                            <label class="label">Tipo Insumo :</label>
					                            <select class="form-control" name="tipo_insumo" id="category">
													<option value="real_eto">ETO</option>
													<option value="real_co">CO2</option>
												</select>
					                        </section>											
										</div>
										<div class="row">
											<section class="col col-12">
												<label><h3>Fecha actual: <?php echo date("d-m-y",time()); ?></h3></label>												
											</section>											
										</div>
									</fieldset>	
									<?php
									//if (in_array(41, $acc))
									//{
									?>
									<footer>										
										<input type="submit" value="Generar" name="guardar_insumo" id="guardar_insumo" class="btn btn-primary" />
									</footer>
									<?php
									//}										
									?>
								</form>
							</div>						
						</div>				
					</div>	
				</article>				
			</div>
		</section>
		<?php
		if (isset($_POST['guardar_insumo']))
		{
		?>
			<section id="widget-grid" class="">
				<div class="row">	
					<article class="col-sm-12 col-md-12 col-lg-6">			
						<div class="jarviswidget" id="wid-id-2" data-widget-editbutton="false" data-widget-custombutton="false">			
							<header>
								<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
								<h2>Informe </h2>											
							</header>
							<div>
								<div class="jarviswidget-editbox"></div>	
								<div class="widget-body no-padding">							
									<form id="checkout-form" name="form1" class="smart-form" novalidate="novalidate" action="informe_consumo.php" method="POST">
									<input type="hidden" name="valor" id="valor" value="1">
										<fieldset>
											<?php
												echo $valor;
											?>
											<br>
											<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
												<thead>
													<tr>
														<th>Insumo</th>									
														<th>Total Kg</th>										
													</tr>
												</thead>
												<tbody>
				                                    <tr>
				                                        <td ><?php echo $consumo; ?></td>
				                                        <td ><?php echo $suma; ?></td>                   
				                                    </tr> 
												</tbody>
											</table>
											<br>
											<br>
											<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
												<form action="busqueda_cilindros_eto.php" method="POST" name="form1">
													<thead>
														<tr>
															<th>Lote </th>
															<th>Total Consumo Lote ETO </th>
															<th>Total Consumo Lote CO2 </th>
															<th>Total Cilindros </th>										
														</tr>
													</thead>
													<tbody>
													<?php
					                                    
						                    			$contador = "0";
						                    	        $consulta = "SELECT DISTINCT lote_pro FROM produccion_mezclas WHERE fech_crea >= '$fecha_inicial1' AND fech_crea <= '$fecha_final1' " ;
						                                $resultado = mysqli_query($con,$consulta) ;
						                                while ($linea = mysqli_fetch_array($resultado))
						                                {

						                                    $contador = $contador + 1;
						                                    $lote_pro = $linea["lote_pro"];
						                                    $id_lote_eto = $linea["id_lote_eto"];

						                                    $contador = "0";
							                    	        $consulta1 = "SELECT SUM(real_eto) FROM produccion_mezclas WHERE lote_pro= '$lote_pro'";
							                                $resultado1 = mysqli_query($con,$consulta1) ;
							                                while ($linea1 = mysqli_fetch_array($resultado1))
							                                {
							                                	$suma_eto = isset($linea1["SUM(real_eto)"]) ? $linea1["SUM(real_eto)"] : NULL;
							                                	$suma_eto =number_format($suma_eto, 2, ".", ",");
							                                }

							                                $consulta2 = "SELECT SUM(real_co) FROM produccion_mezclas WHERE lote_pro= '$lote_pro'";
							                                $resultado2 = mysqli_query($con,$consulta2) ;
							                                while ($linea2 = mysqli_fetch_array($resultado2))
							                                {
							                                	$suma_co2 = isset($linea2["SUM(real_co)"]) ? $linea2["SUM(real_co)"] : NULL;
							                                	$suma_co2 = number_format($suma_co2, 2, ".", ",");
							                                }

							                                $consulta3 = "SELECT COUNT(id_cilindro) FROM produccion_mezclas WHERE lote_pro= '$lote_pro'";
							                                $resultado3 = mysqli_query($con,$consulta3) ;
															$linea3 = mysqli_fetch_array($resultado3);
															$total_cilindro = isset($linea3["COUNT(id_cilindro)"]) ? $linea3["COUNT(id_cilindro)"] : NULL;
							                                   	                         
							                                ?>
						                                    <tr>
						                                        <td><?php echo $lote_pro; ?></td>
						                                        <td><?php echo $suma_eto; ?></td>
						                                        <td><?php echo $suma_co2; ?></td>
						                                        <td><?php echo $total_cilindro; ?></td>	                                                        
						                                	</tr>    
						                                <?php
						                                }
						                                mysqli_free_result($resultado);
						                            ?>
													</tbody>
												</form>
											</table>
											<footer>
												<?php
												if (in_array(13, $acc))
												{
													?>	
													<h2>Imprimir informe</h2>
													<a href="javascript:imprSelec('muestra')"><img src="img/iconos/printer_blue.png"></a>
													<?php
												}
												?>
											</footer>
										</fieldset>	
									</form>									
								</div>						
							</div>				
						</div>	
					</article>				
				</div>
			</section>
		<?php
		}
		?>	
	</div>
	<div id="muestra" style="display:none">
		<p>INGEGAS-INGENIERIA Y GASES LTDA</p>
		<label><h2 align="center">INFORME CONSUMO</h2></label>
		<table class="table table-bordered" style="border: 1px solid black;border-collapse: collapse;" align="center">							
			<thead>
				<tr>                                               
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Insumo </th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Total (Kg)</th>       
				</tr>
			</thead>
			<tbody>
                <tr class="odd gradeX">                                
                  	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $consumo; ?></td>
                  	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $suma; ?></td>
                </tr>
			</tbody>							
		</table>
		<br>
		<br>
		<table class="table table-bordered" style="border: 1px solid black;border-collapse: collapse;" align="center">							
			<thead>
				<tr>
					<th style="border: 1px solid black;border-collapse: collapse;" align="center">LOTE</th>                                                
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Total consumo Lote ETO</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Total consumo lote C02</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Total Cilindros</th>
				</tr>
			</thead>
			<tbody>													
			  	<?php
                $consulta = "SELECT DISTINCT  lote_pro FROM produccion_mezclas WHERE fech_crea >= '$fecha_inicial1' AND fech_crea <= '$fecha_final1' " ;
                $resultado = mysqli_query($con,$consulta) ;
                while ($linea = mysqli_fetch_array($resultado))
                {

                    $contador = $contador + 1;
                    $lote_pro = $linea["lote_pro"];

                    $contador = "0";
        	        $consulta1 = "SELECT SUM(real_eto) FROM produccion_mezclas WHERE lote_pro= '$lote_pro'";
                    $resultado1 = mysqli_query($con,$consulta1) ;
                    while ($linea1 = mysqli_fetch_array($resultado1))
                    {
                    	$suma_eto = isset($linea1["SUM(real_eto)"]) ? $linea1["SUM(real_eto)"] : NULL;
                    	$suma_eto = number_format($suma_eto, 2, ".", ",");
                    }

                    $consulta2 = "SELECT SUM(real_co) FROM produccion_mezclas WHERE lote_pro= '$lote_pro'";
                    $resultado2 = mysqli_query($con,$consulta2) ;
                    while ($linea2 = mysqli_fetch_array($resultado2))
                    {
                    	$suma_co2 = isset($linea2["SUM(real_co)"]) ? $linea2["SUM(real_co)"] : NULL;
                    	$suma_co2 = number_format($suma_co2, 2, ".", ",");
                    }

                    $consulta3 = "SELECT COUNT(id_cilindro) FROM produccion_mezclas WHERE lote_pro= '$lote_pro'";
                    $resultado3 = mysqli_query($con,$consulta3) ;
					$linea3 = mysqli_fetch_array($resultado3);
					$total_cilindro = isset($linea3["COUNT(id_cilindro)"]) ? $linea3["COUNT(id_cilindro)"] : NULL;
                       	                         
                    ?>
                    <tr class="odd gradeX">
                      	<td width="5" style="border: 1px solid black;border-collapse: collapse;"><?php echo $lote_pro; ?></td>                                  
                      	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $suma_eto; ?></td>
                      	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $suma_co2; ?></td>
                      	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $total_cilindro; ?></td>
                    </tr>
                <?php 
                }mysqli_free_result($resultado);                               	
                ?>
			</tbody>							
		</table>
		<br>
		<br>		
		<br>
		<label><h3>Informe de consumo comprendido entre las fechas <?php echo $fecha_inicial ?> y <?php echo $fecha_final ?></h3></label>
		<br>			
	</div>
</div>
<?php
	include("inc/footer.php");
	include("inc/scripts.php"); 
?>
<script src="/js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="/js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="/js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="/js/plugin/flot/jquery.flot.tooltip.min.js"></script>
<script src="/js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="/js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="/js/plugin/moment/moment.min.js"></script>
<script src="/js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script type="text/javascript">
function imprSelec(muestra)
{
	var ficha=document.getElementById(muestra);
	var ventimp=window.open(' ','popimpr');
	ventimp.document.write(ficha.innerHTML);
	ventimp.document.close();
	ventimp.print();
	ventimp.close();
}
</script>
<?php 

	include("inc/google-analytics.php"); 
}
else
{
    header("Location:index.php");
}
?>