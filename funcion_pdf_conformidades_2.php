<?php

require('libraries/fpdf/fpdf.php');


class PDF_Invoice extends FPDF
{
	// private variables
	var $colonnes;
	var $format;
	var $angle=0;

	// private functions
	function RoundedRect($x, $y, $w, $h, $r, $style = '') //Rectángulo con bordes redondeados 
	{
		$k = $this->k;
		$hp = $this->h;
		if($style=='F')
			$op='f';
		elseif($style=='FD' || $style=='DF')
			$op='B';
		else
			$op='S';
		$MyArc = 4/3 * (sqrt(2) - 1);
		$this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));
		$xc = $x+$w-$r ;
		$yc = $y+$r;
		$this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));

		$this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);
		$xc = $x+$w-$r ;
		$yc = $y+$h-$r;
		$this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
		$this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);
		$xc = $x+$r ;
		$yc = $y+$h-$r;
		$this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
		$this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);
		$xc = $x+$r ;
		$yc = $y+$r;
		$this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
		$this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
		$this->_out($op);
	}

	function _Arc($x1, $y1, $x2, $y2, $x3, $y3) //Curvatura del rectángulo
	{
		$h = $this->h;
		$this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
							$x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
	}

	function Rotate($angle, $x=-1, $y=-1) //Rotar un texto dentro de la pantalla
	{
		if($x==-1)
			$x=$this->x;
		if($y==-1)
			$y=$this->y;
		if($this->angle!=0)
			$this->_out('Q');
		$this->angle=$angle;
		if($angle!=0)
		{
			$angle*=M_PI/180;
			$c=cos($angle);
			$s=sin($angle);
			$cx=$x*$this->k;
			$cy=($this->h-$y)*$this->k;
			$this->_out(sprintf('q %.5F %.5F %.5F %.5F %.2F %.2F cm 1 0 0 1 %.2F %.2F cm',$c,$s,-$s,$c,$cx,$cy,-$cx,-$cy));
		}
	}

	function _endpage()
	{
		if($this->angle!=0)
		{
			$this->angle=0;
			$this->_out('Q');
		}
		parent::_endpage();
	}

	// public functions
	function sizeOfText( $texte, $largeur )
	{
		$index    = 0;
		$nb_lines = 0;
		$loop     = TRUE;
		while ( $loop )
		{
			$pos = strpos($texte, "\n");
			if (!$pos)
			{
				$loop  = FALSE;
				$ligne = $texte;
			}
			else
			{
				$ligne  = substr( $texte, $index, $pos);
				$texte = substr( $texte, $pos+1 );
			}
			$length = floor( $this->GetStringWidth($ligne));
			$res = 1 + floor( $length / $largeur) ;
			$nb_lines += $res;
		}
		return $nb_lines;
	}

	// Company Data
	function addDatosEmpresa( $nom, $adresse )
	{
		$x1 = 5;
		$y1 = 42;
		//Positionnement en bas
		$this->SetXY( $x1, $y1 );
		$this->SetFont('Arial','B',12);
		$length = $this->GetStringWidth( $nom );
		$this->Cell( $length, 2, $nom);
		$this->SetXY( $x1, $y1 + 4 );
		$this->SetFont('Arial','',10);
		$length = $this->GetStringWidth( $adresse );
		//Coordonnées de la société
		$lignes = $this->sizeOfText( $adresse, $length) ;
		$this->MultiCell($length, 4, $adresse);
	}

	// Label and number of invoice/estimate


	function fact_dev1( $nombre_empresa_ajustado )//Numero Comprobante
	{
		$r1  = $this->w - 385;
		$r2  = $r1 + 75;
		$y1  = 3; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;		
		

		$this->SetXY( $r1 + ($r2-$r1)/2 -10 , $y1 + 5 );
		$this->SetFont( "Arial", "", 10);
		$this->Cell(100,7,$nombre_empresa_ajustado, 0,0, "l");

	}
	function fact_dev2( $nombre_empresa_ajustado2 )//Numero Comprobante
	{
		$r1  = $this->w - 385;
		$r2  = $r1 + 75;
		$y1  = 8; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;		
				
		$this->SetXY( $r1 + ($r2-$r1)/2 -10 , $y1 + 5 );
		$this->SetFont( "Arial", "", 10);
		$this->Cell(100,7,$nombre_empresa_ajustado2, 0,0, "l");

	}

	
	function fact_dev3( $titulo )//Numero Comprobante
	{
		$r1  = $this->w - 564;
		$r2  = $r1 + 250;
		$y1  = 3; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;		
		
		$this->SetXY( $r1 + ($r2-$r1)/2 -10 , $y1 + 5 );
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,5,utf8_decode($titulo), 0,0, "C");

	}
	function fact_dev4( $titulo )//Numero Comprobante
	{
		$r1  = $this->w - 564;
		$r2  = $r1 + 250;
		$y1  = 8; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;		
		
		$this->SetXY( $r1 + ($r2-$r1)/2 -10 , $y1 + 5 );
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,5,utf8_decode($titulo), 0,0, "C");

	}
	function fact_dev5( $titulo )//Numero Comprobante
	{
		$r1  = $this->w - 564;
		$r2  = $r1 + 250;
		$y1  = 13; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;		
		
		$this->SetXY( $r1 + ($r2-$r1)/2 -10 , $y1 + 5 );
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,5,utf8_decode($titulo), 0,0, "C");

	}
	function fact_dev6( $nombre_empresa_ajustado )//Numero Comprobante
	{
		$r1  = $this->w - 460;
		$r2  = $r1 + 380;
		$y1  = 3; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;		
		


		$this->SetXY( $r1 + ($r2-$r1)/2 -10 , $y1 + 5 );
		$this->SetFont( "Arial", "", 10);
		$this->Cell(100,7,$nombre_empresa_ajustado, 0,0, "l");

	}
	function fact_dev7( $nombre_empresa_ajustado )//Numero Comprobante
	{
		$r1  = $this->w - 460;
		$r2  = $r1 + 380;
		$y1  = 8; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;		
		

		$this->SetXY( $r1 + ($r2-$r1)/2 -10 , $y1 + 5 );
		$this->SetFont( "Arial", "", 10);
		$this->Cell(100,7,$nombre_empresa_ajustado, 0,0, "l");

	}
	function fact_dev8( $nombre_empresa_ajustado )//Numero Comprobante
	{
		$r1  = $this->w - 385;
		$r2  = $r1 + 75;
		$y1  = 13; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;		
		

		$this->SetXY( $r1 + ($r2-$r1)/2 -10 , $y1 + 5 );
		$this->SetFont( "Arial", "", 10);
		$this->Cell(100,7,$nombre_empresa_ajustado, 0,0, "l");

	}
	function fact_dev9( $fecha )
	{
		$r1  = $this->w - 460;
		$r2  = $r1 + 380;
		$y1  = 28; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2 -10  , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("FECHA:"), 0, 0, "l");	
		
		$this->SetXY( $r1 + ($r2-$r1)/2+10  , $y1+(4.5));
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($fecha), 0,0, "l");
	}
	function fact_dev10( $fecha )
	{
		$r1  = $this->w - 460;
		$r2  = $r1 + 75;
		$y1  = 28; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2 -10 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("CLIENTE:"), 0, 0, "R");	
		
		$this->SetXY( $r1 + ($r2-$r1)/2+5  , $y1+(4.5));
		$this->SetFont( "Arial", "", 10);
		$this->Multicell(100,5,utf8_decode($fecha), 0,'J');
		//$this->Cell(10,5,utf8_decode($fecha), 0,0, "l");
	}
	function fact_dev11( $fecha )
	{
		$r1  = $this->w - 460;
		$r2  = $r1 + 380;
		$y1  = 38; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2 -10 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("RECIBE TRANSPORTE:"), 0, 0, "l");	
		
		$this->SetXY( $r1 + ($r2-$r1)/2+32  , $y1+(4.5));
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($fecha), 0,0, "l");
	}
	
	function fact_dev12( $fecha )
	{
		$r1  = $this->w - 460;
		$r2  = $r1 + 75;
		$y1  = 38; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2 -10 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("CME:"), 0, 0, "R");	
		
		$this->SetXY( $r1 + ($r2-$r1)/2+5  , $y1+(4.5));
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($fecha), 0,0, "l");
	}
	function fact_dev13()
	{
		$r1  = $this->w - 390;
		$r2  = $r1 + 380;
		$y1  = 58; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2-10 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("CONDICIONES DE INGRESO"), 0, 0, "l");	;
	}
	function fact_dev14()
	{
		$r1  = $this->w - 390;
		$r2  = $r1 + 380;
		$y1  = 100; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2-10 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("PRODUCCIÓN"), 0, 0, "l");	;
	}
	function fact_dev15($num_cili_eto,$producto,$peso_inicial,$fech_crea,$peso_salida,$tara_vacio,$observaciones,$tapa,$tapon,$presion,$saldo)
	{
		$r1  = $this->w - 460;
		$r2  = $r1 + 380;
		$y1  = 68; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2-10 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("CILÍNDRO No."), 0, 0, "l");

		$this->SetXY( $r1 + ($r2-$r1)/2-10 , $y1+20);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($num_cili_eto), 0,0, "l");

		$this->SetXY( $r1 + ($r2-$r1)/2+20 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("PRODUCTO"), 0, 0, "l");

		$this->SetXY( $r1 + ($r2-$r1)/2+25 , $y1+20);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($producto), 0,0, "C");

		$this->SetXY( $r1 + ($r2-$r1)/2+45 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("TAPA"), 0, 0, "l");

		$this->SetXY( $r1 + ($r2-$r1)/2+50 , $y1+20);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($tapa), 0,0, "C");

		$this->SetXY( $r1 + ($r2-$r1)/2+60 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("TAPON"), 0, 0, "l");	

		$this->SetXY( $r1 + ($r2-$r1)/2+65 , $y1+20);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($tapon), 0,0, "C");

		$this->SetXY( $r1 + ($r2-$r1)/2+75 , $y1);
		$this->SetFont( "Arial", "B", 10);
		$this->Multicell(25,5,"PESO ACTUAL Kg.", 0,'C');
		//$this->Cell(10,4,"PESO"."\n"."ACTUAL", 0, 0, "l");

		$this->SetXY( $r1 + ($r2-$r1)/2+81 , $y1+20);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($peso_inicial), 0,0, "C");

		$this->SetXY( $r1 + ($r2-$r1)/2+105 , $y1);
		$this->SetFont( "Arial", "B", 10);
		//$this->Cell(10,4, utf8_decode("PRESIÓN Psi"), 0, 0, "l")
		$this->Multicell(25,5,utf8_decode("PRESIÓN ACTUAL Psi."), 0,'C');

		$this->SetXY( $r1 + ($r2-$r1)/2+115, $y1+20);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($presion), 0,0, "l");

		$this->SetXY( $r1 + ($r2-$r1)/2+140 , $y1);
		$this->SetFont( "Arial", "B", 10);
		//$this->Cell(10,4, utf8_decode("SALDO PRODUCTO"), 0, 0, "l");
		$this->Multicell(30,5,utf8_decode("SALDO PRODUCTO Kg."), 0,'C');

		$this->SetXY( $r1 + ($r2-$r1)/2+150 , $y1+20);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($saldo), 0,0, "C");

		

		$this->Line(15, 60, 203, 60);
		$this->Line(15, 68, 203, 68);
		$this->Line(15, 78, 203, 78);
		$this->Line(15, 100, 203, 100);
		
		$this->Line(15,60,15,100);
		$this->Line(45,68,45,100);
		$this->Line(71,68,71,100);
		$this->Line(85,68,85,100);
		$this->Line(102,68,102,100);
		$this->Line(130,68,130,100);
		$this->Line(160,68,160,100);
		$this->Line(203,60,203,100);


		//$this->Line(285,60,285,100);


	}
	function fact_dev1515($fech_crea,$peso_salida,$tara_vacio,$lote,$num_ord,$pre_final)
	{
		$r1  = $this->w - 460;
		$r2  = $r1 + 380;
		$y1  = 108; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		

		$this->SetXY( $r1 + ($r2-$r1)/2+10 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("FECHA LLENADO"), 0, 0, "R");

		$this->SetXY( $r1 + ($r2-$r1)/2 , $y1+15);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($fech_crea), 0,0, "C");

		$this->SetXY( $r1 + ($r2-$r1)/2+20 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("LOTE"), 0, 0, "l");

		$this->SetXY( $r1 + ($r2-$r1)/2+20 , $y1+15);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($lote), 0,0, "C");

		$this->SetXY( $r1 + ($r2-$r1)/2+30 , $y1+4);
		$this->SetFont( "Arial", "B", 10);
		$this->Multicell(25,5,utf8_decode("NÚMERO ORDEN"), 0,'C');

		$this->SetXY( $r1 + ($r2-$r1)/2+38 , $y1+15);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($num_ord), 0,0, "C");

		$this->SetXY( $r1 + ($r2-$r1)/2+78 , $y1+4);
		$this->SetFont( "Arial", "B", 10);
		$this->Multicell(25,5,"PESO SALIDA Kg.", 0,'C');

		$this->SetXY( $r1 + ($r2-$r1)/2+85 , $y1+15);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($peso_salida), 0,0, "C");

		$this->SetXY( $r1 + ($r2-$r1)/2+53 , $y1+4);
		$this->SetFont( "Arial", "B", 10);
		//$this->Cell(10,4, utf8_decode("TARA VACIO"), 0, 0, "l");
		$this->Multicell(23,5,utf8_decode("TARA VACÍO Kg."), 0,'C');

		$this->SetXY( $r1 + ($r2-$r1)/2+60 , $y1+15);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($tara_vacio), 0,0, "C");

		

		

		$this->SetXY( $r1 + ($r2-$r1)/2+115 , $y1+4);
		$this->SetFont( "Arial", "B", 10);
		$this->Multicell(25,5,utf8_decode("PRESIÓN SALIDA Psi."), 0,'C');

		$this->SetXY( $r1 + ($r2-$r1)/2+120 , $y1+15);
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($pre_final), 0,0, "L");

		$this->Line(15, 103, 180, 103);
		$this->Line(15, 111, 180, 111);
		$this->Line(15, 122, 180, 122);
		$this->Line(15, 130, 180, 130);
		
		$this->Line(15,103,15,130);
		$this->Line(47,111,47,130);
		$this->Line(60,111,60,130);
		$this->Line(80,111,80,130);
		$this->Line(104,111,104,130);
		$this->Line(130,111,130,130);
		$this->Line(180,103,180,130);




	}
	function fact_dev16( $fecha )
	{
		$r1  = $this->w - 460;
		$r2  = $r1 + 380;
		$y1  = 170; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2 -10  , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("OPERARIO:"), 0, 0, "l");	
		
		$this->SetXY( $r1 + ($r2-$r1)/2+20  , $y1+(4.5));
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($fecha), 0,0, "l");
	}
	function fact_dev17( $fecha )
	{
		$r1  = $this->w - 470;
		$r2  = $r1 + 75;
		$y1  = 170; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2 -10 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("REVISA:"), 0, 0, "l");	
		
		$this->SetXY( $r1 + ($r2-$r1)/2+10  , $y1+(4.5));
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($fecha), 0,0, "l");
	}
	function fact_dev18( $fecha )
	{
		$r1  = $this->w - 460;
		$r2  = $r1 + 380;
		$y1  = 150; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2 -10  , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("OBSERVACIONES:"), 0, 0, "l");	
		
		$this->SetXY( $r1 + ($r2-$r1)/2+25  , $y1+(4.5));
		$this->SetFont( "Arial", "", 10);		
		$this->MultiCell($length, 4,utf8_decode($fecha));

	}


	
}
?>