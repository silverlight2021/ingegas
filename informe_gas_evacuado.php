<?php
date_default_timezone_set("America/Bogota");
require ("libraries/conexion.php");
session_start();

if(@$_SESSION['logged']== 'yes')
{ 
	$valor ="";
    $acc = $_SESSION['acc'];    
    if (isset($_POST['guardar_gas_eva'])) 
    {
    	$fecha_inicial = isset($_REQUEST['fecha_inicial']) ? $_REQUEST['fecha_inicial'] : NULL;
    	$fecha_final = isset($_REQUEST['fecha_final']) ? $_REQUEST['fecha_final'] : "00/00/00";

    	function cambiar_formato($fecha_a_cambiar)
    	{
    		$fecha=$fecha_a_cambiar;
    		$fechass = explode('/', $fecha);
    		$dia = $fechass[0];
    		$mes = $fechass[1];    		
    		$año = $fechass[2];
    		$fecha_cambiada = $año."-".$mes."-".$dia;
    		return $fecha_cambiada;
    	}	
    	
    	$fecha_inicial1= cambiar_formato($fecha_inicial); 
    	$fecha_final1= cambiar_formato($fecha_final);	

    	$fecha_inicial12 = str_replace(".", "", $fecha_inicial1);

    	$consulta  = "SELECT  SUM(g_evacuar) FROM pre_produccion_mezcla WHERE fecha_pre >= '$fecha_inicial12' AND fecha_pre <= '$fecha_final1'";
  		$resultado = mysqli_query($con,$consulta) ;
  		$linea = mysqli_fetch_array($resultado);
  		$g_evacuar1 = isset($linea["SUM(g_evacuar)"]) ? $linea["SUM(g_evacuar)"] : NULL;
  		$g_evacuar = round($g_evacuar1, 2);
    	$valor="<div align=\"center\"><p><strong>Informe de gas evacuado comprendido entre las fechas ".$fecha_inicial." y ".$fecha_final."</strong></p></div>"; 
    }

require_once("inc/init.php");
require_once("inc/config.ui.php");
$page_title = "Informe Gas Evacuado ";
$page_css[] = "your_style.css";
include("inc/header.php");
$page_nav["informes"]["sub"]["evacuado"]["active"] = true;
include("inc/nav.php");

?>
<style type="text/css">
  h2 {display:inline}
</style>
<style type="text/css">
	.center-row {
	display:table;
	}
	.center {
		display:table-cell;
	    vertical-align:middle;
	    float:none;
	}
</style>	
<div id="main" role="main">
	<div id="content">
		<div class="row">
			<div class="" align="center">
				<h1  class="page-title txt-color-blueDark"> <?php echo $page_title; ?></h1>
			</div>	      	
		</div>	
		<section id="widget-grid" class="">
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-6">			
					<div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">			
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h2>Informe </h2>											
						</header>
						<div>
							<div class="jarviswidget-editbox"></div>	
							<div class="widget-body no-padding">								
								<form id="checkout-form" name="form1" class="smart-form" novalidate="novalidate" action="informe_gas_evacuado.php" method="POST">
								<input type="hidden" name="valor" id="valor" value="1">
									<fieldset>
										<div class="row">
											<section class="col col-4">
												<label class="label">Fecha Inicial :</label>
												<label class="input"> 
													<input type="text" name="fecha_inicial" id="fecha_inicial "  placeholder="dd-mm-yyyy" class="form-control datepicker"  data-dateformat="dd/mm/yy" class="input-lg">
												</label>
											</section>
											<section class="col col-4">
												<label class="label">Fecha Final :</label>
												<label class="input"> 
													<input type="text" name="fecha_final" id="fecha_final"  placeholder="dd-mm-yyyy" class="form-control datepicker"  data-dateformat="dd/mm/yy" class="input-lg">
												</label>
											</section>									
										</div>
										<div class="row">
											<section class="col col-12">
												<label><h3>Fecha actual: <?php echo date("d-m-y",time()); ?></h3></label>												
											</section>											
										</div>
									</fieldset>	
									<?php
									//if (in_array(41, $acc))
									//{
									?>
									<footer>										
										<input type="submit" value="Generar" name="guardar_gas_eva" id="guardar_gas_eva" class="btn btn-primary" />
									</footer>
									<?php
									//}										
									?>
								</form>
							</div>						
						</div>				
					</div>	
				</article>				
			</div>
		</section>
		<?php
		if (isset($_POST['guardar_gas_eva']))
		{
		?>
			<section id="widget-grid" class="">
				<div class="row">	
					<article class="col-sm-12 col-md-12 col-lg-6">			
						<div class="jarviswidget" id="wid-id-2" data-widget-editbutton="false" data-widget-custombutton="false">			
							<header>
								<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
								<h2>Informe </h2>											
							</header>
							<div>
								<div class="jarviswidget-editbox"></div>	
								<div class="widget-body no-padding">								
									<form id="checkout-form" name="form1" class="smart-form" novalidate="novalidate" action="informe_consumo.php" method="POST">
									<input type="hidden" name="valor" id="valor" value="1">
										<fieldset>
											<?php
												echo $valor;
											?>
											<br>
											<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
												<thead>
													<tr>
														<th>Gas Evacuado</th>									
														<th>Total Kg</th>										
													</tr>
												</thead>
												<tbody>
				                                    <tr>
				                                        <td >ETO</td>
				                                        <td ><?php echo $g_evacuar; ?></td>                   
				                                    </tr> 
												</tbody>
											</table>
										</fieldset>	
									</form>
									<footer>
										
										<?php
										if (in_array(13, $acc))
										{
											?>	
											<h2 align="right">Imprimir informe</h2>
											<a href="javascript:imprSelec('muestra')"><img src="img/iconos/printer_blue.png" align="right"></a>
											<?php
										}
										?>									
									</footer>
								</div>						
							</div>				
						</div>	
					</article>				
				</div>
			</section>
		<?php
		}
		?>	
	</div>
	<div id="muestra" style="display: none">
		<p>INGEGAS-INGENIERIA Y GASES LTDA</p>
		<label><h2 align="center">INFORME GAS EVACUADO</h2></label>
		<table class="table table-bordered" style="border: 1px solid black;border-collapse: collapse;" align="center">							
			<thead>
				<tr>                                               
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Gas Evacuado</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Total (Kg)</th>       
				</tr>
			</thead>
			<tbody>
                <tr class="odd gradeX">                                
                  	<td style="border: 1px solid black;border-collapse: collapse;" align="center">ETO</td>
                  	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $g_evacuar; ?></td>
                </tr>
			</tbody>							
		</table>
		<br>
		<br>
		<br>
		<label><h3>Informe de consumo comprendido entre las fechas <?php echo $fecha_inicial ?> y <?php echo $fecha_final ?></h3></label>
	</div>
</div>
<?php
	include("inc/footer.php");
	include("inc/scripts.php"); 
?>
<script src="/js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="/js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="/js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="/js/plugin/flot/jquery.flot.tooltip.min.js"></script>
<script src="/js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="/js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="/js/plugin/moment/moment.min.js"></script>
<script src="/js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script type="text/javascript">
	function imprSelec(muestra)
	{
		var ficha=document.getElementById(muestra);
		var ventimp=window.open(' ','popimpr');
		ventimp.document.write(ficha.innerHTML);
		ventimp.document.close();
		ventimp.print();
		ventimp.close();
	}
</script>
<?php 

	include("inc/google-analytics.php"); 
}
else
{
    header("Location:index.php");
}
?>