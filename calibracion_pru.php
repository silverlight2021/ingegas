<?php
date_default_timezone_set("America/Bogota");

require ("libraries/conexion.php");
session_start();
if(@$_SESSION['logged']== 'yes')
{ 
	$id_user=$_SESSION['su'];
    $acc = $_SESSION['acc'];
    $letra = isset($_REQUEST['letra']) ? $_REQUEST['letra'] : NULL;
    //$id_orden = isset($_REQUEST['id_orden']) ? $_REQUEST['id_orden'] : NULL;
    $id_calibracion = isset($_REQUEST['id_calibracion']) ? $_REQUEST['id_calibracion'] : NULL;
    $contador_orden = 0;

	require_once("inc/init.php");
	require_once("inc/config.ui.php");
	$page_title = "Calibración";
	$page_css[] = "your_style.css";
	include("inc/header.php");
	$page_nav[""][""][""][""] = true;
	include("inc/nav.php");	
	$expansion5 = NULL;
?>
<?php
	$consulta1 = "SELECT Name, LastName FROM user WHERE idUser = '$id_user'";
	$resultado1 = mysqli_query($con,$consulta1);
	$linea1 = mysqli_fetch_assoc($resultado1);

	$nombre = isset($linea1["Name"]) ? $linea1["Name"] : NULL;
	$apellido = isset($linea1["LastName"]) ? $linea1["LastName"] : NULL;

	$fecha = date("Y-m-d");
	$hora = date("G:i:s",time());
	$fecha = $fecha." ".$hora;

	$consulta2 = "SELECT * FROM verificacion_metodo_pev WHERE id_verificacion = $id_calibracion";
	$resultado2 = mysqli_query($con,$consulta2);
	if(mysqli_num_rows($resultado2)>0){
		$linea2 = mysqli_fetch_assoc($resultado2);

		$temperatura_camisa = $linea2["temperatura_camisa"];
		$temperatura_cilindro = $linea2["temperatura_cilindro"];
		$diferencia_temperatura = $linea2["diferencia_temperatura"];
		$presion_mano1 = $linea2["presion_manometro1"];
		$presion_mano2 = $linea2["presion_manometro2"];
		$presion_mano3 = $linea2["presion_manometro3"];
		$presion_mano4 = $linea2["presion_manometro4"];
		$presion_mano5 = $linea2["presion_manometro5"];
		$presion_mano6 = $linea2["presion_manometro6"];
		$expansion1 = $linea2["expansion1"];
		$expansion2 = $linea2["expansion2"];
		$expansion3 = $linea2["expansion3"];
		$expansion4 = $linea2["expansion4"];
		$expansion5 = $linea2["expansion5"];
		$expansion6 = $linea2["expansion6"];
		$error1 = $linea2["error1"];
		$error2 = $linea2["error2"];
		$error3 = $linea2["error3"];
		$error4 = $linea2["error4"];
		$error5 = $linea2["error5"];
		$error6 = $linea2["error6"];
		$estado_calibracion = $linea2["estado_calibracion"];
		$rechazo = $linea2["rechazo"];
		$ipequip = $linea2["ipEquip"];
		$estanquidad = $linea2["estanquidad"];
		$contador_orden++;
	}

	$consulta3 = "SELECT MAX(id_incertidumbre) AS id_incertidumbre FROM incertidumbre";
	$resultado3 = mysqli_query($con,$consulta3);
	$linea3 = mysqli_fetch_assoc($resultado3);

	$id_incertidumbre = isset($linea3['id_incertidumbre']) ? $linea3['id_incertidumbre'] : NULL;

	$consulta4 = "SELECT incertidumbre, resolucion_escala_balanza FROM incertidumbre WHERE id_incertidumbre = '$id_incertidumbre'";
	$resultado4 = mysqli_query($con,$consulta4);
	$linea4 = mysqli_fetch_assoc($resultado4);

	$incertidumbre = isset($linea4['incertidumbre']) ? $linea4['incertidumbre'] : NULL;
	$resolucion_escala_balanza = isset($linea4['resolucion_escala_balanza']) ? $linea4['resolucion_escala_balanza'] : NULL;

	$consulta5 = "SELECT MAX(id_incertidumbre_manometro) AS id_incertidumbre_manometro FROM incertidumbre_manometro";
	$resultado5 = mysqli_query($con,$consulta5);
	$linea5 = mysqli_fetch_assoc($resultado5);

	$id_incertidumbre_manometro = isset($linea5['id_incertidumbre_manometro']) ? $linea5['id_incertidumbre_manometro'] : NULL;

	$consulta6 = "SELECT incertidumbre_manometro FROM incertidumbre_manometro WHERE id_incertidumbre_manometro = '$id_incertidumbre_manometro'";
	$resultado6 = mysqli_query($con,$consulta6);
	$linea6 = mysqli_fetch_assoc($resultado6);

	$incertidumbre_manometro = isset($linea6['incertidumbre_manometro']) ? $linea6['incertidumbre_manometro'] : NULL;

?>
<?php
	if(($expansion5!=NULL)&&($estado_calibracion == 0)){

		$incertidumbre_combinada_manometro = 26.51634716;
		
		$uExpBalanza = $incertidumbre/2;
		$uRemBalanza = 0.1/(sqrt(3));


		$va1 = (0-$presion_mano1);
		$va12 = pow($va1,2);
		$va13 = sqrt($va12/1);
		$va14 = ($va13/(sqrt(1)));
		$va15 = sqrt((pow($va14,2))+(pow($uExpBalanza,2))+(pow($uRemBalanza,2)));
		$va2 = (2000-$presion_mano2);
		$va22 = pow($va2,2);
		$va23 = sqrt($va22/1);
		$va24 = ($va23/(sqrt(1)));
		$va25 = sqrt((pow($va24,2))+(pow($uExpBalanza,2))+(pow($uRemBalanza,2)));
		$va3 = (3000-$presion_mano3);
		$va32 = pow($va3,2);
		$va33 = sqrt($va32/1);
		$va34 = ($va33/(sqrt(1)));
		$va35 = sqrt((pow($va34,2))+(pow($uExpBalanza,2))+(pow($uRemBalanza,2)));
		$va4 = (4000-$presion_mano4);
		$va42 = pow($va4,2);
		$va43 = sqrt($va42/1);
		$va44 = ($va43/(sqrt(1)));
		$va45 = sqrt((pow($va44,2))+(pow($uExpBalanza,2))+(pow($uRemBalanza,2)));
		$va5 = (5000-$presion_mano5);
		$va52 = pow($va5,2);
		$va53 = sqrt($va52/1);
		$va54 = ($va53/(sqrt(1)));
		$va55 = sqrt((pow($va54,2))+(pow($uExpBalanza,2))+(pow($uRemBalanza,2)));

		$incertidumbre_combinada_balanza = sqrt((pow($va15,2))+(pow($va25,2))+(pow($va35,2))+(pow($va45,2))+(pow($va55,2)));

		$porcentaje_manometro = ($incertidumbre_combinada_manometro*100)/11000;
		$porcentaje_balanza = ($incertidumbre_combinada_balanza*100)/2200;

		$cuadrados = pow($porcentaje_manometro,2)+pow($porcentaje_balanza,2);

		$incertidumbre_combinada_del_metodo = sqrt($cuadrados);
		$incertidumbre_expandida = $incertidumbre_combinada_del_metodo * 2;

		//echo $incertidumbre_expandida;
	}
?>
<!-- MAIN PANEL -->
<div id="main" role="main">
<?php
if (in_array(39, $acc))
{
?>
	<div id="content">
		<div class="" align="center">
			<h6  class="page-title txt-color-blueDark">VERIFICACIÓN DE LA EXACTITUD DEL SISTEMA DE PRUEBA</h6>			
		</div>
		<section id="widget-grid" class="">
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-12">					
					<!--<form method="POST" action="calibracion.php">-->
						<table  class="table table-striped table-bordered table-hover" width="100%">
							<thead>
								<tr>
									<th>Operario</th>
									<th>Fecha</th>
									<th>Hora</th>
								</tr>
							</thead>	
							<tbody>
								<tr>
									<td width="80%"><?php echo $nombre; ?></td>
									<td><?php echo $fecha; ?></td>
									<td><?php echo $hora; ?></td>
								</tr>
							</tbody>
						</table>
					<!--</form>-->
				</article>
			</div>
		</section>
		<section id="widget-grid" class="">
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-12">					
					<!--<form method="POST" action="calibracion.php">-->
						<table  class="table table-striped table-bordered table-hover" width="100%">
							<thead>
								<tr>
									<th>Material</th>
									<th>Tamaño</th>
									<th>Fabricante</th>
									<th>Modelo No.</th>
									<th>Serie No.</th>
								</tr>
							</thead>	
							<tbody>
								<tr>
									<td>Acero</td>
									<td>9" D 51" L</td>
									<td>NORRIS</td>
									<td>140-234</td>
									<td>4870778Y</td>
								</tr>
							</tbody>
						</table>
					<!--</form>-->
				</article>
			</div>
		</section>
		
		

		<section id="widget-grid" class="">
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-6">
					<table class="table table-striped table-bordered table-hover" width="100%">
						<tbody>
							<tr>
								<td>TEMP. CAMISA (°C)</td>
								<td><input type="text" class="input" name="temp_camisa" id="temp_camisa" onchange="calcularTemp()" value="<?php echo isset($temperatura_camisa) ? $temperatura_camisa : NULL; ?>" required></td>
							</tr>
							<tr>
								<td>TEMP. CILINDRO(°C)</td>
								<td><input type="text" class="input" id="temp_cilindro" name="temp_cilindro" onchange="calcularTemp()" value="<?php echo isset($temperatura_cilindro) ? $temperatura_cilindro : NULL; ?>" required></td>
							</tr>
							<tr>
								<td>DIFERENCIA (°C)</td>
								<td><input type="text" class="input" id="diferencia_temp" name="diferencia_temp" value="<?php echo isset($diferencia_temperatura) ? $diferencia_temperatura : NULL; ?>" required></td>
							</tr>
						</tbody>
					</table>
				</article>
			</div>
		</section>

		<section id="widget-grid" class="">
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-6">
					<table class="table table-striped table-bordered table-hover" width="100%">
						<tbody>
							<tr>
								<td>ESTANQUIDAD</td>
								<td>
									<input type="text" class="input" name="estanquidad1" id="estanquidad1" value="<?php echo isset($estanquidad) ? $estanquidad : NULL; ?>" required>
									<button type="button" name="toma_tiempo" id="toma_tiempo" class="btn btn-success" onclick="contar('tiempo',10)">TOMAR TIEMPO</button> 
									<button type="button" name="tom_lectura_est" id="tom_lectura_est" class="btn btn-success" onclick="estanquidad('estanquidad1')" style="display: none">LECTURA</button>
							</td>
							</tr>
						</tbody>
					</table>
				</article>
			</div>
		</section>

		
		<div class="" align="center">
			<h6  class="page-title txt-color-blueDark">DESVIACIÓN MAXIMA  PERMITIDA 1%</h6>			
		</div>		
		<section id="widget-grid" class="">
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-12">					
					<!--<form method="POST" action="calibracion.php">-->
						<table  class="table table-striped table-bordered table-hover" width="100%">
							<thead>
								<tr>
									<th>PRESIÓN DE REFERENCIA</th>
									<th>EXPANSIÓN DE REFERENCIA</th>
									<th>EXPANSIÓN</th>
									<th>ERROR ABSOLUTO (%)</th>
									<th>GUARDAR</th>
								</tr>
							</thead>	
							<tbody>
								<tr>
									<form id="form1" name="form1" method="post" action="guardarVerificacion.php">
										<td>0</td>
										<td>0,0</td>
										
										<td>
											<input type="text" name="expancion1" class="input" id="expancion1" value="<?php echo isset($expansion1) ? $expansion1 : NULL; ?>">
											<?php
												if($contador_orden==0){
											?>
											<button type="button" name="toma_tiempo1" id="toma_tiempo1" class="btn btn-success" onclick="contar('tiempo',30)">TOMAR TIEMPO</button> 
											<button type="button" name="tom_lectura1" id="tom_lectura1" class="btn btn-success" onclick="validar(1)" style="display: none">LECTURA</button> 
											<?php
												}
											?>
										</td>
										<td>
											<input type="text" name="error_absoluto1" class="input" id="error_absoluto1" readonly value="<?php echo isset($error1) ? $error1 : NULL; ?>"> 
											<input type="hidden" name="idPresion" value="1">
										</td>
										<td>
											<?php
												if($contador_orden==0){
											?>
												<input type="hidden" name="presion_mano1" class="input" id="presion_mano1" value="0" readonly>
												<input type="hidden" name="id_user" value="<?php echo $id_user; ?>">
												<input type="hidden" name="id_calibra" value="<?php echo $id_calibracion; ?>">
												<input type="hidden" name="temperatura_camisa" id="temperatura_camisa">
												<input type="hidden" name="temperatura_cilindro" id="temperatura_cilindro" >
												<input type="hidden" name="diferencia_temperatura" id="diferencia_temperatura">
												<input type="hidden" name="estanquidad" id="estanquidad">
												<input type="hidden" name="idPresion" value="1">
												<input type="submit" value="Guardar" name="guardar1" id="guardar1">
											<?php
												}
											?>
										</td>
									</form>
								</tr><!-- ----------------------------1000----------------------------- -->
								<tr>
									<form id="form2" name="form2" method="post" action="guardarVerificacion.php">
										<td>2000</td>
										<td>86.5</td>
										
										<td>
											<input type="text" class="input"  name="expancion2" id="expancion2" value="<?php echo isset($expansion2) ? $expansion2 : NULL; ?>">
											<?php
												if(($expansion1!=NULL)&&($expansion2==NULL)&&($rechazo!= 1)){
											?>
												<button type="button" name="toma_tiempo1" id="toma_tiempo1" class="btn btn-success" onclick="contar('tiempo',30)">TOMAR TIEMPO</button> 
											<button type="button" name="tom_lectura1" id="tom_lectura1" class="btn btn-success" onclick="validar(2)" style="display: none">LECTURA</button> 
											<?php
												}
											?>
										</td>
										<td>
											<input type="text" class="input" name="error_absoluto2" value="<?php echo isset($error2) ? $error2 : NULL; ?>" id="error_absoluto2">
										</td>
										<td>
											<?php
												if(($expansion1!=NULL)&&($expansion2==NULL)&&($rechazo!= 1)){
											?>
											<input type="hidden" class="input" name="presion_mano2" value="2000" readonly>
											<input type="hidden" name="idPresion" value="2">
											<input type="hidden" name="id_calibra" value="<?php echo $id_calibracion; ?>">
											<input type="hidden" name="rechazada2" id="rechazada2">
											<input type="hidden" name="ipEquip" id="ipEquip" value="<?php echo $ipequip; ?>">
											<input type="submit" value="Guardar" name="guardar2" id="guardar2" onclick="validar_error(2)">
											<?php
												}
											?>
										</td>
									</form>
								</tr><!-- ----------------------------2000----------------------------- -->
								<tr>
									<form id="form3" name="form3" method="post" action="guardarVerificacion.php">
										<td>3000</td>
										<td>130,0</td>
										
										<td>
											<input type="text" class="input" name="expancion3" id="expancion3" value="<?php echo isset($expansion3) ? $expansion3 : NULL; ?>">
											<?php
												if(($expansion2!=NULL)&&($expansion3==NULL)&&($rechazo!= 1)){
											?>
											<button type="button" name="toma_tiempo1" id="toma_tiempo1" class="btn btn-success" onclick="contar('tiempo',30)">TOMAR TIEMPO</button> 
											<button type="button" name="tom_lectura1" id="tom_lectura1" class="btn btn-success" onclick="validar(3)" style="display: none">LECTURA</button> 
											<?php
												}
											?>
										</td>
										<td><input type="text" class="input" name="error_absoluto3" value="<?php echo isset($error3) ? $error3 : NULL; ?>" id="error_absoluto3" ></td>
										<td>
											<?php
												if(($expansion2!=NULL)&&($expansion3==NULL)&&($rechazo!= 1)){
											?>
											<input type="hidden" class="input" name="presion_mano3" value="3000" id="presion_mano3" readonly>
											<input type="hidden" name="idPresion" value="3">
											<input type="hidden" name="id_calibra" value="<?php echo $id_calibracion; ?>">
											<input type="hidden" name="rechazada3" id="rechazada3">
											<input type="hidden" name="ipEquip" id="ipEquip" value="<?php echo $ipequip; ?>">
											<input type="submit" value="Guardar" name="guardar3" id="guardar3" onclick="validar_error(3)">
											<?php
												}
											?>
										</td>
									</form>
								</tr><!-- ----------------------------3000----------------------------- -->
								<tr>
									<form id="form4" name="form4" method="post" action="guardarVerificacion.php">
										<td>4000</td>
										<td>173,5</td>
										
										<td>
											<input type="text" class="input" name="expancion4" id="expancion4" value="<?php echo isset($expansion4) ? $expansion4 : NULL; ?>">
											<?php
												if(($expansion3!=NULL)&&($expansion4==NULL)&&($rechazo!= 1)){
											?>
											<button type="button" name="toma_tiempo1" id="toma_tiempo1" class="btn btn-success" onclick="contar('tiempo',30)">TOMAR TIEMPO</button> 
											<button type="button" name="tom_lectura1" id="tom_lectura1" class="btn btn-success" onclick="validar(4)" style="display: none">LECTURA</button> 
											<?php
												}
											?>
										</td>
										<td><input type="text" class="input" name="error_absoluto4" value="<?php echo isset($error4) ? $error4 : NULL; ?>" id="error_absoluto4" ></td>
										<td>
											<?php
												if(($expansion3!=NULL)&&($expansion4==NULL)&&($rechazo!= 1)){
											?>
											<input type="hidden" class="input" name="presion_mano4" value="4000" readonly id="presion_mano4">
											<input type="hidden" name="idPresion" value="4">
											<input type="hidden" name="id_calibra" value="<?php echo $id_calibracion; ?>">
											<input type="hidden" name="rechazada4" id="rechazada4">
											<input type="hidden" name="ipEquip" id="ipEquip" value="<?php echo $ipequip; ?>">
											<input type="submit" value="Guardar" name="guardar4" id="guardar4" onclick="validar_error(4)">
											<?php
												}
											?>
										</td>
									</form>
								</tr><!-- ----------------------------4000----------------------------- -->
								<tr>
									<form id="form5" name="form5" method="post" action="guardarVerificacion.php">
										<td>5000</td>
										<td>217,0</td>
										
										<td>
											<input type="text" class="input" name="expancion5" id="expancion5" value="<?php echo isset($expansion5) ? $expansion5 : NULL; ?>">
											<?php
												if(($expansion4!=NULL)&&($expansion5==NULL)&&($rechazo!= 1)){
											?>
											<button type="button" name="toma_tiempo1" id="toma_tiempo1" class="btn btn-success" onclick="contar('tiempo',30)">TOMAR TIEMPO</button> 
											<button type="button" name="tom_lectura1" id="tom_lectura1" class="btn btn-success" onclick="validar(5)" style="display: none">LECTURA</button> 
											<?php
												}
											?>
										</td>
										<td><input type="text" class="input" name="error_absoluto5" value="<?php echo isset($error5) ? $error5 : NULL; ?>" id="error_absoluto5" ></td>
										<td>
											<?php
												if(($expansion4!=NULL)&&($expansion5==NULL)&&($rechazo!= 1)){
											?>
											<input type="hidden" class="input" name="presion_mano5" value="5000" readonly id="presion_mano5">
											<input type="hidden" name="idPresion" value="5">
											<input type="hidden" name="id_calibra" value="<?php echo $id_calibracion; ?>">
											<input type="hidden" name="rechazada5" id="rechazada5">
											<input type="hidden" name="ipEquip" id="ipEquip" value="<?php echo $ipequip; ?>">
											<input type="submit" value="Guardar" name="guardar5" id="guardar5" onclick="validar_error(5)">
											<?php
												}
											?>
										</td>
									</form>
								</tr><!-- ----------------------------5000----------------------------- -->
								<tr>
									<form id="form5" name="form5" method="post" action="guardarVerificacion.php">
										<td>6000</td>
										<td>260,5</td>
										
										<td>
											<input type="text" class="input" name="expancion6" id="expancion6" value="<?php echo isset($expansion6) ? $expansion6 : NULL; ?>">
											<?php
												if(($expansion5!=NULL)&&($expansion6==NULL)&&($rechazo!= 1)){
											?>
											<button type="button" name="toma_tiempo1" id="toma_tiempo1" class="btn btn-success" onclick="contar('tiempo',30)">TOMAR TIEMPO</button> 
											<button type="button" name="tom_lectura1" id="tom_lectura1" class="btn btn-success" onclick="validar(6)" style="display: none">LECTURA</button> 
											<?php
												}
											?>
										</td>
										<td><input type="text" class="input" name="error_absoluto6" value="<?php echo isset($error6) ? $error6 : NULL; ?>" id="error_absoluto6" ></td>
										<td>
											<?php
												if(($expansion5!=NULL)&&($expansion6==NULL)&&($rechazo!= 1)){
											?>
											<input type="hidden" class="input" name="presion_mano6" value="6000" readonly id="presion_mano6">
											<input type="hidden" name="idPresion" value="6">
											<input type="hidden" name="id_calibra" value="<?php echo $id_calibracion; ?>">
											<input type="hidden" name="rechazada6" id="rechazada6">
											<input type="hidden" name="ipEquip" id="ipEquip" value="<?php echo $ipequip; ?>">
											<input type="submit" value="Guardar" name="guardar6" id="guardar6" onclick="validar_error(6)">
											<?php
												}
											?>
										</td>
									</form>
								</tr><!-- ----------------------------6000----------------------------- -->
							</tbody>
						</table>
					<!--</form>-->
				</article>
			</div>
		</section>
		<form id="form9" name="form9" method="post" action="guardarVerificacion.php">
				
		<!--<section id="widget-grid" class="">
			<div class="row">						
				<article class="col-sm-12 col-md-12 col-lg-6">
					<table class="table table-striped table-bordered table-hover" width="100%">
						<tr>
							<input type="hidden" name="error_total" id="error_total">
							
							<?php
								if($expansion5!=NULL){
							?>
								<td style="text-align: center;"><input type="submit" name="guardarCalibracion" id="guardarCalibracion" value="Guardar"></td>
								<div style="text-align: center;background: red;cursor: pointer;" id="calculoErrores" onclick="calcularTotalErrores()">Calcular Errores Absolutos</div>
							<?php
								}
							?>
						</tr>		
					</table>
				</article>
			</div>
		</section>-->

		<section id="widget-grid" class="">
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-6">
					<table class="table table-striped table-bordered table-hover" width="100%">
						<tbody>
							<tr>
								<td>Factor de confiabilidad - K	</td>
								<td>2</td>
							</tr>
						</tbody>
					</table>
				</article>
				<article class="col-sm-12 col-md-12 col-lg-6">
					<form id="form11" name="form11" method="post" action="guardarVerificacion.php">
						<table  class="table table-striped table-bordered table-hover" width="100%">
							<tr>
								<?php
									if(($expansion6!=NULL)&&($estado_calibracion == 0)&&($rechazo!=1)){
								?>
								<input type="hidden" value="7" name="idPresion">
								<input type="hidden" value='<?php echo $id_calibracion; ?>' name="id_calibra">
								<input type="hidden" name="incertidumbre_expandida" value="<?php echo isset($incertidumbre_expandida) ? $incertidumbre_expandida : NULL; ?>">
								<td><div class="" align="center"><input type="submit" name="guardarCalibracion" id="guardarCalibracion" value="Finalizar Verificación"></div></td>
								<?php
									}
								?>
							</tr>						
						</table>
					</form>			
					
				</article>
			</div>
		</section>
		</form>

		<section id="widget-grid" class="">
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-6">
					<table class="table table-striped table-bordered table-hover" width="100%">
						<tbody>
						</tbody>
					</table>
				</article>
				<article class="col-sm-12 col-md-12 col-lg-6">
					<form id="form12" name="form12" method="post" action="guardarVerificacion.php">
						<table  class="table table-striped table-bordered table-hover" width="100%">
							<tr>
								<?php
									if($estado_calibracion==1){
										if(in_array(75, $acc)){
								?>
								<input type="hidden" value="1" name="aprobacion">

								<input type="hidden" value='<?php echo $id_calibracion; ?>' name="id_calibra">
								<input type="hidden" name="ipEquip" id="ipEquip" value="<?php echo $ipequip; ?>">
								<td><div class="" align="center"><input type="submit" name="aprobarCalibracion" id="aprobarCalibracion" value="Aprobar Verificación"></div></td>
								<?php
										}else{
											?>
											<td><div class="" align="center">PENDIENTE APROBACIÓN DE CALIDAD</div>
											<?php
										}
									}
								?>
							</tr>						
						</table>
					</form>			
					
				</article>
			</div>
		</section>

		<?php
			if(($rechazo==1)&&($estado_calibracion!=2)){
				?>
				<form id="form10" name="form10" method="post" action="guardarVerificacion.php">
					<section id="widget-grid" class="">
						<div class="row">
							<article class="col-sm-12 col-md-12 col-lg-6">
								<table class="table table-striped table-bordered table-hover" width="100%">
									<tbody>
										<tr>
											<input type="hidden" name="id_calibra" id="id_calibra" value="<?php echo $id_calibracion; ?>">
											<input type="hidden" name="finalizacion_rechazo" id="finalizacion_rechazo" value="1">
											<td align="center"><input type="submit" name="guardar10" value="Finalizar verificación Rechazada"></td>
										</tr>
									</tbody>
								</table>
							</article>
						</div>
					</section>
				</form>
				<?php
			}
		?>
	</div>			
<?php
}										
?>
<div class="modal fade" id="modal_info_tiempo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">Espere 30 Segundos</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
					 			<p id='CuentaAtras' style="font-size: 70px;text-align: center;" align="center">30</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<script type="text/javascript">
	function validar_error(valor){

		campo = valor;

		error = document.getElementById('error_absoluto'+campo).value;

		if(error>1){

			if(confirm("El valor del error Absoluto es mayor a 1%, la verificación de la exactitud será rechazada")){
				document.getElementById('rechazada'+valor).value = 1;
				alert("Prueba Calibración rechazada");
			}
		}

	}
</script>
<script type="text/javascript">
	function contar(tiempo,seg){
	$('#modal_info_'+tiempo).modal();
	updateReloj(seg,seg);
}
</script>

<script type="text/javascript"> 
var totalTiempo=0; 
function updateReloj(seg)
{
	totalTiempo = seg;
	var prueba = 0;
	document.getElementById('CuentaAtras').innerHTML = totalTiempo; 
	if(totalTiempo == 0)
	{
		if(seg==30){
			$('#modal_info_tiempo').modal('hide');
			document.getElementById('toma_tiempo').style.display = "none";
			document.getElementById('tom_lectura').style.display = "block";
		}else{
			prueba1 = document.getElementById('estanquidad1').value;
			prueba = prueba1.length;
			console.log(prueba);
			if(prueba == 0){
				$('#modal_info_tiempo').modal('hide');
				document.getElementById('toma_tiempo').style.display = "none";
				document.getElementById('tom_lectura_est').style.display = "block";
				prueba = 1;
			}else{
				$('#modal_info_tiempo').modal('hide');
				document.getElementById('toma_tiempo1').style.display = "none";
				document.getElementById('tom_lectura1').style.display = "block";
			}
		}
		
	}
	else
	{            
	    totalTiempo-=1;
	    setTimeout("updateReloj(totalTiempo)",1000);
	}
}
</script>

</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->
<?php
	include("inc/footer.php");
	include("inc/scripts.php"); 
?>
<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<script>
	var t = document.getElementById("tom_lectura1");
	t.disabled = true;
	t.addEventListener("mousemove", setTimeout('desbloquear()',6000),false);

	function desbloquear(){
		t.disabled = false;
	}



	function validar(campo){
		var tmp;
		var newcad;
		/*var cadena = "";
		var arrayData = new Array();
		var archivoTxt = new XMLHttpRequest();
		var fileRuta = 'Uploads/CambioArchivo.txt';
		archivoTxt.open("GET",fileRuta,false);
		archivoTxt.send(null);
		var txt = archivoTxt.responseText;
		for (var i = 0; i<txt.length; i++){
			arrayData.push(txt[i]);
		}
		arrayData.forEach(function(data){
			cadena += data;
		});
		console.log(cadena);
		var lect = document.getElementById('expancion'+campo);
		lect.value=cadena;
		cadena="";*/
		$.ajax({
			url: 'leer_balanza.php',
			type: 'POST',
			data: 'prueba'
		})
		.done(function(data){
			var regex = /(\d+)/g;
			var name = data;
			var final = name.match(regex);
			$("input[name=expancion"+campo+"]").val(final);
			calcularDesviacion(campo);
		})
		.fail(function(){
			console.log("Error");
		});
		

	}

	function estanquidad(campo){
		var tmp;
		var newcad;
		/*var cadena = "";
		var arrayData = new Array();
		var archivoTxt = new XMLHttpRequest();
		var fileRuta = 'Uploads/CambioArchivo.txt';
		archivoTxt.open("GET",fileRuta,false);
		archivoTxt.send(null);
		var txt = archivoTxt.responseText;
		for (var i = 0; i<txt.length; i++){
			arrayData.push(txt[i]);
		}
		arrayData.forEach(function(data){
			cadena += data;
		});
		console.log(cadena);
		var lect = document.getElementById(campo);
		lect.value=cadena;
		var lect = document.getElementById('estanquidad');
		lect.value=cadena;
		cadena="";*/
		$.ajax({
			url: 'leer_balanza.php',
			type: 'POST',
			data: 'prueba'
		})
		.done(function(data){
			$("input[name=estanquidad]").val(data);
			$("input[name="+campo+"]").val(data);
		})
		.fail(function(){
			console.log("Error");
		});
		//calcularDesviacion(campo);

	}
</script>
<script type="text/javascript">
	function calcularDesviacion(item){

		var expa = document.getElementById('presion_mano'+item);
		var mano = document.getElementById('expancion'+item);
		var desvia = document.getElementById('error_absoluto'+item);
		var valor_expa = mano.value;
		//var valor_mano = mano.value;
		var valor_desvia = desvia.value;
		var valor = 0;
		var exp_cil_gal;

		if(item==1){
			exp_cil_gal = 0;
		}else if(item==2){
			exp_cil_gal = 86.5;
		}else if(item==3){
			exp_cil_gal = 130.0;
		}else if(item==4){
			exp_cil_gal = 173.5;
		}else if(item==5){
			exp_cil_gal = 217.0;
		}else if(item==6){
			exp_cil_gal = 260.5;
		}
        
        valor_expa1 = parseFloat(valor_expa);
        
		valor = ((exp_cil_gal-valor_expa1)/exp_cil_gal)*100;

		if(item == 1){
			valor = 0;
		}
        
        if(valor<0){
            valor = valor*-1;
        }
        

		//alert("(("+exp_cil_gal+"-"+valor_expa1+")/"+exp_cil_gal+")*100");
        
		valor = valor.toFixed(2);

		desvia.value = valor;
		

	}

	function calcularTotalErrores(){

		error1 = parseFloat(document.getElementById('error_absoluto1').value);
		error2 = parseFloat(document.getElementById('error_absoluto2').value);
		error3 = parseFloat(document.getElementById('error_absoluto3').value);
		error4 = parseFloat(document.getElementById('error_absoluto4').value);
		error5 = parseFloat(document.getElementById('error_absoluto5').value);

		totalDesviacion = error1+error2+error3+error4+error5;
		totalDesviacion = totalDesviacion.toFixed(2)

		document.getElementById('error_total').value = totalDesviacion;
		document.getElementById('calculoErrores').style.display = "none";


	}
</script>
<script type="text/javascript">
	function digita_desviacion(valor_referencia){

		document.getElementById('expa8').value = valor_referencia;
		document.getElementById('expa7').value = valor_referencia;
		document.getElementById('expa6').value = valor_referencia;
		document.getElementById('expa5').value = valor_referencia;
		document.getElementById('expa4').value = valor_referencia;
		document.getElementById('expa3').value = valor_referencia;
		document.getElementById('expa2').value = valor_referencia;
		document.getElementById('expa1').value = valor_referencia;
		

	}
</script>
<script type="text/javascript">
	function calcularDesviacionMetodo(){
		
		var sumatoria = 0;
		var dato;
		<?php
			if($id_calibracion != 0){
				$consulta7 = "SELECT contador_presion FROM calibracion WHERE id_calibracion = '$id_calibracion'";
				$resultado7 = mysqli_query($con,$consulta7);
				$linea7 = mysqli_fetch_assoc($resultado7);

				$contador_presion = isset($linea7['contador_presion']) ? $linea7['contador_presion'] : NULL;

				$contador_campo = $contador_presion+1;
			}
			
		?>
		
		for(var i=1; i< <?php echo $contador_presion+1; ?>; i++){
			
			dato = parseFloat(document.getElementById('desvia'+i).value);
			sumatoria = sumatoria+dato;
			
		}
		i = i-1;

		var desviacion = sumatoria/i;

		desviacion = desviacion.toFixed(2);
		
		document.getElementById('guardar'+<?php echo $contador_campo; ?>).disabled = true;
		
		document.getElementById('desviacionT').value = desviacion;
	}
</script>
<script type="text/javascript">
	function calcularTemp(){
		var temp_cilindro = document.getElementById('temp_cilindro').value;
		var temp_camisa = document.getElementById('temp_camisa').value;

		var dif = temp_camisa-temp_cilindro;
		if(dif<0){
			dif = dif*-1;
		}

		if((dif>=2.2)&&(temp_cilindro != "")&&(temp_camisa != "")){
			alert("La diferencia de temperaturas es mayor a 2.2 por favor espere a que las temperaturas se estabilicen");
			document.getElementById('diferencia_temp').value = 0;
			document.getElementById('diferencia_temperatura').value = 0;
			document.getElementById('temp_camisa').value = "";
			document.getElementById('temp_cilindro').value = "";
		}else{
			document.getElementById('diferencia_temp').value = dif;
			document.getElementById('diferencia_temperatura').value = dif;
			document.getElementById('temperatura_camisa').value = temp_camisa;
			document.getElementById('temperatura_cilindro').value = temp_cilindro;
		}

		
	}
</script>
<script type="text/javascript">
	function calcularTotal() {
		
		var totalDesviacion;
		var Xa = parseFloat(document.getElementById('incertidumbre_manometro').value);
		var Xb = parseFloat(document.getElementById('incertidumbre_bascula').value);
		var Xc = parseFloat(document.getElementById('desviacionT').value);

		var Xa2 = Math.pow(Xa,2);
		var Xb2 = Math.pow(Xb,2);
		var Xc2 = Math.pow(Xc,2);

		var X = Xa2+Xb2+Xc2;
		var totalDesviacion = Math.sqrt(X);
		totalDesviacion = totalDesviacion.toFixed(2);

		document.getElementById('incertidumbre_total').value = totalDesviacion;
		
	}
</script>
<script type="text/javascript">
// DO NOT REMOVE : GLOBAL FUNCTIONS!

$(document).ready(function() {
	
	/* // DOM Position key index //
		
	l - Length changing (dropdown)
	f - Filtering input (search)
	t - The Table! (datatable)
	i - Information (records)
	p - Pagination (paging)
	r - pRocessing 
	< and > - div elements
	<"#id" and > - div with an id
	<"class" and > - div with a class
	<"#id.class" and > - div with an id and class
	
	Also see: http://legacy.datatables.net/usage/features
	*/	

	/* BASIC ;*/
		var responsiveHelper_dt_basic = undefined;
		var responsiveHelper_datatable_fixed_column = undefined;
		var responsiveHelper_datatable_col_reorder = undefined;
		var responsiveHelper_datatable_tabletools = undefined;
		
		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};

		$('#dt_basic').dataTable({
			"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
			"autoWidth" : true,
			"preDrawCallback" : function() {
				// Initialize the responsive datatables helper once.
				if (!responsiveHelper_dt_basic) {
					responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
				}
			},
			"rowCallback" : function(nRow) {
				responsiveHelper_dt_basic.createExpandIcon(nRow);
			},
			"drawCallback" : function(oSettings) {
				responsiveHelper_dt_basic.respond();
			}
		});

	/* END BASIC */
	
	/* COLUMN FILTER  */
    var otable = $('#datatable_fixed_column').DataTable({
    	//"bFilter": false,
    	//"bInfo": false,
    	//"bLengthChange": false
    	//"bAutoWidth": false,
    	//"bPaginate": false,
    	//"bStateSave": true // saves sort state using localStorage
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_fixed_column) {
				responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_fixed_column.respond();
		}		
	
    });
    
    // custom toolbar
    $("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');
    	   
    // Apply the filter
    $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {
    	
        otable
            .column( $(this).parent().index()+':visible' )
            .search( this.value )
            .draw();
            
    } );
    /* END COLUMN FILTER */   

	/* COLUMN SHOW - HIDE */
	$('#datatable_col_reorder').dataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_col_reorder) {
				responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_col_reorder.respond();
		}			
	});
	
	/* END COLUMN SHOW - HIDE */

	/* TABLETOOLS */
	$('#datatable_tabletools').dataTable({
		
		// Tabletools options: 
		//   https://datatables.net/extensions/tabletools/button_options
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        "oTableTools": {
        	 "aButtons": [
             "copy",
             "csv",
             "xls",
                {
                    "sExtends": "pdf",
                    "sTitle": "SmartAdmin_PDF",
                    "sPdfMessage": "SmartAdmin PDF Export",
                    "sPdfSize": "letter"
                },
             	{
                	"sExtends": "print",
                	"sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>"
            	}
             ],
            "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
        },
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_tabletools) {
				responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_tabletools.respond();
		}
	});
	
	/* END TABLETOOLS */

})

</script>
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script type="text/javascript">

 
		
		$(document).ready(function() {
			
			// menu
			$("#menu").menu();
		
			/*
			 * AUTO COMPLETE AJAX
			 */
		
			function log(message) {
				$("<div>").text(message).prependTo("#log");
				$("#log").scrollTop(0);
			}
		
			$("#city").autocomplete({
				source : function(request, response) {
					$.ajax({
						url : "http://ws.geonames.org/searchJSON",
						dataType : "jsonp",
						data : {
							featureClass : "P",
							style : "full",
							maxRows : 12,
							name_startsWith : request.term
						},
						success : function(data) {
							response($.map(data.geonames, function(item) {
								return {
									label : item.name + (item.adminName1 ? ", " + item.adminName1 : "") + ", " + item.countryName,
									value : item.name
								}
							}));
						}
					});
				},
				minLength : 2,
				select : function(event, ui) {
					log(ui.item ? "Selected: " + ui.item.label : "Nothing selected, input was " + this.value);
				}
			});
		
			/*
			 * Spinners
			 */
			$("#spinner").spinner();
			$("#spinner-decimal").spinner({
				step : 0.01,
				numberFormat : "n"
			});
		
			$("#spinner-currency").spinner({
				min : 5,
				max : 2500,
				step : 25,
				start : 1000,
				numberFormat : "C"
			});
		
			/*
			 * CONVERT DIALOG TITLE TO HTML
			 * REF: http://stackoverflow.com/questions/14488774/using-html-in-a-dialogs-title-in-jquery-ui-1-10
			 */
			$.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
				_title : function(title) {
					if (!this.options.title) {
						title.html("&#160;");
					} else {
						title.html(this.options.title);
					}
				}
			}));
		
		
			/*
			* DIALOG SIMPLE
			*/
		
			// Dialog click
			$('#dialog_link').click(function() {
				$('#dialog_simple').dialog('open');
				return false;
		
			});
		
			$('#dialog_simple').dialog({
				autoOpen : false,
				width : 600,
				resizable : false,
				modal : true,
				title : "<div class='widget-header'><h4><i class='fa fa-warning'></i> Empty the recycle bin?</h4></div>",
				buttons : [{
					html : "<i class='fa fa-trash-o'></i>&nbsp; Delete all items",
					"class" : "btn btn-danger",
					click : function() {
						$(this).dialog("close");
					}
				}, {
					html : "<i class='fa fa-times'></i>&nbsp; Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
					}
				}]
			});
		
			/*
			* DIALOG HEADER ICON
			*/
		
			// Modal Link
			$('#modal_link').click(function() {
				$('#dialog-message').dialog('open');
				return false;
			});
		
			$("#dialog-message").dialog({
				autoOpen : false,
				modal : true,
				title : "<div class='widget-header'><h4><i class='icon-ok'></i> jQuery UI Dialog</h4></div>",
				buttons : [{
					html : "Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
					}
				}]
		
			});
		
			/*
			 * Remove focus from buttons
			 */
			$('.ui-dialog :button').blur();
		
			/*
			 * Just Tabs
			 */
		
			$('#tabs').tabs();
		
			/*
			 *  Simple tabs adding and removing
			 */
		
			$('#tabs2').tabs();
		
			// Dynamic tabs
			var tabTitle = $("#tab_title"), tabContent = $("#tab_content"), tabTemplate = "<li style='position:relative;'> <span class='air air-top-left delete-tab' style='top:7px; left:7px;'><button class='btn btn-xs font-xs btn-default hover-transparent'><i class='fa fa-times'></i></button></span></span><a href='#{href}'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; #{label}</a></li>", tabCounter = 2;
		
			var tabs = $("#tabs2").tabs();
		
			// modal dialog init: custom buttons and a "close" callback reseting the form inside
			var dialog = $("#addtab").dialog({
				autoOpen : false,
				width : 600,
				resizable : false,
				modal : true,
				buttons : [{
					html : "<i class='fa fa-times'></i>&nbsp; Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
		
					}
				}]
			});
		
			// addTab form: calls addTab function on submit and closes the dialog
			var form = dialog.find("form").submit(function(event) {
				addTab();
				dialog.dialog("close");
				
			});
		
			// actual addTab function: adds new tab using the input from the form above

			
			function addTab() 
			{
				
				var label = tabTitle.val() || "Tab " + tabCounter, id = "tabs-" + tabCounter, li = $(tabTemplate.replace(/#\{href\}/g, "#" + id).replace(/#\{label\}/g, label)), tabContentHtml = tabContent.val() || "Tab " + tabCounter + " content.";
		
				tabs.find(".ui-tabs-nav").append(li);
				tabs.append("<div id='" + id + "'><p>" + tabContentHtml + "</p></div>");
				tabs.tabs("refresh");
				tabCounter++;
		
				// clear fields
				$("#tab_title").val("");
				$("#tab_content").val("");
			}
		
			// addTab button: just opens the dialog
			$("#add_tab").button().click(function() {
				var idd = document.getElementById('num').firstChild.nodeValue;

				
				dialog.dialog("open");
			});
		
			// close icon: removing the tab on click
					
			/*
			* ACCORDION
			*/
			//jquery accordion
			
		     var accordionIcons = {
		         header: "fa fa-plus",    // custom icon class
		         activeHeader: "fa fa-minus" // custom icon class
		     };
		     
			$("#accordion").accordion({
				autoHeight : false,
				heightStyle : "content",
				collapsible : true,
				animate : 300,
				icons: accordionIcons,
				header : "h4",
			})
		
			/*
			 * PROGRESS BAR
			 */
			$("#progressbar").progressbar({
		     	value: 25,
		     	create: function( event, ui ) {
		     		$(this).removeClass("ui-corner-all").addClass('progress').find(">:first-child").removeClass("ui-corner-left").addClass('progress-bar progress-bar-success');
				}
			});			

		})

	</script>
	


<?php 
	//include footer
	include("inc/google-analytics.php"); 
}
else
{
    header("Location:index.php");
}
?>