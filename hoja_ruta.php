<?php
session_start();
date_default_timezone_set("America/Bogota");

header('Content-Type: text/html; charset=UTF-8');

//$fecha = date('dd'); //PONERLO AQUI

$paren = " (";
$paren1 = ")";
function generarCodigo($longitud) {
 	$key = '';
 	$pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
 	$max = strlen($pattern)-1;
 	for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
 	return $key;
}
if($_SESSION['logged']== 'yes')
{
	require ("libraries/conexion.php"); 
	$id_user=$_SESSION['su'];

	$contador_finalizacion = 0;

	$consulta_1 = "SELECT Name, LastName
				   FROM user WHERE idUser = '".$id_user."'";
	$resulado_1 = mysqli_query($con,$consulta_1);
	if(mysqli_num_rows($resulado_1) > 0)
	{
		$linea_1 = mysqli_fetch_assoc($resulado_1);
		$conductor = $linea_1['Name']." ".$linea_1['LastName'];
	}

    $acc = $_SESSION['acc'];
    $id_cliente_pdte = '';
    $estado_recorrido_pdte = '';

	require_once("inc/init.php");
	require_once("inc/config.ui.php");
	$page_title = "Hojas-Ruta";
	$page_css[] = "your_style.css";
	include("inc/header.php");
	$page_nav["rutero"]["sub"]["hoja_ruta"]["active"] = true;
	include("inc/nav.php");

	$fecha = date("Y-m-d");
	$hora = date("G:i:s",time());
	$fecha_1 = date("Y-m-d");

	$estado = 0;
	$idHoja_ruta = isset($_REQUEST['idHoja_ruta']) ? $_REQUEST['idHoja_ruta'] : NULL;

	$consulta_9 = "SELECT * FROM datos_hoja_ruta WHERE idHoja_ruta = '$idHoja_ruta' ORDER BY id_datos_hoja_ruta DESC LIMIT 1";
	$resultado_9 = mysqli_query($con,$consulta_9);

	if(mysqli_num_rows($resultado_9)>0){
		
		$linea_9 = mysqli_fetch_assoc($resultado_9);
		$estado_recorrido_pdte = isset($linea_9['estado_recorrido']) ? $linea_9['estado_recorrido'] : NULL;
		$odometro_pdte = isset($linea_9['odometro']) ? $linea_9['odometro'] : NULL;
		$hora_salida_pdte = isset($linea_9['hora_salida']) ? $linea_9['hora_salida'] : NULL;
		$id_cliente_pdte = isset($linea_9['id_cliente']) ? $linea_9['id_cliente'] : NULL;
		$num_factura_pdte = isset($linea_9['num_factura']) ? $linea_9['num_factura'] : NULL;
		$num_cme_pdte = isset($linea_9['num_cme']) ? $linea_9['num_cme'] : NULL;
		$hora_llegada_pdte = isset($linea_9['hora_llegada']) ? $linea_9['hora_llegada'] : NULL;
		$id_datos_hoja_ruta_pdte = isset($linea_9['id_datos_hoja_ruta']) ? $linea_9['id_datos_hoja_ruta'] : NULL;
	}

	$consulta_10 = "SELECT nombre FROM clientes WHERE id_cliente = '$id_cliente_pdte'";
	$resultado_10 = mysqli_query($con,$consulta_10);

	if(mysqli_num_rows($resultado_10)){
		$linea_10 = mysqli_fetch_assoc($resultado_10);
		$nombre_pdte = isset($linea_10['nombre']) ? $linea_10['nombre'] : NULL;
	}

	if($estado_recorrido_pdte == 1){
		$contador_finalizacion = 1;
	}

	if(isset($_POST['guardar_datos_basicos']))
	{
		$idHoja_ruta = $_POST['idHoja_ruta'];
			
		$estadoR = 1; //Estado 1: ASIGNANDO EVENTOS DE HOJA DE RUTA
		$fecha = $_POST['fecha'];
		$id_placa_camion = $_POST['id_placa_camion'];
		$salida = $_POST['salida'];
		$odometro_salida = $_POST['odometro_salida'];
		$User_idUser_conductor = $_POST['User_idUser_conductor'];		
		$User_idUser_ayudante = $_POST['User_idUser_ayudante'];
		$agencia_salida = $_POST['agencia_salida'];

		$consulta5 =  "SELECT id_verificacion_mensual, fecha_verificacion, estado FROM verificacion_mensual WHERE id_vehiculo = $id_placa_camion AND estado >= 1 ORDER BY id_verificacion_mensual DESC LIMIT 1";
		$resultado5 = mysqli_query($con,$consulta5);

		$linea5 = mysqli_fetch_assoc($resultado5);

		$id_verificacion_mensual = isset($linea5["id_verificacion_mensual"]) ? $linea5["id_verificacion_mensual"] : NULL;
		$fecha_verificacion = isset($linea5["fecha_verificacion"]) ? $linea5["fecha_verificacion"] : NULL;
		$estado = isset($linea5["estado"]) ? $linea5["estado"] : NULL;

		$fecha_verificacion = strtotime($fecha_verificacion);
		$fecha1 = strtotime('-30 day', strtotime($fecha));

		if($fecha_verificacion < $fecha1){
			 ?>
				<script type="text/javascript">
					window.location = ("busqueda_verificacion_mensual_vehiculos.php");
					alert("No se ha diligenciado ninguna verificacion mensual en los ultimos 30 dias. Por favor diligencie su verificacion mensual para porder continuar");
				</script>
			<?php
		}elseif ($estado == 1) {
			?>
			<script type="text/javascript">
				window.location = ("busqueda_verificacion_mensual_vehiculos.php");
				alert("Su verificacion mensual se encuentra incompleta. Por favor revice su ultima verificacion para poder continuar con el proceso");
			</script>
		<?php
		}// Revisar que este funcionando


		$consulta6 = "SELECT * FROM verificacion_vehiculo WHERE id_verificacion_mensual = $id_verificacion_mensual";
		$resultado6 = mysqli_query($con,$consulta6);

		$linea6 = mysqli_fetch_assoc($resultado6);

		$fecha_soat = isset($linea6["fecha_soat"]) ? $linea6["fecha_soat"] : NULL;
		$fecha_tecnicomecanica = isset($linea6["fecha_tecnicomecanica"]) ? $linea6["fecha_tecnicomecanica"] : NULL;

		$fecha_comparacion = strtotime('+15 day', strtotime($fecha));
		$fecha_soat_comparacion = strtotime($fecha_soat);
		$fecha_tecnicomecanica_comparacion = strtotime($fecha_tecnicomecanica);

		if($fecha_comparacion==$fecha_soat_comparacion){
			?>
			<script type="text/javascript">
			alert("Faltan 15 dias para el vencimiento de su SOAT")	
			</script>
			<?php
		}

		$fecha_comparacion = strtotime('+10 day', strtotime($fecha));

		if($fecha_comparacion==$fecha_soat_comparacion){
			?>
			<script type="text/javascript">
			alert("Faltan 10 dias para el vencimiento de su SOAT")	
			</script>
			<?php
		}

		$fecha_comparacion = strtotime('+5 day', strtotime($fecha));

		if($fecha_comparacion==$fecha_soat_comparacion){
			?>
			<script type="text/javascript">
			alert("Faltan 5 dias para el vencimiento de su SOAT")	
			</script>
			<?php
		}

		#aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa

		$fecha_comparacion = strtotime('+15 day', strtotime($fecha));

		if($fecha_comparacion==$fecha_tecnicomecanica_comparacion){
			?>
			<script type="text/javascript">
			alert("Faltan 15 dias para el vencimiento de su revisión tecnicomecanica")	
			</script>
			<?php
		}

		$fecha_comparacion = strtotime('+10 day', strtotime($fecha));

		if($fecha_comparacion==$fecha_tecnicomecanica_comparacion){
			?>
			<script type="text/javascript">
			alert("Faltan 10 dias para el vencimiento de su revisión tecnicomecanica")	
			</script>
			<?php
		}

		$fecha_comparacion = strtotime('+5 day', strtotime($fecha));

		if($fecha_comparacion==$fecha_tecnicomecanica_comparacion){
			?>
			<script type="text/javascript">
			alert("Faltan 5 dias para el vencimiento de su revisión tecnicomecanica")	
			</script>
			<?php
		}


		$prev_consul = "SELECT odometro_llegada FROM hoja_ruta WHERE placa = $id_placa_camion ORDER BY idHoja_ruta DESC LIMIT 1";
		$result_consul = mysqli_query($con,$prev_consul);
		if(mysqli_num_rows($result_consul) > 0){
			$linea_consul = mysqli_fetch_assoc($result_consul);
			$odometro_llegada_val = isset($linea_consul['odometro_llegada']) ? $linea_consul['odometro_llegada'] : NULL;
		}

		if($odometro_llegada_val > $odometro_salida){
			?>
			<script type="text/javascript">
				alert("El odometro de salida de este vehiculo no puede ser inferior al ultimo registrado");
				window.location = ("nueva_busqueda_hoja_ruta.php");
			</script>
			<?php
		}else{
			if($idHoja_ruta == 0)
			{	
				$consulta9 = "SELECT * FROM hoja_ruta WHERE placa = '$id_placa_camion' AND fecha = '$fecha'";
				$resultado9 = mysqli_query($con,$consulta9);

				if(mysqli_num_rows($resultado9)>0){
					
					$linea9 = mysqli_fetch_assoc($resultado9);

					$salida9 = $linea9["salida"];
					$odometro_salida9 = $linea9["odometro_salida"];
					$idHoja_ruta9 = $linea9["idHoja_ruta"];

					if(($salida == $salida9)&&($odometro_salida == $odometro_salida9)){
						?>
						<script type="text/javascript">
							alert("Tu información se registro repetida por doble pulsación, pero ya fue corregido y guardado exitosamente")
							var idHoja_ruta = '<?php echo $idHoja_ruta9; ?>';
							window.location = ('http://ingegas.info/hoja_ruta.php?idHoja_ruta='+idHoja_ruta);
						</script>
						<?php
					}else{
						$consulta_2 = "INSERT INTO hoja_ruta
							   (fecha, placa, salida, odometro_salida, User_idUser_conductor, User_idUser_ayudante,estado,agencia_salida)
							   VALUES ('".$fecha."',
							   		   '".$id_placa_camion."',
							   		   '".$salida."',
							   		   '".$odometro_salida."',
							   		   '".$User_idUser_conductor."',
							   		   '".$User_idUser_ayudante."',
							   		   '".$estadoR."',
							   		   '".$agencia_salida."')";

						if(mysqli_query($con,$consulta_2))
						{
							$idHoja_ruta = mysqli_insert_id($con);
							$i_apagado_encendido = $_POST["i_apagado_encendido"];
							$i_master = $_POST["i_master"];
							$i_fugas_vehiculo = $_POST["i_fugas_vehiculo"];
							$i_llantas = $_POST["i_llantas"];
							$i_frenos = $_POST["i_frenos"];
							$i_elementos_emergencia = $_POST["i_elementos_emergencia"];
							$i_listado_telefonos = $_POST["i_listado_telefonos"];
							$i_identificacion_vehiculo = $_POST["i_identificacion_vehiculo"];
							$i_hojas_seguridad = $_POST["i_hojas_seguridad"];
							$i_fugas_carga = $_POST["i_fugas_carga"];
							$i_carga_embalada = $_POST["i_carga_embalada"];
							$observaciones_apagado_encendido = $_POST["observaciones_apagado_encendido"];
							$observaciones_master = $_POST["observaciones_master"];
							$observaciones_fugas_vehiculo = $_POST["observaciones_fugas_vehiculo"];
							$observaciones_llantas = $_POST["observaciones_llantas"];
							$observaciones_frenos = $_POST["observaciones_frenos"];
							$observaciones_elementos_emergencia = $_POST["observaciones_elementos_emergencia"];
							$observaciones_listado_telefonos = $_POST["observaciones_listado_telefonos"];
							$observaciones_identificacion_vehiculo = $_POST["observaciones_identificacion_vehiculo"];
							$observaciones_hojas_seguridad = $_POST["observaciones_hojas_seguridad"];
							$observaciones_fugas_carga = $_POST["observaciones_fugas_carga"];
							$observaciones_carga_embalada = $_POST["observaciones_carga_embalada"];


							$consulta3 = "INSERT INTO verificacion_diaria_rutero (idHoja_ruta, apagado_encendido, master, fugas_vehiculo, llantas, frenos, elementos_emergencia, listado_telefonos, identificacion_vehiculo, hojas_seguridad, fugas_carga, carga_embalada,observaciones_apagado_encendido,observaciones_master,observaciones_fugas_vehiculo,observaciones_llantas,observaciones_frenos,observaciones_elementos_emergencia,observaciones_listado_telefonos,observaciones_identificacion_vehiculo,observaciones_hojas_seguridad,observaciones_fugas_carga,observaciones_carga_embalada) VALUES ($idHoja_ruta, $i_apagado_encendido, $i_master, $i_fugas_vehiculo, $i_llantas, $i_frenos, $i_elementos_emergencia, $i_listado_telefonos, $i_identificacion_vehiculo, $i_hojas_seguridad, $i_fugas_carga, $i_carga_embalada,'$observaciones_apagado_encendido','$observaciones_master','$observaciones_fugas_vehiculo','$observaciones_llantas','$observaciones_frenos','$observaciones_elementos_emergencia','$observaciones_listado_telefonos','$observaciones_identificacion_vehiculo','$observaciones_hojas_seguridad','$observaciones_fugas_carga','$observaciones_carga_embalada')";

							if(mysqli_query($con,$consulta3)){
								?>
								<script type="text/javascript">
									alert("Datos guardados de manera exitosa. Presione aceptar para continuar");
									var idHoja_ruta  = '<?php echo $idHoja_ruta;?>';	                
					                window.location = ("hoja_ruta.php?idHoja_ruta="+idHoja_ruta);
								</script>
								<?php
							}else{
								echo $consulta3;
								$error = mysqli_error($con);
								?>
								<script type="text/javascript">
									var error  = '<?php echo $error;?>';	                
									alert("Se presentó un error: " + error);					
					                window.location = ("nueva_busqueda_hoja_ruta.php");
								</script>
								<?php
							}

							?>
							
							<?php
						}
						else
						{
							$error = mysqli_error($con);
							?>
							<script type="text/javascript">
								var error  = '<?php echo $error;?>';	                
								alert("Se presentó un error: " + error);					
				                window.location = ("nueva_busqueda_hoja_ruta.php");
							</script>
							<?php
						}
					}
				}else{

					$consulta10 = "SELECT * FROM verificacion_mensual WHERE id_vehiculo = $id_placa_camion AND estado > 1 ORDER BY id_verificacion_mensual DESC LIMIT 1";
					$resultado10 = mysqli_query($con,$consulta10);

					if(mysqli_num_rows($resultado10)>0){

						$linea10 = mysqli_fetch_assoc($resultado10);

						$fechaVerificacion = $linea10["fecha"];
						$fechaActual = date("Y-m-d");
						$bandera = 0;

						$mesActual = date("m");

						$mesAnterior = date("m", strtotime($fechaActual."-1 month"));

						$anioActual = date("Y");

						if($mesAnterior == 12){
							$anioAnterior = date("Y", strtotime($fechaActual."-1 year"));
							$diasMesAnterior = cal_days_in_month(CAL_GREGORIAN, $mesAnterior, $anioAnterior);
							$finalMesAnterior = $anioAnterior."-".$mesAnterior."-".$diasMesAnterior;
						}else{
							$diasMesAnterior = cal_days_in_month(CAL_GREGORIAN, $mesAnterior, $anioActual);
							$finalMesAnterior = $anioActual."-".$mesAnterior."-".$diasMesAnterior;
						}

						$plazoMax = date("Y-m-d", strtotime($finalMesAnterior."-5 days"));
						$plazoMin = date("Y-m-d", strtotime($finalMesAnterior."+5 days"));

						while ($bandera == 1) {

							if($plazoMax == $fechaVerificacion){
								break;
							}
							
							if($plazoMax == $plazoMin){
								$bandera = 1;
							}else{
								$plazoMax = date("Y-m-d", strtotime($plazoMax."+1 days"));
							}
						}

						if($bandera == 5){
							?>
							<script type="text/javascript">
								window.alert("No se registro verificacion mensual en el ultimo mes");
								window.location = ("nueva_busqueda_hoja_ruta.php");
							</script>
							<?php
						}else{
							$consulta_2 = "INSERT INTO hoja_ruta
									   (fecha, placa, salida, odometro_salida, User_idUser_conductor, User_idUser_ayudante,estado,agencia_salida)
									   VALUES ('".$fecha."',
									   		   '".$id_placa_camion."',
									   		   '".$salida."',
									   		   '".$odometro_salida."',
									   		   '".$User_idUser_conductor."',
									   		   '".$User_idUser_ayudante."',
									   		   '".$estadoR."',
									   		   '".$agencia_salida."')";

							if(mysqli_query($con,$consulta_2))
							{
								$idHoja_ruta = mysqli_insert_id($con);
								$i_apagado_encendido = $_POST["i_apagado_encendido"];
								$i_master = $_POST["i_master"];
								$i_fugas_vehiculo = $_POST["i_fugas_vehiculo"];
								$i_llantas = $_POST["i_llantas"];
								$i_frenos = $_POST["i_frenos"];
								$i_elementos_emergencia = $_POST["i_elementos_emergencia"];
								$i_listado_telefonos = $_POST["i_listado_telefonos"];
								$i_identificacion_vehiculo = $_POST["i_identificacion_vehiculo"];
								$i_hojas_seguridad = $_POST["i_hojas_seguridad"];
								$i_fugas_carga = $_POST["i_fugas_carga"];
								$i_carga_embalada = $_POST["i_carga_embalada"];
								$observaciones_apagado_encendido = $_POST["observaciones_apagado_encendido"];
								$observaciones_master = $_POST["observaciones_master"];
								$observaciones_fugas_vehiculo = $_POST["observaciones_fugas_vehiculo"];
								$observaciones_llantas = $_POST["observaciones_llantas"];
								$observaciones_frenos = $_POST["observaciones_frenos"];
								$observaciones_elementos_emergencia = $_POST["observaciones_elementos_emergencia"];
								$observaciones_listado_telefonos = $_POST["observaciones_listado_telefonos"];
								$observaciones_identificacion_vehiculo = $_POST["observaciones_identificacion_vehiculo"];
								$observaciones_hojas_seguridad = $_POST["observaciones_hojas_seguridad"];
								$observaciones_fugas_carga = $_POST["observaciones_fugas_carga"];
								$observaciones_carga_embalada = $_POST["observaciones_carga_embalada"];


								$consulta3 = "INSERT INTO verificacion_diaria_rutero (idHoja_ruta, apagado_encendido, master, fugas_vehiculo, llantas, frenos, elementos_emergencia, listado_telefonos, identificacion_vehiculo, hojas_seguridad, fugas_carga, carga_embalada,observaciones_apagado_encendido,observaciones_master,observaciones_fugas_vehiculo,observaciones_llantas,observaciones_frenos,observaciones_elementos_emergencia,observaciones_listado_telefonos,observaciones_identificacion_vehiculo,observaciones_hojas_seguridad,observaciones_fugas_carga,observaciones_carga_embalada) VALUES ($idHoja_ruta, $i_apagado_encendido, $i_master, $i_fugas_vehiculo, $i_llantas, $i_frenos, $i_elementos_emergencia, $i_listado_telefonos, $i_identificacion_vehiculo, $i_hojas_seguridad, $i_fugas_carga, $i_carga_embalada,'$observaciones_apagado_encendido','$observaciones_master','$observaciones_fugas_vehiculo','$observaciones_llantas','$observaciones_frenos','$observaciones_elementos_emergencia','$observaciones_listado_telefonos','$observaciones_identificacion_vehiculo','$observaciones_hojas_seguridad','$observaciones_fugas_carga','$observaciones_carga_embalada')";

								if(mysqli_query($con,$consulta3)){
									?>
									<script type="text/javascript">
										alert("Datos guardados de manera exitosa. Presione aceptar para continuar");
										var idHoja_ruta  = '<?php echo $idHoja_ruta;?>';	                
						                window.location = ("hoja_ruta.php?idHoja_ruta="+idHoja_ruta);
									</script>
									<?php
								}else{
									echo $consulta3;
									$error = mysqli_error($con);
									?>
									<script type="text/javascript">
										var error  = '<?php echo $error;?>';	                
										alert("Se presentó un error: " + error);					
						                window.location = ("nueva_busqueda_hoja_ruta.php");
									</script>
									<?php
								}

								?>
								
								<?php
							}
							else
							{
								$error = mysqli_error($con);
								?>
								<script type="text/javascript">
									var error  = '<?php echo $error;?>';	                
									alert("Se presentó un error: " + error);					
					                window.location = ("nueva_busqueda_hoja_ruta.php");
								</script>
								<?php
							}
						}

					}else{
						?>
						<script type="text/javascript">
							window.alert("No se ha registrado NINGUNA verificacion mensual");
							window.location = ("nueva_busqueda_hoja_ruta.php");
						</script>
						<?php
					}
					
				}
				
			}
		}
		
		
		if($idHoja_ruta > 0 )
		{
			?>

		<?php
			$consulta_2 = "UPDATE hoja_ruta 
						   SET fecha = '".$fecha."',
						       placa = '".$id_placa_camion."',
						       salida = '".$salida."',
						       odometro_salida = '".$odometro_salida."',
						       User_idUser_conductor = '".$User_idUser_conductor."',
						       User_idUser_ayudante = '".$User_idUser_ayudante."'
						   WHERE idHoja_ruta = '".$idHoja_ruta."'";
			if(mysqli_query($con,$consulta_2))
			{
				
				$i_apagado_encendido = $_POST["i_apagado_encendido"];
				$i_master = $_POST["i_master"];
				$i_fugas_vehiculo = $_POST["i_fugas_vehiculo"];
				$i_llantas = $_POST["i_llantas"];
				$i_frenos = $_POST["i_frenos"];
				$i_elementos_emergencia = $_POST["i_elementos_emergencia"];
				$i_listado_telefonos = $_POST["i_listado_telefonos"];
				$i_identificacion_vehiculo = $_POST["i_identificacion_vehiculo"];
				$i_hojas_seguridad = $_POST["i_hojas_seguridad"];
				$i_fugas_carga = $_POST["i_fugas_carga"];
				$i_carga_embalada = $_POST["i_carga_embalada"];
				$observaciones_apagado_encendido = $_POST["observaciones_apagado_encendido"];
				$observaciones_master = $_POST["observaciones_master"];
				$observaciones_fugas_vehiculo = $_POST["observaciones_fugas_vehiculo"];
				$observaciones_llantas = $_POST["observaciones_llantas"];
				$observaciones_frenos = $_POST["observaciones_frenos"];
				$observaciones_elementos_emergencia = $_POST["observaciones_elementos_emergencia"];
				$observaciones_listado_telefonos = $_POST["observaciones_listado_telefonos"];
				$observaciones_identificacion_vehiculo = $_POST["observaciones_identificacion_vehiculo"];
				$observaciones_hojas_seguridad = $_POST["observaciones_hojas_seguridad"];
				$observaciones_fugas_carga = $_POST["observaciones_fugas_carga"];
				$observaciones_carga_embalada = $_POST["observaciones_carga_embalada"];


				$consulta3 = "INSERT INTO verificacion_diaria_rutero (idHoja_ruta, apagado_encendido, master, fugas_vehiculo, llantas, frenos, elementos_emergencia, listado_telefonos, identificacion_vehiculo, hojas_seguridad, fugas_carga, carga_embalada,observaciones_apagado_encendido,observaciones_master,observaciones_fugas_vehiculo,observaciones_llantas,observaciones_frenos,observaciones_elementos_emergencia,observaciones_listado_telefonos,observaciones_identificacion_vehiculo,observaciones_hojas_seguridad,observaciones_fugas_carga,observaciones_carga_embalada) VALUES ($idHoja_ruta, $i_apagado_encendido, $i_master, $i_fugas_vehiculo, $i_llantas, $i_frenos, $i_elementos_emergencia, $i_listado_telefonos, $i_identificacion_vehiculo, $i_hojas_seguridad, $i_fugas_carga, $i_carga_embalada,'$observaciones_apagado_encendido','$observaciones_master','$observaciones_fugas_vehiculo','$observaciones_llantas','$observaciones_frenos','$observaciones_elementos_emergencia','$observaciones_listado_telefonos','$observaciones_identificacion_vehiculo','$observaciones_hojas_seguridad','$observaciones_fugas_carga','$observaciones_carga_embalada')";
				$resultado3 = mysqli_query($con,$consulta3);
				?>
				<script type="text/javascript">
					alert("Datos Ingresados con exito!");
					var idHoja_ruta  = '<?php echo $idHoja_ruta;?>';	                
	                //window.location = ("hoja_ruta.php?idHoja_ruta="+idHoja_ruta);
				</script>
				<?php
			}
			else
			{
				$error = mysqli_error($con);
				?>
				<script type="text/javascript">
					var error  = '<?php echo $error;?>';	                
					alert("Se presentó un error: " + error);					
	                window.location = ("nueva_busqueda_hoja_ruta.php");
				</script>
				<?php
			}
		}
				
		
			
	}
	//Botón guarda cambios de cada uno de los registros correspondientes a la hoja de ruta
	if(isset($_POST['guardar_datos_hoja_ruta']))
	{
		$idHoja_ruta = $_POST['idHoja_ruta'];	
		$id_cliente = $_POST['id_cliente'];
		$odometro = $_POST['odometro'];
		$num_factura = $_POST['num_factura'];
		$num_cme = $_POST['num_cme'];
		$hora_llegada = date("G:i:s",time());
		$fecha_llegada = date("Y-m-d");
		$hora_salida = date("G:i:s",time());
		$fecha_salida = date("Y-m-d");
		$cili_entregado = $_POST['cili_entregado'];
		$cili_recibido = $_POST['cili_recibido'];
		$termo_entregado = $_POST['termo_entregado'];
		$termo_recibido = $_POST['termo_recibido'];
		$lin_litros = $_POST['lin_litros'];
		$otros_item = $_POST['otros_item'];
		$otros_entregado = $_POST['otros_entregado'];
		$otros_recibido = $_POST['otros_recibido'];
		$dinero_valor = $_POST['dinero_valor'];
		$efectivo_cheque = $_POST['efectivo_cheque'];
		$observaciones = $_POST['observaciones'];
		if($odometro != 0){
			$estado_recorrido = 0;
		}else{
			$estado_recorrido = 1;
		}

		//Se insertan los datos correspondientes a la ruta realizada

		if($contador_finalizacion!=0){

			$consulta_11 = "UPDATE datos_hoja_ruta SET
							odometro = '".$odometro."',
							hora_salida = '".$hora_salida."',
							fecha_salida = '".$fecha_salida."',
							cili_entregado = '".$cili_entregado."',
							cili_recibido = '".$cili_recibido."',
							termo_entregado = '".$termo_entregado."',
							termo_recibido = '".$termo_recibido."',
							lin_litros = '".$lin_litros."',
							otros_item = '".$otros_item."',
							otros_entregado = '".$otros_entregado."',
							otros_recibido = '".$otros_recibido."',
							dinero_valor = '".$dinero_valor."',
							efectivo_cheque = '".$efectivo_cheque."',
							observaciones = '".$observaciones."',
							estado_recorrido = '0'
							WHERE id_datos_hoja_ruta = '".$id_datos_hoja_ruta_pdte."'";
			
			if(mysqli_query($con,$consulta_11)){
				?>
				<script type="text/javascript">
					alert("Recorrido cerrado");
					var idHoja_ruta = '<?php echo $idHoja_ruta; ?>';
					window.location = ('http://ingegas.info/hoja_ruta.php?idHoja_ruta='+idHoja_ruta);
				</script>
				<?php
			}

		}else{

			$consulta_4 = "INSERT INTO datos_hoja_ruta (idHoja_ruta, id_cliente, odometro, num_factura, num_cme, hora_llegada, fecha_llegada, hora_salida, fecha_salida, cili_entregado, cili_recibido, termo_entregado, termo_recibido, lin_litros, otros_item, otros_entregado, otros_recibido, dinero_valor, efectivo_cheque, observaciones, 			estado_recorrido) VALUES ( '".$idHoja_ruta."', '".$id_cliente."', '".$odometro."', '".$num_factura."', '".$num_cme."', '".$hora_llegada."', '".$fecha_llegada."', '".$hora_salida."', '".$fecha_salida."', '".$cili_entregado."', '".$cili_recibido."', '".$termo_entregado."', '".$termo_recibido."', '".$lin_litros."', '".$otros_item."', '".$otros_entregado."', '".$otros_recibido."', '".$dinero_valor."', '".$efectivo_cheque."', '".$observaciones."', '".$estado_recorrido."')";
			if (mysqli_query($con,$consulta_4))
			{
				?>
				<script type="text/javascript">
					alert("Datos guardados exitosamente.")
					var idHoja_ruta = '<?php echo $idHoja_ruta; ?>';
					window.location = ('http://ingegas.info/hoja_ruta.php?idHoja_ruta='+idHoja_ruta);
				</script>
				<?php
			}
			else
			{
				$consulta7 = "SELECT * FROM datos_hoja_ruta WHERE num_cme = '$num_cme'";
				$resultado7 = mysqli_query($con,$consulta7);

				if(mysqli_num_rows($resultado7)>0){
					$linea7 = mysqli_fetch_assoc($resultado7);
					$idHoja_ruta7 = $linea7["idHoja_ruta"];
					$hora_llegada7 = $linea7["hora_llegada"];

					if(($idHoja_ruta7 == $idHoja_ruta)||($hora_llegada7 == $hora_llegada)){
						?>
						<script type="text/javascript">
							alert("Tu información se registro repetida por doble pulsación, pero ya fue corregido y guardado exitosamente")
							var idHoja_ruta = '<?php echo $idHoja_ruta; ?>';
							window.location = ('http://ingegas.info/hoja_ruta.php?idHoja_ruta='+idHoja_ruta);
						</script>
						<?php
					}else{
						?>
						<script type="text/javascript">
							alert("ERROR:: Este numero de CME no fue guardado, por favor consultalo o asigna un aleatorio en la opción 'Sin CME'.")
							var idHoja_ruta = '<?php echo $idHoja_ruta; ?>';
							window.location = ('http://ingegas.info/hoja_ruta.php?idHoja_ruta='+idHoja_ruta);
						</script>
						<?php
					}

					
				}

			}

		}
	}

	if(isset($_POST['guardado_parcial'])){
		$idHoja_ruta = $_POST['idHoja_ruta'];	
		$id_cliente = $_POST['id_cliente'];
		$odometro = $_POST['odometro'];
		$num_factura = $_POST['num_factura'];
		$num_cme = $_POST['num_cme'];
		$hora_llegada = date("G:i:s",time());
		$fecha_llegada = date("Y-m-d");

		$cili_entregado = $_POST['cili_entregado'];
		$cili_recibido = $_POST['cili_recibido'];
		$termo_entregado = $_POST['termo_entregado'];
		$termo_recibido = $_POST['termo_recibido'];
		$lin_litros = $_POST['lin_litros'];
		$otros_item = $_POST['otros_item'];
		$otros_entregado = $_POST['otros_entregado'];
		$otros_recibido = $_POST['otros_recibido'];
		$dinero_valor = $_POST['dinero_valor'];
		$efectivo_cheque = $_POST['efectivo_cheque'];
		$observaciones = $_POST['observaciones'];

		$consulta8 = "INSERT INTO datos_hoja_ruta (idHoja_ruta, id_cliente, odometro, num_factura, num_cme, hora_llegada, fecha_llegada, cili_entregado, cili_recibido, termo_entregado, termo_recibido, lin_litros, otros_item, otros_entregado, otros_recibido, dinero_valor, efectivo_cheque, observaciones, estado_recorrido) VALUES ( '".$idHoja_ruta."', '".$id_cliente."', '".$odometro."', '".$num_factura."', '".$num_cme."', '".$hora_llegada."', '".$fecha_llegada."', '".$cili_entregado."', '".$cili_recibido."', '".$termo_entregado."', '".$termo_recibido."', '".$lin_litros."', '".$otros_item."', '".$otros_entregado."', '".$otros_recibido."', '".$dinero_valor."', '".$efectivo_cheque."', '".$observaciones."', '1')";

		if (mysqli_query($con,$consulta8))
			{
				?>
				<script type="text/javascript">
					alert("Punto de Control Registrado exitosamente.")
					var idHoja_ruta = '<?php echo $idHoja_ruta; ?>';
					window.location = ('http://ingegas.info/hoja_ruta.php?idHoja_ruta='+idHoja_ruta);
				</script>
				<?php
			}
			else
			{
				$consulta7 = "SELECT * FROM datos_hoja_ruta WHERE num_cme = '$num_cme'";
				$resultado7 = mysqli_query($con,$consulta7);

				if(mysqli_num_rows($resultado7)>0){
					$linea7 = mysqli_fetch_assoc($resultado7);
					$idHoja_ruta7 = $linea7["idHoja_ruta"];
					$hora_llegada7 = $linea7["hora_llegada"];

					if(($idHoja_ruta7 == $idHoja_ruta)||($hora_llegada7 == $hora_llegada)){
						?>
						<script type="text/javascript">
							alert("Tu información se registro repetida por doble pulsación, pero ya fue corregido y guardado exitosamente")
							var idHoja_ruta = '<?php echo $idHoja_ruta; ?>';
							window.location = ('http://ingegas.info/hoja_ruta.php?idHoja_ruta='+idHoja_ruta);
						</script>
						<?php
					}else{
						?>
						<script type="text/javascript">
							alert("ERROR:: Este numero de CME no fue guardado, por favor consultalo o asigna un aleatorio en la opción 'Sin CME'.")
							var idHoja_ruta = '<?php echo $idHoja_ruta; ?>';
							window.location = ('http://ingegas.info/hoja_ruta.php?idHoja_ruta='+idHoja_ruta);
						</script>
						<?php
					}

					
				}

			}

	}
	
	//SE CONSULTAN DATOS DE HOJA DE RUTA EXISTENTE
	if(isset($idHoja_ruta) > 0)
	{
		$consulta_3 = "SELECT * 
		               FROM hoja_ruta 
		               WHERE idHoja_ruta = '".$idHoja_ruta."'";
		$resultado_3 = mysqli_query($con,$consulta_3);
		if(mysqli_num_rows($resultado_3) > 0)
		{
			$linea_3 = mysqli_fetch_assoc($resultado_3);			
			$fecha = isset($linea_3['fecha']) ? $linea_3['fecha'] : NULL;
			$placa = isset($linea_3['placa']) ? $linea_3['placa'] : NULL;
			$salida = isset($linea_3['salida']) ? $linea_3['salida'] : NULL;
			$llegada = isset($linea_3['llegada']) ? $linea_3['llegada'] : NULL;
			$odometro_salida = isset($linea_3['odometro_salida']) ? $linea_3['odometro_salida'] : NULL;
			$odometro_llegada = isset($linea_3['odometro_llegada']) ? $linea_3['odometro_llegada'] : NULL;
			$User_idUser_conductor = isset($linea_3['User_idUser_conductor']) ? $linea_3['User_idUser_conductor'] : NULL;
			$User_idUser_ayudante = isset($linea_3['User_idUser_ayudante']) ? $linea_3['User_idUser_ayudante'] : NULL;
			$estado = isset($linea_3['estado']) ? $linea_3['estado'] : NULL;
			$agencia_salida = isset($linea_3['agencia_salida']) ? $linea_3['agencia_salida'] : NULL;
			$agencia_llegada = isset($linea_3['agencia_llegada']) ? $linea_3['agencia_llegada'] : NULL;			
			$recorrido = isset($linea_3['recorrido']) ? $linea_3['recorrido'] : NULL;

		}
	}


	//Finalizar la hoja de ruta para revisión. El conductor no puede agregar más campos.
	if (isset($_POST['finalizar_hoja_ruta'])) 
	{
		$fecha_1 = date("Y-m-d");
		//$idHoja_ruta = isset($_REQUEST['idHoja_ruta']) ? $_REQUEST['idHoja_ruta'] : NULL;
		$idHoja_ruta = $_POST['idHoja_ruta'];
		$agencia_llegada = $_POST['agencia_llegada'];
		$odometro_llegada = $_POST['odometro_llegada'];
		$llegada = $hora." (".$fecha_1.")";
		$estado = 2; //ESTADO HOJA DE RUTA FINALIZADA


		if ($odometro_llegada < $odometro_salida) 
		{
			?>
			<script type="text/javascript">
				alert("El odómetro de llegada no puede ser menor al odómetro de salida.")
				window.location = ('http://ingegas.info/hoja_ruta.php?idHoja_ruta='+<?php echo $idHoja_ruta; ?>);
			</script>
			<?php
		}
		else
		{
			$recorrido = $odometro_llegada - $odometro_salida;
			//Totalizar
			$consulta_6 = "SELECT SUM(cili_entregado) AS total_cil_ent,
								  SUM(cili_recibido) AS total_cil_rec,
								  SUM(termo_entregado) AS total_ter_ent,
								  SUM(termo_recibido) AS total_ter_rec,
								  SUM(otros_entregado) AS total_otros_ent,
								  SUM(otros_recibido) AS total_otros_rec,
								  SUM(lin_litros) AS total_litros,
								  SUM(dinero_valor) AS total_dinero,
								  COUNT(id_cliente) AS total_clientes
						   FROM datos_hoja_ruta
						   WHERE idHoja_ruta = '".$idHoja_ruta."'";
			$resultado_6 = mysqli_query($con,$consulta_6);
			if($resultado_6)
			{
				$linea_6 = mysqli_fetch_assoc($resultado_6);
				$total_cil_ent = $linea_6['total_cil_ent'];
				$total_cil_rec = $linea_6['total_cil_rec'];
				$total_ter_ent = $linea_6['total_ter_ent'];
				$total_ter_rec = $linea_6['total_ter_rec'];
				$total_otros_ent = $linea_6['total_otros_ent'];
				$total_otros_rec = $linea_6['total_otros_rec'];
				$total_litros = $linea_6['total_litros'];
				$total_dinero = $linea_6['total_dinero'];
				$total_clientes = $linea_6['total_clientes'];

				$consulta_5 = "UPDATE hoja_ruta
							   SET total_cil_ent = '".$total_cil_ent."',
							       total_cil_rec = '".$total_cil_rec."',
							       total_ter_ent = '".$total_ter_ent."',
							       total_ter_rec = '".$total_ter_rec."',
							       total_otros_ent = '".$total_otros_ent."',
							       total_otros_rec = '".$total_otros_rec."',
							       total_litros = '".$total_litros."',
							       total_dinero = '".$total_dinero."',
							       total_clientes = '".$total_clientes."',
							       estado = '".$estado."',
							       agencia_llegada = '".$agencia_llegada."',
							       odometro_llegada = '".$odometro_llegada."',
							       llegada = '".$llegada."',
							       recorrido = '".$recorrido."'
							   WHERE idHoja_ruta = '".$idHoja_ruta."'";			

				if(mysqli_query($con,$consulta_5))
				{
					?>
					<script type="text/javascript">
						alert("La hoja de ruta ha sido finalizada correctamente.");
						var idHoja_ruta  = '<?php echo isset($idHoja_ruta) ? $idHoja_ruta : NULL; ?>';
				        window.location = ("hoja_ruta.php?idHoja_ruta="+idHoja_ruta);
					</script>
					<?php
				}
				else
				{
					?>
					<script type="text/javascript">
						alert("¡Alerta! Ha ocurrido un error.");
						var idHoja_ruta  = '<?php echo isset($idHoja_ruta) ? $idHoja_ruta : NULL; ?>';
				        window.location = ("hoja_ruta.php?idHoja_ruta="+idHoja_ruta);
					</script>
					<?php
				}

			}
		}
			
		
	}
	//Finalizar la hoja de ruta por completo
	if (isset($_POST['revisar_hoja_ruta'])) 
	{
		//$idHoja_ruta = isset($_REQUEST['idHoja_ruta']) ? $_REQUEST['idHoja_ruta'] : NULL;
		$idHoja_ruta = $_POST['idHoja_ruta'];
		$estado = 3; //ESTADO HOJA DE RUTA FINALIZADA

		
		$id_placa_camion = $_POST['id_placa_camion'];		
		$odometro_salida = $_POST['odometro_salida'];		
		$User_idUser_ayudante = $_POST['User_idUser_ayudante'];
		$agencia_salida = $_POST['agencia_salida'];
		$agencia_llegada = $_POST['agencia_llegada'];
		$odometro_llegada = $_POST['odometro_llegada'];

		$recorrido = $odometro_llegada - $odometro_salida;
		//Totalizar
		$consulta_6 = "SELECT SUM(cili_entregado) AS total_cil_ent,
							  SUM(cili_recibido) AS total_cil_rec,
							  SUM(termo_entregado) AS total_ter_ent,
							  SUM(termo_recibido) AS total_ter_rec,
							  SUM(otros_entregado) AS total_otros_ent,
							  SUM(otros_recibido) AS total_otros_rec,
							  SUM(lin_litros) AS total_litros,
							  SUM(dinero_valor) AS total_dinero,
							  SUM(num_errores) AS total_errores,
							  COUNT(id_cliente) AS total_clientes							 
					   FROM datos_hoja_ruta
					   WHERE idHoja_ruta = '".$idHoja_ruta."'";
		$resultado_6 = mysqli_query($con,$consulta_6);
		if($resultado_6)
		{
			$linea_6 = mysqli_fetch_assoc($resultado_6);
			$total_cil_ent = $linea_6['total_cil_ent'];
			$total_cil_rec = $linea_6['total_cil_rec'];
			$total_ter_ent = $linea_6['total_ter_ent'];
			$total_ter_rec = $linea_6['total_ter_rec'];
			$total_otros_ent = $linea_6['total_otros_ent'];
			$total_otros_rec = $linea_6['total_otros_rec'];
			$total_litros = $linea_6['total_litros'];
			$total_dinero = $linea_6['total_dinero'];
			$total_errores = $linea_6['total_errores'];
			$total_clientes = $linea_6['total_clientes'];

			$consulta_5 = "UPDATE hoja_ruta
						   SET total_cil_ent = '".$total_cil_ent."',
						       total_cil_rec = '".$total_cil_rec."',
						       total_ter_ent = '".$total_ter_ent."',
						       total_ter_rec = '".$total_ter_rec."',
						       total_otros_ent = '".$total_otros_ent."',
						       total_otros_rec = '".$total_otros_rec."',
						       total_litros = '".$total_litros."',
						       total_dinero = '".$total_dinero."',
						       total_errores = '".$total_errores."',
						       total_clientes = '".$total_clientes."',
						       placa = '".$id_placa_camion."',
						       odometro_salida = '".$odometro_salida."',
						       odometro_llegada = '".$odometro_llegada."',
						       User_idUser_ayudante = '".$User_idUser_ayudante."',
						       agencia_salida = '".$agencia_salida."',
						       agencia_llegada = '".$agencia_llegada."',
						       recorrido = '".$recorrido."',
						       estado = '".$estado."'
						   WHERE idHoja_ruta = '".$idHoja_ruta."'";			

			if(mysqli_query($con,$consulta_5))
			{
				?>
				<script type="text/javascript">
					alert("La hoja de ruta ha sido finalizada correctamente.");
					var idHoja_ruta  = '<?php echo isset($idHoja_ruta) ? $idHoja_ruta : NULL; ?>';
			        window.location = ("hoja_ruta.php?idHoja_ruta="+idHoja_ruta);
				</script>
				<?php
			}
			else
			{
				echo "Error updating record: " . mysqli_error($con);
				?>
				<script type="text/javascript">
					alert("¡Alerta! Ha ocurrido un error.");
					var idHoja_ruta  = '<?php echo isset($idHoja_ruta) ? $idHoja_ruta : NULL; ?>';
			        window.location = ("hoja_ruta.php?idHoja_ruta="+idHoja_ruta);
				</script>
				<?php
			}
		}
	}
?>

<div class="modal fade" id="modal_verificacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true"> <!-- Verificar que este funcionando -->
	<form name="verificacion_diaria" action="#" method="POST" id="mezcla">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<!-- <button type="button" class="close" data-dismiss="modal" id ="cerrar"aria-hidden="true">
					&times;
					</button> Evita Cerrar el proceso el proceso -->
					<h4 class="modal-title" id="myModalLabel">Aspectos del Vehiculo</h4>
				</div>
				<div class="modal-body">
					<div class="well well-sm well-primary">
						<div class="row">
							<div class="col-sm-5">
								<div class="form-group">
									<label for="category">El sistema eléctrico de apagado y encendido del vehículo funciona correctamente:</label>
								</div>  
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<div class="inline-group">
										<label class="radio-inline">
											<input type="radio" name="apagado_encendido" id="apagado_encendido1" value="1">
											<i></i>Si
										</label>
										<label class="radio-inline">
											<input type="radio" name="apagado_encendido" id="apagado_encendido2" value="2">
											<i></i>No
										</label>
									</div>
								</div>  
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<div class="inline-group">
										<label>
											<input type="text" name="obs_apagado_encendido" placeholder="Observaciones" id="obs_apagado_encendido" value="">
										</label>
									</div>
								</div>  
							</div>
						</div>
						<div class="row">
							<div class="col-sm-5">
								<div class="form-group">
									<label for="category">El interruptor eléctrico central (Master) funciona correctamente:</label>
								</div>  
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<div class="inline-group">
										<label class="radio-inline">
											<input type="radio" name="master" id="master1" value="1">
											<i></i>Si
										</label>
										<label class="radio-inline">
											<input type="radio" name="master" id="master2" value="2">
											<i></i>No
										</label>
									</div>
								</div>  
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<div class="inline-group">
										<label>
											<input type="text" name="obs_master" placeholder="Observaciones" id="obs_master" value="">
										</label>
									</div>
								</div>  
							</div>
						</div>
						<div class="row">
							<div class="col-sm-5">
								<div class="form-group">
									<label for="category">El vehículo presenta fugas de aceite, líquido de frenos o combustible:</label>
								</div>  
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<div class="inline-group">
										<label class="radio-inline">
											<input type="radio" name="fugas_vehiculo" id="fugas_vehiculo1" value="1">
											<i></i>Si
										</label>
										<label class="radio-inline">
											<input type="radio" name="fugas_vehiculo" id="fugas_vehiculo2" value="2">
											<i></i>No
										</label>
									</div>
								</div>  
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<div class="inline-group">
										<label>
											<input type="text" name="obs_fugas_vehiculo" placeholder="Observaciones" id="obs_fugas_vehiculo" value="">
										</label>
									</div>
								</div>  
							</div>
						</div>
						<div class="row">
							<div class="col-sm-5">
								<div class="form-group">
									<label for="category">Las llantas del vehículo están en buen estado:</label>
								</div>  
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<div class="inline-group">
										<label class="radio-inline">
											<input type="radio" name="llantas" id="llantas1" value="1">
											<i></i>Si
										</label>
										<label class="radio-inline">
											<input type="radio" name="llantas" id="llantas2" value="2">
											<i></i>No
										</label>
									</div>
								</div>  
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<div class="inline-group">
										<label>
											<input type="text" name="obs_llantas" placeholder="Observaciones" id="obs_llantas" value="">
										</label>
									</div>
								</div>  
							</div>
						</div>
						<div class="row">
							<div class="col-sm-5">
								<div class="form-group">
									<label for="category">El sistema de frenos del vehículo funciona adecuadamente:</label>
								</div>  
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<div class="inline-group">
										<label class="radio-inline">
											<input type="radio" name="frenos" id="frenos1" value="1">
											<i></i>Si
										</label>
										<label class="radio-inline">
											<input type="radio" name="frenos" id="frenos2" value="2">
											<i></i>No
										</label>
									</div>
								</div>  
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<div class="inline-group">
										<label>
											<input type="text" name="obs_frenos" placeholder="Observaciones" id="obs_frenos" value="">
										</label>
									</div>
								</div>  
							</div>
						</div>
						<div class="row">
							<div class="col-sm-5">
								<div class="form-group">
									<label for="category">El vehículo cuenta con los equipos y elementos de protección para atención de emergencias: extintor de incendios, linterna, botiquín de primeros auxilios, control de derrames, gato, cuñas, conos y herramientas:</label>
								</div>  
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<div class="inline-group">
										<label class="radio-inline">
											<input type="radio" name="elementos_emergencia" id="elementos_emergencia1" value="1">
											<i></i>Si
										</label>
										<label class="radio-inline">
											<input type="radio" name="elementos_emergencia" id="elementos_emergencia2" value="2">
											<i></i>No
										</label>
									</div>
								</div>  
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<div class="inline-group">
										<label>
											<input type="text" name="obs_elementos_emergencia" placeholder="Observaciones" id="obs_elementos_emergencia" value="">
										</label>
									</div>
								</div>  
							</div>
						</div>
						<div class="row">
							<div class="col-sm-5">
								<div class="form-group">
									<label for="category">El vehículo cuenta con un listado de teléfonos para notificación de emergencias:</label>
								</div>  
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<div class="inline-group">
										<label class="radio-inline">
											<input type="radio" name="listado_telefonos" id="listado_telefonos1" value="1">
											<i></i>Si
										</label>
										<label class="radio-inline">
											<input type="radio" name="listado_telefonos" id="listado_telefonos2" value="2">
											<i></i>No
										</label>
									</div>
								</div>  
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<div class="inline-group">
										<label>
											<input type="text" name="obs_listado_telefonos" placeholder="Observaciones" id="obs_listado_telefonos" value="">
										</label>
									</div>
								</div>  
							</div>
						</div>
						<div class="row">
							<div class="col-sm-5">
								<div class="form-group">
									<label for="category">El vehículo y las unidades de transporte y están debidamente identificados de acuerdo al tipo de mercancía que transporta (etiquetas, rombos, UN):</label>
								</div>  
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<div class="inline-group">
										<label class="radio-inline">
											<input type="radio" name="identificacion_vehiculo" id="identificacion_vehiculo1" value="1">
											<i></i>Si
										</label>
										<label class="radio-inline">
											<input type="radio" name="identificacion_vehiculo" id="identificacion_vehiculo2" value="2">
											<i></i>No
										</label>
									</div>
								</div>  
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<div class="inline-group">
										<label>
											<input type="text" name="obs_identificacion_vehiculo" placeholder="Observaciones" id="obs_identificacion_vehiculo" value="">
										</label>
									</div>
								</div>  
							</div>
						</div>
					</div>
				</div><!-- /.modal-content -->
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Aspectos del la Carga</h4>
				</div>
				<div class="modal-body">
					<div class="well well-sm well-primary">
						
						<div class="row">
							<div class="col-sm-5">
								<div class="form-group">
									<label for="category">Se cuenta con las hojas de seguridad y tarjetas de emergencias de las mercancías transportadas:</label>
								</div>  
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<div class="inline-group">
										<label class="radio-inline">
											<input type="radio" name="hojas_seguridad" id="hojas_seguridad1" value="1">
											<i></i>Si
										</label>
										<label class="radio-inline">
											<input type="radio" name="hojas_seguridad" id="hojas_seguridad2" value="2">
											<i></i>No
										</label>
									</div>
								</div>  
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<div class="inline-group">
										<label>
											<input type="text" name="obs_hojas_seguridad" placeholder="Observaciones" id="obs_hojas_seguridad" value="">
										</label>
									</div>
								</div>  
							</div>
						</div>
						<div class="row">
							<div class="col-sm-5">
								<div class="form-group">
									<label for="category">La carga ha sido inspeccionada previamente en ausencia de fugas:</label>
								</div>  
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<div class="inline-group">
										<label class="radio-inline">
											<input type="radio" name="fugas_carga" id="fugas_carga1" value="1">
											<i></i>Si
										</label>
										<label class="radio-inline">
											<input type="radio" name="fugas_carga" id="fugas_carga2" value="2">
											<i></i>No
										</label>
									</div>
								</div>  
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<div class="inline-group">
										<label>
											<input type="text" name="obs_fugas_carga" placeholder="Observaciones" id="obs_fugas_carga" value="">
										</label>
									</div>
								</div>  
							</div>
						</div>
						<div class="row">
							<div class="col-sm-5">
								<div class="form-group">
									<label for="category">La carga se encuentra debidamente embalada, ubicada y ajustada en el vehículo de transporte de acuerdo con el tipo de mercancía:</label>
								</div>  
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<div class="inline-group">
										<label class="radio-inline">
											<input type="radio" name="carga_embalada" id="carga_embalada1" value="1">
											<i></i>Si
										</label>
										<label class="radio-inline">
											<input type="radio" name="carga_embalada" id="carga_embalada2" value="2">
											<i></i>No
										</label>
									</div>
								</div>  
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<div class="inline-group">
										<label>
											<input type="text" name="obs_carga_embalada" placeholder="Observaciones" id="obs_carga_embalada" value="">
										</label>
									</div>
								</div>  
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
						<!-- <button type="button" class="btn btn-default" id="Cancelar" data-dismiss="modal">	Cancelar</button> Evita Cancelar el proceso --> 
						<button type="button" class="btn btn-primary" id="Guardar" onclick="validar_formulario()" data-dismiss="modal">	Guardar</button>
						
					</div>
			</div><!-- /.modal-dialog -->
		</div>
	</form>
</div><!-- /.modal -->
 
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
    
    <!-- MAIN CONTENT -->
    <div id="content">        
        <!-- widget grid -->
        <section id="widget-grid">
            <!-- START ROW -->
            <div class="row">
                <!-- NEW COL START -->
                <article class="col-md-8 col-md-offset-2">
                    
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false" style="align-self: center;">
                        <header>
                            <h2>FORMULARIO HOJA DE RUTA DATOS BÁSICOS</h2>             
                        </header>
                        <!-- widget div-->
                        <div>
                            <!-- widget content -->
                            <div class="widget-body no-padding">
                            	<!--FORMULARIO PARA EL ALMACENAMIENTO DE DATOS BÁSICOS DE LA HOJA DE RUTA -->
                            	
                            		<form id="form1" name="form1" class="smart-form" action="hoja_ruta.php" method="POST" >
                            			<input type="hidden" name="User_idUser_conductor" value="<?php echo isset($id_user) ? $id_user : NULL; ?>">
                            			<input type="hidden" name="idHoja_ruta" value="<?php echo isset($idHoja_ruta) ? $idHoja_ruta : NULL; ?>">
                            			<input type="hidden" name="salida" value="<?php echo isset($hora) ? $hora : NULL; ?>">
                            			
                            			<input type="hidden" name="i_apagado_encendido" id="i_apagado_encendido" value="">
                            			<input type="hidden" name="i_master" id="i_master" value="">
                            			<input type="hidden" name="i_fugas_vehiculo" id="i_fugas_vehiculo" value="">
                            			<input type="hidden" name="i_llantas" id="i_llantas" value="">
                            			<input type="hidden" name="i_frenos" id="i_frenos" value="">
                            			<input type="hidden" name="i_elementos_emergencia" id="i_elementos_emergencia" value="">
                            			<input type="hidden" name="i_listado_telefonos" id="i_listado_telefonos" value="">
                            			<input type="hidden" name="i_identificacion_vehiculo" id="i_identificacion_vehiculo" value="">
                            			<input type="hidden" name="i_hojas_seguridad" id="i_hojas_seguridad" value="">
                            			<input type="hidden" name="i_fugas_carga" id="i_fugas_carga" value="">
                            			<input type="hidden" name="i_carga_embalada" id="i_carga_embalada" value="">
                            			<input type="hidden" name="observaciones_apagado_encendido" id="observaciones_apagado_encendido" value="">
                            			<input type="hidden" name="observaciones_master" id="observaciones_master" value="">
                            			<input type="hidden" name="observaciones_fugas_vehiculo" id="observaciones_fugas_vehiculo" value="">
                            			<input type="hidden" name="observaciones_llantas" id="observaciones_llantas" value="">
                            			<input type="hidden" name="observaciones_frenos" id="observaciones_frenos" value="">
                            			<input type="hidden" name="observaciones_elementos_emergencia" id="observaciones_elementos_emergencia" value="">
                            			<input type="hidden" name="observaciones_listado_telefonos" id="observaciones_listado_telefonos" value="">
                            			<input type="hidden" name="observaciones_identificacion_vehiculo" id="observaciones_identificacion_vehiculo" value="">
                            			<input type="hidden" name="observaciones_hojas_seguridad" id="observaciones_hojas_seguridad" value="">
                            			<input type="hidden" name="observaciones_fugas_carga" id="observaciones_fugas_carga" value="">
                            			<input type="hidden" name="observaciones_carga_embalada" id="observaciones_carga_embalada" value="">


		                            	<fieldset>
		                            		<div class="row">
						  						<section class="col col-4">
						  						<label class="font-lg">Fecha:</label>
							  						<label class="input">
							  							<input type="text" name="fecha" class="input-lg" id="fecha" readonly value="<?php echo isset($fecha) ? $fecha : NULL; ?>">
							  						</label>				  							
						  						</section>
						  						<section class="col col-4">													
													<label class="font-lg">Placa Vehículo:</label>
														<label class='select'>
															<select required name="id_placa_camion" class="input-lg" id="id_placa_camion">
																<option value="">Seleccione...</option>
																<?php
																	$query_1 = "SELECT * 
																			    FROM placa_camion";
																	$result_1 = mysqli_query($con,$query_1);

																	if(mysqli_num_rows($result_1) > 0)
																	{
																		while ($row_1 = mysqli_fetch_assoc($result_1))
																		{
																			$id_placa_camion = $row_1['id_placa_camion'];
																			$placa_camion = $row_1['placa_camion'];
																			if($id_placa_camion != 6)
																			{
																				if($placa == $id_placa_camion)
																				{
																					?>
																					<option selected value="<?php echo $id_placa_camion; ?>"><?php echo $placa_camion; ?></option>";
																					<?php		
																				}
																				else
																				{
																					?>
																					<option value="<?php echo $id_placa_camion; ?>"><?php echo $placa_camion; ?></option>";
																					<?php		
																				}
																				
																			}
																		}
																	}
																?>													
															</select><i></i>
														</label>
												</section>
												<section class="col col-4">
							  					<label class="font-lg">Odómetro Salida:</label>
							  						<label class="input">
							  							<input type="number" class="input-lg" name="odometro_salida" id="odometro_salida" placeholder="Odómetro Salida" title="Ingrese el kilometraje de salida" value="<?php echo isset($odometro_salida) ? $odometro_salida : NULL; ?>" >
							  						</label>
							  					</section>						  									  						
						  					</div>	
		                            		<div class="row">
		                            			<section class="col col-6">
						  						<label class="font-lg">Conductor:</label>
							  						<label class="input">
							  							<input type="text" name="conductor" class="input-lg" id="conductor" readonly value="<?php echo isset($conductor) ? $conductor : NULL; ?>">
							  						</label>				  							
						  						</section>							  					
								  				<section class="col col-6">													
													<label class="font-lg">Ayudante:</label>
														<label class='select'>
															<select required name="User_idUser_ayudante" class="input-lg" id="ayudante">
																<option value="">Seleccione...</option>
																<option value="500">Ninguno</option>
																<?php

																	if($User_idUser_ayudante == "500")
																	{
																		?>
																		<option selected value="500">Ninguno</option>
																		<?php
																	}

																	$query_1 = "SELECT idUser, Name, LastName
																			    FROM user A
																			    INNER JOIN user_has_profile B
																			    WHERE A.idUser = B.User_idUser AND B.profile_idprofile = 10 AND B.state = 1";
																	$result_1 = mysqli_query($con,$query_1);

																	if(mysqli_num_rows($result_1) > 0)
																	{
																		while ($row_1 = mysqli_fetch_assoc($result_1))
																		{
																			$idUser_ayudante = $row_1['idUser'];
																			$Name = $row_1['Name'];
																			$LastName = $row_1['LastName'];
																			$ayudante = $Name." ".$LastName;
																			if($User_idUser_ayudante == $idUser_ayudante)
																			{
																			?>
																				<option selected value="<?php echo $idUser_ayudante; ?>"><?php echo $ayudante; ?></option>
																			<?php
																			}
																			else
																			{		
																				?>
																				<option value="<?php echo $idUser_ayudante; ?>"><?php echo $ayudante; ?></option>
																				<?php
																			}	
																		}
																	}
																?>													
															</select><i></i>
														</label>
												</section>

						  					</div>
						  					
						  					<div class="row">
						  						<?php
							  					if($estado == 0 || $estado == 2 || $estado == 3)
							  					{							  						
							  						?>
									  				<section class="col col-4">													
														<label class="font-lg">Agencia Salida:</label>
															<label class='select'>
																<select required name="agencia_salida" class="input-lg">
																	<option value="">Seleccione...</option>
																	<option <?php if($agencia_salida == 1) echo "selected"; ?> value="1">Toberín</option>
																	<option <?php if($agencia_salida == 2) echo "selected"; ?> value="2">Cazucá</option>
																	<option <?php if($agencia_salida == 3) echo "selected"; ?> value="3">Manizales</option>
																	<option <?php if($agencia_salida == 4) echo "selected"; ?> value="4">Medellin</option>
																	<?php
																	/*if($agencia_salida == 1)
																	{
																		?>
																		<option selected value="1">Toberín</option>
																		<option value="2">Cazucá</option>
																		<option value="3">Manizales</option>
																		<option value="4">Medellin</option>
																		<?php
																	}
																	else
																	{
																		if($agencia_salida == 2)
																		{
																			?>
																			<option selected value="2">Cazucá</option>
																			<option value="1">Toberín</option>
																			<option value="3">Manizales</option>
																			<option value="4">Medellin</option>
																			<?php
																		}
																		else
																		{
																			if($agencia_salida == 3)
																			{
																				?>
																				<option selected value="3">Manizales</option>
																				<option value="1">Toberín</option>
																				<option value="2">Cazucá</option>
																				<option value="4">Medellin</option>
																				<?php
																			}
																			else
																			{
																				?>
																				<option value="1">Toberín</option>
																				<option value="2">Cazucá</option>
																				<option value="3">Manizales</option>
																				<option value="4">Medellin</option>
																				<?php
																			}	
																		}
																	}*/
																	?>											
																</select><i></i>
															</label>
													</section>
													<?php 
							  					}

							  					if($estado == 1 || $estado == 2 || $estado == 3)
							  					{
							  						?>
						  						
									  				<section class="col col-4">													
														<label class="font-lg">Agencia Llegada:</label>
															<label class='select'>
																<select required name="agencia_llegada" class="input-lg">
																	<option value="">Seleccione...</option>
																	<option <?php if($agencia_llegada == 1) echo "selected"; ?> value="1">Toberín</option>
																	<option <?php if($agencia_llegada == 2) echo "selected"; ?> value="2">Cazucá</option>
																	<option <?php if($agencia_llegada == 3) echo "selected"; ?> value="3">Manizales</option>
																	<option <?php if($agencia_llegada == 4) echo "selected"; ?> value="4">Medellin</option>
																	<?php
																	/*if($agencia_llegada == 1)
																	{
																		?>
																		<option selected value="1">Toberin</option>
																		<option value="2">Cazucá</option>
																		<option value="3">Manizales</option>
																		<option value="4">Medellin</option>
																		<?php
																	}
																	else
																	{
																		if($agencia_llegada == 2)
																		{
																			?>
																			<option selected value="2">Cazucá</option>
																			<option value="1">Toberín</option>
																			<option value="3">Manizales</option>
																			<option value="4">Medellin</option>
																			<?php
																		}
																		else
																		{
																			if($agencia_llegada == 3)
																			{
																				?>
																				<option selected value="3">Manizales</option>
																				<option value="1">Toberín</option>
																				<option value="2">Cazucá</option>
																				<option value="4">Medellin</option>
																				<?php
																			}
																			else
																			{
																				?>
																				<option value="1">Toberín</option>
																				<option value="2">Cazucá</option>
																				<option value="3">Manizales</option>
																				<option value="4">Medellin</option>
																				<?php
																			}	
																		}
																	}	*/																
																	?>																				
																</select><i></i>
															</label>
													</section>
													<section class="col col-4">
								  					<label class="font-lg">Odómetro Llegada:</label>
								  						<label class="input">
								  							<input type="number" class="input-lg" name="odometro_llegada" id="odometro_llegada" placeholder="Odómetro llegada" title="Ingrese el kilometraje de llegada" required value="<?php echo isset($odometro_llegada) ? $odometro_llegada : NULL; ?>" >
								  						</label>
								  					</section>
							  						<?php 
							  					}
							  					?>												  										
							  				</div>
							  				<div class="row">
							  					<?php
							  					if ($estado == 3 || $estado == 2) 
							  					{
							  						?>
							  						<section class="col col-4">
							  						<label class="font-lg">Hora y Fecha de Salida:</label>
								  						<label class="input">
								  							<input type="text" name="salida" class="input-lg" id="salida" readonly value="<?php echo isset($salida) ? $salida : NULL, ' (', isset($fecha) ? $fecha : NULL, ')'; ?>">
								  						</label>				  							
							  						</section>
							  						<section class="col col-4">
							  						<label class="font-lg">Hora Y Fecha Llegada:</label>
								  						<label class="input">
								  							<input type="text" name="llegada" class="input-lg" id="llegada " readonly value="<?php echo isset($llegada ) ? $llegada  : NULL; ?>">
								  						</label>
								  					</section>
								  					<section class="col col-4">
							  						<label class="font-lg">Recorrido:</label>
							  						<label class="input">
							  							<input type="text" class="input-lg" name="recorrido" id="recorrido" readonly required value="<?php echo isset($recorrido) ? $recorrido : NULL; ?>" >
							  						</label>
							  					</section>	
							  						<?php
							  					}
							  					?>							  					
							  				</div>
		                            	</fieldset>
		                            	<?php
		                            	if(in_array(58, $acc))
		                            	{		                            		
			                            	if($estado == 0)
			                            	{	
			                            		if(in_array(81, $acc)){		                            		
			                            			?>
				                            		<footer>
				                                		<button type="button" id="boton_verificacion_diaria" name="verificacion_diaria" class="btn btn-primary" onclick="mostrar_verificacion()">REALIZAR VERIFICACION DIARIA</button>

				                                		<input type="submit" style="display: none;" id="boton_guardar_datos_basicos" name="guardar_datos_basicos"  class="btn btn-primary" value="GUARDAR DATOS BÁSICOS">
				                                	</footer>
			                                		<?php
		                                		}else{
		                                			?>
				                            		<footer>
				                                		<input type="submit" name="guardar_datos_basicos" class="btn btn-primary" value="GUARDAR DATOS BÁSICOS">
				                                	</footer>
			                                		<?php
		                                		}
		                                	}
		                                	if($estado == 1)
		                                	{
		                                		?>
			                            		<footer>
			                                		<button type="submit" class="btn btn-danger" name="finalizar_hoja_ruta">FINALIZAR HOJA DE RUTA</button>
			                                	</footer>			                                	
		                                		<?php
		                                	}
		                                	if($estado == 2)
		                                	{
		                                		?>
			                            		<footer>
			                            			<a href="pdf_verificacion_diaria.php?idHoja_ruta=<?php echo $idHoja_ruta; ?>">VER VERIFICACIÓN DIARIA</a>

			                                		<button type="submit" class="btn btn-warning" name="revisar_hoja_ruta">FINALIZAR REVISIÓN</button>
			                                	</footer>			                                	
		                                		<?php
		                                	}
		                            	}
		                                ?>
	                                </form>
                            	<?php                            	
                            	if($estado == 1)
                            	{
                            		?>
                            		<!--FORMULARIO PARA EL ALMACENAMIENTO DE RECORRIDOS DE LA HOJA DE RUTA -->
	                                <form id="form2" class="smart-form" action="hoja_ruta.php" method="POST" name="form2">
	                                	<input type="hidden" name="User_idUser_conductor" value="<?php echo isset($id_user) ? $id_user : NULL; ?>">
                            			<input type="hidden" name="idHoja_ruta" value="<?php echo isset($idHoja_ruta) ? $idHoja_ruta : NULL; ?>">
                            			<input type="hidden" name="salida" value="<?php echo isset($hora) ? $hora : NULL; ?>">
                            			<input type="hidden" name="id_cliente">
                            			
	                                	<fieldset>
	                                		<div>
	                                			<div class="row">
	                                				<div class="page-header" style="padding: 15px;">
												    	<h1 style="font-size: 40px; font-weight: bold; color: red;">Datos del Cliente</h1>      
												  	</div>
													<section class="col col-12">
														<label class="font-lg">Nombre Cliente:</label>
														<div class="input-group">
															<?php
								  								if($contador_finalizacion!=0){
										  							?>
																	<label class="input">
																		<input type="text" name="nombre2" class="input-lg" id="nombre2" placeholder="Nombre o Razón Social"  required value="<?php echo $nombre_pdte; ?>" readOnly>																
																	</label>
																	<span class="input-group-btn">
																		<button type="button" name="cambiar_cliente" class="btn btn-warning btn-lg" data-toggle="modal" data-target="#modal_cliente" disabled>Seleccionar Cliente</button>
																	</span>
																	<?php
								  								}else{
										  							?>
										  							<label class="input">
																		<input type="text" name="nombre2" class="input-lg" id="nombre2" placeholder="Nombre o Razón Social"  required value="<?php echo isset($nombre) ? $nombre : NULL; ?>" >																
																	</label>
																	<span class="input-group-btn">
																		<button type="button" name="cambiar_cliente" class="btn btn-warning btn-lg" data-toggle="modal" data-target="#modal_cliente">Seleccionar Cliente</button>
																	</span>
																	<span class="input-group-btn">
																		<button type="button" name="cambiar_cliente" class="btn btn-danger btn-lg" onclick="sin_cliente()">No encontrado</button>
																	</span>										
																	<?php
										  							}
										  							?>
														</div>
													</section>
												</div>	                                			
							  					<div class="row">
							  						<section class="col col-4">
							  						<label class="font-lg">Número Factura:</label>
								  						<label class="input">
								  							<?php
								  								if($contador_finalizacion!=0){
								  							?>
								  								<input type="text" name="num_factura" class="input-lg" id="num_factura" placeholder="Número de Factura" title="Ingrese el número de factura" value="<?php echo isset($num_factura_pdte) ? $num_factura_pdte : NULL; ?>" readOnly required>
								  							<?php
								  								}else{
								  							?>
								  								<input type="text" name="num_factura" class="input-lg" id="num_factura" placeholder="Número de Factura" title="Ingrese el número de factura" value="<?php echo isset($num_factura) ? $num_factura : NULL; ?>" required>
								  							<?php
								  							}
								  							?>
								  						</label>				  							
							  						</section>
							  						<section class="col col-5">
								  					<label class="font-lg">CME:</label>
								  						<label class="input">
								  							<div class="input-group">
									  							<?php
									  								if($contador_finalizacion!=0){
										  							?>
										  								<input type="text" class="input-lg" name="num_cme" id="num_cme" placeholder="Número CME" title="Ingrese el CME asignado" required value="<?php echo isset($num_cme_pdte) ? $num_cme_pdte : NULL; ?>" readOnly>
										  							<?php
										  								}else{
										  							?>
										  								<input type="text" class="input-lg" name="num_cme" id="num_cme" placeholder="Número CME" title="Ingrese el CME asignado" required value="<?php echo isset($num_cme) ? $num_cme : NULL; ?>" >
										  								<span class="input-group-btn">
																			<button type="button" name="cambiar_cliente" class="btn btn-danger btn-lg" onclick="sin_cme()">Sin CME</button>
																		</span>	
										  							<?php
										  							}
										  						?>
									  						</div>
								  						</label>
								  					</section>				  						
							  					</div>
							  					<div class="row">
							  						<section class="col col-6">
							  							<label class="font-lg">Hora Llegada:</label>
							  							<?php
								  							if($contador_finalizacion!=0){
										  						?>
																<div class="input-group">
																	<label class="input">
																		<input type="text" name="hora_llegada" class="input-lg" readonly required value="<?php echo isset($hora_llegada_pdte) ? $hora_llegada_pdte : NULL; ?>">
																	</label>
																	<span class="input-group-btn">
																		<button type="button" name="registro_llegada" class="btn btn-warning btn-lg" onclick="reg_hora_llegada();" disabled>Registrar
																		</button>
																	</span>
																</div>
																<?php
								  							}else{
										  						?>
										  						<div class="input-group">
																	<label class="input">
																		<input type="text" name="hora_llegada" class="input-lg" readonly required value="<?php echo isset($hora_llegada) ? $hora_llegada : NULL; ?>">
																	</label>
																
																	<span class="input-group-btn">
																		<button type="button" name="registro_llegada" class="btn btn-warning btn-lg" onclick="reg_hora_llegada();">Registrar
																		</button>
																	</span>
																</div>
																<?php
								  							}
								  						?>														
													</section>
													<?php 
													if(($idHoja_ruta == 2620)&&($estado_recorrido_pdte != 1)){
														?>
														<section class="col col-4">
															<label class="font-lg">&nbsp;</label>
															<span class="input-group-btn">
																<input type="submit" name="guardado_parcial" id="guardado_parcial" class="btn-primary btn-lg" value="Punto de Control">
																<!--<button type="button" name="guardado_parcial" class="btn-primary btn-lg">Punto de Control</button>-->
															</span>
														</section>
														<?php
													}
													?>
							  					</div>

							  					<div class="row">
							  						<section class="col col-6">
														<label class="font-lg">Hora Salida:</label>
														<div class="input-group">
															<label class="input">
																<input type="text" name="hora_salida" class="input-lg" readonly required value="<?php echo isset($hora_llegada) ? $hora_llegada : NULL; ?>">
															</label>
															<span class="input-group-btn">
																<button type="button" name="registro_salida" class="btn btn-warning btn-lg" onclick="reg_hora_salida();">Registrar
																</button>
															</span>
														</div>
													</section>	
							  					</div>
							  					<div class="row">							  						
							  						<section class="col col-4">
								  					<label class="font-lg">Odómetro:</label>
								  						<label class="input">
								  							<input type="number" class="input-lg" name="odometro" id="odometro" placeholder="Odómetro Salida" title="Ingrese el kilometraje de salida" value="<?php echo isset($odometro) ? $odometro : NULL; ?>" >
								  						</label>
								  					</section>				  						
							  					</div>
							  					<div class="row">
							  						<div class="page-header" style="padding: 15px;">
												    	<h1 style="font-size: 40px; font-weight: bold; color: red;">Cilindros</h1>      
												  	</div>
						  							<section class="col col-4">
							  						<label class="font-lg">Entregados:</label>
								  						<label class="input">
								  							<input type="number" name="cili_entregado" class="input-lg" id="cili_entregado" placeholder="Entregados" value="<?php echo isset($cili_entregado) ? $cili_entregado : NULL; ?>">
								  						</label>				  							
							  						</section>
							  						<section class="col col-4">
								  					<label class="font-lg">Recibidos:</label>
								  						<label class="input">
								  							<input type="number" class="input-lg" name="cili_recibido" id="cili_recibido" placeholder="Recibidos" title="Ingrese el kilometraje de salida" value="<?php echo isset($cili_recibido) ? $cili_recibido : NULL; ?>" >
								  						</label>
								  					</section>
							  					</div>
							  					<div class="row">
							  						<div class="page-header" style="padding: 15px;">
												    	<h1 style="font-size: 40px; font-weight: bold; color: red;">Termos</h1>      
												  	</div>
						  							<section class="col col-4">
							  						<label class="font-lg">Entregados:</label>
								  						<label class="input">
								  							<input type="number" name="termo_entregado" class="input-lg" id="termo_entregado" placeholder="Entregados" value="<?php echo isset($termo_entregado) ? $termo_entregado : NULL; ?>">
								  						</label>				  							
							  						</section>
							  						<section class="col col-4">
								  					<label class="font-lg">Recibidos:</label>
								  						<label class="input">
								  							<input type="number" class="input-lg" name="termo_recibido" id="termo_recibido" placeholder="Recibidos" title="Ingrese el kilometraje de salida"  value="<?php echo isset($termo_recibido) ? $termo_recibido : NULL; ?>" >
								  						</label>
								  					</section>
							  					</div>
							  					<div class="row">
							  						<div class="page-header" style="padding: 15px;">
												    	<h1 style="font-size: 40px; font-weight: bold; color: red;">LIN</h1>      
												  	</div>
						  							<section class="col col-4">
							  						<label class="font-lg">Litros:</label>
								  						<label class="input">
								  							<input type="number" name="lin_litros" class="input-lg" id="lin_litros" placeholder="Litros" value="<?php echo isset($lin_litros) ? $lin_litros : NULL; ?>">
								  						</label>				  							
							  						</section>							  						
							  					</div>
							  					<div class="row">
							  						<div class="page-header" style="padding: 15px;">
												    	<h1 style="font-size: 40px; font-weight: bold; color: red;">Otros</h1>      
												  	</div>
												  	<section class="col col-4">
							  						<label class="font-lg">Item:</label>
								  						<label class="input">
								  							<input type="text" name="otros_item" class="input-lg" id="otros_item" placeholder="Elemento" value="<?php echo isset($otros_item) ? $otros_item : NULL; ?>">
								  						</label>				  							
							  						</section>
						  							<section class="col col-4">
							  						<label class="font-lg">Entregados:</label>
								  						<label class="input">
								  							<input type="number" name="otros_entregado" class="input-lg" id="otros_entregado" placeholder="Entregados" value="<?php echo isset($otros_entregado) ? $otros_entregado : NULL; ?>">
								  						</label>				  							
							  						</section>
							  						<section class="col col-4">
								  					<label class="font-lg">Recibidos:</label>
								  						<label class="input">
								  							<input type="number" class="input-lg" name="otros_recibido" id="otros_recibido" placeholder="Recibidos" title="Ingrese el kilometraje de salida" value="<?php echo isset($otros_recibido) ? $otros_recibido : NULL; ?>" >
								  						</label>
								  					</section>
							  					</div>
							  					<div class="row">
							  						<div class="page-header" style="padding: 15px;">
												    	<h1 style="font-size: 40px; font-weight: bold; color: red;">Dinero</h1>      
												  	</div>
						  							<section class="col col-4">
							  						<label class="font-lg">Valor:</label>
								  						<label class="input">
								  							<input type="number" name="dinero_valor" class="input-lg" id="dinero_valor" placeholder="Recibido" value="<?php echo isset($dinero_valor) ? $dinero_valor : NULL; ?>">
								  						</label>				  							
							  						</section>
							  						<section class="col col-4">
								  					<label class="font-lg">Efectivo/Cheque:</label>
								  						<label class="select" class="">								  							
								  							<select name="efectivo_cheque" class="input-lg">
								  								<option value="0">Seleccione...</option>
								  								<option value="Efectivo">Efectivo</option>
								  								<option value="Cheque">Cheque</option>
								  							</select><i></i>
								  						</label>
								  					</section>
							  					</div>
							  					<div class="row">
							  						<div class="page-header" style="padding: 15px;">
												    	<h1 style="font-size: 40px; font-weight: bold; color: red;">Observaciones</h1>      
												  	</div>
							  						<section class="col col-4">
								  						<label class="input">
								  							<textarea class="form-control" rows="5" id="comment" name="observaciones"><?php echo isset($observaciones) ? $observaciones : NULL; ?></textarea>
								  						</label>
								  					</section>
							  					</div>
	                                		</div>		                            		
		                            	</fieldset>
		                                <?php
		                            	if(in_array(58, $acc))
		                            	{
		                            		?>
		                            		<footer>
		                                		<input type="submit" name="guardar_datos_hoja_ruta" class="btn btn-primary" value="GUARDAR RECORRIDO" onclick="alerta_espera()" id="guardar_datos_hoja_ruta">
		                                	</footer>
		                                	<?php
		                            	}
		                                ?>
	                                </form>
                            		<?php
                            	}                            	
                            	?>
                            </div>
                            <!-- end widget content -->
                            
                        </div>
                        <!-- end widget div -->
                        
                    </div>
                    <!-- end widget -->                             
                </article>
            </div>
            <div class="row">
				<article>
                    <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false" style="align-self: center;">
                    	<div style="overflow-x:auto;">
                    	<?php
                    		if($estado != 1)
							{
								?>
								<header>
		                            <h2>RESÚMEN HOJA DE RUTA</h2>             
		                        </header>
								<div>
									<!-- widget content -->
                            		<div class="widget-body no-padding">
										<table class="table table-striped table-bordered table-hover" width="100%" align="center">
											<thead>
												<tr>
													<th rowspan="2">#</th>
													<!--<?php
													//if (in_array(59, $acc) && $estado != 3)
													{
													//	echo '<th rowspan="2">EDITAR</th>';	
													}
													?>-->
													<th rowspan="2">EDITAR</th>
													<th rowspan="2">CLIENTE</th>
													<th rowspan="2">ODOMETRO</th>
													<th rowspan="2">FACTURA</th>
													<th rowspan="2">CME</th>
													<th colspan="2" style="text-align: center;">HORA</th>
													<th colspan="2" style="text-align: center;">CILINDROS</th>
													<th colspan="2" style="text-align: center;">TERMOS</th>
													<th colspan="1" style="text-align: center;">LIN</th>
													<th colspan="3" style="text-align: center;">OTROS</th>
													<th colspan="2" style="text-align: center;">DINERO</th>
													<th rowspan="2" style="text-align: center;">ERRORES</th>
													<th rowspan="2" style="text-align: center;">OBSERVACIONES</th>
												</tr>
												<tr>	                            					
													<th>LLEGADA</th>
													<th>SALIDA</th>
													<th>ENT.</th>
													<th>REC.</th>
													<th>ENT.</th>
													<th>REC.</th>
													<th>LITROS</th>
													<th>ITEM</th>
													<th>ENT.</th>
													<th>REC.</th>
													<th>VALOR</th>
													<th>TIPO</th>
												</tr>
											</thead>
											<tbody>
												<?php
													$consulta_7 = "SELECT * FROM datos_hoja_ruta
																   WHERE idHoja_ruta = '".$idHoja_ruta."'";

													$resultado_7 = mysqli_query($con,$consulta_7);

													if(mysqli_num_rows($resultado_7)> 0)
													{
														$contador = 0;
														while($linea_7 = mysqli_fetch_assoc($resultado_7))
														{
															$contador++;
															$id_datos_hoja_ruta = $linea_7['id_datos_hoja_ruta'];
															$id_cliente = $linea_7['id_cliente'];
															$odometro = $linea_7['odometro'];
															$num_factura = $linea_7['num_factura'];
															$num_cme = $linea_7['num_cme'];
															$hora_llegada = $linea_7['hora_llegada'];
															$hora_salida = $linea_7['hora_salida'];
															$cili_entregado = $linea_7['cili_entregado'];
															$cili_recibido = $linea_7['cili_recibido'];
															$termo_entregado = $linea_7['termo_entregado'];
															$termo_recibido = $linea_7['termo_recibido'];
															$lin_litros = $linea_7['lin_litros'];
															$otros_item = $linea_7['otros_item'];
															$otros_entregado = $linea_7['otros_entregado'];
															$otros_recibido = $linea_7['otros_recibido'];
															$dinero_valor = $linea_7['dinero_valor'];
															$efectivo_cheque = $linea_7['efectivo_cheque'];
															$num_errores = $linea_7['num_errores'];
															$observaciones= $linea_7['observaciones'];

															$consulta_8 = "SELECT nombre 
															               FROM clientes 
															               WHERE id_cliente = '".$id_cliente."'";
															$resultado_8 = mysqli_query($con,$consulta_8);
															if(mysqli_num_rows($resultado_8) > 0)
															{
																$linea_8 = mysqli_fetch_assoc($resultado_8);
																$nombre_cliente = $linea_8['nombre'];
															}mysqli_free_result($resultado_8);


															if ($efectivo_cheque == "0")
															{
																$efectivo_cheque = "N/A";
															}
															else
															{
																$efectivo_cheque = $efectivo_cheque;	
															}
															?>
															<tr>
																<td><?php echo $contador; ?></td>
																<?php
																	if(in_array(59, $acc))
																	{
																		?>
																		<td width="5" align="center">
								                                        	<a href="editar_hoja_ruta.php?id_datos_hoja_ruta=<?php echo $id_datos_hoja_ruta; ?>"><img src="img/edit.png" width="30" height="30"></a>
								                                        </td>
																		<?php
																	}
																?>
																<td><?php echo $nombre_cliente; ?></td>
																<td><?php echo $odometro; ?></td>
																<td><?php echo $num_factura; ?></td>
																<td><?php echo $num_cme; ?></td>
																<td><?php echo $hora_llegada; ?></td>
																<td><?php echo $hora_salida; ?></td>
																<td><?php echo $cili_entregado; ?></td>
																<td><?php echo $cili_recibido; ?></td>
																<td><?php echo $termo_entregado; ?></td>
																<td><?php echo $termo_recibido; ?></td>
																<td><?php echo $lin_litros; ?></td>
																<td><?php echo $otros_item; ?></td>
																<td><?php echo $otros_entregado; ?></td>
																<td><?php echo $otros_recibido; ?></td>
																<td><?php echo $dinero_valor; ?></td>
																<td><?php echo $efectivo_cheque; ?></td>
																<td><?php echo $num_errores; ?></td>
																<td><?php echo $observaciones; ?></td>
															</tr>
															<?php
														}
													}

												?>											
											</tbody>
										</table>
									</div>
								</div>
								<?php
							}
                    	?>
                    </div>
                    </div>

                </article>
                <!-- END COL -->        

            </div>

            <!-- END ROW -->

        </section>
        <!-- end widget grid -->
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- Modal para la seleccion del cliente asociado a la Hoja de ruta actual -->
	    <div class="modal fade" id="modal_cliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">
			<form name="mezcla" method="POST" id="mezcla">
			<input type="hidden" name="id_cilindro_eto"  value="id_cilindro_eto">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							&times;
							</button>
							<h4 class="modal-title" id="myModalLabel">Seleccione el Cliente</h4>
						</div>
						<div class="modal-body">
							<div class="row">
								<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">						
									<form  name="form2" id="form2">							
										<thead>
											<tr>
												<th>#</th>
												<th>Seleccionar</th>
												<th>Cliente</th>
												<th>Nit</th>									
											</tr>
										</thead>
										<tbody>										
											<?php                                
			                        			$contador = "0";
			                        	        $consulta = "SELECT * FROM clientes";
			                                    $resultado = mysqli_query($con,$consulta);
			                                    if(mysqli_num_rows($resultado) > 0 )
			                                    {
			                                    	while ($linea = mysqli_fetch_assoc($resultado))
				                                    {
				                                        $contador = $contador + 1;
				                                        $id_cliente = $linea["id_cliente"];
				                                        $nombre = $linea["nombre"];				                                        
				                                        $nit = $linea["nit"];
				                                    ?>
				                                        <tr>                                                	
				                                            <td width="5"><?php echo $contador; ?></td>
				                                            <td align="center"><a href="#" onclick="g_cliente('<?php echo $id_cliente; ?>','<?php echo $nombre; ?>')"><img src="img/point-at.png" width="20" height="20"></a></td>
				                                            <td><strong><?php echo $nombre; ?></strong></td>
				                                            <td><strong><?php echo $nit; ?></strong></td>
				                                    	</tr>   
				                                    <?php
				                                    }mysqli_free_result($resultado); 
			                                    }			                                    
			                                ?>
										</tbody>
									</form>
								</table>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">	Cancel</button>	
						</div>
					</div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			</form>
		</div>	
		<!-- Fin Modal -->

<!-- END MAIN PANEL -->

<!-- ==========================CONTENT ENDS HERE ========================== -->


<?php
	include("inc/footer.php");
	include("inc/scripts.php"); 
?>

<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="js/plugin/datatable-responsive/datatables.responsive.min.js"></script>

<script type="text/javascript">

	/*function alerta_espera(){
		window.alert("Sun Information se esta transmitiendo...");

		document.getElementById("guardar_datos_hoja_ruta").disabled = true;
	}*/

	function g_cliente(id_cliente,nombre_cliente) 
	{
		$('#modal_cliente').modal('hide');
		//$('input:hidden[name=id_cliente]').val(num_cili1);
		

		$("input[name=id_cliente]").val(id_cliente);
		$("input[name=nombre2]").val(nombre_cliente);		
	}
</script>
<script type="text/javascript">
	function reg_hora_llegada()
	{
		var d = new Date();
		var hora = d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
		var fecha = d.getDate()+"-"+(d.getMonth()+1)+"-"+d.getFullYear();
		var r = confirm("¿Está seguro de registrar esta hora de llegada?" + hora + " (" + fecha + ")");
		if(r)
		{
			$("input[name=hora_llegada").val(hora+" ("+fecha+")");
		}

	}
	function sin_cme(){
		var caracteres = "0123456789abcdefABCDEF";
		var longitud = 5;
		var code = "";

		for (x=0; x < longitud; x++)
		{
			rand = Math.floor(Math.random()*caracteres.length);
			code += caracteres.substr(rand, 1);
		}
		
		document.getElementById("num_cme").value = code;
	}
</script>
<script type="text/javascript">
	function reg_hora_salida()
	{
		var d = new Date();
		var hora = d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
		var fecha = d.getDate()+"/"+(d.getMonth()+1)+"/"+d.getFullYear();
		var r = confirm("¿Está seguro de registrar esta hora de salida?" + hora + " (" + fecha + ")");
		if(r)
		{
			$("input[name=hora_salida").val(hora+" ("+fecha+")");
		}

	}
</script>

<script type="text/javascript">
	function mostrar_verificacion(){
		$('#modal_verificacion').modal({
			backdrop: 'static', 
			keyboard: false});
	}
</script>

<script type="text/javascript">

	function sin_cliente(){
		$("input[name=id_cliente]").val('897');
		$("input[name=nombre2]").val('No encontrado');
	}

	function validar_formulario (){

		var obs_apagado_encendido = document.getElementById('obs_apagado_encendido').value;
		var obs_master = document.getElementById('obs_master').value;
		var obs_fugas_vehiculo = document.getElementById('obs_fugas_vehiculo').value;
		var obs_llantas = document.getElementById('obs_llantas').value;
		var obs_frenos = document.getElementById('obs_frenos').value;
		var obs_elementos_emergencia = document.getElementById('obs_elementos_emergencia').value;
		var obs_listado_telefonos = document.getElementById('obs_listado_telefonos').value;
		var obs_identificacion_vehiculo = document.getElementById('obs_identificacion_vehiculo').value;
		var obs_hojas_seguridad = document.getElementById('obs_hojas_seguridad').value;
		var obs_fugas_carga = document.getElementById('obs_fugas_carga').value;
		var obs_carga_embalada = document.getElementById('obs_carga_embalada').value;
		

		if(document.getElementById("apagado_encendido1").checked){
			apagado_encendido_1 = 1;
		}else{
			apagado_encendido_1 = 2;
		}

		if(document.getElementById("master1").checked){
			master_1 = 1;
		}else{
			master_2 = 2;
		}

		if(document.getElementById("fugas_vehiculo1").checked){
			fugas_vehiculo_1 = 1;
		}else{
			fugas_vehiculo_1 = 2;
		}
		
		if(document.getElementById("llantas1").checked){
			llantas_1 = 1;
		}else{
			llantas_1 = 2;
		}

		if(document.getElementById("frenos1").checked){
			frenos_1 = 1;
		}else{
			frenos_1 = 2;
		}

		if(document.getElementById("elementos_emergencia1").checked){
			elementos_emergencia_1 = 1;
		}else{
			elementos_emergencia_1 = 2;
		}

		if(document.getElementById("listado_telefonos1").checked){
			listado_telefonos_1 = 1;
		}else{
			listado_telefonos_1 = 2;
		}

		if(document.getElementById("identificacion_vehiculo1").checked){
			identificacion_vehiculo_1 = 1;
		}else{
			identificacion_vehiculo_1 = 2;
		}

		if(document.getElementById("hojas_seguridad1").checked){
			hojas_seguridad_1 = 1;
		}else{
			hojas_seguridad_1 = 2;
		}

		if(document.getElementById("fugas_carga1").checked){
			fugas_carga_1 = 1;
		}else{
			fugas_carga_1 = 2;
		}

		if(document.getElementById("carga_embalada1").checked){
			carga_embalada_1 = 1;
		}else{
			carga_embalada_1 = 2;
		}

		document.getElementById("i_apagado_encendido").value = apagado_encendido_1;
		document.getElementById("i_master").value = master_1;
		document.getElementById("i_fugas_vehiculo").value = fugas_vehiculo_1;
		document.getElementById("i_llantas").value = llantas_1;
		document.getElementById("i_frenos").value = frenos_1;
		document.getElementById("i_elementos_emergencia").value = elementos_emergencia_1;
		document.getElementById("i_listado_telefonos").value = listado_telefonos_1;
		document.getElementById("i_identificacion_vehiculo").value = identificacion_vehiculo_1;
		document.getElementById("i_hojas_seguridad").value = hojas_seguridad_1;
		document.getElementById("i_fugas_carga").value = fugas_carga_1;
		document.getElementById("i_carga_embalada").value = carga_embalada_1;

		document.getElementById("observaciones_apagado_encendido").value = obs_apagado_encendido;
		document.getElementById("observaciones_master").value = obs_master;
		document.getElementById("observaciones_fugas_vehiculo").value = obs_fugas_vehiculo;
		document.getElementById("observaciones_llantas").value = obs_llantas;
		document.getElementById('observaciones_frenos').value = obs_frenos;
		document.getElementById('observaciones_elementos_emergencia').value = obs_elementos_emergencia;
		document.getElementById('observaciones_listado_telefonos').value = obs_listado_telefonos;
		document.getElementById('observaciones_identificacion_vehiculo').value = obs_identificacion_vehiculo;
		document.getElementById('observaciones_hojas_seguridad').value = obs_hojas_seguridad;
		document.getElementById('observaciones_fugas_carga').value = obs_fugas_carga;
		document.getElementById('observaciones_carga_embalada').value = obs_carga_embalada;

		document.getElementById("boton_verificacion_diaria").style.display = 'none';
		document.getElementById("boton_guardar_datos_basicos").style.display = "block";
	}
	
</script>

<script type="text/javascript">

// DO NOT REMOVE : GLOBAL FUNCTIONS!

$(document).ready(function() {

	
	/* // DOM Position key index //
		
	l - Length changing (dropdown)
	f - Filtering input (search)
	t - The Table! (datatable)
	i - Information (records)
	p - Pagination (paging)
	r - pRocessing 
	< and > - div elements
	<"#id" and > - div with an id
	<"class" and > - div with a class
	<"#id.class" and > - div with an id and class
	
	Also see: http://legacy.datatables.net/usage/features
	*/	

	/* BASIC ;*/
		var responsiveHelper_dt_basic = undefined;
		var responsiveHelper_datatable_fixed_column = undefined;
		var responsiveHelper_datatable_col_reorder = undefined;
		var responsiveHelper_datatable_tabletools = undefined;
		
		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};

		$('#dt_basic').dataTable({
			"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
			"autoWidth" : true,
			"preDrawCallback" : function() {
				// Initialize the responsive datatables helper once.
				if (!responsiveHelper_dt_basic) {
					responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
				}
			},
			"rowCallback" : function(nRow) {
				responsiveHelper_dt_basic.createExpandIcon(nRow);
			},
			"drawCallback" : function(oSettings) {
				responsiveHelper_dt_basic.respond();
			}
		});

	/* END BASIC */
	
	/* COLUMN FILTER  */
    var otable = $('#datatable_fixed_column').DataTable({
    	//"bFilter": false,
    	//"bInfo": false,
    	//"bLengthChange": false
    	//"bAutoWidth": false,
    	//"bPaginate": false,
    	//"bStateSave": true // saves sort state using localStorage
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_fixed_column) {
				responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_fixed_column.respond();
		}		
	
    });
    
    // custom toolbar
    $("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');
    	   
    // Apply the filter
    $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {
    	
        otable
            .column( $(this).parent().index()+':visible' )
            .search( this.value )
            .draw();
            
    } );
    /* END COLUMN FILTER */   

	/* COLUMN SHOW - HIDE */
	$('#datatable_col_reorder').dataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_col_reorder) {
				responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_col_reorder.respond();
		}			
	});
	
	/* END COLUMN SHOW - HIDE */	

})

</script>

<?php 

	include("inc/google-analytics.php"); 

}
else
{
    header("Location:index.php");
}
?>
