<?php
// William Aguirre Carpeta
// PDF HOJA DE RUTA INGEGAS
header('Content-Type: text/html; charset=UTF-8');
set_time_limit(0);
session_start();
date_default_timezone_set("America/Bogota");
require ("libraries/conexion.php");
require('libraries/fpdf/fpdf.php');
$fecha = date("Y-m-d");
$hora = date("G:i:s",time());

////////////////VARIABLES POST///////////////


	$logotipo_empresa = "img/logo-ingegas.png";
	$id_datos_prueba_pev = isset($_REQUEST['id_datos_prueba_pev']) ? $_REQUEST['id_datos_prueba_pev'] : NULL;
	$id_has_movimiento_cilindro_pev = isset($_REQUEST["id_has_movimiento_cilindro_pev"]) ? $_REQUEST["id_has_movimiento_cilindro_pev"] : NULL;
	$firmas = isset($_REQUEST['firmas']) ? $_REQUEST['firmas'] : 0;

	$nombre = "";
	$nit = "";
	$direccion = "";
	$telefono_fijo = "";
	$tabla_inspeccion = "inspeccion_alu";
	$campo_inspeccion = "estado_aluminio";
	
	$consulta1 = "SELECT * FROM has_movimiento_cilindro_pev WHERE id_has_movimiento_cilindro_pev = '$id_has_movimiento_cilindro_pev'";
	$resultado1 = mysqli_query($con,$consulta1);

	$linea1 = mysqli_fetch_assoc($resultado1);

	$id_cliente = isset($linea1["id_cliente"]) ? $linea1["id_cliente"] : NULL;
	$id_transporte_pev = isset($linea1["id_transporte_pev"]) ? $linea1["id_transporte_pev"] : NULL;
	$num_cili = isset($linea1["num_cili"]) ? $linea1["num_cili"] : NULL;
	$material_cilindro_u = isset($linea1["material_cilindro_u"]) ? $linea1["material_cilindro_u"] : NULL;

	$consulta2 = "SELECT * FROM clientes WHERE id_cliente = '$id_cliente'";
	$resultado2 = mysqli_query($con,$consulta2);

	$linea2 = mysqli_fetch_assoc($resultado2);

	$nombre = isset($linea2["nombre"]) ? $linea2["nombre"] : NULL;
	$nit = isset($linea2["nit"]) ? $linea2["nit"] : NULL;
	$direccion = isset($linea2["direccion"]) ? $linea2["direccion"] : NULL;
	$telefono_fijo = isset($linea2["telefono_fijo"]) ? $linea2["telefono_fijo"] : NULL;

	$consulta3 = "SELECT * FROM transporte_pev WHERE id_transporte_pev = $id_transporte_pev";
	$resultado3 = mysqli_query($con,$consulta3);

	$linea3 = mysqli_fetch_assoc($resultado3);

	$fecha_recepcion = isset($linea3["fecha"]) ? $linea3["fecha"] : NULL;

	$consulta4 = "SELECT * FROM cilindro_pev WHERE num_cili_pev = '$num_cili'";
	$resultado4 = mysqli_query($con,$consulta4);

	$linea4 = mysqli_fetch_assoc($resultado4);

	$id_tipo_gas_pev = isset($linea4["id_tipo_gas_pev"]) ? $linea4["id_tipo_gas_pev"] : NULL;
	$id_especi_pev = isset($linea4["id_especi_pev"]) ? $linea4["id_especi_pev"] : NULL;
	$especi_pev = isset($linea4["especi_pev"]) ? $linea4["especi_pev"] : NULL;
	$pre_tra_pev = isset($linea4["pre_tra_pev"]) ? $linea4["pre_tra_pev"] : NULL;
	$pre_pru_pev = isset($linea4["pre_pru_pev"]) ? $linea4["pre_pru_pev"] : NULL;
	$fecha_fab_pev = isset($linea4["fecha_fab_pev"]) ? $linea4["fecha_fab_pev"] : NULL;
	$fecha_ult_pev = isset($linea4["fecha_ult_pev"]) ? $linea4["fecha_ult_pev"] : NULL;
	$volumen_actu = isset($linea4["volumen_actu"]) ? $linea4["volumen_actu"] : NULL;
	$tara_esta_pev = isset($linea4["tara_esta_pev"]) ? $linea4["tara_esta_pev"] : NULL;
	$tara_actu_pev = isset($linea4["tara_actu_pev"]) ? $linea4["tara_actu_pev"] : NULL;
	$num_cili_pev_2 = isset($linea4["num_cili_pev_2"]) ? $linea4["num_cili_pev_2"] : NULL;

	$pre_tra_mpa = $pre_tra_pev/145.038;
	$pre_pru_mpa = $pre_pru_pev/145.038;

	$pre_tra_mpa = round($pre_tra_mpa, 2);
	$pre_pru_mpa = round($pre_pru_mpa, 2);

	$pre_tra_mpa = number_format($pre_tra_mpa,2);
	$pre_pru_mpa = number_format($pre_pru_mpa,2);

	$consulta5 = "SELECT * FROM tipo_gas_pev WHERE id_tipo_gas_pev = '$id_tipo_gas_pev'";
	$resultado5 = mysqli_query($con,$consulta5);

	$linea5 = mysqli_fetch_assoc($resultado5);

	$nombre_gas = isset($linea5["nombre"]) ? $linea5["nombre"] : NULL;

	if($nombre_gas=="OXIGENO INDUSTRIAL"){
		$nombre_gas = "O2                 IND";
	}else if($nombre_gas=="OXIGENO MEDICINAL"){
		$nombre_gas = "O2                 MED";
	}

	$consulta6 = "SELECT * FROM especificacion WHERE id_especificacion = $id_especi_pev";
	$resultado6 = mysqli_query($con,$consulta6);

	$linea6 = mysqli_fetch_assoc($resultado6);

	$especificacion = isset($linea6["especificacion"]) ? $linea6["especificacion"] : NULL;

	$especificacion = $especificacion."(".$especi_pev.")";

	$consulta7 = "SELECT * FROM material_cilindro WHERE id_material = $material_cilindro_u";
	$resultado7 = mysqli_query($con,$consulta7);

	$linea7 = mysqli_fetch_assoc($resultado7);

	$nombre_material = isset($linea7["nombre"]) ? $linea7["nombre"] : NULL;

	$consulta8 = "SELECT * FROM insp_vi_alu_pev WHERE id_has_movimiento_cilindro_pev = '$id_has_movimiento_cilindro_pev'";
	$resultado8 = mysqli_query($con,$consulta8);

	if(mysqli_num_rows($resultado8)>0){
		$linea8 = mysqli_fetch_assoc($resultado8);

		$consecutivo = $linea8["id_insp_vi_alu"];

		$motivo_condena = isset($linea8["motivo_condena"]) ? $linea8["motivo_condena"] : NULL;

		if($motivo_condena == "corrosion_general"){
			$numeral_ref = "5.2.1";
		}else if($motivo_condena == "picaduras_aisladas"){
			$numeral_ref = "5.2.2";
		}else if($motivo_condena == "corrosion_lineal"){
			$numeral_ref = "5.2.3";
		}else if($motivo_condena == "cort_perfo_ranura") {
			$numeral_ref = "5.3.2";
		}else if($motivo_condena == "abolladuras"){
			$numeral_ref = "5.4";
		}else if($motivo_condena == "Fugas o Poros"){
			$numeral_ref = "5.5";
		}else if($motivo_condena == "abombamiento"){
			$numeral_ref = "5.6";
		}else if($motivo_condena == "Daño por Fuego"){
			$numeral_ref = "5.7";
		}else if($motivo_condena == "grieta_cuello"){
			$numeral_ref = "5.8";
		}else if($motivo_condena == "pliegue_cuello"){
			$numeral_ref = "5.8";
		}else if($motivo_condena == "Valles en Cuello"){
			$numeral_ref = "5.8";
		}else if($motivo_condena == "Estampación"){
			$numeral_ref = "ANEXO B2. TABLA B.1";
		}else if($motivo_condena == "Marcas Sospechosas"){
			$numeral_ref = "ANEXO B2. TABLA B.1";	
		}
	
	}

	$consulta8 = "SELECT * FROM insp_vi_ace_baja_pev WHERE id_has_movimiento_cilindro_pev = '$id_has_movimiento_cilindro_pev'";
	$resultado8 = mysqli_query($con,$consulta8);
	
	if(mysqli_num_rows($resultado8)>0){
		$linea8 = mysqli_fetch_assoc($resultado8);

		$consecutivo = $linea8["id_insp_vi_ace_baja"];


		$motivo_condena = isset($linea8["motivo_condena"]) ? $linea8["motivo_condena"] : NULL;

		if($motivo_condena == "Protuberancia"){
			$numeral_ref = "NTC 5136 : ANEXO B2. TABLA B.1.";
		}else if($motivo_condena == "Abolladura"){
			$numeral_ref = "NTC 5136 : 5.2.2";
		}else if($motivo_condena == "abombamiento"){
			$numeral_ref = "NTC 5136 : 5.2.6";
		}else if($motivo_condena == "Indentacion") {
			$numeral_ref = "NTC 5136 : ANEXO B2. TABLA B.1";
		}else if($motivo_condena == "Corte o Estria"){
			$numeral_ref = "NTC 5136 : ANEXO B2. TABLA B.1 / 7.3.2(NTC 5137)";
		}else if($motivo_condena == "Grieta"){
			$numeral_ref = "NTC 5136 : ANEXO B2. TABLA B.1";
		}else if($motivo_condena == "Quemadura por Arco y Antorcha"){
			$numeral_ref = "NTC 5136 : ANEXO B2. TABLA B.1 / 7.3.4(NTC 5137)";
		}else if($motivo_condena == "Daño por Fuego"){
			$numeral_ref = "NTC 5136 : ANEXO B2. TABLA B.1 / 7.3.6(NTC 5137)";
		}else if($motivo_condena == "Insertos en Tapon o Cuello"){
			$numeral_ref = "NTC 5136 : ANEXO B2. TABLA B.1";
		}else if($motivo_condena == "Defectos del Cuello"){
			$numeral_ref = "NTC 5136 : ANEXO B2. TABLA B.1 / 7.3.7(NTC 5137)";
		}else if($motivo_condena == "Estampación"){
			$numeral_ref = "NTC 5136 : ANEXO B2. TABLA B.1";
		}else if($motivo_condena == "Marcas Sospechosas"){
			$numeral_ref = "NTC 5136 : ANEXO B2. TABLA B.1";
		}else if($motivo_condena == "Estabilidad Vertical"){
			$numeral_ref = "NTC 5136 : ANEXO B2. TABLA B.1";
		}else if($motivo_condena == "Corrosion"){
			$numeral_ref = "NTC 5136 : 5.2.1";
		}else if($motivo_condena == "Corrosion General"){
			$numeral_ref = "NTC 5136 : 5.2.1.3.1";
		}else if($motivo_condena == "Picaduras Aisladas"){
			$numeral_ref = "NTC 5136 : 5.2.1.3.2";
		}else if($motivo_condena == "Cortes Perforaciones o Ranuras"){
			$numeral_ref = "NTC 5136 : 5.2.3";
		}
	
	}

	$consulta8 = "SELECT * FROM insp_vi_ace_alta_pev WHERE id_has_movimiento_cilindro_pev = '$id_has_movimiento_cilindro_pev'";
	$resultado8 = mysqli_query($con,$consulta8);
	
	if(mysqli_num_rows($resultado8)>0){
		$linea8 = mysqli_fetch_assoc($resultado8);

		$consecutivo = $linea8["id_insp_vi_ace_alta"];

		$motivo_condena = isset($linea8["motivo_condena"]) ? $linea8["motivo_condena"] : NULL;

		
		if($motivo_condena == "Protuberancia"){
			$numeral_ref = "NTC 2699/5137 : ANEXO B2. TABLA B.1.";
		}else if($motivo_condena == "Abolladura"){
			$numeral_ref = "NTC 5137 : ANEXO B2. TABLA B.1 / 7.3.3";
		}else if($motivo_condena == "abombamiento"){
			$numeral_ref = "NTC 5137 : 7.3.5";
		}else if($motivo_condena == "Indentacion") {
			$numeral_ref = "NTC 2699/5137 : ANEXO B2. TABLA B.1";
		}else if($motivo_condena == "Corte o Estria"){
			$numeral_ref = "NTC 5137 : ANEXO B2. TABLA B.1 / 7.3.2";
		}else if($motivo_condena == "Grieta"){
			$numeral_ref = "NTC 2699/5137 : ANEXO B2. TABLA B.1";
		}else if($motivo_condena == "Quemadura por Arco y Antorcha"){
			$numeral_ref = "NTC 5137 : ANEXO B2. TABLA B.1 / 7.3.4";
		}else if($motivo_condena == "Daño por Fuego"){
			$numeral_ref = "NTC 5137 : ANEXO B2. TABLA B.1 / 7.3.6";
		}else if($motivo_condena == "Insertos en Tapon o Cuello"){
			$numeral_ref = "NTC 2699/5137 : ANEXO B2. TABLA B.1";
		}else if($motivo_condena == "Defectos del Cuello"){
			$numeral_ref = "NTC 5137 : ANEXO B2. TABLA B.1 / 7.3.7";
		}else if($motivo_condena == "Estampacion"){
			$numeral_ref = "NTC 2699/5137 : ANEXO B2. TABLA B.1";
		}else if($motivo_condena == "Marcas Sospechosas"){
			$numeral_ref = "NTC 2699/5137 : ANEXO B2. TABLA B.1";
		}else if($motivo_condena == "Estabilidad Vertical"){
			$numeral_ref = "NTC 2699/5137 : ANEXO B2. TABLA B.1";
		}else if($motivo_condena == "Corrosion General"){
			$numeral_ref = "NTC 5137 : ANEXO B3. TABLA B.2 / 7.3.1.2";
		}else if($motivo_condena == "Corrosion Local"){
			$numeral_ref = "NTC 2699/5137 : ANEXO B3. TABLA B.2";
		}else if($motivo_condena == "Corrosion Lineal"){
			$numeral_ref = "NTC 5137 : ANEXO B3. TABLA B.2 / 7.3.1.1";
		}else if($motivo_condena == "Picaduras Aisladas"){
			$numeral_ref = "NTC 5137 : ANEXO B3. TABLA B.2 / 7.3.1.3";
		}else if($motivo_condena == "Picaduras Multiples"){
			$numeral_ref = "NTC 5137 : 7.3.1.4";
		}else if($motivo_condena == "Corrosion Hendidura"){
			$numeral_ref = "NTC 2699/5137 : ANEXO B3. TABLA B.2";
		}
		
	}

	if($firmas == 1){
		$firmado = 1;
		$consulta10 = "SELECT * FROM firmasCertificados WHERE pev = 1";
		$resultado10 = mysqli_query($con, $consulta10);
		$firmas = "";
		while($linea10 = mysqli_fetch_assoc($resultado10)){
			$firma = $linea10["firma"];
			$firmas .= $firma.",";
			$rutaFirma = "Uploads/FirmasCertificados/";
		}
	}

/**
*  Clase para la maquetación de Factura
*/
class PDF extends FPDF
{
	function _Arc($x1, $y1, $x2, $y2, $x3, $y3) //Curvatura del rectángulo
	{
		$h = $this->h;
		$this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
							$x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
	}
	// private functions
	function RoundedRect($x, $y, $w, $h, $r, $style = '') //Rectángulo con bordes redondeados 
	{
		$k = $this->k;
		$hp = $this->h;
		if($style=='F')
			$op='f';
		elseif($style=='FD' || $style=='DF')
			$op='B';
		else
			$op='S';
		$MyArc = 4/3 * (sqrt(2) - 1);
		$this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));
		$xc = $x+$w-$r ;
		$yc = $y+$r;
		$this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));

		$this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);
		$xc = $x+$w-$r ;
		$yc = $y+$h-$r;
		$this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
		$this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);
		$xc = $x+$r ;
		$yc = $y+$h-$r;
		$this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
		$this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);
		$xc = $x+$r ;
		$yc = $y+$r;
		$this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
		$this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
		$this->_out($op);
	}
	// public functions
	function sizeOfText( $texte, $largeur )
	{
		$index    = 0;
		$nb_lines = 0;
		$loop     = TRUE;
		while ( $loop )
		{
			$pos = strpos($texte, "\n");
			if (!$pos)
			{
				$loop  = FALSE;
				$ligne = $texte;
			}
			else
			{
				$ligne  = substr( $texte, $index, $pos);
				$texte = substr( $texte, $pos+1 );
			}
			$length = floor( $this->GetStringWidth( $ligne ) );
			$res = 1 + floor( $length / $largeur) ;
			$nb_lines += $res;
		}
		return $nb_lines;
	}
	//ENCABEZADO DE PÁGINA
	function Header()
	{
		$x1 = 140;
		$y1 = 6;
		//Variables de encabezado
		global $logotipo_empresa, $nombre_empresa;
		global $reviso, $aprobo,$fecha_reviso,$fecha_aprobo,$codigo,$version,$pagina;

	    $this->SetFont('Arial','B',9);
		$this->SetTextColor(0,0,0);
		$this->SetLineWidth(0);	
		//.....Logo de la compañia		
		$this->Image($logotipo_empresa,30,19,14,22);
		$this->SetXY(60,18);
		
				
		
		$this->SetXY(108,18);
		$this->Cell(100,8, 'INGENIERIA Y GASES LTDA "INGEGAS"', 0,0,'C');
		$this->SetXY(135,26);
		$this->Cell(45,8, 'Calle 163 # 19 A 32 PBX: (1) 6706126 FAX: (1) 6772951', 0,1,'C');
		$this->SetXY(23,40);
		$this->Cell(30,5,'INGEGAS LTDA',0,0,'C');
		$this->SetFont('Arial','B',12);
		$this->SetXY(138,36);
		$this->Cell(40,8,'INFORME DE INSPECCION Y PRUEBA DE CILINDROS',0,0,'C');
		$this->SetFont('Arial', 'B', 7);
		$this->SetXY(138,39);
		$this->Cell(40,8,'IPC-PL-03 (V8)',0,0,'C');
		$this->SetXY(138,41);
		$this->Cell(40,8,utf8_decode('REVISÓ: G.G. 04/12/2014 - APROBÓ: 04/12/2014'),0,0,'C');
		
		
	}
	//PIE DE PÁGINA
	function Footer()
	{
		global $firmado;
		if($firmado == 1){
			global $firmas, $rutaFirma;
			$firmas2 = $firmas;
			$firmas = explode(",", $firmas);
			$firma1 = $firmas[0];
			$firma2 = $firmas[1];

			$firma1 = $rutaFirma.$firma1;
			$firma2 = $rutaFirma.$firma2;
			$this->Image($firma2,230,160,20,25);
			$this->Image($firma1,65,160,25,25);
		}
		$this->SetFont('Arial','B',8);
		$this->SetXY(40,175);
		$this->Cell(16,5,'FIRMADO:',0,1);
		$this->SetXY(56,174);
		$this->Cell(40,5,'','B',1);
		$this->SetXY(56,179);
		$this->Cell(40,5,'DIRECTO TECNICO',0,1,'C');
		$this->SetXY(56,182);
		$this->Cell(40,5,'ING. NELSON FORERO',0,1,'C');

		$this->SetXY(205,175);
		$this->Cell(16,5,'FIRMADO',0,1);
		$this->SetXY(221,174);
		$this->Cell(40,5,'','B',1);
		$this->SetXY(221,179);
		$this->Cell(40,5,'OPERARIO INSP. Y ENSAYO',0,1,'C');
		$this->SetXY(221,182);
		$this->Cell(40,5,'JORGE IVAN AYA',0,1,'C');

		$this->SetXY(17,190);
		$this->Cell(260,5,'',0,1);
		$this->SetXY(17,189);
		$this->Cell(11,5,'NOTA:',0,1);
		$this->SetFont('Arial','',7);
		$this->SetXY(28,190);
		$this->MultiCell(244,3,utf8_decode('Este informe de resultados solo puede ser reproducido parcial o totalmente con la autorización expresa de INGEGAS LTDA. La información del presente informe y los valores respectivos obedecen a actividades realizadas en el momento y en las condiciones en las que se realizó el ensayo y la inspección. INGEGAS LTDA. no se responsabiliza por los perjuicios ocasinados por el uso incorrecto o no autorizado del cilindro ensayado e inspeccionado o de sus componentes.'),0,1);
		$this->SetXY(28,200);
		$this->Cell(244,3,utf8_decode('El valor de incertidumbre fue estimado con un factor de cobertura K=2; que garantiza un nivel de confiabilidad de 95%'),0,1);

	}
}
$pdf = new PDF( 'L', 'mm', 'A4');
$pdf->SetAutoPageBreak(TRUE,40);
$pdf->AliasNbPages();
$pdf->AddPage();

//CONTENIDO DEL CUERPO DE LA HOJA DE RUTA
		
		
		$pdf->SetXY(60,53);
		$pdf->Cell(30,5,"Normas:",0,1);
		$pdf->SetXY(60,61);
		$pdf->Cell(30,5,"Metodo de Ensayo:",0,1);
		
		if($nombre_material == "Acero"){
			$pdf->SetXY(90,53);
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(30,5,'NTC 2699/5137',0,1);
		}else if($nombre_material == "Aluminio"){
			$pdf->SetXY(90,53);
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(30,5,'5136:2009 (Num. 4,5,6)',0,1);
		}else{
			$pdf->SetXY(90,53);
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(30,5,'NTC 5171:2013 (Num. 5)/5136:2009 (Num. 4,5,6)',0,1);
		}
		$pdf->SetXY(90,61);
		$pdf->Cell(30,5,utf8_decode('INSPECCIÓN VISUAL INTERNA Y EXTERNA Y PRUEBA DE EXPANSIÓN VOLUMÉTRICA EN CAMISA DE AGUA'),0,1);

		$pdf->SetFont('Arial','B',8);
		$pdf->SetXY(25,70);
		$pdf->Cell(30,5,'FECHA INFORME:',0,1);
		$pdf->SetXY(55,70);
		$pdf->Cell(40,5,$fecha,0,1);
		$pdf->SetXY(73,70);
		$pdf->Cell(167,5,'',0,1);
		$pdf->SetXY(240,70);
		$pdf->Cell(40,5,'No. LPH '.$consecutivo,0,1);
		$pdf->SetXY(240,75);
		$pdf->Cell(40,5,'PAGINA 1 DE 1',0,1);

		$pdf->SetXY(25,78);
		$pdf->Cell(30,5,'CLIENTE:',0,1);
		$pdf->SetXY(25,83);
		$pdf->Cell(30,5,'NIT:',0,1);
		$pdf->SetXY(25,88);
		$pdf->Cell(30,5,'DIRECCION:',0,1);
		$pdf->SetXY(125,88);
		$pdf->Cell(30,5,'TELEFONO:',0,1);

		$pdf->SetFont('Arial','',8);
		$pdf->SetXY(55,78);
		$pdf->Cell(30,5,$nombre,0,1);
		$pdf->SetXY(55,83);
		$pdf->Cell(30,5,$nit,0,1);
		$pdf->SetXY(55,88);
		$pdf->Cell(30,5,$direccion,0,1);
		$pdf->SetXY(155,88);
		$pdf->Cell(30,5,$telefono_fijo,0,1);

		$pdf->SetFont('Arial','B',6);
		$pdf->setFillColor(250,231,115);
		$pdf->SetXY(15,100);
		$pdf->Cell(14,15,'',1,0,'C',1);
		$pdf->SetXY(15,100);
		$pdf->Cell(14,5,'FECHA',0,0,1);
		$pdf->SetXY(15,105);
		$pdf->Cell(14,5,'RECEPCION',0,0,1);
		$pdf->SetXY(15,110);
		$pdf->Cell(14,5,'aa/mm/dd',0,0,1);
		$pdf->SetXY(15,115);
		$pdf->Cell(14,5,$fecha_recepcion,1,1,'C');

		$pdf->SetXY(29,100);
		$pdf->Cell(13,15,'',1,0,'C',1);
		$pdf->SetXY(29,100);
		$pdf->Cell(13,5,'FECHA',0,0);
		$pdf->SetXY(29,105);
		$pdf->Cell(13,5,'PRUEBA',0,0);
		$pdf->SetXY(29,110);
		$pdf->Cell(13,5,'aa/mm/dd',0,0);
		$pdf->SetXY(29,115);
		$pdf->Cell(13,5,'No Aplica',1,1);

		$pdf->SetXY(42,100);
		$pdf->Cell(11.5,15,'',1,1,'C',1);
		$pdf->SetXY(42,103);
		$pdf->Cell(11.5,5,'CILINDRO',0,1);
		$pdf->SetXY(42,107);
		$pdf->Cell(11.5,5,utf8_decode('NÚMERO'),0,1);
		$pdf->SetXY(42,115);
		if(strlen($num_cili_pev_2)>8){
			$pdf->SetFont('Arial','B',5.5);
			$pdf->SetXY(42,115);
			$pdf->Cell(11.5,5,'',1,1);
			$pdf->SetXY(41,115);
			$pdf->Cell(11.5,5,$num_cili_pev_2,0,1);
		}else{
			$pdf->Cell(11.5,5,$num_cili_pev_2,1,1);
		}

		$pdf->SetXY(53.5,100);
		$pdf->Cell(12.5,15,'',1,1,'C',1);
		$pdf->SetXY(53.5,105);
		$pdf->Cell(12.5,5,'GAS',0,1,'C');
		$pdf->SetFont('Arial','B',7);		
		$pdf->SetXY(53.5,115);
		$pdf->setFillColor(255,255,255);
		$pdf->Cell(12.5,5,'',1,1);
		$pdf->SetXY(53.5,115);
		if(strlen($nombre_gas)>6){
			$pdf->SetFont('Arial','B',5);
			$pdf->MultiCell(12.5,5,$nombre_gas,1,1);
			$pdf->SetFont('Arial','B',7);
		}else{
			$pdf->MultiCell(12.5,5,$nombre_gas,1,1);
		}

		$pdf->setFillColor(250,231,115);

		$pdf->SetFont('Arial','B',6);
		$pdf->SetXY(66,100);
		$pdf->Cell(18,15,'',1,1,'C',1);
		$pdf->SetXY(66,105);
		$pdf->Cell(18,5,utf8_decode('ESPECIFICACIÓN'),0,1,'C');
		$pdf->SetXY(66,115);
		$pdf->Cell(18,5,$especificacion,1,1,'C');

		
		$pdf->SetXY(84,100);
		$pdf->Cell(52,5,utf8_decode('PRESIÓN'),1,1,'C',1);
		$pdf->SetXY(84,105);
		$pdf->Cell(13,5,'Trabajo',1,1,'C',1);
		$pdf->SetXY(97,105);
		$pdf->Cell(13,5,'Prueba',1,1,'C',1);
		$pdf->SetXY(110,105);
		$pdf->Cell(13,5,'Trabajo',1,1,'C',1);
		$pdf->SetXY(123,105);
		$pdf->Cell(13,5,'Prueba',1,1,'C',1);
		$pdf->SetXY(84,110);
		$pdf->Cell(13,5,'Mpa',1,1,'C',1);
		$pdf->SetXY(97,110);
		$pdf->Cell(13,5,'Mpa',1,1,'C',1);
		$pdf->SetXY(110,110);
		$pdf->Cell(13,5,'Psi',1,1,'C',1);
		$pdf->SetXY(123,110);
		$pdf->Cell(13,5,'Psi',1,1,'C',1);
		$pdf->SetXY(84,115);
		$pdf->Cell(13,5,$pre_tra_mpa,1,1,'C');
		$pdf->SetXY(97,115);
		$pdf->Cell(13,5,$pre_pru_mpa,1,1,'C');
		$pdf->SetXY(110,115);
		$pdf->Cell(13,5,$pre_tra_pev,1,1,'C');
		$pdf->SetXY(123,115);
		$pdf->Cell(13,5,$pre_pru_pev,1,1,'C');


		$pdf->SetXY(136,100);
		$pdf->Cell(26,5,'FECHAS',1,1,'C',1);
		$pdf->SetXY(136,105);
		$pdf->Cell(13,5,utf8_decode('F/cación'),1,1,'C',1);
		$pdf->SetXY(149,105);
		$pdf->Cell(13,5,'Ult. Rev.',1,1,'C',1);
		$pdf->SetXY(136,110);
		$pdf->Cell(13,5,'aa/mm/dd',1,1,'C',1);
		$pdf->SetXY(149,110);
		$pdf->Cell(13,5,'aa/mm/dd',1,1,'C',1);
		$pdf->SetXY(136,115);
		$pdf->Cell(13,5,$fecha_fab_pev,1,1,'C');
		$pdf->SetXY(149,115);
		$pdf->Cell(13,5,$fecha_ult_pev,1,1,'C');



		$pdf->SetXY(162,100);
		$pdf->Cell(13,5,'VOL',1,1,'C',1);
		$pdf->SetXY(162,105);
		$pdf->Cell(13,5,'Litros',1,1,'C',1);
		$pdf->SetXY(162,110);
		$pdf->Cell(13,5,'Lt',1,1,'C',1);
		$pdf->SetXY(162,115);
		$pdf->Cell(13,5,$volumen_actu,1,1,'C');

		$pdf->SetXY(175,100);
		$pdf->Cell(26,5,'TARA',1,1,'C',1);
		$pdf->SetXY(175,105);
		$pdf->Cell(13,5,utf8_decode('F/cación'),1,1,'C',1);
		$pdf->SetXY(188,105);
		$pdf->Cell(13,5,'Actual',1,1,'C',1);
		$pdf->SetXY(175,110);
		$pdf->Cell(13,5,'Kg',1,1,'C',1);
		$pdf->SetXY(188,110);
		$pdf->Cell(13,5,'Kg',1,1,'C',1);
		$pdf->SetXY(175,115);
		$pdf->Cell(13,5,$tara_esta_pev,1,1,'C');
		$pdf->SetXY(188,115);
		$pdf->Cell(13,5,$tara_actu_pev,1,1,'C');

		$pdf->SetXY(201,100);
		$pdf->Cell(14,5,utf8_decode('INSPECCIÓN'),'T , L',0,'C',1);
		$pdf->SetXY(201,105);
		$pdf->Cell(14,5,'VISUAL','L',0,'C',1);
		$pdf->SetXY(201,110);
		$pdf->Cell(14,5,'RESULTADO','L',0,'C',1);
		$pdf->SetFont('Arial','B',5);
		$pdf->SetXY(201,115);
		$pdf->Cell(14,5,"NO APROBADO",1,1,'C');
		

		$pdf->SetFont('Arial','B',6);
		$pdf->SetXY(215,100);
		$pdf->Cell(39,5,utf8_decode('EXPANS VOLUMÉTRICA'),1,1,'C',1);
		$pdf->SetXY(215,105);
		$pdf->Cell(12,5,'Total',1,1,'C',1);
		$pdf->SetXY(227,105);
		$pdf->Cell(26,5,'Perman.',1,1,'C',1);
		$pdf->SetXY(215,110);
		$pdf->Cell(12,5,'gr',1,1,'C',1);
		$pdf->SetXY(227,110);
		$pdf->Cell(13,5,'gr',1,1,'C',1);
		$pdf->SetXY(240,110);
		$pdf->Cell(13,5,'%',1,1,'C',1);
		$pdf->SetXY(215,115);
		$pdf->SetFont('Arial','B',5);
		$pdf->Cell(12,5,'NO APLICA',1,1,'C');
		$pdf->SetXY(227,115);
		$pdf->Cell(13,5,'NO APLICA',1,1,'C');
		$pdf->SetXY(240,115);
		$pdf->Cell(13,5,'NO APLICA',1,1,'C');

		$pdf->SetXY(253,100);
		$pdf->Cell(13,15,'',1,1,'C',1);
		$pdf->SetXY(253,102);
		$pdf->Cell(13,5,'MATERIAL',0,0,'C');
		$pdf->SetXY(253,105);
		$pdf->Cell(13,5,'DEL',0,0,'C');
		$pdf->SetXY(253,108);
		$pdf->Cell(13,5,'CILINDRO',0,0,'C');
		$pdf->SetXY(253,115);
		$pdf->Cell(13,5,$nombre_material,1,1,'C');
		

		$pdf->SetFont('Arial','B',6);
		$pdf->SetXY(266,100);
		$pdf->Cell(14,15,'RESULTADO',1,1,'C',1);
		$pdf->SetXY(266,115);
		$pdf->Cell(14,5,'CONDENADO',1,1,'C');

		$pdf->SetFont('Arial','B',8);
		$pdf->SetXY(17,125);
		$pdf->Cell(62,5,utf8_decode('Valor de incertidumbre máx expandida de  ± :'),0,1);
		$pdf->SetXY(80,126);
		$pdf->Cell(22,3,'NO APLICA','B',1);
		$pdf->SetXY(102,126);
		$pdf->Cell(5,3,'%',0,1);

		$pdf->SetXY(17,130);
		$pdf->Cell(29,5,utf8_decode('TOTAL CILINDROS :'),0,1);
		$pdf->SetXY(47,131);
		$pdf->Cell(4,3,'1','B',1);

		$pdf->SetXY(17,135);
		$pdf->Cell(8,5,utf8_decode('Obs:'),0,1);
		$pdf->SetFont('Arial','',7);
		$pdf->SetXY(25,135);
		$pdf->Cell(255,5,utf8_decode('La expansión volumétrica permanente respecto a la expansión total debe ser menor al 10% para ser aprobado'),0,1);

		$pdf->SetFont('Arial','B',8);
		$pdf->SetXY(17,142);
		$pdf->Cell(36,5,utf8_decode('CAUSA(S) DE CONDENA:'),0,1);
		$pdf->SetFont('Arial','',7);
		$pdf->SetXY(53,142);
		$pdf->Cell(35,4,utf8_decode($motivo_condena),'B',1,'C');

		$pdf->SetFont('Arial','B',8);
		$pdf->SetXY(17,147);
		$pdf->Cell(55,5,utf8_decode('NORMA Y NUMERAL DE REFERENCIA:'),0,1);
		$pdf->SetFont('Arial','',7);
		$pdf->SetXY(72,147);
		$pdf->Cell(35,4,$numeral_ref,'B',1,'C');

		$pdf->SetXY(201,130);
		$pdf->Cell(80,5,'CONVENCIONES',1,1,'C');
		$pdf->SetXY(201,135);
		$pdf->Cell(80,8,'',1,1,'C');
		$pdf->SetFont('Arial','B',7.5);
		$pdf->SetXY(201,135);
		$pdf->Cell(15,4,'Aprobado:',0,1);
		$pdf->SetFont('Arial','',7.2);
		$pdf->SetXY(214.5,135);
		$pdf->MultiCell(66.5,4,utf8_decode('el cilindro cumple con los parámetros de evaluación. (Inspección visual y/o prueba de expansión volumétrica).'),0,1);
		$pdf->SetXY(201,143);
		$pdf->Cell(80,8,'',1,1,'C');
		$pdf->SetFont('Arial','B',7.5);
		$pdf->SetXY(201,143);
		$pdf->Cell(15,4,'Condenado:',0,1);
		$pdf->SetFont('Arial','',7.1);
		$pdf->SetXY(216.5,143);
		$pdf->MultiCell(64.5,4,utf8_decode('el cilindro no cumple con los parámetros de evaluación. (visual y/o prueba de expansión volumétrica)'),0,1);
		$pdf->SetXY(201,151);
		$pdf->Cell(80,12,'',1,1,'C');
		$pdf->SetFont('Arial','B',7.5);
		$pdf->SetXY(201,151);
		$pdf->Cell(19,4,'incertidumbre:',0,1);
		$pdf->SetFont('Arial','',6.7);
		$pdf->SetXY(220,151);
		$pdf->MultiCell(61,4,utf8_decode('parámetro asociado con el resultado de una medición, que caracteriza la dispersión de los valores que pudieran ser razonablemente atribuidos al mensurando.'),0,1);

		

		

//Datos de archivo de descarga
$file_name = str_replace(' ','_',$nombre_empresa_ajustado)."_Factura_No._".$num_factura.".pdf";
$file_destination = $path_pdf.$file_name;
$pdf->Output($file_destination,'F');
$pdf->Output($file_name,'I');
?>