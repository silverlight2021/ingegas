<?php
session_start();
date_default_timezone_set("America/Bogota");
require ("libraries/conexion.php");


if($_SESSION['logged']== 'yes')
{ 
  require_once("inc/init.php");
  require_once("inc/config.ui.php");
  $page_title = "Lote ETO";
  $page_css[] = "your_style.css";
  include("inc/header.php");
  $page_nav[""][""][""][""] = true;
  include("inc/nav.php");

  $id_lote_eto = isset($_REQUEST['id_lote_eto']) ? $_REQUEST['id_lote_eto'] : NULL;
  $User_idUser =$_SESSION['su'];
  $acc = $_SESSION['acc'];


  if(isset($_POST['g_cilindro_eto']))//Botón de creación y actualización del cilindro
  {
    if(strlen($id_lote_eto) > 0)//Se actualiza cilindro existente con datos nuevos
    {
      $num_cilindro_co2 = $_POST['num_cilindro_co2'] ;
      $fecha_llegada = $_POST['fecha_llegada'] ;
      $id_estado_eto = $_POST['id_estado_eto'] ; 
      $pureza = $_POST['pureza'] ; 
      $humedad = $_POST['humedad'] ; 
      $capacidad = $_POST['capacidad'] ; 
      $alerta = $_POST['alerta'] ; 
      $fecha_inicio = $_POST['fecha_inicio']; 

      $consulta= "UPDATE lote_eto SET 
            num_cilindro_eto = '".$num_cilindro_co2."',
            fecha_llegada = '".$fecha_llegada."',
            id_estado_eto = '".$id_estado_eto."',
            pureza = '".$pureza."',
            humedad = '".$humedad."',
            capacidad = '".$capacidad."', alerta = '".$alerta."',
            fecha_inicio = '".$fecha_inicio."',
            uso_actual = '".$capacidad."' 
            WHERE id_lote_eto = '".$id_lote_eto."'";
      $resultado = mysqli_query($con,$consulta) ;

      if ($resultado == FALSE)
      {
        echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
      }
      else
      {
        ?>
        <script type="text/javascript">
          alert("Datos actualizados correctamente.");
          window.location = "busqueda_eto.php";
        </script>
        <?php
      }
    }
    else
    {
      $num_cilindro_co2 = $_POST['num_cilindro_co2'] ;
      $fecha_llegada = $_POST['fecha_llegada'] ;
      $id_estado_eto = $_POST['id_estado_eto'] ; 
      $pureza = $_POST['pureza'] ; 
      $humedad = $_POST['humedad'] ; 
      $capacidad = $_POST['capacidad'] ; 
      $alerta = $_POST['alerta'] ; 
      $fecha_inicio = $_POST['fecha_inicio']; 
      
      $consulta = "INSERT INTO lote_eto
                   (num_cilindro_eto,fecha_llegada, id_estado_eto,pureza,humedad,capacidad,alerta,fecha_inicio, uso_actual) 
                   VALUES ('".$num_cilindro_co2."','".$fecha_llegada."', 
                           '".$id_estado_eto."', '".$pureza."', '".$humedad."', 
                          '".$capacidad."', '".$alerta."', '".$fecha_inicio."', '".$capacidad."')";
      $resultado = mysqli_query($con,$consulta) ;
      if ($resultado == FALSE)
      {
        echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
      }
      else
      {
        $id_cilindro = mysqli_insert_id($con);
        ?>
        <script type="text/javascript">
          alert("Cilindro creado con exito.");
          window.location = "busqueda_eto.php";
        </script>
        <?php
      }
    }
  }
  if(strlen($id_lote_eto) > 0)//Se obtienen datos a mostrar en formulario del cilindro seleccionado
  {
    $consulta  = "SELECT * FROM lote_eto 
                  WHERE id_lote_eto= '".$id_lote_eto."'";
    $resultado = mysqli_query($con,$consulta);
    if(mysqli_num_rows($resultado) > 0)
    {
      $linea = mysqli_fetch_assoc($resultado);
      $num_cilindro_co2 = isset($linea["num_cilindro_eto"]) ? $linea["num_cilindro_eto"] : NULL;  
      $fecha_llegada = isset($linea["fecha_llegada"]) ? $linea["fecha_llegada"] : NULL;
      $id_estado_eto = isset($linea["id_estado_eto"]) ? $linea["id_estado_eto"] : NULL;
      $pureza = isset($linea["pureza"]) ? $linea["pureza"] : NULL;
      $humedad = isset($linea["humedad"]) ? $linea["humedad"] : NULL;
      $capacidad = isset($linea["capacidad"]) ? $linea["capacidad"] : NULL;
      $alerta = isset($linea["alerta"]) ? $linea["alerta"] : NULL;
      $fecha_inicio = isset($linea["fecha_inicio"]) ? $linea["fecha_inicio"] : NULL;
    }mysqli_free_result($resultado);

    $consulta1 = "SELECT SUM(real_eto) AS suma_consumo 
                  FROM produccion_mezclas WHERE id_lote_eto = '".$id_lote_eto."' AND fech_crea >= '".$fecha_inicio."'";
    $resultado1 = mysqli_query($con,$consulta1);
    if(mysqli_num_rows($resultado1) > 0)
    {
      $linea1 = mysqli_fetch_assoc($resultado1);
      $suma_consumo = $linea1['suma_consumo'];
      $uso_actual = $capacidad - $suma_consumo;
    }mysqli_free_result($resultado1);
  }
  
?>

<div id="main" role="main">	
	<div id="content">
		<section id="widget-grid" class="">
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-6">
					<div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h2>Cilindro </h2>
						</header>
						<div>
							<div class="jarviswidget-editbox"></div>							
							<div class="widget-body no-padding">
								<form id="checkout-form" class="smart-form" novalidate="novalidate" action="lote_eto.php" method="POST">
								<input type="hidden" name="id_lote_eto" id="id_lote_eto" value="<?php echo $id_lote_eto; ?>">
									<fieldset>
										<div class="row">											
											<section class="col col-4">
												<label class="label">Num Cilindro :</label>
												<label class="input"> 
													<input type="text" name="num_cilindro_co2" placeholder="Num Cilindro Co2"  value="<?php echo isset($num_cilindro_co2) ? $num_cilindro_co2 : NULL; ?>">
												</label>
											</section>
											<section class="col col-4">
												<label class="label">Fecha llegada :</label>
												<label class="input"> 
													<input type="date" name="fecha_llegada" placeholder="Fecha llegada"  value="<?php echo isset($fecha_llegada) ? $fecha_llegada : NULL; ?>">
												</label>
											</section>
											<section class="col col-4">	
												<?php
												$consulta6 ="SELECT * FROM estado_lote_co2 ORDER BY estado ASC";
												$resultado6 = mysqli_query($con,$consulta6) ;
												echo "<section>";
												echo "<label class='label'>Estado</label>";
												echo"<label class='select'>";
												echo "<select name='id_estado_eto' ¿>";
												echo "<option value='0'>Seleccione...</option>";
												while($linea6 = mysqli_fetch_array($resultado6))
												{
													$id_estado_co2 = $linea6['id_estado_co2'];
													$estado = $linea6['estado'];
													if ($id_estado_co2==$id_estado_eto)
													{
															echo "<option value='$id_estado_co2' selected >$estado</option>"; 
													}
													else 
													{
															echo "<option value='$id_estado_co2'>$estado</option>"; 
													} 
												}//fin while 
												echo "</select>";
												echo "<i></i>";
												echo "</label>";
												echo "</section>";
															
												?>
											</section>										
										</div>
                    <div class="row">                     
                      <section class="col col-6">
                        <label class="label">Pureza (%) :</label>
                        <label class="input"> 
                          <input type="text" name="pureza" placeholder="Pureza" onkeypress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"  value="<?php echo isset($pureza) ? $pureza : NULL; ?>">
                        </label>
                      </section>
                      <section class="col col-6">
                        <label class="label">Humedad (ppm):</label>
                        <label class="input"> 
                          <input type="text" name="humedad" placeholder="Humedad"onkeypress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"  value="<?php echo isset($humedad) ? $humedad : NULL; ?>">
                        </label>
                      </section>                                   
                    </div>
                    <div class="row">                     
                      <section class="col col-6">
                        <label class="label">Capacidad Inicial :</label>
                        <label class="input"> 
                          <input type="text" name="capacidad" id="capacidad" placeholder="Capacidad Inicial" onkeypress="if(event.keyCode < 45 || event.keyCode > 57) evenXt.returnValue = false;"  value="<?php echo isset($capacidad) ? $capacidad : NULL; ?>">
                        </label>
                      </section>
                      <section class="col col-6">
                        <label class="label">Alerta :</label>
                        <label class="input"> 
                          <input type="text" name="alerta" id="alerta" placeholder="alerta"onkeypress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"  value="<?php echo isset($alerta) ? $alerta : NULL; ?>">
                        </label>
                      </section>                                   
                    </div>
                    <div class="row">                     
                      <section class="col col-6">
                        <label class="label">Disponible :</label>
                        <label class="input"> 
                          <input type="text" name="uso_actual" id="uso_actual" readonly placeholder="Disponible" onkeypress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"  value="<?php echo isset($uso_actual) ? $uso_actual : NULL; ?>">
                        </label>
                      </section> 
                      <section class="col col-6">
                        <label class="label">Fecha inicio :</label>
                        <label class="input"> 
                          <input type="date" name="fecha_inicio" placeholder="Fecha inicio"  value="<?php echo isset($fecha_inicio) ? $fecha_inicio : NULL; ?>">
                        </label>
                      </section>                             
                    </div>
									</fieldset>	
									    <footer>
                      <?php 
                        if($id_lote_eto > 0)
                        {
                          ?>
                          <input type="submit" value="ACTUALIZAR" name="g_cilindro_eto" id="g_cilindro_eto" class="btn btn-primary" />
                          <?php
                        }
                        else
                        {
                          ?>
                          <input type="submit" value="CREAR" name="g_cilindro_eto" id="g_cilindro_eto" class="btn btn-primary" />
                          <?php
                        }
                        ?>
	                  	</footer>
         					</form>
    					</div>  
     				</div>
     			</div>
     		</article>
    	</div>
    </section>
  </div>
  <!-- END MAIN CONTENT -->
</div>

<?php
  include("inc/footer.php");
  include("inc/scripts.php"); 
?>

<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>

<!-- Vector Maps Plugin: Vectormap engine, Vectormap language -->
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>

<!-- Full Calendar -->
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/jquery-form/jquery-form.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() 
  {
    var uso = document.getElementById("uso_actual").value;
    var alerta = document.getElementById("alerta").value;
    //var capacidad = document.getElementById("capacidad").value;
    
    
    if(parseInt(uso)<=parseInt(alerta)) 
    {
      $.bigBox({
        title : "Alerta Insumo ETO",
        content : "El contenido del Cilindro se está agotando. Por favor tenga en cuenta el restante para las siguientes órdenes de producción.",
        color : "#C46A69",
        //timeout: 6000,
        icon : "fa fa-warning shake animated",
        number : "1",
        timeout : 60000
      });
    }
    else
    {
      $.bigBox({
        title : "Alerta Insumo ETO",
        content : "Contenido disponible.",
        color : "#28B463",
        //timeout: 6000,
        icon : "fa fa-check-square-o shake animated",
        number : "1",
        timeout : 60000
      });
    }
  });
</script>

<script type="text/javascript">
  $(document).ready(function() 
  {

    var $checkoutForm = $('#checkout-form').validate(
    {
    // Rules for form validation
      rules :
      {
        name1 : {
          required : true
        },
        idgender1 : {
          required : true
        },
        last_name : {
          required : true
        },
        home_phone : {
          required : true
        },
        primary_mail : {
          required : true
        },
        street_adress1 : {
          required : true
        }
      },

      // Messages for form validation
      messages : {
        name1 : {
          required : 'Please enter your first name'
        },
        idgender1 : {
          required : ''
        },
        last_name : {
          required : 'Please enter your last name'
        },
        home_phone : {
          required : 'Please enter your phone'
        },
        primary_mail : {
          required : 'Please enter your mail'
        },
        street_adress1 : {
          required : 'Please enter your Adress'
        }
      },

      // Do not change code below
      errorPlacement : function(error, element) {
        error.insertAfter(element.parent());
      }
    }); 

  }) 
</script>


<?php 
  //include footer
  include("inc/google-analytics.php"); 
?>
<?php

}
else
{
    ?>
    <script type="text/javascript">
      window.location = "index.php";
    </script>
    <?php
}
?>
					                       
										