<?php
// William Aguirre Carpeta
// PDF HOJA DE RUTA INGEGAS
header('Content-Type: text/html; charset=UTF-8');
set_time_limit(0);
session_start();
date_default_timezone_set("America/Bogota");
require ("libraries/conexion.php");
require('libraries/fpdf/fpdf.php');
$fecha = date("Y-m-d");
$hora = date("G:i:s",time());

////////////////VARIABLES POST///////////////


	$logotipo_empresa = "img/logo-ingegas.png";
	$id_verificacion_mensual = isset($_REQUEST['id_verificacion_mensual']) ? $_REQUEST['id_verificacion_mensual'] : NULL;
	
	$consulta1 = "SELECT * FROM verificacion_mensual WHERE id_verificacion_mensual = $id_verificacion_mensual";
	$resultado1 = mysqli_query($con,$consulta1);

	$linea1 = mysqli_fetch_array($resultado1);

	$fecha_verificacion = isset($linea1["fecha_verificacion"]) ? $linea1["fecha_verificacion"] : NULL;
	$ciudad = isset($linea1["ciudad"]) ? $linea1["ciudad"] : NULL;
	$id_usuario = isset($linea1["id_usuario"]) ? $linea1["id_usuario"] : NULL;
	$identificacion = isset($linea1["identificacion"]) ? $linea1["identificacion"] : NULL;
	$cargo = isset($linea1["cargo"]) ? $linea1["cargo"] : NULL;
	$area = isset($linea1["area"]) ? $linea1["area"] : NULL;
	$id_vehiculo = isset($linea1["id_vehiculo"]) ? $linea1["id_vehiculo"] : NULL;
	$kilometraje = isset($linea1["kilometraje"]) ? $linea1["kilometraje"] : NULL;

	$consulta2 = "SELECT * FROM user WHERE idUser = '$id_usuario'";
	$resultado2 = mysqli_query($con,$consulta2);

	$linea2 = mysqli_fetch_array($resultado2);

	$Name = isset($linea2["Name"]) ? $linea2["Name"] : NULL;
	$LastName = isset($linea2["LastName"]) ? $linea2["LastName"] : NULL;

	$nombre_usuario = $Name." ".$LastName;

	$consulta3 = "SELECT * FROM placa_camion WHERE id_placa_camion = '$id_vehiculo'";
	$resultado3 = mysqli_query($con,$consulta3);

	$linea3 = mysqli_fetch_array($resultado3);

	$placa_camion = isset($linea3["placa_camion"]) ? $linea3["placa_camion"] : NULL;


	$consulta9 = "SELECT * FROM verificacion_vehiculo WHERE id_verificacion_mensual = $id_verificacion_mensual";
			$resultado9 = mysqli_query($con,$consulta9);

			$linea9 = mysqli_fetch_array($resultado9);

			$licencia = isset($linea9["licencia"]) ? $linea9["licencia"] : NULL;
			$soat = isset($linea9["soat"]) ? $linea9["soat"] : NULL;
			$rtm = isset($linea9["rtm"]) ? $linea9["rtm"] : NULL;
			$seguro_dano_rc = isset($linea9["seguro_dano_rc"]) ? $linea9["seguro_dano_rc"] : NULL;
			$manual_seguridad = isset($linea9["manual_seguridad"]) ? $linea9["manual_seguridad"] : NULL;
			$tarjetas_emergencia = isset($linea9["tarjetas_emergencia"]) ? $linea9["tarjetas_emergencia"] : NULL;
			$telefonos_emergencia = isset($linea9["telefonos_emergencia"]) ? $linea9["telefonos_emergencia"] : NULL;
			$limpiabrisas = isset($linea9["limpiabrisas"]) ? $linea9["limpiabrisas"] : NULL;
			$llantas_delanteras = isset($linea9["llantas_delanteras"]) ? $linea9["llantas_delanteras"] : NULL;
			$llantas_traseras = isset($linea9["llantas_traseras"]) ? $linea9["llantas_traseras"] : NULL;
			$llantas_repuesto = isset($linea9["llantas_repuesto"]) ? $linea9["llantas_repuesto"] : NULL;
			$plataforma = isset($linea9["plataforma"]) ? $linea9["plataforma"] : NULL;
			$frenos = isset($linea9["frenos"]) ? $linea9["frenos"] : NULL;
			$aceite = isset($linea9["aceite"]) ? $linea9["aceite"] : NULL;
			$refrigerante = isset($linea9["refrigerante"]) ? $linea9["refrigerante"] : NULL;
			$cinturon_seguridad = isset($linea9["cinturon_seguridad"]) ? $linea9["cinturon_seguridad"] : NULL;
			$fecha_tecnicomecanica = isset($linea9["fecha_tecnicomecanica"]) ? $linea9["fecha_tecnicomecanica"] : NULL;
			$fecha_soat = isset($linea9["fecha_soat"]) ? $linea9["fecha_soat"] : NULL;
			$carro_portatermo = isset($linea9["carro_portatermos"]) ? $linea9["carro_portatermos"] : NULL;
			$riatas = isset($linea9["riatas"]) ? $linea9["riatas"] : NULL;
			$direccionales_delanteras = isset($linea9["direccionales_delanteras"]) ? $linea9["direccionales_delanteras"] : NULL;
			$direccionales_traseras = isset($linea9["direccionales_traseras"]) ? $linea9["direccionales_traseras"] : NULL;
			$luces_altas = isset($linea9["luces_altas"]) ? $linea9["luces_altas"] : NULL;
			$luces_bajas = isset($linea9["luces_bajas"]) ? $linea9["luces_bajas"] : NULL;
			$luces_stops = isset($linea9["luces_stops"]) ? $linea9["luces_stops"] : NULL;
			$luces_reversa = isset($linea9["luces_reversa"]) ? $linea9["luces_reversa"] : NULL;
			$luces_parqueo = isset($linea9["luces_parqueo"]) ? $linea9["luces_parqueo"] : NULL;
			$frenos_principal = isset($linea9["frenos_principal"]) ? $linea9["frenos_principal"] : NULL;
			$frenos_emergencia = isset($linea9["frenos_emergencia"]) ? $linea9["frenos_emergencia"] : NULL;
			$laterales_der_izq = isset($linea9["laterales_der_izq"]) ? $linea9["laterales_der_izq"] : NULL;
			$retrovisor = isset($linea9["retrovisor"]) ? $linea9["retrovisor"] : NULL;
			$pito = isset($linea9["pito"]) ? $linea9["pito"] : NULL;
			$apoya_cabezas_delantero = isset($linea9["apoya_cabezas_delantero"]) ? $linea9["apoya_cabezas_delantero"] : NULL;
			$apoya_cabezas_trasero = isset($linea9["apoya_cabezas_trasero"]) ? $linea9["apoya_cabezas_trasero"] : NULL;
			$cambio_aceite = isset($linea9["cambio_aceite"]) ? $linea9["cambio_aceite"] : NULL;
			$sincronizacion = isset($linea9["sincronizacion"]) ? $linea9["sincronizacion"] : NULL;
			$alineacion_balanceo = isset($linea9["alineacion_balanceo"]) ? $linea9["alineacion_balanceo"] : NULL;
			$cambio_llantas = isset($linea9["cambio_llantas"]) ? $linea9["cambio_llantas"] : NULL;
			$carro_portacilindros = isset($linea9["carro_portacilindros"]) ? $linea9["carro_portacilindros"] : NULL;
			$cadenas = isset($linea9["cadenas"]) ? $linea9["cadenas"] : NULL;                      

			$observaciones_licencia = isset($linea9["observaciones_licencia"]) ? $linea9["observaciones_licencia"] : NULL;
			$observaciones_soat = isset($linea9["observaciones_soat"]) ? $linea9["observaciones_soat"] : NULL;
			$observaciones_rtm = isset($linea9["observaciones_rtm"]) ? $linea9["observaciones_rtm"] : NULL;
			$observaciones_seguro_dano_rc = isset($linea9["observaciones_seguro_dano_rc"]) ? $linea9["observaciones_seguro_dano_rc"] : NULL;
			$observaciones_manual_seguridad = isset($linea9["observaciones_manual_seguridad"]) ? $linea9["observaciones_manual_seguridad"] : NULL;
			$observaciones_tarjetas_emergencia = isset($linea9["observaciones_tarjetas_emergencia"]) ? $linea9["observaciones_tarjetas_emergencia"] : NULL;
			$observaciones_telefonos_emergencia = isset($linea9["observaciones_telefonos_emergencia"]) ? $linea9["observaciones_telefonos_emergencia"] : NULL;
			$observaciones_limpiabrisas = isset($linea9["observaciones_limpiabrisas"]) ? $linea9["observaciones_limpiabrisas"] : NULL;
			$observaciones_llantas_delanteras = isset($linea9["observaciones_llantas_delanteras"]) ? $linea9["observaciones_llantas_delanteras"] : NULL;
			$observaciones_llantas_traseras = isset($linea9["observaciones_llantas_traseras"]) ? $linea9["observaciones_llantas_traseras"] : NULL;
			$observaciones_llantas_repuesto = isset($linea9["observaciones_llantas_repuesto"]) ? $linea9["observaciones_llantas_repuesto"] : NULL;
			$observaciones_plataforma = isset($linea9["observaciones_plataforma"]) ? $linea9["observaciones_plataforma"] : NULL;
			$observaciones_frenos = isset($linea9["observaciones_frenos"]) ? $linea9["observaciones_frenos"] : NULL;
			$observaciones_aceite = isset($linea9["observaciones_aceite"]) ? $linea9["observaciones_aceite"] : NULL;
			$observaciones_refrigerante = isset($linea9["observaciones_refrigerante"]) ? $linea9["observaciones_refrigerante"] : NULL;
			$observaciones_cinturon_seguridad = isset($linea9["observaciones_cinturon_seguridad"]) ? $linea9["observaciones_cinturon_seguridad"] : NULL;
			$observaciones_fecha_tecnicomecanica = isset($linea9["observaciones_fecha_tecnicomecanica"]) ? $linea9["observaciones_fecha_tecnicomecanica"] : NULL;
			$observaciones_fecha_soat = isset($linea9["observaciones_fecha_soat"]) ? $linea9["observaciones_fecha_soat"] : NULL;
			$observaciones_carro_portatermo = isset($linea9["observaciones_carro_portatermo"]) ? $linea9["observaciones_carro_portatermo"] : NULL;
			$observaciones_cadenas = isset($linea9["observaciones_cadenas"]) ? $linea9["observaciones_cadenas"] : NULL;
			$observaciones_riatas = isset($linea9["observaciones_riatas"]) ? $linea9["observaciones_riatas"] : NULL;
			$observaciones_direccionales_delanteras = isset($linea9["observaciones_direccionales_delanteras"]) ? $linea9["observaciones_direccionales_delanteras"] : NULL;
			$observaciones_direccionales_traseras = isset($linea9["observaciones_direccionales_traseras"]) ? $linea9["observaciones_direccionales_traseras"] : NULL;
			$observaciones_luces_altas = isset($linea9["observaciones_luces_altas"]) ? $linea9["observaciones_luces_altas"] : NULL;
			$observaciones_luces_bajas = isset($linea9["observaciones_luces_bajas"]) ? $linea9["observaciones_luces_bajas"] : NULL;
			$observaciones_luces_stops = isset($linea9["observaciones_luces_stops"]) ? $linea9["observaciones_luces_stops"] : NULL;
			$observaciones_luces_reversa = isset($linea9["observaciones_luces_reversa"]) ? $linea9["observaciones_luces_reversa"] : NULL;
			$observaciones_luces_parqueo = isset($linea9["observaciones_luces_parqueo"]) ? $linea9["observaciones_luces_parqueo"] : NULL;
			$observaciones_frenos_principal = isset($linea9["observaciones_frenos_principal"]) ? $linea9["observaciones_frenos_principal"] : NULL;
			$observaciones_frenos_emergencia = isset($linea9["observaciones_frenos_emergencia"]) ? $linea9["observaciones_frenos_emergencia"] : NULL;
			$observaciones_laterales_der_izq = isset($linea9["observaciones_laterales_der_izq"]) ? $linea9["observaciones_laterales_der_izq"] : NULL;
			$observaciones_retrovisor = isset($linea9["observaciones_retrovisor"]) ? $linea9["observaciones_retrovisor"] : NULL;
			$observaciones_pito = isset($linea9["observaciones_pito"]) ? $linea9["observaciones_pito"] : NULL;
			$observaciones_carro_portacilindros = isset($linea9["observaciones_carro_portacilindros"]) ? $linea9["observaciones_carro_portacilindros"] : NULL;
			$observaciones_apoya_cabezas_delantero = isset($linea9["observaciones_apoya_cabezas_delantero"]) ? $linea9["observaciones_apoya_cabezas_delantero"] : NULL;
			$observaciones_apoya_cabezas_trasero = isset($linea9["observaciones_apoya_cabezas_trasero"]) ? $linea9["observaciones_apoya_cabezas_trasero"] : NULL;

			$consulta12 = "SELECT * FROM verificacion_elementos_emergencia WHERE id_verificacion_mensual = $id_verificacion_mensual";
				$resultado12 = mysqli_query($con,$consulta12);

				$linea12 = mysqli_fetch_array($resultado12);

				$vencimiento_extintor = isset($linea12["vencimiento_extintor"]) ? $linea12["vencimiento_extintor"] : NULL;
				$capacidad_extintor = isset($linea12["capacidad_extintor"]) ? $linea12["capacidad_extintor"] : NULL;
				$conforme_vencimiento_extintor = isset($linea12["conforme_vencimiento_extintor"]) ? $linea12["conforme_vencimiento_extintor"] : NULL;
				$conforme_capacidad_extintor = isset($linea12["conforme_capacidad_extintor"]) ? $linea12["conforme_capacidad_extintor"] : NULL;
				$alicates_destornilladores = isset($linea12["alicates_destornilladores"]) ? $linea12["alicates_destornilladores"] : NULL;
				$llave_fija = isset($linea12["llave_fija"]) ? $linea12["llave_fija"] : NULL;
				$cruceta = isset($linea12["cruceta"]) ? $linea12["cruceta"] : NULL;
				$gato = isset($linea12["gato"]) ? $linea12["gato"] : NULL;
				$conos = isset($linea12["conos"]) ? $linea12["conos"] : NULL;
				$tacos = isset($linea12["tacos"]) ? $linea12["tacos"] : NULL;
				$senales = isset($linea12["senales"]) ? $linea12["senales"] : NULL;
				$chaleco = isset($linea12["chaleco"]) ? $linea12["chaleco"] : NULL;
				$botiquin = isset($linea12["botiquin"]) ? $linea12["botiquin"] : NULL;

				$observaciones_vencimiento_extintor = isset($linea12["observaciones_vencimiento_extintor"]) ? $linea12["observaciones_vencimiento_extintor"] : NULL;
				$observaciones_capacidad_extintor = isset($linea12["observaciones_capacidad_extintor"]) ? $linea12["observaciones_capacidad_extintor"] : NULL;
				$observaciones_alicates_destornilladores = isset($linea12["observaciones_alicates_destornilladores"]) ? $linea12["observaciones_alicates_destornilladores"] : NULL;
				$observaciones_llave_fija = isset($linea12["observaciones_llave_fija"]) ? $linea12["observaciones_llave_fija"] : NULL;
				$observaciones_cruceta = isset($linea12["observaciones_cruceta"]) ? $linea12["observaciones_cruceta"] : NULL;
				$observaciones_gato = isset($linea12["observaciones_gato"]) ? $linea12["observaciones_gato"] : NULL;
				$observaciones_conos = isset($linea12["observaciones_conos"]) ? $linea12["observaciones_conos"] : NULL;
				$observaciones_tacos = isset($linea12["observaciones_tacos"]) ? $linea12["observaciones_tacos"] : NULL;
				$observaciones_senales = isset($linea12["observaciones_senales"]) ? $linea12["observaciones_senales"] : NULL;
				$observaciones_chaleco = isset($linea12["observaciones_chaleco"]) ? $linea12["observaciones_chaleco"] : NULL;
				$observaciones_botiquin = isset($linea12["observaciones_botiquin"]) ? $linea12["observaciones_botiquin"] : NULL;

				$conforme_vencimiento_extintor = isset($linea12["conforme_vencimiento_extintor"]) ? $linea12["conforme_vencimiento_extintor"] : NULL;
				$conforme_capacidad_extintor = isset($linea12["conforme_capacidad_extintor"]) ? $linea12["conforme_capacidad_extintor"] : NULL;

/**
*  Clase para la maquetación de Factura
*/
class PDF extends FPDF
{
	function _Arc($x1, $y1, $x2, $y2, $x3, $y3) //Curvatura del rectángulo
	{
		$h = $this->h;
		$this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
							$x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
	}
	// private functions
	function RoundedRect($x, $y, $w, $h, $r, $style = '') //Rectángulo con bordes redondeados 
	{
		$k = $this->k;
		$hp = $this->h;
		if($style=='F')
			$op='f';
		elseif($style=='FD' || $style=='DF')
			$op='B';
		else
			$op='S';
		$MyArc = 4/3 * (sqrt(2) - 1);
		$this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));
		$xc = $x+$w-$r ;
		$yc = $y+$r;
		$this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));

		$this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);
		$xc = $x+$w-$r ;
		$yc = $y+$h-$r;
		$this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
		$this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);
		$xc = $x+$r ;
		$yc = $y+$h-$r;
		$this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
		$this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);
		$xc = $x+$r ;
		$yc = $y+$r;
		$this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
		$this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
		$this->_out($op);
	}
	// public functions
	function sizeOfText( $texte, $largeur )
	{
		$index    = 0;
		$nb_lines = 0;
		$loop     = TRUE;
		while ( $loop )
		{
			$pos = strpos($texte, "\n");
			if (!$pos)
			{
				$loop  = FALSE;
				$ligne = $texte;
			}
			else
			{
				$ligne  = substr( $texte, $index, $pos);
				$texte = substr( $texte, $pos+1 );
			}
			$length = floor( $this->GetStringWidth( $ligne ) );
			$res = 1 + floor( $length / $largeur) ;
			$nb_lines += $res;
		}
		return $nb_lines;
	}
	//ENCABEZADO DE PÁGINA
	function Header()
	{
		$x1 = 140;
		$y1 = 6;
		//Variables de encabezado
		global $logotipo_empresa, $nombre_empresa;
		global $reviso, $aprobo,$fecha_reviso,$fecha_aprobo,$codigo,$version,$pagina;

	    $this->SetFont('Arial','B',10);
		$this->SetTextColor(0,0,0);
		$this->SetLineWidth(0);	
		//.....Logo de la compañia
		
		
	}
	//PIE DE PÁGINA
	function Footer()
	{
		$this->SetFont('Arial','B',8);

	}
}
$pdf = new PDF('P','mm', 'A4');
$pdf->SetAutoPageBreak(TRUE,40);
$pdf->AliasNbPages();
$pdf->AddPage();

//CONTENIDO DEL CUERPO DE LA HOJA DE RUTA

		$pdf->SetXY(15,15);
		$pdf->Cell(180,10,'LISTA DE CHEQUEO DEL VEHICULO ANTES DE LA MARCHA',0,0,'C');

		$pdf->SetFont('Arial','',9);

		$pdf->SetXY(15,25);
		$pdf->MultiCell(180,5,utf8_decode('Elaborada bajo referencia Anexo II de la Resolución 1565 del 6 de junio de 2014, por la cual se expide la Guía metodológica para la elaboración del Plan Estratégico de Seguridad Vial, expedida por el Ministerio de Transporte.'),0,'L');
		
		
		$pdf->SetXY(15,40);
		$pdf->Cell(30,5,utf8_decode('FECHA:'),1,1);
		$pdf->SetXY(45,40);
		$pdf->Cell(40,5,utf8_decode($fecha_verificacion),1,1);
		$pdf->SetXY(15,45);
		$pdf->Cell(30,5,utf8_decode('CIUDAD:'),1,1);
		$pdf->SetXY(45,45);
		$pdf->Cell(40,5,utf8_decode($ciudad),1,1);
		$pdf->SetXY(15,50);
		$pdf->Cell(30,5,utf8_decode('RESPONSABLE:'),1,1);
		$pdf->SetXY(45,50);
		$pdf->Cell(40,5,utf8_decode($nombre_usuario),1,1);
		$pdf->SetXY(15,55);
		$pdf->Cell(30,5,utf8_decode('IDENTIFICACIÓN:'),1,1);
		$pdf->SetXY(45,55);
		$pdf->Cell(40,5,utf8_decode($identificacion),1,1);
		$pdf->SetXY(15,60);
		$pdf->Cell(30,5,utf8_decode('CARGO:'),1,1);
		$pdf->SetXY(45,60);
		$pdf->Cell(40,5,utf8_decode($cargo),1,1);
		$pdf->SetXY(15,65);
		$pdf->Cell(30,5,utf8_decode('AREA:'),1,1);
		$pdf->SetXY(45,65);
		$pdf->Cell(40,5,utf8_decode($area),1,1);

		$pdf->SetFont('Arial','B',8);

		$pdf->SetXY(85,55);
		$pdf->Cell(55,5,utf8_decode('VEHICULO QUE ENTREGA'),1,1,'C');

		$pdf->SetFont('Arial','',8);

		$pdf->SetXY(85,60);
		$pdf->Cell(55,5,utf8_decode('PLACA:'),1,1);
		$pdf->SetXY(112.5,60);
		$pdf->Cell(27.5,5,utf8_decode($placa_camion),1,1);
		$pdf->SetXY(85,65);
		$pdf->Cell(55,5,utf8_decode('KILOMETRAJE:'),1,1);
		$pdf->SetXY(112.5,65);
		$pdf->Cell(27.5,5,utf8_decode($kilometraje),1,1);

		$pdf->SetFont('Arial','B',8);

		$pdf->SetXY(15,75);
		$pdf->Cell(180,10,'',0,1);

		$pdf->SetXY(15,75);
		$pdf->Cell(67,10,utf8_decode('ELEMENTOS QUE SE INSPECCIONAN'),1,1,'C');	

		$pdf->SetXY(82,75);
		$pdf->Cell(43,10,utf8_decode('CRITERIO'),1,1,'C');

		$pdf->SetXY(125,75);
		$pdf->Cell(42,5,'ESTADO',1,1,'C');

		$pdf->SetXY(125,80);
		$pdf->Cell(21,5,'CONFORME',1,1,'C');

		$pdf->SetXY(146,80);
		$pdf->Cell(21,5,'NO CONFORME',1,1,'C');		

		$pdf->SetXY(167,75);
		$pdf->Cell(29,10,'OBSERVACIONES',1,1,'C');

		$pdf->SetFont('Arial','',8);

		$pdf->SetXY(15,85);
		$pdf->Cell(31,66,'DOCUMENTOS',1,1);

		$pdf->SetXY(46,85);
		$pdf->MultiCell(36,3,utf8_decode('LICENCIA DE CONDUCCIÓN'),1,1);

		$pdf->SetXY(46,91);
		$pdf->Cell(36,5,utf8_decode('SOAT'),1,1);

		$pdf->SetXY(46,96);
		$pdf->Cell(36,5,utf8_decode('RTM'),1,1);

		$pdf->SetXY(46,101);
		$pdf->MultiCell(36,3,utf8_decode('SEGURO DE DAÑOS Y RC'),1,1);

		$pdf->SetXY(46,107);
		$pdf->MultiCell(36,4,utf8_decode('MANUAL DE SEGURIDAD DE TRANSPORTE DE CILINDROS'),1,1);

		$pdf->SetXY(46,123);
		$pdf->MultiCell(36,4,utf8_decode('TARJETAS DE EMERGENCIA DE LOS GASES TRANSPORTADOS'),1,1);

		$pdf->SetXY(46,139);
		$pdf->MultiCell(36,4,utf8_decode('TELÉFONOS DE EMERGENCIA'),0,1);

		$pdf->SetXY(46,139);
		$pdf->Cell(36,12,utf8_decode(''),1,1);

		$pdf->SetXY(82,85);
		$pdf->MultiCell(43,3,utf8_decode('Los documentos se encuentran en físico y la fecha está vigente'),0,1);

		$pdf->SetXY(82,85);
		$pdf->MultiCell(43,22,utf8_decode(''),1,1);

		$pdf->SetXY(82,107);
		$pdf->MultiCell(43,3,utf8_decode('Constatar que existe un documento informativo sobre la manipulación y transporte de los cilindros'),0,1);

		$pdf->SetXY(82,107);
		$pdf->MultiCell(43,16,utf8_decode(''),1,1);

		$pdf->SetXY(82,123);
		$pdf->MultiCell(43,3,utf8_decode('Verificar que se encuentren las fichas correspondientes y actualizadas de los gases transportados'),0,1);

		$pdf->SetXY(82,123);
		$pdf->MultiCell(43,16,utf8_decode(''),1,1);

		$pdf->SetXY(82,139);
		$pdf->MultiCell(43,3,utf8_decode('Constatar que la lista cuenta con los contactos de los principales agentes de atención de emergencias'),0,1);

		$pdf->SetXY(82,139);
		$pdf->MultiCell(43,12,utf8_decode(''),1,1);

		$pdf->SetXY(125,85);
		if($licencia==1){
			$pdf->Cell(21,6,'X',1,1,'C');
		}else{
			$pdf->Cell(21,6,'',1,1,'C');
		}

		$pdf->SetXY(146,85);
		if($licencia==2){
			$pdf->Cell(21,6,'X',1,1,'C');
		}else{
			$pdf->Cell(21,6,'',1,1,'C');
		}

		$pdf->SetXY(167,85);
		$pdf->Cell(29,6,utf8_decode($observaciones_licencia),1,1,'C');

		$pdf->SetXY(125,91);
		if($soat==1){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(146,91);
		if($soat==2){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(167,91);
		$pdf->Cell(29,5,utf8_decode($observaciones_soat),1,1,'C');

		$pdf->SetXY(125,96);
		if($rtm==1){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(146,96);
		if($rtm==2){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(167,96);
		$pdf->Cell(29,5,utf8_decode($observaciones_rtm),1,1,'C');

		$pdf->SetXY(125,101);
		if($seguro_dano_rc==1){
			$pdf->Cell(21,6,'X',1,1,'C');
		}else{
			$pdf->Cell(21,6,'',1,1,'C');
		}

		$pdf->SetXY(146,101);
		if($seguro_dano_rc==2){
			$pdf->Cell(21,6,'X',1,1,'C');
		}else{
			$pdf->Cell(21,6,'',1,1,'C');
		}

		$pdf->SetXY(167,101);
		$pdf->Cell(29,6,utf8_decode($observaciones_seguro_dano_rc),1,1,'C');

		$pdf->SetXY(125,107);
		if($manual_seguridad==1){
			$pdf->Cell(21,16,'X',1,1,'C');
		}else{
			$pdf->Cell(21,16,'',1,1,'C');
		}

		$pdf->SetXY(146,107);
		if($manual_seguridad==2){
			$pdf->Cell(21,16,'X',1,1,'C');
		}else{
			$pdf->Cell(21,16,'',1,1,'C');
		}

		$pdf->SetXY(167,107);
		$pdf->Cell(29,16,utf8_decode($observaciones_manual_seguridad),1,1,'C');
		
		$pdf->SetXY(125,123);
		if($tarjetas_emergencia==1){
			$pdf->Cell(21,16,'X',1,1,'C');
		}else{
			$pdf->Cell(21,16,'',1,1,'C');
		}

		$pdf->SetXY(146,123);
		if($tarjetas_emergencia==2){
			$pdf->Cell(21,16,'X',1,1,'C');
		}else{
			$pdf->Cell(21,16,'',1,1,'C');
		}

		$pdf->SetXY(167,123);
		$pdf->Cell(29,16,utf8_decode($observaciones_tarjetas_emergencia),1,1,'C');

		$pdf->SetXY(125,139);
		if($telefonos_emergencia==1){
			$pdf->Cell(21,12,'X',1,1,'C');
		}else{
			$pdf->Cell(21,12,'',1,1,'C');
		}

		$pdf->SetXY(146,139);
		if($telefonos_emergencia==2){
			$pdf->Cell(21,12,'X',1,1,'C');
		}else{
			$pdf->Cell(21,12,'',1,1,'C');
		}

		$pdf->SetXY(167,139);
		$pdf->Cell(29,12,utf8_decode($observaciones_telefonos_emergencia),1,1,'C');


		$pdf->SetXY(15,151);
		$pdf->Cell(31,10,'DIRECCIONALES',1,1);

		$pdf->SetXY(46,151);
		$pdf->Cell(36,5,utf8_decode('DELANTERAS'),1,1);

		$pdf->SetXY(46,156);
		$pdf->Cell(36,5,utf8_decode('TRASERAS'),1,1);

		$pdf->SetXY(82,151);
		$pdf->MultiCell(43,4,utf8_decode('Funcionamiento adecuado, respuesta inmediata'),0,1);

		$pdf->SetXY(82,151);
		$pdf->MultiCell(43,10,utf8_decode(''),1,1);

		$pdf->SetXY(125,151);
		if($direccionales_delanteras==1){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(146,151);
		if($direccionales_delanteras==2){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(167,151);
		$pdf->Cell(29,5,utf8_decode($observaciones_direccionales_delanteras),1,1,'C');

		$pdf->SetXY(125,156);
		if($direccionales_traseras==1){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(146,156);
		if($direccionales_traseras==2){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(167,156);
		$pdf->Cell(29,5,utf8_decode($observaciones_direccionales_traseras),1,1,'C');

		$pdf->SetXY(15,161);
		$pdf->Cell(31,25,'LUCES',1,1);

		$pdf->SetXY(46,161);
		$pdf->Cell(36,5,utf8_decode('ALTAS'),1,1);

		$pdf->SetXY(46,166);
		$pdf->Cell(36,5,utf8_decode('BAJAS'),1,1);

		$pdf->SetXY(46,171);
		$pdf->Cell(36,5,utf8_decode('STOPS'),1,1);

		$pdf->SetXY(46,176);
		$pdf->Cell(36,5,utf8_decode('REVERSA'),1,1);

		$pdf->SetXY(46,181);
		$pdf->Cell(36,5,utf8_decode('PARQUEO'),1,1);

		$pdf->SetXY(82,161);
		$pdf->MultiCell(43,4,utf8_decode('Funcionamiento de bombillas, cubierta sin rotura, leds no fundidos'),0,1);

		$pdf->SetXY(82,161);
		$pdf->MultiCell(43,25,utf8_decode(''),1,1);

		$pdf->SetXY(125,161);
		if($luces_altas==1){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(146,161);
		if($luces_altas==2){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(167,161);
		$pdf->Cell(29,5,utf8_decode($observaciones_luces_altas),1,1,'C');

		$pdf->SetXY(125,166);
		if($luces_bajas==1){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(146,166);
		if($luces_bajas==2){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}
		$pdf->SetXY(167,166);
		$pdf->Cell(29,5,utf8_decode($observaciones_luces_bajas),1,1,'C');

		$pdf->SetXY(125,171);
		if($luces_stops==1){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(146,171);
		if($luces_stops==2){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(167,171);
		$pdf->Cell(29,5,utf8_decode($observaciones_luces_stops),1,1,'C');

		$pdf->SetXY(125,176);
		if($luces_reversa==1){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(146,176);
		if($luces_reversa==2){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(167,176);
		$pdf->Cell(29,5,utf8_decode($observaciones_luces_reversa),1,1,'C');

		$pdf->SetXY(125,181);
		if($luces_parqueo==1){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(146,181);
		if($luces_parqueo==2){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(167,181);
		$pdf->Cell(29,5,utf8_decode($observaciones_luces_parqueo),1,1,'C');


		$pdf->SetXY(15,186);
		$pdf->Cell(31,10,'LIMPIABRISAS',1,1);

		$pdf->SetXY(46,186);
		$pdf->Cell(36,10,utf8_decode('DER/IZQ/ATRÁS'),1,1);

		$pdf->SetXY(82,186);
		$pdf->MultiCell(43,4,utf8_decode('Plumilla en buen estado,(limpieza y estructura)'),0,1);

		$pdf->SetXY(82,186);
		$pdf->MultiCell(43,10,utf8_decode(''),1,1);

		$pdf->SetXY(125,186);
		if($limpiabrisas==1){
			$pdf->Cell(21,10,'X',1,1,'C');
		}else{
			$pdf->Cell(21,10,'',1,1,'C');
		}

		$pdf->SetXY(146,186);
		if($limpiabrisas==2){
			$pdf->Cell(21,10,'X',1,1,'C');
		}else{
			$pdf->Cell(21,10,'',1,1,'C');
		}

		$pdf->SetXY(167,186);
		$pdf->Cell(29,10,utf8_decode($observaciones_limpiabrisas),1,1,'C');

		$pdf->SetXY(15,196);
		$pdf->Cell(31,10,'FRENOS',1,1);

		$pdf->SetXY(46,196);
		$pdf->Cell(36,5,utf8_decode('PRINCIPAL'),1,1);

		$pdf->SetXY(46,201);
		$pdf->Cell(36,5,utf8_decode('EMERGENCIA'),1,1);

		$pdf->SetXY(82,196);
		$pdf->MultiCell(43,4,utf8_decode('Verificar cada dia al momento de comenzar la marcha'),0,1);

		$pdf->SetXY(82,196);
		$pdf->MultiCell(43,10,utf8_decode(''),1,1);

		$pdf->SetXY(125,196);
		if($frenos_principal==1){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(146,196);
		if($frenos_principal==2){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(167,196);
		$pdf->Cell(29,5,utf8_decode($observaciones_frenos_principal),1,1,'C');

		$pdf->SetXY(125,201);
		if($frenos_emergencia==1){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(146,201);
		if($frenos_emergencia==2){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(167,201);
		$pdf->Cell(29,5,utf8_decode($observaciones_frenos_emergencia),1,1,'C');

		$pdf->SetXY(15,206);
		$pdf->Cell(31,15,'LLANTAS',1,1);

		$pdf->SetXY(46,206);
		$pdf->Cell(36,5,utf8_decode('DELANTERAS'),1,1);

		$pdf->SetXY(46,211);
		$pdf->Cell(36,5,utf8_decode('TRASERAS'),1,1);

		$pdf->SetXY(46,216);
		$pdf->Cell(36,5,utf8_decode('REPUESTO'),1,1);

		$pdf->SetXY(82,206);
		$pdf->MultiCell(43,4,utf8_decode('Cada dia, antes de iniciar la marcha, verificar estado, presión y profundidad del labrado'),0,1);

		$pdf->SetXY(82,206);
		$pdf->MultiCell(43,15,utf8_decode(''),1,1);

		$pdf->SetXY(125,206);
		if($llantas_delanteras==1){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(146,206);
		if($llantas_delanteras==2){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(167,206);
		$pdf->Cell(29,5,utf8_decode($observaciones_llantas_delanteras),1,1,'C');

		$pdf->SetXY(125,211);
		if($llantas_traseras==1){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(146,211);
		if($llantas_traseras==2){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(167,211);
		$pdf->Cell(29,5,utf8_decode($observaciones_llantas_traseras),1,1,'C');

		$pdf->SetXY(125,216);
		if($llantas_repuesto==1){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(146,216);
		if($llantas_repuesto==2){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(167,216);
		$pdf->Cell(29,5,utf8_decode($observaciones_llantas_repuesto),1,1,'C');

		$pdf->SetXY(15,221);
		$pdf->Cell(31,10,'ESPEJOS',1,1);

		$pdf->SetXY(46,221);
		$pdf->Cell(36,5,utf8_decode('LATERALES DER/IZQ'),1,1);

		$pdf->SetXY(46,226);
		$pdf->Cell(36,5,utf8_decode('RETROVISOR'),1,1);

		$pdf->SetXY(82,221);
		$pdf->MultiCell(43,4,utf8_decode('Verificar estado(limpieza, sin rotura, ni opacidad)'),0,1);

		$pdf->SetXY(82,221);
		$pdf->MultiCell(43,10,utf8_decode(''),1,1);

		$pdf->SetXY(125,221);
		if($laterales_der_izq==1){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(146,221);
		if($laterales_der_izq==2){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(167,221);
		$pdf->Cell(29,5,utf8_decode($observaciones_laterales_der_izq),1,1,'C');

		$pdf->SetXY(125,226);
		if($retrovisor==1){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(146,226);
		if($retrovisor==2){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(167,226);
		$pdf->Cell(29,5,utf8_decode($observaciones_retrovisor),1,1,'C');

		$pdf->SetXY(15,231);
		$pdf->Cell(67,20,'PLATAFORMA',1,1,'C');

		$pdf->SetXY(82,231);
		$pdf->MultiCell(43,3,utf8_decode('Verificar el buen funcionamiento del sistema mecánico de la plataforma y el buen estado de las cadenas y barras para sujetar los cilindros'),0,1);

		$pdf->SetXY(82,231);
		$pdf->MultiCell(43,20,utf8_decode(''),1,1);

		$pdf->SetXY(125,231);
		if($plataforma==1){
			$pdf->Cell(21,20,'X',1,1,'C');
		}else{
			$pdf->Cell(21,20,'',1,1,'C');
		}

		$pdf->SetXY(146,231);
		if($plataforma==2){
			$pdf->Cell(21,20,'X',1,1,'C');
		}else{
			$pdf->Cell(21,20,'',1,1,'C');
		}

		$pdf->SetXY(167,231);
		$pdf->Cell(29,20,utf8_decode($observaciones_plataforma),1,1,'C');

		$pdf->AddPage();

		$pdf->SetXY(15,15);
		$pdf->Cell(67,10,'PITO',1,1,'C');

		$pdf->SetXY(82,15);
		$pdf->MultiCell(43,3,utf8_decode('Accionar antes de iniciar la marcha, debe responder en forma adecuada'),0,1);

		$pdf->SetXY(82,15);
		$pdf->MultiCell(43,10,utf8_decode(''),1,1);

		$pdf->SetXY(125,15);
		if($pito==1){
			$pdf->Cell(21,10,'X',1,1,'C');
		}else{
			$pdf->Cell(21,10,'',1,1,'C');
		}

		$pdf->SetXY(146,15);
		if($pito==2){
			$pdf->Cell(21,10,'X',1,1,'C');
		}else{
			$pdf->Cell(21,10,'',1,1,'C');
		}

		$pdf->SetXY(167,15);
		$pdf->Cell(29,10,utf8_decode($observaciones_pito),1,1,'C');

		$pdf->SetXY(15,25);
		$pdf->Cell(31,15,'NIVELES DE FLUIDOS',1,1);

		$pdf->SetXY(46,25);
		$pdf->Cell(36,5,utf8_decode('FRENOS'),1,1);

		$pdf->SetXY(46,30);
		$pdf->Cell(36,5,utf8_decode('ACEITE'),1,1);

		$pdf->SetXY(46,35);
		$pdf->Cell(36,5,utf8_decode('REFRIGERANTE'),1,1);	

		$pdf->SetXY(82,25);
		$pdf->MultiCell(43,4,utf8_decode('Verificar que los niveles de los fluidos sean adecuados(reportar fugas)'),0,1);

		$pdf->SetXY(82,25);
		$pdf->MultiCell(43,15,utf8_decode(''),1,1);	

		$pdf->SetXY(125,25);
		if($frenos==1){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(146,25);
		if($frenos==2){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(167,25);
		$pdf->Cell(29,5,utf8_decode($observaciones_frenos),1,1,'C');

		$pdf->SetXY(125,30);
		if($aceite==1){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(146,30);
		if($aceite==2){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(167,30);
		$pdf->Cell(29,5,utf8_decode($observaciones_aceite),1,1,'C');

		$pdf->SetXY(125,35);
		if($refrigerante==1){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(146,35);
		if($refrigerante==2){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(167,35);
		$pdf->Cell(29,5,utf8_decode($observaciones_refrigerante),1,1,'C');

		$pdf->SetXY(15,40);
		$pdf->Cell(31,10,'APOYA CABEZAS',1,1);

		$pdf->SetXY(46,40);
		$pdf->Cell(36,5,utf8_decode('DELANTEROS'),1,1);

		$pdf->SetXY(46,45);
		$pdf->Cell(36,5,utf8_decode('TRASEROS'),1,1);

		$pdf->SetXY(82,40);
		$pdf->MultiCell(43,3,utf8_decode('Verificar su estado'),0,1);

		$pdf->SetXY(82,40);
		$pdf->MultiCell(43,10,utf8_decode(''),1,1);

		$pdf->SetXY(125,40);
		if($apoya_cabezas_delantero==1){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(146,40);
		if($apoya_cabezas_delantero==2){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(167,40);
		$pdf->Cell(29,5,utf8_decode($observaciones_apoya_cabezas_delantero),1,1,'C');

		$pdf->SetXY(125,45);
		if($apoya_cabezas_trasero==1){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(146,45);
		if($apoya_cabezas_trasero==2){
			$pdf->Cell(21,5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,5,'',1,1,'C');
		}

		$pdf->SetXY(167,45);
		$pdf->Cell(29,5,utf8_decode($observaciones_apoya_cabezas_trasero),1,1,'C');

		$pdf->SetXY(15,50);
		$pdf->Cell(67,10,'CINTURONES DE SEGURIDAD DEL / TRAS',1,1,'C');

		$pdf->SetXY(82,50);
		$pdf->MultiCell(43,4,utf8_decode('Verificar estado de las partes (hebillas, tela) y ajuste'),0,1);

		$pdf->SetXY(82,50);
		$pdf->MultiCell(43,10,utf8_decode(''),1,1);

		$pdf->SetXY(125,50);
		if($cinturon_seguridad==1){
			$pdf->Cell(21,10,'X',1,1,'C');
		}else{
			$pdf->Cell(21,10,'',1,1,'C');
		}

		$pdf->SetXY(146,50);
		if($cinturon_seguridad==2){
			$pdf->Cell(21,10,'X',1,1,'C');
		}else{
			$pdf->Cell(21,10,'',1,1,'C');
		}

		$pdf->SetXY(167,50);
		$pdf->Cell(29,10,utf8_decode($observaciones_cinturon_seguridad),1,1,'C');

		$pdf->SetXY(15,65);
		$pdf->MultiCell(31,4,'ULTIMA FECHA DE MANTENIMIENTO',0,1);		


		$pdf->SetXY(15,60);
		$pdf->MultiCell(31,20,'',1,1);

		$pdf->SetXY(46,60);
		$pdf->Cell(36,5,utf8_decode('CAMBIO DE ACEITE'),1,1);

		$pdf->SetXY(46,65);
		$pdf->Cell(36,5,utf8_decode('SINCRONIZACIÓN'),1,1);

		$pdf->SetXY(46,70);
		$pdf->Cell(36,5,utf8_decode('ALINEACIÓN-BALANCEO'),1,1);

		$pdf->SetXY(46,75);
		$pdf->Cell(36,5,utf8_decode('CAMBIO DE LLANTAS'),1,1);

		$pdf->SetXY(82,60);
		$pdf->MultiCell(43,5,utf8_decode('Fecha'),1,1);
		$pdf->SetXY(82,65);
		$pdf->MultiCell(43,5,utf8_decode('Fecha'),1,1);
		$pdf->SetXY(82,70);
		$pdf->MultiCell(43,5,utf8_decode('Fecha'),1,1);
		$pdf->SetXY(82,75);
		$pdf->MultiCell(43,5,utf8_decode('Fecha'),1,1);

		$pdf->SetXY(125,60);
		$pdf->Cell(71,5,utf8_decode($cambio_aceite),1,1,'C');
		$pdf->SetXY(125,65);
		$pdf->Cell(71,5,utf8_decode($sincronizacion),1,1,'C');
		$pdf->SetXY(125,70);
		$pdf->Cell(71,5,utf8_decode($alineacion_balanceo),1,1,'C');
		$pdf->SetXY(125,75);
		$pdf->Cell(71,5,utf8_decode($cambio_llantas),1,1,'C');

		$pdf->SetXY(15,82.5);
		$pdf->MultiCell(31,4,'FECHAS DE VENCIMIENTO',0,1);

		$pdf->SetXY(15,80);
		$pdf->Cell(31,15,'',1,1);

		$pdf->SetXY(46,80);
		$pdf->MultiCell(36,4,utf8_decode('REVISIÓN TÉCNICOMECANICA'),0,1);

		$pdf->SetXY(46,80);
		$pdf->Cell(36,10,'',1,1);

		$pdf->SetXY(46,90);
		$pdf->Cell(36,5,utf8_decode('SOAT'),1,1);

		$pdf->SetXY(82,80);
		$pdf->MultiCell(43,10,utf8_decode('Fecha'),1,1);
		$pdf->SetXY(82,90);
		$pdf->MultiCell(43,5,utf8_decode('Fecha'),1,1);

		$pdf->SetXY(125,80);
		$pdf->Cell(42,10,utf8_decode($fecha_tecnicomecanica),1,1,'C');
		$pdf->SetXY(125,90);
		$pdf->Cell(42,5,utf8_decode($fecha_soat),1,1,'C');

		$pdf->SetXY(167,80);
		$pdf->Cell(29,10,utf8_decode($observaciones_fecha_tecnicomecanica),1,1,'C');

		$pdf->SetXY(167,90);
		$pdf->Cell(29,5,utf8_decode($observaciones_fecha_soat),1,1,'C');

		$pdf->SetXY(15,95);
		$pdf->Cell(67,7.5,'CARRO PORTA CILINDROS',1,1,'C');

		$pdf->SetXY(15,102.5);
		$pdf->Cell(67,7.5,'CARRO PORTA TERMOS',1,1,'C');

		$pdf->SetXY(82,95);
		$pdf->MultiCell(43,4,utf8_decode('Verificar estado de las llantas y condiciones generales de seguridad y limpieza'),0,1);

		$pdf->SetXY(82,95);
		$pdf->MultiCell(43,15,utf8_decode(''),1,1);

		$pdf->SetXY(125,95);
		if($carro_portacilindros==1){
			$pdf->Cell(21,7.5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,7.5,'',1,1,'C');
		}

		$pdf->SetXY(146,95);
		if($carro_portacilindros==2){
			$pdf->Cell(21,7.5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,7.5,'',1,1,'C');
		}

		$pdf->SetXY(167,95);
		$pdf->Cell(29,7.5,utf8_decode($observaciones_carro_portacilindros),1,1,'C');

		$pdf->SetXY(125,102.5);
		if($carro_portatermo==1){
			$pdf->Cell(21,7.5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,7.5,'',1,1,'C');
		}

		$pdf->SetXY(146,102.5);
		if($carro_portatermo==2){
			$pdf->Cell(21,7.5,'X',1,1,'C');
		}else{
			$pdf->Cell(21,7.5,'',1,1,'C');
		}

		$pdf->SetXY(167,102.5);
		$pdf->Cell(29,7.5,utf8_decode($observaciones_carro_portatermo),1,1,'C');

		$pdf->SetXY(15,110);
		$pdf->MultiCell(67,4,'CADENAS EN PLATAFORMA PARA SUJETAR CILINDROS',0,1);

		$pdf->SetXY(15,110);
		$pdf->MultiCell(67,10,'',1,1);

		$pdf->SetXY(82,110);
		$pdf->MultiCell(43,4,utf8_decode('Verificar estado y cantidad'),0,1);

		$pdf->SetXY(82,110);
		$pdf->MultiCell(43,10,utf8_decode(''),1,1);

		$pdf->SetXY(125,110);
		if($cadenas==1){
			$pdf->Cell(21,10,'X',1,1,'C');
		}else{
			$pdf->Cell(21,10,'',1,1,'C');
		}

		$pdf->SetXY(146,110);
		if($cadenas==2){
			$pdf->Cell(21,10,'X',1,1,'C');
		}else{
			$pdf->Cell(21,10,'',1,1,'C');
		}

		$pdf->SetXY(167,110);
		$pdf->Cell(29,10,utf8_decode($observaciones_cadenas),1,1,'C');

		$pdf->SetXY(15,120);
		$pdf->MultiCell(67,4,'RIATAS PARA SUJETAR CILINDROS',0,1);

		$pdf->SetXY(15,120);
		$pdf->MultiCell(67,10,'',1,1);

		$pdf->SetXY(82,120);
		$pdf->MultiCell(43,4,utf8_decode('Verificar estado y cantidad'),0,1);

		$pdf->SetXY(82,120);
		$pdf->MultiCell(43,10,utf8_decode(''),1,1);

		$pdf->SetXY(125,120);
		if($cadenas==1){
			$pdf->Cell(21,10,'X',1,1,'C');
		}else{
			$pdf->Cell(21,10,'',1,1,'C');
		}

		$pdf->SetXY(146,120);
		if($cadenas==2){
			$pdf->Cell(21,10,'X',1,1,'C');
		}else{
			$pdf->Cell(21,10,'',1,1,'C');
		}

		$pdf->SetXY(167,120);
		$pdf->Cell(29,10,utf8_decode($observaciones_riatas),1,1,'C');

		$pdf->SetFont('Arial','B',9);

		$pdf->SetXY(15,135);
		$pdf->Cell(30,5,'EQUIPO DE SEGURIDAD',0,1);

		$pdf->SetFont('Arial','',9);

		$pdf->SetXY(15,145);
		$pdf->MultiCell(29,4,'EQUIPOS DE CARRETERA',0,1);

		$pdf->SetXY(15,145);
		$pdf->Cell(29,10,'',1,1);

		$pdf->SetXY(44,145);
		$pdf->Cell(73,10,'CRITERIO',1,1,'C');

		$pdf->SetXY(117,145);
		$pdf->Cell(50,5,'ESTADO(Conforme/No Conforme)',1,1,'C');

		$pdf->SetXY(117,150);
		$pdf->Cell(25,5,'CONFORME',1,1,'C');

		$pdf->SetXY(142,150);
		$pdf->Cell(25,5,'NO CONFORME',1,1,'C');	

		$pdf->SetXY(167,145);
		$pdf->Cell(29,10,'OBSERVACIONES',1,1,'C');

		$pdf->SetXY(15,155);
		$pdf->MultiCell(29,10,'EXTINTOR',1,1);

		$pdf->SetXY(44,155);
		$pdf->Cell(73,5,utf8_decode('Vencimiento: '.$vencimiento_extintor),1,1,'C');

		$pdf->SetXY(44,160);
		$pdf->Cell(73,5,utf8_decode('Capacidad: '.$capacidad_extintor),1,1,'C');

		$pdf->SetXY(117,155);
		if($conforme_vencimiento_extintor==1){
			$pdf->Cell(25,5,'X',1,1,'C');
		}else{
			$pdf->Cell(25,5,'',1,1,'C');
		}

		$pdf->SetXY(142,155);
		if($conforme_vencimiento_extintor==2){
			$pdf->Cell(25,5,'X',1,1,'C');
		}else{
			$pdf->Cell(25,5,'',1,1,'C');
		}

		$pdf->SetXY(117,160);
		if($conforme_capacidad_extintor==1){
			$pdf->Cell(25,5,'X',1,1,'C');
		}else{
			$pdf->Cell(25,5,'',1,1,'C');
		}

		$pdf->SetXY(142,160);
		if($conforme_capacidad_extintor==2){
			$pdf->Cell(25,5,'X',1,1,'C');
		}else{
			$pdf->Cell(25,5,'',1,1,'C');
		}

		$pdf->SetXY(167,155);
		$pdf->Cell(29,5,utf8_decode($observaciones_vencimiento_extintor),1,1);

		$pdf->SetXY(167,160);
		$pdf->Cell(29,5,utf8_decode($observaciones_capacidad_extintor),1,1);

		$pdf->SetXY(15,165);
		$pdf->MultiCell(29,10,'HERRAMIENTAS',1,1);

		$pdf->SetXY(44,165);
		$pdf->Cell(73,5,utf8_decode('Alicate, destornilladores y llaves fijas'),1,1,'C');

		$pdf->SetXY(44,170);
		$pdf->Cell(73,5,utf8_decode('Llave Expansiva'),1,1,'C');

		$pdf->SetXY(117,165);
		if($alicates_destornilladores==1){
			$pdf->Cell(25,5,'X',1,1,'C');
		}else{
			$pdf->Cell(25,5,'',1,1,'C');
		}

		$pdf->SetXY(142,165);
		if($alicates_destornilladores==2){
			$pdf->Cell(25,5,'X',1,1,'C');
		}else{
			$pdf->Cell(25,5,'',1,1,'C');
		}

		$pdf->SetXY(117,170);
		if($llave_fija==1){
			$pdf->Cell(25,5,'X',1,1,'C');
		}else{
			$pdf->Cell(25,5,'',1,1,'C');
		}

		$pdf->SetXY(142,170);
		if($llave_fija==2){
			$pdf->Cell(25,5,'X',1,1,'C');
		}else{
			$pdf->Cell(25,5,'',1,1,'C');
		}

		$pdf->SetXY(167,165);
		$pdf->Cell(29,5,utf8_decode($observaciones_alicates_destornilladores),1,1);

		$pdf->SetXY(167,170);
		$pdf->Cell(29,5,utf8_decode($observaciones_llave_fija),1,1);

		$pdf->SetXY(15,175);
		$pdf->MultiCell(29,5,'CRUCETA',1,1);

		$pdf->SetXY(44,175);
		$pdf->Cell(73,5,utf8_decode('Apta para el vehículo'),1,1,'C');

		$pdf->SetXY(117,175);
		if($cruceta==1){
			$pdf->Cell(25,5,'X',1,1,'C');
		}else{
			$pdf->Cell(25,5,'',1,1,'C');
		}

		$pdf->SetXY(142,175);
		if($cruceta==2){
			$pdf->Cell(25,5,'X',1,1,'C');
		}else{
			$pdf->Cell(25,5,'',1,1,'C');
		}

		$pdf->SetXY(167,175);
		$pdf->Cell(29,5,utf8_decode($observaciones_cruceta),1,1);

		$pdf->SetXY(15,180);
		$pdf->MultiCell(29,5,'GATO',1,1);

		$pdf->SetXY(44,180);
		$pdf->Cell(73,5,utf8_decode('Con capacidad para elevar el vehículo'),1,1,'C');

		$pdf->SetXY(117,180);
		if($gato==1){
			$pdf->Cell(25,5,'X',1,1,'C');
		}else{
			$pdf->Cell(25,5,'',1,1,'C');
		}

		$pdf->SetXY(142,180);
		if($gato==2){
			$pdf->Cell(25,5,'X',1,1,'C');
		}else{
			$pdf->Cell(25,5,'',1,1,'C');
		}

		$pdf->SetXY(167,180);
		$pdf->Cell(29,5,utf8_decode($observaciones_gato),1,1);

		$pdf->SetXY(15,185);
		$pdf->MultiCell(29,5,'CONOS',1,1);

		$pdf->SetXY(44,185);
		$pdf->Cell(73,5,utf8_decode('Dos conos con cinta reflectiva '),1,1,'C');

		$pdf->SetXY(117,185);
		if($conos==1){
			$pdf->Cell(25,5,'X',1,1,'C');
		}else{
			$pdf->Cell(25,5,'',1,1,'C');
		}

		$pdf->SetXY(142,185);
		if($conos==2){
			$pdf->Cell(25,5,'X',1,1,'C');
		}else{
			$pdf->Cell(25,5,'',1,1,'C');
		}

		$pdf->SetXY(167,185);
		$pdf->Cell(29,5,utf8_decode($observaciones_conos),1,1);

		$pdf->SetXY(15,190);
		$pdf->MultiCell(29,5,'TACOS',1,1);

		$pdf->SetXY(44,190);
		$pdf->Cell(73,5,utf8_decode('Dos tacos para bloquear el vehículo'),1,1,'C');

		$pdf->SetXY(117,190);
		if($conos==1){
			$pdf->Cell(25,5,'X',1,1,'C');
		}else{
			$pdf->Cell(25,5,'',1,1,'C');
		}

		$pdf->SetXY(142,190);
		if($conos==2){
			$pdf->Cell(25,5,'X',1,1,'C');
		}else{
			$pdf->Cell(25,5,'',1,1,'C');
		}	

		$pdf->SetXY(167,190);
		$pdf->Cell(29,5,utf8_decode($observaciones_tacos),1,1);	

		$pdf->SetXY(15,195);
		$pdf->Cell(29,10,utf8_decode('SEÑALES'),1,1);

		$pdf->SetXY(44,195);
		$pdf->MultiCell(73,3,utf8_decode('Rombos de seguridad, señal UN, banda amarilla con franjas negras, roja con franjas blancas de material reflectivo'),0,1);

		$pdf->SetXY(44,195);
		$pdf->Cell(73,10,utf8_decode(''),1,1);

		$pdf->SetXY(117,195);
		if($senales==1){
			$pdf->Cell(25,10,'X',1,1,'C');
		}else{
			$pdf->Cell(25,10,'',1,1,'C');
		}

		$pdf->SetXY(142,195);
		if($senales==2){
			$pdf->Cell(25,10,'X',1,1,'C');
		}else{
			$pdf->Cell(25,10,'',1,1,'C');
		}

		$pdf->SetXY(167,195);
		$pdf->Cell(29,10,utf8_decode($observaciones_senales),1,1);

		$pdf->SetXY(15,205);
		$pdf->MultiCell(29,5,'CHALECO',1,1);

		$pdf->SetXY(44,205);
		$pdf->Cell(73,5,utf8_decode('Debe ser reflectivo'),1,1,'C');

		$pdf->SetXY(117,205);
		if($chaleco==1){
			$pdf->Cell(25,5,'X',1,1,'C');
		}else{
			$pdf->Cell(25,5,'',1,1,'C');
		}

		$pdf->SetXY(142,205);
		if($chaleco==2){
			$pdf->Cell(25,5,'X',1,1,'C');
		}else{
			$pdf->Cell(25,5,'',1,1,'C');
		}

		$pdf->SetXY(167,205);
		$pdf->Cell(29,5,utf8_decode($observaciones_chaleco),1,1);

		$pdf->SetXY(15,210);
		$pdf->Cell(29,20,utf8_decode('BOTIQUIN'),1,1);

		$pdf->SetXY(44,210);
		$pdf->MultiCell(73,3,utf8_decode('Yodopividona solución antiséptico bolsa (120 ml), jabón, gasas, curas, venda elástica, rollo micropore, algodón, sales de rehidratación oral, baja lenguas, bolsa de suero fisiológico, guantes de látex desechables, toallas higiénicas, tijeras y termómetro oral'),0,1);

		$pdf->SetXY(44,210);
		$pdf->Cell(73,20,utf8_decode(''),1,1);

		$pdf->SetXY(117,210);
		if($botiquin==1){
			$pdf->Cell(25,20,'X',1,1,'C');
		}else{
			$pdf->Cell(25,20,'',1,1,'C');
		}

		$pdf->SetXY(142,210);
		if($botiquin==2){
			$pdf->Cell(25,20,'X',1,1,'C');
		}else{
			$pdf->Cell(25,20,'',1,1,'C');
		}

		$pdf->SetXY(167,210);
		$pdf->Cell(29,20,utf8_decode($observaciones_llave_fija),1,1);
			
//Datos de archivo de descarga
$file_name = str_replace(' ','_',$nombre_empresa_ajustado)."_Factura_No._".$num_factura.".pdf";
$file_destination = $path_pdf.$file_name;
$pdf->Output($file_destination,'F');
$pdf->Output($file_name,'I');
?>