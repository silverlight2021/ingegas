<?php
session_start();
date_default_timezone_set("America/Bogota");
set_time_limit(0);
require ("libraries/conexion.php");

if($_SESSION['logged']== 'yes')
{ 
	
	require_once("inc/init.php");
	require_once("inc/config.ui.php");
	$page_title = "Reportes";
	$page_css[] = "your_style.css";
	include("inc/header.php");
	$page_nav["rutero"]["sub"]["informes"]["active"] = true;
	include("inc/nav.php");

	$idUser1 = $_SESSION['su'];
	/*if(isset($_POST['submit_f1']))//Boton descargar excel
	{
		$fecha_inicio = $_POST['fecha_inicio'];
		$fecha_fin = $_POST['fecha_fin'];

		?>
        <script type="text/javascript">
            var fecha_inicio  = '<?php echo isset($fecha_inicio) ? $fecha_inicio : NULL; ?>';
            var fecha_fin  = '<?php echo isset($fecha_fin) ? $fecha_fin : NULL; ?>';
            var url = "descargar_excel/despachos/paso_1.php?fecha_inicio="+fecha_inicio+"&fecha_fin="+fecha_fin;
            window.open(url);
        </script>
        <?php
	}*/
	if(isset($_POST['submit_f3']))//Boton descargar excel Propietario
	{
		$fecha_inicio = $_POST['fecha_inicio'];
		$fecha_fin = $_POST['fecha_fin'];
		$numOrden = $_POST['numOrden'];

		?>
        <script type="text/javascript">
            var fecha_inicio  = '<?php echo isset($fecha_inicio) ? $fecha_inicio : NULL; ?>';
            var fecha_fin  = '<?php echo isset($fecha_fin) ? $fecha_fin : NULL; ?>';
            var numOrden  = '<?php echo isset($numOrden) ? $numOrden : NULL; ?>';
            var url = "descargar_excel/propietarios/reporte_despacho_propietario.php?fecha_inicio="+fecha_inicio+"&fecha_fin="+fecha_fin+"&numOrden="+numOrden;
            window.open(url);
        </script>
        <?php
	}
	?>
	<div id="main" role="main">
	<?php
		if (in_array(61, $acc))
		{
			?>
			<!-- MAIN CONTENT -->
			<div id="content">
				<section id="widget-grid" class="">
					<!-- START ROW -->
					<div class="row">
						<article class="col-md-6 col-md-offset-3">
							<div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
								<header>
									<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
									<h6>INFORME HOJA DE RUTA</h6>	
								</header>
								<div>
									<div class="widget-body no-padding">
										<form id="form_1" class="smart-form" name="form_1" novalidate="novalidate">
											<fieldset>
												<legend>Seleccione la fecha a consultar</legend>
												<div class="row">
													<section class="col col-6">
														<label class="font-lg">Fecha inicio:</label>
														<label class="input">
															<input type="date" name="datepicker_1" id="datepicker_1" placeholder="Fecha Inicial" class="input-md"/><i></i>
														</label>
													</section>
													<section class="col col-6">
														<label class="font-lg">Fecha fin:</label>
														<label class="input">
															<input type="date" name="datepicker_2" id="datepicker_2" placeholder="Fecha Final" class="input-md"/><i></i>
														</label>
													</section>
												</div>
											</fieldset>
											<footer>
												<div align="right">													
													<?php
													if (in_array(62, $acc))
													{
														?>
														<p id="cargando" style="font-size: 24px; color: green"></p>
														<input type="submit" name="submit_f1" id="submit_f1" value="DESCARGAR INFORME" class="btn btn-primary">
														<?php
													}
													?>													
												</div>
											</footer>
										</form>
									</div>
								</div>
							</div>
						</article>
					</div>
				</section>
			</div>

			<?php
		}


		?>		
	</div>
	<?php

		include("inc/footer.php");
		include("inc/scripts.php"); 
	?>

	<style type="text/css">
		

		.table {
		    max-width:980px;
		    table-layout:fixed;
		    margin:auto;
		}
		.table-scroll {
		    position:relative;
		    max-width:1024px;		    
		    margin:auto;
		    overflow:hidden;
		    border:1px solid #000;
		}
		.table-wrap {
		    width:100%;
		    overflow:auto;
		}
		.table-scroll table {
		    width:100%;
		    margin:auto;
		    border-collapse:separate;
		    border-spacing:0;
		}
		.table-scroll th, .table-scroll td {
		    padding:5px 10px;
		    border:1px solid #000;
		    background:#fff;
		    white-space:nowrap;
		    vertical-align:top;
		}
		.table-scroll thead, .table-scroll tfoot {
		    background:#f9f9f9;
		}
		.clone {
		    position:absolute;
		    top:0;
		    left:0;
		    pointer-events:none;
		}
		.clone th, .clone td {
		    visibility:hidden
		}
		.clone td, .clone th {
		    border-color:transparent
		}
		.clone tbody th {
		    visibility:visible;
		    color:red;
		}
		.clone .fixed-side {
		    border:1px solid #000;
		    background:#eee;
		    visibility:visible;
		}
		.clone thead, .clone tfoot{background:transparent;}

	</style>

	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/js/bootstrap.js" data-modules="effect effect-bounce effect-blind effect-bounce effect-clip effect-drop effect-fold effect-slide"></script>
	<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
	<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
	<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
	<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>
	<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
	<script src="js/plugin/moment/moment.min.js"></script>
	

	<script type="text/javascript">
		//Script para la consulta y descarga del archivo Excel de reportes
		//$(document).ready(function() {
		$("#submit_f1").click(function(e){ 
			e.preventDefault();
			var fecha_inicio = document.getElementById("datepicker_1").value;
			var fecha_fin = document.getElementById("datepicker_2").value;
			if(fecha_inicio == "" || fecha_fin == "")
			{
				$("#cargando").html("Por favor seleccione una fecha válida.");
			}
			else
			{
				$.ajax({
					type: 'POST',
					data: {	fecha_inicio: fecha_inicio,
							fecha_fin : fecha_fin },
					beforeSend: function () {
	                    $("#cargando").html("Procesando solicitud, espere por favor...");
	           		},
					success: function(result_1){
						if (result_1 == 1) {
							$("#cargando").html("No existen datos para la fecha seleccionada.");
						}
						else
						{
							if (result_1 == 2) {
								$("#cargando").html("No existen datos para la fecha seleccionada.");
							}
							else
							{
								$("#cargando").html("El archivo comenzará a descargarse.");
								var url = "descargar_excel/hoja_ruta/informe_hoja_ruta_excel.php?fecha_inicio="+ fecha_inicio +"&fecha_fin="+fecha_fin;
				        		window.open(url);
							}
						}							    		
						
				    },
				    error: function(){
				        window.alert("Wrong query 'queryDB.php': ");
				    }
				});			
			}			

		});			

	</script>
	<?php 
	include("inc/google-analytics.php"); 

}
else
{
	header("Location:index.php");
}
?>
