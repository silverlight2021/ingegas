<?php
date_default_timezone_set("America/Bogota");
require ("libraries/conexion.php");
require ("libraries/seguimiento_pev.php");
session_start();

if(@$_SESSION['logged']== 'yes')
{
  	$id_transporte_pev = isset($_REQUEST['id_transporte_pev']) ? $_REQUEST['id_transporte_pev'] : NULL;
    $id_user =$_SESSION['su'];
    $acc = $_SESSION['acc'];
    $id_estado_transporte_pev = 1;/////id transporte
    $id_finalizar = 1; //transporte abierto, si es dos, transporte cerrado
    $id_asignar_cilindros = 0; //si el transporte no se guarda, entonces no se pueden agregar cilindros a la CME. En 1 si se permite. Cero por defecto.
    $tipo = 2; //Se establece tipo de cilindro para PEV. ETO es 1
    $time = time();
    $user_agent = $_SERVER['HTTP_USER_AGENT'];
    function getPlatform($user_agent) {
	   	$plataformas = array(
	    	'Windows 10' => 'Windows NT 10.0+',
	      	'Windows 8.1' => 'Windows NT 6.3+',
	      	'Windows 8' => 'Windows NT 6.2+',
	      	'Windows 7' => 'Windows NT 6.1+',
	      	'Windows Vista' => 'Windows NT 6.0+',
	      	'Windows XP' => 'Windows NT 5.1+',
	      	'Windows 2003' => 'Windows NT 5.2+',
	      	'Windows' => 'Windows otros',
	      	'iPhone' => 'iPhone',
	      	'iPad' => 'iPad',
	      	'Mac OS X' => '(Mac OS X+)|(CFNetwork+)',
	      	'Mac otros' => 'Macintosh',
	      	'Android' => 'Android',
	      	'BlackBerry' => 'BlackBerry',
	      	'Linux' => 'Linux',
	   	);
	   	foreach($plataformas as $plataforma=>$pattern){
			if (preg_match('/'.$pattern.'/', $user_agent))
			return $plataforma;
	    }
	   	return 'Otras';
	}

	$SO = getPlatform($user_agent);

	if(isset($_GET['id_cliente'])) //SE CREA TRANSPORTE EN BASE DE DATOS
	{
		$id_cliente = $_GET['id_cliente'];
	    
	    $consulta  = "INSERT INTO transporte_pev (id_cliente,fecha,hora,id_user,id_estado_transporte_pev,id_finalizar,id_asignar_cilindros) 
	        		  VALUES ('".$id_cliente."', '".date("Y/m/d")."', '".date("H:i:s")."','".$id_user."',
	        		  		  '".$id_estado_transporte_pev."','".$id_finalizar."','".$id_asignar_cilindros."')";
	    $resultado = mysqli_query($con,$consulta) ;
	    if ($resultado == FALSE)
	    {
	     	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
	    }
	    else
	    {
			$id_transporte1 = mysqli_insert_id($con);
			?>
	  		<script type="text/javascript">
	  			var js_id_transporte_pev  = '<?php echo $id_transporte1;?>';
	  			window.location = ("transporte_pev.php?id_transporte_pev="+js_id_transporte_pev);
	  		</script>
	  		<?php
	    }
	}
	if(isset($_POST['guardar_movimiento'])) // SE GUARDAN DATOS DEL TRANSPORTE Y SE ACTUALIZAN
	{	
	    $id_placa_camion = $_POST['id_placa_camion1'];
	    $num_trans = $_POST['num_trans'] ;
	    $obs_trans = $_POST['obs_trans'] ;
	    $placa_otro = $_POST['placa_otro'];
	    $transportador_otro = $_POST['transportador_otro'];
	    $id_estado_transporte_pev = 2; // Se pueden agregar cilindros
	    $documento_transportador = $_POST['documento_transportador'];

	    if(strlen($id_transporte_pev) > 0)
		{
		    $consulta= "UPDATE transporte_pev SET 
		                id_placa_camion = '".$id_placa_camion."',
		                num_trans = '".$num_trans."',
		                obs_trans = '".$obs_trans."',
		                placa_otro = '".$placa_otro."',
		                transportador_otro = '".$transportador_otro."',
		                id_estado_transporte_pev = '".$id_estado_transporte_pev."',
		                documento_transportador = '".$documento_transportador."'
		                WHERE id_transporte_pev = '".$id_transporte_pev."'";
		    $resultado = mysqli_query($con,$consulta) ;

		    if ($resultado == FALSE)
		    {
		      	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    }
		    else
		    {
		    	$mostrar_cilindros = 1;
		    	?>
		  		<script type="text/javascript">
		  			alert("Transporte guardado con éxito. Ahora puede agregar cilindros.")
		  			var js_id_transporte_pev  = '<?php echo $id_transporte_pev;?>';
		  			window.location = ("transporte_pev.php?id_transporte_pev="+js_id_transporte_pev);
		  		</script>
		  		<?php
		    }
	    }  
	}
	if(strlen($id_transporte_pev) > 0) //Se obtienen datos de transporte para mostrar en formulario
	{ 
	  	$consulta  = "SELECT * FROM transporte_pev WHERE id_transporte_pev = '".$id_transporte_pev."'";
	  	$resultado = mysqli_query($con,$consulta) ;
	  	if(mysqli_num_rows($resultado) > 0)
	  	{
	  		$linea = mysqli_fetch_assoc($resultado);
		  	$id_cliente = isset($linea["id_cliente"]) ? $linea["id_cliente"] : NULL;  
		  	$id_estado_transporte_pev = isset($linea["id_estado_transporte_pev"]) ? $linea["id_estado_transporte_pev"] : NULL;  
			$fecha = isset($linea["fecha"]) ? $linea["fecha"] : NULL;
			$hora = isset($linea["hora"]) ? $linea["hora"] : NULL;
			$num_trans = isset($linea["num_trans"]) ? $linea["num_trans"] : NULL;
			$obs_trans = isset($linea["obs_trans"]) ? $linea["obs_trans"] : NULL;
			$id_placa_camion1 = isset($linea["id_placa_camion"]) ? $linea["id_placa_camion"] : NULL;
			$placa_otro = isset($linea["placa_otro"]) ? $linea["placa_otro"] : NULL;
			$transportador_otro = isset($linea["transportador_otro"]) ? $linea["transportador_otro"] : NULL;
			$documento_transportador = isset($linea["documento_transportador"]) ? $linea["documento_transportador"] : NULL;
			$id_finalizar = isset($linea["id_finalizar"]) ? $linea["id_finalizar"] : NULL;
	  		$id_user = $linea["id_user"];
		  	$sql = "SELECT * FROM user WHERE idUser = '".$id_user."'";
		    $sql_res = mysqli_query($con,$sql) ;
		    if(mysqli_num_rows($sql_res) > 0)
		    {
		    	$sql_row = mysqli_fetch_assoc($sql_res);
		    	$Name = $sql_row["Name"];
		    	$LastName = $sql_row["LastName"];
		    	$usuario = $Name." ".$LastName;
		    }
		    $sql1 = "SELECT * FROM placa_camion WHERE id_placa_camion = $id_placa_camion1";
		    $sql_res1 = mysqli_query($con,$sql1) ;
		    if(mysqli_num_rows($sql_res1) > 0)
		    {
		    	$sql_row1 = mysqli_fetch_assoc($sql_res1);
		    	$placa = $sql_row1["placa_camion"];
		    }
		    $consulta1  = "SELECT nombre FROM clientes WHERE id_cliente= $id_cliente";
	  		$resultado1 = mysqli_query($con,$consulta1) ;
	  		if(mysqli_num_rows($resultado1) > 0)
	  		{
	  			$linea1 = mysqli_fetch_assoc($resultado1);
			  	$nombre = isset($linea1["nombre"]) ? $linea1["nombre"] : NULL;
			  	$nit1 = isset($linea1["nit"]) ? $linea1["nit"] : NULL;
	  		}
	  	}
	}
	//Se agregan cilindros a la orden de transporte
	if(isset($_POST['agregar_cilindro']))
	{
		$id_cliente2 = $_POST['id_cliente2'];
		
		$id_movimiento_pev = $_POST['id_transporte_pev'];
		$consulta  = "SELECT id_cliente FROM transporte_pev WHERE id_transporte_pev = '".$id_transporte_pev."'";
	  	$resultado = mysqli_query($con,$consulta);
	  	if(mysqli_num_rows($resultado) > 0)
	  	{
	  		$linea = mysqli_fetch_assoc($resultado);
	  		$id_cliente = $linea["id_cliente"];
	  		if ($id_cliente2 == "")
			{
				$id_cliente2 = $id_cliente;
			}
	  	}
	  	
		$num_cili = $_POST['num_cili'];
		$tapa = $_POST['tapa'];
		$tipo_gas_pev = $_POST['tipo_gas_pev'];
		
		$consulta2  = "SELECT * FROM cilindro_pev 
	               	   WHERE num_cili_pev = '".$num_cili."' AND tipo = '".$tipo."'";
		$resultado2 = mysqli_query($con,$consulta2) ;
		if(mysqli_num_rows($resultado2) > 0)
		{
			$consulta3  = "SELECT * FROM has_movimiento_cilindro_pev 
		               	   WHERE id_transporte_pev = '".$id_transporte_pev."' AND num_cili = '".$num_cili."'";
			$resultado3 = mysqli_query($con,$consulta3) ;
			if(mysqli_num_rows($resultado3) > 0)
			{
				?>
				<script type="text/javascript">
					alert("El cilindro ingresado ya existe en esta CME. Intente nuevamente.");
					var js_id_transporte_pev  = '<?php echo $id_transporte_pev;?>';
					window.location = ("transporte_pev.php?id_transporte_pev="+js_id_transporte_pev);
				</script>
				<?php
			}
			else
			{
				$consulta = "INSERT INTO has_movimiento_cilindro_pev (id_transporte_pev, num_cili, id_tapa, id_tipo_gas_pev, id_cliente) 
					 	 	 VALUES ('".$id_transporte_pev."', '".$num_cili."', '".$tapa."' , '".$tipo_gas_pev."', '".$id_cliente2."')";
				if(mysqli_query($con,$consulta) == TRUE)
				{
					$id_cliente2 = "";
					/*$id_has_movimiento_cilindro_pev_1 = mysqli_insert_id($con);
					seguimiento_pev(8,$);*/
					?>
			  		<script type="text/javascript">
			  			var js_id_transporte_pev  = '<?php echo $id_transporte_pev;?>';
			  			window.location = ("transporte_pev.php?id_transporte_pev="+js_id_transporte_pev);
			  		</script>
			  		<?php
				}
				else
				{
					echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
				}
			}
		}
		else
		{
			$consulta = "INSERT INTO cilindro_pev (num_cili_pev, id_estado, id_tipo_gas_pev, id_tapa, id_cliente, tipo, fech_crea) 
						 VALUES ('".$num_cili."', '1','".$tipo_gas_pev."', '".$tapa."', '".$id_cliente2."', '".$tipo."', '".$fecha."')";
			if(mysqli_query($con,$consulta))
			{
				$id_cilindro_pev_1 = mysqli_insert_id($con);
				$consulta3  = "SELECT * FROM has_movimiento_cilindro_pev 
		               	   WHERE id_transporte_pev = '".$id_transporte_pev."' AND num_cili = '".$num_cili."'";
				$resultado3 = mysqli_query($con,$consulta3) ;
				if(mysqli_num_rows($resultado3) > 0)
				{

					?>
					<script type="text/javascript">
						alert("El cilindro ingresado ya existe en esta CME. Intente nuevamente.");
						var js_id_transporte_pev  = '<?php echo $id_transporte_pev;?>';
						window.location = ("transporte_pev.php?id_transporte_pev="+js_id_transporte_pev);
					</script>
					<?php
				}
				else
				{
					$consulta = "INSERT INTO has_movimiento_cilindro_pev (id_transporte_pev, num_cili, id_tapa, id_tipo_gas_pev, id_cliente) 
						 	 	 VALUES ('".$id_transporte_pev."', '".$num_cili."', '".$tapa."', '".$tipo_gas_pev."', '".$id_cliente2."')";
					if(mysqli_query($con,$consulta) == TRUE)
					{
						$id_has_movimiento_cilindro_pev_1 = mysqli_insert_id($con);
						seguimiento_pev(8,$id_cilindro_pev_1,$id_user,6,$id_has_movimiento_cilindro_pev_1);
						$id_cliente2 = "";
						?>
				  		<script type="text/javascript">
				  			var js_id_transporte_pev  = '<?php echo $id_transporte_pev;?>';
				  			window.location = ("transporte_pev.php?id_transporte_pev="+js_id_transporte_pev);
				  		</script>
				  		<?php
					}
					else
					{
						echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
					}
				}
			}
			else
			{
				echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
			}
		}	
	}

require_once("inc/init.php");
require_once("inc/config.ui.php");
$page_title = "Control Transporte";
$page_css[] = "your_style.css";
include("inc/header.php");
$page_nav["pev"]["sub"]["transporte"]["active"] = true;
include("inc/nav.php");	

?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
	<div id="content">	
	<?php
	if ($id_transporte_pev == "") 
	{		
	?>		   
		<div class="">	      	
			<h1  class="page-title txt-color-blueDark"> Seleccione el Cliente :</h1>			
		</div>
		<!-- widget grid -->       
		<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">			
			<header>
				<span class="widget-icon"> <i class="fa fa-table"></i> </span>
				<h6>Clientes</h6>
			</header>	
			<div>
				<div class="jarviswidget-editbox"></div>
				<div class="widget-body no-padding">
					<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">						
						<form  method="POST" name="form1">							
							<thead>
								<tr>
									<th>#</th>
									<th>Acción</th>
									<th>Cliente</th>
									<th>Nit</th>
									<th>Telefono</th>
									<th>Celular</th>
									<th>E-Mail</th>
									<th>Direccion</th>
									<th>Observacion</th>
								</tr>
							</thead>
							<tbody>										
							<?php
                           
                    			$contador = "0";
                    	        $consulta = "SELECT * FROM clientes ";
                                $resultado = mysqli_query($con,$consulta) ;
                                while ($linea = mysqli_fetch_array($resultado))
                                {
                                    $contador = $contador + 1;
                                    $id_cliente = $linea["id_cliente"];
                                    $nombre = $linea["nombre"];
                                    $telefono_fijo = $linea["telefono_fijo"];
                                    $telefono_celular = $linea["telefono_celular"];
                                    $correo = $linea["correo"];
                                    $persona_contacto = $linea["persona_contacto"];
                                    $direccion = $linea["direccion"];
                                    $observacion = $linea["observacion"];
                                    $nit = $linea["nit"];
                                	?>
                                    <tr>
                                        <td width="5"><?php echo $contador; ?></td>
                                        <td width="5"><a href="transporte_pev.php?id_cliente=<?php echo $id_cliente; ?>"><img src="img/chulo.png" width="20" height="20"></a></td>
                                        <td><?php echo $nombre; ?></td>
                                        <td><?php echo $nit; ?></td>
                                        <td><?php echo $telefono_fijo; ?></td>
                                        <td><?php echo $telefono_celular; ?></td>
                                        <td><?php echo $correo; ?></td>
                                        <td><?php echo $direccion; ?></td>
                                        <td><?php echo $observacion; ?></td>
                                	</tr>   
                                <?php
                                }mysqli_free_result($resultado);   
                            	
                            ?>
							</tbody>
						</form>
					</table>
				</div>
			</div>
		</div>
	<?php
	}
	if ($id_transporte_pev > 0)  
	{		
	?>
		<section id="widget-grid" class="">
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-6">
					<div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h6>Cliente </h6>				
						</header>
						<div>				
						<div class="jarviswidget-editbox"></div>
							<div class="widget-body no-padding">
							<form id="checkout-form" class="smart-form" novalidate="novalidate" action="transporte_pev.php" method="POST">
							<input type="hidden" name="id_transporte_pev" id="id_transporte_pev" value="<?php echo $id_transporte_pev; ?>">
								<fieldset>
									<div class="row">
										<section class="col col-8">
											<label class="font-lg">Cliente :</label>
											<label class="input"> 
												<input type="text" class="input-lg" name="nombre" readonly placeholder="Nombre" value="<?php echo isset($nombre) ? $nombre : NULL; ?>">
											</label>
										</section>
										<section class="col col-4">
											<label class="font-lg">CME :</label>
											<label class="input"> 
												<input type="text" class="input-lg" name="num_trans" placeholder="CME" value="<?php echo isset($num_trans) ? $num_trans : NULL; ?>">
											</label>
										</section>
									</div>	
									<div class="row">
										<section class="col col-4">           
				                        
					                        
					                        <section>
					                        <label class='font-lg'>Placa Camión :</label>
					                        <label class='select'>
					                        <select class='input-lg' name='id_placa_camion1' id='id_placa_camion1' onchange='habilitar();'>
					                        <option value=''>Seleccione...</option>
					                        <?php
					                        $consulta6 ="SELECT * FROM placa_camion";
					                        $resultado6 = mysqli_query($con,$consulta6);
					                        while($linea6 = mysqli_fetch_array($resultado6))
					                        {
					                          	$id_placa_camion = $linea6['id_placa_camion'];
					                          	$placa_camion = $linea6['placa_camion'];
					                          	if ($id_placa_camion == $id_placa_camion1)
					                          	{
					                            	echo "<option value='$id_placa_camion' selected >$placa_camion</option>"; 
					                          	}
					                          	else 
					                          	{
					                            	echo "<option value='$id_placa_camion'>$placa_camion</option>"; 
					                          	} 
					                        }//fin while
					                        ?>  
					                        </select>
					                        <i></i>
					                        </label>
					                        </section>      
					                                               
					                    </section> 
				                      	<section class="col col-4">
											<label class="font-lg">Fecha :</label>
											<label class="input"> 
												<input type="text" class="input-lg" name="fecha" readonly placeholder="Fecha" value="<?php echo isset($fecha) ? $fecha : NULL; ?>">
											</label>
										</section>
										<section class="col col-4">
											<label class="font-lg">Hora:</label>
											<label class="input">
												<input type="text" name="hora" class="input-lg" placeholder="Hora" value="<?php echo isset($hora) ? $hora : NULL; ?>" readonly>	
											</label>
										</section>													
									</div>
									<fieldset>
										<legend>Datos de recibido en Planta:</legend>
										<div class="row">
											<section class="col col-6">
												<label class="font-lg">Placa Vehículo:</label>
												<label class="input">
													<input type="text" name="placa_otro" class="input-lg" placeholder="Placa" id="placa_otro" value="<?php echo isset($placa_otro) ? $placa_otro : NULL; ?>">	
												</label>
											</section>
											<section class="col col-6">
												<label class="font-lg">Transportador Vehículo:</label>
												<label class="input"> 
													<input type="text" name="transportador_otro" class="input-lg" placeholder="Transportador" id="transportador_otro" value="<?php echo isset($transportador_otro) ? $transportador_otro : NULL; ?>">
												</label>
											</section>	
										</div>
										<div class="row">
											<section class="col col-6">
												<label class="font-lg">Documento Transportador:</label>
												<label class="input"> 
													<input type="text" name="documento_transportador" class="input-lg" placeholder="Documento Transportador" id="documento_transportador" value="<?php echo isset($documento_transportador) ? $documento_transportador : NULL; ?>">
												</label>
											</section>	
										</div>
									</fieldset>
									
									<div class="row">
										<section class="col col-6">
											<label class="font-lg">Usuario:</label>
											<label class="input">
												<input type="text" name="usuario" class="input-lg" placeholder="Usuario" value="<?php echo isset($usuario) ? $usuario : NULL; ?>" readonly>	
											</label>
										</section>
										<section class="col col-6">
											<label class="font-lg">Observaciones:</label>
											<label class="input"> 
												<textarea class="form-control" rows="3" name="obs_trans"><?php echo isset($obs_trans) ? $obs_trans : NULL; ?></textarea>
											</label>
										</section>	
									</div>
								</fieldset>														
								<footer>
										<?php
										if($SO=="Android")
										{
											if($id_estado_transporte_pev == 3)
											{	
											?>
												<a href="com.fidelier.printfromweb://
												$biguhw$INGEGAS-INGENIERIA Y GASES LTDA$intro$
												$small$------------------------------------------$intro$
												$small$Fecha   : <?echo date("d-m-Y (H:i:s)", $time); ?>$intro$
												$small$Cliente : <? echo $nombre;?>$intro$
												$small$Nit     : <? echo $nit1;?>$intro$
												$small$Usuario : <?=@$_SESSION['name']?>$intro$
												$small$Obs     : <? echo $obs_trans;?>$intro$										
												$small$------------------------------------------$intro$
												$big$#  Cilindro$intro$
												$small$------------------------------------------$intro$
												$small$<?php
												          	$contador = "0";	                
												          	$consulta = "SELECT * FROM has_movimiento_cilindro_pev WHERE id_transporte_pev = $id_transporte_pev";
												          	$resultado = mysqli_query($con,$consulta) ;
												          	$aux="";
												          	while ($linea = mysqli_fetch_array($resultado))
												          	{
													            $contador = $contador + 1;
													            $num_cili = $linea["num_cili"];
												    	        $id_has_movimiento_cilindro_pev = $linea["id_has_movimiento_cilindro_pev"];
												    	        $id_cliente = $linea['id_cliente'];
													            
													            $consulta1  = "SELECT id_cliente,nombre,nit FROM clientes WHERE id_cliente= $id_cliente";
															  	$resultado1 = mysqli_query($con,$consulta1) ;
															  	if(mysqli_num_rows($resultado1) > 0 )
															  	{
															  		$linea1 = mysqli_fetch_array($resultado1);
																  	$nombre = isset($linea1["nombre"]) ? $linea1["nombre"] : NULL;
																  	$nit1 = isset($linea1["nit"]) ? $linea1["nit"] : NULL; 	
															  	}                           
													            echo $contador.")  ".$num_cili;?>$intro$$small$<?php
															}mysqli_free_result($resultado);	                                            
														?>$intro$
												$small$------------------------------------------$intro$
												$small$Aprobado Por$intro$
												$intro$
												$intro$
												$intro$
												$small$------------------------------------------$intro$
												$small$Firma
												$intro$
												$intro$



												$small$ Observaciones: <?php echo $obs_trasn; ?></P>
														
														Placa del vehículo:
															<?php if($id_placa_camion1 == 6)
															{
																echo $placa_otro;
															}
															else
															{	
																echo $placa;
															}
															?>
														Transportador:
														 	<?php 
														 	if($id_placa_camion1 == 6 )
														 	{
														 		echo $transportador_otro;
														 	}
														 	else
														 	{
														 		echo $usuario;
														 	}
														 	?>
												 		?>
												$small$Sujeto Auditoria Y Verificacion $intro$
												$cut$$intro$$intro$$intro$"><img src="/img/iconos/printer_black.png"></a>
											<?php
											}
										}
										if ($SO!="Android") 
										{
											if($id_estado_transporte_pev == 3)
											{
											?>
												<a href="javascript:imprSelec('muestra')"><img src="/img/iconos/printer_blue.png"></a>
											<?php
											}
										}
										if($id_estado_transporte_pev == 1)	
										{
										?>														
											<input type="submit" value="Guardar Transporte" name="guardar_movimiento" id="guardar_movimiento" class="btn btn-primary" />
										<?php				
										}
										if ($id_estado_transporte_pev == 2) 
										{			
										?>
											<input type="button" onclick="validar_tr()" value="Cerrar Transporte" class="btn btn-danger btn-lg" >
										<?php				
										}											
										?>	
								</footer>	
							</form>	
							</div>						
						</div>					
					</div>
					<?php
					if ($id_finalizar == 1)
					{
						if($id_estado_transporte_pev == 2)
						{
						?>
						<div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
							<header>
								<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
								<h6>Cilindros y equipos recibidos del cliente</h6>				
							</header>
							<div>				
								<div class="jarviswidget-editbox"></div>
								<div class="widget-body no-padding">
									<form id="checkout-form1" class="smart-form" action="transporte_pev.php" method="POST">
										<input type="hidden" name="id_transporte_pev" id="id_transporte_pev" value="<?php echo $id_transporte_pev; ?>">
										<input type="hidden" name="id_cliente1" id="id_cliente1" value="<?php echo $id_cliente1 ?>">
										<input type="hidden" name="id_cliente2" id="id_cliente2" value="<?php echo $id_cliente2; ?>">
										<fieldset>
											<div class="row">
												<section class="col col-4">
													<label class="font-lg">No. Serial</label>
													<label class="input">
														<input type="text" name="num_cili" class="input-lg" required="required">
													</label>
												</section>
												<section class="col col-6">
													<label class="font-lg">Tipo de gas:</label>
													<?php
														$consulta1 ="SELECT * FROM tipo_gas_pev ORDER BY nombre ASC";
							                            $resultado1 = mysqli_query($con,$consulta1);
														echo "<label class='select'>";
														echo "<select name='tipo_gas_pev' class='input-lg' id='tipo_gas_pev'>";
														echo "<option value=''>$nombre_gas</option>";
														while($linea1 = mysqli_fetch_array($resultado1))
					                                    {						                                      
					                                      	$nombre_gas = $linea1['nombre'];
					                                      	$id_tipo_gas_pev = $linea1['id_tipo_gas_pev'];
					                                      	echo "<option value='$id_tipo_gas_pev'>$nombre_gas</option>";
					                                    }//fin while
														echo "</select>";
														echo "<i></i>";
														echo "</label>";
													?>
												</section>
												<section class="col col-2">
													<label class="font-lg">Tapa:</label>
													<?php
														$consulta1 ="SELECT * FROM tapa";
							                            $resultado1 = mysqli_query($con,$consulta1);
														echo "<label class='select'>";
														echo "<select name='tapa' class='input-lg' id='tapa'>";
														echo "<option value=''>$tapa</option>";
														while($linea1 = mysqli_fetch_array($resultado1))
					                                    {						                                      
					                                      	$tapa = $linea1['tapa'];
					                                      	$id_tapa = $linea1['id_tapa'];
					                                      	echo "<option value='$id_tapa'>$tapa</option>";
					                                    }//fin while
														echo "</select>";
														echo "<i></i>";
														echo "</label>";
													?>
												</section>
											</div>
											<div class="row">
												<section class="col col-12">
													<label class="font-lg">Propiedad:</label>
													<div class="input-group">
														<label class="input"><input type="text" name="nombre2" class="input-lg" value="<?php echo $nombre; ?>">
														</label>
														<span class="input-group-btn">
															<button type="button" name="cambiar_cliente" class="btn btn-warning btn-lg" data-toggle="modal" data-target="#modal_cliente">Cambiar Cliente</button>
														</span>
													</div>
												</section>
											</div>
										</fieldset>
										<footer>
											<input type="submit" name="agregar_cilindro" class="btn btn-primary" value="Agregar Cilindro">
										</footer>
									</form>
								</div>
							</div>
						</div>
						<?php
						}
					}
					?>
				</article>
				
				<article class="col-sm-12 col-md-12 col-lg-6">
					<div class="jarviswidget" id="wi6-id-3" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h6>Cilindros Transportados </h6>				
						</header>
						<div>				
						<div class="jarviswidget-editbox"></div>																	
							<table class="table tabl6-bordered">
								<thead>
									<tr>
										<th>#</th>                                                
			                            <th>Cilindro</th>
			                            <th>Tapa</th>
			                            <th>Tipo Gas</th>
			                            <th>Cliente</th>
			                            <?php				
										if ($id_estado_transporte_pev == 2) 
										{			
											?>
				                            <th>Eliminar</th>
				                            <?php				
										}											
										?>
									</tr>
								</thead>
								<tbody>													
								  <?php
		                          $contador = "0";	                
		                          $consulta = "SELECT * FROM has_movimiento_cilindro_pev WHERE id_transporte_pev =".$id_transporte_pev;
		                          $resultado = mysqli_query($con,$consulta) ;
		                          while ($linea = mysqli_fetch_array($resultado))
		                          {
		                            $contador = $contador + 1;
		                            $id_has_movimiento_cilindro_pev = $linea["id_has_movimiento_cilindro_pev"];
		                            $id_movimiento_pev = $linea["id_movimiento_pev"];
		                            $num_cili = $linea["num_cili"];
		                            $id_tapa = $linea["id_tapa"];
		                            $id_tipo_gas_pev = $linea["id_tipo_gas_pev"];
		                            $id_cliente = $linea["id_cliente"];


		                            $consulta1 = "SELECT tapa FROM tapa WHERE id_tapa = $id_tapa";
		                            $resultado1 = mysqli_query($con,$consulta1) ;
		                            while ($linea1 = mysqli_fetch_array($resultado1))
		                            {
		                            	$tapa = $linea1["tapa"];
		                            }
		                            $consulta2 = "SELECT nombre FROM tipo_gas_pev WHERE id_tipo_gas_pev = $id_tipo_gas_pev";
		                            $resultado2 = mysqli_query($con,$consulta2) ;
		                            while ($linea2 = mysqli_fetch_array($resultado2))
		                            {
		                            	$nombre_gas = $linea2["nombre"];
		                            }
		                            $consulta3 = "SELECT nombre FROM clientes WHERE id_cliente = $id_cliente";
		                            $resultado3 = mysqli_query($con,$consulta3) ;
		                            while ($linea3 = mysqli_fetch_array($resultado3))
		                            {
		                            	$nombre_cliente = $linea3["nombre"];
		                            }
		                            ?>
		                            <tr class="odd gradeX">
		                              	<td width="40"><?php echo $contador; ?></td>                                  
		                              	<td><?php echo $num_cili; ?></td> 
		                              	<td><?php echo $tapa; ?></td> 
		                              	<td><?php echo $nombre_gas; ?></td>
		                              	<td><?php echo $nombre_cliente; ?></td>
		                              	<?php				
										if ($id_estado_transporte_pev == 2) 
										{			
										?>
		                              	<td width="100" style="vertical-align:bottom;">			                              	
		                              		<a href="eliminar_has_movi_cili_pev.php?id_has_movimiento_cilindro_pev=<?php echo $id_has_movimiento_cilindro_pev; ?>&id_transporte_pev=<?php echo $id_transporte_pev; ?>"><input type="image"  src="img/eliminar.png" width="35" height="35"></a>			                              
		                              	</td> 
		                              	<?php				
										}											
										?>                            
		                           	</tr>                           
		                            <?php
		                            }mysqli_free_result($resultado);	                                            
		                  		    ?>
								</tbody>
							</table>												
						</div>					
					</div>				
				</article>				
			</div>
		</section>
		<div id="muestra" style="display:none">
		<p>INGEGAS-INGENIERIA Y GASES LTDA</p>
		<?php
			$consulta1  = "SELECT id_cliente,nombre,nit FROM clientes WHERE id_cliente= '".$id_cliente."'";
		  	$resultado1 = mysqli_query($con,$consulta1) ;
		  	if(mysqli_num_rows($resultado1) > 0 )
		  	{
		  		$linea1 = mysqli_fetch_array($resultado1);
			  	$nombre = isset($linea1["nombre"]) ? $linea1["nombre"] : NULL;
			  	$nit1 = isset($linea1["nit"]) ? $linea1["nit"] : NULL; 	
		  	}
		?>
		<p><h2 align="center"><? echo $nombre;?></h2>
		<h2 align="center">NIT :<? echo $nit1;?></h2>
		<h2 align="center">CME: <?php echo $num_trans; ?></h2></p>
		<br>
		<h2 align="center">CILINDROS TRANSPORTADOS</h2>
			<table class="table table-bordered" style="border: 1px solid black;border-collapse: collapse;" align="center">	
				<thead>
					<tr>
						<th style="border: 1px solid black;border-collapse: collapse;">#</th>                                                
		                <th style="border: 1px solid black;border-collapse: collapse;">Cilindro</th>			                            
		                <th style="border: 1px solid black;border-collapse: collapse;">Cliente</th>
					</tr>
				</thead>
			<tbody>													
				<?php
	          	$contador = "0";	                
	          	$consulta = "SELECT * FROM has_movimiento_cilindro_pev WHERE id_transporte_pev =".$id_transporte_pev;
	          	$resultado = mysqli_query($con,$consulta) ;
	          	while ($linea = mysqli_fetch_array($resultado))
	          	{
		            $contador = $contador + 1;
		            $num_cili = $linea["num_cili"];
	    	        $id_has_movimiento_cilindro_pev = $linea["id_has_movimiento_cilindro_pev"];
	    	        $id_cliente = $linea['id_cliente'];

	    	        $consulta1  = "SELECT id_cliente,nombre,nit FROM clientes WHERE id_cliente= $id_cliente";
				  	$resultado1 = mysqli_query($con,$consulta1) ;
				  	if(mysqli_num_rows($resultado1) > 0 )
				  	{
				  		$linea1 = mysqli_fetch_array($resultado1);
					  	$nombre = isset($linea1["nombre"]) ? $linea1["nombre"] : NULL;
					  	$nit1 = isset($linea1["nit"]) ? $linea1["nit"] : NULL; 	
				  	}
				  	
		            
		            ?>
		            <tr class="odd gradeX">
		              	<td width="40" style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $contador; ?></td>
		              	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $num_cili; ?></td>
		              	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $nombre; ?></td>
		           	</tr>                           
		            <?php
	            }mysqli_free_result($resultado);	                                            
	  		    ?>
			</tbody>
			</table>
			<br>
			<P>Observaciones: <?php echo $obs_trasn; ?></P>
			<br>
			<p>Placa del vehículo:
				<?php if($id_placa_camion1 == 6)
				{
					echo $placa_otro;
				}
				else
				{	
					echo $placa;
				}
				?>
			</p>
			<p>Transportador:
			 	<?php 
			 	if($id_placa_camion1 == 6 )
			 	{
			 		echo $transportador_otro;
			 	}
			 	else
			 	{
			 		echo $usuario;
			 	}
			 	?></p>
			<br>
			<br>
			<h6>Sujeto Auditoria Y Verificacion</h6>
			<p>Fecha elaboración :<?echo date("d-m-Y (H:i:s)", $time); ?></p>			
		</div>
		<!-- Modal para la seleccion del cliente en caso de querer cambiarlo -->
	    <div class="modal fade" id="modal_cliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">
			<form name="mezcla" method="POST" id="mezcla">
			<input type="hidden" name="id_cilindro_pev"  value="id_cilindro_pev">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							&times;
							</button>
							<h4 class="modal-title" id="myModalLabel">Seleccione el Cliente</h4>
						</div>
						<div class="modal-body">
							<div class="row">
								<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">						
									<form  name="form2" id="form2">							
										<thead>
											<tr>
												<th>#</th>
												<th>Acción</th>
												<th>Cliente</th>
												<th>Nit</th>									
											</tr>
										</thead>
										<tbody>										
											<?php                                
			                        			$contador = "0";
			                        	        $consulta = "SELECT * FROM clientes ";
			                                    $resultado = mysqli_query($con,$consulta) ;
			                                    while ($linea = mysqli_fetch_array($resultado))
			                                    {
			                                        $contador = $contador + 1;
			                                        $id_cliente = $linea["id_cliente"];
			                                        $nombre = $linea["nombre"];
			                                        $telefono_fijo = $linea["telefono_fijo"];
			                                        $telefono_celular = $linea["telefono_celular"];
			                                        $correo = $linea["correo"];
			                                        $persona_contacto = $linea["persona_contacto"];
			                                        $direccion = $linea["direccion"];
			                                        $observacion = $linea["observacion"];
			                                        $nit = $linea["nit"];
			                                    ?>
			                                        <tr>                                                	
			                                            <td width="5"><?php echo $contador; ?></td>
			                                            <td><a href="#" onclick="g_cliente(<?php echo $id_cliente; ?>)"><img src="img/chulo.png" width="20" height="20"></a></td>
			                                            <td><?php echo $nombre; ?></td>
			                                            <td><?php echo $nit; ?></td>                                            
			                                    	</tr>   
			                                    <?php
			                                    }mysqli_free_result($resultado); 
			                                ?>
										</tbody>
									</form>
								</table>
							</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">	Cancel</button>	
						</div>
					</div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			</form>
		</div>
		<!-- Fin Modal -->
	<?php
	}
	?>
	</div>
	<!-- END MAIN CONTENT -->
</div>
<!-- ==========================CONTENT ENDS HERE ========================== -->
<?php
	include("inc/footer.php");
	include("inc/scripts.php"); 
?>
<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>

<!-- Vector Maps Plugin: Vectormap engine, Vectormap language -->
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>

<!-- Full Calendar -->
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/jquery-form/jquery-form.min.js"></script>
<script type="text/javascript">
function imprSelec(muestra)
{
	var ficha=document.getElementById(muestra);
	var ventimp=window.open(' ','popimpr');
	ventimp.document.write(ficha.innerHTML);
	ventimp.document.close();
	ventimp.print();
	ventimp.close();
}
</script>
<script type="text/javascript">
	function g_cliente(id_cliente) 
	{
		$('#modal_cliente').modal('hide');
		//$('input:hidden[name=id_cliente]').val(num_cili1);

		$.ajax({
			url: 'id_cliente.php',
			type: 'POST',
			data: 'dato='+id_cliente,
		})
		.done(function(data) {
			var objeto = JSON.parse(data);				

			$("input[name=id_cliente2]").val(objeto.idcliente);
			$("input[name=nombre2]").val(objeto.nombre);
			
			document.getElementById("form2").reset();

		})
		.fail(function() {
			console.log("error");
		});
	}
</script>
<script type="text/javascript">
function validar_tr()
{ 
	var r = confirm("¿Esta seguro de finalizar el Transporte?");
	if (r == true) 
	{
		$(document).ready(function()
        {
			var formulario = $("#checkout-form").serialize();
        	var id_transporte_pev = <?php echo $id_transporte_pev; ?>;
        	$.ajax({
    		url: 'finalizar_trans_pev.php',
	       	type: 'POST',
	       	data: formulario,
	       	})
    		.done(function(resp){
    		  	if(resp == 1)
    		  	{
    		  		alert("Transporte finalizado exitosamente. Ahora puede imprimir el ticket.")
    		  		window.location="/transporte_pev.php?id_transporte_pev="+id_transporte_pev;
    		  	}
    		  	else
    		  	{
    		  		if(resp == 2)
    		  		{
    		  			alert("No se puede cerrar el transporte hasta que agregue al menos un cilindro.");    		  	
    		  			window.location="/transporte_pev.php?id_transporte_pev="+id_transporte_pev;
    		  		}
    		  	}
    		  	
    		  	
		    })
        });
	} 
}
</script>
<script type="text/javascript">

/*$(document).ready(function(){
	var valor = document.getElementById("id_placa_camion1").value;	
});

function habilitar()
{ 

	var valor = document.getElementById("id_placa_camion1").value;
	
	if (valor == 6 || valor == '') 
	{
		document.getElementById("placa_otro").disabled = false;
		document.getElementById("transportador_otro").disabled = false;
		document.getElementById("documento_transportador").disabled = false;
	}
	else
	{
		document.getElementById("placa_otro").disabled = true;
		document.getElementById("placa_otro").value = "";
		document.getElementById("transportador_otro").disabled = true;
		document.getElementById("transportador_otro").value = "";
		document.getElementById("documento_transportador").disabled = true;
		document.getElementById("documento_transportador").value = "";
	}
}*/
</script>

<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<script type="text/javascript">

// DO NOT REMOVE : GLOBAL FUNCTIONS!

$(document).ready(function() {

	/* // DOM Position key index //
		
	l - Length changing (dropdown)
	f - Filtering input (search)
	t - The Table! (datatable)
	i - Information (records)
	p - Pagination (paging)
	r - pRocessing 
	< and > - div elements
	<"#id" and > - div with an id
	<"class" and > - div with a class
	<"#id.class" and > - div with an id and class
	
	Also see: http://legacy.datatables.net/usage/features
	*/	

	/* BASIC ;*/
		var responsiveHelper_dt_basic = undefined;
		var responsiveHelper_datatable_fixed_column = undefined;
		var responsiveHelper_datatable_col_reorder = undefined;
		var responsiveHelper_datatable_tabletools = undefined;
		
		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};

		$('#dt_basic').dataTable({
			"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
			"autoWidth" : true,
			"preDrawCallback" : function() {
				// Initialize the responsive datatables helper once.
				if (!responsiveHelper_dt_basic) {
					responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
				}
			},
			"rowCallback" : function(nRow) {
				responsiveHelper_dt_basic.createExpandIcon(nRow);
			},
			"drawCallback" : function(oSettings) {
				responsiveHelper_dt_basic.respond();
			}
		});

	/* END BASIC */
	
	/* COLUMN FILTER  */
    var otable = $('#datatable_fixed_column').DataTable({
    	//"bFilter": false,
    	//"bInfo": false,
    	//"bLengthChange": false
    	//"bAutoWidth": false,
    	//"bPaginate": false,
    	//"bStateSave": true // saves sort state using localStorage
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_fixed_column) {
				responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_fixed_column.respond();
		}		
	
    });
    
    // custom toolbar
    $("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');
    	   
    // Apply the filter
    $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {
    	
        otable
            .column( $(this).parent().index()+':visible' )
            .search( this.value )
            .draw();
            
    } );
    /* END COLUMN FILTER */   

	/* COLUMN SHOW - HIDE */
	$('#datatable_col_reorder').dataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_col_reorder) {
				responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_col_reorder.respond();
		}			
	});
	
	/* END COLUMN SHOW - HIDE */

	/* TABLETOOLS */
	$('#datatable_tabletools').dataTable({
		
		// Tabletools options: 
		//   https://datatables.net/extensions/tabletools/button_options
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        "oTableTools": {
        	 "aButtons": [
             "copy",
             "csv",
             "xls",
                {
                    "sExtends": "pdf",
                    "sTitle": "SmartAdmin_PDF",
                    "sPdfMessage": "SmartAdmin PDF Export",
                    "sPdfSize": "letter"
                },
             	{
                	"sExtends": "print",
                	"sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>"
            	}
             ],
            "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
        },
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_tabletools) {
				responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_tabletools.respond();
		}
	});
	
	/* END TABLETOOLS */

})

</script>

<script type="text/javascript">

	$(document).ready(function() 
	{
		var $checkoutForm = $('#checkout-form').validate(
		{
		// Rules for form validation
			rules : 
			 {
				num_trans : 
				{
					required : true
				},
		        id_placa_camion1 : 
		        {
		          required : true
		        },
		        tipo_gas_pev :
		        {
		        	required : true
		        }
			},

			// Messages for form validation
			messages : {
				num_trans : {
					required : 'Debe ingresar el número CME'
				},
				id_placa_camion1 : {
					required : 'Debe seleccionar un elemento de la lista'
				},
				tipo_gas_pev : {
					required : 'Debe seleccionar un elemento de la lista'
				}
			},

			// Do not change code below
			errorPlacement : function(error, element) {
				error.insertAfter(element.parent());
			}
		});
		var $checkoutForm = $('#checkout-form1').validate(
		{
		// Rules for form validation
			rules : 
			 {
		        tipo_gas_pev :
		        {
		        	required : true
		        },
		        tapa :
		        {
		        	required : true
		        }
			},

			// Messages for form validation
			messages : {
				
				tipo_gas_pev : {
					required : 'Debe seleccionar un elemento de la lista'
				},
				tapa : {
					required : 'Debe seleccionar un elemento de la lista'
				}
			},

			// Do not change code below
			errorPlacement : function(error, element) {
				error.insertAfter(element.parent());
			}
		});	
	})

</script>


<?php 
	//include footer
	include("inc/google-analytics.php"); 
?>
<?php

}
else
{
    header("Location:index.php");
}
?>	    	