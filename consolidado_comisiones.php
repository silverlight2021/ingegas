<?php
session_start();
date_default_timezone_set("America/Bogota");

require ("libraries/conexion.php");

if(@$_SESSION['logged']== 'yes')
{ 
	$id_user=$_SESSION['su'];
	$idusuario = $_SESSION['su'];
    $acc = $_SESSION['acc'];

	require_once("inc/init.php");
	require_once("inc/config.ui.php");
	$page_title = "Consolidado de Recibos de Caja";
	$page_css[] = "your_style.css";
	include("inc/header.php");
	$page_nav['comisiones']['sub']['messer']['sub']['consolidado']["active"] = true;
	include("inc/nav.php");


?>
<!-- MAIN PANEL -->
<div id="main" role="main">

<?php 
if(isset($_REQUEST["sucursal"])){
	$sucursal = $_REQUEST["sucursal"];

	$consulta = "INSERT INTO comisiones2 SELECT * FROM comisiones";
	$resultado = mysqli_query($con, $consulta);

	if($resultado){
		?>
		<script type="text/javascript">
			window.alert("Archivo almacenado");
		</script>
		<?php
	}
}
?>

<?php
if (in_array(89, $acc))
{
?>
	<div id="content">	
		<div class="" align="center">
			<h1  class="page-title txt-color-blueDark"> <?php echo $page_title; ?>
			</h1>			
		</div>		
		<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">		
			<header>
				<span class="widget-icon"> <i class="fa fa-table"></i> </span>
				<h2>Cilindros</h2>				
			</header>
			<div>
				<div class="jarviswidget-editbox"></div>
				<div class="widget-body no-padding">
					<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
						<form action="" method="POST" name="form1">
							<thead>
								<tr>
									<th>#</th>
									<th>Fecha</th>
									<th>No. de Archivos</th>
									<th>Sucursal</th>
									<th>Descargar</th>
									<th>Consolidado Correcto</th>
                  <th>Habilitar Cargue</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$contador = 0;
								$consulta = "SELECT DISTINCT(sucursal) AS 'sucursal' FROM comisiones";
								$resultado = mysqli_query($con, $consulta);

								while($linea = mysqli_fetch_array($resultado)){
									$contador++;
									$sucursal = $linea["sucursal"];

									$consulta2 = "SELECT * FROM comisiones1 WHERE sucursal = '$sucursal'";
									$resultado2 = mysqli_query($con, $consulta2);

									if(mysqli_num_rows($resultado2)){
										$numeroArchivos = 2;
									}else{
										$numeroArchivos = 1;
									}

									$consulta3 = "SELECT * FROM cargue_comisiones WHERE estado_cargue = 1 ORDER BY id_cargue DESC LIMIT 1";
									$resultado3 = mysqli_query($con, $consulta3);

									$linea3 = mysqli_fetch_array($resultado3);

									$fecha = $linea3["fecha"];

									?>
									<tr>
										<td><?php echo $contador; ?></td>
										<td><?php echo $fecha; ?></td>
										<td><?php echo $numeroArchivos; ?></td>
										<td><?php echo $sucursal; ?></td>
										<td><a href="descargar_excel/comisiones/descargar_consolidado_comisiones.php?sucursal=<?php echo $sucursal; ?>"><img src="img/excel_icon.png" width="30" height="30"></a></td>
										<td>
											<a href="consolidado_comisiones.php?sucursal=<?php echo $sucursal ?>"><img src="img/guardar_recibos.png" width="30" height="30"></a>
										</td>
										<td style="text-align:center; cursor:pointer">
											<p onclick="habilitar_cargue()"><img src="img/cargar_recibos.png" width="30" height="30">
											</p>
										
										</td>
									</tr>
									<?php
								}
								?>
							</tbody>
						</form>
					</table>
				</div>
			</div>
			<section id="widget-grid" class="">
				<div class="row">
					<p>&nbsp;</p>
				</div>
				<div class="row">
					<p>&nbsp;</p>
				</div>
				<div class="row">
					<p>&nbsp;</p>
				</div>
				<div class="row">
					<p>&nbsp;</p>
				</div>
        <div class="row" style="display:none" id="cargue">
          <article class="col-md-6 col-md-offset-3">		
            <div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">			
              <header><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
                <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                <h2>Archivos a cargar</h2>											
              </header>
              <div>
                <div class="jarviswidget-editbox"></div>	
                <div class="widget-body no-padding">
                  <form id="upload_csv" class="smart-form" method="POST" name="upload_csv" enctype="multipart/form-data">
                    <fieldset>
                      <div class="row" align="center">
                        <section style="padding: 15px">
                          <label class="font-lg">Archivo</label>
                          <input type="file" name="file" class="file" style="display:none">
                          <div class="input-group">
                            <label class="input"><input type="text" class="form-control input-lg" disabled placeholder="Subir Excel">
                            </label>
                            <span class="input-group-btn">
                              <button type="button" class="btn1 btn-warning btn-lg">Seleccionar</button>
                            </span>
                          </div>
                        </section>
                      </div>
                    </fieldset>
                    <footer>
                      <p id="cargando" style="font-size: 20px; color: green"></p>  
                      <input type="submit" name="Import" id="submit" class="btn btn-primary" value="SUBIR ARCHIVOS">      
                    </footer>
                  </form>
                </div>
              </div>				
            </div>	
          </article>				
        </div>
      </section>
      
		</div>
	</div>
	<!-- END MAIN CONTENT -->
<?php
}										
?>

</div>
<!-- ==========================CONTENT ENDS HERE ========================== -->
<?php
	include("inc/footer.php");
	include("inc/scripts.php"); 
?>

<script type="text/javascript">
    $(document).on('click', '.btn1', function(){
    var file = $(this).parent().parent().parent().find('.file');
      file.trigger('click');
    });
    $(document).on('change', '.file', function(){
      $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
    });
  </script>

	<script type="text/javascript">
    $(document).ready(function(){
      $('#upload_csv').on("submit", function(e){
        e.preventDefault();
        $.ajax({
          url: "comisiones/importar_archivo_consolidado.php",
          method: "POST",
          data: new FormData(this),
          contentType: false, 
          cache: false, 
          processData: false,
          beforeSend: function(){
            $('#cargando').html("Importando archivo, espere por favor...")
          }, 
          success: function(data){
            if(data==1){
              $('#cargando').html("Archivo seleccionado invalido");
            }else if(data == "Error2"){
              $('#cargando').html("Por favor seleccione un archivo");
            }else if(data == "Exito"){
              $('#cargando').html("Cargue Realizado!");
            $.ajax({
                type: 'POST',
                url: 'comisiones/evaluar_cargue_consolidado.php',
                beforeSend: function(){
                  $('#cargando').html("Filtrando Recibos...");
                },
                success: function(result_2){
                  if(result_2 == 1){
                    $('#cargando').html("Ha ocurrido un error");
                  }else if(result_2 == 2){
                    $('#cargando').html("Cargue y filtrado finalizado");
                    /*$.ajax({
                      type: 'POST',
                      url: 'comisiones/consolidar_comisiones.php',
                      beforeSend: function(){
                        $('#cargando').html("Consolidando Informacion...");
                      },
                      success: function(result_3){
                        $('#cargando').html("Consolidando Informacion...");
                        /*if(result_3 == 1){
                          $('#cargando').html("Por favor cargue el primer archivo");
                        }else if(result_3 == 2){
                          $('#cargando').html("Ha ocurrido un error")
                        }else if(result_3 == 3){
                          $('#cargando').html("Consolidacion exitosa")
                          $.ajax({
                            type: 'POST',
                            url: 'comisiones/anexar_tabla.php',
                            beforeSend: function(){
                              $('#cargando').html("Anexando a tabla de comisiones");
                            },
                            success: function(result_4){
                              if(result_4 == 1){
                                $('#cargando').html("Ha ocurrido un error");
                              }else if(result_4 == 2){
                                $('#cargando').html("Error en el anexo");
                              }else if(result_4 == 3){
                                $('#cargando').html("Anexo exitoso")
                                $.ajax({
                                  type: 'POST',
                                  url: 'comisiones/evaluar_faltantes.php',
                                  beforeSend: function(){
                                    $('#cargando').html("Evaluando Recibos Faltantes");
                                  },
                                  success: function(result_5){
                                    $('#cargando').html('Descargando Recibos Faltantes')
                                    var url = "comisiones/descargar_recibos.php";
                                    window.open(url);
                                    $.ajax({
                                      type: 'POST',
                                      url: 'comisiones/evaluar_notas_faltantes.php',
                                      beforeSend: function(){
                                        $('#cargando').html('Evaluando Notas Faltantes')
                                      },
                                      success: function(result_6){
                                        $('#cargando').html('Operacion Completada')
                                        var url = "comisiones/descargar_notas.php";
                                        window.open(url);
                                      }
                                    });
                                  },
                                  error: function(){
                                    $('#cargando').html('Descargando Recibos Faltantes')
                                    var url = "comisiones/descargar_recibos.php";
                                    window.open(url);
                                    $.ajax({
                                      type: 'POST',
                                      url: 'comisiones/evaluar_notas_faltantes.php',
                                      beforeSend: function(){
                                        $('#cargando').html('Evaluando Notas Faltantes')
                                      },
                                      success: function(result_6){
                                        $('#cargando').html('Operacion Completada')
                                        var url = "comisiones/descargar_notas.php";
                                        window.open(url);
                                      },
                                      error: function(){
                                        $('#cargando').html('Operacion Completada')
                                        var url = "comisiones/descargar_notas.php";
                                        window.open(url);
                                      }
                                    });
                                  }
                                });
                              }
                            }
                          });
                        }
                      }
                    });*/
                  }
                }
              });
            }else{
              $('#cargando').html(data);
            }
          }
        })
      });
    });
  </script>

<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<script type="text/javascript">
	function habilitar_cargue(){
		document.getElementById("cargue").style.display = "block";
	}
</script>
<script type="text/javascript">



// DO NOT REMOVE : GLOBAL FUNCTIONS!

$(document).ready(function() {
	
	/* // DOM Position key index //
		
	l - Length changing (dropdown)
	f - Filtering input (search)
	t - The Table! (datatable)
	i - Information (records)
	p - Pagination (paging)
	r - pRocessing 
	< and > - div elements
	<"#id" and > - div with an id
	<"class" and > - div with a class
	<"#id.class" and > - div with an id and class
	
	Also see: http://legacy.datatables.net/usage/features
	*/	

	/* BASIC ;*/
		var responsiveHelper_dt_basic = undefined;
		var responsiveHelper_datatable_fixed_column = undefined;
		var responsiveHelper_datatable_col_reorder = undefined;
		var responsiveHelper_datatable_tabletools = undefined;
		
		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};

		$('#dt_basic').dataTable({
			"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
			"autoWidth" : true,
			"preDrawCallback" : function() {
				// Initialize the responsive datatables helper once.
				if (!responsiveHelper_dt_basic) {
					responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
				}
			},
			"rowCallback" : function(nRow) {
				responsiveHelper_dt_basic.createExpandIcon(nRow);
			},
			"drawCallback" : function(oSettings) {
				responsiveHelper_dt_basic.respond();
			}
		});

	/* END BASIC */
	
	/* COLUMN FILTER  */
    var otable = $('#datatable_fixed_column').DataTable({
    	//"bFilter": false,
    	//"bInfo": false,
    	//"bLengthChange": false
    	//"bAutoWidth": false,
    	//"bPaginate": false,
    	//"bStateSave": true // saves sort state using localStorage
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_fixed_column) {
				responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_fixed_column.respond();
		}		
	
    });
    
    // custom toolbar
    $("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');
    	   
    // Apply the filter
    $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {
    	
        otable
            .column( $(this).parent().index()+':visible' )
            .search( this.value )
            .draw();
            
    } );
    /* END COLUMN FILTER */   

	/* COLUMN SHOW - HIDE */
	$('#datatable_col_reorder').dataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_col_reorder) {
				responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_col_reorder.respond();
		}			
	});
	
	/* END COLUMN SHOW - HIDE */

	/* TABLETOOLS */
	$('#datatable_tabletools').dataTable({
		
		// Tabletools options: 
		//   https://datatables.net/extensions/tabletools/button_options
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        "oTableTools": {
        	 "aButtons": [
             "copy",
             "csv",
             "xls",
                {
                    "sExtends": "pdf",
                    "sTitle": "SmartAdmin_PDF",
                    "sPdfMessage": "SmartAdmin PDF Export",
                    "sPdfSize": "letter"
                },
             	{
                	"sExtends": "print",
                	"sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>"
            	}
             ],
            "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
        },
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_tabletools) {
				responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_tabletools.respond();
		}
	});
	
	/* END TABLETOOLS */

})

</script>

<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script type="text/javascript">

 
		
		$(document).ready(function() {
			
			// menu
			$("#menu").menu();
		
			/*
			 * AUTO COMPLETE AJAX
			 */
		
			function log(message) {
				$("<div>").text(message).prependTo("#log");
				$("#log").scrollTop(0);
			}
		
			$("#city").autocomplete({
				source : function(request, response) {
					$.ajax({
						url : "http://ws.geonames.org/searchJSON",
						dataType : "jsonp",
						data : {
							featureClass : "P",
							style : "full",
							maxRows : 12,
							name_startsWith : request.term
						},
						success : function(data) {
							response($.map(data.geonames, function(item) {
								return {
									label : item.name + (item.adminName1 ? ", " + item.adminName1 : "") + ", " + item.countryName,
									value : item.name
								}
							}));
						}
					});
				},
				minLength : 2,
				select : function(event, ui) {
					log(ui.item ? "Selected: " + ui.item.label : "Nothing selected, input was " + this.value);
				}
			});
		
			/*
			 * Spinners
			 */
			$("#spinner").spinner();
			$("#spinner-decimal").spinner({
				step : 0.01,
				numberFormat : "n"
			});
		
			$("#spinner-currency").spinner({
				min : 5,
				max : 2500,
				step : 25,
				start : 1000,
				numberFormat : "C"
			});
		
			/*
			 * CONVERT DIALOG TITLE TO HTML
			 * REF: http://stackoverflow.com/questions/14488774/using-html-in-a-dialogs-title-in-jquery-ui-1-10
			 */
			$.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
				_title : function(title) {
					if (!this.options.title) {
						title.html("&#160;");
					} else {
						title.html(this.options.title);
					}
				}
			}));
		
		
			/*
			* DIALOG SIMPLE
			*/
		
			// Dialog click
			$('#dialog_link').click(function() {
				$('#dialog_simple').dialog('open');
				return false;
		
			});
		
			$('#dialog_simple').dialog({
				autoOpen : false,
				width : 600,
				resizable : false,
				modal : true,
				title : "<div class='widget-header'><h4><i class='fa fa-warning'></i> Empty the recycle bin?</h4></div>",
				buttons : [{
					html : "<i class='fa fa-trash-o'></i>&nbsp; Delete all items",
					"class" : "btn btn-danger",
					click : function() {
						$(this).dialog("close");
					}
				}, {
					html : "<i class='fa fa-times'></i>&nbsp; Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
					}
				}]
			});
		
			/*
			* DIALOG HEADER ICON
			*/
		
			// Modal Link
			$('#modal_link').click(function() {
				$('#dialog-message').dialog('open');
				return false;
			});
		
			$("#dialog-message").dialog({
				autoOpen : false,
				modal : true,
				title : "<div class='widget-header'><h4><i class='icon-ok'></i> jQuery UI Dialog</h4></div>",
				buttons : [{
					html : "Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
					}
				}]
		
			});
		
			/*
			 * Remove focus from buttons
			 */
			$('.ui-dialog :button').blur();
		
			/*
			 * Just Tabs
			 */
		
			$('#tabs').tabs();
		
			/*
			 *  Simple tabs adding and removing
			 */
		
			$('#tabs2').tabs();
		
			// Dynamic tabs
			var tabTitle = $("#tab_title"), tabContent = $("#tab_content"), tabTemplate = "<li style='position:relative;'> <span class='air air-top-left delete-tab' style='top:7px; left:7px;'><button class='btn btn-xs font-xs btn-default hover-transparent'><i class='fa fa-times'></i></button></span></span><a href='#{href}'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; #{label}</a></li>", tabCounter = 2;
		
			var tabs = $("#tabs2").tabs();
		
			// modal dialog init: custom buttons and a "close" callback reseting the form inside
			var dialog = $("#addtab").dialog({
				autoOpen : false,
				width : 600,
				resizable : false,
				modal : true,
				buttons : [{
					html : "<i class='fa fa-times'></i>&nbsp; Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
		
					}
				}]
			});
		
			// addTab form: calls addTab function on submit and closes the dialog
			var form = dialog.find("form").submit(function(event) {
				addTab();
				dialog.dialog("close");
				
			});
		
			// actual addTab function: adds new tab using the input from the form above

			
			function addTab() 
			{
				
				var label = tabTitle.val() || "Tab " + tabCounter, id = "tabs-" + tabCounter, li = $(tabTemplate.replace(/#\{href\}/g, "#" + id).replace(/#\{label\}/g, label)), tabContentHtml = tabContent.val() || "Tab " + tabCounter + " content.";
		
				tabs.find(".ui-tabs-nav").append(li);
				tabs.append("<div id='" + id + "'><p>" + tabContentHtml + "</p></div>");
				tabs.tabs("refresh");
				tabCounter++;
		
				// clear fields
				$("#tab_title").val("");
				$("#tab_content").val("");
			}
		
			// addTab button: just opens the dialog
			$("#add_tab").button().click(function() {
				var idd = document.getElementById('num').firstChild.nodeValue;

				
				dialog.dialog("open");
			});
		
			// close icon: removing the tab on click
					
			/*
			* ACCORDION
			*/
			//jquery accordion
			
		     var accordionIcons = {
		         header: "fa fa-plus",    // custom icon class
		         activeHeader: "fa fa-minus" // custom icon class
		     };
		     
			$("#accordion").accordion({
				autoHeight : false,
				heightStyle : "content",
				collapsible : true,
				animate : 300,
				icons: accordionIcons,
				header : "h4",
			})
		
			/*
			 * PROGRESS BAR
			 */
			$("#progressbar").progressbar({
		     	value: 25,
		     	create: function( event, ui ) {
		     		$(this).removeClass("ui-corner-all").addClass('progress').find(">:first-child").removeClass("ui-corner-left").addClass('progress-bar progress-bar-success');
				}
			});			

		})

	</script>
	


<?php 

	include("inc/google-analytics.php"); 


}
else
{
    header("Location:index.php");
}
?>