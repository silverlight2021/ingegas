<?php
// William Aguirre Carpeta
// PDF HOJA DE RUTA INGEGAS
header('Content-Type: text/html; charset=UTF-8');
set_time_limit(0);
session_start();
date_default_timezone_set("America/Bogota");
require ("libraries/conexion.php");
require('libraries/fpdf/fpdf.php');
$fecha = date("Y-m-d");
$hora = date("G:i:s",time());

////////////////VARIABLES POST///////////////


	$logotipo_empresa = "img/logo-ingegas.png";
	$id_datos_prueba_pev = isset($_REQUEST['id_datos_prueba_pev']) ? $_REQUEST['id_datos_prueba_pev'] : NULL;
	$num_cili1 = isset($_REQUEST['num_cili']) ? $_REQUEST['num_cili'] : NULL;
	$firma = "img/firma_nelson_forero.PNG";

	$nombre = "";
	$nit = "";
	$direccion = "";
	$telefono_fijo = "";
	$tabla_inspeccion = "inspeccion_alu";
	$campo_inspeccion = "estado_aluminio";

	$consulta1 = "SELECT * FROM datos_prueba_pev WHERE id_datos_prueba_pev = '$id_datos_prueba_pev'";
	$resultado1 = mysqli_query($con,$consulta1);
	if(mysqli_num_rows($resultado1) > 0){

		$linea1 = mysqli_fetch_assoc($resultado1);

		$id_has_movimiento_cilindro_pev = isset($linea1["id_has_movimiento_cilindro_pev"]) ? $linea1["id_has_movimiento_cilindro_pev"] : NULL;
		$expansion_total_1 = isset($linea1["expansion_total_1"]) ? $linea1["expansion_total_1"] : NULL;
		$expansion_total_2 = isset($linea1["expansion_total_2"]) ? $linea1["expansion_total_2"] : NULL;
		$expansion_total_3 = isset($linea1["expansion_total_3"]) ? $linea1["expansion_total_3"] : NULL;
		$porcentaje_prueba = isset($linea1["porcentaje_prueba"]) ? $linea1["porcentaje_prueba"] : NULL;
		$idUser = isset($linea1["idUser"]) ? $linea1["idUser"] : NULL;
		$fecha_hora = isset($linea1["fecha_hora"]) ? $linea1["fecha_hora"] : NULL;

		$fecha_prueba = substr($fecha_hora, -19, 10);

		$consulta2 = "SELECT id_transporte_pev, num_cili, id_tipo_gas_pev, material_cilindro_u FROM has_movimiento_cilindro_pev WHERE id_has_movimiento_cilindro_pev = $id_has_movimiento_cilindro_pev";
		$resultado2 = mysqli_query($con,$consulta2);

		if(mysqli_num_rows($resultado2) > 0){

			$linea2 = mysqli_fetch_assoc($resultado2);

			$id_transporte_pev = isset($linea2["id_transporte_pev"]) ? $linea2["id_transporte_pev"] : NULL;
			$num_cili = isset($linea2["num_cili"]) ? $linea2["num_cili"] : NULL;
			$id_tipo_gas_pev = isset($linea2["id_tipo_gas_pev"]) ? $linea2["id_tipo_gas_pev"] : NULL;
			$material_cilindro_u = isset($linea2["material_cilindro_u"]) ? $linea2["material_cilindro_u"] : NULL;

			$consulta3 = "SELECT id_cliente FROM transporte_pev WHERE id_transporte_pev = $id_transporte_pev";
			$resultado3 = mysqli_query($con,$consulta3);

			if(mysqli_num_rows($resultado3) > 0){

				$linea3 = mysqli_fetch_assoc($resultado3);

				$id_cliente = isset($linea3["id_cliente"]) ? $linea3["id_cliente"] : NULL;

				$consulta4 = "SELECT nombre, nit, direccion, telefono_fijo FROM clientes WHERE id_cliente = $id_cliente";
				$resultado4 = mysqli_query($con,$consulta4);

				if(mysqli_num_rows($resultado4) > 0){

					$linea4 = mysqli_fetch_assoc($resultado4);

					$nombre = isset($linea4["nombre"]) ? $linea4["nombre"] : NULL;
					$nit = isset($linea4["nit"]) ? $linea4["nit"] : NULL;
					$direccion = isset($linea4["direccion"]) ? $linea4["direccion"] : NULL;
					$telefono_fijo = isset($linea4["telefono_fijo"]) ? $linea4["telefono_fijo"] : NULL;
				}
			}

			$consulta5 = "SELECT nombre FROM tipo_gas_pev WHERE id_tipo_gas_pev = $id_tipo_gas_pev";
			$resultado5 = mysqli_query($con,$consulta5);

			if(mysqli_num_rows($resultado5) > 0){

				$linea5 = mysqli_fetch_assoc($resultado5);

				$nombre_gas = isset($linea5["nombre"]) ? $linea5["nombre"] : NULL;
			}

			$consulta6 = "SELECT id_especi_eto, especi_eto, pre_tra_eto, pre_pru_eto, fecha_fab_eto, fecha_ult_eto, volumen_eto, tara_esta_eto, tara_actu_eto, tipo, fech_crea FROM cilindro_eto WHERE num_cili_eto = '$num_cili'";
			$resultado6 = mysqli_query($con,$consulta6);

			if(mysqli_num_rows($resultado6) > 0){

				$linea6 = mysqli_fetch_assoc($resultado6);

				$id_especi_eto = isset($linea6["id_especi_eto"]) ? $linea6["id_especi_eto"] : NULL;
				$especi_eto = isset($linea6["especi_eto"]) ? $linea6["especi_eto"] : NULL;
				$pre_tra_eto_psi = isset($linea6["pre_tra_eto"]) ? $linea6["pre_tra_eto"] : NULL;
				$pre_pru_eto_psi = isset($linea6["pre_pru_eto"]) ? $linea6["pre_pru_eto"] : NULL;
				$fecha_fab_eto = isset($linea6["fecha_fab_eto"]) ? $linea6["fecha_fab_eto"] : NULL;
				$fecha_ult_eto = isset($linea6["fecha_ult_eto"]) ? $linea6["fecha_ult_eto"] : NULL;
				$volumen_eto = isset($linea6["volumen_eto"]) ? $linea6["volumen_eto"] : NULL;
				$tara_esta_eto = isset($linea6["tara_esta_eto"]) ? $linea6["tara_esta_eto"] : NULL;
				$tara_actu_eto = isset($linea6["tara_actu_eto"]) ? $linea6["tara_actu_eto"] : NULL;
				$tipo = isset($linea6["tipo"]) ? $linea6["tipo"] : NULL;
				$fech_crea = isset($linea6["fech_crea"]) ? $linea6["fech_crea"] : NULL;

				$pre_tra_eto_mpa = $pre_tra_eto_psi/145.038;
				$pre_pru_eto_mpa = $pre_pru_eto_psi/145.038;

				$pre_tra_eto_mpa = round($pre_tra_eto_mpa, 5);
				$pre_pru_eto_mpa = round($pre_pru_eto_mpa, 5);

				$consulta7 = "SELECT especificacion FROM especificacion WHERE id_especificacion = $id_especi_eto";
				$resultado7 = mysqli_query($con,$consulta7);

				if(mysqli_num_rows($resultado7) > 0){

					$linea7 = mysqli_fetch_assoc($resultado7);

					$especificacion = isset($linea7["especificacion"]) ? $linea7["especificacion"] : NULL;
				}
			}

			$consulta8 = "SELECT nombre FROM material_cilindro WHERE id_material = '$material_cilindro_u'";
			$resultado8 = mysqli_query($con,$consulta8);

			if(mysqli_num_rows($resultado8) > 0){

				$linea8 = mysqli_fetch_assoc($resultado8);

				$nombre_material = isset($linea8["nombre"]) ? $linea8["nombre"] : NULL;
			}

			if($material_cilindro_u == 1){
				$tabla_inspeccion = 'inspeccion_ace';
				$campo_inspeccion = 'estado_acero';
			}else if($material_cilindro_u == 2){
				$tabla_inspeccion = 'inspeccion_alu';
				$campo_inspeccion = 'estado_aluminio';
			}

			$consulta9 = "SELECT $campo_inspeccion FROM $tabla_inspeccion WHERE id_has_movimiento_cilindro_pev = $id_has_movimiento_cilindro_pev";
			$resultado9 = mysqli_query($con,$consulta9);

			if(mysqli_num_rows($resultado9) > 0){

				$linea9 = mysqli_fetch_assoc($resultado9);

				$resultado_insp_visual = isset($linea9[$campo_inspeccion]) ? $linea9[$campo_inspeccion] : NULL;


			}
		}
	}
	

	





/**
*  Clase para la maquetación de Factura
*/
class PDF extends FPDF
{
	function _Arc($x1, $y1, $x2, $y2, $x3, $y3) //Curvatura del rectángulo
	{
		$h = $this->h;
		$this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
							$x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
	}
	// private functions
	function RoundedRect($x, $y, $w, $h, $r, $style = '') //Rectángulo con bordes redondeados 
	{
		$k = $this->k;
		$hp = $this->h;
		if($style=='F')
			$op='f';
		elseif($style=='FD' || $style=='DF')
			$op='B';
		else
			$op='S';
		$MyArc = 4/3 * (sqrt(2) - 1);
		$this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));
		$xc = $x+$w-$r ;
		$yc = $y+$r;
		$this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));

		$this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);
		$xc = $x+$w-$r ;
		$yc = $y+$h-$r;
		$this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
		$this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);
		$xc = $x+$r ;
		$yc = $y+$h-$r;
		$this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
		$this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);
		$xc = $x+$r ;
		$yc = $y+$r;
		$this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
		$this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
		$this->_out($op);
	}
	// public functions
	function sizeOfText( $texte, $largeur )
	{
		$index    = 0;
		$nb_lines = 0;
		$loop     = TRUE;
		while ( $loop )
		{
			$pos = strpos($texte, "\n");
			if (!$pos)
			{
				$loop  = FALSE;
				$ligne = $texte;
			}
			else
			{
				$ligne  = substr( $texte, $index, $pos);
				$texte = substr( $texte, $pos+1 );
			}
			$length = floor( $this->GetStringWidth( $ligne ) );
			$res = 1 + floor( $length / $largeur) ;
			$nb_lines += $res;
		}
		return $nb_lines;
	}
	//ENCABEZADO DE PÁGINA
	function Header()
	{
		$x1 = 140;
		$y1 = 6;
		//Variables de encabezado
		global $logotipo_empresa, $nombre_empresa;
		global $reviso, $aprobo,$fecha_reviso,$fecha_aprobo,$codigo,$version,$pagina;

	    $this->SetFont('Arial','B',9);
		$this->SetTextColor(0,0,0);
		$this->SetLineWidth(0);	
		//.....Logo de la compañia		
		$this->Image($logotipo_empresa,15,8,10,15);
		$this->SetXY(60,18);
		$this->setFillColor(255,181,198);
		$this->SetXY(15,8);
		$this->Cell(180,20,'',1,1,'',1);
		$this->Image($logotipo_empresa,15,8,10,15);
		$this->SetXY(60,18);
		
		$this->SetXY(56,8);
		$this->Cell(100,5, 'INGENIERIA Y GASES LTDA "INGEGAS"', 0,0,'C');
		$this->SetXY(56,13);
		$this->Cell(100,5, 'Calle 163 # 19 A 32 PBX: (1) 6706126 FAX: (1) 6772951', 0,1,'C');
		$this->SetXY(15,23);
		$this->Cell(30,5,'www.ingegas.com',0,1);
		$this->SetXY(15,28);
		$this->Cell(180,10,'',1,1,'',1);
		$this->SetXY(56,28);
		$this->Cell(100,5,'INFORME CILINDOS RECHAZADOS',0,1,'C');
		$this->SetFont('Arial','',6);
		$this->SetXY(56,35);
		$this->Cell(100,3,'ICP-PL-02(V2)',0,1,'C');
	}
	//PIE DE PÁGINA
	function Footer()
	{
		global $firma;
		$this->SetFont('Arial','B',10);
		
		$this->SetXY(15,225);
		$this->Cell(35,5,'OBSERVACIONES',0,1);
		$this->SetXY(15,230);
		$this->Cell(180,5,'','B',1);
		$this->SetXY(15,235);
		$this->Cell(180,5,'','B',1);
		$this->SetXY(15,240);
		$this->Cell(180,5,'','B',1);
		$this->SetXY(15,250);
		$this->Cell(180,35,'',0,1);
		$this->Image($firma,50,252,100,30);
	}
}
$pdf = new PDF( 'P', 'mm', 'A4');
$pdf->SetAutoPageBreak(TRUE,40);
$pdf->AliasNbPages();
$pdf->AddPage();

//CONTENIDO DEL CUERPO DE LA HOJA DE RUTA
		
		$pdf->SetFont('Arial','B',9);
		$pdf->SetXY(15,38);
		$pdf->Cell(180,5,'',1,1);
		$pdf->SetXY(15,38);
		$pdf->Cell(20,5,'FECHA:',0,1);
		$pdf->SetXY(15,43);
		$pdf->Cell(180,5,'',1,1);
		$pdf->SetXY(15,43);
		$pdf->Cell(35,5,'PROPIETARIO:',0,1);
		$pdf->SetXY(15,48);
		$pdf->Cell(180,5,'',1,1);
		$pdf->SetXY(15,48);
		$pdf->Cell(20,5,'CILINDRO:',0,1);
		$pdf->SetXY(120,38);
		$pdf->Cell(30,5,'LPH-R 528');

		$pdf->SetFont('Arial','',9);
		$pdf->SetXY(35,38);
		$pdf->Cell(30,5,$fecha_prueba,0,1); //<!------------------------------------------------
		$pdf->SetXY(50,43);
		$pdf->Cell(70,5,$nombre,0,1);  //<!--------------------------------------------
		$pdf->SetXY(35,48);
		$pdf->Cell(35,5,$num_cili,0,1);  //<!---------------------------------------------------

		$pdf->SetFont('Arial','B',9);
		$pdf->SetXY(15,53);
		$pdf->Cell(180,5,'',1,1);
		$pdf->SetXY(15,53);
		$pdf->Cell(105,5,'RECHAZADO POR:',1,1,'C');
		$pdf->SetXY(120,53);
		$pdf->Cell(75,5,'OBSERVACIONES',1,1,'C');		

		$pdf->SetXY(15,58);
		$pdf->Cell(180,5,'',1,1);
		$pdf->SetXY(15,58);
		$pdf->Cell(105,5,'IDENTIFICACION ADULTERADA:',1,1);
		$pdf->SetXY(120,58);
		$pdf->Cell(75,5,'',1,1,'C');
		$pdf->SetXY(15,63);
		$pdf->Cell(180,5,'',1,1);
		$pdf->SetXY(15,63);
		$pdf->Cell(105,5,'FUSURAS:',1,1);
		$pdf->SetXY(120,63);
		$pdf->Cell(75,5,'',1,1,'C');
		$pdf->SetXY(15,68);
		$pdf->Cell(180,5,'',1,1);
		$pdf->SetXY(15,68);
		$pdf->Cell(105,5,'CORTES:',1,1);
		$pdf->SetXY(120,68);
		$pdf->Cell(75,5,'',1,1,'C');	
		$pdf->SetXY(15,73);
		$pdf->Cell(180,5,'',1,1);
		$pdf->SetXY(15,73);
		$pdf->Cell(105,5,'PROTUBERANCIAS:',1,1);
		$pdf->SetXY(120,73);
		$pdf->Cell(75,5,'',1,1,'C');
		$pdf->SetXY(15,78);
		$pdf->Cell(180,5,'',1,1);
		$pdf->SetXY(15,78);
		$pdf->Cell(105,5,'GRIETAS:',1,1);
		$pdf->SetXY(120,78);
		$pdf->Cell(75,5,'',1,1,'C');
		$pdf->SetXY(15,83);
		$pdf->Cell(180,5,'',1,1);
		$pdf->SetXY(15,83);
		$pdf->Cell(105,5,'INCISIONES:',1,1);
		$pdf->SetXY(120,83);
		$pdf->Cell(75,5,'',1,1,'C');
		$pdf->SetXY(15,88);
		$pdf->Cell(180,5,'',1,1);
		$pdf->SetXY(15,88);
		$pdf->Cell(105,5,'PICADURAS:',1,1);
		$pdf->SetXY(120,88);
		$pdf->Cell(75,5,'',1,1,'C');
		$pdf->SetXY(15,93);
		$pdf->Cell(180,5,'',1,1);
		$pdf->SetXY(15,93);
		$pdf->Cell(105,5,'ABOLLADURAS:',1,1);
		$pdf->SetXY(120,93);
		$pdf->Cell(75,5,'',1,1,'C');
		$pdf->SetXY(15,98);
		$pdf->Cell(180,5,'',1,1);
		$pdf->SetXY(15,98);
		$pdf->Cell(105,5,utf8_decode('CORROSIÓN EXTERNA:'),1,1);
		$pdf->SetXY(120,98);
		$pdf->Cell(75,5,'',1,1,'C');
		$pdf->SetXY(15,103);
		$pdf->Cell(180,5,'',1,1);
		$pdf->SetXY(15,103);
		$pdf->Cell(105,5,utf8_decode('CORROSIÓN INTERNA:'),1,1);
		$pdf->SetXY(120,103);
		$pdf->Cell(75,5,'',1,1,'C');
		$pdf->SetXY(15,108);
		$pdf->Cell(180,5,'',1,1);
		$pdf->SetXY(15,108);
		$pdf->Cell(105,5,utf8_decode('LAMINACIÓN:'),1,1);
		$pdf->SetXY(120,108);
		$pdf->Cell(75,5,'',1,1,'C');
		$pdf->SetXY(15,113);
		$pdf->Cell(180,5,'',1,1);
		$pdf->SetXY(15,113);
		$pdf->Cell(105,5,'PERDIDA DE MASA SUPERIOR AL 2% (GNV):',1,1);
		$pdf->SetXY(120,113);
		$pdf->Cell(75,5,'',1,1,'C');
		$pdf->SetXY(15,118);
		$pdf->Cell(180,5,'',1,1);
		$pdf->SetXY(15,118);
		$pdf->Cell(105,5,'PERDIDA DE PESO SUPERIOR AL 3%:',1,1);
		$pdf->SetXY(120,118);
		$pdf->Cell(75,5,'',1,1,'C');
		$pdf->SetXY(15,123);
		$pdf->Cell(180,5,'',1,1);
		$pdf->SetXY(15,123);
		$pdf->Cell(105,5,utf8_decode('QUEMADURAS POR ARCO ELÉCTRICO:'),1,1);
		$pdf->SetXY(120,123);
		$pdf->Cell(75,5,'',1,1,'C');
		$pdf->SetXY(15,128);
		$pdf->Cell(180,5,'',1,1);
		$pdf->SetXY(15,128);
		$pdf->Cell(105,5,'SOLDADURA INTERNA:',1,1);
		$pdf->SetXY(120,128);
		$pdf->Cell(75,5,'',1,1,'C');
		$pdf->SetXY(15,133);
		$pdf->Cell(180,5,'',1,1);
		$pdf->SetXY(15,133);
		$pdf->Cell(105,5,'',1,1);
		$pdf->SetXY(120,133);
		$pdf->Cell(75,5,'',1,1,'C');
		$pdf->SetXY(15,138);
		$pdf->Cell(180,5,'',1,1);
		$pdf->SetXY(15,138);
		$pdf->Cell(105,5,utf8_decode('ROSCA DE CUELLO DAÑADA:'),1,1);
		$pdf->SetXY(120,138);
		$pdf->Cell(75,5,'',1,1,'C');
		$pdf->SetXY(15,143);
		$pdf->Cell(180,5,'',1,1);
		$pdf->SetXY(15,143);
		$pdf->Cell(105,5,utf8_decode('VÁLVULA DAÑADA'),1,1);
		$pdf->SetXY(120,143);
		$pdf->Cell(75,5,'',1,1,'C');
		$pdf->SetXY(15,148);
		$pdf->Cell(180,5,'',1,1);
		$pdf->SetXY(15,148);
		$pdf->Cell(105,5,utf8_decode('% EXPANSIÓN PERMANENTE SUPERIOR AL 10%: '),1,1);
		$pdf->SetXY(120,148);
		$pdf->Cell(75,5,'X',1,1,'C');
		$pdf->SetXY(15,153);
		$pdf->Cell(180,5,'',1,1);
		$pdf->SetXY(15,153);
		$pdf->Cell(105,5,utf8_decode('EDAD SUPERIOR A CINCUENTA(50) AÑOS :'),1,1);
		$pdf->SetXY(120,153);
		$pdf->Cell(75,5,'',1,1,'C');

		$pdf->SetXY(15,158);
		$pdf->Cell(180,5,'',1,1);
		$pdf->SetXY(15,158);
		$pdf->Cell(105,5,'LAVADO ESPECIAL',1,1,'C');
		$pdf->SetXY(120,158);
		$pdf->Cell(75,5,'OBSERVACIONES',1,1,'C');
		$pdf->SetXY(15,163);
		$pdf->Cell(180,5,'',1,1);
		$pdf->SetXY(15,163);
		$pdf->Cell(105,5,'INTERNO POR:',1,1);
		$pdf->SetXY(120,163);
		$pdf->Cell(75,5,'',1,1);
		$pdf->SetXY(15,168);
		$pdf->Cell(180,5,'',1,1);
		$pdf->SetXY(15,168);
		$pdf->Cell(105,5,'EXTERNO POR:',1,1);
		$pdf->SetXY(120,168);
		$pdf->Cell(75,5,'',1,1);

		$pdf->SetXY(15,173);
		$pdf->Cell(180,5,'',1,1);
		$pdf->SetXY(15,173);
		$pdf->Cell(105,5,utf8_decode('EVACUACIÓN POR'),1,1,'C');
		$pdf->SetXY(120,173);
		$pdf->Cell(75,5,'OBSERVACIONES',1,1,'C');
		$pdf->SetXY(15,178);
		$pdf->Cell(105,5,'GAS INFLAMABLE',1,1);
		$pdf->SetXY(120,178);
		$pdf->Cell(75,5,'',1,1);
		$pdf->SetXY(15,183);
		$pdf->Cell(105,5,utf8_decode('GAS TÓXICO'),1,1);
		$pdf->SetXY(120,183);
		$pdf->Cell(75,5,'',1,1);
		$pdf->SetXY(15,188);
		$pdf->Cell(105,5,'GAS CORROSIVO',1,1);
		$pdf->SetXY(120,188);
		$pdf->Cell(75,5,'',1,1);

		$pdf->SetXY(15,193);
		$pdf->Cell(180,5,'',1,1);
		$pdf->SetXY(15,193);
		$pdf->Cell(105,5,'RESULTADO FINAL',1,1,'C');
		$pdf->SetXY(120,193);
		$pdf->Cell(75,5,'RECHAZADO',1,1,'C');

		$pdf->SetXY(15,198);
		$pdf->Cell(180,10,'',1,1);
		$pdf->SetXY(15,198);
		$pdf->Cell(105,10,'',1,1);

		$pdf->SetXY(15,198);
		$pdf->Cell(105,5,'CILINDRO PARA HABILITAR:',1,1);
		$pdf->SetXY(15,203);
		$pdf->Cell(105,5,'CILINDRO PARA DESTRUIR:',1,1);

		$pdf->SetXY(120,198);
		$pdf->Cell(75,5,'',1,1);
		$pdf->SetXY(120,203);
		$pdf->Cell(75,5,'X',1,1,'C');


//Datos de archivo de descarga
$file_name = str_replace(' ','_',$nombre_empresa_ajustado)."_Factura_No._".$num_factura.".pdf";
$file_destination = $path_pdf.$file_name;
$pdf->Output($file_destination,'F');
$pdf->Output($file_name,'I');
?>