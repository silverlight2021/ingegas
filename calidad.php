<?php
session_start();
date_default_timezone_set("America/Bogota");
require ("libraries/conexion.php");
require ("libraries/seguimiento.php");
$fecha_cali_20 = date("d-m-Y");
$hora_cali_20 = date("h:i:s A", time());

if(@$_SESSION['logged']== 'yes')
{ 
    $id_orden = isset($_REQUEST['id_orden']) ? $_REQUEST['id_orden'] : NULL;
    $id_calidad = isset($_REQUEST['id_calidad']) ? $_REQUEST['id_calidad'] : NULL;
    $User_idUser =$_SESSION['su'];
    $acc = $_SESSION['acc'];
    $calidad_20=2;


if ($_POST['g_calidad_90']) 
{
	$id_tapa_90=$_POST['id_tapa_90'];
	$id_tapon_90=$_POST['id_tapon_90'];
	$id_tara_90=$_POST['id_tara_90'];
	$id_barras_90=$_POST['id_barras_90'];
	$id_lote_ven_90=$_POST['id_lote_ven_90'];
	$id_eti_90=$_POST['id_eti_90'];
	$id_collarin_90=$_POST['id_collarin_90'];

  	if ($id_tapa_90 == 1 && $id_tapon_90 == 1 && $id_tara_90 == 1 && $id_barras_90 == 1 && $id_lote_ven_90 == 1 && $id_eti_90 ==1 && $id_collarin_90 == 1 ) 
  	{    
	    $consulta5  = "UPDATE ordenes 
	                  SET id_estado='3' , fecha_fin = '".date("Y/m/d H:i:s")."'
	                  WHERE id_orden =".$id_orden;
	    $resultado5 = mysqli_query($con,$consulta5) ;

	    $consulta1 = "SELECT id_cilindro FROM produccion_mezclas WHERE id_orden = $id_orden ";
	    $resultado1 = mysqli_query($con,$consulta1) ;
	    while ($linea = mysqli_fetch_array($resultado1))
	    {
	    	$id_cilindro_eto = $linea["id_cilindro"];
	    	seguimiento(13,$id_cilindro_eto,$User_idUser,4,$id_orden);
	    }
  	}
	else
	{	  	
	  	$consulta = "SELECT id_cilindro FROM produccion_mezclas WHERE id_orden = $id_orden ";
	    $resultado = mysqli_query($con,$consulta) ;
	    while ($linea = mysqli_fetch_array($resultado))
	    {
	    	$id_cilindro_eto = $linea["id_cilindro"];
	    	seguimiento(14,$id_cilindro_eto,$User_idUser,4,$id_orden);
	    }
	}
         
	if(strlen($id_calidad) > 0)
	{
	    $consulta= "UPDATE calidad SET 
	                id_tapa_90 = '$id_tapa_90' ,
	                id_tapon_90 = '$id_tapon_90' ,
	                id_tara_90 = '$id_tara_90' , 
	                id_barras_90 = '$id_barras_90',
	                id_lote_ven_90 = '$id_lote_ven_90',
	                id_eti_90 = '$id_eti_90', 
	                id_collarin_90 = '$id_collarin_90'                                 
	                WHERE id_calidad = $id_calidad ";
	    $resultado = mysqli_query($con,$consulta) ;

	    if ($resultado == FALSE)
	    {
	      echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
	    }
	    else
	    {
	      header('Location: calidad.php?id_orden='.$id_orden);
	    }
	}
	else
	{
	    $consulta = "INSERT INTO calidad
	              (id_orden,id_tapa_90,id_tapon_90, id_tara_90, id_barras_90,id_lote_ven_90,id_eti_90,id_collarin_90) 
	              VALUES ('".$id_orden."','".$id_tapa_90."', '".$id_tapon_90."', '".$id_tara_90."', '".$id_barras_90."', '".$id_lote_ven_90."', '".$id_eti_90."', '".$id_collarin_90."' )";
	    $resultado = mysqli_query($con,$consulta) ;
	    if ($resultado == FALSE)
	    {
	      echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
	    }
	    else
	    {
	      $id_cilindro = mysqli_insert_id($con);
	      header('Location: calidad.php?id_orden='.$id_orden);
	    }
	}	  
}
if ($_POST['g_calidad_20']) 
{

  	$id_tapa_20=$_POST['id_tapa_20'];
  	$id_capuchon_20=$_POST['id_capuchon_20'];
  	$id_tara_20=$_POST['id_tara_20'];
  	$id_lote_ven_20=$_POST['id_lote_ven_20'];
  	$id_eti_20=$_POST['id_eti_20'];
  	$id_codigo_20=$_POST['id_codigo_20'];

  	if ($id_tapa_20==1 && $id_capuchon_20 ==1 && $id_tara_20 ==1 && $id_lote_ven_20 ==1 && $id_eti_20 ==1 && $id_codigo_20 ==1 ) 
  	{
    	$consulta5  = "UPDATE ordenes 
	                  SET id_estado='3' , fecha_fin = '".date("Y/m/d H:i:s")."'
	                  WHERE id_orden =".$id_orden;
	    $resultado5 = mysqli_query($con,$consulta5) ;

	    $consulta1 = "SELECT id_cilindro FROM produccion_mezclas WHERE id_orden = $id_orden ";
	    $resultado1 = mysqli_query($con,$consulta1) ;
	    while ($linea = mysqli_fetch_array($resultado1))
	    {
	    	$id_cilindro_eto = $linea["id_cilindro"];
	    	seguimiento(13,$id_cilindro_eto,$User_idUser,4,$id_orden);
	    }
  	}
	else
	{	  	
	  	$consulta = "SELECT id_cilindro FROM produccion_mezclas WHERE id_orden = $id_orden ";
	    $resultado = mysqli_query($con,$consulta) ;
	    while ($linea = mysqli_fetch_array($resultado))
	    {
	    	$id_cilindro_eto = $linea["id_cilindro"];
	    	seguimiento(14,$id_cilindro_eto,$User_idUser,4,$id_orden);
	    }
	}


  if(strlen($id_calidad) > 0)
	{
	    $consulta= "UPDATE calidad SET 
	                id_tapa_20 = '$id_tapa_20' ,
	                id_capuchon_20 = '$id_capuchon_20' ,
	                id_tara_20 = '$id_tara_20' , 
	                id_lote_ven_20 = '$id_lote_ven_20',
	                id_eti_20 = '$id_eti_20',
	                id_codigo_20 = '$id_codigo_20'                                
	                WHERE id_calidad = $id_calidad ";
	    $resultado = mysqli_query($con,$consulta) ;

	    if ($resultado == FALSE)
	    {
	      echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
	    }
	    else
	    {
	      header('Location: calidad.php?id_orden='.$id_orden);
	    }
	}
	else
	{
	    $consulta = "INSERT INTO calidad
	              (id_orden,id_tapa_20,id_capuchon_20, id_tara_20, id_lote_ven_20,id_eti_20,id_codigo_20) 
	              VALUES ('".$id_orden."','".$id_tapa_20."', '".$id_capuchon_20."', '".$id_tara_20."', '".$id_lote_ven_20."', '".$id_eti_20."', '".$id_codigo_20."')";
	    $resultado = mysqli_query($con,$consulta) ;
	    if ($resultado == FALSE)
	    {
	      echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
	    }
	    else
	    {
	      $id_cilindro = mysqli_insert_id($con);
	      header('Location: calidad.php?id_orden='.$id_orden);
	    }
	}
  	
}
if ($_POST['g_calidad_100']) 
{
  	$id_tara_100=$_POST['id_tara_100'];
  	$id_codigo_100=$_POST['id_codigo_100'];
  	$id_lote_ven_100=$_POST['id_lote_ven_100'];
  	$id_eti_100=$_POST['id_eti_100'];

    if ($id_tara_100==1 && $id_codigo_100 ==1 && $id_lote_ven_100 ==1 && $id_eti_100 ==1 ) 
    {
      	$consulta5  = "UPDATE ordenes 
	                  SET id_estado='3' , fecha_fin = '".date("Y/m/d H:i:s")."'
	                  WHERE id_orden =".$id_orden;
	    $resultado5 = mysqli_query($con,$consulta5) ;

	    $consulta1 = "SELECT id_cilindro FROM produccion_mezclas WHERE id_orden = $id_orden ";
	    $resultado1 = mysqli_query($con,$consulta1) ;
	    while ($linea = mysqli_fetch_array($resultado1))
	    {
	    	$id_cilindro_eto = $linea["id_cilindro"];
	    	seguimiento(13,$id_cilindro_eto,$User_idUser,4,$id_orden);
	    }
  	}
	else
	{	  	
	  	$consulta = "SELECT id_cilindro FROM produccion_mezclas WHERE id_orden = $id_orden ";
	    $resultado = mysqli_query($con,$consulta) ;
	    while ($linea = mysqli_fetch_array($resultado))
	    {
	    	$id_cilindro_eto = $linea["id_cilindro"];
	    	seguimiento(14,$id_cilindro_eto,$User_idUser,4,$id_orden);
	    }
	}

  	if(strlen($id_calidad) > 0)
	{
	    $consulta= "UPDATE calidad SET 
	                id_tara_100 = '$id_tara_100' ,
	                id_codigo_100 = '$id_codigo_100' ,
	                id_lote_ven_100 = '$id_lote_ven_100' , 
	                id_eti_100 = '$id_eti_100'                              
	                WHERE id_calidad = $id_calidad ";
	    $resultado = mysqli_query($con,$consulta) ;

	    if ($resultado == FALSE)
	    {
	      echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
	    }
	    else
	    {
	      header('Location: calidad.php?id_orden='.$id_orden);
	    }
	}
	else
	{
	    $consulta = "INSERT INTO calidad
	              (id_orden,id_tara_100,id_codigo_100, id_lote_ven_100, id_eti_100) 
	              VALUES ('".$id_orden."','".$id_tara_100."', '".$id_codigo_100."', '".$id_lote_ven_100."', '".$id_eti_100."')";
	    $resultado = mysqli_query($con,$consulta) ;
	    if ($resultado == FALSE)
	    {
	      echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
	    }
	    else
	    {
	      $id_cilindro = mysqli_insert_id($con);
	      header('Location: calidad.php?id_orden='.$id_orden);
	    }
	}  
}


if(strlen($id_orden) > 0)
{ 
    $consulta  = "SELECT * FROM has_orden_cilindro WHERE id_orden= $id_orden";
    $resultado = mysqli_query($con,$consulta) ;
    $linea = mysqli_fetch_array($resultado);

    $id_tipo_cilindro = isset($linea["id_tipo_cilindro"]) ? $linea["id_tipo_cilindro"] : NULL; 

    mysqli_free_result($resultado);
}
if(strlen($id_orden) > 0)
{ 
    $consulta  = "SELECT * FROM ordenes WHERE id_orden= '".$id_orden."'";
    $resultado = mysqli_query($con,$consulta) ;
    if(mysqli_num_rows($resultado) > 0)
    {
      $linea = mysqli_fetch_array($resultado);
      $num_ord = $linea["num_ord"];
      $fecha_ven = isset($linea["fecha_ven"]) ? $linea["fecha_ven"] : NULL;   
    }
    mysqli_free_result($resultado);
}
if(strlen($id_orden) > 0)
{ 
    $consulta  = "SELECT * FROM produccion_mezclas WHERE id_orden= $id_orden";
    $resultado = mysqli_query($con,$consulta) ;
    $linea = mysqli_fetch_array($resultado);

    $lote_pro = isset($linea["lote_pro"]) ? $linea["lote_pro"] : NULL; 

    mysqli_free_result($resultado);
}
if(strlen($id_orden) > 0)
{ 
    $consulta  = "SELECT * FROM calidad WHERE id_orden= $id_orden";
    $resultado = mysqli_query($con,$consulta) ;
    $linea = mysqli_fetch_array($resultado);

    $id_calidad = isset($linea["id_calidad"]) ? $linea["id_calidad"] : NULL; 
    $id_tapa_90 = isset($linea["id_tapa_90"]) ? $linea["id_tapa_90"] : 2; 
    $id_tapon_90 = isset($linea["id_tapon_90"]) ? $linea["id_tapon_90"] : 2; 
    $id_tara_90 = isset($linea["id_tara_90"]) ? $linea["id_tara_90"] : 2; 
    $id_barras_90 = isset($linea["id_barras_90"]) ? $linea["id_barras_90"] : 2; 
    $id_lote_ven_90 = isset($linea["id_lote_ven_90"]) ? $linea["id_lote_ven_90"] : 2; 
    $id_eti_90 = isset($linea["id_eti_90"]) ? $linea["id_eti_90"] : 2; 
    $id_collarin_90 = isset($linea["id_collarin_90"]) ? $linea["id_collarin_90"] : 2;
    $id_tapa_20 = isset($linea["id_tapa_20"]) ? $linea["id_tapa_20"] : 2;
    $id_tara_20 = isset($linea["id_tara_20"]) ? $linea["id_tara_20"] : 2; 
    $id_capuchon_20 = isset($linea["id_capuchon_20"]) ? $linea["id_capuchon_20"] : 2; 
    $id_lote_ven_20 = isset($linea["id_lote_ven_20"]) ? $linea["id_lote_ven_20"] : 2; 
    $id_eti_20 = isset($linea["id_eti_20"]) ? $linea["id_eti_20"] : 2; 
    $id_codigo_20 = isset($linea["id_codigo_20"]) ? $linea["id_codigo_20"] : 2; 
    $id_tara_100 = isset($linea["id_tara_100"]) ? $linea["id_tara_100"] : 2; 
    $id_codigo_100 = isset($linea["id_codigo_100"]) ? $linea["id_codigo_100"] : 2; 
    $id_lote_ven_100 = isset($linea["id_lote_ven_100"]) ? $linea["id_lote_ven_100"] : 2; 
    $id_eti_100 = isset($linea["id_eti_100"]) ? $linea["id_eti_100"] : 2; 

    if ($id_tapa_90==2 ||$id_tapon_90 ==2 ||$id_tara_90 ==2 ||$id_barras_90 ==2 ||$id_lote_ven_90 ==2 ||$id_eti_90 ==2 ||$id_collarin_90 ==2 ) 
  	{
  		$conformidad=1;		 
  	}
    
  	if ($id_tapa_20==2 || $id_capuchon_20 ==2 || $id_tara_20 ==2 || $id_lote_ven_20 ==2 || $id_eti_20 ==2 || $id_codigo_20 ==2 ) 
  	{
    	$calidad_20=1;    
  	}
    
  	if ($id_tara_100==2 || $id_codigo_100 ==2 || $id_lote_ven_100 ==2 || $id_eti_100 ==2 ) 
  	{
    	$calidad_100=1;    
  	}
  
  	
     	

    mysqli_free_result($resultado);
}

require_once("inc/init.php");
require_once("inc/config.ui.php");
$page_title = "Calidad";
$page_css[] = "your_style.css";
include("inc/header.php");
$page_nav[""][""][""][""] = true;
include("inc/nav.php");
?>

<div id="main" role="main">
  <div id="content">
    <section id="widget-grid" class="">
      <div class="row">
        <?php
        if ($id_tipo_cilindro==3) 
        {
          
        ?>
        <article class="col-sm-12 col-md-12 col-lg-6">
          <div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
            <header>
              <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
              <h2>Chequeo : ETO 90 </h2>
            </header>
            <div>
              <div class="jarviswidget-editbox"></div>              
              <div class="widget-body no-padding">
                <form id="checkout-form" class="smart-form" novalidate="novalidate" action="calidad.php" method="POST">
                <input type="hidden" name="id_orden" id="id_orden" value="<?php echo $id_orden; ?>">
                <input type="hidden" name="id_calidad" id="id_calidad" value="<?php echo $id_calidad; ?>">
                  <fieldset>
                  <?php 
              		if($conformidad==1)
              		{
                  ?>
                  	<div class="alert alert-danger alert-block">
    							  <h4 class="alert-heading">¡Cuidado!</h4>
    							     Por favor revise los items de no conformidad
    						    </div>                  		
                	<?php
                	}                  	
                	?>
                  <div class="row">         
                      <section class="col col-6">
                        <label>Fecha Revisión Calidad :</label>
                        <label class="input">                      
                        <input type="text" readonly class="input-lg" placeholder="Fecha Revisión Calidad" value="<?php echo isset($fecha_cali_20) ? $fecha_cali_20 : NULL; ?>" />
                        </label>
                      </section>
                      <section class="col col-6">
                        <label for="category">Hora Revisión Calidad :</label>
                        <label class="input">                        
                        <input type="text" readonly class="input-lg" placeholder="Hora Revisión Calidad" value="<?php echo isset($hora_cali_20) ? $hora_cali_20 : NULL; ?>" />
                        </label>
                      </section> 
                    </div>
                  <fieldset>
                    <div class="row"> 
                        <section class="col col-4">
                          <label for="category">Lote :</label>
                          <label class="input">
                            <input type="text" readonly class="input-lg" placeholder="Lote Producción" value="<?php echo isset($lote_pro) ? $lote_pro : NULL; ?>" />  
                          </label>
                        </section>
                        <section class="col col-4">
                          <label for="category">Fecha Ven :</label>                       
                          <label class="input">
                            <input type="text" readonly class="input-lg" placeholder="Fecha Ven" value="<?php echo isset($fecha_ven) ? $fecha_ven : NULL; ?>" />  
                          </label>
                        </section>                        
                        <section class="col col-4">
                          <label for="category">Orden de producción :</label>                      
                          <label class="input">
                            <input type="text" readonly class="input-lg" placeholder="Orden de producción" value="<?php echo isset($num_ord) ? $num_ord : NULL; ?>"/>  
                          </label>
                        </section>
                    </div>
                  </fieldset>
                  <fieldset>
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label class="label">Tapa :</label>                            
                        </div>  
                      </div>                                        
                      <div class="col-sm-6">
                        <div class="form-group">                            
                          <div class="inline-group">
                            <label class="radio">
                              <input type="radio" name="id_tapa_90" value="1" <?php if($id_tapa_90=='1') print "checked=true"?> >
                              <i></i>Si</label>
                            <label class="radio">
                              <input type="radio" name="id_tapa_90" value="2" <?php if($id_tapa_90=='2') print "checked=true"?> >
                              <i></i>No</label>                     
                          </div>
                        </div>  
                      </div>
                    </div>  
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label class="label">Tapón :</label>                            
                        </div>  
                      </div>   
                      <div class="col-sm-6">
                        <div class="form-group">
                          <div class="inline-group">
                            <label class="radio">
                              <input type="radio" name="id_tapon_90"  value="1" <?php if($id_tapon_90=='1') print "checked=true"?> >
                              <i></i>Si</label>
                            <label class="radio">
                              <input type="radio" name="id_tapon_90" value="2" <?php if($id_tapon_90=='2') print "checked=true"?> >
                              <i></i>No</label>                     
                          </div>
                        </div>  
                      </div>
                    </div>  
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label class="label">Tara Vacío :</label>                            
                        </div>  
                      </div>   
                      <div class="col-sm-6">
                        <div class="form-group">
                          <div class="inline-group">
                            <label class="radio">
                              <input type="radio" name="id_tara_90" value="1" <?php if($id_tara_90=='1') print "checked=true"?> >
                              <i></i>Si</label>
                            <label class="radio">
                              <input type="radio" name="id_tara_90" value="2" <?php if($id_tara_90=='2') print "checked=true"?> >
                              <i></i>No</label>                     
                          </div>
                        </div>  
                      </div>
                    </div>  
                    <div class="row"> 
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label class="label">Código de Barras :</label>                            
                        </div>  
                      </div>   
                      <div class="col-sm-6">
                        <div class="form-group">
                          <div class="inline-group">
                            <label class="radio">
                              <input type="radio" name="id_barras_90" value="1" <?php if($id_barras_90=='1') print "checked=true"?> >
                              <i></i>Si</label>
                            <label class="radio">
                              <input type="radio" name="id_barras_90" value="2" <?php if($id_barras_90=='2') print "checked=true"?> >
                              <i></i>No</label>                     
                          </div>
                        </div>  
                      </div>  
                    </div>
                    <div class="row"> 
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label class="label">Etiqueta de Lote y Vencimiento :</label>                            
                        </div>  
                      </div>   
                      <div class="col-sm-6">
                        <div class="form-group">
                          <div class="inline-group">
                            <label class="radio">
                              <input type="radio" name="id_lote_ven_90" value="1" <?php if($id_lote_ven_90=='1') print "checked=true"?> >
                              <i></i>Si</label>
                            <label class="radio">
                              <input type="radio" name="id_lote_ven_90" value="2" <?php if($id_lote_ven_90=='2') print "checked=true"?> >
                              <i></i>No</label>                     
                          </div>
                        </div>  
                      </div>  
                    </div>
                    <div class="row"> 
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label class="label">Etiqueta de Producto :</label>                            
                        </div>  
                      </div>   
                      <div class="col-sm-6">
                        <div class="form-group">
                          <div class="inline-group">
                            <label class="radio">
                              <input type="radio" name="id_eti_90" value="1" <?php if($id_eti_90=='1') print "checked=true"?> >
                              <i></i>Si</label>
                            <label class="radio">
                              <input type="radio" name="id_eti_90" value="2" <?php if($id_eti_90=='2') print "checked=true"?> >
                              <i></i>No</label>                     
                          </div>
                        </div>  
                      </div>  
                    </div>
                    <div class="row"> 
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label class="label">Collarin :</label>                            
                        </div>  
                      </div>   
                      <div class="col-sm-6">
                        <div class="form-group">
                          <div class="inline-group">
                            <label class="radio">
                              <input type="radio" name="id_collarin_90" value="1" <?php if($id_collarin_90=='1') print "checked=true"?> >
                              <i></i>Si</label>
                            <label class="radio">
                              <input type="radio" name="id_collarin_90" value="2" <?php if($id_collarin_90=='2') print "checked=true"?> >
                              <i></i>No</label>                     
                          </div>
                        </div>  
                      </div>  
                    </div>
                  </fieldset>
                  <?php
                  if (in_array(24, $acc))
                  {
                  ?>  
                  <footer>
                        <?php 
                        if($conformidad==1)
                        {
                        ?>
                          <input type="submit" value="Guardar" name="g_calidad_90" id="g_calidad_90" class="btn btn-primary" />                          
                        <?php
                        } 
                        else
                        { 
                        ?>
                          <a href="calidad_pdf.php?id_orden=<?php echo $id_orden; ?>" target="_blank"><img src="img/pdf-icon.png" width="50" height="50"></a>
                        <?php                     
                        }
                        ?>

                  </footer>
                  <?php
                  }
                  ?> 
                </form> 
              </div>  
            </div>
          </div>
        </article>
        <?php
        }
        if ($id_tipo_cilindro==1||$id_tipo_cilindro==2||$id_tipo_cilindro==10) 
        {         
        ?>
        <article class="col-sm-12 col-md-12 col-lg-6">
          <div class="jarviswidget" id="wid-id-2" data-widget-editbutton="false" data-widget-custombutton="false">
            <header>
              <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
              <h6>Chequeo : ETO 20 y 10 </h6>
            </header>
            <div>
              <div class="jarviswidget-editbox"></div>              
              <div class="widget-body no-padding">
                <form id="checkout-form" class="smart-form" novalidate="novalidate" action="calidad.php" method="POST">
                <input type="hidden" name="id_orden" id="id_orden" value="<?php echo $id_orden; ?>">
                <input type="hidden" name="id_calidad" id="id_calidad" value="<?php echo $id_calidad; ?>">
                  <fieldset>
                  	<?php                  	
                  	if($calidad_20==1)
                  	{
                  	?>
                  		<div class="alert alert-danger alert-block">
          							<h4 class="alert-heading">¡Cuidado!</h4>
          							Por favor revise los items de no conformidad
          						</div>                  		
                  	<?php
                    }
                    ?>
                    </fieldset>
                    <fieldset>
                    <div class="row">         
                      <section class="col col-6">
                        <label>Fecha Revisión Calidad :</label>
                        <label class="input">                      
                        <input type="text" readonly class="input-lg" placeholder="Fecha Revisión Calidad" value="<?php echo isset($fecha_cali_20) ? $fecha_cali_20 : NULL; ?>" />
                        </label>
                      </section>
                      <section class="col col-6">
                        <label for="category">Hora Revisión Calidad :</label>
                        <label class="input">                        
                        <input type="text" readonly class="input-lg" placeholder="Hora Revisión Calidad" value="<?php echo isset($hora_cali_20) ? $hora_cali_20 : NULL; ?>" />
                        </label>
                      </section> 
                    </div>
                    </fieldset>
                    <fieldset>
                    <div class="row">         
                      <section class="col col-4">
                        <label for="category">Lote :</label>
                          <label class="input">                      
                          <input type="text" readonly class="input-lg" placeholder="Lote Producción" value="<?php echo isset($lote_pro) ? $lote_pro : NULL; ?>" />
                          </label>
                      </section>
                      <section class="col col-4">
                        <label for="category">Fecha Ven :</label>
                          <label class="input">
                            <input type="text" readonly class="input-lg" placeholder="Fecha Ven" value="<?php echo isset($fecha_ven) ? $fecha_ven : NULL; ?>" />  
                          </label>
                      </section>                        
                      <section class="col col-4">
                        <label for="category">Orden de producción :</label>
                          <label class="input">
                            <input type="text" readonly class="input-lg" placeholder="Tara" value="<?php echo isset($num_ord) ? $num_ord : NULL; ?>" />  
                          </label>
                      </section>
                    </div>
                    </fieldset>
                    <fieldset>
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label class="label">Tapa :</label>                            
                        </div>  
                      </div>                                        
                      <div class="col-sm-6">
                        <div class="form-group">                            
                          <div class="inline-group">
                            <label class="radio">
                              <input type="radio" name="id_tapa_20" value="1" <?php if($id_tapa_20=='1') print "checked=true"?> >
                              <i></i>Si</label>
                            <label class="radio">
                              <input type="radio" name="id_tapa_20" value="2" <?php if($id_tapa_20=='2') print "checked=true"?> >
                              <i></i>No</label>                     
                          </div>
                        </div>  
                      </div>
                    </div>  
                    <div class="row"> 
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label class="label">Termoencogible :</label>                            
                        </div>  
                      </div>   
                      <div class="col-sm-6">
                        <div class="form-group">
                          <div class="inline-group">
                            <label class="radio">
                              <input type="radio" name="id_capuchon_20" value="1" <?php if($id_capuchon_20=='1') print "checked=true"?> >
                              <i></i>Si</label>
                            <label class="radio">
                              <input type="radio" name="id_capuchon_20" value="2" <?php if($id_capuchon_20=='2') print "checked=true"?> >
                              <i></i>No</label>                     
                          </div>
                        </div>  
                      </div>  
                    </div>
                    <div class="row"> 
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label class="label">Tara Vacío :</label>                                                        
                        </div>  
                      </div>   
                      <div class="col-sm-6">
                        <div class="form-group">
                          <div class="inline-group">
                            <label class="radio">
                              <input type="radio" name="id_tara_20" value="1" <?php if($id_tara_20=='1') print "checked=true"?> >
                              <i></i>Si</label>
                            <label class="radio">
                              <input type="radio" name="id_tara_20" value="2" <?php if($id_tara_20=='2') print "checked=true"?> >
                              <i></i>No</label>                     
                          </div>
                        </div>  
                      </div>  
                    </div>
                    <div class="row"> 
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label class="label">Código de Barras :</label>                            
                        </div>  
                      </div>   
                      <div class="col-sm-6">
                        <div class="form-group">
                          <div class="inline-group">
                            <label class="radio">
                              <input type="radio" name="id_codigo_20" value="1" <?php if($id_codigo_20=='1') print "checked=true"?> >
                              <i></i>Si</label>
                            <label class="radio">
                              <input type="radio" name="id_codigo_20" value="2" <?php if($id_codigo_20=='2') print "checked=true"?> >
                              <i></i>No</label>                     
                          </div>
                        </div>  
                      </div>  
                    </div>
                    <div class="row"> 
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label class="label">Etiqueta de Lote y Vencimiento :</label>                            
                        </div>  
                      </div>   
                      <div class="col-sm-6">
                        <div class="form-group">
                          <div class="inline-group">
                            <label class="radio">
                              <input type="radio" name="id_lote_ven_20" value="1" <?php if($id_lote_ven_20=='1') print "checked=true"?> >
                              <i></i>Si</label>
                            <label class="radio">
                              <input type="radio" name="id_lote_ven_20" value="2" <?php if($id_lote_ven_20=='2') print "checked=true"?> >
                              <i></i>No</label>                     
                          </div>
                        </div>  
                      </div>  
                    </div>
                    <div class="row"> 
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label class="label">Etiqueta de Producto :</label>                            
                        </div>  
                      </div>   
                      <div class="col-sm-6">
                        <div class="form-group">
                          <div class="inline-group">
                            <label class="radio">
                              <input type="radio" name="id_eti_20" value="1" <?php if($id_eti_20=='1') print "checked=true"?> >
                              <i></i>Si</label>
                            <label class="radio">
                              <input type="radio" name="id_eti_20" value="2" <?php if($id_eti_20=='2') print "checked=true"?> >
                              <i></i>No</label>                     
                          </div>
                        </div>  
                      </div>  
                    </div>
                  </fieldset>
                  <?php
                  if (in_array(24, $acc))
                  {
                  ?> 
                  <footer>                    
                        
                        <?php 
                        if($calidad_20==1)
                        {
                        ?>
                         <input type="submit" value="Guardar" name="g_calidad_20" id="g_calidad_20" class="btn btn-primary" />
                        <?php 
                        }
                        else
                        {   
                        ?>
                          <a href="calidad_pdf.php?id_orden=<?php echo $id_orden; ?>" target="_blank"><img src="img/pdf-icon.png" width="50" height="50"></a>
                        <?php 
                        }
                        ?>
                  </footer>
                  <?php
                  }
                  ?>
                </form> 
              </div>  
            </div>
          </div>
        </article>
        <?php
        }
        if ($id_tipo_cilindro==4) 
        {         
        ?>
        <article class="col-sm-12 col-md-12 col-lg-6">
          <div class="jarviswidget" id="wid-id-3" data-widget-editbutton="false" data-widget-custombutton="false">
            <header>
              <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
              <h2>Chequeo : ETO  100 </h2>
            </header>
            <div>
              <div class="jarviswidget-editbox"></div>              
              <div class="widget-body no-padding">
                <form id="checkout-form" class="smart-form" novalidate="novalidate" action="calidad.php" method="POST">
                <input type="hidden" name="id_orden" id="id_orden" value="<?php echo $id_orden; ?>">
                <input type="hidden" name="id_calidad" id="id_calidad" value="<?php echo $id_calidad; ?>">
                  <fieldset>
                  	<?php                  	
                  	if($calidad_100==1)
                  	{
                  	?>
                  		<div class="alert alert-danger alert-block">
          							<h4 class="alert-heading">¡Cuidado!</h4>
          							Por favor revise los items de no conformidad
          						</div>                  		
                  	<?php
                    }
                    ?>
                    <div class="row">         
                      <section class="col col-4">
                          <label for="category">Lote :</label>
                          <label class="input">
                            <input type="text" readonly class="form-control" placeholder="Lote Producción" value="<?php echo isset($lote_pro) ? $lote_pro : NULL; ?>" />  
                          </label>
                      </section>
                      <section class="col col-4">
                          <label for="category">Fecha Ven :</label>
                          <label class="input">
                            <input type="text" readonly class="form-control" placeholder="Fecha Ven" value="<?php echo isset($fecha_ven) ? $fecha_ven : NULL; ?>" />  
                          </label>
                      </section>
                      <section class="col col-4">
                          <label for="category">Orden de producción:</label>
                          <label class="input">
                            <input type="text" readonly class="form-control" placeholder="Tara" value="<?php echo isset($num_ord) ? $num_ord : NULL; ?>" />  
                          </label>
                      </section>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label class="label">Tara Vacío :</label>                                                        
                        </div>  
                      </div>   
                      <div class="col-sm-6">
                        <div class="form-group">
                          <div class="inline-group">
                            <label class="radio">
                              <input type="radio" name="id_tara_100" value="1" <?php if($id_tara_100=='1') print "checked=true"?> >
                              <i></i>Si</label>
                            <label class="radio">
                              <input type="radio" name="id_tara_100" value="2" <?php if($id_tara_100=='2') print "checked=true"?> >
                              <i></i>No</label>                     
                          </div>
                        </div>  
                      </div>  
                    </div>
                    <div class="row"> 
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label class="label">Código de Barras :</label>                            
                        </div>  
                      </div>   
                      <div class="col-sm-6">
                        <div class="form-group">
                          <div class="inline-group">
                            <label class="radio">
                              <input type="radio" name="id_codigo_100" value="1" <?php if($id_codigo_100=='1') print "checked=true"?> >
                              <i></i>Si</label>
                            <label class="radio">
                              <input type="radio" name="id_codigo_100" value="2" <?php if($id_codigo_100=='2') print "checked=true"?> >
                              <i></i>No</label>                     
                          </div>
                        </div>  
                      </div>  
                    </div>
                    <div class="row"> 
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label class="label">Etiqueta de Lote y Vencimiento :</label>                            
                        </div>  
                      </div>   
                      <div class="col-sm-6">
                        <div class="form-group">
                          <div class="inline-group">
                            <label class="radio">
                              <input type="radio" name="id_lote_ven_100" value="1" <?php if($id_lote_ven_100=='1') print "checked=true"?> >
                              <i></i>Si</label>
                            <label class="radio">
                              <input type="radio" name="id_lote_ven_100" value="2" <?php if($id_lote_ven_100=='2') print "checked=true"?> >
                              <i></i>No</label>                     
                          </div>
                        </div>  
                      </div>  
                    </div>
                    <div class="row"> 
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label class="label">Etiqueta de Producto :</label>                            
                        </div>  
                      </div>   
                      <div class="col-sm-6">
                        <div class="form-group">
                          <div class="inline-group">
                            <label class="radio">
                              <input type="radio" name="id_eti_100" value="1" <?php if($id_eti_100=='1') print "checked=true"?> >
                              <i></i>Si</label>
                            <label class="radio">
                              <input type="radio" name="id_eti_100" value="2" <?php if($id_eti_100=='2') print "checked=true"?> >
                              <i></i>No</label>                     
                          </div>
                        </div>  
                      </div>  
                    </div>                    
                  </fieldset> 
                  <?php
                  if (in_array(24, $acc))
                  {
                  ?> 
                  <footer>
                  <?php 
                    if($calidad_100==1)
                    {
                    ?>
                      <input type="submit" value="Guardar" name="g_calidad_100" id="g_calidad_100" class="btn btn-primary" />
                    <?php
                    } 
                    else
                    {   
                    ?>                          
                      <a href="calidad_pdf.php?id_orden=<?php echo $id_orden; ?>" target="_blank"><img src="img/pdf-icon.png" width="50" height="50"></a>
                    <?php 
                    }
                  ?>
                  </footer>
                  <?php
                  }
                  ?> 
                </form> 
              </div>  
            </div>
          </div>
        </article>
        <?php
        }                
        ?>
      </div>
    </section>
  </div>
  <!-- END MAIN CONTENT -->
</div>

<?php
  include("inc/footer.php");
  include("inc/scripts.php"); 
?>

<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>

<!-- Vector Maps Plugin: Vectormap engine, Vectormap language -->
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>

<!-- Full Calendar -->
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/jquery-form/jquery-form.min.js"></script>


<script type="text/javascript">

  $(document).ready(function() 
  {

    var $checkoutForm = $('#checkout-form').validate(
    {
    // Rules for form validation
      rules :
      {
        name1 : {
          required : true
        },
        idgender1 : {
          required : true
        },
        last_name : {
          required : true
        },
        home_phone : {
          required : true
        },
        primary_mail : {
          required : true
        },
        street_adress1 : {
          required : true
        }
      },

      // Messages for form validation
      messages : {
        name1 : {
          required : 'Please enter your first name'
        },
        idgender1 : {
          required : ''
        },
        last_name : {
          required : 'Please enter your last name'
        },
        home_phone : {
          required : 'Please enter your phone'
        },
        primary_mail : {
          required : 'Please enter your mail'
        },
        street_adress1 : {
          required : 'Please enter your Adress'
        }
      },

      // Do not change code below
      errorPlacement : function(error, element) {
        error.insertAfter(element.parent());
      }
    }); 

    }) 
   

</script>


<?php 
  //include footer
  include("inc/google-analytics.php"); 
?>
<?php

}
else
{
    header("Location:index.php");
}
?>
                                 
                    