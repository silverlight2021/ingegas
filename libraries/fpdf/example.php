<?php
// (c) Xavier Nicolay
// Exemple de génération de devis/facture PDF

/////////////////VARIABLES///////////////////
$path_imagenes = "../";
$path_pdf = "../pdf/factura/";

$logotipo_empresa = $path_imagenes."logo_happy_mundo.jpg";
$tipo_imagen = "JPG";

$nombre_empresa = utf8_decode("Happymundo Ltda.");
$nombre_empresa_ajustado = utf8_decode(substr($nombre_empresa, 0, 47));

$Tipo_Documento_empresa = "NIT";
$Documento_empresa = "900209242-1";
$Documento_empresa_ajustado = substr($Tipo_Documento_empresa." ".$Documento_empresa, 0, 47);

$direccion_empresa = utf8_decode("Calle 74 #15-80 Oficina 206 Interior 1");
$ciudad_empresa = utf8_decode("Bogota");
$depto_empresa = utf8_decode("Cundinamarca");
$direccion_empresa_ajustada = substr($direccion_empresa." ".$ciudad_empresa." ".$depto_empresa, 0, 70);

$telefono_empresa = "6065290";
$tel_movil_empresa = "3134100433";
$telefono_empresa_ajustado = substr("Tel(s): ".$telefono_empresa." // ".$tel_movil_empresa, 0, 70);

$email_empresa = "camiloosorio@silverlight.com.co";

$num_factura = 999999999;

$tipo_factura = "Original Factura";//Original Factura, Copia Factura, Factura Anulada

$fecha_emision_factura = "2015-03-20";
$fecha_vencimiento_factura = "2015-03-31";

$Clasificacion_contribuyente = utf8_decode("Régimen Común");
if($Clasificacion_contribuyente == "Gran Contribuyente")
{
	$Gran_Contribuyente = "Somos Grandes Contribuyentes";
}
else
{
	$Gran_Contribuyente = "No Somos Grandes Contribuyentes";
}

$num_res_facturacion = "123456789012";
$fecha_res_facturacion = "2015-03-24";
$num1_res_facturacion = "10000001";
$num2_res_facturacion = "99999999";
$res_facturacion = utf8_decode("Resolución DIAN No.".$num_res_facturacion." Fecha ".$fecha_res_facturacion." - Habilita Facturación por computador de la ".$num1_res_facturacion." a la ".$num2_res_facturacion);

$nombre_cliente = utf8_decode(substr("Silverlight Colombia Ltda.", 0, 55));
$ciudad_cliente = "Bogotá";
$direccion_cliente = "Calle 74 #15-80 Oficina 206 Interior 1";
$direccion_completa_cliente = utf8_decode(substr($ciudad_cliente." ".$direccion_cliente, 0, 52));
$documento_cliente = "900209242";
$dv_cliente = "1";

$telefono1_cliente = "6065290";
$telMovil1_cliente = "3134100433";
$telefonos_cliente = substr($telefono1_cliente." // ".$telMovil1_cliente, 0, 24);

$forma_pago = utf8_decode(substr("Crédito 30 Días", 0, 52));

$observacion1 = utf8_decode(substr("Lugar Para incluir Observaciones dentro de la factura", 0, 83));
$observacion2 = utf8_decode(substr("Lugar Para incluir Observaciones dentro de la factura", 0, 83));
$observacion3 = utf8_decode(substr("Lugar Para incluir Observaciones dentro de la factura", 0, 450));
////////////////////////////////////////////

require('invoice.php');

$pdf = new PDF_Invoice( 'P', 'mm', 'A4' );
$pdf->AddPage();
$pdf->Image($logotipo_empresa,5,5,0,0,$tipo_imagen);
$pdf->addDatosEmpresa( $nombre_empresa_ajustado,
                  $Documento_empresa_ajustado."\n".
                  $direccion_empresa_ajustada."\n".
                  $telefono_empresa_ajustado."\n".
				  $email_empresa);
$pdf->fact_dev( "Factura de Venta ", $num_factura );
$pdf->tipo_fact( $tipo_factura );
$pdf->addDate( $fecha_emision_factura );
$pdf->addDate2($fecha_vencimiento_factura);
$pdf->addPageNumber($pdf->PageNo());
$pdf->addFactData($Gran_Contribuyente."\n".
						$res_facturacion);
$pdf->addCLientName($nombre_cliente);
$pdf->addCLientAdress($direccion_completa_cliente);
$pdf->addCLientDoc($documento_cliente."-".$dv_cliente);
$pdf->addClientPhone($telefonos_cliente);
$pdf->addPayMode($forma_pago);
$pdf->addReference($observacion1);
$pdf->addObserv2($observacion2);
$cols=array( "REFERENCIA"    => 23,
             "DESCRIPCION"  => 78,
             "CANTIDAD"     => 22,
             "V.UNITARIO"      => 26,
             "V.TOTAL" => 30,
             "IVA"          => 11 );
$pdf->addCols( $cols);
$cols=array( "REFERENCIA"    => "L",
             "DESCRIPCION"  => "L",
             "CANTIDAD"     => "C",
             "V.UNITARIO"      => "R",
             "V.TOTAL" => "R",
             "IVA"          => "C" );
$pdf->addLineFormat( $cols);

//////////////////////////////////////////////
$filas = 0;

$filas += 1;
$y    = 102;
$referencia = "REF1";
$descripcion = utf8_decode("Computador MSI 6378 Procesador AMD 1Ghz 128Mo SDRAM, 30 GB SSD, CD-ROM, Floppy, Tarjeta de Video Independiente Nvidia GForce XXX");
$descripcion_completa = wordwrap($descripcion,38,"	\n");
$cantidad = "1";
$valor_unitario1 = 1234567;
$valor_unitario = number_format($valor_unitario1, 2, ",", ".");
$valor_total1 = 12345678;
$valor_total = number_format($valor_total1, 2, ",", ".");
$iva = 16;
$filas += substr_count($descripcion_completa, '	');
$line = array( "REFERENCIA"    => $referencia,
               "DESCRIPCION"  => $descripcion_completa." ".$filas,
               "CANTIDAD"     => $cantidad,
               "V.UNITARIO"      => $valor_unitario,
               "V.TOTAL" => $valor_total,
               "IVA"          => $iva );
$size = $pdf->addLine( $y, $line );
$y   += $size + 2;

$filas += 1;
$referencia = "REF2";
$descripcion = utf8_decode("Cable RS232");
$descripcion_completa = wordwrap($descripcion,38,"	\n");
$cantidad = "1";
$valor_unitario1 = 1234567;
$valor_unitario = number_format($valor_unitario1, 2, ",", ".");
$valor_total1 = 12345678;
$valor_total = number_format($valor_total1, 2, ",", ".");
$iva = 16;
$filas += substr_count($descripcion_completa, '	');
$line = array( "REFERENCIA"    => $referencia,
               "DESCRIPCION"  => $descripcion_completa." ".$filas,
               "CANTIDAD"     => $cantidad,
               "V.UNITARIO"      => $valor_unitario,
               "V.TOTAL" => $valor_total,
               "IVA"          => $iva );
$size = $pdf->addLine( $y, $line );
$y   += $size + 2;

$filas += 1;
$referencia = "REF2";
$descripcion = utf8_decode("Cable RS232");
$descripcion_completa = wordwrap($descripcion,38,"	\n");
$cantidad = "1";
$valor_unitario1 = 1234567;
$valor_unitario = number_format($valor_unitario1, 2, ",", ".");
$valor_total1 = 12345678;
$valor_total = number_format($valor_total1, 2, ",", ".");
$iva = 16;
$filas += substr_count($descripcion_completa, '	');
$line = array( "REFERENCIA"    => $referencia,
               "DESCRIPCION"  => $descripcion_completa." ".$filas,
               "CANTIDAD"     => $cantidad,
               "V.UNITARIO"      => $valor_unitario,
               "V.TOTAL" => $valor_total,
               "IVA"          => $iva );
$size = $pdf->addLine( $y, $line );
$y   += $size + 2;

$filas += 1;
$referencia = "REF3";
$descripcion = utf8_decode("Producto 3");
$descripcion_completa = wordwrap($descripcion,38,"	\n");
$cantidad = "1";
$valor_unitario1 = 1234567;
$valor_unitario = number_format($valor_unitario1, 2, ",", ".");
$valor_total1 = 12345678;
$valor_total = number_format($valor_total1, 2, ",", ".");
$iva = 16;
$filas += substr_count($descripcion_completa, '	');
$line = array( "REFERENCIA"    => $referencia,
               "DESCRIPCION"  => $descripcion_completa." ".$filas,
               "CANTIDAD"     => $cantidad,
               "V.UNITARIO"      => $valor_unitario,
               "V.TOTAL" => $valor_total,
               "IVA"          => $iva );
$size = $pdf->addLine( $y, $line );
$y   += $size + 2;

$filas += 1;
$referencia = "REF4";
$descripcion = utf8_decode("Producto 4");
$descripcion_completa = wordwrap($descripcion,38,"	\n");
$cantidad = "1";
$valor_unitario1 = 1234567;
$valor_unitario = number_format($valor_unitario1, 2, ",", ".");
$valor_total1 = 12345678;
$valor_total = number_format($valor_total1, 2, ",", ".");
$iva = 16;
$filas += substr_count($descripcion_completa, '	');
$line = array( "REFERENCIA"    => $referencia,
               "DESCRIPCION"  => $descripcion_completa." ".$filas,
               "CANTIDAD"     => $cantidad,
               "V.UNITARIO"      => $valor_unitario,
               "V.TOTAL" => $valor_total,
               "IVA"          => $iva );
$size = $pdf->addLine( $y, $line );
$y   += $size + 2;

$filas += 1;
$referencia = "REF5";
$descripcion = utf8_decode("Producto 5");
$descripcion_completa = wordwrap($descripcion,38,"	\n");
$cantidad = "1";
$valor_unitario1 = 1234567;
$valor_unitario = number_format($valor_unitario1, 2, ",", ".");
$valor_total1 = 12345678;
$valor_total = number_format($valor_total1, 2, ",", ".");
$iva = 16;
$filas += substr_count($descripcion_completa, '	');
$line = array( "REFERENCIA"    => $referencia,
               "DESCRIPCION"  => $descripcion_completa." ".$filas,
               "CANTIDAD"     => $cantidad,
               "V.UNITARIO"      => $valor_unitario,
               "V.TOTAL" => $valor_total,
               "IVA"          => $iva );
$size = $pdf->addLine( $y, $line );
$y   += $size + 2;

$filas += 1;
$referencia = "REF6";
$descripcion = utf8_decode("Lenovo G40 70, IdeaPad. Procesador 2.2 GHz, Intel Core i7 4500U. Memoria interna: 6 GB DDR3-SDRAM Velocidad de memoria del reloj: 1600 MHz Capacidad de disco duro: 1000 GB. WINDOWS 8 Pantalla: 14' HD LED (1366x768), 16:9 widescree WebCam: integrada Sonido: Dispositivos de sonido HD SRS Premium Sound Unidad óptica: DVD-RW Lector Tarjetas Multimedia: SD Teclado: Español, negro Touchpad: Multi-Touch Dispositivos de Comunicaciones. Lan: 10/100M WiFi: Sí Bluetooth: Sí Puertos de Entrada/Salida.  1 x HDMI 1 x VGA 3 x USB 1 x eSATA 1 x LAN 1 x Audífono 1 x Micrófono");
$descripcion_completa = wordwrap($descripcion,38,"	\n");
$cantidad = "1";
$valor_unitario1 = 1234567;
$valor_unitario = number_format($valor_unitario1, 2, ",", ".");
$valor_total1 = 12345678;
$valor_total = number_format($valor_total1, 2, ",", ".");
$iva = 16;
$filas += substr_count($descripcion_completa, '	');
$line = array( "REFERENCIA"    => $referencia,
               "DESCRIPCION"  => $descripcion_completa." ".$filas,
               "CANTIDAD"     => $cantidad,
               "V.UNITARIO"      => $valor_unitario,
               "V.TOTAL" => $valor_total,
               "IVA"          => $iva );
$size = $pdf->addLine( $y, $line );
$y   += $size + 2;

$filas += 1;
$referencia = "REF7";
$descripcion = utf8_decode("Producto 7");
$descripcion_completa = wordwrap($descripcion,38,"	\n");
$cantidad = "1";
$valor_unitario1 = 1234567;
$valor_unitario = number_format($valor_unitario1, 2, ",", ".");
$valor_total1 = 12345678;
$valor_total = number_format($valor_total1, 2, ",", ".");
$iva = 16;
$filas += substr_count($descripcion_completa, '	');
$line = array( "REFERENCIA"    => $referencia,
               "DESCRIPCION"  => $descripcion_completa." ".$filas,
               "CANTIDAD"     => $cantidad,
               "V.UNITARIO"      => $valor_unitario,
               "V.TOTAL" => $valor_total,
               "IVA"          => $iva );
$size = $pdf->addLine( $y, $line );
$y   += $size + 2;

$filas += 1;
$referencia = "REF8";
$descripcion = utf8_decode("Producto 8");
$descripcion_completa = wordwrap($descripcion,38,"	\n");
$cantidad = "1";
$valor_unitario1 = 1234567;
$valor_unitario = number_format($valor_unitario1, 2, ",", ".");
$valor_total1 = 12345678;
$valor_total = number_format($valor_total1, 2, ",", ".");
$iva = 16;
$filas += substr_count($descripcion_completa, '	');
$line = array( "REFERENCIA"    => $referencia,
               "DESCRIPCION"  => $descripcion_completa." ".$filas,
               "CANTIDAD"     => $cantidad,
               "V.UNITARIO"      => $valor_unitario,
               "V.TOTAL" => $valor_total,
               "IVA"          => $iva );
$size = $pdf->addLine( $y, $line );
$y   += $size + 2;
/////////////////////////////////////////////////////////////

$pdf->addObserv3($observacion3);
$pdf->addSign($nombre_empresa_ajustado);
$pdf->addSign2();

$pdf->addTotalInvoice();

$neto = 1234567890;
$neto = "$ ".number_format($neto, 2, ",", ".");
$descuento = 1234567890;
$descuento = "$ ".number_format($descuento, 2, ",", ".");
$subtotal = 1234567890;
$subtotal = "$ ".number_format($subtotal, 2, ",", ".");
$iva = 1234567890;
$iva = "$ ".number_format($iva, 2, ",", ".");
$total = 1234567890;
$total = "$ ".number_format($total, 2, ",", ".");
$pdf->addTotalInvoice2($neto,$descuento,$subtotal,$iva,$total);

$file_name = str_replace(' ','_',$nombre_empresa_ajustado)."_Factura_No._".$num_factura.".pdf";
$file_destination = $path_pdf.$file_name;
$pdf->Output($file_destination,'F');
$pdf->Output($file_name,'I');
?>