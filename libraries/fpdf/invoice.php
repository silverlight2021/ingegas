<?php
require('fpdf.php');

// Camilo Osorio 2015
// Version 1.00

//////////////////////////////////////
//  Funciones Públicas              //
//////////////////////////////////////
//  sizeOfText( $texte, $largeur )
//  addDatosEmpresa( $nom, $adresse ) //Company Data
//  fact_dev( $libelle, $num ) // Label and number of invoice/estimate
//  addDate( $date ) //Issue Date
//  addDate2( $date ) //Maturity Date
//  addPageNumber( $page ) //Page Number
//  addFactData( $data ) // Invoice Data
//  addCLientName( $name ) // Client Name
//  addCLientAdress($address) // CLient Address
//  addCLientDoc( $document ) // Client Document
//  addClientPhone( $phone ) // Client Phone
//  addPayMode($pay) // Payment Mode
//  addReference($observ) //Observation 1 (Top)
//  addObserv2($observ) //Observation 2 (Bottom)
//  addObserv3( $observ ) //Observation 3 (Lower Panel)
//  addTotalInvoice() //Total Invoice Texts
//  addTotalInvoice2($neto,$descuento,$subtotal,$iva,$total) //Total Invoice Numbers
//  addCols( $tab ) //Add Columns
//  addLineFormat( $tab ) //Line Format
//  lineVert( $tab ) //Draw Vertical Lines
//  addLine( $ligne, $tab ) //Add Lines
//  addSign( $sign ) // Add Sign From
//  addSign2() // Add Sign Accept
//  tipo_fact( $tipo ) // add a watermark (Original Invoice, Invoice Copy...) call this method first

class PDF_Invoice extends FPDF
{
	// private variables
	var $colonnes;
	var $format;
	var $angle=0;

	// private functions
	function RoundedRect($x, $y, $w, $h, $r, $style = '') //Rectángulo con bordes redondeados 
	{
		$k = $this->k;
		$hp = $this->h;
		if($style=='F')
			$op='f';
		elseif($style=='FD' || $style=='DF')
			$op='B';
		else
			$op='S';
		$MyArc = 4/3 * (sqrt(2) - 1);
		$this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));
		$xc = $x+$w-$r ;
		$yc = $y+$r;
		$this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));

		$this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);
		$xc = $x+$w-$r ;
		$yc = $y+$h-$r;
		$this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
		$this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);
		$xc = $x+$r ;
		$yc = $y+$h-$r;
		$this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
		$this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);
		$xc = $x+$r ;
		$yc = $y+$r;
		$this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
		$this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
		$this->_out($op);
	}

	function _Arc($x1, $y1, $x2, $y2, $x3, $y3) //Curvatura del rectángulo
	{
		$h = $this->h;
		$this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
							$x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
	}

	function Rotate($angle, $x=-1, $y=-1) //Rotar un texto dentro de la pantalla
	{
		if($x==-1)
			$x=$this->x;
		if($y==-1)
			$y=$this->y;
		if($this->angle!=0)
			$this->_out('Q');
		$this->angle=$angle;
		if($angle!=0)
		{
			$angle*=M_PI/180;
			$c=cos($angle);
			$s=sin($angle);
			$cx=$x*$this->k;
			$cy=($this->h-$y)*$this->k;
			$this->_out(sprintf('q %.5F %.5F %.5F %.5F %.2F %.2F cm 1 0 0 1 %.2F %.2F cm',$c,$s,-$s,$c,$cx,$cy,-$cx,-$cy));
		}
	}

	function _endpage()
	{
		if($this->angle!=0)
		{
			$this->angle=0;
			$this->_out('Q');
		}
		parent::_endpage();
	}

	// public functions
	function sizeOfText( $texte, $largeur )
	{
		$index    = 0;
		$nb_lines = 0;
		$loop     = TRUE;
		while ( $loop )
		{
			$pos = strpos($texte, "\n");
			if (!$pos)
			{
				$loop  = FALSE;
				$ligne = $texte;
			}
			else
			{
				$ligne  = substr( $texte, $index, $pos);
				$texte = substr( $texte, $pos+1 );
			}
			$length = floor( $this->GetStringWidth( $ligne ) );
			$res = 1 + floor( $length / $largeur) ;
			$nb_lines += $res;
		}
		return $nb_lines;
	}

	// Company Data
	function addDatosEmpresa( $nom, $adresse )
	{
		$x1 = 5;
		$y1 = 42;
		//Positionnement en bas
		$this->SetXY( $x1, $y1 );
		$this->SetFont('Arial','B',12);
		$length = $this->GetStringWidth( $nom );
		$this->Cell( $length, 2, $nom);
		$this->SetXY( $x1, $y1 + 4 );
		$this->SetFont('Arial','',10);
		$length = $this->GetStringWidth( $adresse );
		//Coordonnées de la société
		$lignes = $this->sizeOfText( $adresse, $length) ;
		$this->MultiCell($length, 4, $adresse);
	}

	// Label and number of invoice/estimate
	function fact_dev( $libelle, $num )
	{
		$r1  = $this->w - 80;
		$r2  = $r1 + 70;
		$y1  = 6;
		$y2  = $y1 + 2;
		$mid = ($r1 + $r2 ) / 2;
		
		$texte  = $libelle . " No. " . $num;    
		$szfont = 12;
		$loop   = 0;
		
		while ( $loop == 0 )
		{
		   $this->SetFont( "Arial", "B", $szfont );
		   $sz = $this->GetStringWidth( $texte );
		   if ( ($r1+$sz) > $r2 )
			  $szfont --;
		   else
			  $loop ++;
		}

		$this->SetLineWidth(0.1);
		$this->SetFillColor(192);
		$this->RoundedRect($r1, $y1, ($r2 - $r1), $y2, 2.5, 'DF');
		$this->SetXY( $r1+1, $y1+2);
		$this->Cell($r2-$r1 -1,5, $texte, 0, 0, "C" );
	}

	//Issue Date 
	function addDate( $date )
	{
		$r1  = $this->w - 67;
		$r2  = $r1 + 29;
		$y1  = 17;
		$y2  = $y1 ;
		$mid = $y1 + ($y2 / 2);
		$this->RoundedRect($r1, $y1, ($r2 - $r1), $y2, 3.5, 'D');
		$this->Line( $r1, $mid, $r2, $mid);
		$this->SetXY( $r1 + ($r2-$r1)/2 - 5, $y1+3 );
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,5, utf8_decode("EMISIÓN"), 0, 0, "C");
		$this->SetXY( $r1 + ($r2-$r1)/2 - 5, $y1+9 );
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,$date, 0,0, "C");
	}

	//Maturity Date
	function addDate2( $date )
	{
		$r1  = $this->w - 38;
		$r2  = $r1 + 28;
		$y1  = 17;
		$y2  = $y1;
		$mid = $y1 + ($y2 / 2);
		$this->RoundedRect($r1, $y1, ($r2 - $r1), $y2, 3.5, 'D');
		$this->Line( $r1, $mid, $r2, $mid);
		$this->SetXY( $r1 + ($r2-$r1)/2 - 5, $y1+3 );
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,5, utf8_decode("VENCIMIENTO"), 0, 0, "C");
		$this->SetXY( $r1 + ($r2-$r1)/2 - 5, $y1 + 9 );
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,$date, 0,0, "C");
	}

	//Page Number
	function addPageNumber( $page )
	{
		$r1  = $this->w - 80;
		$r2  = $r1 + 13;
		$y1  = 17;
		$y2  = $y1;
		$mid = $y1 + ($y2 / 2);
		$this->RoundedRect($r1, $y1, ($r2 - $r1), $y2, 3.5, 'D');
		$this->Line( $r1, $mid, $r2, $mid);
		$this->SetXY( $r1 + ($r2-$r1)/2 - 5, $y1+3 );
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,5, utf8_decode("PAG."), 0, 0, "C");
		$this->SetXY( $r1 + ($r2-$r1)/2 - 5, $y1 + 9 );
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,$page, 0,0, "C");
	}

	// Invoice Data
	function addFactData( $data )
	{
		$r1     = $this->w - 80;
		$r2     = $r1 + 68;
		$y1     = 40;
		$this->SetXY( $r1, $y1);
		$this->SetFont( "Arial", "", 8);
		$this->MultiCell( 70, 4, $data);
	}

	// Client Name
	function addCLientName( $name )
	{
		$r1  = 10;
		$r2  = $r1 + 100;
		$y1  = 65;
		$y2  = $y1+10;
		$mid = $y1 + (($y2-$y1) / 2);
		$this->RoundedRect($r1, $y1, ($r2 - $r1), ($y2-$y1), 2.5, 'D');
		$this->Line( $r1, $mid, $r2, $mid);
		$this->SetXY( $r1 + ($r2-$r1)/2 -5 , $y1+1 );
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("SEÑOR(ES)"), 0, 0, "C");
		$this->SetXY( $r1 + ($r2-$r1)/2 -5 , $y1 + 5 );
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,$name, 0,0, "C");
	}

	// CLient Address
	function addCLientAdress($address)
	{
		$this->SetFont( "Arial", "B", 10);
		$r1  = $this->w - 100;
		$r2  = $r1 + 90;
		$y1  = 65;
		$y2  = $y1+10;
		$mid = $y1 + (($y2-$y1) / 2);
		$this->RoundedRect($r1, $y1, ($r2 - $r1), ($y2-$y1), 2.5, 'D');
		$this->Line( $r1, $mid, $r2, $mid);
		$this->SetXY( $r1 + 16 , $y1+1 );
		$this->Cell(58, 4, utf8_decode("DIRECCIÓN"), '', '', "C");
		$this->SetFont( "Arial", "", 10);
		$this->SetXY( $r1 + 16 , $y1+5 );
		$this->Cell(58, 5, $address, '', '', "C");
	}

	// Client Document
	function addCLientDoc( $document )
	{
		$r1  = 10;
		$r2  = $r1 + 50;
		$y1  = 75;
		$y2  = $y1+10;
		$mid = $y1 + (($y2-$y1) / 2);
		$this->RoundedRect($r1, $y1, ($r2 - $r1), ($y2-$y1), 2.5, 'D');
		$this->Line( $r1, $mid, $r2, $mid);
		$this->SetXY( $r1 + ($r2-$r1)/2 -5 , $y1+1 );
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("DOCUMENTO"), 0, 0, "C");
		$this->SetXY( $r1 + ($r2-$r1)/2 -5 , $y1 + 5 );
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,$document, 0,0, "C");
	}

	// Client Phone
	function addClientPhone( $phone )
	{
		$r1  = 60;
		$r2  = $r1 + 50;
		$y1  = 75;
		$y2  = $y1+10;
		$mid = $y1 + (($y2-$y1) / 2);
		$this->RoundedRect($r1, $y1, ($r2 - $r1), ($y2-$y1), 2.5, 'D');
		$this->Line( $r1, $mid, $r2, $mid);
		$this->SetXY( $r1 + ($r2 - $r1)/2 - 5 , $y1+1 );
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("TELÉFONO(S)"), 0, 0, "C");
		$this->SetXY( $r1 + ($r2-$r1)/2 - 5 , $y1 + 5 );
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,$phone, 0,0, "C");
	}

	// Payment Mode
	function addPayMode($pay)
	{
		$this->SetFont( "Arial", "B", 10);
		$r1  = $this->w - 100;
		$r2  = $r1 + 90;
		$y1  = 75;
		$y2  = $y1+10;
		$mid = $y1 + (($y2-$y1) / 2);
		$this->RoundedRect($r1, $y1, ($r2 - $r1), ($y2-$y1), 2.5, 'D');
		$this->Line( $r1, $mid, $r2, $mid);
		$this->SetXY( $r1 + 16 , $y1+1 );
		$this->Cell(58, 4, utf8_decode("FORMA DE PAGO"), '', '', "C");
		$this->SetFont( "Arial", "", 10);
		$this->SetXY( $r1 + 16 , $y1+5 );
		$this->Cell(58, 5, $pay, '', '', "C");
	}

	//Observation 1 (Top)
	function addReference($observ)
	{
		$this->SetFont( "Arial", "", 10);
		$length = $this->GetStringWidth( "Observaciones : " . $observ );
		$r1  = 10;
		$r2  = $r1 + $length;
		$y1  = 87;
		$y2  = $y1+5;
		$this->SetXY( $r1 , $y1 );
		$this->Cell($length,4, "Observaciones : " . $observ);
	}

	//Observation 2 (Bottom)
	function addObserv2($observ)
	{
		$this->SetFont( "Arial", "", 10);
		$length = $this->GetStringWidth( "Observacion : " . $observ );
		$r1  = 10;
		$r2  = $r1 + $length;
		$y1  = $this->h - 78.5;
		$y2  = $y1+5;
		$this->SetXY( $r1 , $y1 );
		$this->Cell($length,4, "Observacion : " . $observ);
	}

	//Observation 3 (Lower Panel)
	function addObserv3( $observ )
	{
		$r1  = 10;
		$r2  = $r1 + 123;
		$y1  = 225;
		$y2  = $y1+30;
		$mid = $y1 + (($y2-$y1) / 2);
		$this->RoundedRect($r1, $y1, ($r2 - $r1), ($y2-$y1), 2.5, 'D');
		$this->SetXY( $r1 + ($r2-$r1)/2 -60 , $y1+1 );
		$this->SetFont( "Arial", "", 10);
		//$this->Cell(10,4, $mode, 0, 0, "C");
		$this->MultiCell( 120, 4, $observ, 0, "C");
	}

	//Total Invoice Texts
	function addTotalInvoice()
	{
		$this->SetFont( "Arial", "B", 10);
		$r1  = $this->w - 77;
		$r2  = $r1 + 25;
		$y1  = 225;
		$y2  = $y1+30;
		$this->RoundedRect($r1, $y1, ($r2 - $r1), ($y2-$y1), 2.5, 'D');
		$this->Line( $r1, 231, $r2, 231);
		$this->SetXY( $r1 + 1 , $y1+1 );
		$this->Cell(20, 4, utf8_decode("NETO"), '', '', "L");
		$this->Line( $r1, 237, $r2, 237);
		$this->SetXY( $r1 + 1 , $y1+7 );
		$this->Cell(20, 4, utf8_decode("DESCUENTO"), '', '', "L");
		$this->Line( $r1, 243, $r2, 243);
		$this->SetXY( $r1 + 1 , $y1+13 );
		$this->Cell(20, 4, utf8_decode("SUBTOTAL"), '', '', "L");
		$this->Line( $r1, 249, $r2, 249);
		$this->SetXY( $r1 + 1 , $y1+19 );
		$this->Cell(20, 4, utf8_decode("IVA"), '', '', "L");
		$this->SetXY( $r1 + 1 , $y1+25 );
		$this->Cell(20, 4, utf8_decode("TOTAL"), '', '', "L");
	}

	//Total Invoice Numbers
	function addTotalInvoice2($neto,$descuento,$subtotal,$iva,$total)
	{
		$this->SetFont( "Arial", "", 10);
		$r1  = $this->w - 52;
		$r2  = $r1 + 42;
		$y1  = 225;
		$y2  = $y1+30;
		$this->RoundedRect($r1, $y1, ($r2 - $r1), ($y2-$y1), 2.5, 'D');
		$this->Line( $r1, 231, $r2, 231);
		$this->SetXY( $r1 + 1 , $y1+1 );
		$this->Cell(40, 4, $neto, '', '', "R");
		$this->Line( $r1, 237, $r2, 237);
		$this->SetXY( $r1 + 1 , $y1+7 );
		$this->Cell(40, 4, $descuento, '', '', "R");
		$this->Line( $r1, 243, $r2, 243);
		$this->SetXY( $r1 + 1 , $y1+13 );
		$this->Cell(40, 4, $subtotal, '', '', "R");
		$this->Line( $r1, 249, $r2, 249);
		$this->SetXY( $r1 + 1 , $y1+19 );
		$this->Cell(40, 4, $iva, '', '', "R");
		$this->SetXY( $r1 + 1 , $y1+25 );
		$this->Cell(40, 4, $total, '', '', "R");
	}

	//Add Columns
	function addCols( $tab )
	{
		global $colonnes;
		
		$r1  = 10;
		$r2  = $this->w - ($r1 * 2) ;
		$y1  = 93;
		$y2  = $this->h - 80 - $y1;
		$this->SetXY( $r1, $y1 );
		$this->Rect( $r1, $y1, $r2, $y2, "D");
		$this->Line( $r1, $y1+6, $r1+$r2, $y1+6);
		$colX = $r1;
		$colonnes = $tab;
		while ( list( $lib, $pos ) = each ($tab) )
		{
			$this->SetXY( $colX, $y1+2 );
			$this->Cell( $pos, 1, $lib, 0, 0, "C");
			$colX += $pos;
			$this->Line( $colX, $y1, $colX, $y1+$y2);
		}
	}

	//Line Format
	function addLineFormat( $tab )
	{
		global $format, $colonnes;
		
		while ( list( $lib, $pos ) = each ($colonnes) )
		{
			if ( isset( $tab["$lib"] ) )
				$format[ $lib ] = $tab["$lib"];
		}
	}

	//Draw Vertical Lines
	function lineVert( $tab )
	{
		global $colonnes;

		reset( $colonnes );
		$maxSize=0;
		while ( list( $lib, $pos ) = each ($colonnes) )
		{
			$texte = $tab[ $lib ];
			$longCell  = $pos -2;
			$size = $this->sizeOfText( $texte, $longCell );
			if ($size > $maxSize)
				$maxSize = $size;
		}
		return $maxSize;
	}

	//Add Lines
	function addLine( $ligne, $tab )
	{
		global $colonnes, $format;

		$ordonnee     = 10;
		$maxSize      = $ligne;

		reset( $colonnes );
		while ( list( $lib, $pos ) = each ($colonnes) )
		{
			$longCell  = $pos -2;
			$texte     = $tab[ $lib ];
			$length    = $this->GetStringWidth( $texte );
			$tailleTexte = $this->sizeOfText( $texte, $length );
			$formText  = $format[ $lib ];
			$this->SetXY( $ordonnee, $ligne-1);
			$this->MultiCell( $longCell, 4 , $texte, 0, $formText);
			if ( $maxSize < ($this->GetY()  ) )
				$maxSize = $this->GetY() ;
			$ordonnee += $pos;
		}
		return ( $maxSize - $ligne );
	}

	// Add Sign From
	function addSign( $sign )
	{
		$this->SetFont( "Arial", "", 10);
		$length = $this->GetStringWidth( "ATENTAMENTE : ".$sign);
		$r1  = 10;
		$r2  = $r1 + $length;
		$y1  = $this->h - 40;
		$y2  = $y1+5;
		$this->SetXY( $r1 , $y1 );
		$this->SetFont( "Arial", "", 8);
		$this->Cell($length,3, "ATENTAMENTE : ".$sign);
		$this->Line( $r1, $y1+17, $r2+20, $y1+17);
	}

	// Add Sign Accept
	function addSign2()
	{
		$this->SetFont( "Arial", "", 10);
		$length = $this->GetStringWidth( "ACEPTO (Nombre Completo y Documento)" );
		$r1  = 110;
		$r2  = $r1 + $length;
		$y1  = $this->h - 40;
		$y2  = $y1+5;
		$this->SetXY( $r1 , $y1 );
		$this->SetFont( "Arial", "", 8);
		$this->Cell($length,3, "ACEPTO (Nombre Completo y Documento)" );
		$this->Line( $r1, $y1+17, $r2+20, $y1+17);
	}

	// add a watermark (Original Invoice, Invoice Copy...)
	// call this method first
	function tipo_fact( $tipo )
	{
		$this->SetFont('Arial','B',50);
		$this->SetTextColor(203,203,203);
		$this->Rotate(45,95,200);
		$this->Text(55,190,$tipo);
		$this->Rotate(0);
		$this->SetTextColor(0,0,0);
	}
}
?>