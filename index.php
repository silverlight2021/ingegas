<?php
require ("libraries/conexion.php");
if (isset($_POST['enviar'])) 
{
   	$contrasena = isset($_POST['password']) ? $_POST['password'] : NULL;
   	$user = isset($_POST['user']) ? $_POST['user'] : NULL;
   	$consulta = "SELECT h.profile_idprofile, h.state, h.User_idUser,h.UserLogin, u.Name, u.LastName
		         FROM user_has_profile h
		         INNER JOIN user u
		         ON h.User_idUser = u.idUser 
		         WHERE BINARY h.PassLogin = '".md5($contrasena)."' AND BINARY h.UserLogin = '".$user."'";
	$resultado = mysqli_query($con,$consulta) ;	
	$_SESSION = $resultado;
	if (mysqli_num_rows($resultado) > 0)
	{
		ini_set("session.cookie_lifetime", 900);
		ini_set("session.gc_maxlifetime", 900);
		//ini_set("session.save_path","/tmp");
		session_cache_expire(900);
		session_start();
			
		$linea = mysqli_fetch_array($resultado);
	  	$idUser = $linea["User_idUser"];
	  	$idprofile = $linea["profile_idprofile"];
	  	$state = $linea["state"];
	  	$UserLogin = $linea["UserLogin"];
	  	$Name = $linea['Name']." ".$linea['LastName'];
				
	  	if ($state == 2)
	  	{
			mysqli_close($con);
		    ?>
		    <script language="javascript" type="text/javascript">
		    	alert("Usted se encuentra INACTIVO. Consulte al administrador del sistema.");
		    	document.location="index.php"</script>
		    <?php
	  	}
	  	else 
	  	{	  		
		    $_SESSION['aut'] = "SI";
		    $_SESSION['sp'] = $idprofile;
		    $_SESSION['su'] = $idUser;
		    $_SESSION['sl'] = $UserLogin; 
		    $_SESSION['name'] = $Name;   
		    $_SESSION['id'] = session_id();
		    $permission = array();
		    /////////////////ASIGNO PERMISOS///////////////////
		    $consulta1 = "SELECT permission_idpermission AS permission  
		        		  FROM profile_has_permission 
		         		  WHERE profile_idprofile = ".$idprofile." 
		        		  ORDER BY permission_idpermission ASC";
		    $resultado1 = mysqli_query($con,$consulta1) ;
		    
		    while ($linea1 = mysqli_fetch_array($resultado1))
		    {
		    	array_push($permission, $linea1["permission"]);
		    	$_SESSION['acc'] = $permission;
			    $_SESSION['logged']= 'yes';
			    ?> 
		    	<script language="javascript" type="text/javascript">document.location="menu_ppal.php"</script>
		   		<?php	
		    }
		     
	  	} 
	}
	else
	{  		
  		?> 
  		<script language="javascript" type="text/javascript">alert("Datos de usuario incorrectos."),document.location="index.php"</script> 
  		<?php
  		$Error=1;
	}
}
//initilize the page
require_once("inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("inc/config.ui.php");

/*---------------- PHP Custom Scripts ---------*/

/*YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */

$page_title = "Login";

/* ---------------- END PHP Custom Scripts ------------- */

//include header
//you can add your custom css in $page_css array.
//Note: all css files are inside css/ folder
$page_css[] = "your_style.css";
$no_main_header = true;
$page_html_prop = array("id"=>"extr-page", "class"=>"animated fadeInDown");
include("inc/header.php");

?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
<header id="header">
	<!--<span id="logo"></span>-->
	<div id="logo-group">
		<span id="logo"> <img src="img/ingegas.png" alt="INGEGAS"> </span>
		<!-- END AJAX-DROPDOWN -->
	</div>
</header>

<div id="main" role="main">
	<!-- MAIN CONTENT -->
	<div id="content" class="container">
		
		<div class="row">
			<article class="col-md-8 col-md-offset-4">
				<div class="col-md-6">
					<div class="well no-padding">			
						<form action="index.php" id="login-form" method="post" class="smart-form client-form">
							<header style="background-color: rgba(0,0,0,.5);color: white;font-weight: bold">
								Iniciar Sesión
							</header>
							<fieldset>							
								<section>
									<label class="label">Usuario:</label>
									<label class="input"> <i class="icon-append fa fa-user"></i>
										<input type="text" name="user" autofocus="yes" autocomplete="off">
										<b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Por favor ingrese el usuario</b></label>
								</section>
								<section>
									<label class="label">Contraseña:</label>
									<label class="input"> <i class="icon-append fa fa-lock"></i>
										<input type="password" name="password" autocomplete="off">
										<b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Por favor ingrese su contraseña</b>
									</label>								
								</section>							
							</fieldset>
							<footer style="background-color: rgba(0,0,0,.5);color: white;font-weight: bold">
								<input type="submit" id="enviar" name="enviar" class="btn btn-primary" value="Entrar">
								<?php 
					            if (@$Error==1) 
					            {
						            ?>
									<label class="alert alert-danger fade in">Datos de usuario Incorrectos.</label>
									<?php
						        }
						       	?>						
							</footer>
						</form>			
					</div>
				</div>
			</article>
		</div>
		
		
		
	</div>
</div>

<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->

<?php 
	//include required scripts
	include("inc/scripts.php"); 
?>

<!-- PAGE RELATED PLUGIN(S) 
<script src="..."></script>-->

<script type="text/javascript">
	runAllForms();

	$(function() {
		// Validation
		$("#login-form").validate({
			// Rules for form validation
			rules : {
				user : {
					required : true,
					
				},
				password : {
					required : true,
				}
			},

			// Messages for form validation
			messages : {
				user : {
					required : 'Por favor ingrese un nombre usuario',
					user : 'Ingrese un usuario válido'
				},
				password : {
					required : 'Por favor ingrese una contraseña'
				}
			},

			// Do not change code below
			errorPlacement : function(error, element) {
				error.insertAfter(element.parent());
			}
		});
	});
</script>

<?php 
	//include footer
	include("inc/google-analytics.php"); 
?>