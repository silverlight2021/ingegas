<?php
date_default_timezone_set("America/Bogota");
require ("libraries/conexion.php");
session_start();

if(@$_SESSION['logged']== 'yes')
{ 
  	$id_num_cilindro = isset($_REQUEST['id_num_cilindro']) ? $_REQUEST['id_num_cilindro'] : NULL;
    $User_idUser =$_SESSION['su'];
    $acc = $_SESSION['acc'];

if(isset($_POST['g_cilindro_co2']))
{
	$num_cilindro_co22 = $_POST['num_cilindro_co2'];
	$fecha_llegada22=$_POST['fecha_llegada'];
	$id_estado_co22=$_POST['id_estado_co21']; 
  $pureza1=$_POST['pureza']; 
  $humedad1=$_POST['humedad']; 
  $capacidad1=$_POST['capacidad']; 
  $alerta1=$_POST['alerta']; 
  $fecha_inicio1=$_POST['fecha_inicio']; 

	if(strlen($id_num_cilindro) > 0)
	{
    $consulta= "UPDATE lote_co2 SET 
                num_cilindro_co2 = '$num_cilindro_co22' ,
                fecha_llegada = '$fecha_llegada22' ,
                id_estado_co2 = '$id_estado_co22',
                pureza = '$pureza1',
                humedad = '$humedad1',
                capacidad = '$capacidad1',
                alerta = '$alerta1',
                uso_actual = '$capacidad1',
                fecha_inicio = '$fecha_inicio1'                                 
                WHERE id_num_cilindro = $id_num_cilindro ";
    $resultado = mysqli_query($con,$consulta) ;

    if ($resultado == FALSE)
    {
      echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
    }
    else
    {
      header('Location: busqueda_co2.php');
    }
  }
  else
  {
    $consulta = "INSERT INTO lote_co2
          		(num_cilindro_co2,fecha_llegada, id_estado_co2,pureza,humedad,capacidad,alerta,uso_actual,fecha_inicio) 
          		VALUES ('".$num_cilindro_co22."','".$fecha_llegada22."', '".$id_estado_co22."', '".$pureza1."', '".$humedad1."', '".$capacidad1."', '".$alerta1."', '".$capacidad1."', '".$fecha_inicio1."' )";
    $resultado = mysqli_query($con,$consulta) ;
    if ($resultado == FALSE)
    {
      echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
    }
    else
    {
      $id_cilindro = mysqli_insert_id($con);
      header('Location: busqueda_co2.php');
    }
  }    
}
if(strlen($id_num_cilindro) > 0)
{ 
	$consulta  = "SELECT * FROM lote_co2 WHERE id_num_cilindro= $id_num_cilindro";
	$resultado = mysqli_query($con,$consulta) ;
	$linea = mysqli_fetch_array($resultado);

	$num_cilindro_co2 = isset($linea["num_cilindro_co2"]) ? $linea["num_cilindro_co2"] : NULL;  
	$fecha_llegada = isset($linea["fecha_llegada"]) ? $linea["fecha_llegada"] : NULL;
	$id_estado_co21 = isset($linea["id_estado_co2"]) ? $linea["id_estado_co2"] : NULL;
  $pureza = isset($linea["pureza"]) ? $linea["pureza"] : NULL;
  $humedad = isset($linea["humedad"]) ? $linea["humedad"] : NULL;
  $capacidad = isset($linea["capacidad"]) ? $linea["capacidad"] : NULL;
  $alerta = isset($linea["alerta"]) ? $linea["alerta"] : NULL;
  $uso_actual = isset($linea["uso_actual"]) ? $linea["uso_actual"] : NULL;
  $fecha_inicio = isset($linea["fecha_inicio"]) ? $linea["fecha_inicio"] : NULL;

  mysqli_free_result($resultado);
}
require_once("inc/init.php");
require_once("inc/config.ui.php");
$page_title = "Lote Co2";
$page_css[] = "your_style.css";
include("inc/header.php");
$page_nav[""][""][""][""] = true;
include("inc/nav.php");
?>
<div id="main" role="main">	
	<div id="content">
		<section id="widget-grid" class="">
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-6">
					<div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h2>Cilindro </h2>
						</header>
						<div>
							<div class="jarviswidget-editbox"></div>							
							<div class="widget-body no-padding">
								<form id="checkout-form" class="smart-form" novalidate="novalidate" action="lote_co2.php" method="POST">
								<input type="hidden" name="id_num_cilindro" id="id_num_cilindro" value="<?php echo $id_num_cilindro; ?>">
									<fieldset>
										<div class="row">											
											<section class="col col-6">
												<label class="label">Num Cilindro :</label>
												<label class="input"> 
													<input type="text" name="num_cilindro_co2" placeholder="Num Cilindro Co2"  value="<?php echo isset($num_cilindro_co2) ? $num_cilindro_co2 : NULL; ?>">
												</label>
											</section>
											<section class="col col-6">
												<label class="label">Fecha llegada :</label>
												<label class="input"> 
													<input type="date" name="fecha_llegada" placeholder="Fecha llegada"  value="<?php echo isset($fecha_llegada) ? $fecha_llegada : NULL; ?>">
												</label>
											</section>
                    </div>
                    <div class="row"> 
											<section class="col col-6">	
												<?php
												$consulta6 ="SELECT * FROM estado_lote_co2 ORDER BY estado ASC";
												$resultado6 = mysqli_query($con,$consulta6) ;
												echo "<section>";
												echo "<label class='label'>Estado</label>";
												echo"<label class='select'>";
												echo "<select name='id_estado_co21' ¿>";
												echo "<option value='0'>Seleccione...</option>";
												while($linea6 = mysqli_fetch_array($resultado6))
												{
													$id_estado_co2 = $linea6['id_estado_co2'];
													$estado = $linea6['estado'];
													if ($id_estado_co2==$id_estado_co21)
													{
															echo "<option value='$id_estado_co2' selected >$estado</option>"; 
													}
													else 
													{
															echo "<option value='$id_estado_co2'>$estado</option>"; 
													} 
												}//fin while 
												echo "</select>";
												echo "<i></i>";
												echo "</label>";
												echo "</section>";
															
												?>
											</section>	
                      <section class="col col-6">
                        <label class="label">Fecha Inicio :</label>
                        <label class="input"> 
                          <input type="date" name="fecha_inicio" placeholder=""  value="<?php echo isset($fecha_inicio) ? $fecha_inicio : NULL; ?>">
                        </label>
                      </section>									
										</div>
                    <div class="row">                     
                      <section class="col col-6">
                        <label class="label">Pureza (%) :</label>
                        <label class="input"> 
                          <input type="text" name="pureza" placeholder="Pureza" onkeypress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"  value="<?php echo isset($pureza) ? $pureza : NULL; ?>">
                        </label>
                      </section>
                      <section class="col col-6">
                        <label class="label">Humedad (ppm):</label>
                        <label class="input"> 
                          <input type="text" name="humedad" placeholder="Humedad"onkeypress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"  value="<?php echo isset($humedad) ? $humedad : NULL; ?>">
                        </label>
                      </section>                                   
                    </div>
                     <div class="row">                     
                      <section class="col col-6">
                        <label class="label">Capacidad Inicial :</label>
                        <label class="input"> 
                          <input type="text" name="capacidad" placeholder="Capacidad Inicial" onkeypress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"  value="<?php echo isset($capacidad) ? $capacidad : NULL; ?>">
                        </label>
                      </section>
                      <section class="col col-6">
                        <label class="label">Alerta :</label>
                        <label class="input"> 
                          <input type="text" name="alerta" placeholder="alerta"onkeypress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"  value="<?php echo isset($alerta) ? $alerta : NULL; ?>">
                        </label>
                      </section>                                   
                    </div>
                    <div class="row">                     
                      <section class="col col-6">
                        <label class="label">Uso Actual :</label>
                        <label class="input"> 
                          <input type="text" name="uso_actual" placeholder="Uso Actual" onkeypress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"  value="<?php echo isset($uso_actual) ? $uso_actual : NULL; ?>">
                        </label>
                      </section>                              
                    </div>
									</fieldset>								
									
									<footer>                    
	                  <input type="submit" value="Save" name="g_cilindro_co2" id="g_cilindro_co2" class="btn btn-primary" />
	                </footer>
         				</form>								


          					</div>  
           				</div>
           			</div>
         		</article>
        	</div>
        </section>
  </div>
  <!-- END MAIN CONTENT -->
</div>

<?php
  include("inc/footer.php");
  include("inc/scripts.php"); 
?>

<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>

<!-- Vector Maps Plugin: Vectormap engine, Vectormap language -->
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>

<!-- Full Calendar -->
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/jquery-form/jquery-form.min.js"></script>


<script type="text/javascript">

  $(document).ready(function() 
  {

    var $checkoutForm = $('#checkout-form').validate(
    {
    // Rules for form validation
      rules :
      {
        name1 : {
          required : true
        },
        idgender1 : {
          required : true
        },
        last_name : {
          required : true
        },
        home_phone : {
          required : true
        },
        primary_mail : {
          required : true
        },
        street_adress1 : {
          required : true
        }
      },

      // Messages for form validation
      messages : {
        name1 : {
          required : 'Please enter your first name'
        },
        idgender1 : {
          required : ''
        },
        last_name : {
          required : 'Please enter your last name'
        },
        home_phone : {
          required : 'Please enter your phone'
        },
        primary_mail : {
          required : 'Please enter your mail'
        },
        street_adress1 : {
          required : 'Please enter your Adress'
        }
      },

      // Do not change code below
      errorPlacement : function(error, element) {
        error.insertAfter(element.parent());
      }
    }); 

    }) 
   

</script>


<?php 
  //include footer
  include("inc/google-analytics.php"); 
?>
<?php

}
else
{
    header("Location:index.php");
}
?>
					                       
										