<?php

require('libraries/fpdf/fpdf.php');


class PDF_Invoice extends FPDF
{
	// private variables
	var $colonnes;
	var $format;
	var $angle=0;

	// private functions
	function RoundedRect($x, $y, $w, $h, $r, $style = '') //Rectángulo con bordes redondeados 
	{
		$k = $this->k;
		$hp = $this->h;
		if($style=='F')
			$op='f';
		elseif($style=='FD' || $style=='DF')
			$op='B';
		else
			$op='S';
		$MyArc = 4/3 * (sqrt(2) - 1);
		$this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));
		$xc = $x+$w-$r ;
		$yc = $y+$r;
		$this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));

		$this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);
		$xc = $x+$w-$r ;
		$yc = $y+$h-$r;
		$this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
		$this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);
		$xc = $x+$r ;
		$yc = $y+$h-$r;
		$this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
		$this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);
		$xc = $x+$r ;
		$yc = $y+$r;
		$this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
		$this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
		$this->_out($op);
	}

	function _Arc($x1, $y1, $x2, $y2, $x3, $y3) //Curvatura del rectángulo
	{
		$h = $this->h;
		$this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
							$x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
	}

	function Rotate($angle, $x=-1, $y=-1) //Rotar un texto dentro de la pantalla
	{
		if($x==-1)
			$x=$this->x;
		if($y==-1)
			$y=$this->y;
		if($this->angle!=0)
			$this->_out('Q');
		$this->angle=$angle;
		if($angle!=0)
		{
			$angle*=M_PI/180;
			$c=cos($angle);
			$s=sin($angle);
			$cx=$x*$this->k;
			$cy=($this->h-$y)*$this->k;
			$this->_out(sprintf('q %.5F %.5F %.5F %.5F %.2F %.2F cm 1 0 0 1 %.2F %.2F cm',$c,$s,-$s,$c,$cx,$cy,-$cx,-$cy));
		}
	}

	function _endpage()
	{
		if($this->angle!=0)
		{
			$this->angle=0;
			$this->_out('Q');
		}
		parent::_endpage();
	}

	// public functions
	function sizeOfText( $texte, $largeur )
	{
		$index    = 0;
		$nb_lines = 0;
		$loop     = TRUE;
		while ( $loop )
		{
			$pos = strpos($texte, "\n");
			if (!$pos)
			{
				$loop  = FALSE;
				$ligne = $texte;
			}
			else
			{
				$ligne  = substr( $texte, $index, $pos);
				$texte = substr( $texte, $pos+1 );
			}
			$length = floor( $this->GetStringWidth($ligne));
			$res = 1 + floor( $length / $largeur) ;
			$nb_lines += $res;
		}
		return $nb_lines;
	}

	// Company Data
	function addDatosEmpresa( $nom, $adresse )
	{
		$x1 = 5;
		$y1 = 42;
		//Positionnement en bas
		$this->SetXY( $x1, $y1 );
		$this->SetFont('Arial','B',12);
		$length = $this->GetStringWidth( $nom );
		$this->Cell( $length, 2, $nom);
		$this->SetXY( $x1, $y1 + 4 );
		$this->SetFont('Arial','',10);
		$length = $this->GetStringWidth( $adresse );
		//Coordonnées de la société
		$lignes = $this->sizeOfText( $adresse, $length) ;
		$this->MultiCell($length, 4, $adresse);
	}

	// Label and number of invoice/estimate


	function fact_dev1( $nombre_empresa_ajustado )//Numero Comprobante
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 75;
		$y1  = 3; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;		
		

		$this->SetXY( $r1 + ($r2-$r1)/2 -10 , $y1 + 5 );
		$this->SetFont( "Arial", "", 10);
		$this->Cell(100,7,$nombre_empresa_ajustado, 0,0, "l");

	}
	function fact_dev2( $nombre_empresa_ajustado2 )//Numero Comprobante
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 75;
		$y1  = 8; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;		
				
		$this->SetXY( $r1 + ($r2-$r1)/2 -10 , $y1 + 5 );
		$this->SetFont( "Arial", "", 10);
		$this->Cell(100,7,$nombre_empresa_ajustado2, 0,0, "l");

	}

	
	function fact_dev3( $titulo )//Numero Comprobante
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 250;
		$y1  = 3; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;		
		
		$this->SetXY( $r1 + ($r2-$r1)/2 -10 , $y1 + 5 );
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,5,utf8_decode($titulo), 0,0, "C");

	}
	function fact_dev4( $titulo )//Numero Comprobante
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 250;
		$y1  = 8; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;		
		
		$this->SetXY( $r1 + ($r2-$r1)/2 -10 , $y1 + 5 );
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,5,utf8_decode($titulo), 0,0, "C");

	}
	function fact_dev5( $titulo )//Numero Comprobante
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 250;
		$y1  = 13; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;		
		
		$this->SetXY( $r1 + ($r2-$r1)/2 -10 , $y1 + 5 );
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,5,utf8_decode($titulo), 0,0, "C");

	}
	function fact_dev6( $nombre_empresa_ajustado )//Numero Comprobante
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 380;
		$y1  = 3; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;		
		

		$this->SetXY( $r1 + ($r2-$r1)/2 -10 , $y1 + 5 );
		$this->SetFont( "Arial", "", 10);
		$this->Cell(100,7,$nombre_empresa_ajustado, 0,0, "l");

	}
	function fact_dev7( $nombre_empresa_ajustado )//Numero Comprobante
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 380;
		$y1  = 8; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;		
		

		$this->SetXY( $r1 + ($r2-$r1)/2 -10 , $y1 + 5 );
		$this->SetFont( "Arial", "", 10);
		$this->Cell(100,7,$nombre_empresa_ajustado, 0,0, "l");

	}
	function fact_dev8( $nombre_empresa_ajustado )//Numero Comprobante
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 380;
		$y1  = 13; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;		
		

		$this->SetXY( $r1 + ($r2-$r1)/2 -10 , $y1 + 5 );
		$this->SetFont( "Arial", "", 10);
		$this->Cell(100,7,$nombre_empresa_ajustado, 0,0, "l");

	}
	function fact_dev9( $fecha )
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 75;
		$y1  = 28; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2 -10  , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("1. FECHA:"), 0, 0, "l");	
		
		$this->SetXY( $r1 + ($r2-$r1)/2+10  , $y1+(4.5));
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($fecha), 0,0, "l");
	}
	function fact_dev10( $fecha )
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 75;
		$y1  = 33; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2 -10 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("2. TIPO DE REGISTRO:"), 0, 0, "l");	
		
		$this->SetXY( $r1 + ($r2-$r1)/2+30  , $y1+(4.5));
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($fecha), 0,0, "l");
	}
	function fact_dev11( $fecha )
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 75;
		$y1  = 38; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2 -10 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("3. ORIGEN:"), 0, 0, "l");	
		
		$this->SetXY( $r1 + ($r2-$r1)/2+10  , $y1+(4.5));
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($fecha), 0,0, "l");
	}
	function fact_dev12( $fecha )
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 75;
		$y1  = 43; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2 -10 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("4. DESTINO:"), 0, 0, "l");	
		
		$this->SetXY( $r1 + ($r2-$r1)/2+12  , $y1+(4.5));
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($fecha), 0,0, "l");
	}
	function fact_dev13( $fecha )
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 75;
		$y1  = 48; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2 -10 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("5. DESCRIPCION DE LA NO CONFROMIDAD O PUNTO DE MEJORA"), 0, 0, "l");	
		
		$this->SetXY( $r1 + ($r2-$r1)/2-10  , $y1+(10));
		$this->SetFont( "Arial", "", 10);
		$this->MultiCell($length, 4,utf8_decode($fecha));
	}
	function fact_dev14( $fecha )
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 75;
		$y1  = 88; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2 -10 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("Solicitante:"), 0, 0, "l");	
		
		$this->SetXY( $r1 + ($r2-$r1)/2+12  , $y1+(4.5));
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($fecha), 0,0, "l");
	}
	function fact_dev15( $fecha )
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 75;
		$y1  = 93; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2-10 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("6. CORRECCION"), 0, 0, "l");	
		
		$this->SetXY( $r1 + ($r2-$r1)/2-10  , $y1+(10));
		$this->SetFont( "Arial", "", 10);
		$this->MultiCell($length, 4,utf8_decode($fecha));
	}
	function fact_dev16( $fecha )
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 75;
		$y1  = 113; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2-10 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("7. ANALISIS DE LAS CAUSAS"), 0, 0, "l");	
		
		$this->SetXY( $r1 + ($r2-$r1)/2-10  , $y1+(10));
		$this->SetFont( "Arial", "", 10);
		$this->MultiCell($length, 4,utf8_decode($fecha));
	}
	function fact_dev17( $fecha )
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 75;
		$y1  = 133; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2-10 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("8. ACCION CORRECTIVA O DE MEJORA"), 0, 0, "l");	
		
		$this->SetXY( $r1 + ($r2-$r1)/2-10  , $y1+(10));
		$this->SetFont( "Arial", "", 10);
		$this->MultiCell($length, 4,utf8_decode($fecha));
	}
	function fact_dev18( $fecha )
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 75;
		$y1  = 153; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2 -10 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("Responsables:"), 0, 0, "l");	
		
		$this->SetXY( $r1 + ($r2-$r1)/2+16  , $y1+(4.5));
		$this->SetFont( "Arial", "", 10);
		$this->Cell(10,5,utf8_decode($fecha), 0,0, "l");
	}
	function fact_dev19(  )
	{
		$r1  = $this->w - 435;
		$r2  = $r1 + 75;
		$y1  = 163; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2-10 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("9. SEGUIMIENTO"), 0, 0, "l");	;	
		
	
	}
	

	function addCols( $tab )
	{
		global $colonnes;
		
		$r1  = 10;
		$r2  = $this->w - ($r1 * 1) ;
		$y1  = 163;
		$y2  = $this->h - 85 - $y1;
		$this->SetXY( $r1, $y1 );
		//$this->Rect( 5, $y1, $r2, 150, "D");
		//$this->Line( $r1, $y1+6, $r1+$r2, $y1+6);
		$colX = $r1;
		$colonnes = $tab;
		while ( list( $lib, $pos ) = each ($tab) )
		{
			$this->SetXY( $colX, $y1+2 );
			$this->Cell( $pos, 30, $lib, 0, 0, "C");
			$colX += $pos;
			//$this->Line( $colX, $y1, $colX, $y1+$y2);
		}
	}
	function addLineFormat( $tab )
	{
		global $format, $colonnes;
		
		while ( list( $lib, $pos ) = each ($colonnes) )
		{
			if ( isset( $tab["$lib"] ) )
				$format[ $lib ] = $tab["$lib"];
		}
	}
	function addLine( $ligne, $tab )
	{
		global $colonnes, $format;

		$ordonnee     = 21;
		$maxSize      = $ligne;

		reset( $colonnes );
		while ( list( $lib, $pos ) = each ($colonnes) )
		{
			$longCell  = $pos -10;
			$texte     = $tab[ $lib ];
			$length    = $this->GetStringWidth( $texte );
			$tailleTexte = $this->sizeOfText( $texte, $length );
			$formText  = $format[ $lib ];
			$this->SetXY( $ordonnee, $ligne-1);
			$this->MultiCell( $longCell, 4 , $texte, 0, $formText);
			if ( $maxSize < ($this->GetY()  ) )
				$maxSize = $this->GetY() ;
			$ordonnee += $pos;
		}
		return ( $maxSize - $ligne );
	}
	function fact_dev20( $fecha )
	{
		$uno =" * Los items 1,2,3,4 y 5 son diligenciados por el funcionario de Ingegas o cliente que detecta la no conformidad o punto de mejora.";
		$dos =" * Los items 6,7, y 8 son diligenciados por el proveedor.";
		$tres =" * El item 9 es diligenciado por el funcionario responsable de relizar el seguimiento de la accion correctiva determinada por el proveedor.";

		$r1  = $this->w - 435;
		$r2  = $r1 + 75;
		$y1  = 240; //top
		$y2  = 12;
		$mid = ($r1 + $r2 ) ;

		$this->SetXY( $r1 + ($r2-$r1)/2-10 , $y1+5);
		$this->SetFont( "Arial", "B", 10);
		$this->Cell(10,4, utf8_decode("NOTAS"), 0, 0, "l");	;	
		
		$this->SetXY( $r1 + ($r2-$r1)/2-10  , $y1+(10));
		$this->SetFont( "Arial", "", 10);
		$this->MultiCell($length, 4,utf8_decode($uno));

		$this->SetXY( $r1 + ($r2-$r1)/2-10  , $y1+(20));
		$this->SetFont( "Arial", "", 10);
		$this->MultiCell($length, 4,utf8_decode($dos));

		$this->SetXY( $r1 + ($r2-$r1)/2-10  , $y1+(25));
		$this->SetFont( "Arial", "", 10);
		$this->MultiCell($length, 4,utf8_decode($tres));

	
	}

	
}
?>