<?php

date_default_timezone_set("America/Bogota");



require ("libraries/conexion.php");

session_start();
$fecha = date("Y-m-d");
$hora = date("G:i:s",time());

$fecha = $fecha." ".$hora;

if(@$_SESSION['logged']== 'yes')

{ 
	$finalizacion_rechazo = 0;
	$id_user=$_SESSION['su'];
    $acc = $_SESSION['acc'];
    $letra = isset($_REQUEST['letra']) ? $_REQUEST['letra'] : NULL;
    //$id_orden = isset($_REQUEST['id_orden']) ? $_REQUEST['id_orden'] : NULL;
    $id_calibracion = isset($_REQUEST['id_calibracion']) ? $_REQUEST['id_calibracion'] : NULL;
    $contador_orden = 0;
    $temperatura_camisa = $_POST['temperatura_camisa'];
    $temperatura_cilindro = $_POST['temperatura_cilindro'];
    $diferencia_temperatura = $_POST['diferencia_temperatura'];
    $finalizacion_rechazo = $_POST["finalizacion_rechazo"];
    $estanquidad = $_POST["estanquidad"];


	$idPresion = $_POST['idPresion'];
	$presion_mano = $_POST['presion_mano'.$idPresion];
	$expancion = $_POST['expancion'.$idPresion];
	$error = $_POST['error_absoluto'.$idPresion];
	$rechazo = $_POST['rechazada'.$idPresion];
	$id_calibracion1 = $_POST['id_calibra'];
	$incertidumbre_total_metodo = $_POST['incertidumbre_total'];
	$observaciones = $_POST['observaciones'];
	$desviacion_metodo = $_POST['desviacionT'];
	$id_user = $_POST['id_user'];
	$total_errores = $_POST['error_total'];
	$aprobacion = $_POST['aprobacion'];
	$incertidumbre_expandida = $_POST['incertidumbre_expandida'];
	$ipEquip1 = $_POST['ipEquip'];



	function getIpEquipo(){
		if (getenv("HTTP_CLIENT_IP") && strcasecmp(getenv("HTTP_CLIENT_IP"),"unknown"))
		    $ip = getenv("HTTP_CLIENT_IP");
		else if (getenv("HTTP_X_FORWARDED_FOR") && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"), "unknown"))
		    $ip = getenv("HTTP_X_FORWARDED_FOR");
		else if (getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"), "unknown"))
			$ip = getenv("REMOTE_ADDR");
		else if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], "unknown"))
			$ip = $_SERVER['REMOTE_ADDR'];
		else
	 	    $ip = "IP desconocida";
		return($ip);
	}
	$ipequip = getIpEquipo();
		
	///Nombre Equipo Login
	$nomequip = gethostbyaddr($ipequip);

	if(($ipequip==$ipEquip1)||($idPresion==1)||($idPresion==7)){

	
	
		if($idPresion==1){

			echo $consulta1 = "INSERT INTO verificacion_metodo_pev (temperatura_camisa, temperatura_cilindro, diferencia_temperatura, estanquidad, presion_manometro1, expansion1, error1, fecha,idusuario, ipEquip) VALUES ('$temperatura_camisa', '$temperatura_cilindro', '$diferencia_temperatura', '$estanquidad', '$presion_mano', '$expancion', '$error', '$fecha', '$id_user', '$ipequip')";
			$resultado1 = mysqli_query($con,$consulta1);
			if($resultado1){
				$id_calibracion = mysqli_insert_id($con);
				?>
				<script type="text/javascript">
					window.alert("La lectura fue guardada");
					window.location = ("calibracion_pru.php?id_calibracion="+<?php echo $id_calibracion; ?>);

				</script>
				<?php
			}else{
				?>
				<script type="text/javascript">
					window.alert(""+<?php echo mysqli_errno($con) . ": " . mysqli_error($con) ?>);

				</script>
				<?php
			}
		}else if ($idPresion == 7) {
			 $consulta3 = "UPDATE verificacion_metodo_pev 
						SET desviacion_total = '$total_errores', 
						estado_calibracion = 1,
						incertidumbre_expandida = '$incertidumbre_expandida'
						WHERE id_verificacion = '$id_calibracion1'";
			 $resultado3 = mysqli_query($con,$consulta3);
			if($resultado3){

				if($total_errores<1){

				?>
					<script type="text/javascript">
						window.alert("La verificacion de la exactitud fue aprobada fue guardada");
						window.location = ("prueba_pev.php");

					</script>
				<?php
				}else{
				?>
					<script type="text/javascript">
						window.alert("La verificacion de la exactitud NO fue aprobada, el total de los errores es: "+<?php echo $total_errores; ?>);
						window.location = ("busqueda_calibracion.php");

					</script>
				<?php
				}
			}else{
				?>
				<script type="text/javascript">
					window.alert(""+<?php echo mysqli_errno($con) . ": " . mysqli_error($con) ?>);
				</script>
				<?php
			}
		}else{

			if($finalizacion_rechazo == 1){

				$consulta4 = "UPDATE verificacion_metodo_pev SET estado_calibracion = '2' WHERE id_verificacion = '$id_calibracion1'";

				$resultado4 = mysqli_query($con,$consulta4);

				if($resultado4){
					?>
					<script type="text/javascript">
						window.alert("La verificacion del metodo fue finalizada");
						window.location = ("calibracion_pru.php?id_calibracion="+<?php echo $id_calibracion1; ?>);
					</script>
					<?php
				}else{
					?>
					<script type="text/javascript">
						window.alert("Error");

					</script>
					<?php
				}

			}else if($aprobacion == 1){

				$consulta5 = "UPDATE verificacion_metodo_pev SET estado_calibracion = '3' WHERE id_verificacion = '$id_calibracion1'";
				$resultado5 = mysqli_query($con,$consulta5);

				if($resultado5){
					?>
					<script type="text/javascript">
						window.alert("La verificacion fue aprobada::");
						window.location = ("calibracion_pru.php?id_calibracion="+<?php echo $id_calibracion1; ?>);
					</script>
					<?php
				}

			}else{
				$consulta2 = "UPDATE verificacion_metodo_pev 
						SET presion_manometro".$idPresion." = '$presion_mano', 
						expansion".$idPresion." = '$expancion', 
						error".$idPresion." = '$error',
						rechazo = '$rechazo'
						WHERE id_verificacion = $id_calibracion1";
				

						
				$resultado2 = mysqli_query($con,$consulta2);

				if($resultado2){
					$id_calibracion = mysqli_insert_id($con);
					?>
					<script type="text/javascript">
						window.alert("La lectura fue guardada");
						window.location = ("calibracion_pru.php?id_calibracion="+<?php echo $id_calibracion1; ?>);

					</script>
					<?php
				}else{
					?>
					<script type="text/javascript">
						window.alert("Error");
					</script>
					<?php
				}
			}
		}

	}else{
		?>
		<script type="text/javascript">
			window.alert("La verificacion no puede ser ejecutada en este Equipo");
			window.location = ("calibracion_pru.php?id_calibracion="+<?php echo $id_calibracion1; ?>);
		</script>
		<?php
	}

	
}
?>

