<?php
session_start();
date_default_timezone_set("America/Bogota");
$Fecha = date("Y-m-d");
require ("libraries/conexion.php");

$motivo_condena = "NINGUNO";
$estado_prueba = "APROBADO";
$prueba_realizada = 0;
if($_SESSION['logged']== 'yes')
{ 
	$id_has_movimiento_cilindro_pev = isset($_REQUEST['id_has_movimiento_cilindro_pev']) ? $_REQUEST['id_has_movimiento_cilindro_pev'] : NULL;
	$idUser =$_SESSION['su'];
	$acc = $_SESSION['acc'];

	require_once("inc/init.php");
	require_once("inc/config.ui.php");
	$page_title = "Inspección Visual Acero";
	$page_css[] = "your_style.css";
	include("inc/header.php");
	$page_nav[""][""][""][""] = true;
	include("inc/nav.php");

	$consulta0 = "SELECT * FROM has_movimiento_cilindro_pev WHERE id_has_movimiento_cilindro_pev = '$id_has_movimiento_cilindro_pev'";
	$resultado0 = mysqli_query($con,$consulta0);

	$linea0 = mysqli_fetch_assoc($resultado0);



	if (isset($_POST['g_insp_visual_acero'])) //guardar información de inspección visual de acero
	{
		$id_has_movimiento_cilindro_pev = $_POST['id_has_movimiento_cilindro_pev'];
		$estado_prueba = $_POST['estado_prueba'];
		$corrosion_general = $_POST['corrosion_general_h'];
		$picaduras_aisladas = $_POST['picaduras_aisladas_h'];
		$corrosion_lineal = $_POST['corrosion_lineal_h'];
		$cort_perfo_ranura = $_POST['cort_perfo_ranura_h'];
		$abolladuras = $_POST['abolladuras_h'];
		$fugas_poros = $_POST['fugas_poros_h'];
		$abombamiento = $_POST['abombamiento_h'];
		$fuego_termico = $_POST['fuego_termico_h'];
		$estampacion = 2;
		$marcas = 2;
		$grieta_cuello = $_POST['grieta_cuello_h'];
		$pliegues_cuello = $_POST['pliegue_cuello_h'];
		$valle_cuello = $_POST['valle_cuello_h'];
		$motivo_condena = $_POST['motivo_condena'];
		$observaciones = $_POST['observaciones'];

		$consulta1 = "INSERT INTO insp_vi_alu_pev(id_has_movimiento_cilindro_pev, estado_prueba, corrosion_general, picaduras_aisladas, corrosion_lineal, cort_perfo_ranura, abolladuras, fugas_poros, abombamiento, fuego_termico, estampacion, marcas, grieta_cuello, pliegue_cuello, valle_cuello, motivo_condena) VALUES ($id_has_movimiento_cilindro_pev, '$estado_prueba', $corrosion_general, $picaduras_aisladas, $corrosion_lineal, $cort_perfo_ranura, $abolladuras, $fugas_poros, $abombamiento, $fuego_termico, $estampacion, $marcas, $grieta_cuello, $pliegues_cuello, $valle_cuello, '$motivo_condena')";

		if(mysqli_query($con,$consulta1))
			{
				?>
					<script type="text/javascript">
						var id_orden_pev = '<?php echo $id_orden_pev; ?>';
						alert("Inspección guardada correctamente.")
						window.location = 'insp_vi_alu_pru.php?id_has_movimiento_cilindro_pev='+<?php echo $id_has_movimiento_cilindro_pev; ?>;
					</script>
				<?php
			}
			else
			{
				echo "Error: " . $consulta1 . "<br>" . mysqli_error($con);
			}
	}

	 //Consulta el responsable de la inspección
	$consulta = "SELECT Name, LastName FROM user WHERE idUser = '".$idUser."'";
	$resultado = mysqli_query($con,$consulta);
	if(mysqli_num_rows($resultado) > 0 )
	{
	  	$linea = mysqli_fetch_assoc($resultado);
		$Name = $linea['Name'];
		$LastName = $linea['LastName'];
		$responsable = $Name." ".$LastName;
	}
	 //Si existe el cilindro se consultan datos
	if(strlen($id_has_movimiento_cilindro_pev) > 0)
	{
		$consulta3 = "SELECT num_cili, id_tipo_gas_pev, id_transporte_pev
						FROM has_movimiento_cilindro_pev 
						WHERE id_has_movimiento_cilindro_pev = '".$id_has_movimiento_cilindro_pev."'";
		$resultado3 = mysqli_query($con,$consulta3);
		if(mysqli_num_rows($resultado3) > 0)
		{
			$linea3 = mysqli_fetch_assoc($resultado3);
			$num_cili = $linea3['num_cili'];
			$id_tipo_gas_pev = $linea3['id_tipo_gas_pev'];
			$id_transporte_pev = $linea3['id_transporte_pev'];

			$consulta10 = "SELECT esp_fab_pev, esp_actu_pev FROM cilindro_pev WHERE num_cili_pev = '".$num_cili."'";
			$resultado10 = mysqli_query($con,$consulta10);
			if(mysqli_num_rows($resultado10) > 0)
			{
				$linea10 = mysqli_fetch_assoc($resultado10);
				$esp_fab_pev = isset($linea10["esp_fab_pev"]) ? $linea10["esp_fab_pev"] : NULL;
				$esp_actu_pev = isset($linea10['esp_actu_pev']) ? $linea10["esp_fab_pev"] : NULL;

				if($esp_fab_pev == NULL){
					$esp_fab_pev = "DESCONOCIDO";
				}
				if($esp_actu_pev == NULL){
					$esp_actu_pev = "DESCONOCIDO";
				}
			}
			$consulta4 = "SELECT id_cliente,fecha FROM transporte_pev WHERE id_transporte_pev = '".$id_transporte_pev."'";
			$resultado4 = mysqli_query($con,$consulta4);
			if(mysqli_num_rows($resultado4) > 0)
			{
				$linea4 = mysqli_fetch_assoc($resultado4);
				$fecha = $linea4['fecha'];
				$id_cliente = $linea4['id_cliente'];

				$consulta8 = "SELECT nombre FROM clientes WHERE id_cliente = '".$id_cliente."'";
				$resultado8 = mysqli_query($con,$consulta8);
				if(mysqli_num_rows($resultado8) > 0)
				{
					$linea8 = mysqli_fetch_assoc($resultado8);
					$nombre_cliente = $linea8['nombre'];
				}
			}
			$consulta5 = "SELECT nombre FROM tipo_gas_pev WHERE id_tipo_gas_pev = '".$id_tipo_gas_pev."'";
			$resultado5 = mysqli_query($con,$consulta5);
			if(mysqli_num_rows($resultado5) > 0)
			{
				$linea5 = mysqli_fetch_assoc($resultado5);
				$nombre = $linea5['nombre'];
			}            
				
			$consulta6 = "SELECT id_especi_pev, especi_pev, num_cili_pev_2 FROM cilindro_pev WHERE num_cili_pev = '".$num_cili."'";
			$resultado6 = mysqli_query($con,$consulta6);
			if(mysqli_num_rows($resultado6) > 0)
			{
				$linea6 = mysqli_fetch_assoc($resultado6);
				$id_especi_pev = $linea6['id_especi_pev'];
				$especi_pev = $linea6['especi_pev'];
				$num_cili_pev_2 = $linea6['num_cili_pev_2'];

				$consulta7 = "SELECT especificacion FROM especificacion WHERE id_especificacion = '".$id_especi_pev."'";
				$resultado7 = mysqli_query($con,$consulta7);
				if(mysqli_num_rows($resultado7))
				{
					$linea7 = mysqli_fetch_assoc($resultado7);
					$especificacion = $linea7['especificacion'];
				}
			}
			$consulta11 = "SELECT * FROM insp_vi_alu_pev WHERE id_has_movimiento_cilindro_pev = '".$id_has_movimiento_cilindro_pev."'";
			$resultado11 = mysqli_query($con,$consulta11);
			if(mysqli_num_rows($resultado11) > 0)
			{
				$prueba_realizada = 1;
				$linea11 = mysqli_fetch_assoc($resultado11);
				$num_cili_pev = isset($linea11["num_cili_pev"]) ? $linea11["num_cili_pev"] : NULL;
				$estado_prueba_i = isset($linea11["estado_prueba"]) ? $linea11["estado_prueba"] : NULL;
				$corrosion_general_i = isset($linea11["corrosion_lineal"]) ? $linea11["corrosion_lineal"] : NULL;
				$picaduras_aisladas_i = isset($linea11["picaduras_aisladas"]) ? $linea11["picaduras_aisladas"] : NULL;
				$corrosion_lineal_i = isset($linea11["corrosion_lineal"]) ? $linea11["corrosion_lineal"] : NULL;
				$cort_perfo_ranura_i = isset($linea11["cort_perfo_ranura"]) ? $linea11["cort_perfo_ranura"] : NULL;
				$abolladuras_i = isset($linea11["abolladuras"]) ? $linea11["abolladuras"]	: NULL;
				$fugas_poros_i = isset($linea11["fugas_poros"]) ? $linea11["fugas_poros"] : NULL;
				$abombamiento_i = isset($linea11["abombamiento"]) ? $linea11["abombamiento"] : NULL;
				$fuego_termico_i = isset($linea11["fuego_termico"]) ? $linea11["fuego_termico"] : NULL;
				$estampacion_i = isset($linea11["estampacion"]) ? $linea11["estampacion"] : NULL;
				$marcas_i = isset($linea11["marcas"]) ? $linea11["marcas"] : NULL;
				$grietas_cuello_i = isset($linea11["grieta_cuello"]) ? $linea11["grieta_cuello"] : NULL;
				$pliegue_cuello_i = isset($linea11["pliegue_cuello"]) ? $linea11["pliegue_cuello"] : NULL;
				$valle_cuello_i = isset($linea11["valle_cuello"]) ? $linea11["valle_cuello"] : NULL;
				$motivo_condena_i = isset($linea11["motivo_condena"]) ? $linea11["motivo_condena"] : NULL;
			}
		}

	}
?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
	 <div id="content">
		  <section id="widget-grid" class="">
				<div class="row">
					 <article class="col-md-6 col-md-offset-3">
						  <div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
								<header>
									 <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
									 <h2>INSPECCIÓN VISUAL CILINDRO DE ACERO</h2>
								</header>
								<div>
									 <div class="jarviswidget-editbox">
									 </div>
									 <div class="widget-body no-padding">                                
										  <form name="mezcla" action="insp_vi_alu_pru.php" method="POST" id="mezcla">
												<input type="hidden" name="id_has_movimiento_cilindro_pev" value="<?php echo $id_has_movimiento_cilindro_pev; ?>">
												<input type="hidden" name="motivo_condena" id="motivo_condena" value="<?php echo $motivo_condena; ?>">
												<div class="well well-sm well-primary">
													 <fieldset>
													 <legend><center>IDENTIFICACIÓN DEL CILINDRO</center></legend>
														  <div class="row">
																<div class="col-md-3">
																	 <div class="form-group">
																		  <label for="category">N° Cilindro<br>Ingreso:</label>
																		  <input type="text" class="form-control" placeholder="Numero Cilindro" name="num_cili_al" readonly required value="<?php echo $num_cili; ?>" />
																	 </div>
																</div>
																<div class="col-md-3">
																	 <div class="form-group">
																		  <label for="category">N° Cilindro<br>Verificado:</label>
																		  <input type="text" class="form-control" placeholder="Numero Cilindro" name="num_cili_al_2" readonly required value="<?php echo isset($num_cili_pev_2) ? $num_cili_pev_2 : NULL; ?>"/>
																	 </div>
																</div>
																<div class="col-md-3">
																	 <div class="form-group">
																		  <label for="category">Fecha de<br> Ingreso:</label>
																		  <input type="text" class="form-control" placeholder="Fecha Ingreso" name="fecha_ingreso_al" readonly value="<?php echo isset($fecha) ? $fecha : NULL; ?>"/>                                                    </div>
																</div>
																<div class="col-md-3">
																	 <div class="form-group">
																		  <label for="category">Fecha de<br> Inspección:</label>
																		  <input type="text" class="form-control" placeholder="Fecha Inspección" name="fecha_inspeccion_al" readonly required value="<?php echo $Fecha ?>"/>
																	 </div>
																</div>
														  </div>
														  <div class="row">
																<div class="col-md-6">
																	 <div class="form-group">
																		  <label for="category">GAS:</label>
																		  <input type="text" class="form-control" placeholder="Tipo de GAS" name="gas_al" readonly required value="<?php echo isset($nombre) ? $nombre : NULL; ?>"/>
																	 </div>
																</div>
																<div class="col-md-2">
																	 <div class="form-group">
																		  <label for="category">Especificación:</label>
																		  <input type="text" class="form-control" placeholder="Especificación" name="especificacion_al" readonly value="<?php echo isset($especificacion) ? $especificacion : NULL; ?>"/>
																	 </div>
																</div>
																<div class="col-md-4">
																	 <div class="form-group">
																		<label for="category">Descripción:</label>
																		  <input type="text" class="form-control" placeholder="Especificación" name="especi_pev2" readonly value="<?php echo isset($especi_pev) ? $especi_pev : NULL; ?>"/>
																	 </div>
																</div>
														  </div>
														  <div class="row">
																<div class="col-md-6">
																	 <div class="form-group">
																		<label for="category">Aprobación / Rechazo:</label>
																		  <input type="text" class="form-control" placeholder="ESTADO" name="estado_prueba" id="estado_prueba" readonly value="<?php echo isset($estado_prueba_i) ? $estado_prueba_i : NULL; ?>"/>
																	 </div>
																</div>
																<?php
																	if($prueba_realizada == 1){

																		if($estado_prueba_i != "APROBADO"){
																?>
																<div class="col-md-6">
																	 <div class="form-group">
																		  <label for="category">Causa de Condena:</label>
																		  <input type="text" class="form-control" placeholder="Cliente" name="nombre_cliente" readonly value="<?php echo isset($motivo_condena_i) ? $motivo_condena_i : NULL; ?>"/>
																	 </div>
																</div>
																<?php
																	}
																}
																?>
														  </div>
														  <div class="row">
																<div class="col-md-6">
																	 <div class="form-group">
																		<label for="category">Responsable:</label>
																		  <input type="text" class="form-control" placeholder="Responsable" name="responsable" readonly value="<?php echo isset($responsable) ? $responsable : NULL; ?>"/>
																	 </div>
																</div>
																<div class="col-md-6">
																	 <div class="form-group">
																		  <label for="category">Cliente:</label>
																		  <input type="text" class="form-control" placeholder="Cliente" name="nombre_cliente" readonly value="<?php echo isset($nombre_cliente) ? $nombre_cliente : NULL; ?>"/>
																	 </div>
																</div>
														  </div>
													 </fieldset>
												</div>  
												<div class="well well-sm well-primary">
													<legend><center>INSPECCIÓN VISUAL </center></legend>
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Corrosión General: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('corrosion')"></label>
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="corr_gene" id="corr_gene1" onclick="checkCorrosionGeneral('1');" value="1" <?php if($corrosion_general_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?> >
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="corr_gene" id="corr_gene2" onclick="checkCorrosionGeneral('0');" value="2" <?php if($corrosion_general_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?> >
																		<i></i>No
																		<input type="hidden" name="corrosion_general_h" id="corrosion_general_h">
																	</label>
																</div>
															</div>  
														</div>
														<div class="row" id="tipo_corrosion_general" style="display: none;">
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="category">Tipo de Defecto:</label>
																	<label for="radio-inline">
																		<input type="radio" name="tipo_corr_gene" id="tipo_corr_gene1" onclick="check_corro_gral_interna('1')" value="1"><i></i>Interno
																	</label>
																	<label for="radio-inline">
																		<input type="radio" name="tipo_corr_gene" id="tipo_corr_gene2" onclick="check_corro_gral_interna('0')" value="2"><i></i>Externo
																	</label>
																</div>
																<div class="form-group" id="datos_corrosion_general" style="display: none">
																	<label for="category"><strong>Información de la Corrosión General</strong></label>
																	<label class="input"> Espesor Cilindro:
																		<input type="text" readonly name="espesor_cilindro" id="espesor_cilindro" value="<?php echo $esp_fab_pev; ?>" size="12">
																	</label>
																	<label class="input">Profundidad Corrosión:
																		<input type="text" name="prof_corr_gene" id="prof_corr_gene" onchange="validar_prof_corr_gene(this.value)" size="10">
																	</label>
																	<label class="input">Extension area de Corrosión:
																		<input type="text" name="area_corr_gene" id="area_corr_gene" size="10">
																	</label>
																	<label class="input">Superficie del Cilindro:
																		<input type="text" name="area_cilindro" id="area_cilindro" size="10" onchange="validar_ext_corr_gene(this.value)">
																	</label>
																</div>
															</div>
														</div>	
													</div>
													<div class="row">
														<div class="col-sm-6">
														  	<div class="form-group">
																<label for="category">Picaduras Aisladas: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('picadura')"></label>                            
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="picadura" id="picadura1" onclick="checkPicaduraAislada('1');" value="1" <?php if($picaduras_aisladas_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?> >
																		<i></i>Si</label>
																	<label class="radio-inline">
																		<input type="radio" name="picadura" id="picadura2" onclick="checkPicaduraAislada('0');" value="2" <?php if($picaduras_aisladas_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No</label>
																		<input type="hidden" name="picaduras_aisladas_h" id="picaduras_aisladas_h">
																</div>
															</div>  
														</div>
														<div class="row" id="tipo_picadura_aislada" style="display: none;">
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="category">Tipo de Defecto:</label>
																	<label for="radio-inline">
																		<input type="radio" name="tipo_picadura_aislada" id="tipo_picadura_aislada1" onclick="check_picadura_aislada_interna('1')" value="1"><i></i>Interno
																	</label>
																	<label for="radio-inline">
																		<input type="radio" name="tipo_picadura_aislada" id="tipo_picadura_aislada2" onclick="check_picadura_aislada_interna('0')" value="2"><i></i>Externo
																	</label>
																</div>
																<div class="form-group" id="datos_picadura_aislada" style="display: none">
																	<label for="category"><strong>Información de la Picadura Aislada</strong></label>
																	<label class="input"> Espesor Cilindro:
																		<input type="text" readonly name="espesor_cilindro1" id="espesor_cilindro1" value="<?php echo isset($esp_fab_pev) ? $esp_fab_pev : NULL; ?>" size="12">
																	</label>
																	<label class="input">Profundidad Corrosión:
																		<input type="text" name="prof_pic_ais" id="prof_pic_ais" onchange="validar_prof_pic_ais(this.value)" size="10">
																	</label>
																	<label class="input">Espesor Minimo de Diseño:
																		<input type="text" name="prof_min_dis" id="prof_min_dis" onchange="validar_prof_esp_min(this.value)" size="9">
																	</label>
																</div>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Corrosión Lineal: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('corrosionl')">
																</label>                            
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="corr_line" id="corr_line1" onclick="checkCorrLine('1');" value="1" <?php if($corrosion_lineal_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="corr_line" id="corr_line2" onclick="checkCorrLine('0');" value="2" <?php if($corrosion_lineal_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="corrosion_lineal_h" id="corrosion_lineal_h">
																	</label>
																</div>
															</div>  
														</div>
														<div class="row" id="tipo_corr_lin" style="display: none">
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="category">Tipo de Defecto:</label>
																	<label for="radio-inline">
																		<input type="radio" name="tipo_corr_lin" id="tipo_corr_lin1" onclick="check_tipo_corr_lin()" value="1"><i></i>Interno
																	</label>
																	<label for="radio-inline">
																		<input type="radio" name="tipo_corr_lin" id="tipo_corr_lin2" onclick="check_tipo_corr_lin()" value="2"><i></i>Externo
																	</label>
																</div>
																<div class="form-group" id="datos_corr_lin" style="display: none">
																	<label for="category"><strong>Información de la Corrosión Lineal</strong></label>
																	<label class="input"> Espesor de Pared:
																		<input type="text" readonly name="espesor_cilindro2" id="espesor_cilindro2" value="<?php echo isset($esp_fab_pev) ? $esp_fab_pev : NULL; ?>" size="12">
																	</label>
																	<label class="input">Profundidad Corrosión:
																		<input type="text" name="prof_corr_lin" id="prof_corr_lin" onchange="validar_prof_corr_lin(this.value)" size="10">
																	</label>
																	<label class="input">Espesor Minimo de Diseño:
																		<input type="text" name="prof_min_dis_lin" id="prof_min_dis_lin" onchange="validar_corr_esp_min(this.value)" size="9">
																	</label>
																	<label class="input">Longitud de Corrosión:
																		<input type="text" name="long_corr_lin" id="long_corr_lin" onchange="validar_long_corr_lin(this.value)" size="9">
																	</label>
																</div>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Cortes, Perforaciones, Ranuras: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('cortes_perf')">
																</label>                            
															
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="corr_perfo" id="corr_perfo1" onclick="checkCorrPerfo('1');" value="1" <?php if($cort_perfo_ranura_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="corr_perfo" id="corr_perfo2" onclick="checkCorrPerfo('0');" value="2" <?php if($cort_perfo_ranura_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="cort_perfo_ranura_h" id="cort_perfo_ranura_h">
																	</label>
																</div>
															</div>  
														</div>
														<div class="row" id="tipo_cor_perf" style="display: none">
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="category">Tipo de Defecto:</label>
																	<label for="radio-inline">
																		<input type="radio" name="tipo_cor_perf" id="tipo_cor_perf1" onclick="check_tipo_cor_perf()" value="1"><i></i>Interno
																	</label>
																	<label for="radio-inline">
																		<input type="radio" name="tipo_cor_perf" id="tipo_cor_perf2" onclick="check_tipo_cor_perf()" value="2"><i></i>Externo
																	</label>
																</div>
																<div class="form-group" id="datos_cor_perf" style="display: none">
																	<label for="category"><strong>Información de los Cortes, Perforaciones, Ranuras</strong></label>
																	<label class="input"> Espesor de Pared:
																		<input type="text" readonly name="espesor_cilindro3" id="espesor_cilindro3" value="<?php echo isset($esp_fab_pev) ? $esp_fab_pev : NULL; ?>" size="12">
																	</label>
																	<label class="input">Profundidad del Defecto:
																		<input type="text" name="prof_defect" id="prof_defect" onchange="validar_prof_defect(this.value)" size="10">
																	</label>
																	<label class="input">Espesor Minimo de Diseño:
																		<input type="text" name="prof_min_dis_defe" id="prof_min_dis_defe" onchange="validar_defe_esp_min(this.value)" size="9">
																	</label>
																	<label class="input">Longitud de Corrosión:
																		<input type="text" name="long_defect" id="long_defect" onchange="validar_long_defect(this.value)" size="9">
																	</label>
																</div>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Abolladuras: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('abolladuras')"></label>
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="abolladuras" id="abolladuras1" onclick="checkAbolladura('1')" value="1" <?php if($abolladuras_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="abolladuras" id="abolladuras2" onclick="checkAbolladura('0')" value="2" <?php if($abolladuras_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="abolladuras_h" id="abolladuras_h">
																	</label>
																</div>
															</div>  
														</div>
														<div class="row" id="info_abolladura" style="display: none">
															<div class="col-sm-6">
																<label for="category"><strong>Información de la Abolladura</strong></label>
																<label class="input"> Profundidad del defecto:
																	<input type="text" name="prof_abolladura" id="prof_abolladura" onchange="validar_prof_abolladura(this.value)" size="10">
																</label>
																<label class="input">Diametro del Defecto:
																	<input type="text" name="diam_abolladura" id="diam_abolladura" onchange="validar_diam_abolladura(this.value)" size="10">
																</label>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Fugas y Poros: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('fugas_poros')"></label>
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="fugas_poros" id="fugas_poros1" onclick="checkFugasPoros('1');"  value="1" <?php if($fugas_poros_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="fugas_poros" id="fugas_poros2" onclick="checkFugasPoros('0');" value="2" <?php if($fugas_poros_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="fugas_poros_h" id="fugas_poros_h">
																	</label>
																</div>
															</div>  
														</div>
													</div>
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Abombamiento: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('abombamiento')"></label>
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="abombamiento" id="abombamiento1" onclick="checkAbombamiento('1');" value="1" <?php if($abombamiento_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="abombamiento" id="abombamiento2" onclick="checkAbombamiento('0');" value="2" <?php if($abombamiento_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="abombamiento_h" id="abombamiento_h">
																	</label>
																</div>
															</div>  
														</div>
													</div>
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Daño por Fuego y Daño Termico: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('fuego_termico')"></label>
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="fuego_termico" id="fuego_termico1" onclick="checkFuegoTermico('1');" value="1" <?php if($fuego_termico_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="fuego_termico" id="fuego_termico2" onclick="checkFuegoTermico('0');" value="2" <?php if($fuego_termico_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="fuego_termico_h" id="fuego_termico_h">
																	</label>
																</div>
															</div>  
														</div>
														<div class="row" id="info_fuego_termico" style="display: none">
															<div class="col-sm-6">
																<label for="category"><strong>Descripción del Daño:</strong></label>
																<label class="input">
																	<textarea name="descrip_fuego_termico" cols="30" rows="4"></textarea>
																</label>
															</div>
														</div>
													</div>			 
												</div>
												<div class="well well-sm well-primary">                                           
													<fieldset>
														<legend><center>DEFECTOS DEL CUELLO</center></legend>     
														<div class="row">
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="category">Grietas: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('grietas_cuello')"></label>                            
																</div>  
															</div>   
															<div class="col-sm-6">
																<div class="form-group">
																	<div class="inline-group">
																		<label class="radio-inline">
																			<input type="radio" name="grietaCuello" id="grietaCuello1" onclick="checkGrietaCuello('1');" value="1" <?php if($grietas_cuello_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																			<i></i>Si
																		</label>
																		<label class="radio-inline">
																			<input type="radio" name="grietaCuello" id="grietaCuello2" onclick="checkGrietaCuello('0');" value="2" <?php if($grietas_cuello_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																			<i></i>No
																			<input type="hidden" name="grieta_cuello_h" id="grieta_cuello_h">
																		</label>
																	</div>
																</div>  
															</div>
														</div>
														<div class="row">
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="category">Pliegues: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('pliegues_cuello')"></label>                            
																</div>  
															</div>   
															<div class="col-sm-6">
																<div class="form-group">
																	<div class="inline-group">
																		<label class="radio-inline">
																			<input type="radio" name="pliegueCuello" id="pliegueCuello1" onclick="checkPliegueCuello('1');" value="1" <?php if($pliegue_cuello_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																			<i></i>Si
																		</label>
																		<label class="radio-inline">
																			<input type="radio" name="pliegueCuello" id="pliegueCuello2" onclick="checkPliegueCuello('0');" value="2" <?php if($pliegue_cuello_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																			<i></i>No
																			<input type="hidden" name="pliegue_cuello_h" id="pliegue_cuello_h">
																		</label>
																	</div>
																</div>  
															</div>
														</div>      
														<div class="row">
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="category">Valles: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('valles_cuello')"></label>                            
																</div>  
															</div>   
															<div class="col-sm-6">
																<div class="form-group">
																	<div class="inline-group">
																		<label class="radio-inline">
																			<input type="radio" name="valleCuello" id="valleCuello1" onclick="checkValleCuello('1');" value="1" <?php if($valle_cuello_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																			<i></i>Si
																		</label>
																		<label class="radio-inline">
																			<input type="radio" name="valleCuello" id="valleCuello2" onclick="checkValleCuello('0');" value="2" <?php if($valle_cuello_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																			<i></i>No
																			<input type="hidden" name="valle_cuello_h" id="valle_cuello_h">
																		</label>
																	</div>
																</div>  
															</div>
														</div>		
																                                    
													</fieldset>                              
												</div>
												<div class="well well-sm well-primary">     
													<div class="row">
														<div class="form-group">
															<label for="category">Observaciones Generales:</label>
															<textarea class="form-control" name="observaciones"><?php echo $observaciones; ?></textarea>
														</div>
													</div>
												</div> 
												<div class="modal-footer">
													<?php
														if (in_array(22, $acc))
														{
															if($prueba_realizada != 1){
														?>
															<input type="submit" value="Guardar" name="g_insp_visual_acero" id="g_insp_visual_acero" class="btn btn-primary" />
														<?php
															}
														}                   
														?>
												</div>
											</form>
									 </div>
								</div>
						  </div>
					 </article>
				</div>
		  </section>
	 </div>
</div>
<!-- END MAIN PANEL -->
<div class="modal fade" id="modal_info_corrosion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">CORROSIÓN GENERAL</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	El espesor de pared del cilindro es desconocido, la profundidad permisible  de corrosión es superior a 0,787 mm (0,03 pulgadas).
								</p>
								<p>
									*	El espesor de pared del cilindro es conocido, la profundidad permisible de corrosión es superior al 15% del espesor mínimo de diseño. 
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>
<div class="modal fade" id="modal_info_picadura" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">PICADURAS AISLADAS</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	El espesor de pared del cilindro es desconocido, la profundidad permisible  de corrosión es superior a 1,575 mm (0,062 pulgadas).
								</p>
								<p>
									*	 El espesor de pared del cilindro es conocido, la profundidad permisible de corrosión es superior al 30% del espesor mínimo de diseño. 
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>
<div class="modal fade" id="modal_info_corrosionl" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">CORROSIÓN LINEAL</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	El espesor de pared del cilindro es desconocido, la profundidad permisible  de corrosión es superior a 0,787 mm (0,031 pulgadas).
								</p>
								<p>
									*	El espesor de pared del cilindro es conocido, la profundidad permisible de corrosión es superior al 15% del espesor mínimo de diseño. 
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>
<div class="modal fade" id="modal_info_cortes_perf" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">CORTES, PERFORACIONES Y RANURAS</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	El espesor de pared del cilindro es desconocido, la profundidad del corte es superior a 0,787 mm (0,031 pulgadas).
								</p>
								<p>
									*	El espesor de pared del cilindro es conocido, la profundidad del corte es superior al 15% del espesor mínimo de diseño. 
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>
<div class="modal fade" id="modal_info_abolladuras" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">ABOLLADURAS</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	Una o más abolladuras que sean de diámetro menor a 50,8 mm (2 pulgadas) 
								</p>
								<p>
									*	Abolladuras superiores a 1,575 mm (0,062 pulgadas) de profundidad y su diámetro es igual o inferior a 50,8 mm (2 pulgadas)  
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>
<div class="modal fade" id="modal_info_fugas_poros" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">CORTES, PERFORACIONES Y RANURAS</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	Cilindros con fugas a través del metal 
								</p>
								<p>
									*	Cilindros con poros 
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>
<div class="modal fade" id="modal_info_abombamiento" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">AMBOMBAMIENTO</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	Todo cilindro con abombamiento visiblemente notable
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>
<div class="modal fade" id="modal_info_fuego_termico" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">DAÑOS POR FUEGO Y DAÑO TERMICO</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	Todo cilindro calentado a  temperaturas de metal de más de 176,7 °C  
								</p>
								<p>
									*	Todo cilindro con quemaduras de arco 
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>
<div class="modal fade" id="modal_info_grietas_cuello" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">GRIETAS EN EL CUELLO</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	Grietas visibles en el cuello 
								</p>
								
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>
<div class="modal fade" id="modal_info_pliegues_cuello" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">PLIEGUES EN EL CUELLO</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	Cilindros con pliegues que entren en más de un hilo de rosca completo y continuo 
								</p>
								
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>
<div class="modal fade" id="modal_info_valles_cuello" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">VALLES EN EL CUELLO</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	Cilindros con uno o más valles
								</p>
								
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->
<?php
	 include("inc/footer.php");
?>
<!-- END PAGE FOOTER -->

<?php 
	 //include required scripts
	 include("inc/scripts.php"); 
?>


<!-- PAGE RELATED PLUGIN(S) 
<script src="..."></script>-->
<!-- Flot Chart Plugin: Flot Engine, Flot Resizer, Flot Tooltip -->
<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>

<!-- Vector Maps Plugin: Vectormap engine, Vectormap language -->
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>

<!-- Full Calendar -->
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/jquery-form/jquery-form.min.js"></script>

<script type="text/javascript">
function info(ventana){
	 $('#modal_info_'+ventana).modal();
}



function checkCorrosionGeneral(valor){
  	valorCheck = valor;

  	if(valorCheck == 1){
  		document.getElementById('tipo_corrosion_general').style.display = 'block';
  		document.getElementById('corrosion_general_h').value = '1';
  		
  	}else if(valorCheck == 0){
  		document.getElementById('tipo_corrosion_general').style.display = 'none';
  		document.getElementById('tipo_corr_gene1').checked = false;
  		document.getElementById('tipo_corr_gene2').checked = false;
  		document.getElementById('datos_corrosion_general').style.display = 'none';
  		document.getElementById('corrosion_general_h').value = '2';
  	}
}
function check_corro_gral_interna(valor){
	valorCheck = valor;
	
	document.getElementById('datos_corrosion_general').style.display = 'block';
}

function inhabilitarFormulario(check, campo){

	habilitar = check;
	llenar = campo;

	document.getElementById("corr_gene1").disabled = true;
	document.getElementById("corr_gene2").disabled = true;
	document.getElementById("picadura1").disabled = true;
	document.getElementById("picadura2").disabled = true;
	document.getElementById("corr_line1").disabled = true;
	document.getElementById("corr_line2").disabled = true;
	document.getElementById("corr_perfo1").disabled = true;
	document.getElementById("corr_perfo2").disabled = true;
	document.getElementById("abolladuras1").disabled = true;
	document.getElementById("abolladuras2").disabled = true;
	document.getElementById("fugas_poros1").disabled = true;
	document.getElementById("fugas_poros2").disabled = true;
	document.getElementById("abombamiento1").disabled = true;
	document.getElementById("abombamiento2").disabled = true;
	document.getElementById("fuego_termico1").disabled = true;
	document.getElementById("fuego_termico2").disabled = true;
	document.getElementById("grietaCuello1").disabled = true;
	document.getElementById("grietaCuello2").disabled = true;
	document.getElementById("pliegueCuello1").disabled = true;
	document.getElementById("pliegueCuello2").disabled = true;
	document.getElementById("valleCuello1").disabled = true;
	document.getElementById("valleCuello2").disabled = true;

	document.getElementById("corrosion_general_h").value = 2;
	document.getElementById("picaduras_aisladas_h").value = 2;
	document.getElementById("corrosion_lineal_h").value = 2;
	document.getElementById("cort_perfo_ranura_h").value = 2;
	document.getElementById("abolladuras_h").value = 2;
	document.getElementById("fugas_poros_h").value = 2;
	document.getElementById("abombamiento_h").value = 2;
	document.getElementById("fuego_termico_h").value = 2;
	document.getElementById("grieta_cuello_h").value = 2;
	document.getElementById("pliegue_cuello_h").value = 2;
	document.getElementById("valle_cuello_h").value = 2;

	document.getElementById("corr_gene2").checked = true;	
	document.getElementById("picadura2").checked = true;
	document.getElementById("corr_line2").checked = true;
	document.getElementById("corr_perfo2").checked = true;
	document.getElementById("abolladuras2").checked = true;
	document.getElementById("fugas_poros2").checked = true;
	document.getElementById("abombamiento2").checked = true;
	document.getElementById("fuego_termico2").checked = true;
	document.getElementById("grietaCuello2").checked = true;
	document.getElementById("pliegueCuello2").checked = true;
	document.getElementById("valleCuello2").checked = true;

	document.getElementById(habilitar).checked = true;
	document.getElementById(llenar).value = 1;
}

function validar_prof_corr_gene(valor){
	var valor_prof_corr_gral = valor;
	var porc_espesor = 0;
	var espesor_fab = document.getElementById("espesor_cilindro").value;

	if(espesor_fab=="DESCONOCIDO"){
		
		if(valor_prof_corr_gral >= 0.787){
			if(confirm("Con estos datos el cilindro será condenado, ¿Desea continuar?")){
				alert("Cilindro Condenado!");
				document.getElementById("area_corr_gene").readOnly = true;
				document.getElementById("area_cilindro").readOnly = true;
				document.getElementById("prof_corr_gene").readOnly = true;
				document.getElementById('estado_prueba').value = 'CONDENADO';

				inhabilitarFormulario('corr_gene1','corrosion_general_h');
				document.getElementById('motivo_condena'). value = 'corrosion_general';
			}else{
				document.getElementById("prof_corr_gene").value = "";
			}
		}

	}else{
		
		porc_espesor = valor*100/espesor_fab;

		if(porc_espesor >= 15){
			if(confirm("Con estos datos el cilindro será condenado, ¿Desea continuar?1")){
				alert("Cilindro Condenado!");

				document.getElementById("area_corr_gene").readOnly = true;
				document.getElementById("area_cilindro").readOnly = true;
				document.getElementById("prof_corr_gene").readOnly = true;
				document.getElementById('estado_prueba').value = 'CONDENADO';

				inhabilitarFormulario('corr_gene1','corrosion_general_h');
				document.getElementById('motivo_condena'). value = 'corrosion_general';
			}else{
				document.getElementById("prof_corr_gene").value = "";
			}
		}
	}
}

function validar_ext_corr_gene(valor){

	var superficie_cilindro = valor;
	var porc_ext = 0;
	var extension_corrosion_gral = document.getElementById("area_corr_gene").value;

	porc_ext = extension_corrosion_gral * 100 / superficie_cilindro;

	if(porc_ext >= 25){
		if(confirm("Con estos datos el cilindro será condenado, ¿Desea continuar?")){
			alert("Cilindro Condenado!");
			document.getElementById("area_corr_gene").readOnly = true;
			document.getElementById("area_cilindro").readOnly = true;
			document.getElementById("prof_corr_gene").readOnly = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';

			inhabilitarFormulario('corr_gene1','corrosion_general_h');
			document.getElementById('motivo_condena'). value = 'corrosion_general';
		}else{
			document.getElementById("area_corr_gene").value = "";
			document.getElementById("area_cilindro").value = "";
		}
	}
}

function checkPicaduraAislada(valor){

	valorCheck = valor;

	if(valorCheck == 1){

		document.getElementById("tipo_picadura_aislada").style.display = 'block';
		document.getElementById('picaduras_aisladas_h').value = '1';
	}else if(valorCheck == 0){

		document.getElementById("tipo_picadura_aislada").style.display = 'none';
		document.getElementById("tipo_picadura_aislada1").checked = false;
		document.getElementById("tipo_picadura_aislada2").checked = false;
		document.getElementById("datos_picadura_aislada").style.display = 'none';
		document.getElementById('picaduras_aisladas_h').value = '2';
	}
}

function check_picadura_aislada_interna(valor){

	document.getElementById("datos_picadura_aislada").style.display = 'block';
}

function validar_prof_pic_ais(valor){

	var espesor = document.getElementById("espesor_cilindro1").value;	
	var profundidad_corrosion = valor;

	if(espesor == "DESCONOCIDO"){

		if(profundidad_corrosion >= 1.575){

			if(confirm("Con estos datos el cilindro será condenado, ¿Desea continuar?")){

				alert("Cilindro Condenado!");
				document.getElementById("prof_pic_ais").readOnly = true;
				document.getElementById("prof_min_dis").readOnly = true;
				document.getElementById('estado_prueba').value = 'CONDENADO';

				inhabilitarFormulario('picadura1','picaduras_aisladas_h');
				document.getElementById('motivo_condena'). value = 'picaduras_aisladas';
			}else{
				document.getElementById("prof_pic_ais").readOnly = false;
				document.getElementById("prof_min_dis").value = "";
				document.getElementById("prof_pic_ais").value = "";
			}
		}
	}
}

function validar_prof_esp_min(valor){

	var espesor = document.getElementById("espesor_cilindro1").value;
	var profundidad_picadura = document.getElementById("prof_pic_ais").value;
	var espesor_diseno = valor;
	var porc_espesor = 0;

	if(espesor != "DESCONOCIDO"){

		porc_espesor = profundidad_picadura * 100 / espesor_diseno;

		if(porc_espesor >= 30){

			if(confirm("Con estos datos el cilindro será condenado, ¿Desea continuar?")){

				alert("Cilindro Condenado!");
				document.getElementById("prof_pic_ais").readOnly = true;
				document.getElementById("prof_min_dis").readOnly = true;
				document.getElementById('estado_prueba').value = 'CONDENADO';

				inhabilitarFormulario('picadura1','picaduras_aisladas_h');
				document.getElementById('motivo_condena'). value = 'picaduras_aisladas';
			}else{
				document.getElementById("prof_pic_ais").readOnly = false;
				document.getElementById("prof_pic_ais").value = "";
				document.getElementById("prof_min_dis").value="";
			}

		}
	}
}

function checkCorrLine(valor){

	var valorCheck = valor;

	if(valorCheck == 1){
		document.getElementById("tipo_corr_lin").style.display = "block";
		document.getElementById('corrosion_lineal_h').value = '1';
	}else if (valorCheck == 0) {
		document.getElementById("tipo_corr_lin").style.display = "none";
		document.getElementById('corrosion_lineal_h').value = '2';
	}
}

function check_tipo_corr_lin(){

	document.getElementById("datos_corr_lin").style.display = "block";
}

function validar_prof_corr_lin(valor){

	var profundidad_corrosion_lineal = valor;
	var espesor_cilindro = document.getElementById("espesor_cilindro2").value;

	if(espesor_cilindro == "DESCONOCIDO"){

		if(profundidad_corrosion_lineal >= 0.787){

			if(confirm("Con estos datos el cilindro será condenado, ¿Desea continuar?")){

				alert("Cilindro Condenado!");
				document.getElementById("prof_corr_lin").readOnly = true;
				document.getElementById("prof_min_dis_lin").readOnly = true;
				document.getElementById('estado_prueba').value = 'CONDENADO';

				inhabilitarFormulario('corr_line1','corrosion_lineal_h');
				document.getElementById('motivo_condena'). value = 'corrosion_lineal';
			}else{
				document.getElementById("prof_corr_lin").readOnly = false;
				document.getElementById("prof_corr_lin").value = "";
				document.getElementById("prof_min_dis_lin").value="";
			}
		}
	}
}

function validar_corr_esp_min(valor){

	var espesor_minimo = valor;
	var profundidad_corrosion_lineal = document.getElementById("prof_corr_lin").value;
	var espesor_cilindro = document.getElementById("espesor_cilindro2").value;
	var porc_corrosion = 0;

	if(espesor_cilindro != "DESCONOCIDO"){

		porc_corrosion = profundidad_corrosion_lineal * 100 / espesor_minimo;

		if(porc_corrosion >= 15){

			if(confirm("Con estos datos el cilindro será condenado, ¿Desea continuar?")){

				alert("Cilindro Condenado!");
				document.getElementById("prof_corr_lin").readOnly = true;
				document.getElementById("prof_min_dis_lin").readOnly = true;
				document.getElementById('estado_prueba').value = 'CONDENADO';

				inhabilitarFormulario('corr_line1','corrosion_lineal_h');
				document.getElementById('motivo_condena'). value = 'corrosion_lineal';
			}else{
				document.getElementById("prof_corr_lin").readOnly = false;
				document.getElementById("prof_corr_lin").value = "";
				document.getElementById("prof_min_dis_lin").value="";
			}

		}

	}
}

function validar_long_corr_lin(valor){

	var longitud_corrosion = valor;

	if(longitud_corrosion >= 152.4){

		if(confirm("Con estos datos el cilindro será condenado, ¿Desea continuar?")){

			alert("Cilindro Condenado!");
			document.getElementById("prof_corr_lin").readOnly = true;
			document.getElementById("prof_min_dis_lin").readOnly = true;
			document.getElementById("long_corr_lin").readOnly = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';

			inhabilitarFormulario('corr_line1','corrosion_lineal_h');
			document.getElementById('motivo_condena'). value = 'corrosion_lineal';
		}else{
			document.getElementById("prof_corr_lin").readOnly = false;
			document.getElementById("prof_corr_lin").value = "";
			document.getElementById("prof_min_dis_lin").value="";
			document.getElementById("long_corr_lin").readOnly = false;
			document.getElementById("long_corr_lin").value="";
		}

	}
}

function checkCorrPerfo(valor){

	var valorCheck = valor;

	if(valorCheck == 1){

		document.getElementById("tipo_cor_perf").style.display = "block";
		document.getElementById('cort_perfo_ranura_h').value = '1';

	}else if(valorCheck == 0){
		document.getElementById("tipo_cor_perf").style.display = "none";
		document.getElementById('cort_perfo_ranura_h').value = '2';
	}
}

function check_tipo_cor_perf(valor){

	document.getElementById("datos_cor_perf").style.display = "block";
}

function validar_prof_defect(valor){

	var profundidad_defecto = valor;
	var espesor_cilindro = document.getElementById("espesor_cilindro3").value;

	if(espesor_cilindro == "DESCONOCIDO"){

		if(profundidad_defecto >= 0.787){

			if(confirm("Con estos datos el cilindro será condenado, ¿Desea continuar?")){

				alert("Cilindro Condenado!");
				document.getElementById("prof_defect").readOnly = true;
				document.getElementById("prof_min_dis_defe").readOnly = true;
				document.getElementById("long_defect").readOnly = true;
				document.getElementById('estado_prueba').value = 'CONDENADO';

				inhabilitarFormulario('corr_perfo1','cort_perfo_ranura_h');
				document.getElementById('motivo_condena'). value = 'cort_perfo_ranura';
			}else{
				document.getElementById("prof_defect").readOnly = false;
				document.getElementById("prof_defect").value = "";
			}
		}
	}
}

function validar_defe_esp_min(valor){

	var espesor_minimo = valor;
	var espesor_cilindro = document.getElementById("espesor_cilindro3").value;
	var profundidad_defecto = document.getElementById("prof_defect").value;
	var porc_defecto = 0;

	if(espesor_cilindro != "DESCONOCIDO"){

		porc_defecto = profundidad_defecto * 100 / espesor_minimo;

		if(porc_defecto >= 15){

			if(confirm("Con estos datos el cilindro será condenado, ¿Desea continuar?")){

				alert("Cilindro Condenado!");
				document.getElementById("prof_defect").readOnly = true;
				document.getElementById("prof_min_dis_defe").readOnly = true;
				document.getElementById("long_defect").readOnly = true;
				document.getElementById('estado_prueba').value = 'CONDENADO';

				inhabilitarFormulario('corr_perfo1','cort_perfo_ranura_h');
				document.getElementById('motivo_condena'). value = 'cort_perfo_ranura';
			}else{
				document.getElementById("prof_defect").readOnly = false;
				document.getElementById("prof_min_dis_defe").value="";
			}		

		}		
	}
}

function validar_long_defect(valor){

	longitud_defecto = valor;

	if(longitud_defecto >= 152.4){

		if(confirm("Con estos datos el cilindro será condenado, ¿Desea continuar?")){

			alert("Cilindro Condenado!");
			document.getElementById("prof_defect").readOnly = true;
			document.getElementById("prof_min_dis_defe").readOnly = true;
			document.getElementById("long_defect").readOnly = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';

			inhabilitarFormulario('corr_perfo1','cort_perfo_ranura_h');
			document.getElementById('motivo_condena'). value = 'cort_perfo_ranura';
		}else{
			document.getElementById("prof_defect").readOnly = false;
			document.getElementById("long_defect").value="";
		}

	}
}

function checkAbolladura(valor){

	var valorCheck = valor;

	if(valorCheck == 1){
		document.getElementById("info_abolladura").style.display = "block";
		document.getElementById('abolladuras_h').value = '1';
	}else if(valorCheck == 0){
		document.getElementById("info_abolladura").style.display = "none";
		document.getElementById('abolladuras_h').value = '2';
	}
}

function validar_prof_abolladura(valor){

	var profundidad_abolladura = valor;

	if(profundidad_abolladura >= 50.8){
		if(confirm("Con estos datos el cilindro será condenado, ¿Desea continuar?")){

			alert("Cilindro Condenado!");
			document.getElementById("diam_abolladura").readOnly = true;
			document.getElementById("prof_abolladura").readOnly = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';

			inhabilitarFormulario('abolladuras1','abolladuras_h');
			document.getElementById('motivo_condena'). value = 'abolladuras';
		}else{
			document.getElementById("diam_abolladura").readOnly = false;
			document.getElementById("prof_abolladura").value="";
		}
	}
}

function validar_diam_abolladura(valor){

	var diametro_abolladura = valor;

	if(diametro_abolladura >= 1.575){
		if(confirm("Con estos datos el cilindro será condenado, ¿Desea continuar?")){

			alert("Cilindro Condenado!");
			document.getElementById("diam_abolladura").readOnly = true;
			document.getElementById("prof_abolladura").readOnly = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';

			inhabilitarFormulario('abolladuras1','abolladuras_h');
			document.getElementById('motivo_condena'). value = 'abolladuras';
		}else{
			document.getElementById("prof_abolladura").readOnly = false;
			document.getElementById("diam_abolladura").value="";
		}
	}
}

function checkFugasPoros(valor){

	var valorCheck = valor;

	if(valorCheck==1){
		if(confirm("Al seleccionar esta opción el cilindro será condenado, ¿Desea continuar?")){

			alert("Cilindro Condenado!");
			document.getElementById("fugas_poros1").disabled = true;
			document.getElementById("fugas_poros2").disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';

			inhabilitarFormulario('fugas_poros1','fugas_poros_h');
			document.getElementById('motivo_condena'). value = 'fugas_poros';
			document.getElementById('fugas_poros_h').value = '1';
		}else{
			document.getElementById("fugas_poros1").checked = false;
			document.getElementById('fugas_poros_h').value = '2';
		}
	}else if (valorCheck == 0){
		document.getElementById("fugas_poros1").checked = false;
		document.getElementById('fugas_poros_h').value = '2';
	}
}

function checkAbombamiento(valor){

	var valorCheck = valor;

	if(valorCheck==1){
		if(confirm("Al seleccionar esta opción el cilindro será condenado, ¿Desea continuar?")){

			alert("Cilindro Condenado!");
			document.getElementById("abombamiento1").disabled = true;
			document.getElementById("abombamiento2").disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';

			inhabilitarFormulario('abombamiento1','abombamiento_h');
			document.getElementById('motivo_condena'). value = 'abombamiento';
			document.getElementById('abombamiento_h').value = '1';
		}else{
			document.getElementById("abombamiento1").checked = false;
			document.getElementById('abombamiento_h').value = '2';
		}
	}else if (valorCheck == 0){
		document.getElementById("abombamiento1").checked = false;
		document.getElementById('abombamiento_h').value = '2';
	}
}

function checkFuegoTermico(valor){

	var valorCheck = valor;

	if(valorCheck==1){
		if(confirm("Al seleccionar esta opción el cilindro será condenado, ¿Desea continuar?")){

			alert("Cilindro Condenado!");
			document.getElementById("fuego_termico1").disabled = true;
			document.getElementById("fuego_termico2").disabled = true;
			document.getElementById("info_fuego_termico").style.display = "block";
			document.getElementById('estado_prueba').value = 'CONDENADO';

			inhabilitarFormulario('fuego_termico1','fuego_termico_h');
			document.getElementById('motivo_condena'). value = 'fuego_termico';
			document.getElementById('fuego_termico_h').value = '1';
		}else{
			document.getElementById("fuego_termico1").checked = false;
			document.getElementById('fuego_termico_h').value = '2';
		}
	}else if (valorCheck == 0){
		document.getElementById("fuego_termico1").checked = false;
		document.getElementById('fuego_termico_h').value = '2';
	}
}

function checkGrietaCuello(valor){

	var valorCheck = valor;

	if(valorCheck==1){
		if(confirm("Al seleccionar esta opción el cilindro será condenado, ¿Desea continuar?")){

			alert("Cilindro Condenado!");
			document.getElementById("grietaCuello1").disabled = true;
			document.getElementById("grietaCuello2").disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';

			inhabilitarFormulario('grietaCuello1','grieta_cuello_h');
			document.getElementById('motivo_condena'). value = 'grieta_cuello';
			document.getElementById('grieta_cuello_h').value = '1';
		}else{
			document.getElementById("grietaCuello1").checked = false;
			document.getElementById('grieta_cuello_h').value = '2';
		}
	}else if (valorCheck == 0){
		document.getElementById("pliegueCuello1").checked = false;
		document.getElementById('grieta_cuello_h').value = '2';
	}
}

function checkPliegueCuello(valor){

	var valorCheck = valor;

	if(valorCheck==1){
		if(confirm("Al seleccionar esta opción el cilindro será condenado, ¿Desea continuar?")){

			alert("Cilindro Condenado!");
			document.getElementById("pliegueCuello1").disabled = true;
			document.getElementById("pliegueCuello2").disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';

			inhabilitarFormulario('pliegueCuello1','pliegue_cuello_h');
			document.getElementById('motivo_condena'). value = 'pliegue_cuello';
			document.getElementById('pliegue_cuello_h').value = '1';
		}else{
			document.getElementById("pliegueCuello1").checked = false;
			document.getElementById('pliegue_cuello_h').value = '2';
		}
	}else if (valorCheck == 0){
		document.getElementById("pliegueCuello1").checked = false;
		document.getElementById('pliegue_cuello_h').value = '2';
	}
}

function checkValleCuello(valor){

	var valorCheck = valor;

	if(valorCheck==1){
		if(confirm("Al seleccionar esta opción el cilindro será condenado, ¿Desea continuar?")){

			alert("Cilindro Condenado!");
			document.getElementById("valleCuello1").disabled = true;
			document.getElementById("valleCuello2").disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';

			inhabilitarFormulario('valleCuello1','valle_cuello_h');
			document.getElementById('motivo_condena'). value = 'valle_cuello';
			document.getElementById('valle_cuello_h').value = '1';
		}else {
			document.getElementById("valleCuello1").checked = false;
			document.getElementById('valle_cuello_h').value = '2';
		}
	}else if (valorCheck == 0){
		document.getElementById("valleCuello1").checked = false;
		document.getElementById('valle_cuello_h').value = '2';
	}
}
</script>

<?php 
	 //include footer
	 include("inc/google-analytics.php"); 
}
else
{
	 header("Location:index.php");
}
?>