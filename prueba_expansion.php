<?php
/*define('DURACION_SESION','7200');
ini_set("session.cookie_lifetime", DURACION_SESION);
ini_set("session.gc_maxlifetime", DURACION_SESION);
ini_set("session.save_path", "/tmp");
session_cache_expire(DURACION_SESION);*/
session_start();
//session_regenerate_id(true);
/* ARCHIVO QUE REALIZA LA PRUEBA DE EXPANSIÓN VOLUMÉTRICA DEL CILINDO ESCOGIDO*/

if($_SESSION['logged'] == yes)
{
	require("libraries/conexion.php");
	require_once("inc/init.php");
	require_once("inc/config.ui.php");
	$page_title = "Prueba";
	$page_css[] = "your_style.css";
	include("inc/header.php");
	$page_nav["pev"]["sub"]["prueba"]["active"] = true;
	include("inc/nav.php");
	//Identificación del usuario que hace la prueba
	$idUser = $_SESSION['su'];
	//SE SOLICITAN LOS DATOS DE CONSULTA
	$id_has_movimiento_cilindro_pev = isset($_REQUEST['id_has_movimiento_cilindro_pev']) ? $_REQUEST['id_has_movimiento_cilindro_pev'] : NULL;
    $num_cili = isset($_REQUEST['num_cili']) ? $_REQUEST['num_cili'] : NULL;

    $fecha_hora = date("Y-m-d H:i:s");

    if(strlen($num_cili) > 0)
    {
    	//SE CONSULTAN LOS DATOS EN BASE    
	    $consulta_1 = "SELECT * 
	    			   FROM cilindro_pev
	    			   WHERE num_cili_pev = '".$num_cili."' AND tipo = '2'";
	    $resultado_1 = mysqli_query($con,$consulta_1);
	    if(mysqli_num_rows($resultado_1) > 0)
	    {
	    	$linea_1 = mysqli_fetch_assoc($resultado_1);
	    	$id_cilindro_pev = isset($linea_1["id_cilindro_pev"]) ? $linea_1["id_cilindro_pev"] : NULL;  
	    	$num_cili_pev = isset($linea_1["num_cili_pev"]) ? $linea_1["num_cili_pev"] : NULL;  
			$num_cili_pev_2 = isset($linea_1["num_cili_pev_2"]) ? $linea_1["num_cili_pev_2"] : NULL;
			$especi_pev = isset($linea_1["especi_pev"]) ? $linea_1["especi_pev"] : NULL;
			$pre_tra_pev = isset($linea_1["pre_tra_pev"]) ? $linea_1["pre_tra_pev"] : NULL;
			$pre_pru_pev = isset($linea_1["pre_pru_pev"]) ? $linea_1["pre_pru_pev"] : NULL;
			$fecha_fab_pev = isset($linea_1["fecha_fab_pev"]) ? $linea_1["fecha_fab_pev"] : NULL;  
			$fecha_ult_pev = isset($linea_1["fecha_ult_pev"]) ? $linea_1["fecha_ult_pev"] : NULL;
			$volumen_pev = isset($linea_1["volumen_pev"]) ? $linea_1["volumen_pev"] : NULL;
			$tara_esta_pev = isset($linea_1["tara_esta_pev"]) ? $linea_1["tara_esta_pev"] : NULL;
			$tara_actu_pev = isset($linea_1["tara_actu_pev"]) ? $linea_1["tara_actu_pev"] : NULL;
			$esp_fab_pev = isset($linea_1["esp_fab_pev"]) ? $linea_1["esp_fab_pev"] : NULL;
			$esp_actu_pev = isset($linea_1["esp_actu_pev"]) ? $linea_1["esp_actu_pev"] : NULL;
			$id_conexion = isset($linea_1["cone_val_pev"]) ? $linea_1["cone_val_pev"] : NULL;
			$fecha_lav_pev = isset($linea_1["fecha_lav_pev"]) ? $linea_1["fecha_lav_pev"] : NULL;
			$id_propiedad_cilindro = isset($linea_1["propiedad_pev"]) ? $linea_1["propiedad_pev"] : NULL;
			$tipo_cili_pev = isset($linea_1["tipo_cili_pev"]) ? $linea_1["tipo_cili_pev"] : NULL;
			$id_tipo_cilindro = isset($linea_1["id_tipo_cilindro"]) ? $linea_1["id_tipo_cilindro"] : NULL;
			$id_tipo_envace = isset($linea_1["id_tipo_envace"]) ? $linea_1["id_tipo_envace"] : NULL;
			$volumen_actu = isset($linea_1["volumen_actu"]) ? $linea_1["volumen_actu"] : NULL;
			$id_estado = isset($linea_1["id_estado"]) ? $linea_1["id_estado"] : NULL;
			$id_cliente = isset($linea_1["id_cliente"]) ? $linea_1["id_cliente"] : NULL;
			$id_especificacion = isset($linea_1["id_especi_pev"]) ? $linea_1["id_especi_pev"] : NULL;
			$id_conversion = isset($linea_1["id_conversion"]) ? $linea_1["id_conversion"] : NULL;
			$tipo_eto_pev = isset($linea_1["tipo"]) ? $linea_1["tipo"] : NULL;
			$id_tipo_gas_pev = isset($linea_1["id_tipo_gas_pev"]) ? $linea_1["id_tipo_gas_pev"] : NULL;
			$pre_trabajo = isset($linea_1["pre_trabajo"]) ? $linea_1["pre_trabajo"] : NULL;
			$fech_crea = isset($linea_1["fech_crea"]) ? $linea_1["fech_crea"] : NULL;
			/////////////////////////////////////////////////////////////////////////////////////////
			$consulta_2  = "SELECT * FROM estados_pev WHERE id_estado_pev = '".$id_estado."'";
		  	$resultado_2 = mysqli_query($con,$consulta_2) ;
		  	$linea_2 = mysqli_fetch_assoc($resultado_2);
		  	if(mysqli_num_rows($resultado_2) > 0)
		  	{
		  		$estado_cili_pev = isset($linea_2["estado_cili_pev"]) ? $linea_2["estado_cili_pev"] : NULL;	
		  	}	  	
		  	mysqli_free_result($resultado_2);
		  	/////////////////////////////////////////////////////////////////////////////////////////
		  	$consulta_3  = "SELECT * FROM clientes WHERE id_cliente = '".$id_cliente."'";
		  	$resultado_3 = mysqli_query($con,$consulta_3) ;
		  	if(mysqli_num_rows($resultado_3) > 0)
		  	{
		  		$linea_3 = mysqli_fetch_assoc($resultado_3);
		  		$nombre_cliente = isset($linea_3["nombre"]) ? $linea_3["nombre"] : NULL;
		  	}	  	
		  	mysqli_free_result($resultado_3);

		  	$consulta_4  = "SELECT * FROM tipo_envace WHERE id_tipo_envace = '".$id_tipo_envace."'";
		  	$resultado_4 = mysqli_query($con,$consulta_4);
		  	if(mysqli_num_rows($resultado_4) > 0)
		  	{		  		
			  	$linea_4 = mysqli_fetch_assoc($resultado_4);
			  	$tipo = isset($linea_4["tipo"]) ? $linea_4["tipo"] : NULL;	
		  	}
		  	mysqli_free_result($resultado_4);

		 	$consulta_5  = "SELECT * FROM especificacion WHERE id_especificacion = '".$id_especificacion."'";
		  	$resultado_5 = mysqli_query($con,$consulta_5);
		  	if(mysqli_num_rows($resultado_5) > 0)
		  	{		  		
			  	$linea_5 = mysqli_fetch_assoc($resultado_5);
			  	$especificacion = isset($linea_5["especificacion"]) ? $linea_5["especificacion"] : NULL;	
		  	}
		  	mysqli_free_result($resultado_5);

		  	$consulta_6  = "SELECT * FROM tipo_gas_pev WHERE id_tipo_gas_pev = '".$id_tipo_gas_pev."'";
		  	$resultado_6 = mysqli_query($con,$consulta_6);
		  	if(mysqli_num_rows($resultado_6) > 0)
		  	{		  		
			  	$linea_6 = mysqli_fetch_assoc($resultado_6);
			  	$nombre_gas = isset($linea_6["nombre"]) ? $linea_6["nombre"] : NULL;	
		  	}
		  	mysqli_free_result($resultado_6);

		  	$consulta_7  = "SELECT * FROM conversion WHERE id_conversion = '".$id_conversion."'";
		  	$resultado_7 = mysqli_query($con,$consulta_7);
		  	if(mysqli_num_rows($resultado_7) > 0)
		  	{		  		
			  	$linea_7 = mysqli_fetch_assoc($resultado_7);
			  	$conversion = isset($linea_7["conversion"]) ? $linea_7["conversion"] : NULL;	
		  	}
		  	mysqli_free_result($resultado_7);

		  	$consulta_8  = "SELECT * FROM tipo_cili WHERE id_tipo_cili = '".$tipo_cili_pev."'";
		  	$resultado_8 = mysqli_query($con,$consulta_8);
		  	if(mysqli_num_rows($resultado_8) > 0)
		  	{		  		
			  	$linea_8 = mysqli_fetch_assoc($resultado_8);
			  	$tipo_cilindro = isset($linea_8["tipo"]) ? $linea_8["tipo"] : NULL;	
		  	}
		  	mysqli_free_result($resultado_8);

		  	$consulta_9  = "SELECT * FROM conexion_valvula_pev WHERE id_valvula_pev = '".$id_conexion."'";
		  	$resultado_9 = mysqli_query($con,$consulta_9);
		  	if(mysqli_num_rows($resultado_9) > 0)
		  	{		  		
			  	$linea_9 = mysqli_fetch_assoc($resultado_9);
			  	$tipo_valvula = isset($linea_9["tipo_valvula"]) ? $linea_9["tipo_valvula"] : NULL;	
		  	}
		  	mysqli_free_result($resultado_9);

		  	$consulta_10  = "SELECT propiedad FROM propiedad_cilindro WHERE id_propiedad_cilindro = '".$id_propiedad_cilindro."'";
		  	$resultado_10 = mysqli_query($con,$consulta_10);
		  	if(mysqli_num_rows($resultado_10) > 0)
		  	{		  		
			  	$linea_10 = mysqli_fetch_assoc($resultado_10);
			  	$propiedad = isset($linea_10["propiedad"]) ? $linea_10["propiedad"] : NULL;	
		  	}
		  	mysqli_free_result($resultado_10);

		  	$consulta_11  = "SELECT propiedad FROM propiedad_cilindro WHERE id_propiedad_cilindro = '".$id_propiedad_cilindro."'";
		  	$resultado_11 = mysqli_query($con,$consulta_11);
		  	if(mysqli_num_rows($resultado_11) > 0)
		  	{		  		
			  	$linea_11 = mysqli_fetch_assoc($resultado_11);
			  	$propiedad = isset($linea_11["propiedad"]) ? $linea_11["propiedad"] : NULL;	
		  	}
		  	mysqli_free_result($resultado_11);

		  	$consulta_12  = "SELECT estado_cilindro_pev FROM estado_cilindro_pev WHERE id_estado_cilindro_pev = '".$id_estado."'";
		  	$resultado_12 = mysqli_query($con,$consulta_12);
		  	if(mysqli_num_rows($resultado_12) > 0)
		  	{		  		
			  	$linea_12 = mysqli_fetch_assoc($resultado_12);
			  	$estado_cilindro_pev = isset($linea_12["estado_cilindro_pev"]) ? $linea_12["estado_cilindro_pev"] : NULL;	
		  	}
		  	mysqli_free_result($resultado_12);
		  	 				
	    }
	    mysqli_free_result($resultado_1);
    }

	    
    

	?>
	<style type="text/css">
		.custab{
			border: 1px solid #CCC;
			padding: 1px;
			margin: 5% 0;
			box-shadow: 3px 3px 2px #CCC;
			transition: 0.5s;
		}
		.custab:hover{
			box-shadow: 3px 3px 0px transparent;
			transition: 0.5s;
		}
	</style>
	<div id="main" role="main">
		<div id="content">
			<section id="widget-grid" class="">
				<div class="row">
					<article class="col-md-5 col-md-offset-0">
						<div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
							<header>
								<span class="widget-icon"><i class="fa fa-check"></i></span>								
							</header>
							<div>
								<div class="jarviswidget-editbox"></div>
								<div class="widget-body no-padding">
									<h2 align="center">RESUMEN DATOS CILÍNDRO</h2>								
									<table class="table table-striped custab" align="center" border="1">
										<tbody>
											<tr>
												<td># CILÍNDRO:</td>
												<td><?php echo $num_cili_pev;?></td>
												<td>ESPECIFICACIÓN:</td>
												<td><?php echo $especificacion."-".$especi_pev;?></td>
											</tr>
											<tr>
												<td>TIPO GAS:</td>
												<td><?php echo $nombre_gas;?></td>
												<td>CAPACIDAD:</td>
												<td><?php echo "capacidad";?></td>
											</tr>
											<tr>
												<td>UNIDAD:</td>
												<td><?php echo $conversion;?></td>
												<td>PRE-TRABAJO:</td>
												<td><?php echo $pre_trabajo;?></td>
											</tr>
											<tr>
												<td>PRESIÓN TRABAJO (Psi):</td>
												<td><?php echo $pre_tra_pev;?></td>
												<td style="background-color: yellow;"><strong>PRESIÓN PRUEBA (Psi):</strong></td>
												<td style="background-color: yellow;"><strong><?php echo $pre_pru_pev;?></strong></td>
											</tr>
											<tr>
												<td>FECHA FABRICACIÓN:</td>
												<td><?php echo $fecha_fab_pev;?></td>
												<td>FECHA ÚLTIMA REVISIÓN:</td>
												<td><?php echo $fecha_ult_pev;?></td>
											</tr>
											<tr>
												<td>VOLÚMEN ESTAMPADO (Lt):</td>
												<td><?php echo $volumen_actu;?></td>
												<td>VOLUMEN REVISADO (Lt):</td>
												<td><?php echo $volumen_pev;?></td>
											</tr>
											<tr>
												<td>TARA ESTAMPADA (Kg):</td>
												<td><?php echo $tara_esta_pev;?></td>
												<td>TARA VERIFICADA (Kg):</td>
												<td><?php echo $tara_actu_pev;?></td>
											</tr>
											<tr>
												<td>ESPESOR FABRICACIÓN (mm):</td>
												<td><?php echo $esp_fab_pev;?></td>
												<td>ESPESOR ACTUAL (mm):</td>
												<td><?php echo $esp_actu_pev;?></td>
											</tr>
											<tr>
												<td>TIPO CILINDRO:</td>
												<td><?php echo $tipo_cilindro;?></td>
												<td>CONEXIÓN VÁLVULA:</td>
												<td><?php echo $tipo_valvula;?></td>
											</tr>
											<tr>
												<td>FECHA DE LAVADO:</td>
												<td><?php echo $fecha_lav_pev;?></td>
												<td>PROPIEDAD:</td>
												<td><?php echo $propiedad;?></td>
											</tr>
											<tr>
												<td>ESTADO:</td>
												<td><?php echo $estado_cilindro_pev;?></td>
											</tr>
										</tbody>								
									</table>
								</div>
							</div>
						</div>
						<div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
							<header>
								<span class="widget-icon"><i class="fa fa-check"></i></span>
								<h2 align="center">RESUMEN PRUEBAS REALIZADAS</h2>
							</header>
							<div>
								<div class="widget-body no-padding">									
									<table class="table table-striped custab" align="center" border="1">
										<thead>
											<tr align="center">
												<th>FECHA<br>PRUEBA</th>
												<th>EXPANSIÓN<br>TOTAL</th>
												<th>EXPANSIÓN<br>PERMANENTE (gr.)</th>
												<th>RESULTADO (%)</th>
												<th>ESTADO</th>
											</tr>
										</thead>
										<tbody>
											<?php

											$consulta_13 = "SELECT * 
															FROM datos_prueba_pev 
															WHERE id_has_movimiento_cilindro_pev = '".$id_has_movimiento_cilindro_pev."'";
											$resultado_13 = mysqli_query($con,$consulta_13);
											if(mysqli_num_rows($resultado_13) > 0)
											{
												while($linea_13 = mysqli_fetch_assoc($resultado_13))
												{
													$id_datos_prueba_pev = $linea_13['id_datos_prueba_pev'];
													$fecha_hora = $linea_13['fecha_hora'];
													//$expansion_total_1 = $linea_13['expansion_total_1'];
													$expansion_total_2 = $linea_13['expansion_total_2'];
													$expansion_total_3 = $linea_13['expansion_total_3'];
													$porcentaje_prueba = $linea_13['porcentaje_prueba'];
													$impresion = $linea_13['impresion'];
													if($porcentaje_prueba >= 10)
													{
														if($impresion==1){
															$estado_prueba = "RECHAZADO/</br>TERMINADO";
														}else{
															$estado_prueba = "RECHAZADO";
														}
														$link = "<a href='pdf_certificado_pev_prueba.php?id_datos_prueba_pev=".$id_datos_prueba_pev."&id_has_movimiento_cilindro_pev=".$id_has_movimiento_cilindro_pev."'><img src='img/pdf_icon.png' width='30' height='30'></a>";													
													}
													else
													{
														if($impresion==1){
															$estado_prueba = "APROBADO/</br>TERMINADO";
														}else{
															$estado_prueba = "APROBADO";
														}
														$link = "<a href='pdf_certificado_pev_prueba.php?id_datos_prueba_pev=".$id_datos_prueba_pev."&id_has_movimiento_cilindro_pev=".$id_has_movimiento_cilindro_pev."'><img src='img/pdf_icon.png' width='30' height='30'></a>";
													}

													//$porcentaje_prueba= 50;
													//$expansion_total_2 = 100; 

													$lectura2 = round((($porcentaje_prueba*100)/$expansion_total_2),2);
													

													?>
													<tr>
														<td><?php echo $fecha_hora; ?></td>														
														<td><?php echo $expansion_total_2; ?></td>
														<td><?php echo $expansion_total_3; ?></td>
														<td><?php echo $porcentaje_prueba; ?></td>
														<td>
															<?php echo $estado_prueba; ?>
															<?php echo $link; ?>
														</td>
													</tr>

													<?php
												}
											}
											?>
										</tbody>								
									</table>
								</div>
							</div>
						</div>
					</article>
					<!--WIDGET PARA EL INICIO Y APLICACIÓN DE LA PRUEBA PEV-->
					<article class="col-md-5 col-md-offset-0">
						<div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
							<input type="hidden" name="id_has_movimiento_cilindro_pev" id="id_has_movimiento_cilindro_pev" value="<?php echo $id_has_movimiento_cilindro_pev; ?>">
							<input type="hidden" name="idUser" id="idUser" value="<?php echo $idUser; ?>">
							<input type="hidden" name="num_cili" id="num_cili" value="<?php echo $num_cili; ?>">
							<input type="hidden" name="fecha_hora" id="fecha_hora" value="<?php echo $fecha_hora; ?>">
							<header>								
								<span class="widget-icon"><i class="fa fa-check"></i></span>
								<h2>MÓDULO PRUEBA PEV</h2>
							</header>
							<div>
								<div class="widget-body no-padding">
									
									<div class="well" id="paso_1">
										<div class="row" align="center">
											<table>
												<tr>
													<td colspan="2" align="justify" style="padding: 15px;"><p style="font-size: 25px">Efectúe la medida de Expansión Total cuando la presión alcance <?php echo $pre_pru_pev;?> Psi. Inicie el contador cuando el valor en la balanza esté estable, recuerde que debe esperar 30 segundos para el registro de la lectura.</p></td>
												</tr>
												<tr>			
													<td align="center" style="padding: 15px;"><button class="btn btn-primary" id="boton_prueba" onclick="updateReloj_2()" style="width: 180px; height: 180px;"><p style="font-size: 25px;" align="center">INICIAR<br>CONTADOR</p></button></td>
													<td width="200" align="center"><p id='CuentaAtras_2' style="font-size: 70px;">0</p></td>
												</tr>
											</table>										
										</div>								
									</div>
									<div class="well" id="div_1" align="center" style="display: none;"></div>
									<div class="well" id="div_2" align="center" style="display: none;"></div>
									<div class="well" id="div_3" align="center" style="display: none;"></div>			
									<div class="well" id="div_4" align="center" style="display: none;"></div>
									<div class="well" id="div_5" align="center" style="display: none;"></div>
									<div class="well" id="div_6" align="center" style="display: none;"></div>
									<div class="well" id="cargando" align="center" style="display: none;"></div>									
								</div>
							</div>
						</div>
					</article>
				</div>				
			</section>			
		</div>		
	</div>

	<script type="text/javascript"> 
		var totalTiempo=30; 
		function updateReloj()
		{
		    document.getElementById('CuentaAtras').innerHTML = totalTiempo; 
		    if(totalTiempo == 0)
		    {
		    	$("#paso_1").hide();
		        $("#div_1").show();
				$("#div_1").load("exp_toma_1.php");
				totalTiempo=30;
		    }
		    else
		    {            
		        totalTiempo-=1;
		        setTimeout("updateReloj()",1000);
		    }
		}
	</script>
	<script type="text/javascript"> 
		var totalTiempo=30; 
		function updateReloj_2()
		{
		    document.getElementById('CuentaAtras_2').innerHTML = totalTiempo; 
		    if(totalTiempo == 0)
		    {
		    	$("#paso_1").hide();
		        $("#div_1").show();
				$("#div_1").load("exp_toma_3.php");
				totalTiempo=30;
		    }
		    else
		    {            
		        totalTiempo-=1;
		        setTimeout("updateReloj_2()",1000);
		    }
		}
	</script>
	<script type="text/javascript"> 
		var totalTiempo=30; 
		function updateReloj_3()
		{
		    document.getElementById('CuentaAtras_3').innerHTML = totalTiempo; 
		    if(totalTiempo == 0)
		    {
		    	$("#div_2").hide();
		        $("#div_3").show();
				$("#div_3").load("exp_toma_5.php");
				totalTiempo=30;
		    }
		    else
		    {            
		        totalTiempo-=1;
		        setTimeout("updateReloj_3()",1000);
		    }
		}
	</script>
	<script type="text/javascript">
		//Función que toma el dato de expansión total UNO ingresado
		function continuar_1()
		{
			//var expansion_total_1 = document.getElementById("expansion_total_1").value;
			if(expansion_total_1 != "")
			{
				$("#div_1").hide();
				$("#cargando").hide();
				$("#div_2").show();
				$("#div_2").load("exp_toma_2.php");
			}
			else
			{
				$("#cargando").show();
				$("#cargando").html("<h1><font color='red'><strong>Debe ingresar el valor medido.</strong></font></h1>");		
			}
		}
	</script>
	<script type="text/javascript">
		function abortar_1()
		{
			var confirmar = confirm("¿Está seguro de abortar la operación?");
			if(confirmar == true)
			{
				$("#div_1").hide();
				$("#cargando").hide();
				$("#paso_1").show();
			}
		}
	</script>
	<script type="text/javascript">
		//Función que toma el dato de expansión total UNO ingresado
		function continuar_2()
		{
			var expansion_total_2 = document.getElementById("expansion_total_2").value;
			if(expansion_total_2 != "")
			{
				$("#div_1").hide();
				$("#cargando").hide();
				$("#div_2").show();
				$("#div_2").load("exp_toma_4.php");
			}
			else
			{
				$("#cargando").show();
				$("#cargando").html("<h1><font color='red'><strong>Debe ingresar el valor medido.</strong></font></h1>");		
			}
		}
	</script>
	<script type="text/javascript">
		function abortar_2()
		{
			var confirmar = confirm("¿Está seguro de abortar la operación?");
			if(confirmar == true)
			{
				$("#div_3").hide();
				$("#cargando").hide();
				$("#paso_1").show();
			}
		}
	</script>
	<script type="text/javascript">
		//Función que toma el dato de expansión Permanente ingresado
		function continuar_3()
		{
			//var expansion_total_1 = document.getElementById("expansion_total_1").value;
			var expansion_total_2 = document.getElementById("expansion_total_2").value;
			if(expansion_total_2<0){
				expansion_total_2 = expansion_total_2*-1;
			}
			var expansion_total_3 = document.getElementById("expansion_total_3").value;
			if(expansion_total_3<0){
				expansion_total_3 = expansion_total_3*-1;
			}
			var data = expansion_total_2 + "," + expansion_total_3;
			if(expansion_total_3 != "")
			{
				$("#div_3").hide();
				$("#cargando").hide();
				$("#div_4").show();
				$("#div_4").load("exp_toma_6.php", {data:data});
			}
			else
			{
				$("#cargando").show();
				$("#cargando").html("<h1><font color='red'><strong>Debe ingresar el valor medido.</strong></font></h1>");		
			}
		}
	</script>
	<script type="text/javascript">
		function abortar_3()
		{
			var confirmar = confirm("¿Está seguro de abortar la operación?");
			if(confirmar == true)
			{
				$("#div_5").hide();
				$("#cargando").hide();
				$("#paso_1").show();
			}
		}
	</script>
	<script type="text/javascript">
		//Funcíón que guarda los datos de la prueba en base de datos
		function guardar_prueba()
		{
			//var expansion_total_1 = $('#expansion_total_1').val();
			var expansion_total_2 = $('#expansion_total_2').val();
			var expansion_total_3 = $('#expansion_total_3').val();
			var porcentaje_prueba = $('#porcentaje_prueba').val();
			var idUser = $('#idUser').val();
			var id_has_movimiento_cilindro_pev = $('#id_has_movimiento_cilindro_pev').val();
			var fecha_hora = $('#fecha_hora').val();	
			var data = id_has_movimiento_cilindro_pev + "-" + expansion_total_2 + "-" + expansion_total_3 + "-" + porcentaje_prueba + "-" + idUser;

			var confirmar = confirm("¿Se encuentra seguro de guardar los datos!?");
			if(confirmar == true)
			{
				$.ajax({
                    type: 'POST',
                    url: 'prueba_pev/guardar_prueba.php',
                    data: { data },
                    
                    success: function(result_6){
                        if(result_6 == 1)
                        {
                        	alert("Los datos de la prueba han sido guardados con éxito.");
                        	$("#div_4").hide();
                            $("#paso_1").show();
                            window.location=("prueba_expansion.php?id_has_movimiento_cilindro_pev=<?php echo $id_has_movimiento_cilindro_pev; ?>&num_cili=<?php echo $num_cili; ?>");
                        }
                        else if(result_6 == 2)
                        {
                        	alert("Ha ocurrido un error. Consulte al administrador del sistema.");
                        	$("#div_4").hide();
                            $("#paso_1").show();
                        	//$("#cargando").show();
                            //$("#cargando").html("<h1><font color='red'><strong>Ha ocurrido un error</strong></font></h1>");
                        }                                                      
                    },
                    error: function(){
                        window.alert("Ha ocurrido un error. Intente nuevamente.");
                    }
                });
			}
			else
			{
				alert("La operación ha sido cancelada.")
			}						
		}
	</script>
	<script type="text/javascript">
		//Funcíón que guarda los datos de la prueba en base de datos
		function cancelar()
		{
			var confirmar = confirm("¿Se encuentra seguro de cancelar la operación?");
			if(confirmar == true)
			{				
				$("#div_1").hide();
				$("#div_2").hide();
				$("#div_3").hide();
				$("#div_4").hide();
				$("#cargando").hide();
				$("#paso_1").show();
			}
		}
	</script>
	<?php
	include("inc/footer.php");
	include("inc/scripts.php");
}
else
{
	header("location:index.php");
}
?>