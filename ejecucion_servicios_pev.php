<?php
session_start();
date_default_timezone_set("America/Bogota");
require ("libraries/conexion.php");
require ("libraries/seguimiento_pev.php");
$hora= date ("h:i:s");
$count_servicios = 0;
if($_SESSION['logged']== 'yes')
{ 
	require_once("inc/init.php");
	require_once("inc/config.ui.php");
	$page_title = "Ejecucion Servicios";
	$page_css[] = "your_style.css";
	include("inc/header.php");
	$page_nav[""][""][""][""] = true;
	include("inc/nav.php");

	//Se solicitan datos del transporte
  	$id_orden_pev = isset($_REQUEST['id_orden_pev']) ? $_REQUEST['id_orden_pev'] : NULL;
  	//$orden_pev = isset($_REQUEST['orden_pev']) ? $_REQUEST['orden_pev'] : NULL;
    $id_user =$_SESSION['su'];
    $acc = $_SESSION['acc'];
    $opciones[] = '';

    if (strlen($id_orden_pev) > 0)
    {	
    	$consulta3 = "SELECT * FROM orden_servicio_pev WHERE id_orden_pev = $id_orden_pev";
    	$resultado3 = mysqli_query($con,$consulta3);
    	$linea3 = mysqli_fetch_assoc($resultado3);

    	$id_transporte_pev = isset($linea3["id_transporte_pev"]) ? $linea3["id_transporte_pev"] : NULL;

    	$consulta4 = "SELECT * FROM has_movimiento_cilindro_pev WHERE id_transporte_pev = '$id_transporte_pev'";
    	$resultado4 = mysqli_query($con,$consulta4);
    	$linea4 = mysqli_fetch_assoc($resultado4);

    	$num_cili = isset($linea4["num_cili"]) ? $linea4["num_cili"] : NULL;
    	$id_tipo_gas_pev = isset($linea4["id_tipo_gas_pev"]) ? $linea4["id_tipo_gas_pev"] : NULL;
    	$id_cliente = isset($linea4["id_cliente"]) ? $linea4["id_cliente"] : NULL;
    	$ph_u = isset($linea4["ph_u"]) ? $linea4["ph_u"] : NULL;
    	$llenado_u = isset($linea4["llenado_u"]) ? $linea4["llenado_u"] : NULL;
    	$pintura_u = isset($linea4["pintura_u"]) ? $linea4["pintura_u"] : NULL;
    	$valvula_u = isset($linea4["valvula_u"]) ? $linea4["valvula_u"] : NULL;
    	$granallado_u = isset($linea4["granallado_u"]) ? $linea4["granallado_u"] : NULL;
    	$cambio_serv_u = isset($linea4["cambio_serv_u"]) ? $linea4["cambio_serv_u"] : NULL;
    	$lavado_especial_u = isset($linea4["lavado_especial_u"]) ? $linea4["lavado_especial_u"] : NULL;
    	$material_cilindro_u = isset($linea4["material_cilindro_u"]) ? $linea4["material_cilindro_u"] : NULL;

    	
    }
    

?>
<div id="main" role="main">
	<div id="content">
		<section id="widget-grid" class="">
			<div class="row">
				<article class="col-sm-14 col-md-14 col-lg-10">
					<div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h6>Cilindros que pertenecen a la orden </h6>				
						</header>
						<div>
						
						<input type="hidden" name="id_transporte_pev" value="<?php echo isset($_REQUEST['id_transporte_pev']) ? $_REQUEST['id_transporte_pev'] : NULL; ?>">
						<input type="hidden" name="orden_pev" value="<?php echo isset($_REQUEST['orden_pev']) ? $_REQUEST['orden_pev'] : NULL; ?>">
							<fieldset>
								<legend>Empresa: <?php echo $nombre; ?> | CME: <?php echo $num_trans; ?> | Fecha Transporte:<?php echo $fecha; ?></legend>
								<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">

										<thead>
											<tr>
												<th>#</th>
					                            <th>Cilindro Serial</th>
					                            <th>Material del cilindro</th>
					                            <th>Tipo de Gas</th>
					                            <th>PEV-IVA</th>
					                            <th>Llenado</th>
					                            <th>Pintura</th>
					                            <th>Cambio Válvula</th>
					                            <th>Granallado</th>
					                            <th>Cambio Servicio</th>
					                            <th>Lavado Especial</th>
											</tr>
										</thead>
										<tbody>													
										  <?php
				                          $contador = "0";	                
				                          $consulta1 = "SELECT * FROM has_movimiento_cilindro_pev WHERE id_transporte_pev = '".$id_transporte_pev."'";
				                          $resultado1 = mysqli_query($con,$consulta1);
				                          while ($linea1 = mysqli_fetch_array($resultado1))
				                          {
				                            $contador = $contador + 1;
				                            $id_has_movimiento_cilindro_pev = $linea1["id_has_movimiento_cilindro_pev"];
				                            $id_movimiento_pev = $linea1["id_movimiento_pev"];
				                            $num_cili = $linea1["num_cili"];
				                            $num_cili2 = $linea1["num_cili2"];
				                            $material_cilindro_u = $linea1["material_cilindro_u"];

				                            $id_tapa = $linea1["id_tapa"];
				                            $id_tipo_gas_pev = $linea1["id_tipo_gas_pev"];
				                            $id_cliente = $linea1["id_cliente"];
				                            $observacion_u = $linea1["observacion_u"];
				                            $ph_u = $linea1['ph_u'] ;
				                            $llenado_u = $linea1['llenado_u'];
				                            $pintura_u = $linea1['pintura_u'];
				                            $valvula_u = $linea1['valvula_u'];
				                            $granallado_u = $linea1['granallado_u'];
				                            $cambio_serv_u = $linea1['cambio_serv_u'];
				                            $lavado_especial_u = $linea1['lavado_especial_u'];

				                            $consulta2 = "SELECT nombre FROM tipo_gas_pev WHERE id_tipo_gas_pev = '".$id_tipo_gas_pev."'";
				                            $resultado2 = mysqli_query($con,$consulta2) ;
				                            if(mysqli_num_rows($resultado2) > 0)
				                            {
				                            	$linea2 = mysqli_fetch_array($resultado2);
				                            	$nombre_gas = $linea2["nombre"];
				                            }

				                            $consulta10 = "SELECT id_cilindro_eto, id_estado FROM cilindro_eto WHERE num_cili_eto = '$num_cili'";
				                            $resultado10 = mysqli_query($con,$consulta10);
				                            $linea10 = mysqli_fetch_assoc($resultado10);
 
				                            $id_cilindro_eto = isset($linea10["id_cilindro_eto"]) ? $linea10["id_cilindro_eto"] : NULL;
				                            $id_estado = isset($linea10["id_estado"]) ? $linea10["id_estado"] : NULL;

				                            $consulta12 = "SELECT * FROM cilindro_pev WHERE num_cili_pev = '$num_cili'";
				                            $resultado12 = mysqli_query($con,$consulta12);

				                            $linea12 = mysqli_fetch_assoc($resultado12);

				                            $id_estado = isset($linea12["id_estado"]) ? $linea12["id_estado"] : NULL;
				                            

				                            ?>
				                            <tr>
				                              	<td width="10"><?php echo $contador; ?></td>
				                              	<td width="30"><?php echo $num_cili; ?></td>
				                              	<td width="10">
				                              		<?php

														$consulta7 = "SELECT * FROM material_cilindro WHERE id_material = '$material_cilindro_u'";
							                            $resultado7 = mysqli_query($con,$consulta7) ;		
														
														
														
														$linea7 = mysqli_fetch_array($resultado7);
					                                    
					                                   	$id_material = $linea7['id_material'];
					                                   	$nombre_material = $linea7['nombre'];
					                                    
					                                    echo $nombre_material;
					                                    
													?>
				                              	</td>
				                              	<td><?php echo $nombre_gas; ?></td>
				                              	<?php

				                              	//--------------------------PEV----------------------------------------
				                              		if($ph_u != 0){

				                              			$habilitacion = 0;

				                              			if($id_material!=3){

				                              			if(($id_estado==1)||($id_estado == 2)||($id_estado == 10)||($id_estado == 3)){
				                              				?>
				                              				<td align="center" width="40">
				                              					NO DISPONIBLE PARA EJECUTAR
				                              				</td>
				                              				<?php
				                              			}else if($id_estado == 15){
				                              				?>
				                              				<td align="center" width="40">
				                              					CILINDRO CONDENADO
				                              				</td>
				                              				<?php
				                              			}else if($id_estado == 14){
				                              				?>
				                              				<td align="center" width="40">
				                              					CILINDRO RECHAZADO TEMPORALMENTE
				                              				</td>
				                              				<?php
				                              			}else{
				                              				$consulta5 = "SELECT * FROM datos_prueba_pev WHERE id_has_movimiento_cilindro_pev = $id_has_movimiento_cilindro_pev";
				                              				$resultado5 = mysqli_query($con,$consulta5);

				                              				if(mysqli_num_rows($resultado5)){
				                              					$habilitacion = 1;
				                              					if($id_estado == 15){
					                              					?>
					                              					<td align="center" width="40">
						                              					<p>EJECUTADO</p>
						                              					<p>(NO APROBADO)</p>
						                              				</td>
					                              					<?php
				                              					}else{
				                              						?>
				                              						<td align="center" width="40">
						                              					<p>EJECUTADO</p>
						                              					<p>(APROBADO)</p>
						                              				</td>
					                              					<?php
				                              					}
				                              				}else{
				                              					?>
				                              					<td align="center" width="40">
						                              				PENDIENTE EJECUCIÓN
						                              			</td>
					                              				<?php
				                              				}
				                              			}
				                              			}else{
				                              				$habilitacion = 1;
				                              				?>
				                              				<td align="center" width="40">
						                              			-
						                              		</td>
					                              			<?php
				                              			}	
				                              		}else{
				                              			$habilitacion = 1;
				                              			?>
				                              			<td align="center" width="40">
						                              		-
						                              	</td>
					                              		<?php
				                              		}


				                              	//--------------------------LLENADO------------------------------------
				                              		if($llenado_u == 1){
				                              			
				                            
				                              			if(($id_estado==1)||($id_estado == 10)){
				                              				?>
				                              				<td align="center" width="40">
				                              					NO DISPONIBLE PARA EJECUTAR
				                              				</td>
				                              				<?php
				                              			}else if($id_estado == 15){
				                              				?>
				                              				<td align="center" width="40">
				                              					CILINDRO CONDENADO
				                              				</td>
				                              				<?php
				                              			}else if($id_estado == 14){
				                              				?>
				                              				<td align="center" width="40">
				                              					CILINDRO RECHAZADO TEMPORALMENTE
				                              				</td>
				                              				<?php
				                              			}else{
				                              				if($habilitacion == 1){
					                              				?>
					                              				<td align="center" width="140">
					                              					<form id="form_llenado" name="form_llenado" method="POST" action="ejecucion_servicio_pev.php">
					                              						<input type="hidden" name="movimiento_cilindro" value="<?php echo $id_has_movimiento_cilindro_pev ?>">
					                              						<input type="hidden" name="tipo_servicio" value="llenado_u">
					                              						<input type="submit" name="Ejecucion" value="EJECUTAR">
					                              					</form>
					                              				</td>
					                              				<?php
				                              				}else{
				                              					?>
				                              					<td align="center" width="140">
					                              					SIN EJECUTAR
					                              				</td>
					                              				<?php
				                              				}
				                              			}

				                              		}else if($llenado_u == 2){
				                              			?>
				                              			<td align="center" width="40">
				                              				SERVICIO EJECUTADO
				                              			</td>
				                              			<?php
				                              		}else{
				                              			?>
				                              			<td align="center" width="40">
				                              				
				                              			</td>
				                              			<?php
				                              		}


				                              		//------------------------------------PINTURA---------------------------------------

				                              		if($pintura_u == 1){
				                              			
				                            
				                              			if(($id_estado==1)||($id_estado == 10)){
				                              				?>
				                              				<td align="center" width="40">
				                              					NO DISPONIBLE PARA EJECUTAR
				                              				</td>
				                              				<?php
				                              			}else if($id_estado == 15){
				                              				?>
				                              				<td align="center" width="40">
				                              					CILINDRO CONDENADO
				                              				</td>
				                              				<?php
				                              			}else if($id_estado == 14){
				                              				?>
				                              				<td align="center" width="40">
				                              					CILINDRO RECHAZADO TEMPORALMENTE
				                              				</td>
				                              				<?php
				                              			}else{
				                              				if($habilitacion == 1){
					                              				?>
					                              				<td align="center" width="140">
					                              					<form id="form_pintura" name="form_pintura" method="POST" action="ejecucion_servicio_pev.php">
					                              						<input type="hidden" name="movimiento_cilindro" value="<?php echo $id_has_movimiento_cilindro_pev ?>">
					                              						<input type="hidden" name="tipo_servicio" value="pintura_u">
					                              						<input type="submit" name="Ejecucion" value="EJECUTAR">
					                              					</form>
					                              				</td>
					                              				<?php
				                              				}else{
				                              					?>
				                              					<td align="center" width="140">
					                              					SIN EJECUTAR
					                              				</td>
					                              				<?php
				                              				}
				                              			}

				                              		}else if($pintura_u == 2){
				                              			?>
				                              			<td align="center" width="40">
				                              				SERVICIO EJECUTADO
				                              			</td>
				                              			<?php
				                              		}else{
				                              			?>
				                              			<td align="center" width="40">
				                              				
				                              			</td>
				                              			<?php
				                              		}


				                              		//---------------------------------CAMBIO VALVULA----------------------------------


				                              		if($valvula_u == 1){
				                              			
				                            
				                              			if(($id_estado==1)||($id_estado == 10)){
				                              				?>
				                              				<td align="center" width="40">
				                              					<form id="form_valvula" name="form_valvula" method="POST" action="ejecucion_servicio_pev.php">
    				                              						<input type="hidden" name="movimiento_cilindro" value="<?php echo $id_has_movimiento_cilindro_pev ?>">
					                              						<input type="hidden" name="tipo_servicio" value="valvula_u">
					                              						<input type="submit" name="Ejecucion" value="EJECUTAR">
					                              					</form>
				                              				</td>
				                              				<?php
				                              			}else if($id_estado == 15){
				                              				?>
				                              				<td align="center" width="40">
				                              					CILINDRO CONDENADO
				                              				</td>
				                              				<?php
				                              			}else if($id_estado == 14){
				                              				?>
				                              				<td align="center" width="40">
				                              					CILINDRO RECHAZADO TEMPORALMENTE
				                              				</td>
				                              				<?php
				                              			}else{
				                              				if($habilitacion == 1){
					                              				?>
					                              				<td align="center" width="140">
					                              					<form id="form_valvula" name="form_valvula" method="POST" action="ejecucion_servicio_pev.php">
					                              						<input type="hidden" name="movimiento_cilindro" value="<?php echo $id_has_movimiento_cilindro_pev ?>">
					                              						<input type="hidden" name="tipo_servicio" value="valvula_u">
					                              						<input type="submit" name="Ejecucion" value="EJECUTAR">
					                              					</form>
					                              				</td>
					                              				<?php
				                              				}else{
				                              					?>
				                              					<td align="center" width="140">
					                              					SIN EJECUTAR
					                              				</td>
					                              				<?php
				                              				}
				                              			}

				                              		}else if($valvula_u == 2){
				                              			?>
				                              			<td align="center" width="40">
				                              				SERVICIO EJECUTADO
				                              			</td>
				                              			<?php
				                              		}else{
				                              			?>
				                              			<td align="center" width="40">
				                              				
				                              			</td>
				                              			<?php
				                              		}


				                              		//-----------------------------GRANALLADO......................................

				                              		if($granallado_u == 1){
				                              			
				                            
				                              			if(($id_estado==1)||($id_estado == 2)||($id_estado == 10)||($id_estado == 3)){
				                              				?>
				                              				<td align="center" width="40">
				                              					NO DISPONIBLE PARA EJECUTAR
				                              				</td>
				                              				<?php
				                              			}else if($id_estado == 15){
				                              				?>
				                              				<td align="center" width="40">
				                              					CILINDRO CONDENADO
				                              				</td>
				                              				<?php
				                              			}else if($id_estado == 14){
				                              				?>
				                              				<td align="center" width="40">
				                              					CILINDRO RECHAZADO TEMPORALMENTE
				                              				</td>
				                              				<?php
				                              			}else{
				                              				if($habilitacion == 1){
					                              				?>
					                              				<td align="center" width="140">
					                              					<form id="form_granallado" name="form_granallado" method="POST" action="ejecucion_servicio_pev.php">
					                              						<input type="hidden" name="movimiento_cilindro" value="<?php echo $id_has_movimiento_cilindro_pev ?>">
					                              						<input type="hidden" name="tipo_servicio" value="granallado_u">
					                              						<input type="submit" name="Ejecucion" value="EJECUTAR">
					                              					</form>
					                              				</td>
					                              				<?php
				                              				}else{
				                              					?>
				                              					<td align="center" width="140">
					                              					SIN EJECUTAR
					                              				</td>
					                              				<?php
				                              				}
				                              			}

				                              		}else if($granallado_u == 2){
				                              			?>
				                              			<td align="center" width="40">
				                              				SERVICIO EJECUTADO
				                              			</td>
				                              			<?php
				                              		}else{
				                              			?>
				                              			<td align="center" width="40">
				                              				
				                              			</td>
				                              			<?php
				                              		}


				                              		//-------------------------------CAMBIO SERVCIO-----------------------------------

				                              		if($cambio_serv_u == 1){
				                              			
				                            
				                              			if(($id_estado==1)||($id_estado == 2)||($id_estado == 10)||($id_estado == 3)){
				                              				?>
				                              				<td align="center" width="40">
				                              					NO DISPONIBLE PARA EJECUTAR
				                              				</td>
				                              				<?php
				                              			}else if($id_estado == 15){
				                              				?>
				                              				<td align="center" width="40">
				                              					CILINDRO CONDENADO
				                              				</td>
				                              				<?php
				                              			}else if($id_estado == 14){
				                              				?>
				                              				<td align="center" width="40">
				                              					CILINDRO RECHAZADO TEMPORALMENTE
				                              				</td>
				                              				<?php
				                              			}else{
				                              				if($habilitacion == 1){
					                              				?>
					                              				<td align="center" width="140">
					                              					<form id="form_cambio_serv" name="form_cambio_serv" method="POST" action="ejecucion_servicio_pev.php">
					                              						<input type="hidden" name="movimiento_cilindro" value="<?php echo $id_has_movimiento_cilindro_pev ?>">
					                              						<input type="hidden" name="tipo_servicio" value="cambio_serv_u">
					                              						<input type="submit" name="Ejecucion" value="EJECUTAR">
					                              					</form>
					                              				</td>
					                              				<?php
				                              				}else{
				                              					?>
				                              					<td align="center" width="140">
					                              					SIN EJECUTAR
					                              				</td>
					                              				<?php
				                              				}
				                              			}

				                              		}else if($cambio_serv_u == 2){
				                              			?>
				                              			<td align="center" width="40">
				                              				SERVICIO EJECUTADO
				                              			</td>
				                              			<?php
				                              		}else{
				                              			?>
				                              			<td align="center" width="40">
				                              				
				                              			</td>
				                              			<?php
				                              		}


				                              		//---------------------------------LAVADO ESPECIAL-----------------------------------

				                              		if($lavado_especial_u == 1){
				                              			
				                            
				                              			if(($id_estado==1)||($id_estado == 2)||($id_estado == 10)||($id_estado == 3)){
				                              				?>
				                              				<td align="center" width="40">
				                              					NO DISPONIBLE PARA EJECUTAR
				                              				</td>
				                              				<?php
				                              			}else if($id_estado == 15){
				                              				?>
				                              				<td align="center" width="40">
				                              					CILINDRO CONDENADO
				                              				</td>
				                              				<?php
				                              			}else if($id_estado == 14){
				                              				?>
				                              				<td align="center" width="40">
				                              					CILINDRO RECHAZADO TEMPORALMENTE
				                              				</td>
				                              				<?php
				                              			}else{
				                              				if($habilitacion == 1){
					                              				?>
					                              				<td align="center" width="140">
					                              					<form id="form_lavado" name="form_lavado" method="POST" action="ejecucion_servicio_pev.php">
					                              						<input type="hidden" name="movimiento_cilindro" value="<?php echo $id_has_movimiento_cilindro_pev ?>">
					                              						<input type="hidden" name="tipo_servicio" value="lavado_especial_u">
					                              						<input type="submit" name="Ejecucion" value="EJECUTAR">
					                              					</form>
					                              				</td>
					                              				<?php
				                              				}else{
				                              					?>
				                              					<td align="center" width="140">
					                              					SIN EJECUTAR
					                              				</td>
					                              				<?php
				                              				}
				                              			}

				                              		}else if($lavado_especial_u == 2){
				                              			?>
				                              			<td align="center" width="40">
				                              				SERVICIO EJECUTADO
				                              			</td>
				                              			<?php
				                              		}else{
				                              			?>
				                              			<td align="center" width="40">
				                              				
				                              			</td>
				                              			<?php
				                              		}


				                              	?>
				                           	</tr>                           
				                            <?php
				                            }mysqli_free_result($resultado1);	                                            
				                  		    ?>
										</tbody>
								</table>
							</fieldset>
							<footer>
								
							</footer>
						

						</div>
					</div>
				</article>
			</div>
		</section>
	</div>
</div>
<!-- <script type="text/javascript">
	function confirmar()
	{
		var id = document.getElementById("opc_1").value;
		//var numberChecked = $('input:checkbox:checked').length;
		alert("el valor es:"+id);
		/*if ($("#opc_1:checked").length)
		{       
		    
		} 
		else 
		{
		    if($("#opc_2:checked").length)
		    {
		    	
		    }
		    else
		    {
		    	if($("#opc_3:checked").length)
		    	{
		    		
		    	}
		    	else
		    	{
		    		if($("#opc_4:checked").length )
					{
						submit.form1();
					}
					else
					{
						alert("Debe seleccionar al menos un servicio para cada cilindro");
						return false;
					}
		    	}
		    }
		}*/
	}
</script> -->
<?php
	include("inc/footer.php");
	include("inc/scripts.php"); 
?>
<?php 
	//include footer
	include("inc/google-analytics.php"); 
?>
<?php
}
else
{
    header("Location:index.php");
}
?>
