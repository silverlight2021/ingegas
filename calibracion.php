<?php
date_default_timezone_set("America/Bogota");

require ("libraries/conexion.php");
session_start();
if(@$_SESSION['logged']== 'yes')
{ 
	$id_user=$_SESSION['su'];
    $acc = $_SESSION['acc'];
    $letra = isset($_REQUEST['letra']) ? $_REQUEST['letra'] : NULL;
    //$id_orden = isset($_REQUEST['id_orden']) ? $_REQUEST['id_orden'] : NULL;
    $id_calibracion = isset($_REQUEST['id_calibracion']) ? $_REQUEST['id_calibracion'] : NULL;
    $contador_orden = 0;

	require_once("inc/init.php");
	require_once("inc/config.ui.php");
	$page_title = "Calibración";
	$page_css[] = "your_style.css";
	include("inc/header.php");
	$page_nav[""][""][""][""] = true;
	include("inc/nav.php");	
?>
<?php
	$consulta1 = "SELECT Name, LastName FROM user WHERE idUser = '$id_user'";
	$resultado1 = mysqli_query($con,$consulta1);
	$linea1 = mysqli_fetch_assoc($resultado1);

	$nombre = isset($linea1["Name"]) ? $linea1["Name"] : NULL;
	$apellido = isset($linea1["LastName"]) ? $linea1["LastName"] : NULL;

	$fecha = date("Y-m-d");
	$hora = date("G:i:s",time());

	$consulta2 = "SELECT * FROM calibracion WHERE id_calibracion = $id_calibracion";
	$resultado2 = mysqli_query($con,$consulta2);
	if(mysqli_num_rows($resultado2)>0){
		$linea2 = mysqli_fetch_assoc($resultado2);

		$expancion1 = isset($linea2['expancion1']) ? $linea2['expancion1'] : NULL;
		$expancion2 = isset($linea2['expancion2']) ? $linea2['expancion2'] : NULL;
		$expancion3 = isset($linea2['expancion3']) ? $linea2['expancion3'] : NULL;
		$expancion4 = isset($linea2['expancion4']) ? $linea2['expancion4'] : NULL;
		$expancion5 = isset($linea2['expancion5']) ? $linea2['expancion5'] : NULL;
		$expancion6 = isset($linea2['expancion6']) ? $linea2['expancion6'] : NULL;
		$expancion7 = isset($linea2['expancion7']) ? $linea2['expancion7'] : NULL;
		$expancion8 = isset($linea2['expancion8']) ? $linea2['expancion8'] : NULL;
		$lectura_manometro_1 = isset($linea2['lectura_manometro_1']) ? $linea2['lectura_manometro_1'] : NULL;
		$lectura_manometro_2 = isset($linea2['lectura_manometro_2']) ? $linea2['lectura_manometro_2'] : NULL;
		$lectura_manometro_3 = isset($linea2['lectura_manometro_3']) ? $linea2['lectura_manometro_3'] : NULL;
		$lectura_manometro_4 = isset($linea2['lectura_manometro_4']) ? $linea2['lectura_manometro_4'] : NULL;
		$lectura_manometro_5 = isset($linea2['lectura_manometro_5']) ? $linea2['lectura_manometro_5'] : NULL;
		$lectura_manometro_6 = isset($linea2['lectura_manometro_6']) ? $linea2['lectura_manometro_6'] : NULL;
		$lectura_manometro_7 = isset($linea2['lectura_manometro_7']) ? $linea2['lectura_manometro_7'] : NULL;
		$lectura_manometro_8 = isset($linea2['lectura_manometro_8']) ? $linea2['lectura_manometro_8'] : NULL;
		$desviacion_1 = isset($linea2["desviacion_1"]) ? $linea2["desviacion_1"] : NULL;
		$desviacion_2 = isset($linea2["desviacion_2"]) ? $linea2["desviacion_2"] : NULL;
		$desviacion_3 = isset($linea2["desviacion_3"]) ? $linea2["desviacion_3"] : NULL;
		$desviacion_4 = isset($linea2["desviacion_4"]) ? $linea2["desviacion_4"] : NULL;
		$desviacion_5 = isset($linea2["desviacion_5"]) ? $linea2["desviacion_5"] : NULL;
		$desviacion_6 = isset($linea2["desviacion_6"]) ? $linea2["desviacion_6"] : NULL;
		$desviacion_7 = isset($linea2["desviacion_7"]) ? $linea2["desviacion_7"] : NULL;
		$desviacion_8 = isset($linea2["desviacion_8"]) ? $linea2["desviacion_8"] : NULL;
		$temp_camisa = isset($linea2["temp_camisa"]) ? $linea2["temp_camisa"] : NULL;
		$temp_cilindro = isset($linea2["temp_cilindro"]) ? $linea2["temp_cilindro"] : NULL;
		$diferencia_temp = isset($linea2["diferencia_temp"]) ? $linea2["diferencia_temp"] : NULL;
		$incertidumbre_total_metodo = isset($linea2["incertidumbre_total_metodo"]) ? $linea2["incertidumbre_total_metodo"] : NULL;
		$desviacion_metodo = isset($linea2["desviacion_metodo"]) ? $linea2["desviacion_metodo"] : NULL;

		$contador_orden++;
	}

	$consulta3 = "SELECT MAX(id_incertidumbre) AS id_incertidumbre FROM incertidumbre";
	$resultado3 = mysqli_query($con,$consulta3);
	$linea3 = mysqli_fetch_assoc($resultado3);

	$id_incertidumbre = isset($linea3['id_incertidumbre']) ? $linea3['id_incertidumbre'] : NULL;

	$consulta4 = "SELECT incertidumbre FROM incertidumbre WHERE id_incertidumbre = '$id_incertidumbre'";
	$resultado4 = mysqli_query($con,$consulta4);
	$linea4 = mysqli_fetch_assoc($resultado4);

	$incertidumbre = isset($linea4['incertidumbre']) ? $linea4['incertidumbre'] : NULL;

	$consulta5 = "SELECT MAX(id_incertidumbre_manometro) AS id_incertidumbre_manometro FROM incertidumbre_manometro";
	$resultado5 = mysqli_query($con,$consulta5);
	$linea5 = mysqli_fetch_assoc($resultado5);

	$id_incertidumbre_manometro = isset($linea5['id_incertidumbre_manometro']) ? $linea5['id_incertidumbre_manometro'] : NULL;

	$consulta6 = "SELECT incertidumbre_manometro FROM incertidumbre_manometro WHERE id_incertidumbre_manometro = '$id_incertidumbre_manometro'";
	$resultado6 = mysqli_query($con,$consulta6);
	$linea6 = mysqli_fetch_assoc($resultado6);

	$incertidumbre_manometro = isset($linea6['incertidumbre_manometro']) ? $linea6['incertidumbre_manometro'] : NULL;

?>
<!-- MAIN PANEL -->
<div id="main" role="main">
<?php
if (in_array(39, $acc))
{
?>
	<div id="content">
		<div class="" align="center">
			<h6  class="page-title txt-color-blueDark">VERIFICACIÓN DE LA EXACTITUD DEL SISTEMA DE PRUEBA</h6>			
		</div>
		
		<section id="widget-grid" class="">
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-12">					
					<!--<form method="POST" action="calibracion.php">-->
						<table  class="table table-striped table-bordered table-hover" width="100%">
							<thead>
								<tr>
									<th>Material</th>
									<th>Tamaño</th>
									<th>Fabricante</th>
									<th>Modelo No.</th>
									<th>Serie No.</th>
								</tr>
							</thead>	
							<tbody>
								<tr>
									<td>Acero</td>
									<td>9" D 51" L</td>
									<td>NORRIS</td>
									<td>140-234</td>
									<td>4870778Y</td>
								</tr>
							</tbody>
						</table>
					<!--</form>-->
				</article>
			</div>
		</section>
		
		<div class="" align="center">
			<h6  class="page-title txt-color-blueDark">DESVIACIÓN MAXIMA  PERMITIDA 1%</h6>			
		</div>

		<section id="widget-grid" class="">
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-6">
					<table class="table table-striped table-bordered table-hover" width="100%">
						<tbody>
							<tr>
								<td>TEMP. CAMISA (°C)</td>
								<td><input type="text" class="input" name="temp_camisa" id="temp_camisa" onchange="calcularTemp()" value="<?php echo isset($temp_camisa) ? $temp_camisa : NULL; ?>"></td>
							</tr>
							<tr>
								<td>TEMP. CILINDRO(°C)</td>
								<td><input type="text" class="input" id="temp_cilindro" name="temp_cilindro" onchange="calcularTemp()" value="<?php echo isset($temp_cilindro) ? $temp_cilindro : NULL; ?>"></td>
							</tr>
							<tr>
								<td>DIFERENCIA (°C)</td>
								<td><input type="text" class="input" id="diferencia_temp" name="diferencia_temp" value="<?php echo isset($diferencia_temp) ? $diferencia_temp : NULL; ?>"></td>
							</tr>
						</tbody>
					</table>
				</article>
				<article class="col-sm-12 col-md-12 col-lg-6">			
					<table  class="table table-striped table-bordered table-hover" width="100%">
						<tr>
							<div class="" align="center"><strong>FÓRMULA MATEMÁTICA PARA ESTIMAR INCERTIDUMBRE TOTAL DEL MÉTODO</strong></div>
						</tr>
						<tr>
							<td><div class="" align="center">U = &#8730; (Xa)^2 + (Xb)^2 + (Xc)^2 </div></td>
						</tr>						
					</table>
				</article>
			</div>
		</section>
		<!--<section id="widget-grid" class="">
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-6">
					<table class="table table-striped table-bordered table-hover" width="100%">
						<tbody>
							<tr>
								<td><strong>DESVIACIÓN LECTURA MANÓMETROS</strong></td>
								<td>
									<input type="text" class="input" onchange="digita_desviacion(this.value)">	
								</td>
							</tr>
						</tbody>
					</table>
				</article>
				<article class="col-sm-12 col-md-12 col-lg-6">
					<table class="table table-striped table-bordered table-hover" width="100%">
						<tbody>
							<tr>
								<td style="text-align: center;background-color: #aa0000;cursor: pointer;color: white" onclick="calcularDesviacionMetodo(), calcularTotal()"><strong>CALCULAR DESVIACIONES</strong></td>
							</tr>
						</tbody>
					</table>
				</article>
			</div>
		</section>-->		
		<section id="widget-grid" class="">
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-12">					
					<!--<form method="POST" action="calibracion.php">-->
						<table  class="table table-striped table-bordered table-hover" width="100%">
							<thead>
								<tr>
									<th>PRESIÓN (PSI)</th>
									<th>EXP. CIL.CAL (G)</th>
									<th>MANOMETRO DE REFERENCIA (PSI)</th>
									<th>LECTURA MANOMETRO</th>
									<th>DESVIACION (%)</th>
									<th>GUARDAR</th>
								</tr>
							</thead>	
							<tbody>
								<tr>
									<form id="form1" name="form1" method="post" action="guardarCalibracion.php">
										<td>0</td>
										<td>0,0</td>
										<td><input type="text" name="expa1" class="input" value="<?php echo isset($expancion1) ? $expancion1 : NULL; ?>" id="expa1" readonly></td>
										<td>
											<input type="text" name="desvia1" class="input" value="<?php echo isset($desviacion_1) ? $desviacion_1 : NULL; ?>" readonly id="desvia1">
											<?php
												if($contador_orden==0){
											?>
											<button type="button" name="tom_lectura" id="tom_lectura" class="btn btn-success" onclick="validar(1)">TOMAR LECTURA</button> 
											<?php
												}
											?>
										</td>
										<td>
											<input type="text" name="mano1" class="input" value="<?php echo isset($lectura_manometro_1) ? $lectura_manometro_1 : NULL; ?>" id="mano1"> 
											<input type="hidden" name="idPresion" value="1">
										</td>
										<td>
											<?php
												if($contador_orden==0){
											?>
												<input type="hidden" name="id_user" value="<?php echo $id_user; ?>">
												<input type="hidden" name="id_calibra" value="<?php echo $id_calibracion; ?>">
												<input type="submit" value="Guardar" name="guardar1" id="guardar1">
											<?php
												}
											?>
										</td>
									</form>
								</tr>
								<tr>
									<form id="form2" name="form2" method="post" action="guardarCalibracion.php">
										<td>2000</td>
										<td>86.5</td>
										<td><input type="text" class="input"  name="expa2" value="<?php echo isset($expancion2) ? $expancion2 : NULL; ?>" id="expa2" readonly></td>
										<td>
											<input type="text" class="input"  name="desvia2" readonly id="desvia2" value="<?php echo isset($desviacion_2) ? $desviacion_2 : NULL; ?>">
											<?php
												if(($expancion1!=NULL)&&($expancion2==NULL)){
											?>
											<button type="button" name="tom_lectura" id="tom_lectura" class="btn btn-success" onclick="validar(2)">TOMAR LECTURA</button>
											<?php
												}
											?>
										</td>
										<td><input type="text" class="input" name="mano2" value="<?php echo isset($lectura_manometro_2) ? $lectura_manometro_2 : NULL; ?>" id="mano2"></td>
										<td>
											<?php
												if(($expancion1!=NULL)&&($expancion2==NULL)){
											?>
											<input type="hidden" name="idPresion" value="2">
											<input type="hidden" name="id_calibra" value="<?php echo $id_calibracion; ?>">
											<input type="submit" value="Guardar" name="guardar2" id="guardar2">
											<?php
												}
											?>
										</td>
									</form>
								</tr>
								<tr>
									<form id="form3" name="form3" method="post" action="guardarCalibracion.php">
										<td>3000</td>
										<td>130,0</td>
										<td><input type="text" class="input" name="expa3" value="<?php echo isset($expancion3) ? $expancion3 : NULL; ?>" id="expa3" readonly></td>
										<td>
											<input type="text" class="input" name="desvia3" readonly id="desvia3" value="<?php echo isset($desviacion_3) ? $desviacion_3 : NULL; ?>">
											<?php
												if(($expancion2!=NULL)&&($expancion3==NULL)){
											?>
											<button type="button" name="tom_lectura" id="tom_lectura" class="btn btn-success" onclick="validar(3)">TOMAR LECTURA</button>
											<?php
												}
											?>
										</td>
										<td><input type="text" class="input" name="mano3" value="<?php echo isset($lectura_manometro_3) ? $lectura_manometro_3 : NULL; ?>" id="mano3" ></td>
										<td>
											<?php
												if(($expancion2!=NULL)&&($expancion3==NULL)){
											?>
											<input type="hidden" name="idPresion" value="3">
											<input type="hidden" name="id_calibra" value="<?php echo $id_calibracion; ?>">
											<input type="submit" value="Guardar" name="guardar3" id="guardar3">
											<?php
												}
											?>
										</td>
									</form>
								</tr>
								<tr>
									<form id="form4" name="form4" method="post" action="guardarCalibracion.php">
										<td>4000</td>
										<td>173,5</td>
										<td><input type="text" class="input" name="expa4" value="<?php echo isset($expancion4) ? $expancion4 : NULL; ?>" id="expa4" readonly></td>
										<td>
											<input type="text" class="input" name="desvia4" readonly id="desvia4" value="<?php echo isset($desviacion_4) ? $desviacion_4 : NULL; ?>">
											<?php
												if(($expancion3!=NULL)&&($expancion4==NULL)){
											?>
											<button type="button" name="tom_lectura" id="tom_lectura" class="btn btn-success" onclick="validar(4)">TOMAR LECTURA</button>
											<?php
												}
											?>
										</td>
										<td><input type="text" class="input" name="mano4" value="<?php echo isset($lectura_manometro_4) ? $lectura_manometro_4 : NULL; ?>" id="mano4" ></td>
										<td>
											<?php
												if(($expancion3!=NULL)&&($expancion4==NULL)){
											?>
											<input type="hidden" name="idPresion" value="4">
											<input type="hidden" name="id_calibra" value="<?php echo $id_calibracion; ?>">
											<input type="submit" value="Guardar" name="guardar4" id="guardar4">
											<?php
												}
											?>
										</td>
									</form>
								</tr>
								<tr>
									<form id="form5" name="form5" method="post" action="guardarCalibracion.php">
										<td>5000</td>
										<td>217,0</td>
										<td><input type="text" class="input" name="expa5" value="<?php echo isset($expancion5) ? $expancion5 : NULL; ?>" id="expa5" readonly></td>
										<td>
											<input type="text" class="input" name="desvia5" readonly id="desvia5" value="<?php echo isset($desviacion_5) ? $desviacion_5 : NULL; ?>">
											<?php
												if(($expancion4!=NULL)&&($expancion5==NULL)){
											?>
											<button type="button" name="tom_lectura" id="tom_lectura" class="btn btn-success" onclick="validar(5)">TOMAR LECTURA</button>
											<?php
												}
											?>
										</td>
										<td><input type="text" class="input" name="mano5" value="<?php echo isset($lectura_manometro_5) ? $lectura_manometro_5 : NULL; ?>" id="mano5" ></td>
										<td>
											<?php
												if(($expancion4!=NULL)&&($expancion5==NULL)){
											?>
											<input type="hidden" name="idPresion" value="5">
											<input type="hidden" name="id_calibra" value="<?php echo $id_calibracion; ?>">
											<input type="submit" value="Guardar" name="guardar5" id="guardar5">
											<?php
												}
											?>
										</td>
									</form>
								</tr>
								<tr>
									<form id="form6" name="form6" method="post" action="guardarCalibracion.php">
										<td>6000</td>
										<td>260,5</td>
										<td><input type="text" class="input" name="expa6" value="<?php echo isset($expancion6) ? $expancion6 : NULL; ?>" id="expa6" readonly></td>
										<td>
											<input type="text" class="input" name="desvia6" readonly id="desvia6" value="<?php echo isset($desviacion_6) ? $desviacion_6 : NULL; ?>">
											<?php
												if(($expancion5!=NULL)&&($expancion6==NULL)){
											?>
											<button type="button" name="tom_lectura" id="tom_lectura" class="btn btn-success" onclick="validar(6)">TOMAR LECTURA</button>
											<?php
												}
											?>
										</td>
										<td><input type="text" class="input" name="mano6" value="<?php echo isset($lectura_manometro_6) ? $lectura_manometro_6 : NULL; ?>" id="mano6" ></td>
										<td>
											<?php
												if(($expancion5!=NULL)&&($expancion6==NULL)){
											?>
											<input type="hidden" name="idPresion" value="6">
											<input type="hidden" name="id_calibra" value="<?php echo $id_calibracion; ?>">
											<input type="submit" value="Guardar" name="guardar6" id="guardar6">
											<?php
												}
											?>
										</td>
									</form>
								</tr>
								<tr>
									<form id="form7" name="form7" method="post" action="guardarCalibracion.php">
										<td>7000</td>
										<td>304,0</td>
										<td><input type="text" class="input" name="expa7" value="<?php echo isset($expancion7) ? $expancion7 : NULL; ?>" id="expa7" readonly></td>
										<td>
											<input type="text" class="input" name="desvia7" readonly id="desvia7" value="<?php echo isset($desviacion_7) ? $desviacion_7 : NULL; ?>">
											<?php
												if(($expancion6!=NULL)&&($expancion7==NULL)){
											?>
											<button type="button" name="tom_lectura" id="tom_lectura" class="btn btn-success" onclick="validar(7)">TOMAR LECTURA</button>
											<?php
												}
											?>
										</td>
										<td><input type="text" class="input" name="mano7" value="<?php echo isset($lectura_manometro_7) ? $lectura_manometro_7 : NULL; ?>" id="mano7" ></td>
										<td>
											<?php
												if(($expancion6!=NULL)&&($expancion7==NULL)){
											?>
											<input type="hidden" name="idPresion" value="7">
											<input type="hidden" name="id_calibra" value="<?php echo $id_calibracion; ?>">
											<input type="submit" value="Guardar" name="guardar7" id="guardar7">
											<?php
												}
											?>
										</td>
									</form>
								</tr>
								<tr>
									<form id="form8" name="form8" method="post" action="guardarCalibracion.php">
										<td>7500</td>
										<td>326,0</td>
										<td><input type="text" class="input" name="expa8" value="<?php echo isset($expancion8) ? $expancion8 : NULL; ?>" id="expa8" readonly></td>
										<td>
											<input type="text" class="input" name="desvia8" readonly id="desvia8" value="<?php echo isset($desviacion_8) ? $desviacion_8 : NULL; ?>">
											<?php
												if(($expancion7!=NULL)&&($expancion8==NULL)){
											?>
											<button type="button" name="tom_lectura" id="tom_lectura" class="btn btn-success" onclick="validar(8)">TOMAR LECTURA</button>
											<?php
												}
											?>
										</td>
										<td><input type="text" class="input" name="mano8" value="<?php echo isset($lectura_manometro_8) ? $lectura_manometro_8 : NULL; ?>" id="mano8" ></td>
										<td>
											<?php
												if(($expancion7!=NULL)&&($expancion8==NULL)){
											?>
											<input type="hidden" name="idPresion" value="8">
											<input type="hidden" name="id_calibra" value="<?php echo $id_calibracion; ?>">
											<input type="submit" value="Guardar" name="guardar8" id="guardar8">
											<?php
												}
											?>
										</td>
									</form>
									</tr>
							</tbody>
						</table>
					<!--</form>-->
				</article>
			</div>
		</section>
		<div>
			<p>DESVIACIÓN(%) = ((EXP.CIL.CAL(g) - EXPANSIÓN (g)) / EXP.CIL.CAL(g))*100</p>
			<p>Nota: Si el resultado es negativo se debe multiplicar por (-1) para que sea valida la operación</p>
			<p>DESVIACIÓN DEL MÉTODO: (SUMATORÍA DE DESVIACIÓN)/N. TOTAL DE ENSAYOS</p>
		</div>
		<form id="form9" name="form9" method="post" action="guardarCalibracion.php">
		
		<section id="widget-grid" class="">
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-6">
					<table class="table table-striped table-bordered table-hover" width="100%">
						<tbody>
							<tr>
								<td>TEMP. CAMISA (°C)</td>
								<td><input type="text" class="input" name="temp_camisa" id="temp_camisa" onchange="calcularTemp()" value="<?php echo isset($temp_camisa) ? $temp_camisa : NULL; ?>"></td>
							</tr>
							<tr>
								<td>TEMP. CILINDRO(°C)</td>
								<td><input type="text" class="input" id="temp_cilindro" name="temp_cilindro" onchange="calcularTemp()" value="<?php echo isset($temp_cilindro) ? $temp_cilindro : NULL; ?>"></td>
							</tr>
							<tr>
								<td>DIFERENCIA (°C)</td>
								<td><input type="text" class="input" id="diferencia_temp" name="diferencia_temp" value="<?php echo isset($diferencia_temp) ? $diferencia_temp : NULL; ?>"></td>
							</tr>
						</tbody>
					</table>
				</article>
				<article class="col-sm-12 col-md-12 col-lg-6">			
					<table  class="table table-striped table-bordered table-hover" width="100%">
						<tr>
							<div class="" align="center"><strong>FÓRMULA MATEMÁTICA PARA ESTIMAR INCERTIDUMBRE TOTAL DEL MÉTODO</strong></div>
						</tr>
						<tr>
							<td><div class="" align="center">U = &#8730; (Xa)^2 + (Xb)^2 + (Xc)^2 </div></td>
						</tr>						
					</table>
				</article>
			</div>
		</section>
		
		<section id="widget-grid" class="">
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-6">			
					<table class="table table-striped table-bordered table-hover" width="100%">
						<tr>
							<div class="" align="center"><strong>Incertidumbre de las fuentes identificadas</strong></div>
						</tr>
						<tr>
							<td>a) MANÓNEMETRO (psi)</td>
							<td><input type="text" class="input" readonly name="incertidumbre_manometro" id="incertidumbre_manometro" value="<?php echo isset($incertidumbre_manometro) ? $incertidumbre_manometro : NULL; ?>"></td>
						</tr>
						<tr>
							<td>b) BÁSCULA (GR)</td>
							<td>
								<input type="text" class="input" readonly name="incertidumbre_bascula" id="incertidumbre_bascula" value="<?php echo isset($incertidumbre) ? $incertidumbre : NULL; ?>">
							</td>
						</tr>
						<tr>
							<td>c) DESVIACIÓN DEL MÉTODO</td>
							<td><input type="text" class="input" name="desviacionT" id="desviacionT" onclick="calcularDesviacionMetodo()" value="<?php echo isset($desviacion_metodo) ? $desviacion_metodo : NULL; ?>"></td>
						</tr>
					</table>
				</article>	
				<article class="col-sm-12 col-md-12 col-lg-6">
					<table class="table table-striped table-bordered table-hover" width="100%">
						<thead>
							<th><p align="center">INCERTIDUMBRE DEL MÉTODO (U)</p></th>
						</thead>
						<tbody>
							<tr>
								<td align="center"><input type="text" class="input" name="incertidumbre_total" id="incertidumbre_total" onclick="calcularTotal()" value="<?php echo isset($incertidumbre_total_metodo) ? $incertidumbre_total_metodo : NULL; ?>"></td>
							</tr>
						</tbody>
					</table>
				</article>
			</div>
		</section>
		<div>
			<p align="center"><strong>TENIENDO EN CUENTA LOS VALORES OBTENIDOS AL FINALIZAR LA VALIDACIÓN DEL MÉTODO, INDIQUE SI EL SISTEMA CUMPLE CON LA EXACTITUD REQUERIDA: </strong></p>
			<p align="center"><strong>SI: <input type="radio"> NO: <input type="radio"></strong></p>
		</div>
		<div>
			<p align="center"><strong>APRUEBA: ___________________________      REVISADO: ___________________________</strong></p>
		</div>
		</br>
		</br>
		</br>		
		<section id="widget-grid" class="">
			<div class="row">						
				<article class="col-sm-12 col-md-12 col-lg-6">
					<table  class="table table-striped table-bordered table-hover" width="100%">
						<tr>
							<td><strong>OBSERVACIONES</strong></td>
						</tr>
						<tr>
							<td><textarea class="form-control" rows="5" name="observaciones" id="observaciones"></textarea></td> 
						</tr>
					</table>
				</article>
				<article class="col-sm-12 col-md-12 col-lg-6">
					<table class="table table-striped table-bordered table-hover" width="100%">
						<tr>
							<input type="hidden" value="10" name="idPresion">
							<input type="hidden" value='<?php echo $id_calibracion; ?>' name="id_calibra">
							<td style="text-align: center;"><input type="submit" name="guardarCalibracion" id="guardarCalibracion" value="Guardar"></td>
						</tr>		
					</table>
				</article>
			</div>
		</section>
		<section id="widget-grid" class="">
			<div class="row">
				
			</div>
		</section>
		</form>
	</div>			
<?php
}										
?>

</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->
<?php
	include("inc/footer.php");
	include("inc/scripts.php"); 
?>
<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<script>
	var t = document.getElementById("tom_lectura");
	t.disabled = true;
	t.addEventListener("mousemove", setTimeout('desbloquear()',6000),false);

	function desbloquear(){
		t.disabled = false;
	}



	function validar(campo){
		var tmp;
		var newcad;
		var cadena = "";
		var arrayData = new Array();
		var archivoTxt = new XMLHttpRequest();
		var fileRuta = 'Uploads/ArchivoNvo.txt';
		archivoTxt.open("GET",fileRuta,false);
		archivoTxt.send(null);
		var txt = archivoTxt.responseText;
		for (var i = 0; i<txt.length; i++){
			arrayData.push(txt[i]);
		}
		arrayData.forEach(function(data){
			cadena += data;
		});
		console.log(cadena);
		var lect = document.getElementById('desvia'+campo);
		lect.value=cadena;
		cadena="";

		calcularDesviacion(campo);

	}
</script>
<script type="text/javascript">
	function calcularDesviacion(item){

		var expa = document.getElementById('expa'+item);
		var mano = document.getElementById('mano'+item);
		var desvia = document.getElementById('desvia'+item);
		var valor_expa = expa.value;
		//var valor_mano = mano.value;
		var valor_desvia = desvia.value;
		var valor = 0;
		var exp_cil_gal;

		if(item==1){
			exp_cil_gal = 0;
		}else if(item==2){
			exp_cil_gal = 86.5;
		}else if(item==3){
			exp_cil_gal = 130.0;
		}else if(item==4){
			exp_cil_gal = 173.5;
		}else if(item==5){
			exp_cil_gal = 217.0;
		}else if(item==6){
			exp_cil_gal = 260.5;
		}else if(item==7){
			exp_cil_gal = 304.0;
		}else if(item==8){
			exp_cil_gal = 326.0;
		}

		valor = (exp_cil_gal-valor_expa)/exp_cil_gal*100;
		if(valor<0){
			valor = valor*-1;
		}

		if(item == 1){
			valor = 0;
		}

		valor = valor.toFixed(2);

		mano.value = valor;
		

	}
</script>
<script type="text/javascript">
	function digita_desviacion(valor_referencia){

		document.getElementById('expa8').value = valor_referencia;
		document.getElementById('expa7').value = valor_referencia;
		document.getElementById('expa6').value = valor_referencia;
		document.getElementById('expa5').value = valor_referencia;
		document.getElementById('expa4').value = valor_referencia;
		document.getElementById('expa3').value = valor_referencia;
		document.getElementById('expa2').value = valor_referencia;
		document.getElementById('expa1').value = valor_referencia;
		

	}
</script>
<script type="text/javascript">
	function calcularDesviacionMetodo(){
		
		var sumatoria = 0;
		var dato;
		<?php
			if($id_calibracion != 0){
				$consulta7 = "SELECT contador_presion FROM calibracion WHERE id_calibracion = '$id_calibracion'";
				$resultado7 = mysqli_query($con,$consulta7);
				$linea7 = mysqli_fetch_assoc($resultado7);

				$contador_presion = isset($linea7['contador_presion']) ? $linea7['contador_presion'] : NULL;

				$contador_campo = $contador_presion+1;
			}
			
		?>
		
		for(var i=1; i< <?php echo $contador_presion+1; ?>; i++){
			
			dato = parseFloat(document.getElementById('desvia'+i).value);
			sumatoria = sumatoria+dato;
			
		}
		i = i-1;

		var desviacion = sumatoria/i;

		desviacion = desviacion.toFixed(2);
		
		document.getElementById('guardar'+<?php echo $contador_campo; ?>).disabled = true;
		
		document.getElementById('desviacionT').value = desviacion;
	}
</script>
<script type="text/javascript">
	function calcularTemp(){
		var temp_cilindro = document.getElementById('temp_cilindro').value;
		var temp_camisa = document.getElementById('temp_camisa').value;

		var dif = temp_camisa-temp_cilindro;
		if(dif<0){
			dif = dif*-1;
		}

		document.getElementById('diferencia_temp').value = dif;
	}
</script>
<script type="text/javascript">
	function calcularTotal() {
		
		var totalDesviacion;
		var Xa = parseFloat(document.getElementById('incertidumbre_manometro').value);
		var Xb = parseFloat(document.getElementById('incertidumbre_bascula').value);
		var Xc = parseFloat(document.getElementById('desviacionT').value);

		var Xa2 = Math.pow(Xa,2);
		var Xb2 = Math.pow(Xb,2);
		var Xc2 = Math.pow(Xc,2);

		var X = Xa2+Xb2+Xc2;
		var totalDesviacion = Math.sqrt(X);
		totalDesviacion = totalDesviacion.toFixed(2);

		document.getElementById('incertidumbre_total').value = totalDesviacion;
		
	}
</script>
<script type="text/javascript">
// DO NOT REMOVE : GLOBAL FUNCTIONS!

$(document).ready(function() {
	
	/* // DOM Position key index //
		
	l - Length changing (dropdown)
	f - Filtering input (search)
	t - The Table! (datatable)
	i - Information (records)
	p - Pagination (paging)
	r - pRocessing 
	< and > - div elements
	<"#id" and > - div with an id
	<"class" and > - div with a class
	<"#id.class" and > - div with an id and class
	
	Also see: http://legacy.datatables.net/usage/features
	*/	

	/* BASIC ;*/
		var responsiveHelper_dt_basic = undefined;
		var responsiveHelper_datatable_fixed_column = undefined;
		var responsiveHelper_datatable_col_reorder = undefined;
		var responsiveHelper_datatable_tabletools = undefined;
		
		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};

		$('#dt_basic').dataTable({
			"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
			"autoWidth" : true,
			"preDrawCallback" : function() {
				// Initialize the responsive datatables helper once.
				if (!responsiveHelper_dt_basic) {
					responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
				}
			},
			"rowCallback" : function(nRow) {
				responsiveHelper_dt_basic.createExpandIcon(nRow);
			},
			"drawCallback" : function(oSettings) {
				responsiveHelper_dt_basic.respond();
			}
		});

	/* END BASIC */
	
	/* COLUMN FILTER  */
    var otable = $('#datatable_fixed_column').DataTable({
    	//"bFilter": false,
    	//"bInfo": false,
    	//"bLengthChange": false
    	//"bAutoWidth": false,
    	//"bPaginate": false,
    	//"bStateSave": true // saves sort state using localStorage
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_fixed_column) {
				responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_fixed_column.respond();
		}		
	
    });
    
    // custom toolbar
    $("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');
    	   
    // Apply the filter
    $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {
    	
        otable
            .column( $(this).parent().index()+':visible' )
            .search( this.value )
            .draw();
            
    } );
    /* END COLUMN FILTER */   

	/* COLUMN SHOW - HIDE */
	$('#datatable_col_reorder').dataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_col_reorder) {
				responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_col_reorder.respond();
		}			
	});
	
	/* END COLUMN SHOW - HIDE */

	/* TABLETOOLS */
	$('#datatable_tabletools').dataTable({
		
		// Tabletools options: 
		//   https://datatables.net/extensions/tabletools/button_options
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        "oTableTools": {
        	 "aButtons": [
             "copy",
             "csv",
             "xls",
                {
                    "sExtends": "pdf",
                    "sTitle": "SmartAdmin_PDF",
                    "sPdfMessage": "SmartAdmin PDF Export",
                    "sPdfSize": "letter"
                },
             	{
                	"sExtends": "print",
                	"sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>"
            	}
             ],
            "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
        },
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_tabletools) {
				responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_tabletools.respond();
		}
	});
	
	/* END TABLETOOLS */

})

</script>
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script type="text/javascript">

 
		
		$(document).ready(function() {
			
			// menu
			$("#menu").menu();
		
			/*
			 * AUTO COMPLETE AJAX
			 */
		
			function log(message) {
				$("<div>").text(message).prependTo("#log");
				$("#log").scrollTop(0);
			}
		
			$("#city").autocomplete({
				source : function(request, response) {
					$.ajax({
						url : "http://ws.geonames.org/searchJSON",
						dataType : "jsonp",
						data : {
							featureClass : "P",
							style : "full",
							maxRows : 12,
							name_startsWith : request.term
						},
						success : function(data) {
							response($.map(data.geonames, function(item) {
								return {
									label : item.name + (item.adminName1 ? ", " + item.adminName1 : "") + ", " + item.countryName,
									value : item.name
								}
							}));
						}
					});
				},
				minLength : 2,
				select : function(event, ui) {
					log(ui.item ? "Selected: " + ui.item.label : "Nothing selected, input was " + this.value);
				}
			});
		
			/*
			 * Spinners
			 */
			$("#spinner").spinner();
			$("#spinner-decimal").spinner({
				step : 0.01,
				numberFormat : "n"
			});
		
			$("#spinner-currency").spinner({
				min : 5,
				max : 2500,
				step : 25,
				start : 1000,
				numberFormat : "C"
			});
		
			/*
			 * CONVERT DIALOG TITLE TO HTML
			 * REF: http://stackoverflow.com/questions/14488774/using-html-in-a-dialogs-title-in-jquery-ui-1-10
			 */
			$.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
				_title : function(title) {
					if (!this.options.title) {
						title.html("&#160;");
					} else {
						title.html(this.options.title);
					}
				}
			}));
		
		
			/*
			* DIALOG SIMPLE
			*/
		
			// Dialog click
			$('#dialog_link').click(function() {
				$('#dialog_simple').dialog('open');
				return false;
		
			});
		
			$('#dialog_simple').dialog({
				autoOpen : false,
				width : 600,
				resizable : false,
				modal : true,
				title : "<div class='widget-header'><h4><i class='fa fa-warning'></i> Empty the recycle bin?</h4></div>",
				buttons : [{
					html : "<i class='fa fa-trash-o'></i>&nbsp; Delete all items",
					"class" : "btn btn-danger",
					click : function() {
						$(this).dialog("close");
					}
				}, {
					html : "<i class='fa fa-times'></i>&nbsp; Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
					}
				}]
			});
		
			/*
			* DIALOG HEADER ICON
			*/
		
			// Modal Link
			$('#modal_link').click(function() {
				$('#dialog-message').dialog('open');
				return false;
			});
		
			$("#dialog-message").dialog({
				autoOpen : false,
				modal : true,
				title : "<div class='widget-header'><h4><i class='icon-ok'></i> jQuery UI Dialog</h4></div>",
				buttons : [{
					html : "Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
					}
				}]
		
			});
		
			/*
			 * Remove focus from buttons
			 */
			$('.ui-dialog :button').blur();
		
			/*
			 * Just Tabs
			 */
		
			$('#tabs').tabs();
		
			/*
			 *  Simple tabs adding and removing
			 */
		
			$('#tabs2').tabs();
		
			// Dynamic tabs
			var tabTitle = $("#tab_title"), tabContent = $("#tab_content"), tabTemplate = "<li style='position:relative;'> <span class='air air-top-left delete-tab' style='top:7px; left:7px;'><button class='btn btn-xs font-xs btn-default hover-transparent'><i class='fa fa-times'></i></button></span></span><a href='#{href}'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; #{label}</a></li>", tabCounter = 2;
		
			var tabs = $("#tabs2").tabs();
		
			// modal dialog init: custom buttons and a "close" callback reseting the form inside
			var dialog = $("#addtab").dialog({
				autoOpen : false,
				width : 600,
				resizable : false,
				modal : true,
				buttons : [{
					html : "<i class='fa fa-times'></i>&nbsp; Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
		
					}
				}]
			});
		
			// addTab form: calls addTab function on submit and closes the dialog
			var form = dialog.find("form").submit(function(event) {
				addTab();
				dialog.dialog("close");
				
			});
		
			// actual addTab function: adds new tab using the input from the form above

			
			function addTab() 
			{
				
				var label = tabTitle.val() || "Tab " + tabCounter, id = "tabs-" + tabCounter, li = $(tabTemplate.replace(/#\{href\}/g, "#" + id).replace(/#\{label\}/g, label)), tabContentHtml = tabContent.val() || "Tab " + tabCounter + " content.";
		
				tabs.find(".ui-tabs-nav").append(li);
				tabs.append("<div id='" + id + "'><p>" + tabContentHtml + "</p></div>");
				tabs.tabs("refresh");
				tabCounter++;
		
				// clear fields
				$("#tab_title").val("");
				$("#tab_content").val("");
			}
		
			// addTab button: just opens the dialog
			$("#add_tab").button().click(function() {
				var idd = document.getElementById('num').firstChild.nodeValue;

				
				dialog.dialog("open");
			});
		
			// close icon: removing the tab on click
					
			/*
			* ACCORDION
			*/
			//jquery accordion
			
		     var accordionIcons = {
		         header: "fa fa-plus",    // custom icon class
		         activeHeader: "fa fa-minus" // custom icon class
		     };
		     
			$("#accordion").accordion({
				autoHeight : false,
				heightStyle : "content",
				collapsible : true,
				animate : 300,
				icons: accordionIcons,
				header : "h4",
			})
		
			/*
			 * PROGRESS BAR
			 */
			$("#progressbar").progressbar({
		     	value: 25,
		     	create: function( event, ui ) {
		     		$(this).removeClass("ui-corner-all").addClass('progress').find(">:first-child").removeClass("ui-corner-left").addClass('progress-bar progress-bar-success');
				}
			});			

		})

	</script>
	


<?php 
	//include footer
	include("inc/google-analytics.php"); 
}
else
{
    header("Location:index.php");
}
?>