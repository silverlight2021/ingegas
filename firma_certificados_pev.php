<?php
date_default_timezone_set("America/Bogota");

require ("libraries/conexion.php");
session_start();
if(@$_SESSION['logged']== 'yes')
{ 
	$id_user=$_SESSION['su'];
    $acc = $_SESSION['acc'];

	require_once("inc/init.php");
	require_once("inc/config.ui.php");
	$page_title = "Búsqueda Cilindros-PEV";
	$page_css[] = "your_style.css";
	include("inc/header.php");
	$page_nav[""][""][""][""] = true;
	include("inc/nav.php");
	
	$id_has_movimiento_cilindro_pev = isset($_REQUEST["id_has_movimiento_cilindro_pev"]) ? $_REQUEST["id_has_movimiento_cilindro_pev"] : NULL;
	$firma = isset($_REQUEST["firma"]) ? $_REQUEST["firma"] : NULL;
	
	if(strlen($id_has_movimiento_cilindro_pev)>0){
	    if($firma == 1){
	        $consulta = "UPDATE has_movimiento_cilindro_pev SET firma1 = 1 WHERE id_has_movimiento_cilindro_pev = $id_has_movimiento_cilindro_pev";
	        $resultado = mysqli_query($con, $consulta);
	        if($resultado){
    	        ?>
    	        <script type="text/javascript">
    	            window.alert("Firmado!");
    	            window.location = ("firma_certificados_pev.php");
    	        </script>
    	        <?php
    	    }
	    }else if($firma == 2){
	        $consulta = "UPDATE has_movimiento_cilindro_pev SET firma2 = 1 WHERE id_has_movimiento_cilindro_pev = $id_has_movimiento_cilindro_pev";
	        $resultado = mysqli_query($con, $consulta);
	        if($resultado){
    	        ?>
    	        <script type="text/javascript">
    	            window.alert("Firmado!");
    	            window.location = ("firma_certificados_pev.php");
    	        </script>
    	        <?php
    	    }
	    }
	}
?>
<!-- MAIN PANEL -->
<div id="main" role="main">
    <div id="content">
		<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">		
			<header>
				<span class="widget-icon"> <i class="fa fa-table"></i> </span>
				<h2>Certificados</h2>				
			</header>
			<div>
				<div class="jarviswidget-editbox"></div>
				<div class="widget-body no-padding">
					<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
						<form action="busqueda_cilindros_pev.php" method="POST" name="form1">
							<thead>
								<tr>
									<th>#</th>
									<th>Ver Cilindro</th>
									<th>Número Cilindro</th>
									<th>Cliente</th>
									<th>Orden</th>
									<th>Resultado de la prueba</th>
									<?php
									$consulta3 = "SELECT u.Name, u.LastName FROM firmasCertificados f
												 INNER JOIN user u
												 ON f.idUser = u.idUser
												 WHERE pev = 1";
									$resultado3 = mysqli_query($con,$consulta3);
									if(mysqli_num_rows($resultado3)>0){
										while ($linea3 = mysqli_fetch_array($resultado3)) {
											$Name = $linea3["Name"];
											$LastName = $linea3["LastName"];
											$nombreCompleto = $Name." ".$LastName;
											?>
											<th><?php echo $nombreCompleto; ?></th>
											<?php
										}
									}
									?>
									<th>Vista Previa </th>
									<th>Ver Certificado </th>
								</tr>
							</thead>
							<tbody>
								<?php
                                $contador = "1";
	                    	    $consulta = "SELECT h.*, c.tipo_cili_pev, l.nombre, o.orden_pev
		                    	        	FROM has_movimiento_cilindro_pev h
		                    	        	INNER JOIN cilindro_pev c
		                    	        	ON h.num_cili = c.num_cili_pev
																		INNER JOIN clientes l
																		ON h.id_cliente = l.id_cliente
																		INNER JOIN orden_servicio_pev o
																		ON o.id_transporte_pev = h.id_transporte_pev
		                    	        	ORDER BY h.id_has_movimiento_cilindro_pev DESC";
	                    	    $resultado = mysqli_query($con, $consulta);
	                    	    while($linea = mysqli_fetch_array($resultado)){
	                    	        $escribir = 0;
																$visual = 0;
																$nombreCliente = $linea["nombre"];
																$orden_pev = $linea["orden_pev"];
	                    	        $num_cili = $linea["num_cili"];
	                    	        $material_cilindro = $linea["material_cilindro_u"];
	                    	        $ph_u = $linea["ph_u"];
	                    	        $id_has_movimiento_cilindro_pev = $linea["id_has_movimiento_cilindro_pev"];
	                    	        $firma1 = $linea["firma1"];
	                    	        $firma2 = $linea["firma2"];
	                    	        $tipo_cili_pev = $linea["tipo_cili_pev"];
	                    	        if($material_cilindro == 3){
	                    	            $porcentaje_prueba = "No Aplica";
	                    	            $escribir = 1;
	                    	        }else{
	                    	            if($ph_u == 1){
	                    	                $consulta2 = "SELECT * FROM datos_prueba_pev WHERE id_has_movimiento_cilindro_pev = $id_has_movimiento_cilindro_pev";
	                    	                $resultado2 = mysqli_query($con, $consulta2);
	                    	                
	                    	                if(mysqli_num_rows($resultado2) > 0){
	                    	                    $escribir = 1;
	                    	                    $linea2 = mysqli_fetch_array($resultado2);
	                    	                    $porcentaje_prueba = $linea2["porcentaje_prueba"];
																						$id_datos_prueba_pev = $linea2["id_datos_prueba_pev"];
																						$fecha_hora = $linea2["fecha_hora"];
	                    	                    if($porcentaje_prueba < 10){
	                    	                        $porcentaje_prueba = "Aprobada - ".$fecha_hora;
	                    	                    }else{
	                    	                        $porcentaje_prueba = "Rechazada - ".$fecha_hora;
	                    	                    }
	                    	                }else{
	                    	                	$escribir = 1;
	                    	                	$porcentaje_prueba = "No Aplica";
	                    	                	if($material_cilindro == 1){
	                    	                		if($tipo_cili_pev == 1){
	                    	                			$tabla = "insp_vi_ace_alta_pev";
	                    	                			$campo = "id_insp_vi_ace_alta";
	                    	                		}else if($tipo_cili_pev == 2){
	                    	                			$tabla = "insp_vi_ace_baja_pev";
	                    	                			$campo = "id_insp_vi_ace_baja";
	                    	                		}else{
	                    	                			$escribir = 0;
	                    	                		}
	                    	                	}else if($material_cilindro == 2){
	                    	                		$tabla = "insp_vi_alu_pev";
	                    	                		$campo = "id_insp_vi_alu";
	                    	                	}else{
	                    	                		$escribir = 0;
	                    	                	}

	                    	                	if($escribir == 1){
																						
	                    	                		$consulta4 = "SELECT * FROM $tabla WHERE id_has_movimiento_cilindro_pev = $id_has_movimiento_cilindro_pev ORDER BY $campo DESC LIMIT 1";
																						$resultado4 = mysqli_query($con, $consulta4);
																						
																						

	                    	                		$linea4 = mysqli_fetch_array($resultado4);

	                    	                		$estado_prueba = $linea4["estado_prueba"];

	                    	                		if($estado_prueba == "CONDENADO"){
	                    	                			$visual = "1";
	                    	                		}else{
	                    	                			$escribir = 0;
	                    	                		}
	                    	                	}

	                    	                	
	                    	                }
	                    	            }else{
	                    	            	$porcentaje_prueba = "No Aplica";
	                    	            	$escribir = 0;
	                    	            }
	                    	        }
	                    	        
	                    	        if($escribir == 1){
	                    	            ?>
	                    	            <tr>
                                            <td width="5"><?php echo $contador++; ?></td>
                                            <td width="5" align="center"><a href="cilindro_pev.php?id_cilindro_pev=<?php echo $id_cilindro_pev; ?>"><img src="img/lupa.png" width="20" height="20"></a></td>
																						<td><?php echo $num_cili; ?></td>
																						<td><?php echo $nombreCliente; ?></td>
																						<td><?php echo $orden_pev; ?></td>
                                            <td><?php echo $porcentaje_prueba; ?></td>
                                            <td align="center">
	                                        <?php
	                                        $consulta3 = "SELECT * FROM firmasCertificados WHERE pev = 1 LIMIT 1";
											$resultado3 = mysqli_query($con,$consulta3);
											$linea3 = mysqli_fetch_array($resultado3);

											$idUserFirma = $linea3["idUser"];
                                                if(($id_user == 1)||($id_user == $idUserFirma)){
                                                    if($firma1 == 1){
                                                        ?>
                                                        <p>Firmado</p>
                                                        <?php
                                                    }else{
                                                        ?>
                                                        <a class="btn btn-info btn-block" href="firma_certificados_pev.php?id_has_movimiento_cilindro_pev=<?php echo $id_has_movimiento_cilindro_pev; ?>&firma=1">Firmar</a>
                                                        <?php
                                                    }
                                                }else{
                                                    if($firma1 == 1){
                                                        ?>
                                                        <p>Firmado</p>
                                                        <?php
                                                    }else{
                                                        ?>
                                                        <p>No Firmado</p>
                                                        <?php
                                                    }
                                                }
                                            ?>
                                            </td>
                                            <td align="center">
                                            <?php
                                            $consulta3 = "SELECT * FROM firmasCertificados WHERE pev = 1 LIMIT 1, 2";
											$resultado3 = mysqli_query($con,$consulta3);
											$linea3 = mysqli_fetch_array($resultado3);

											$idUserFirma = $linea3["idUser"];
                                                if(($id_user == 1)||($id_user == $idUserFirma)){
                                                    if($firma2 == 1){
                                                        ?>
                                                        <p>Firmado</p>
                                                        <?php
                                                    }else{
                                                        ?>
                                                        <a class="btn btn-info btn-block" href="firma_certificados_pev.php?id_has_movimiento_cilindro_pev=<?php echo $id_has_movimiento_cilindro_pev; ?>&firma=2">Firmar</a>
                                                        <?php
                                                    }
                                                }else{
                                                    if($firma2 == 1){
                                                        ?>
                                                        <p>Firmado</p>
                                                        <?php
                                                    }else{
                                                        ?>
                                                        <p>No Firmado</p>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </td>
                                            <?php 
                                            if($visual == 1){
                                            	$vista_previa = "pdf_certificado_pev_insp_visual.php?id_has_movimiento_cilindro_pev=".$id_has_movimiento_cilindro_pev;
                                            }else{
                                            	$vista_previa = "pdf_certificado_pev_prueba.php?id_datos_prueba_pev=".$id_datos_prueba_pev."&id_has_movimiento_cilindro_pev=".$id_has_movimiento_cilindro_pev;
                                            }
                                            ?>
                                            <td align="center"><a href='<?php echo $vista_previa; ?>'><img src='img/vista_previa.png' width='30' height='30'></a></td>
                                            <td align="center">
                                                <?php
                                                if(($firma1 == 1)&&($firma2 == 1)){
                                                	if($visual == 1){
                                                		$certificado = "pdf_certificado_pev_insp_visual.php?&id_has_movimiento_cilindro_pev=".$id_has_movimiento_cilindro_pev."&firmas=1";
                                                	}else{
                                                		$certificado = "pdf_certificado_pev_prueba.php?id_datos_prueba_pev=".$id_datos_prueba_pev."&id_has_movimiento_cilindro_pev=".$id_has_movimiento_cilindro_pev."&firmas=1";
                                                	}
                                                    
                                                    ?>
                                                    <a href='<?php echo $certificado; ?>'><img src='img/pdf_icon.png' width='30' height='30'></a>
                                                    <?php
                                                }else{
                                                    ?>
                                                    <p>No Firmado</p>
                                                    <?php
                                                }
                                                ?>
                                            </td>
                                    	</tr>
	                    	            <?php
	                    	        }
                                }
                                mysqli_free_result($resultado);   
                            	
                            ?>
							</tbody>
						</form>
					</table>
				</div>
			</div>
		</div>
	</div>

</div>
<!-- ==========================CONTENT ENDS HERE ========================== -->
<?php
	include("inc/footer.php");
	include("inc/scripts.php"); 
?>

<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<script type="text/javascript">



// DO NOT REMOVE : GLOBAL FUNCTIONS!

$(document).ready(function() {
	
	/* // DOM Position key index //
		
	l - Length changing (dropdown)
	f - Filtering input (search)
	t - The Table! (datatable)
	i - Information (records)
	p - Pagination (paging)
	r - pRocessing 
	< and > - div elements
	<"#id" and > - div with an id
	<"class" and > - div with a class
	<"#id.class" and > - div with an id and class
	
	Also see: http://legacy.datatables.net/usage/features
	*/	

	/* BASIC ;*/
		var responsiveHelper_dt_basic = undefined;
		var responsiveHelper_datatable_fixed_column = undefined;
		var responsiveHelper_datatable_col_reorder = undefined;
		var responsiveHelper_datatable_tabletools = undefined;
		
		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};

		$('#dt_basic').dataTable({
			"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
			"autoWidth" : true,
			"preDrawCallback" : function() {
				// Initialize the responsive datatables helper once.
				if (!responsiveHelper_dt_basic) {
					responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
				}
			},
			"rowCallback" : function(nRow) {
				responsiveHelper_dt_basic.createExpandIcon(nRow);
			},
			"drawCallback" : function(oSettings) {
				responsiveHelper_dt_basic.respond();
			}
		});

	/* END BASIC */
	
	/* COLUMN FILTER  */
    var otable = $('#datatable_fixed_column').DataTable({
    	//"bFilter": false,
    	//"bInfo": false,
    	//"bLengthChange": false
    	//"bAutoWidth": false,
    	//"bPaginate": false,
    	//"bStateSave": true // saves sort state using localStorage
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_fixed_column) {
				responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_fixed_column.respond();
		}		
	
    });
    
    // custom toolbar
    $("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');
    	   
    // Apply the filter
    $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {
    	
        otable
            .column( $(this).parent().index()+':visible' )
            .search( this.value )
            .draw();
            
    } );
    /* END COLUMN FILTER */   

	/* COLUMN SHOW - HIDE */
	$('#datatable_col_reorder').dataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_col_reorder) {
				responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_col_reorder.respond();
		}			
	});
	
	/* END COLUMN SHOW - HIDE */

	/* TABLETOOLS */
	$('#datatable_tabletools').dataTable({
		
		// Tabletools options: 
		//   https://datatables.net/extensions/tabletools/button_options
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        "oTableTools": {
        	 "aButtons": [
             "copy",
             "csv",
             "xls",
                {
                    "sExtends": "pdf",
                    "sTitle": "SmartAdmin_PDF",
                    "sPdfMessage": "SmartAdmin PDF Export",
                    "sPdfSize": "letter"
                },
             	{
                	"sExtends": "print",
                	"sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>"
            	}
             ],
            "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
        },
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_tabletools) {
				responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_tabletools.respond();
		}
	});
	
	/* END TABLETOOLS */

})

</script>

<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script type="text/javascript">

 
		
		$(document).ready(function() {
			
			// menu
			$("#menu").menu();
		
			/*
			 * AUTO COMPLETE AJAX
			 */
		
			function log(message) {
				$("<div>").text(message).prependTo("#log");
				$("#log").scrollTop(0);
			}
		
			$("#city").autocomplete({
				source : function(request, response) {
					$.ajax({
						url : "http://ws.geonames.org/searchJSON",
						dataType : "jsonp",
						data : {
							featureClass : "P",
							style : "full",
							maxRows : 12,
							name_startsWith : request.term
						},
						success : function(data) {
							response($.map(data.geonames, function(item) {
								return {
									label : item.name + (item.adminName1 ? ", " + item.adminName1 : "") + ", " + item.countryName,
									value : item.name
								}
							}));
						}
					});
				},
				minLength : 2,
				select : function(event, ui) {
					log(ui.item ? "Selected: " + ui.item.label : "Nothing selected, input was " + this.value);
				}
			});
		
			/*
			 * Spinners
			 */
			$("#spinner").spinner();
			$("#spinner-decimal").spinner({
				step : 0.01,
				numberFormat : "n"
			});
		
			$("#spinner-currency").spinner({
				min : 5,
				max : 2500,
				step : 25,
				start : 1000,
				numberFormat : "C"
			});
		
			/*
			 * CONVERT DIALOG TITLE TO HTML
			 * REF: http://stackoverflow.com/questions/14488774/using-html-in-a-dialogs-title-in-jquery-ui-1-10
			 */
			$.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
				_title : function(title) {
					if (!this.options.title) {
						title.html("&#160;");
					} else {
						title.html(this.options.title);
					}
				}
			}));
		
		
			/*
			* DIALOG SIMPLE
			*/
		
			// Dialog click
			$('#dialog_link').click(function() {
				$('#dialog_simple').dialog('open');
				return false;
		
			});
		
			$('#dialog_simple').dialog({
				autoOpen : false,
				width : 600,
				resizable : false,
				modal : true,
				title : "<div class='widget-header'><h4><i class='fa fa-warning'></i> Empty the recycle bin?</h4></div>",
				buttons : [{
					html : "<i class='fa fa-trash-o'></i>&nbsp; Delete all items",
					"class" : "btn btn-danger",
					click : function() {
						$(this).dialog("close");
					}
				}, {
					html : "<i class='fa fa-times'></i>&nbsp; Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
					}
				}]
			});
		
			/*
			* DIALOG HEADER ICON
			*/
		
			// Modal Link
			$('#modal_link').click(function() {
				$('#dialog-message').dialog('open');
				return false;
			});
		
			$("#dialog-message").dialog({
				autoOpen : false,
				modal : true,
				title : "<div class='widget-header'><h4><i class='icon-ok'></i> jQuery UI Dialog</h4></div>",
				buttons : [{
					html : "Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
					}
				}]
		
			});
		
			/*
			 * Remove focus from buttons
			 */
			$('.ui-dialog :button').blur();
		
			/*
			 * Just Tabs
			 */
		
			$('#tabs').tabs();
		
			/*
			 *  Simple tabs adding and removing
			 */
		
			$('#tabs2').tabs();
		
			// Dynamic tabs
			var tabTitle = $("#tab_title"), tabContent = $("#tab_content"), tabTemplate = "<li style='position:relative;'> <span class='air air-top-left delete-tab' style='top:7px; left:7px;'><button class='btn btn-xs font-xs btn-default hover-transparent'><i class='fa fa-times'></i></button></span></span><a href='#{href}'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; #{label}</a></li>", tabCounter = 2;
		
			var tabs = $("#tabs2").tabs();
		
			// modal dialog init: custom buttons and a "close" callback reseting the form inside
			var dialog = $("#addtab").dialog({
				autoOpen : false,
				width : 600,
				resizable : false,
				modal : true,
				buttons : [{
					html : "<i class='fa fa-times'></i>&nbsp; Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
		
					}
				}]
			});
		
			// addTab form: calls addTab function on submit and closes the dialog
			var form = dialog.find("form").submit(function(event) {
				addTab();
				dialog.dialog("close");
				
			});
		
			// actual addTab function: adds new tab using the input from the form above

			
			function addTab() 
			{
				
				var label = tabTitle.val() || "Tab " + tabCounter, id = "tabs-" + tabCounter, li = $(tabTemplate.replace(/#\{href\}/g, "#" + id).replace(/#\{label\}/g, label)), tabContentHtml = tabContent.val() || "Tab " + tabCounter + " content.";
		
				tabs.find(".ui-tabs-nav").append(li);
				tabs.append("<div id='" + id + "'><p>" + tabContentHtml + "</p></div>");
				tabs.tabs("refresh");
				tabCounter++;
		
				// clear fields
				$("#tab_title").val("");
				$("#tab_content").val("");
			}
		
			// addTab button: just opens the dialog
			$("#add_tab").button().click(function() {
				var idd = document.getElementById('num').firstChild.nodeValue;

				
				dialog.dialog("open");
			});
		
			// close icon: removing the tab on click
					
			/*
			* ACCORDION
			*/
			//jquery accordion
			
		     var accordionIcons = {
		         header: "fa fa-plus",    // custom icon class
		         activeHeader: "fa fa-minus" // custom icon class
		     };
		     
			$("#accordion").accordion({
				autoHeight : false,
				heightStyle : "content",
				collapsible : true,
				animate : 300,
				icons: accordionIcons,
				header : "h4",
			})
		
			/*
			 * PROGRESS BAR
			 */
			$("#progressbar").progressbar({
		     	value: 25,
		     	create: function( event, ui ) {
		     		$(this).removeClass("ui-corner-all").addClass('progress').find(">:first-child").removeClass("ui-corner-left").addClass('progress-bar progress-bar-success');
				}
			});			

		})

	</script>
	


<?php 

	include("inc/google-analytics.php"); 


}
else
{
    header("Location:index.php");
}
?>