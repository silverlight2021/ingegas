<div class="row" align="center">
	<table>
		<tr>
			<td colspan="3" style="padding: 15px;"><strong><h4 class="modal-title" align="center">PRIMERA LECTURA EXPANSIÓN TOTAL</h4></strong></td>
		</tr>
		<tr>
			<td colspan="3" align="justify"><p>Por favor registre el valor obtenido luego de transcurridos 30 segundos para la primera medida de Expansión Total, luego oprima el botón para iniciar la segunda medida de Expansión Total Final</p></td>
		</tr>
		<tr>
			<td align="center"><input type="text" name="expansion_total_1" id="expansion_total_1" class="form-control"></td>
			<td align="center"><button type="button" class="btn btn-success" onclick="continuar_1();">SIGUIENTE PASO</button></td>			
			<td align="center"><button type="button" class="btn btn-danger" onclick="abortar_1();">ABORTAR</button></td>
		</tr>
	</table>										
</div>
