<?php
date_default_timezone_set("America/Bogota");

require ("libraries/conexion.php");
require ("libraries/seguimiento.php");
session_start();

if(@$_SESSION['logged']== 'yes')
{ 	
	$id_user =$_SESSION['su'];
	if(isset($_POST['ph_cilindro']))
	{
		$id_cilindro_eto_1 = $_POST['id_cilindro_eto_1']; 

		$consulta= "UPDATE cilindro_eto SET 	                
    	id_estado = 10,
    	fecha_ult_eto = '".date("Y/m/d")."'
        WHERE id_cilindro_eto = $id_cilindro_eto_1";
	    $resultado = mysqli_query($con,$consulta) ;
	    if ($resultado == FALSE)
	    {
	      	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
	    }
	    else
	    {

	    	seguimiento(11,$id_cilindro_eto_1,$id_user,0,0);//id_seguimiento,id_cilindro_eto,id_user,id_proceso,id_has_proceso
	    	
	    	?>
            <script type="text/javascript"> 
              window.location ="ph.php";
            </script>
            <?php
	    }
	}

	require_once("inc/init.php");
	require_once("inc/config.ui.php");
	$page_title = "Prueba Hidrostatica";
	$page_css[] = "your_style.css";
	include("inc/header.php");
	$page_nav[""][""][""][""] = true;
	include("inc/nav.php");
	$fecha=date("Y/m/d");
?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<!-- Modal -->
<div class="modal fade" id="modal_ph" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">	
	<div class="modal-dialog">
		<form name="mezcla" action="ph.php" method="POST" id="mezcla" >
			<input type="hidden" name="id_cilindro_eto_1"  id="id_cilindro_eto_1" value="">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					&times;
					</button>
					<h4 class="modal-title" id="myModalLabel">Prueba Hidrostatica</h4>
				</div>
				<div class="modal-body">
					<div class="well well-sm well-primary">
						<div class="row">
							<div class="col-md-8">
								<div class="form-group">
									<label for="category">Usuario :</label>
									<input type="text" class="form-control" placeholder="Usuario" name="Usuario" readonly required value="<?php echo $_SESSION['name']; ?>"/>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="category">Fecha Actual:</label>
									<input type="text" class="form-control" placeholder="Fecha" name="Fecha" readonly value="<?php echo date("Y/m/d"); ?>"/>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
						<input type="submit" value="Guardar" name="ph_cilindro" id="ph_cilindro" class="btn btn-primary" />
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</form>
	</div><!-- /.modal -->
	<!-- Fin Modal -->
</div>
<div id="main" role="main">
<?php
if (in_array(18, $acc))
{
?>
	<div id="content">
		<section id="widget-grid" class="">
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-6">						
					<div class="jarviswidget" id="wid-id-2" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h2>Cilindros Para PH</h2>			
						</header>
						<div>				
							<div class="jarviswidget-editbox"></div>																	
							<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">						
								<thead>
									<tr>
										<th>#</th>                                            
			                            <th>Cilindro</th>
			                            <th>Estado</th>
			                            <th>Tipo Gas</th>
										<th>Capacidad (Kg)</th>
			                            <th>Acción</th>			                            
									</tr>
								</thead>
								<tbody>													
								  	<?php			                          	
			                            $consulta1 = "SELECT cilindro_eto.num_cili_eto,cilindro_eto.id_cilindro_eto,cilindro_eto.id_estado,pre_produccion_mezcla.id_pre_produccion_mezcla  FROM cilindro_eto INNER JOIN pre_produccion_mezcla ON pre_produccion_mezcla.id_cilindro_eto = cilindro_eto.id_cilindro_eto WHERE cilindro_eto.id_estado =  8" 	;
			                            $resultado1 = mysqli_query($con,$consulta1) ;
			                            while ($linea1 = mysqli_fetch_array($resultado1))
			                            {
			                            	$contador1 = $contador1 + 1;
			                            	$num_cili_eto = $linea1["num_cili_eto"];
											$id_cilindro_eto = $linea1["id_cilindro_eto"];
											$id_pre_produccion_mezcla22 = $linea1["id_pre_produccion_mezcla"];
											$id_estado = $linea1["id_estado"];
											
											$consulta = "SELECT * FROM cilindro_eto WHERE id_cilindro_eto =".$id_cilindro_eto;
			                                $resultado = mysqli_query($con,$consulta) ;
			                                while ($linea = mysqli_fetch_array($resultado))
			                                {
			                                	$id_tipo_cilindro = $linea["id_tipo_cilindro"];
			                                	$id_tipo_envace = $linea["id_tipo_envace"];
			                                }mysqli_free_result($resultado);	

											$consulta3 = "SELECT * FROM tipo_cilindro WHERE id_tipo_cilindro =".$id_tipo_cilindro;
		                                    $resultado3 = mysqli_query($con,$consulta3) ;
		                                    while ($linea3 = mysqli_fetch_array($resultado3))
		                                    {
		                                    	$tipo_cili = $linea3["tipo_cili"];	                                    	
		                                    }mysqli_free_result($resultado3);

		                                    $consulta4 = "SELECT * FROM tipo_envace WHERE id_tipo_envace =".$id_tipo_envace;
		                                    $resultado4 = mysqli_query($con,$consulta4) ;
		                                    while ($linea4 = mysqli_fetch_array($resultado4))
		                                    {
		                                    	$tipo = $linea4["tipo"];
		                                    }mysqli_free_result($resultado4);	

											$consulta5 = "SELECT * FROM cilindro_eto WHERE id_cilindro_eto =".$id_cilindro_eto;
											$resultado5 = mysqli_query($con,$consulta5) ;
											while ($linea5 = mysqli_fetch_array($resultado5))
											{
											  	$num_cili_eto = $linea5["num_cili_eto"];
											  	//$id_estado = $linea5["id_estado"];
											  	
											}mysqli_free_result($resultado5); 
											$consulta6 = "SELECT * FROM estado_cilindro_eto WHERE id_estado_cilindro = $id_estado";
		                                    $resultado6 = mysqli_query($con,$consulta6) ;
		                                    while ($linea6 = mysqli_fetch_array($resultado6))
		                                    {
		                                    	$estado_cili_eto = $linea6["estado_cili_eto"];		                                                	
		                                    }mysqli_free_result($resultado6);
			                            			                            		                            
			                            ?>
				                            <tr class="odd gradeX">
				                              	<td width="5"><?php echo $contador1; ?></td> 
				                              	<td><?php echo $num_cili_eto; ?></td>
				                              	<td><?php echo $estado_cili_eto; ?></td>
				                              	<td><?php echo $tipo_cili; ?></td>
				                              	<td><?php echo $tipo; ?></td>
				                              	<?php
												if (in_array(20, $acc))
												{
												?>
					                              	<td width="100" style="vertical-align:bottom;">			                              	
					                              		<input type="image" onclick="editar(<?php echo $id_cilindro_eto; ?>)" src="img/edit.png" width="30" height="30">			                              				                              
					                              	</td>
					                            <?php
												}										
												?>   

				                            </tr>                           
			                            <?php
			                            }mysqli_free_result($resultado1);	                                            
		                  		    ?>
								</tbody>							
							</table>
						</div>            
         			</div>
         		</article>
        	</div>      
   		</section> 
    </div>
<?php
}										
?>
</div>

<?php
  include("inc/footer.php");
  include("inc/scripts.php"); 
?>
<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/jquery-form/jquery-form.min.js"></script>
<script type="text/javascript">
function resta()
{ 
    mezcla.g_evacuar.value = ((parseFloat(mezcla.peso_inicial.value))-(parseFloat(mezcla.peso_final.value))).toFixed(2);      	    
}
</script>
<script type="text/javascript">
	function editar(cili)
	{
		
		$("#modal_ph").modal();
		//alert(cili);
		//document.getElementById("id_cilindro_eto_1").innerHTML = cili;
		$("input[name=id_cilindro_eto_1]").val(cili);
		//$("input[name=num_cili]").val(num);
		
		
	}
</script>
<script type="text/javascript">
 
		
		$(document).ready(function() {
			
			// menu
			$("#menu").menu();
		
			/*
			 * AUTO COMPLETE AJAX
			 */
		
			function log(message) {
				$("<div>").text(message).prependTo("#log");
				$("#log").scrollTop(0);
			}
		
			$("#city").autocomplete({
				source : function(request, response) {
					$.ajax({
						url : "http://ws.geonames.org/searchJSON",
						dataType : "jsonp",
						data : {
							featureClass : "P",
							style : "full",
							maxRows : 12,
							name_startsWith : request.term
						},
						success : function(data) {
							response($.map(data.geonames, function(item) {
								return {
									label : item.name + (item.adminName1 ? ", " + item.adminName1 : "") + ", " + item.countryName,
									value : item.name
								}
							}));
						}
					});
				},
				minLength : 2,
				select : function(event, ui) {
					log(ui.item ? "Selected: " + ui.item.label : "Nothing selected, input was " + this.value);
				}
			});
		
			/*
			 * Spinners
			 */
			$("#spinner").spinner();
			$("#spinner-decimal").spinner({
				step : 0.01,
				numberFormat : "n"
			});
		
			$("#spinner-currency").spinner({
				min : 5,
				max : 2500,
				step : 25,
				start : 1000,
				numberFormat : "C"
			});
		
			/*
			 * CONVERT DIALOG TITLE TO HTML
			 * REF: http://stackoverflow.com/questions/14488774/using-html-in-a-dialogs-title-in-jquery-ui-1-10
			 */
			$.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
				_title : function(title) {
					if (!this.options.title) {
						title.html("&#160;");
					} else {
						title.html(this.options.title);
					}
				}
			}));
		
		
			/*
			* DIALOG SIMPLE
			*/
		
			// Dialog click
			$('#dialog_link').click(function() {
				$('#dialog_simple').dialog('open');
				return false;
		
			});
		
			$('#dialog_simple').dialog({
				autoOpen : false,
				width : 600,
				resizable : false,
				modal : true,
				title : "<div class='widget-header'><h4><i class='fa fa-warning'></i> Empty the recycle bin?</h4></div>",
				buttons : [{
					html : "<i class='fa fa-trash-o'></i>&nbsp; Delete all items",
					"class" : "btn btn-danger",
					click : function() {
						$(this).dialog("close");
					}
				}, {
					html : "<i class='fa fa-times'></i>&nbsp; Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
					}
				}]
			});
		
			/*
			* DIALOG HEADER ICON
			*/
		
			// Modal Link
			$('#modal_link').click(function() {
				$('#dialog-message').dialog('open');
				return false;
			});
		
			$("#dialog-message").dialog({
				autoOpen : false,
				modal : true,
				title : "<div class='widget-header'><h4><i class='icon-ok'></i> jQuery UI Dialog</h4></div>",
				buttons : [{
					html : "Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
					}
				}]
		
			});
		
			/*
			 * Remove focus from buttons
			 */
			$('.ui-dialog :button').blur();
		
			/*
			 * Just Tabs
			 */
		
			$('#tabs').tabs();
		
			/*
			 *  Simple tabs adding and removing
			 */
		
			$('#tabs2').tabs();
		
			// Dynamic tabs
			var tabTitle = $("#tab_title"), tabContent = $("#tab_content"), tabTemplate = "<li style='position:relative;'> <span class='air air-top-left delete-tab' style='top:7px; left:7px;'><button class='btn btn-xs font-xs btn-default hover-transparent'><i class='fa fa-times'></i></button></span></span><a href='#{href}'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; #{label}</a></li>", tabCounter = 2;
		
			var tabs = $("#tabs2").tabs();
		
			// modal dialog init: custom buttons and a "close" callback reseting the form inside
			var dialog = $("#addtab").dialog({
				autoOpen : false,
				width : 600,
				resizable : false,
				modal : true,
				buttons : [{
					html : "<i class='fa fa-times'></i>&nbsp; Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
		
					}
				}]
			});
		
			// addTab form: calls addTab function on submit and closes the dialog
			var form = dialog.find("form").submit(function(event) {
				addTab();
				dialog.dialog("close");
				
			});
		
			// actual addTab function: adds new tab using the input from the form above

			
			function addTab() 
			{
				
				var label = tabTitle.val() || "Tab " + tabCounter, id = "tabs-" + tabCounter, li = $(tabTemplate.replace(/#\{href\}/g, "#" + id).replace(/#\{label\}/g, label)), tabContentHtml = tabContent.val() || "Tab " + tabCounter + " content.";
		
				tabs.find(".ui-tabs-nav").append(li);
				tabs.append("<div id='" + id + "'><p>" + tabContentHtml + "</p></div>");
				tabs.tabs("refresh");
				tabCounter++;
		
				// clear fields
				$("#tab_title").val("");
				$("#tab_content").val("");
			}
		
			// addTab button: just opens the dialog
			$("#add_tab").button().click(function() {
				var idd = document.getElementById('num').firstChild.nodeValue;

				
				dialog.dialog("open");
			});
		
			// close icon: removing the tab on click
					
			/*
			* ACCORDION
			*/
			//jquery accordion
			
		     var accordionIcons = {
		         header: "fa fa-plus",    // custom icon class
		         activeHeader: "fa fa-minus" // custom icon class
		     };
		     
			$("#accordion").accordion({
				autoHeight : false,
				heightStyle : "content",
				collapsible : true,
				animate : 300,
				icons: accordionIcons,
				header : "h4",
			})
		
			/*
			 * PROGRESS BAR
			 */
			$("#progressbar").progressbar({
		     	value: 25,
		     	create: function( event, ui ) {
		     		$(this).removeClass("ui-corner-all").addClass('progress').find(">:first-child").removeClass("ui-corner-left").addClass('progress-bar progress-bar-success');
				}
			});			

		})

	</script>
	<script src="js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<script type="text/javascript">
// DO NOT REMOVE : GLOBAL FUNCTIONS!

$(document).ready(function() {
	
	/* // DOM Position key index //
		
	l - Length changing (dropdown)
	f - Filtering input (search)
	t - The Table! (datatable)
	i - Information (records)
	p - Pagination (paging)
	r - pRocessing 
	< and > - div elements
	<"#id" and > - div with an id
	<"class" and > - div with a class
	<"#id.class" and > - div with an id and class
	
	Also see: http://legacy.datatables.net/usage/features
	*/	

	/* BASIC ;*/
		var responsiveHelper_dt_basic = undefined;
		var responsiveHelper_datatable_fixed_column = undefined;
		var responsiveHelper_datatable_col_reorder = undefined;
		var responsiveHelper_datatable_tabletools = undefined;
		
		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};

		$('#dt_basic').dataTable({
			"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
			"autoWidth" : true,
			"preDrawCallback" : function() {
				// Initialize the responsive datatables helper once.
				if (!responsiveHelper_dt_basic) {
					responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
				}
			},
			"rowCallback" : function(nRow) {
				responsiveHelper_dt_basic.createExpandIcon(nRow);
			},
			"drawCallback" : function(oSettings) {
				responsiveHelper_dt_basic.respond();
			}
		});

	/* END BASIC */
	
	/* COLUMN FILTER  */
    var otable = $('#datatable_fixed_column').DataTable({
    	//"bFilter": false,
    	//"bInfo": false,
    	//"bLengthChange": false
    	//"bAutoWidth": false,
    	//"bPaginate": false,
    	//"bStateSave": true // saves sort state using localStorage
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_fixed_column) {
				responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_fixed_column.respond();
		}		
	
    });
    
    // custom toolbar
    $("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');
    	   
    // Apply the filter
    $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {
    	
        otable
            .column( $(this).parent().index()+':visible' )
            .search( this.value )
            .draw();
            
    } );
    /* END COLUMN FILTER */   

	/* COLUMN SHOW - HIDE */
	$('#datatable_col_reorder').dataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_col_reorder) {
				responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_col_reorder.respond();
		}			
	});
	
	/* END COLUMN SHOW - HIDE */

	/* TABLETOOLS */
	$('#datatable_tabletools').dataTable({
		
		// Tabletools options: 
		//   https://datatables.net/extensions/tabletools/button_options
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        "oTableTools": {
        	 "aButtons": [
             "copy",
             "csv",
             "xls",
                {
                    "sExtends": "pdf",
                    "sTitle": "SmartAdmin_PDF",
                    "sPdfMessage": "SmartAdmin PDF Export",
                    "sPdfSize": "letter"
                },
             	{
                	"sExtends": "print",
                	"sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>"
            	}
             ],
            "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
        },
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_tabletools) {
				responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_tabletools.respond();
		}
	});
	
	/* END TABLETOOLS */

})

</script>
<?php 
include("inc/google-analytics.php"); 
}
else
{
    header("Location:index.php");
}
?>
					                       
										