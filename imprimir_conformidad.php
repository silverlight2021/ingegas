<?php
$id_no_conformidad = isset($_REQUEST['id_no_conformidad']) ? $_REQUEST['id_no_conformidad'] : NULL;
date_default_timezone_set("America/Bogota");

require ("libraries/conexion.php");
session_start();
?>
<script type="text/javascript">


  
window.onload = function() {
  imprSelec('muestra');
};
function imprSelec(muestra)
	{
		var ficha=document.getElementById(muestra);
		var ventimp=window.open(' ','popimpr');
		ventimp.document.write(ficha.innerHTML);
		ventimp.document.close();
		ventimp.print();
		ventimp.close();
	}

	
</script>

	
<div id="muestra" style="display:none">
	<p align="center">INGEGAS-INGENIERIA Y GASES LTDA</p>
	<p align="center">PROCESO "MEZCLAS DE OXIDO DE ETILENO"</p>
	<p align="center">INGRESO A PLANTA PRODUCTO NO CONFORME"</p>
	<table width="100%">
		<tr>
			<td>
				FECHA
			</td>
			<td>
				CLIENTE
			</td>
		</tr>
		<br>
		<tr>
			<td>
				RECIBE
			</td>
			<td>
				TOTAL UNDS
			</td>
		</tr>
	</table>
	<br>
	<br>
	<br>

	<table class="table table-bordered" style="border: 1px solid black;border-collapse: collapse;" align="center">
		<thead>
			<tr>
				<th  style="border: 1px solid black;border-collapse: collapse;">
					CIL. No.
				</th>
				<th  style="border: 1px solid black;border-collapse: collapse;">
					PRODUCTO
				</th>
				<th  style="border: 1px solid black;border-collapse: collapse;">
					TAPA
				</th>
				<th  style="border: 1px solid black;border-collapse: collapse;">
					TAPON
				</th>
				<th  style="border: 1px solid black;border-collapse: collapse;"> 
					PESO ACTUAL
				</th>
				<th  style="border: 1px solid black;border-collapse: collapse;">
					TARA ESTAMPADA
				</th>			
				<th  style="border: 1px solid black;border-collapse: collapse;">
					FECHA LLENADO
				</th>
				<th  style="border: 1px solid black;border-collapse: collapse;">
					PESO SALIDA
				</th>
				<th  style="border: 1px solid black;border-collapse: collapse;">
					TARA VACIO
				</th>
				<th  style="border: 1px solid black;border-collapse: collapse;">
					OBSERVACIONES
				</th>
			</tr>
		</thead>
		<tbody>
			<?php
            $consulta  = "SELECT * FROM no_conformidad WHERE id_no_conformidad =".$id_no_conformidad;
			$resultado = mysqli_query($con,$consulta) ; 
			$linea= mysqli_fetch_array($resultado);
			if ($linea>0) 
			{				
				$origen = $linea["origen"];
				if ($origen=="cliente") 
				{
					$origen_1 ="Cliente";
					$destino_1 ="Planta";

				}
				else
				{
					$origen_1 ="Planta";
					$destino_1 ="Cliente";
				}
				$fecha_hora = $linea["fecha_hora"];
				$id_tipo_registro = $linea["id_tipo_registro"];
				$solicitante = $linea["solicitante"];
				$descripcion = $linea["descripcion"];
				$correccion = $linea["correccion"];
				$ana_causas = $linea["ana_causas"];
				$accion = $linea["accion"];
				$responsable = $linea["responsable"];
				$observaciones = $linea["observaciones"];
				$id_cilindro_eto = $linea["id_cilindro_eto"];

				$consulta2 = "SELECT num_cili_eto,id_tipo_cilindro  
						FROM cilindro_eto  
						WHERE id_cilindro_eto = ".$id_cilindro_eto;
				$resultado2 = mysqli_query($con,$consulta2) ;
				while ($linea1 = mysqli_fetch_array($resultado2))
				{
					$num_cili_eto = $linea1["num_cili_eto"];
					$id_tipo_cilindro = $linea1["id_tipo_cilindro"];
				}mysqli_free_result($resultado2);

				$consulta3 = "SELECT tipo_cili 
						FROM tipo_cilindro  
						WHERE id_tipo_cilindro = ".$id_tipo_cilindro;
				$resultado3 = mysqli_query($con,$consulta3) ;
				while ($linea3 = mysqli_fetch_array($resultado3))
				{
					$producto = $linea3["tipo_cili"];
				}mysqli_free_result($resultado3);


				$consulta4  = "SELECT MAX(id_produccion_mezclas ) as fech_crea FROM produccion_mezclas  WHERE id_cilindro=".$id_cilindro_eto;
		  		$resultado5 = mysqli_query($con,$consulta4) ;
		  		$linea5 = mysqli_fetch_array($resultado5);
		  		if ($linea5>0) 
				{
			  		$id_produccion_mezclas = isset($linea5["fech_crea"]) ? $linea5["fech_crea"] : NULL;
					
					$consulta5  = "SELECT fech_crea,peso_inicial,pre_final,tara_vacio,peso_final_1 FROM produccion_mezclas WHERE id_produccion_mezclas= $id_produccion_mezclas";
				  	$resultado5 = mysqli_query($con,$consulta5) ;
				  	$linea5 = mysqli_fetch_array($resultado5);
				  	$peso_inicial = isset($linea5["peso_inicial"]) ? $linea5["peso_inicial"] : NULL;
					
					$fech_crea = isset($linea5["fech_crea"]) ? $linea5["fech_crea"] : NULL;
					$peso_final_1 = isset($linea5["peso_final_1"]) ? $linea5["peso_final_1"] : NULL;
					$tara_vacio = isset($linea5["tara_vacio"]) ? $linea5["tara_vacio"] : NULL;
					//$pre_final = isset($linea5["pre_final"]) ? $linea5["pre_final"] : NULL;
					$peso_salida =  $peso_final_1+$tara_vacio ." Kg";
					


					mysqli_free_result($resultado5);
				}
                 
                ?>
                <tr class="odd gradeX">                                
                  	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $num_cili_eto; ?></td>
                  	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $producto; ?></td>
                  	<td style="border: 1px solid black;border-collapse: collapse;" align="center"></td>  
                  	<td style="border: 1px solid black;border-collapse: collapse;" align="center"></td>  
                  	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $peso_inicial." Kg"; ?></td>  
                  	<td style="border: 1px solid black;border-collapse: collapse;" align="center"></td>     
                  	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $fech_crea; ?></td>  
                  	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $peso_salida; ?></td>  
                  	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $tara_vacio; ?></td>  
                  	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $observaciones; ?></td>
                </tr>                           
            <?php
            }mysqli_free_result($resultado);	                                            
  		    ?>
		</tbody>					
	</table>
	<br>
	<br>
	<table width="100%">
		<tr>
			<td>
				OPERARIO
			</td>
			<td>
				REVISA
			</td>
		</tr>
		<br>
	</table>
	
	<br>			
</div>