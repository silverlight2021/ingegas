<?php
require ("libraries/conexion.php");
date_default_timezone_set("America/Bogota");

$id_orden = isset($_REQUEST['id_orden']) ? $_REQUEST['id_orden'] : NULL;

$consulta  = "SELECT * FROM has_orden_cilindro WHERE id_orden= $id_orden";
$resultado = mysqli_query($con,$consulta) ;
$linea = mysqli_fetch_array($resultado);
$id_tipo_cilindro = isset($linea["id_tipo_cilindro"]) ? $linea["id_tipo_cilindro"] : NULL;
$id_tipo_envace = isset($linea["id_tipo_envace"]) ? $linea["id_tipo_envace"] : NULL;

$consulta1 = "SELECT * FROM tipo_cilindro WHERE id_tipo_cilindro =".$id_tipo_cilindro;
$resultado1 = mysqli_query($con,$consulta1) ;
$linea1 = mysqli_fetch_array($resultado1);
$tipo_cili = isset($linea1["tipo_cili"]) ? $linea1["tipo_cili"] : NULL;
$obs_cili = isset($linea1["obs_cili"]) ? $linea1["obs_cili"] : NULL;



$consulta2 = "SELECT * FROM tipo_envace WHERE id_tipo_envace =".$id_tipo_envace;
$resultado2 = mysqli_query($con,$consulta2) ;
$linea2 = mysqli_fetch_array($resultado2);
$tipo1 = isset($linea2["tipo"]) ? $linea2["tipo"] : NULL;

$consulta3  = "SELECT * FROM produccion_mezclas WHERE id_orden= $id_orden";
$resultado3 = mysqli_query($con,$consulta3) ;
$linea3 = mysqli_fetch_array($resultado3);
$fech_crea = isset($linea3["fech_crea"]) ? $linea3["fech_crea"] : NULL;
$lote_co = isset($linea3["lote_co"]) ? $linea3["lote_co"] : NULL;
$id_lote_eto = isset($linea3["id_lote_eto"]) ? $linea3["id_lote_eto"] : NULL;
$lote_pro = isset($linea3["lote_pro"]) ? $linea3["lote_pro"] : NULL;




$consulta4  = "SELECT * FROM ordenes WHERE id_orden= $id_orden";
$resultado4 = mysqli_query($con,$consulta4) ;
$linea4 = mysqli_fetch_array($resultado4);
$num_ord = isset($linea4["num_ord"]) ? $linea4["num_ord"] : NULL;
$fecha_ven = isset($linea4["fecha_ven"]) ? $linea4["fecha_ven"] : NULL;

$consulta5 = "SELECT * FROM lote_co2 WHERE id_num_cilindro =".$lote_co;
$resultado5 = mysqli_query($con,$consulta5) ;
$linea5 = mysqli_fetch_array($resultado5);
$num_cilindro_co2 = isset($linea5["num_cilindro_co2"]) ? $linea5["num_cilindro_co2"] : NULL;
$pureza_co2 = isset($linea5["pureza"]) ? $linea5["pureza"] : NULL;
$humedad_co2 = isset($linea5["humedad"]) ? $linea5["humedad"] : NULL;

$consulta6 = "SELECT * FROM lote_eto WHERE id_lote_eto =".$id_lote_eto;
$resultado6 = mysqli_query($con,$consulta6) ;
$linea6 = mysqli_fetch_array($resultado6);
$num_cilindro_eto = isset($linea6["num_cilindro_eto"]) ? $linea6["num_cilindro_eto"] : NULL;
$pureza_eto = isset($linea6["pureza"]) ? $linea6["pureza"] : NULL;
$humedad_eto = isset($linea6["humedad"]) ? $linea6["humedad"] : NULL;

$consulta67 = "SELECT * FROM firmas WHERE id_formas = 1";
$resultado67 = mysqli_query($con,$consulta67) ;
$linea67 = mysqli_fetch_array($resultado67);
$firmas = isset($linea67["firmas"]) ? $linea67["firmas"] : NULL;
$cargo = isset($linea67["cargo"]) ? $linea67["cargo"] : NULL;

$consulta6 = "SELECT * FROM firmas WHERE id_formas = 2";
$resultado6 = mysqli_query($con,$consulta6) ;
$linea6 = mysqli_fetch_array($resultado6);
$firmas1 = isset($linea6["firmas"]) ? $linea6["firmas"] : NULL;
$cargo1 = isset($linea6["cargo"]) ? $linea6["cargo"] : NULL;
$firma_1 = $firmas."-".$cargo;
$firma_2 = $firmas1."-".$cargo1;

mysqli_free_result($resultado6);
mysqli_free_result($resultado5);
mysqli_free_result($resultado4);
mysqli_free_result($resultado3);
mysqli_free_result($resultado2);
mysqli_free_result($resultado1);	
mysqli_free_result($resultado);

if ($tipo_cili=="ETO-10") 
{
	$valor= 10;
	$valvula= 3;
	$tipo="K";
}
if ($tipo_cili=="ETO-20") 
{
	$valor= 20;
	$valvula= 3;
	$tipo="K";
}
if ($tipo_cili=="ETO-90") 
{
	$valor= 90;
	$valvula= 2;

	if ($tipo1=="25") 
	{
		$tipo="K";
	}
	if ($tipo1=="35") 
	{
		$tipo="G";
	}

	//$tipo="K";
}
if ($tipo_cili=="ETO-100") 
{
	$valor= 100;
	$valvula= 1;
	$tipo="G";
}


$consulta66 = "SELECT * FROM conexion_valvula WHERE id_conexion = ".$valvula;
$resultado66 = mysqli_query($con,$consulta66) ;
$linea66 = mysqli_fetch_array($resultado66);
$tip_cone = isset($linea66["tip_cone"]) ? $linea66["tip_cone"] : NULL;

$consulta77 = "SELECT * FROM manometro_co2 ";
$resultado77 = mysqli_query($con,$consulta77) ;
$linea77 = mysqli_fetch_array($resultado77);
$manometro_co2 = isset($linea77["manometro_co2"]) ? $linea77["manometro_co2"] : NULL;

$consulta88 = "SELECT * FROM manometro_eto ";
$resultado88 = mysqli_query($con,$consulta88) ;
$linea88 = mysqli_fetch_array($resultado88);
$manometro_eto = isset($linea88["manometro_eto"]) ? $linea88["manometro_eto"] : NULL;

$consulta88 = "SELECT * FROM incertidumbre ";
$resultado88 = mysqli_query($con,$consulta88) ;
$linea88 = mysqli_fetch_array($resultado88);
$incertidumbre = isset($linea88["incertidumbre"]) ? $linea88["incertidumbre"] : NULL;




$expo="";
//$id_orden=14;
$consulta33 = "SELECT *  
			FROM produccion_mezclas  
			WHERE id_orden = ".$id_orden;
$resultado33 = mysqli_query($con,$consulta33) ;
while ($linea33 = mysqli_fetch_array($resultado33))
{
	$filas += 1;	
	$id_cilindro_eto = $linea33["id_cilindro"];
	$peso_esperado = $linea33["peso_esperado"];
	$pre_final = $linea33["pre_final"];
	$tara_vacio = $linea33["tara_vacio"];
	$desviacion = $linea33["desviacion"];

	$expo=$expo+(pow($desviacion,2));

}
mysqli_free_result($resultado33);

$raiz1= sqrt($expo);
$total_raiz=100-$raiz1-$manometro_co2-$manometro_eto-$incertidumbre."%";


$restante = 100-$valor;
$oxi_eti = $valor."%";
$restante_car = $restante."%";


$path_imagenes = "img/ingegas_pdf.png";
$path_pdf = "Uploads/pdf/certificado/";
 @$num_factura	 = $consecutivo;
$nombre_empresa = utf8_decode("INGEGAS");
$nombre_empresa_ajustado = utf8_decode(substr($nombre_empresa, 0, 47));

$nombre_empresa_1 = utf8_decode("INGENIERIA Y GASES LTDA");
$nombre_empresa_ajustado_1 = utf8_decode(substr($nombre_empresa_1, 0, 47));

@$titulo="CERTIFICADO DE PRODUCCIÓN Y CALIDAD";
@$Registro_Sanitario='REGISTRO SANITARIO No.'.$obs_cili;
@$mezcla_eto='MEZCLA'." ".$tipo_cili;
@$numero="Número";
@$codigo_producto=$tipo_cili;
@$numero_lote=$num_ord;
@$tipo_cili1=$tipo;
@$con_val=$tip_cone;
@$oficina="Calle 163 a N° 19 a - 48 TEL : 670 61 26 FAX :677 29 51 ";
@$bogota="Bogotá D.C - Colombia";
@$llenado="GRAVIMETRÍA";
@$firma1="Hola Hola";
@$firma2="Hola Hola";

require('funcion_mezcla_pdf.php');

$pdf = new PDF_Invoice( 'L', 'cm', 'Legal' );
$pdf->AddPage();
$pdf->Image($path_imagenes,1,1,0,0,'');
$pdf->fact_dev1($nombre_empresa_ajustado);
$pdf->fact_dev2($nombre_empresa_ajustado_1);
$pdf->fact_dev3($titulo);
$pdf->fact_dev4($Registro_Sanitario);
$pdf->fact_dev5($mezcla_eto);

//$pdf->addLetras($letras);
//$pdf->addCLientName($recibido_de);
//$pdf->addCLientAdress($valor_total);
//$pdf->addCLientDoc($ciudad_empresa,$fechaAfiliacion);
//$pdf->addClientConcepto($Concepto);
//$pdf->addPayMode2($No_Cheque,$Sucursal,$Banco);
//$pdf->addPayMode($No_Cheque,$Sucursal);
//$pdf->addSign("");
//
$file_name = str_replace(' ','_',$nombre_empresa_ajustado)."_Certificado_No._".$id_orden.".pdf";
$file_destination = $path_pdf.$file_name;
$pdf->Output($file_destination,'F');
$pdf->Output($file_name,'I');

?>