<?php
session_start();
date_default_timezone_set("America/Bogota");

require ("libraries/conexion.php");

if(@$_SESSION['logged']== 'yes')
{ 
	$id_user=$_SESSION['su'];
    $acc = $_SESSION['acc'];

	require_once("inc/init.php");
	require_once("inc/config.ui.php");
	$page_title = "Recepcion Cilindros Especiales";
	$page_css[] = "your_style.css";
	include("inc/header.php");
	$page_nav[""][""][""][""] = true;
	include("inc/nav.php");
	$fecha_hoy = date("Y-m-d");
?>

<?php
	if(isset($_POST['r_cilindro'])){
		$id_cilindro_eto = $_POST["id_cilindro_eto"];
		$id_orden_especial = $_POST["id_orden_especial"];
		$num_factura = $_POST["num_factura"];

		$consulta7 = "UPDATE cilindro_eto SET id_estado = 3 WHERE id_cilindro_eto = $id_cilindro_eto";
		$resultado7 = mysqli_query($con,$consulta7);

		$consulta8 = "UPDATE produccion_especiales SET estado_devolucion = 1, fecha_recepcion = '$fecha_hoy', num_factura = '$num_factura' WHERE id_cilindro_eto = $id_cilindro_eto AND id_orden_especial = $id_orden_especial";
		$resultado8 = mysqli_query($con,$consulta8);

		$consulta9 = "SELECT * FROM produccion_especiales WHERE id_orden_especial = $id_orden_especial AND estado_devolucion = 0";
		$resultado9 = mysqli_query($con,$consulta9);

		if(mysqli_num_rows($resultado9)>0){
			?>
			<script type="text/javascript">
				alert("Cilindro Recibido!")
				window.location = ("recepcion_especiales.php");
			</script>
			<?php
		}else{
			$consulta10 = "UPDATE ordenes_especiales SET id_estado = 3 WHERE id_orden_especial = '$id_orden_especial'";
			$resultado10 = mysqli_query($con,$consulta10);

			if($resultado10){
				?>
				<script type="text/javascript">
					alert("Cilindro Recibido!")
					window.location = ("recepcion_especiales.php");
				</script>
				<?php
			}
		}

	}
?>
<!-- MAIN PANEL -->
<div id="main" role="main">

	<div id="content">	
		<div class="" align="center">
			<h1  class="page-title txt-color-blueDark"> <?php echo $page_title; ?>
			</h1>			
		</div>		
		<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">		
			<header>
				<span class="widget-icon"> <i class="fa fa-table"></i> </span>
				<h2>Cilindros</h2>				
			</header>
			<div>
				<div class="jarviswidget-editbox"></div>
				<div class="widget-body no-padding">
					<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
						<form action="busqueda_cilindros_eto.php" method="POST" name="form1">
							<thead>
								<tr>
									<th>#</th>
									<?php
									if(in_array(80, $acc)){
									?>
										<th>Recibir</th>
									<?php
									}
									?>
									<th>Número Cilindro</th>
									<th>Tipo Gas</th>
									<th>Capacidad (Kg)</th>
									<th>Última Revisión PEV </th>
									<th>Fecha De Lavado</th>
									<th>Fecha Ultimo Llenado</th>
									<th>Fecha Ultima<br>Recogida</th>
									<th>Estado Cilindro</th>

								</tr>
							</thead>
							<tbody>
								<?php
                                    
	                    			$contador = "0";
	                    	        $consulta = "SELECT id_cilindro_eto,num_cili_eto,fecha_lav_eto,especi_eto,id_estado,fecha_ult_eto,id_tipo_cilindro,id_tipo_envace FROM cilindro_eto WHERE tipo = '1' AND id_tipo_cilindro >= 5 AND id_tipo_cilindro <> 10 ORDER BY id_cilindro_eto DESC ";
	                                $resultado = mysqli_query($con,$consulta) ;
	                                while ($linea = mysqli_fetch_array($resultado))
	                                {	                                    
	                                    $contador = $contador + 1;
	                                    $id_cilindro_eto = $linea["id_cilindro_eto"];
	                                    $num_cili_eto = $linea["num_cili_eto"];
	                                    $fecha_lav_eto = $linea["fecha_lav_eto"];
	                                    $especi_eto = $linea["especi_eto"];
	                                    $id_estado = $linea["id_estado"];
	                                    $fecha_ult_eto = $linea["fecha_ult_eto"];
	                                    $id_tipo_cilindro = $linea["id_tipo_cilindro"];
	                                    $id_tipo_envace = $linea["id_tipo_envace"];

	                                    $consulta4 = "SELECT * FROM estado_cilindro_eto WHERE id_estado_cilindro = $id_estado";
	                                    $resultado4 = mysqli_query($con,$consulta4) ;
	                                    while ($linea4 = mysqli_fetch_array($resultado4))
	                                    {
	                                    	$estado_cili_eto = $linea4["estado_cili_eto"];		                                                	
	                                    }mysqli_free_result($resultado4);

	                                    $consulta3 = "SELECT * FROM tipo_cilindro WHERE id_tipo_cilindro =".$id_tipo_cilindro;
	                                    $resultado3 = mysqli_query($con,$consulta3) ;
	                                    while ($linea3 = mysqli_fetch_array($resultado3))
	                                    {
	                                    	$tipo_cili = $linea3["tipo_cili"];	                                    	
	                                    }mysqli_free_result($resultado3);

	                                    $consulta4 = "SELECT * FROM tipo_envace WHERE id_tipo_envace =".$id_tipo_envace;
	                                    $resultado4 = mysqli_query($con,$consulta4) ;
	                                    while ($linea4 = mysqli_fetch_array($resultado4))
	                                    {
	                                    	$tipo = $linea4["tipo"];
	                                    }mysqli_free_result($resultado4);

	                                    $consulta5  = "SELECT MAX(fech_crea) FROM produccion_mezclas  WHERE id_cilindro=".$id_cilindro_eto;
								  		$resultado5 = mysqli_query($con,$consulta5) ;
								  		$linea5 = mysqli_fetch_array($resultado5);
								  		$fech_crea = isset($linea5["MAX(fech_crea)"]) ? $linea5["MAX(fech_crea)"] : NULL;

								  		$consulta6  = "SELECT MAX(fecha_hora) FROM seguimiento_cilindro WHERE id_seguimiento = 8 AND id_cilindro_eto=".$id_cilindro_eto ;
								  		$resultado6 = mysqli_query($con,$consulta6) ;
								  		$linea6 = mysqli_fetch_array($resultado6);
								  		$fecha = isset($linea6["MAX(fecha_hora)"]) ? $linea6["MAX(fecha_hora)"] : NULL;	                         

								  		

	                                ?>
                                    <tr>
                                        <td width="5"><?php echo $contador; ?></td>
                                        <?php
                                        if((in_array(80, $acc))&&($id_estado == 12)){
                                        ?>
                                        	<td width="5"><a href="#"><img src="img/recepcion_cilindro.png" width="20" height="20" onclick="recibir_cilindro(<?php echo $id_cilindro_eto; ?>)"></a></td>
                                        <?php
                                        }else{
                                        ?>
                                        	<td width="5"><img src="img/recepcion_cilindro_disabled.png" width="20" height="20"></td>
                                        <?php
                                    	}
                                        ?>
                                        <td><?php echo $num_cili_eto; ?></td>
                                        <td><?php echo $tipo_cili; ?></td>
                                        <td><?php echo $tipo; ?></td>
                                        <td><?php echo $fecha_ult_eto; ?></td>
                                        <td><?php echo $fecha_lav_eto; ?></td>
                                        <td><?php echo $fech_crea; ?></td>	
                                        <td><?php echo $fecha; ?></td>	
                                        <td><?php echo $estado_cili_eto; ?></td>	                                                        
                                	</tr>    
                                <?php
                                }
                                mysqli_free_result($resultado);   
                            	
                            ?>
							</tbody>
						</form>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- END MAIN CONTENT -->
<div class="modal fade" id="modal_orden" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">
	<form name="mezcla" action="recepcion_especiales.php" method="POST" id="mezcla">
	<input type="hidden" name="id_cilindro_eto"  value="id_cilindro_eto">
	<input type="hidden" name="id_orden_especial" id="id_orden_especial" value="id_orden_especial">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" id ="cerrar"aria-hidden="true">
					&times;
					</button>
					<h4 class="modal-title" id="myModalLabel">Rececpción Cilindro</h4>
				</div>
				<div class="modal-body">
				<div class="well well-sm well-primary">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="category">Numero Cilindro :</label>
								<input type="text" class="form-control" placeholder="Numero Cilindro" name="num_cili_eto" readonly required />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="category">Fecha de Recepción :</label>
								<input type="date" class="form-control" name="fecha_recepcion" readonly required value="<?php echo $fecha_hoy; ?>" />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="category">No de Factura :</label>
								<input type="text" class="form-control" placeholder="Numero de Factura" name="num_factura" required />
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" id="Cancelar" data-dismiss="modal">	Cancelar</button>
					
						<input type="submit" value="Guardar" name="r_cilindro" id="r_cilindro" class="btn btn-primary" />
					
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</form>
</div><!-- /.modal -->
<!-- Fin Modal -->

</div>
<!-- ==========================CONTENT ENDS HERE ========================== -->
<?php
	include("inc/footer.php");
	include("inc/scripts.php"); 
?>

<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<script type="text/javascript">
	function recibir_cilindro(id_cili){

		form_cili(id_cili);
		$("#modal_orden").modal();

		
	}

	function form_cili(id_cili){
		$.ajax({
			url: 'id_cili3_especial.php',
			type: 'POST',
			data: 'dato='+id_cili,
		})
		.done(function(data){
			var objeto = JSON.parse(data);							
			$("input[name=id_cilindro_eto]").val(objeto.id);
			$("input[name=num_cili_eto]").val(objeto.num_cili_eto);
			$("input[name=id_orden_especial]").val(objeto.id_orden_especial);
		})
		.fail(function() {
			console.log("error");
		});
	}
</script>
<script type="text/javascript">



// DO NOT REMOVE : GLOBAL FUNCTIONS!

$(document).ready(function() {
	
	/* // DOM Position key index //
		
	l - Length changing (dropdown)
	f - Filtering input (search)
	t - The Table! (datatable)
	i - Information (records)
	p - Pagination (paging)
	r - pRocessing 
	< and > - div elements
	<"#id" and > - div with an id
	<"class" and > - div with a class
	<"#id.class" and > - div with an id and class
	
	Also see: http://legacy.datatables.net/usage/features
	*/	

	/* BASIC ;*/
		var responsiveHelper_dt_basic = undefined;
		var responsiveHelper_datatable_fixed_column = undefined;
		var responsiveHelper_datatable_col_reorder = undefined;
		var responsiveHelper_datatable_tabletools = undefined;
		
		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};

		$('#dt_basic').dataTable({
			"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
			"autoWidth" : true,
			"preDrawCallback" : function() {
				// Initialize the responsive datatables helper once.
				if (!responsiveHelper_dt_basic) {
					responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
				}
			},
			"rowCallback" : function(nRow) {
				responsiveHelper_dt_basic.createExpandIcon(nRow);
			},
			"drawCallback" : function(oSettings) {
				responsiveHelper_dt_basic.respond();
			}
		});

	/* END BASIC */
	
	/* COLUMN FILTER  */
    var otable = $('#datatable_fixed_column').DataTable({
    	//"bFilter": false,
    	//"bInfo": false,
    	//"bLengthChange": false
    	//"bAutoWidth": false,
    	//"bPaginate": false,
    	//"bStateSave": true // saves sort state using localStorage
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_fixed_column) {
				responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_fixed_column.respond();
		}		
	
    });
    
    // custom toolbar
    $("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');
    	   
    // Apply the filter
    $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {
    	
        otable
            .column( $(this).parent().index()+':visible' )
            .search( this.value )
            .draw();
            
    } );
    /* END COLUMN FILTER */   

	/* COLUMN SHOW - HIDE */
	$('#datatable_col_reorder').dataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_col_reorder) {
				responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_col_reorder.respond();
		}			
	});
	
	/* END COLUMN SHOW - HIDE */

	/* TABLETOOLS */
	$('#datatable_tabletools').dataTable({
		
		// Tabletools options: 
		//   https://datatables.net/extensions/tabletools/button_options
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        "oTableTools": {
        	 "aButtons": [
             "copy",
             "csv",
             "xls",
                {
                    "sExtends": "pdf",
                    "sTitle": "SmartAdmin_PDF",
                    "sPdfMessage": "SmartAdmin PDF Export",
                    "sPdfSize": "letter"
                },
             	{
                	"sExtends": "print",
                	"sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>"
            	}
             ],
            "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
        },
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_tabletools) {
				responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_tabletools.respond();
		}
	});
	
	/* END TABLETOOLS */

})

</script>

<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script type="text/javascript">

 
		
		$(document).ready(function() {
			
			// menu
			$("#menu").menu();
		
			/*
			 * AUTO COMPLETE AJAX
			 */
		
			function log(message) {
				$("<div>").text(message).prependTo("#log");
				$("#log").scrollTop(0);
			}
		
			$("#city").autocomplete({
				source : function(request, response) {
					$.ajax({
						url : "http://ws.geonames.org/searchJSON",
						dataType : "jsonp",
						data : {
							featureClass : "P",
							style : "full",
							maxRows : 12,
							name_startsWith : request.term
						},
						success : function(data) {
							response($.map(data.geonames, function(item) {
								return {
									label : item.name + (item.adminName1 ? ", " + item.adminName1 : "") + ", " + item.countryName,
									value : item.name
								}
							}));
						}
					});
				},
				minLength : 2,
				select : function(event, ui) {
					log(ui.item ? "Selected: " + ui.item.label : "Nothing selected, input was " + this.value);
				}
			});
		
			/*
			 * Spinners
			 */
			$("#spinner").spinner();
			$("#spinner-decimal").spinner({
				step : 0.01,
				numberFormat : "n"
			});
		
			$("#spinner-currency").spinner({
				min : 5,
				max : 2500,
				step : 25,
				start : 1000,
				numberFormat : "C"
			});
		
			/*
			 * CONVERT DIALOG TITLE TO HTML
			 * REF: http://stackoverflow.com/questions/14488774/using-html-in-a-dialogs-title-in-jquery-ui-1-10
			 */
			$.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
				_title : function(title) {
					if (!this.options.title) {
						title.html("&#160;");
					} else {
						title.html(this.options.title);
					}
				}
			}));
		
		
			/*
			* DIALOG SIMPLE
			*/
		
			// Dialog click
			$('#dialog_link').click(function() {
				$('#dialog_simple').dialog('open');
				return false;
		
			});
		
			$('#dialog_simple').dialog({
				autoOpen : false,
				width : 600,
				resizable : false,
				modal : true,
				title : "<div class='widget-header'><h4><i class='fa fa-warning'></i> Empty the recycle bin?</h4></div>",
				buttons : [{
					html : "<i class='fa fa-trash-o'></i>&nbsp; Delete all items",
					"class" : "btn btn-danger",
					click : function() {
						$(this).dialog("close");
					}
				}, {
					html : "<i class='fa fa-times'></i>&nbsp; Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
					}
				}]
			});
		
			/*
			* DIALOG HEADER ICON
			*/
		
			// Modal Link
			$('#modal_link').click(function() {
				$('#dialog-message').dialog('open');
				return false;
			});
		
			$("#dialog-message").dialog({
				autoOpen : false,
				modal : true,
				title : "<div class='widget-header'><h4><i class='icon-ok'></i> jQuery UI Dialog</h4></div>",
				buttons : [{
					html : "Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
					}
				}]
		
			});
		
			/*
			 * Remove focus from buttons
			 */
			$('.ui-dialog :button').blur();
		
			/*
			 * Just Tabs
			 */
		
			$('#tabs').tabs();
		
			/*
			 *  Simple tabs adding and removing
			 */
		
			$('#tabs2').tabs();
		
			// Dynamic tabs
			var tabTitle = $("#tab_title"), tabContent = $("#tab_content"), tabTemplate = "<li style='position:relative;'> <span class='air air-top-left delete-tab' style='top:7px; left:7px;'><button class='btn btn-xs font-xs btn-default hover-transparent'><i class='fa fa-times'></i></button></span></span><a href='#{href}'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; #{label}</a></li>", tabCounter = 2;
		
			var tabs = $("#tabs2").tabs();
		
			// modal dialog init: custom buttons and a "close" callback reseting the form inside
			var dialog = $("#addtab").dialog({
				autoOpen : false,
				width : 600,
				resizable : false,
				modal : true,
				buttons : [{
					html : "<i class='fa fa-times'></i>&nbsp; Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
		
					}
				}]
			});
		
			// addTab form: calls addTab function on submit and closes the dialog
			var form = dialog.find("form").submit(function(event) {
				addTab();
				dialog.dialog("close");
				
			});
		
			// actual addTab function: adds new tab using the input from the form above

			
			function addTab() 
			{
				
				var label = tabTitle.val() || "Tab " + tabCounter, id = "tabs-" + tabCounter, li = $(tabTemplate.replace(/#\{href\}/g, "#" + id).replace(/#\{label\}/g, label)), tabContentHtml = tabContent.val() || "Tab " + tabCounter + " content.";
		
				tabs.find(".ui-tabs-nav").append(li);
				tabs.append("<div id='" + id + "'><p>" + tabContentHtml + "</p></div>");
				tabs.tabs("refresh");
				tabCounter++;
		
				// clear fields
				$("#tab_title").val("");
				$("#tab_content").val("");
			}
		
			// addTab button: just opens the dialog
			$("#add_tab").button().click(function() {
				var idd = document.getElementById('num').firstChild.nodeValue;

				
				dialog.dialog("open");
			});
		
			// close icon: removing the tab on click
					
			/*
			* ACCORDION
			*/
			//jquery accordion
			
		     var accordionIcons = {
		         header: "fa fa-plus",    // custom icon class
		         activeHeader: "fa fa-minus" // custom icon class
		     };
		     
			$("#accordion").accordion({
				autoHeight : false,
				heightStyle : "content",
				collapsible : true,
				animate : 300,
				icons: accordionIcons,
				header : "h4",
			})
		
			/*
			 * PROGRESS BAR
			 */
			$("#progressbar").progressbar({
		     	value: 25,
		     	create: function( event, ui ) {
		     		$(this).removeClass("ui-corner-all").addClass('progress').find(">:first-child").removeClass("ui-corner-left").addClass('progress-bar progress-bar-success');
				}
			});			

		})

	</script>
	


<?php 

	include("inc/google-analytics.php"); 


}
else
{
    header("Location:index.php");
}
?>