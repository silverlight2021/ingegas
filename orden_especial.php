<?php
date_default_timezone_set("America/Bogota");
require ("libraries/conexion.php");
session_start();
if(@$_SESSION['logged']== 'yes')
{ 
  	$id_orden_especial = isset($_REQUEST['id_orden_especial']) ? $_REQUEST['id_orden_especial'] : NULL;
    $id_user =$_SESSION['su'];
    $acc = $_SESSION['acc'];

	if($id_orden_especial == 0)
	{
		$id_orden_especial = $_POST['id_orden_especial'] ;
		
		$consulta  = "SELECT MAX( num_ord ) FROM ordenes_especiales ";
  		$resultado = mysqli_query($con,$consulta) ;
  		$linea = mysqli_fetch_array($resultado);
  		$max = isset($linea["MAX( num_ord )"]) ? $linea["MAX( num_ord )"] : NULL;	
		  			
	    $num_ord=$max+1; 	
		$id_finalizar=1;
		$Fecha = date("Y/m/d");

		$consulta  = "INSERT INTO ordenes_especiales
	        (fecha,num_ord,id_estado,id_finalizar) 
	        VALUES ( '".date("Y/m/d H:i:s")."','".$num_ord."','1','".$id_finalizar."')";
	    $resultado = mysqli_query($con,$consulta) ;
	    if ($resultado == FALSE)
	    {
	      	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
	    }
	    else
	    {
	      	$id_orden_especial1 = mysqli_insert_id($con);
	      	header('Location: orden_especial.php?id_orden_especial='.$id_orden_especial1);
	    }	  	
	}
	if(isset($_POST['editar_orden_movimiento']))
	{
		$obs_orden_editar = $_POST['obs_orden_editar'] ;
		$id_orden_especial1 = $_POST['id_orden_especial1'] ;
		
	    $consulta= "UPDATE has_orden_especial_cilindro SET 
	                obs_orden = '$obs_orden_editar'                                           
	                WHERE id_orden_especial = $id_orden_especial1 ";
	    	$resultado = mysqli_query($con,$consulta) ;

		    if ($resultado == FALSE)
		    {
		      	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    }
		    else
		    {
		      	header('Location: orden_especial.php?id_orden_especial='.$id_orden_especial1);
		    }
	}
	if(isset($_POST['g_orden_movimiento']))
	{
		$id_tipo_cilindro2 = $_POST['id_tipo_cilindro1'] ;
		$id_tipo_envace2=18 ;
		$cantidad1=$_POST['cantidad'] ;
		$obs_orden1=$_POST['obs_orden'] ;
		$kg1=$_POST['kg'] ; 
		
		
	    $consulta = "INSERT INTO has_orden_especial_cilindro
	          		(id_orden_especial,id_tipo_cilindro, id_tipo_envace, cantidad, obs_orden,id_user,kg) 
	          		VALUES ('".$id_orden_especial."','".$id_tipo_cilindro2."','".$id_tipo_envace2."', '".$cantidad1."', '".$obs_orden1."','".$id_user."','".$kg1."' )";
	    $resultado = mysqli_query($con,$consulta) ;
	    if ($resultado == FALSE)
	    {
	      echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
	    }
	    else
	    {
	    	header('Location: orden_especial.php?id_orden_especial='.$id_orden_especial);	      
	    }   
	}
		
if(strlen($id_orden_especial) > 0)
{ 
  	$consulta  = "SELECT * FROM ordenes_especiales WHERE id_orden_especial= $id_orden_especial";
  	$resultado = mysqli_query($con,$consulta) ;
  	$linea = mysqli_fetch_array($resultado);

	$fecha = isset($linea["fecha"]) ? $linea["fecha"] : NULL;
	$num_ord = isset($linea["num_ord"]) ? $linea["num_ord"] : NULL;
	$id_finalizar = isset($linea["id_finalizar"]) ? $linea["id_finalizar"] : NULL;
	$id_estado = isset($linea["id_estado"]) ? $linea["id_estado"] : NULL;
	
    mysqli_free_result($resultado);

    $consulta1  = "SELECT * FROM has_orden_especial_cilindro WHERE id_orden_especial= $id_orden_especial";
  	$resultado1 = mysqli_query($con,$consulta1) ;
  	$linea1 = mysqli_fetch_array($resultado1);
  	if ($linea1>0) 
  	{
  		$contador1=1;
  	}
  	else
  	{
  		$contador1=0;
  	}mysqli_free_result($resultado1);
}
require_once("inc/init.php");
require_once("inc/config.ui.php");
$page_title = "Ordenes";
$page_css[] = "your_style.css";
include("inc/header.php");
$page_nav[""][""][""][""] = true;
include("inc/nav.php");

?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">	
	<div id="content">
		<section id="widget-grid" class="">
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-6">
					<div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h2>Crear Orden </h2>				
						</header>
						<div>				
						<div class="jarviswidget-editbox"></div>
							<div class="widget-body no-padding">
							<form id="checkout-form" class="smart-form" novalidate="novalidate" action="orden.php" method="POST">						
							<input type="hidden" name="id_orden_especial" id="id_orden_especial" value="<?php echo $id_orden_especial; ?>">
								<fieldset>
									<div class="row">
										<?php				
										if ($id_orden_especial>0) 
										{		
										?>
											<section class="col col-4">
												<label class="label">Fecha :</label>
												<label class="input"> 
													<input type="text" name="fecha" readonly placeholder="Fecha" value="<?php echo isset($fecha) ? $fecha : NULL; ?>">
												</label>
											</section>
										<?php				
										}		
										?>	
										<section class="col col-4">
											<label class="label">Num Orden :</label>
											<label class="input"> 
												<input type="text" name="num_ord" readonly placeholder="Numero de Orden" value="<?php echo isset($num_ord) ? $num_ord : NULL; ?>">
											</label>
										</section>
										<?php		
										if ($id_orden_especial>0) 
										{
														
											?>
												<section class="col col-4">	
													<label class="label">&nbsp</label>
													<button type="button" class="btn btn-primary" id="button_tr">Agregar Cilindros </button>
												</section>
											<?php				
											
										}			
										?>	
									</div>																
								</fieldset>
								<?php
								if (in_array(16, $acc))
								{
								?>
									<footer>										
										<?php				
											if ($id_finalizar==1) 
											{			
											?>
												<input type="button" onclick="validar()" value="Cerrar Orden" class="btn btn-danger btn-lg" >
											<?php				
											}											
										?>	
									</footer>
								<?php
								}										
								?>
							</form>		
							</div>						
						</div>					
					</div>				
				</article>
				<?php				
				if ($id_orden_especial>0) 
				{		
				?>
				<article class="col-sm-12 col-md-12 col-lg-6">
					<div class="jarviswidget" id="wid-id-3" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h2>Orden </h2>				
						</header>
						<div>				
						<div class="jarviswidget-editbox"></div>																	
							<table class="table table-bordered">
								<thead>
									<tr>
										<th>#</th>                                                
			                            <th>Mezcla</th>
			                            <th>Envase</th>
			                            <th>Cantidad</th>
				                        <th colspan="2">Acción</th>
									</tr>
								</thead>
								<tbody>													
								  <?php
			                          $contador = "0";	                
			                          $consulta = "SELECT * FROM has_orden_especial_cilindro WHERE id_orden_especial=".$id_orden_especial;
			                          $resultado = mysqli_query($con,$consulta) ;
			                          while ($linea = mysqli_fetch_array($resultado))
			                          {
			                            $contador = $contador + 1;
			                            $id_has_orden_especial_cili = $linea["id_has_orden_especial_cili"];
			                            $id_orden_especial = $linea["id_orden_especial"];
			                            $id_tipo_cilindro = $linea["id_tipo_cilindro"];
			                            $id_tipo_envace = $linea["id_tipo_envace"];
			                            $cantidad = $linea["cantidad"];
			                            $obs_orden = $linea["obs_orden"];
			                            $kg = $linea["kg"];	

			                            $consulta1 = "SELECT * FROM tipo_cilindro WHERE id_tipo_cilindro = $id_tipo_cilindro";
			                            $resultado1 = mysqli_query($con,$consulta1) ;
			                            while ($linea1 = mysqli_fetch_array($resultado1))
			                            {
			                            	$tipo_cili = $linea1["tipo_cili"];
			                            }
			                            $consulta2 = "SELECT * FROM tipo_envace WHERE id_tipo_envace = $id_tipo_envace";
			                            $resultado2 = mysqli_query($con,$consulta2) ;
			                            while ($linea2 = mysqli_fetch_array($resultado2))
			                            {
			                            	$tipo_envace = $linea2["tipo"];
			                            }		                            
			                            ?>
			                            <tr class="odd gradeX">
			                              	<td width="5"><?php echo $contador; ?></td>                                  
			                              	<td width=""><?php echo $tipo_cili; ?></td>  
			                              	<td width=""><?php echo $tipo_envace; ?></td>  
			                              	<td width=""><?php echo $cantidad; ?></td>  
			                              	<td width="100" style="vertical-align:bottom;">		                              	
			                              		
			                              		<img src="img/edit.png" width="35" height="35" onClick="if(confirm('Esta seguro que desea Editar ?'))editar_orden(<?php echo $id_has_orden_cili; ?>)"/>			                              
			                              	</td>
			                              	<?php				
											if ($id_finalizar==1) 
											{			
											?>
			                              	<td width="100" style="vertical-align:bottom;">		                              	
			                              		
			                              		<img src="img/eliminar.png" width="35" height="35" onClick="if(confirm('Esta seguro que desea Eliminar ?'))document.location='eliminar_has_orden_cili.php?id_has_orden_cili=<?php echo $id_has_orden_cili; ?>&id_orden=<?php echo $id_orden_especial; ?>'"/>			                              
			                              	</td> 
			                              	<?php				
											}											
											?>

			                            </tr>                           
			                            <?php
			                            }mysqli_free_result($resultado);	                                            
		                  		    ?>
								</tbody>
							</table>												
						</div>					
					</div>				
				</article>
				<?php
				}
				?>				
			</div>
		</section>
		

		<div class="modal fade" id="modal_orden" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog" >
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							&times;
						</button>
						<h4 class="modal-title" id="myModalLabel">Crear </h4>
					</div>
					<div class="modal-body">			
						<div class="row">
							<form id="wizard-1" novalidate="novalidate" name="form1" method="POST" action="orden_especial.php">
							<input type="hidden" name="id_orden_especial" id="id_orden_especial" value="<?php echo $id_orden_especial; ?>">
								<div id="bootstrap-wizard-1" class="col-sm-12">
									<div class="tab-content">
										<div class="tab-pane active" id="tab1">
											<div class="row">
												<div class="col-sm-6">
													<div class="form-group">
														<label for="category">Tipo Gas :</label>
														<div class="input-group">
															<span class="input-group-addon"></span>
															<?php
							                                $consulta6 ="SELECT * 
							                                             FROM tipo_cilindro WHERE id_tipo_cilindro >= 5";
							                                $resultado6 = mysqli_query($con,$consulta6) ;                               
							                                echo "<select class='form-control input-lg' name='id_tipo_cilindro1' onChange='cilindro(this.value)' required> ";
							                                echo "<option value='0' selected >Seleccione...</option>";
							                                while($linea6 = mysqli_fetch_array($resultado6))
							                                {

							                                  $id_tipo_cilindro = $linea6['id_tipo_cilindro'];
							                                  $tipo_cili = $linea6['tipo_cili'];
							                                  if ($id_tipo_cilindro==$id_tipo_cilindro1)
							                                  {
							                                      echo "<option value='$id_tipo_cilindro'>$tipo_cili</option>"; 
							                                  }
							                                  else 
							                                  {
							                                      echo "<option value='$id_tipo_cilindro'>$tipo_cili</option>"; 
							                                  } 
							                                }//fin while 
							                                echo "</select>";
							                                ?>
							                                															
														</div>
													</div>
												</div>
												<div class="col-sm-6">
													<div class="form-group">
														<label for="category">Capacidad :</label>
														<div class="input-group">
															<span class="input-group-addon"></span>
															<?php
							                                $consulta6 ="SELECT * 
							                                             FROM tipo_envace WHERE id_tipo_cilindro >= 5";
							                                $resultado6 = mysqli_query($con,$consulta6) ;                               
							                                echo "<select class='form-control input-lg' name='id_tipo_envace' required> ";
							                                /*while($linea6 = mysqli_fetch_array($resultado6))
							                                {
							                                  $id_tipo_cilindro = $linea6['id_tipo_cilindro'];
							                                  $tipo = $linea6['tipo'];
							                                  if ($id_tipo_cilindro==$id_tipo_cilindro1)
							                                  {
							                                      echo "<option value='$id_tipo_envace' selected >$tipo</option>"; 
							                                  }
							                                  else 
							                                  {
							                                      echo "<option value='$id_tipo_envace'>$tipo</option>"; 
							                                  } 
							                                }//fin while*/ 
							                                echo "</select>";
							                                ?>										
														</div>
													</div>
												</div>										
																														
											</div>
											<div class="row">
												<div class="col-sm-6">
													<div class="form-group">
														<label for="category">Cantidad :</label>
														<div class="input-group">
															<span class="input-group-addon"></span>
															<input class="form-control input-lg"  placeholder="Cantidad" id="cantidad" name="cantidad" type="text" value="" onKeyUp='conversion()' onkeypress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" required>								
														</div>
													</div>
												</div>												
											</div>
										</div>								
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">
										Cancel
									</button>
									<input type="submit" value="Guardar" name="g_orden_movimiento" id="g_orden_movimiento" class="btn btn-primary" />
								</div>							
							</form>	
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="modal fade" id="modal_orden_editar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog" >
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							&times;
						</button>
						<h4 class="modal-title" id="myModalLabel">Crear </h4>
					</div>
					<div class="modal-body">			
						<div class="row">
							<form id="wizard-2" novalidate="novalidate" name="form4" method="POST" action="orden_especial.php">
							<input type="hidden" name="id_orden_especial1" id="id_orden_especial1" value="<?php echo $id_orden_especial; ?>">
								<div id="bootstrap-wizard-1" class="col-sm-12">
									<div class="tab-content">
										<div class="tab-pane active" id="tab1">											
											<div class="row">												
												<div class="col-sm-12">
													<div class="form-group">
														<label for="category">Observaciones :</label>
														<div class="input-group">
															<span class="input-group-addon"></span>
															<textarea class="form-control col-xs-12" rows="4" id="obs_orden_editar" name="obs_orden_editar"></textarea>
														</div>
													</div>
												</div>												
											</div>
										</div>								
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">
										Cancel
									</button>
									<input type="submit" value="Guardar" name="editar_orden_movimiento" id="editar_orden_movimiento" class="btn btn-primary" />
								</div>							
							</form>	
						</div>
					</div>
				</div>
			</div>
		</div>


	<!-- Fin Modal -->
	</div>
	<!-- END MAIN CONTENT -->
</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->
<!-- PAGE FOOTER -->
<?php
	include("inc/footer.php");
	include("inc/scripts.php"); 
?>
<script type="text/javascript">
function editar_orden (valor)
{
	//alert(valor);
	$.ajax({
	url: 'editar_orden.php',
   	type: 'POST',
   	data: 'dato='+valor,
   	})
	.done(function(resp){
		form4.obs_orden_editar.value= resp;
	  	$("#modal_orden_editar").modal(); 		  				 	
       		
    })
}
</script>
<script type="text/javascript">

function conversion()
{
		id_tipo_envace=form1.id_tipo_envace.value;	
		//id_especificacion=form1.id_especificacion1.value;
		//var id_especificacion = document.getElementById("id_especificacion1").value;
		//alert(id_especificacion);
		
		if (id_tipo_envace==4) 
		{
			valor= 25;
		}
		if (id_tipo_envace==5) 
		{
			valor= 25;
		}
		if (id_tipo_envace==6) 
		{
			valor= 25;
		}
		if (id_tipo_envace==7) 
		{
			valor= 35;
		}
		if (id_tipo_envace==8) 
		{
			valor= 1;
		}
		if (id_tipo_envace==9) 
		{
			valor= 180;
		}
		if (id_tipo_envace==12) 
		{
			valor= 60;
		}
        if (id_tipo_envace==13)
        {
            valor= 5;
        }
		if (id_tipo_envace==14) 
		{
			valor= 15;
		}
		if (id_tipo_envace==15) 
		{
			valor= 75;
		}		
		//alert(valor_espe);
		
	    form1.kg.value = ((parseInt(form1.cantidad.value))*valor);
}
</script>
<script type="text/javascript">
function validar()
{ 
	var r = confirm("¿Esta seguro de finalizar la orden?");
	if (r == true) 
	{

		$(document).ready(function()
        {
			var formulario = $("#checkout-form").serialize();
        
        	$.ajax({
    		url: 'finalizar_especiales.php',
	       	type: 'POST',
	       	data: formulario,
	       	})
    		.done(function(resp){
    		  	alert(resp);
    		  	window.location="busqueda_ordenes_especiales.php"; 		  				 	
		       		
		    })

        });
	} 
}
</script>

<script type="text/javascript">
  function cilindro(id_tipo_cilindro)
  {
    document.form1.id_tipo_envace.length=0;
    document.form1.id_tipo_envace.options[0] = new Option("Seleccione...","0","defaultSelected","");
    var indice=1;
    <?php
    $consulta = "SELECT * 
          FROM tipo_envace 
          ORDER BY tipo";
    $resultado = mysqli_query($con,$consulta);
    if(mysqli_num_rows($resultado)>0)
    {
      while($linea = mysqli_fetch_assoc($resultado))
      {
        ?>
        if(id_tipo_cilindro=='<?php echo $linea["id_tipo_cilindro"]; ?>')
        {
          document.form1.id_tipo_envace.options[indice] = new Option("<?php echo $linea["tipo"]; ?>","<?php echo $linea["id_tipo_envace"]?>");
          indice++;
        }
        <?php
      }
    }
    ?>
  }


</script>

<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>

<!-- Vector Maps Plugin: Vectormap engine, Vectormap language -->
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>

<!-- Full Calendar -->
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/jquery-form/jquery-form.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		jQuery("#button_tr").click(function(){
    		var dato;
	        $("#modal_orden").modal();
        });
	})
</script>
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="js/plugin/datatable-responsive/datatables.responsive.min.js"></script>



<script type="text/javascript">



// DO NOT REMOVE : GLOBAL FUNCTIONS!

$(document).ready(function() {
	
	/* // DOM Position key index //
		
	l - Length changing (dropdown)
	f - Filtering input (search)
	t - The Table! (datatable)
	i - Information (records)
	p - Pagination (paging)
	r - pRocessing 
	< and > - div elements
	<"#id" and > - div with an id
	<"class" and > - div with a class
	<"#id.class" and > - div with an id and class
	
	Also see: http://legacy.datatables.net/usage/features
	*/	

	/* BASIC ;*/
		var responsiveHelper_dt_basic = undefined;
		var responsiveHelper_datatable_fixed_column = undefined;
		var responsiveHelper_datatable_col_reorder = undefined;
		var responsiveHelper_datatable_tabletools = undefined;
		
		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};

		$('#dt_basic').dataTable({
			"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
			"autoWidth" : true,
			"preDrawCallback" : function() {
				// Initialize the responsive datatables helper once.
				if (!responsiveHelper_dt_basic) {
					responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
				}
			},
			"rowCallback" : function(nRow) {
				responsiveHelper_dt_basic.createExpandIcon(nRow);
			},
			"drawCallback" : function(oSettings) {
				responsiveHelper_dt_basic.respond();
			}
		});

	/* END BASIC */
	
	/* COLUMN FILTER  */
    var otable = $('#datatable_fixed_column').DataTable({
    	//"bFilter": false,
    	//"bInfo": false,
    	//"bLengthChange": false
    	//"bAutoWidth": false,
    	//"bPaginate": false,
    	//"bStateSave": true // saves sort state using localStorage
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_fixed_column) {
				responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_fixed_column.respond();
		}		
	
    });
    
    // custom toolbar
    $("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');
    	   
    // Apply the filter
    $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {
    	
        otable
            .column( $(this).parent().index()+':visible' )
            .search( this.value )
            .draw();
            
    } );
    /* END COLUMN FILTER */   

	/* COLUMN SHOW - HIDE */
	$('#datatable_col_reorder').dataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_col_reorder) {
				responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_col_reorder.respond();
		}			
	});
	
	/* END COLUMN SHOW - HIDE */

	/* TABLETOOLS */
	$('#datatable_tabletools').dataTable({
		
		// Tabletools options: 
		//   https://datatables.net/extensions/tabletools/button_options
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        "oTableTools": {
        	 "aButtons": [
             "copy",
             "csv",
             "xls",
                {
                    "sExtends": "pdf",
                    "sTitle": "SmartAdmin_PDF",
                    "sPdfMessage": "SmartAdmin PDF Export",
                    "sPdfSize": "letter"
                },
             	{
                	"sExtends": "print",
                	"sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>"
            	}
             ],
            "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
        },
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_tabletools) {
				responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_tabletools.respond();
		}
	});
	
	/* END TABLETOOLS */

})

</script>

<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script type="text/javascript">

 
		
		$(document).ready(function() {
			
			// menu
			$("#menu").menu();
		
			/*
			 * AUTO COMPLETE AJAX
			 */
		
			function log(message) {
				$("<div>").text(message).prependTo("#log");
				$("#log").scrollTop(0);
			}
		
			$("#city").autocomplete({
				source : function(request, response) {
					$.ajax({
						url : "http://ws.geonames.org/searchJSON",
						dataType : "jsonp",
						data : {
							featureClass : "P",
							style : "full",
							maxRows : 12,
							name_startsWith : request.term
						},
						success : function(data) {
							response($.map(data.geonames, function(item) {
								return {
									label : item.name + (item.adminName1 ? ", " + item.adminName1 : "") + ", " + item.countryName,
									value : item.name
								}
							}));
						}
					});
				},
				minLength : 2,
				select : function(event, ui) {
					log(ui.item ? "Selected: " + ui.item.label : "Nothing selected, input was " + this.value);
				}
			});
		
			/*
			 * Spinners
			 */
			$("#spinner").spinner();
			$("#spinner-decimal").spinner({
				step : 0.01,
				numberFormat : "n"
			});
		
			$("#spinner-currency").spinner({
				min : 5,
				max : 2500,
				step : 25,
				start : 1000,
				numberFormat : "C"
			});
		
			/*
			 * CONVERT DIALOG TITLE TO HTML
			 * REF: http://stackoverflow.com/questions/14488774/using-html-in-a-dialogs-title-in-jquery-ui-1-10
			 */
			$.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
				_title : function(title) {
					if (!this.options.title) {
						title.html("&#160;");
					} else {
						title.html(this.options.title);
					}
				}
			}));
		
		
			/*
			* DIALOG SIMPLE
			*/
		
			// Dialog click
			$('#dialog_link').click(function() {
				$('#dialog_simple').dialog('open');
				return false;
		
			});
		
			$('#dialog_simple').dialog({
				autoOpen : false,
				width : 600,
				resizable : false,
				modal : true,
				title : "<div class='widget-header'><h4><i class='fa fa-warning'></i> Empty the recycle bin?</h4></div>",
				buttons : [{
					html : "<i class='fa fa-trash-o'></i>&nbsp; Delete all items",
					"class" : "btn btn-danger",
					click : function() {
						$(this).dialog("close");
					}
				}, {
					html : "<i class='fa fa-times'></i>&nbsp; Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
					}
				}]
			});
		
			/*
			* DIALOG HEADER ICON
			*/
		
			// Modal Link
			$('#modal_link').click(function() {
				$('#dialog-message').dialog('open');
				return false;
			});
		
			$("#dialog-message").dialog({
				autoOpen : false,
				modal : true,
				title : "<div class='widget-header'><h4><i class='icon-ok'></i> jQuery UI Dialog</h4></div>",
				buttons : [{
					html : "Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
					}
				}]
		
			});
		
			/*
			 * Remove focus from buttons
			 */
			$('.ui-dialog :button').blur();
		
			/*
			 * Just Tabs
			 */
		
			$('#tabs').tabs();
		
			/*
			 *  Simple tabs adding and removing
			 */
		
			$('#tabs2').tabs();
		
			// Dynamic tabs
			var tabTitle = $("#tab_title"), tabContent = $("#tab_content"), tabTemplate = "<li style='position:relative;'> <span class='air air-top-left delete-tab' style='top:7px; left:7px;'><button class='btn btn-xs font-xs btn-default hover-transparent'><i class='fa fa-times'></i></button></span></span><a href='#{href}'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; #{label}</a></li>", tabCounter = 2;
		
			var tabs = $("#tabs2").tabs();
		
			// modal dialog init: custom buttons and a "close" callback reseting the form inside
			var dialog = $("#addtab").dialog({
				autoOpen : false,
				width : 600,
				resizable : false,
				modal : true,
				buttons : [{
					html : "<i class='fa fa-times'></i>&nbsp; Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
		
					}
				}]
			});
		
			// addTab form: calls addTab function on submit and closes the dialog
			var form = dialog.find("form").submit(function(event) {
				addTab();
				dialog.dialog("close");
				
			});
		
			// actual addTab function: adds new tab using the input from the form above

			
			function addTab() 
			{
				
				var label = tabTitle.val() || "Tab " + tabCounter, id = "tabs-" + tabCounter, li = $(tabTemplate.replace(/#\{href\}/g, "#" + id).replace(/#\{label\}/g, label)), tabContentHtml = tabContent.val() || "Tab " + tabCounter + " content.";
		
				tabs.find(".ui-tabs-nav").append(li);
				tabs.append("<div id='" + id + "'><p>" + tabContentHtml + "</p></div>");
				tabs.tabs("refresh");
				tabCounter++;
		
				// clear fields
				$("#tab_title").val("");
				$("#tab_content").val("");
			}
		
			// addTab button: just opens the dialog
			$("#add_tab").button().click(function() {
				var idd = document.getElementById('num').firstChild.nodeValue;

				
				dialog.dialog("open");
			});
		
			// close icon: removing the tab on click
					
			/*
			* ACCORDION
			*/
			//jquery accordion
			
		     var accordionIcons = {
		         header: "fa fa-plus",    // custom icon class
		         activeHeader: "fa fa-minus" // custom icon class
		     };
		     
			$("#accordion").accordion({
				autoHeight : false,
				heightStyle : "content",
				collapsible : true,
				animate : 300,
				icons: accordionIcons,
				header : "h4",
			})
		
			/*
			 * PROGRESS BAR
			 */
			$("#progressbar").progressbar({
		     	value: 25,
		     	create: function( event, ui ) {
		     		$(this).removeClass("ui-corner-all").addClass('progress').find(">:first-child").removeClass("ui-corner-left").addClass('progress-bar progress-bar-success');
				}
			});			

		})

	</script>		







<script type="text/javascript">

	$(document).ready(function() 
	{

		var $checkoutForm = $('#checkout-form').validate(
		{
		// Rules for form validation
			rules : 
			 {
				nombre : 
				{
					required : true
				},
				nit : 
				{
					required : true
				},
				telefono_fijo : 
				{
					required : true				
				},
		        correo : 
		        {
		          required : true,
		          email : true
		          
		        },
		        direccion : 
		        {
		          required : true
		          
		        }
			},

			// Messages for form validation
			messages : {
				nombre : {
					required : 'Ingrese el nombre de la empresa'
				},
				nit : {
					required : 'Ingrese el NIT de la empresa'
				},
		        telefono_fijo : {
		          required : 'Ingrese un numero de telefono fijo'
		        },
				correo : {
					required : 'Ingrese un correo Electronico',
					email : 'Ingrese un correo Electronico valido'
				}
				,
				direccion : {
					required : 'Ingrese una direccion'
				}
			},

			// Do not change code below
			errorPlacement : function(error, element) {
				error.insertAfter(element.parent());
			}
		});
		
		
		
				
	})

</script>


<?php 
	//include footer
	include("inc/google-analytics.php"); 

}
else
{
    header("Location:index.php");
}
?>	    	