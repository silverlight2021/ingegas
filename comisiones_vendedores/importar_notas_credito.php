<?php
session_start();
date_default_timezone_get('America/Bogota');

$estado = 1;

if($_SESSION['logged'] == 'yes'){
  require("../libraries/conexion.php");
  require('../libraries/PHPExcel/Classes/PHPExcel.php');

  /*$truncateNC = "TRUNCATE TABLE notas_credito_vendedores";
  $resulNC = mysqli_query($con, $truncateNC);*/

  if(is_array($_FILES['file']) && count($_FILES['file'])>0){

    $tmpfname = $_FILES['file']['tmp_name'];
    $leerexcel = PHPExcel_IOFactory::createReaderForFile($tmpfname);

    $excelobj = $leerexcel->load($tmpfname);

    $hoja = $excelobj->getSheet(0);
    $filas = $hoja->getHighestRow();

    $consulta1 = "SELECT mes FROM cargue_comisiones_vendedores ORDER BY id_cargue_comisiones_vendedores DESC LIMIT 1";
    $resultado1 = mysqli_query($con, $consulta1);

    $linea1 = mysqli_fetch_array($resultado1);

    $mes = $linea1["mes"];

    for($row = 1; $row <= $filas; $row++){

      $sucursal = $hoja->getCell('A'.$row)->getValue();
      $codigo = $hoja->getCell('B'.$row)->getValue();
      $nombre_cliente = $hoja->getCell('C'.$row)->getValue();
      $mercado = $hoja->getCell('D'.$row)->getValue();
      $factura = $hoja->getCell('E'.$row)->getValue();
      $valor_sin_iva = $hoja->getCell('F'.$row)->getValue();
      $valor_con_iva = $hoja->getCell('G'.$row)->getValue();
      $fecha_emision = $hoja->getCell('H'.$row)->getValue();
      $fecha_iv = date($format = "Y-m-d", PHPExcel_Shared_Date::ExcelToPHP($fecha_emision));
      $observaciones = $hoja->getCell('I'.$row)->getValue();

      if($sucursal != "" && $sucursal != "SUCURSAL"){

        $consulta = "INSERT INTO notas_credito_vendedores (sucursal, mes, valor_sin_iva, valor_con_iva, fecha_emision, codigo_cliente,
                  mercado) VALUES ('$sucursal', '$mes', '$valor_sin_iva', '$valor_con_iva', '$fecha_iv', '$codigo', '$mercado')";
      
        $resultado = mysqli_query($con, $consulta);

        if(!$resultado){
          $estado = 2;
        }
      }

    
    }

    $hoja = $excelobj->getSheet(1);
    $filas = $hoja->getHighestRow();

    for($row = 1; $row <= $filas; $row++){

      $sucursal = $hoja->getCell('A'.$row)->getValue();
      $codigo = $hoja->getCell('B'.$row)->getValue();
      $nombre_cliente = $hoja->getCell('C'.$row)->getValue();
      $mercado = $hoja->getCell('D'.$row)->getValue();
      $factura = $hoja->getCell('E'.$row)->getValue();
      $valor_sin_iva = $hoja->getCell('F'.$row)->getValue();
      $valor_con_iva = $hoja->getCell('G'.$row)->getValue();
      $fecha_emision = $hoja->getCell('H'.$row)->getValue();
      $observaciones = $hoja->getCell('I'.$row)->getValue();

      if($sucursal != "" && $sucursal != "SUCURSAL"){

        $consulta = "INSERT INTO notas_credito_vendedores (sucursal, mes, valor_sin_iva, valor_con_iva, fecha_emision, codigo_cliente,
                  mercado) VALUES ('$sucursal', '$mes', '$valor_sin_iva', '$valor_con_iva', '$fecha_emision', '$codigo', '$mercado')";
      
        $resultado = mysqli_query($con, $consulta);

        if(!$resultado){
          $estado = 2;
        }
      }

    }
  }

  if($estado == 1){
    echo "Exito";
  }else{
    echo "Error";
  }
}
?>