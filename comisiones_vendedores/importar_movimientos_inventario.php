<?php
session_start();

$estado = 1;

if($_SESSION['logged'] == 'yes'){
  require("../libraries/conexion.php");
  require('../libraries/PHPExcel/Classes/PHPExcel.php');

  if(is_array($_FILES['file']) && count($_FILES['file'])>0){

    $mes = $_POST["mes"];
    $fecha_proceso = date("Y-m-d");

    $tmpfname = $_FILES['file']['tmp_name'];
    $leerexcel = PHPExcel_IOFactory::createReaderForFile($tmpfname);

    $excelobj = $leerexcel->load($tmpfname);

    $hoja = $excelobj->getSheet(0);
    $filas = $hoja->getHighestRow();

    $consulta5 = "TRUNCATE TABLE vt_ing";
    $resultado5 = mysqli_query($con, $consulta5);

    for($row = 1; $row <= $filas; $row++){
      $fecha = $hoja->getCell('A'.$row)->getValue();
      $fecha_iv = date($format = "Y-m-d", PHPExcel_Shared_Date::ExcelToPHP($fecha));
      $empresa = $hoja->getCell('B'.$row)->getValue();
      $tipo_documento = $hoja->getCell('C'.$row)->getValue();
      $prefijo = $hoja->getCell('D'.$row)->getValue();
      $numero_documento = $hoja->getCell('E'.$row)->getValue();
      $documento_externo = $hoja->getCell('F'.$row)->getValue();
      $tercero = $hoja->getCell('G'.$row)->getValue();
      $tercero_interno = $hoja->getCell('H'.$row)->getValue();
      $nota = $hoja->getCell('I'.$row)->getValue();
      $clasificacion = $hoja->getCell('J'.$row)->getValue(); 
      $forma_pago = $hoja->getCell('K'.$row)->getValue();
      $placa = $hoja->getCell('L'.$row)->getValue();
      $codigo_producto = $hoja->getCell('M'.$row)->getValue();
      $descripcion_producto = $hoja->getCell('N'.$row)->getValue();
      $bodega = $hoja->getCell('O'.$row)->getValue();
      $cantidad = $hoja->getCell('P'.$row)->getValue();
      $unidad_medida = $hoja->getCell('Q'.$row)->getValue();
      $valor_unitario = $hoja->getCell('R'.$row)->getValue();
      $iva = $hoja->getCell('S'.$row)->getValue();
      $porc_dto = $hoja->getCell('T'.$row)->getValue();
      $nota_detalle = $hoja->getCell('U'.$row)->getValue();
      $vencimiento = $hoja->getCell('V'.$row)->getValue();
      $fecha_VEN = date($format = "Y-m-d", PHPExcel_Shared_Date::ExcelToPHP($vencimiento));
      $centro_costos = $hoja->getCell('W'.$row)->getValue();
      $numero_lote = $hoja->getCell('X'.$row)->getValue();
      $descripcion_lote = $hoja->getCell('Y'.$row)->getValue();

      $consulta = "INSERT INTO vt_ing (fecha, empresa, tipo_documento, prefijo, numero_documento, documento_externo, tercero, tercero_interno, 
                  nota, clasificacion, forma_pago, placa, codigo_producto, descripcion_producto, bodega, cantidad, unidad_medida, valor_unitario,
                  iva, porc_dto, nota_detalle, vencimiento, centro_costos, numero_lote, descripcion_lote) VALUES ('$fecha_iv', '$empresa', 
                  '$tipo_documento', '$prefijo', '$numero_documento', '$documento_externo', '$tercero', '$tercero_interno', '$nota',
                  '$clasificacion', '$forma_pago', '$placa', '$codigo_producto', '$descripcion_producto', '$bodega', '$cantidad', '$unidad_medida',
                  '$valor_unitario', '$iva', '$porc_dto', '$nota_detalle', '$fecha_VEN', '$centro_costos', '$numero_lote', '$descripcion_lote')";
      
      $resultado = mysqli_query($con, $consulta);

      if(!$resultado){
        $estado = 2;
      }

      $consulta1 = "DELETE FROM vt_ing WHERE fecha = 'Fecha' AND empresa = 'Empresa'";
      $resultado1 = mysqli_query($con, $consulta1);

      if(!$resultado1){
        $estado = 2;
      }

      $consulta2 = "SELECT id_vt_ing, valor_unitario FROM vt_ing WHERE prefijo = 'NCC' OR prefijo = 'DMC'";
      $resultado2 = mysqli_query($con, $consulta2);

      while($linea2 = mysqli_fetch_array($resultado2)){
        $id_vt_ing = $linea2["id_vt_ing"];
        $valor_unitario_N = $linea2["valor_unitario"];

        $valor_unitario = $valor_unitario_N * -1;

        $consulta3 = "UPDATE vt_ing SET valor_unitario = '$valor_unitario' WHERE id_vt_ing = $id_vt_ing";
        $resultado3 = mysqli_query($con, $consulta3);

        if(!$resultado3){
          $estado = 2;
        }
      }
    }

    $consulta4 = "INSERT INTO cargue_comisiones_vendedores (fecha, mes, id_usuario, no_archivos, estado) VALUES ('$fecha_proceso', $mes, 1, 1, 1)";
    $resultado4 = mysqli_query($con, $consulta4);

    if(!$resultado4){
      $estado = 2;
    }
  }

  if($estado == 1){
    echo "1";
  }else{
    echo "2";
  }
}
?>