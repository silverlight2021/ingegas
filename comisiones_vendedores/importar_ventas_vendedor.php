<?php
session_start();

$estado = 1;

if($_SESSION['logged'] == 'yes'){
  require("../libraries/conexion.php");
  require('../libraries/PHPExcel/Classes/PHPExcel.php');

  if(is_array($_FILES['file']) && count($_FILES['file'])>0){

    $tmpfname = $_FILES['file']['tmp_name'];
    $leerexcel = PHPExcel_IOFactory::createReaderForFile($tmpfname);

    $excelobj = $leerexcel->load($tmpfname);

    $hoja = $excelobj->getSheet(0);
    $filas = $hoja->getHighestRow();

    /*$consulta7 = "TRUNCATE TABLE vtas_vdor";
    $resultado7 = mysqli_query($con, $consulta7);*/

    for($row = 1; $row <= $filas; $row++){

      $vendedor = $hoja->getCell('A'.$row)->getValue();
      $cc = $hoja->getCell('B'.$row)->getValue();
      $cliente = $hoja->getCell('C'.$row)->getValue();
      $fecha = $hoja->getCell('D'.$row)->getValue();
      $fecha_iv = date($format = "Y-m-d", PHPExcel_Shared_Date::ExcelToPHP($fecha));
      $documento = $hoja->getCell('E'.$row)->getValue();
      $venta_bruta = $hoja->getCell('F'.$row)->getValue();
      $descuento = $hoja->getCell('G'.$row)->getValue();
      $venta_neta = $hoja->getCell('H'.$row)->getValue();
      $iva = $hoja->getCell('I'.$row)->getValue();
      $total = $hoja->getCell('J'.$row)->getValue();

      $consulta = "INSERT INTO vtas_vdor (vendedor, cc, cliente, fecha, documento, venta_bruta, descuento, venta_neta, 
                  iva, total) VALUES ('$vendedor', '$cc', '$cliente', '$fecha_iv', '$documento', '$venta_bruta', '$descuento',
                  '$venta_neta', '$iva', '$total')";
      
      $resultado = mysqli_query($con, $consulta);

      if(!$resultado){
        $estado = 2;
      }

      $consulta1 = "DELETE FROM vtas_vdor WHERE vendedor = 'Vendedor' AND cliente = 'Cliente'";
      $resultado1 = mysqli_query($con, $consulta1);

      if(!$resultado1){
        $estado = 2;
      }

      $consulta4 = "DELETE FROM vtas_vdor WHERE vendedor LIKE '%Total%'";
      $resultado4 = mysqli_query($con, $consulta4);

      if(!$resultado4){
        $estado=2;
      }

      $constante_vendedor = "";

      $consulta2 = "SELECT vendedor, id_vtas_vdor FROM vtas_vdor ORDER BY id_vtas_vdor";
      $resultado2 = mysqli_query($con, $consulta2);

      while($linea2 = mysqli_fetch_array($resultado2)){
        $vendedor1 = $linea2["vendedor"];
        $id_vtas_vdor = $linea2["id_vtas_vdor"];

        if(strlen($vendedor1)>0){
          $constante_vendedor = $vendedor1;
        }else{
          $consulta3 = "UPDATE vtas_vdor SET vendedor = '$constante_vendedor' WHERE id_vtas_vdor = $id_vtas_vdor";
          $resultado3 = mysqli_query($con, $consulta3);

          if(!$resultado3){
            $estado = 2;
          }
        }
      }

      $consulta5 = "SELECT documento, id_vtas_vdor FROM vtas_vdor WHERE documento LIKE '%FV%'";
      $resultado5 = mysqli_query($con, $consulta5);

      while($linea5 = mysqli_fetch_array($resultado5)){
        $documento1 = $linea5["documento"];
        $id_vtas_vdor1 = $linea5["id_vtas_vdor"];

        $documento1 = str_replace('FV ', '', $documento1);
        
        $consulta6 = "UPDATE vtas_vdor SET documento = '$documento1' WHERE id_vtas_vdor = $id_vtas_vdor1";
        $resultado6 = mysqli_query($con, $consulta6);

        if(!$resultado6){
          $estado = 2;
        }
      }
    }

    $consulta8 = "SELECT no_archivos, id_cargue_comisiones_vendedores FROM cargue_comisiones_vendedores ORDER BY id_cargue_comisiones_vendedores DESC LIMIT 1";
    $resultado8 = mysqli_query($con, $consulta8);

    $linea8 = mysqli_fetch_array($resultado8);

    $id_cargue_comisiones_vendedores = $linea8["id_cargue_comisiones_vendedores"];
    $no_archivos = $linea8["no_archivos"];

    $no_archivos++;

    $consulta9 = "UPDATE cargue_comisiones_vendedores SET no_archivos = $no_archivos WHERE id_cargue_comisiones_vendedores = $id_cargue_comisiones_vendedores";
    $resultado9 = mysqli_query($con, $consulta9);

    if(!$resultado9){
      $estado = 2;
    }
  }

  if($estado == 1){
    echo "Exito";
  }else{
    echo "2";
  }
}
?>