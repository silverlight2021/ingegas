<?php
session_start();
date_default_timezone_get('America/Bogota');

$estado = 1;

if($_SESSION['logged'] == 'yes'){
  require("../libraries/conexion.php");
  require('../libraries/PHPExcel/Classes/PHPExcel.php');

  if(is_array($_FILES['file']) && count($_FILES['file'])>0){

    $sucursal = $_POST["sucursal"];
    $mes = $_POST["mes"];
    $porcentaje = 0;
    $porcentaje_asignado = 0;

    $tmpfname = $_FILES['file']['tmp_name'];
    $leerexcel = PHPExcel_IOFactory::createReaderForFile($tmpfname);

    $excelobj = $leerexcel->load($tmpfname);

    $hoja = $excelobj->getSheet(0);
    $filas = $hoja->getHighestRow();

    if($sucursal == "051"){

      $hoja = $excelobj->getSheet(0);
      $filas = $hoja->getHighestRow();

      for($row = 6; $row <= $filas; $row++){
        
        $rec = $hoja->getCell('A'.$row)->getValue();
        $porc_1 = $hoja->getCell('K'.$row)->getValue();

        /*if($porc_1 != "30%"){
          $porcentaje_asignado = 1;
        }*/

        if($rec != "" && $rec != "Rec" && $rec != "RC 051 TOBERIN" && $rec != "ABRIL 2021" && $rec!="Detectores autorizados por la Sra Blanca Melendez (5 cuotas)" && $porc_1!= "1%"){
          $fecha = $hoja->getCell('B'.$row)->getValue();

          $fecha_iv = date($format = "Y-m-d", PHPExcel_Shared_Date::ExcelToPHP($fecha));

          $codigo = $hoja->getCell('C'.$row)->getValue();
          $cliente = $hoja->getCell('D'.$row)->getValue();
          $valor_aplicado = $hoja->getCell('E'.$row)->getValue();
          $base_comision = $hoja->getCell('F'.$row)->getValue();
          $tipo = $hoja->getCell('G'.$row)->getValue();
          $numero_ = $hoja->getCell('H'.$row)->getValue();
          $fecha_doc = $hoja->getCell('I'.$row)->getValue();

          $fecha_doc_iv = date($format = "Y-m-d H:i:s", PHPExcel_Shared_Date::ExcelToPHP($fecha_doc));

          $dias = $hoja->getCell('J'.$row)->getValue();
          $porc_2 = $hoja->getCell('L'.$row)->getValue();
          $porc_3 = $hoja->getCell('M'.$row)->getValue();
          $porc_4 = $hoja->getCell('N'.$row)->getValue();
          $porc_5 = $hoja->getCell('O'.$row)->getValue();
          $merdo = $hoja->getCell('P'.$row)->getValue();
          $uno_porc = $hoja->getCell('Q'.$row)->getValue();
          $trans = $hoja->getCell('R'.$row)->getValue();
          $tr_sin_iva = $hoja->getCell('S'.$row)->getValue();
          $iva = $hoja->getCell('T'.$row)->getValue();
          $total = $hoja->getCell('U'.$row)->getValue();
          $dif = $hoja->getCell('V'.$row)->getValue();
          $base_trans = $hoja->getCell('W'.$row)->getValue();
          $porcentaje = $hoja->getCell('X'.$row)->getValue();

          

          $consulta = "INSERT INTO comisiones_toberin (rec, fecha, codigo, cliente, valor_aplicado, base_comision, tipo, numero_,
                      fecha_doc, dias, porc_1, porc_2, porc_3, porc_4, porc_5, merdo, uno_porc, trans, tr_sin_iva, iva, total,
                      dif, base_trans, mes, porcentaje) VALUES ('$rec', '$fecha', '$codigo', '$cliente', '$valor_aplicado', 
                      '$base_comision', '$tipo', '$numero_', '$fecha_doc_iv', '$dias', '$porc_1', '$porc_2', '$porc_3', '$porc_4',
                      '$porc_5', '$merdo', '$uno_porc', '$trans', '$tr_sin_iva', '$iva', '$total', '$dif', '$base_trans', '$mes', '$porcentaje')";
          $resultado = mysqli_query($con, $consulta);

          if(!$resultado){
            $estado = 2;
          }

          $consulta1 = "DELETE FROM comisiones_toberin WHERE base_comision LIKE '%=(%'";
          $resultado1 = mysqli_query($con, $consulta1);

          if(!$resultado1){
            $estado = 2;
          }
        }
      }
      
      $hoja = $excelobj->getSheet(1);
      $filas = $hoja->getHighestRow();

      for($row = 6; $row <= $filas; $row++){
        
        $rec = $hoja->getCell('A'.$row)->getValue();

        if($rec != "" && $rec != "Recibo" && $rec != "RC 051 TOBERIN"){
          $fecha = $hoja->getCell('B'.$row)->getValue();

          $fecha_iv = date($format = "Y-m-d", PHPExcel_Shared_Date::ExcelToPHP($fecha));

          $codigo = $hoja->getCell('C'.$row)->getValue();
          $cliente = $hoja->getCell('D'.$row)->getValue();
          $valor_aplicado = $hoja->getCell('E'.$row)->getValue();
          $base_comision = $hoja->getCell('F'.$row)->getValue();
          $tipo = $hoja->getCell('G'.$row)->getValue();
          $numero_ = $hoja->getCell('H'.$row)->getValue();
          $fecha_doc = $hoja->getCell('I'.$row)->getValue();

          $fecha_doc_iv = date($format = "Y-m-d H:i:s", PHPExcel_Shared_Date::ExcelToPHP($fecha_doc));

          $dias = $hoja->getCell('J'.$row)->getValue();
          $porc_1 = $hoja->getCell('K'.$row)->getValue();
          $porc_2 = $hoja->getCell('L'.$row)->getValue();
          $porc_3 = $hoja->getCell('M'.$row)->getValue();
          $porc_4 = $hoja->getCell('N'.$row)->getValue();
          $porc_5 = $hoja->getCell('O'.$row)->getValue();
          $merdo = $hoja->getCell('P'.$row)->getValue();
          $uno_porc = $hoja->getCell('Q'.$row)->getValue();
          $trans = $hoja->getCell('R'.$row)->getValue();
          $tr_sin_iva = $hoja->getCell('S'.$row)->getValue();
          $iva = $hoja->getCell('T'.$row)->getValue();
          $total = $hoja->getCell('U'.$row)->getValue();
          $dif = $hoja->getCell('V'.$row)->getValue();
          $base_trans = $hoja->getCell('W'.$row)->getValue();
          $porcentaje = $hoja->getCell('X'.$row)->getValue();
          

          $consulta = "INSERT INTO comisiones_toberin (rec, fecha, codigo, cliente, valor_aplicado, base_comision, tipo, numero_,
                      fecha_doc, dias, porc_1, porc_2, porc_3, porc_4, porc_5, merdo, uno_porc, trans, tr_sin_iva, iva, total,
                      dif, base_trans, mes, porcentaje) VALUES ('$rec', '$fecha_iv', '$codigo', '$cliente', '$valor_aplicado', 
                      '$base_comision', '$tipo', '$numero_', '$fecha_doc_iv', '$dias', '$porc_1', '$porc_2', '$porc_3', '$porc_4',
                      '$porc_5', '$merdo', '$uno_porc', '$trans', '$tr_sin_iva', '$iva', '$total', '$dif', '$base_trans', '$mes', '$porcentaje')";
          $resultado = mysqli_query($con, $consulta);

          if(!$resultado){
            $estado = 2;
          }
        }
      }
    }else if($sucursal == "060"){
      $hoja = $excelobj->getSheet(0);
      $filas = $hoja->getHighestRow();

      for($row = 6; $row <= $filas; $row++){
        
        $rec = $hoja->getCell('A'.$row)->getValue();
        $porc_1 = $hoja->getCell('K'.$row)->getValue();

        if($rec != "" && $rec != "Recibo" && $rec != "RC 060 CAZUCA" && $rec != "ABRIL 2021" && $rec!="Detectores autorizados por la Sra Blanca Melendez (5 cuotas)" && $porc_1!= "1%"){
          $fecha = $hoja->getCell('B'.$row)->getValue();

          $fecha_iv = date($format = "Y-m-d", PHPExcel_Shared_Date::ExcelToPHP($fecha));

          $codigo = $hoja->getCell('C'.$row)->getValue();
          $cliente = $hoja->getCell('D'.$row)->getValue();
          $valor_aplicado = $hoja->getCell('E'.$row)->getValue();
          $base_comision = $hoja->getCell('F'.$row)->getValue();
          $tipo = $hoja->getCell('G'.$row)->getValue();
          $numero_ = $hoja->getCell('H'.$row)->getValue();
          $fecha_doc = $hoja->getCell('I'.$row)->getValue();

          $fecha_doc_iv = date($format = "Y-m-d ", PHPExcel_Shared_Date::ExcelToPHP($fecha_doc));

          $dias = $hoja->getCell('J'.$row)->getValue();
          $porc_2 = $hoja->getCell('L'.$row)->getValue();
          $porc_3 = $hoja->getCell('M'.$row)->getValue();
          $porc_4 = $hoja->getCell('N'.$row)->getValue();
          $porc_5 = $hoja->getCell('O'.$row)->getValue();
          $merdo = $hoja->getCell('P'.$row)->getValue();
          $uno_porc = $hoja->getCell('Q'.$row)->getValue();
          $trans = $hoja->getCell('R'.$row)->getValue();
          $tr_sin_iva = $hoja->getCell('S'.$row)->getValue();
          $iva = $hoja->getCell('T'.$row)->getValue();
          $total = $hoja->getCell('U'.$row)->getValue();
          $dif = $hoja->getCell('V'.$row)->getValue();
          $base_com = $hoja->getCell('W'.$row)->getValue();
          $porcentaje = $hoja->getCell('X'.$row)->getValue();

          $consulta = "INSERT INTO comisiones_cazuca (recibo, fecha, codigo, cliente, valor_aplicado_rec, base_comision, tipo_, 
                      numero, fecha_doc, dias, porc_1, porc_2, porc_3, porc_4, porc_5, merc, uno_porc, trans_con_iva, trans_sin_iva, 
                      iva, total, dif, base_com, mes, porcentaje) VALUES ('$rec', '$fecha_iv', '$codigo', '$cliente', '$valor_aplicado', 
                      '$base_comision', '$tipo', '$numero_', '$fecha_doc_iv', '$dias', '$porc_1', '$porc_2', '$porc_3', '$porc_4',
                      '$porc_5', '$merdo', '$uno_porc', '$trans', '$tr_sin_iva', '$iva', '$total', '$dif', '$base_com', '$mes', '$porcentaje')";
          $resultado = mysqli_query($con, $consulta);

          if(!$resultado){
            $estado = 2;
          }

          $consulta1 = "DELETE FROM comisiones_cazuca WHERE base_comision LIKE '%=(%'";
          $resultado1 = mysqli_query($con, $consulta1);

          if(!$resultado1){
            $estado = 2;
          }
        }
      }
      
      $hoja = $excelobj->getSheet(1);
      $filas = $hoja->getHighestRow();

      for($row = 6; $row <= $filas; $row++){
        
        $rec = $hoja->getCell('A'.$row)->getValue();
        $porc_1 = $hoja->getCell('K'.$row)->getValue();

        if($rec != "" && $rec != "Recibo" && $rec != "RC 060 CAZUCA" && $rec != "ABRIL 2021" && $rec!="Detectores autorizados por la Sra Blanca Melendez (5 cuotas)" && $porc_1!= "1%"){
          $fecha = $hoja->getCell('B'.$row)->getValue();

          $fecha_iv = date($format = "Y-m-d", PHPExcel_Shared_Date::ExcelToPHP($fecha));

          $codigo = $hoja->getCell('C'.$row)->getValue();
          $cliente = $hoja->getCell('D'.$row)->getValue();
          $valor_aplicado = $hoja->getCell('E'.$row)->getValue();
          $base_comision = $hoja->getCell('F'.$row)->getValue();
          $tipo = $hoja->getCell('G'.$row)->getValue();
          $numero_ = $hoja->getCell('H'.$row)->getValue();
          $fecha_doc = $hoja->getCell('I'.$row)->getValue();

          $fecha_doc_iv = date($format = "Y-m-d", PHPExcel_Shared_Date::ExcelToPHP($fecha_doc));

          $dias = $hoja->getCell('J'.$row)->getValue();
          $porc_2 = $hoja->getCell('L'.$row)->getValue();
          $porc_3 = $hoja->getCell('M'.$row)->getValue();
          $porc_4 = $hoja->getCell('N'.$row)->getValue();
          $porc_5 = $hoja->getCell('O'.$row)->getValue();
          $merdo = $hoja->getCell('P'.$row)->getValue();
          $uno_porc = $hoja->getCell('Q'.$row)->getValue();
          $trans = $hoja->getCell('R'.$row)->getValue();
          $tr_sin_iva = $hoja->getCell('S'.$row)->getValue();
          $iva = $hoja->getCell('T'.$row)->getValue();
          $total = $hoja->getCell('U'.$row)->getValue();
          $dif = $hoja->getCell('V'.$row)->getValue();
          $base_com = $hoja->getCell('W'.$row)->getValue();
          $porcentaje = $hoja->getCell('X'.$row)->getValue();

          $consulta = "INSERT INTO comisiones_cazuca (recibo, fecha, codigo, cliente, valor_aplicado_rec, base_comision, tipo_, numero,
                      fecha_doc, dias, porc_1, porc_2, porc_3, porc_4, porc_5, merc, uno_porc, trans_con_iva, trans_sin_iva, iva, total,
                      dif, base_com, mes, porcentaje) VALUES ('$rec', '$fecha_iv', '$codigo', '$cliente', '$valor_aplicado', 
                      '$base_comision', '$tipo', '$numero_', '$fecha_doc_iv', '$dias', '$porc_1', '$porc_2', '$porc_3', '$porc_4',
                      '$porc_5', '$merdo', '$uno_porc', '$trans', '$tr_sin_iva', '$iva', '$total', '$dif', '$base_com', '$mes', '$porcentaje')";
          $resultado = mysqli_query($con, $consulta);

          if(!$resultado){
            $estado = 2;
          }

          $consulta1 = "DELETE FROM comisiones_cazuca WHERE base_comision LIKE '%=(%'";
          $resultado1 = mysqli_query($con, $consulta1);

          if(!$resultado1){
            $estado = 2;
          }
        }
      }
    }else if($sucursal == "370"){
      $hoja = $excelobj->getSheet(0);
      $filas = $hoja->getHighestRow();

      for($row = 5; $row <= $filas; $row++){
        $rec = $hoja->getCell('A'.$row)->getValue();
        $porc_1 = $hoja->getCell('K'.$row)->getValue();

        if($rec != "" && $rec != "# Rec" && $rec != "RC 370 MANIZALES" && $rec != "ABRIL 2021" && $rec!="Detectores autorizados por la Sra Blanca Melendez (5 cuotas)" && $porc_1!= "1%"){
          $fecha = $hoja->getCell('B'.$row)->getValue();

          $fecha_iv = date($format = "Y-m-d H:i:s", PHPExcel_Shared_Date::ExcelToPHP($fecha));

          $codigo = $hoja->getCell('C'.$row)->getValue();
          $cliente = $hoja->getCell('D'.$row)->getValue();
          $valor_aplicado = $hoja->getCell('E'.$row)->getValue();
          $base_comision = $hoja->getCell('F'.$row)->getValue();
          $tipo = $hoja->getCell('G'.$row)->getValue();
          $numero_ = $hoja->getCell('H'.$row)->getValue();
          $fecha_doc = $hoja->getCell('I'.$row)->getValue();

          $fecha_doc_iv = date($format = "Y-m-d H:i:s", PHPExcel_Shared_Date::ExcelToPHP($fecha_doc));

          $dias = $hoja->getCell('J'.$row)->getValue();
          $porc_2 = $hoja->getCell('L'.$row)->getValue();
          $porc_3 = $hoja->getCell('M'.$row)->getValue();
          $porc_4 = $hoja->getCell('N'.$row)->getValue();
          $porc_5 = $hoja->getCell('O'.$row)->getValue();
          $merdo = $hoja->getCell('P'.$row)->getValue();
          $uno_porc = $hoja->getCell('Q'.$row)->getValue();
          $trans = $hoja->getCell('R'.$row)->getValue();
          $tr_sin_iva = $hoja->getCell('S'.$row)->getValue();
          $iva = $hoja->getCell('T'.$row)->getValue();
          $total = $hoja->getCell('U'.$row)->getValue();
          $dif = $hoja->getCell('V'.$row)->getValue();
          $base_com = $hoja->getCell('W'.$row)->getValue();
          $porcentaje = $hoja->getCell('X'.$row)->getValue();

          $consulta = "INSERT INTO comisiones_manizales (rec, fecha_aplicacion_pago, codigo_cliente, cliente, valor_aplicado, 
                      base_comision, doc, no_fact, fecha_doc, dias_transcurridos, porc_1, porc_2, porc_3, porc_4, porc_5, mercado, 
                      casa_cobranza, trans, tr_iv, iva, total_con_iva, dif, base_com, mes, porcentaje) VALUES ('$rec', '$fecha', '$codigo', '$cliente', 
                      '$valor_aplicado', '$base_comision', '$tipo', '$numero_', '$fecha_doc_iv', '$dias', '$porc_1', '$porc_2', '$porc_3', 
                      '$porc_4', '$porc_5', '$merdo', '$uno_porc', '$trans', '$tr_sin_iva', '$iva', '$total', '$dif', '$base_com', '$mes', '$porcentaje')";
          $resultado = mysqli_query($con, $consulta);

          if(!$resultado){
            $estado = 2;
          }

          $consulta1 = "DELETE FROM comisiones_manizales WHERE base_comision LIKE '%=(%'";
          $resultado1 = mysqli_query($con, $consulta1);

          if(!$resultado1){
            $estado = 2;
          }
        }
      }


      $hoja = $excelobj->getSheet(1);
      $filas = $hoja->getHighestRow();

      for($row = 5; $row <= $filas; $row++){
        $rec = $hoja->getCell('A'.$row)->getValue();
        $porc_1 = $hoja->getCell('K'.$row)->getValue();

        if($rec != "" && $rec != "# Recibo" && $rec != "# Recibo de pago " && $rec != "RC 370 MANIZALES" && $rec != "ABRIL 2021" && $rec!="Detectores autorizados por la Sra Blanca Melendez (5 cuotas)" && $porc_1!= "1%"){
          $fecha = $hoja->getCell('B'.$row)->getValue();

          $fecha_iv = date($format = "Y-m-d H:i:s", PHPExcel_Shared_Date::ExcelToPHP($fecha)); 
          
          $codigo = $hoja->getCell('C'.$row)->getValue();
          $cliente = $hoja->getCell('D'.$row)->getValue();
          $valor_aplicado = $hoja->getCell('E'.$row)->getValue();
          $base_comision = $hoja->getCell('F'.$row)->getValue();
          $tipo = $hoja->getCell('G'.$row)->getValue();
          $numero_ = $hoja->getCell('H'.$row)->getValue();
          $fecha_doc = $hoja->getCell('I'.$row)->getValue();

          $fecha_doc_iv = date($format = "Y-m-d H:i:s", PHPExcel_Shared_Date::ExcelToPHP($fecha_doc));

          $dias = $hoja->getCell('J'.$row)->getValue();
          $porc_2 = $hoja->getCell('L'.$row)->getValue();
          $porc_3 = $hoja->getCell('M'.$row)->getValue();
          $porc_4 = $hoja->getCell('N'.$row)->getValue();
          $porc_5 = $hoja->getCell('O'.$row)->getValue();
          $merdo = $hoja->getCell('P'.$row)->getValue();
          $uno_porc = $hoja->getCell('Q'.$row)->getValue();
          $trans = $hoja->getCell('R'.$row)->getValue();
          $tr_sin_iva = $hoja->getCell('S'.$row)->getValue();
          $iva = $hoja->getCell('T'.$row)->getValue();
          $total = $hoja->getCell('U'.$row)->getValue();
          $dif = $hoja->getCell('V'.$row)->getValue();
          $base_com = $hoja->getCell('W'.$row)->getValue();
          $porcentaje = $hoja->getCell('X'.$row)->getValue();

          $consulta = "INSERT INTO comisiones_manizales (rec, fecha_aplicacion_pago, codigo_cliente, cliente, valor_aplicado, 
                      base_comision, doc, no_fact, fecha_doc, dias_transcurridos, porc_1, porc_2, porc_3, porc_4, porc_5, mercado, 
                      casa_cobranza, trans, tr_iv, iva, total_con_iva, dif, base_com, mes, porcentaje) VALUES ('$rec', '$fecha', '$codigo', '$cliente', 
                      '$valor_aplicado', '$base_comision', '$tipo', '$numero_', '$fecha_doc_iv', '$dias', '$porc_1', '$porc_2', '$porc_3', 
                      '$porc_4', '$porc_5', '$merdo', '$uno_porc', '$trans', '$tr_sin_iva', '$iva', '$total', '$dif', '$base_com', '$mes', '$porcentaje')";
          $resultado = mysqli_query($con, $consulta);

          if(!$resultado){
            $estado = 2;
          }

          $consulta1 = "DELETE FROM comisiones_manizales WHERE base_comision LIKE '%=(%'";
          $resultado1 = mysqli_query($con, $consulta1);

          if(!$resultado1){
            $estado = 2;
          }
        }
      }
    }else if($sucursal == "590"){
      $hoja = $excelobj->getSheet(0);
      $filas = $hoja->getHighestRow();

      for($row = 5; $row <= $filas; $row++){
        $rec = $hoja->getCell('A'.$row)->getValue();
        $porc_1 = $hoja->getCell('K'.$row)->getValue();

        if($rec != "" && $rec != "Recibo" && $rec != "RC 590 ITAGUI" && $rec != "ABRIL 2021" && $rec!="Detectores autorizados por la Sra Blanca Melendez (5 cuotas)" && $porc_1!= "1%"){
          $fecha = $hoja->getCell('B'.$row)->getValue();
          
          $fecha_iv = date($format = "Y-m-d H:i:s", PHPExcel_Shared_Date::ExcelToPHP($fecha));
          
          $codigo = $hoja->getCell('C'.$row)->getValue();
          $cliente = $hoja->getCell('D'.$row)->getValue();
          $valor_aplicado = $hoja->getCell('E'.$row)->getValue();
          $base_comision = $hoja->getCell('F'.$row)->getValue();
          $tipo = $hoja->getCell('G'.$row)->getValue();
          $numero_ = $hoja->getCell('H'.$row)->getValue();
          $fecha_doc = $hoja->getCell('I'.$row)->getValue();

          $fecha_doc_iv = date($format = "Y-m-d H:i:s", PHPExcel_Shared_Date::ExcelToPHP($fecha_doc));

          $dias = $hoja->getCell('J'.$row)->getValue();
          $porc_2 = $hoja->getCell('L'.$row)->getValue();
          $porc_3 = $hoja->getCell('M'.$row)->getValue();
          $porc_4 = $hoja->getCell('N'.$row)->getValue();
          $porc_5 = $hoja->getCell('O'.$row)->getValue();
          $merdo = $hoja->getCell('P'.$row)->getValue();
          $uno_porc = $hoja->getCell('Q'.$row)->getValue();
          $trans = $hoja->getCell('R'.$row)->getValue();
          $tr_sin_iva = $hoja->getCell('S'.$row)->getValue();
          $iva = $hoja->getCell('T'.$row)->getValue();
          $total = $hoja->getCell('U'.$row)->getValue();
          $dif = $hoja->getCell('V'.$row)->getValue();
          $base_com = $hoja->getCell('W'.$row)->getValue();
          $porcentaje = $hoja->getCell('X'.$row)->getValue();

          $consulta = "INSERT INTO comisiones_itagui (recibo, fecha, codigo, cliente, valor_aplicado_rec, base_comision, tipo_, numero_, 
                      fecha_doc, dias, porc_1, porc_2, porc_3, porc_4, porc_5, merc, pago_bta, trans_con_iva, trans_sin_iva, iva, total, dif, 
                      base_com, mes, porcentaje) VALUES ('$rec', '$fecha', '$codigo', '$cliente', '$valor_aplicado', '$base_comision', '$tipo', '$numero_', 
                      '$fecha_doc_iv', '$dias', '$porc_1', '$porc_2', '$porc_3', '$porc_4', '$porc_5', '$merdo', '$uno_porc', '$trans', 
                      '$tr_sin_iva', '$iva', '$total', '$dif', '$base_com', '$mes', '$porcentaje')";
          $resultado = mysqli_query($con, $consulta);

          if(!$resultado){
            $estado = 2;
          }

          $consulta1 = "DELETE FROM comisiones_itagui WHERE base_comision LIKE '%=(%'";
          $resultado1 = mysqli_query($con, $consulta1);

          if(!$resultado1){
            $estado = 2;
          }
        }
      }


      $hoja = $excelobj->getSheet(1);
      $filas = $hoja->getHighestRow();

      for($row = 5; $row <= $filas; $row++){
        $rec = $hoja->getCell('A'.$row)->getValue();
        $porc_1 = $hoja->getCell('K'.$row)->getValue();

        if($rec != "" && $rec != "Recibo" && $rec != "RC 590 ITAGUI" && $rec != "ABRIL 2021" && $rec!="Detectores autorizados por la Sra Blanca Melendez (5 cuotas)" && $porc_1!= "1%"){
          $fecha = $hoja->getCell('B'.$row)->getValue();

          $fecha_iv = date($format = "Y-m-d H:i:s", PHPExcel_Shared_Date::ExcelToPHP($fecha));

          $codigo = $hoja->getCell('C'.$row)->getValue();
          $cliente = $hoja->getCell('D'.$row)->getValue();
          $valor_aplicado = $hoja->getCell('E'.$row)->getValue();
          $base_comision = $hoja->getCell('F'.$row)->getValue();
          $tipo = $hoja->getCell('G'.$row)->getValue();
          $numero_ = $hoja->getCell('H'.$row)->getValue();
          $fecha_doc = $hoja->getCell('I'.$row)->getValue();

          $fecha_doc_iv = date($format = "Y-m-d H:i:s", PHPExcel_Shared_Date::ExcelToPHP($fecha_doc));

          $dias = $hoja->getCell('J'.$row)->getValue();
          $porc_2 = $hoja->getCell('L'.$row)->getValue();
          $porc_3 = $hoja->getCell('M'.$row)->getValue();
          $porc_4 = $hoja->getCell('N'.$row)->getValue();
          $porc_5 = $hoja->getCell('O'.$row)->getValue();
          $merdo = $hoja->getCell('P'.$row)->getValue();
          $uno_porc = $hoja->getCell('Q'.$row)->getValue();
          $trans = $hoja->getCell('R'.$row)->getValue();
          $tr_sin_iva = $hoja->getCell('S'.$row)->getValue();
          $iva = $hoja->getCell('T'.$row)->getValue();
          $total = $hoja->getCell('U'.$row)->getValue();
          $dif = $hoja->getCell('V'.$row)->getValue();
          $base_com = $hoja->getCell('W'.$row)->getValue();
          $porcentaje = $hoja->getCell('X'.$row)->getValue();

          $consulta = "INSERT INTO comisiones_itagui (recibo, fecha, codigo, cliente, valor_aplicado_rec, base_comision, tipo_, numero_, 
                      fecha_doc, dias, porc_1, porc_2, porc_3, porc_4, porc_5, merc, pago_bta, trans_con_iva, trans_sin_iva, iva, total, dif, 
                      base_com, mes, porcentaje) VALUES ('$rec', '$fecha', '$codigo', '$cliente', '$valor_aplicado', '$base_comision', '$tipo', '$numero_', 
                      '$fecha_doc_iv', '$dias', '$porc_1', '$porc_2', '$porc_3', '$porc_4', '$porc_5', '$merdo', '$uno_porc', '$trans', 
                      '$tr_sin_iva', '$iva', '$total', '$dif', '$base_com', '$mes', '$porcentaje')";
          $resultado = mysqli_query($con, $consulta);

          if(!$resultado){
            $estado = 2;
          }

          $consulta1 = "DELETE FROM comisiones_itagui WHERE base_comision LIKE '%=(%'";
          $resultado1 = mysqli_query($con, $consulta1);

          if(!$resultado1){
            $estado = 2;
          }
        }
      }
    }
    
  }

  if($estado == 1){
    echo "1";
  }else{
    echo "2";
  }
}
?>