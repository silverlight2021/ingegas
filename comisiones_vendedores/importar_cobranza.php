<?php
session_start();
date_default_timezone_set("America/Bogota");

$estado = 1;

if($_SESSION['logged'] == 'yes'){
  require("../libraries/conexion.php");
  require('../libraries/PHPExcel/Classes/PHPExcel.php');

  $truncateNC = "TRUNCATE TABLE cobranza";
  $resulNC = mysqli_query($con, $truncateNC);

  if(is_array($_FILES['file']) && count($_FILES['file'])>0){

    $tmpfname = $_FILES['file']['tmp_name'];
    $leerexcel = PHPExcel_IOFactory::createReaderForFile($tmpfname);

    $excelobj = $leerexcel->load($tmpfname);

    $hoja = $excelobj->getSheet(0);
    $filas = $hoja->getHighestRow();

    for($row = 4; $row <= $filas; $row++){
      $Empresa = $hoja->getCell('A'.$row)->getValue();
      $Vendedor = $hoja->getCell('B'.$row)->getValue();
      $Cliente = $hoja->getCell('C'.$row)->getValue();
      $Sucursal = $hoja->getCell('D'.$row)->getValue();
      $rc = $hoja->getCell('E'.$row)->getValue();
      $Fecha = $hoja->getCell('F'.$row)->getValue();
      $fecha_iv = date($format = "Y-m-d", PHPExcel_Shared_Date::ExcelToPHP($Fecha));
      $Factura = $hoja->getCell('G'.$row)->getValue();
      $Recaudo = $hoja->getCell('H'.$row)->getValue();
      $mes = date("m")-1;

      $consulta = "INSERT INTO cobranza(empresa, vendedor, cliente, sucursal, rc, fecha, factura, recaudo, mes) VALUES ('$Empresa', 
                  '$Vendedor', '$Cliente', '$Sucursal', '$rc', '$fecha_iv', '$Factura', '$Recaudo', '$mes')";

      $resultado = mysqli_query($con, $consulta);

      if(!$resultado){
        $estado = 2;
      }

    }
  }

  if($estado == 1){
    echo "Exito";
  }else{
    echo "Error";
  }

  
}
?>