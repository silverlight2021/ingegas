<?php
// William Aguirre Carpeta
// PDF HOJA DE RUTA INGEGAS
header('Content-Type: text/html; charset=UTF-8');
ini_set("session.cookie_lifetime", 7000);
		ini_set("session.gc_maxlifetime", 7000);
		//ini_set("session.save_path","/tmp");
		session_cache_expire(7000);
session_start();
date_default_timezone_set("America/Bogota");
require ("libraries/conexion.php");
require('libraries/fpdf/fpdf.php');

////////////////VARIABLES POST///////////////
$idHoja_ruta = isset($_REQUEST['idHoja_ruta']) ? $_REQUEST['idHoja_ruta'] : NULL;
/////////////////////////////////////////////


	$consulta_3 = "SELECT * 
		           FROM hoja_ruta 
		           WHERE idHoja_ruta = '".$idHoja_ruta."'";
	$resultado_3 = mysqli_query($con,$consulta_3);
	if(mysqli_num_rows($resultado_3) > 0)
	{
		$linea_3 = mysqli_fetch_assoc($resultado_3);			
		$fecha = isset($linea_3['fecha']) ? $linea_3['fecha'] : NULL;
		$placa = isset($linea_3['placa']) ? $linea_3['placa'] : NULL;
		$salida = isset($linea_3['salida']) ? $linea_3['salida'] : NULL;
		$llegada = isset($linea_3['llegada']) ? $linea_3['llegada'] : NULL;
		$odometro_salida = isset($linea_3['odometro_salida']) ? $linea_3['odometro_salida'] : NULL;
		$odometro_llegada = isset($linea_3['odometro_llegada']) ? $linea_3['odometro_llegada'] : NULL;
		$recorrido = isset($linea_3['recorrido']) ? $linea_3['recorrido'] : NULL;
		$User_idUser_conductor = isset($linea_3['User_idUser_conductor']) ? $linea_3['User_idUser_conductor'] : NULL;
		$User_idUser_ayudante = isset($linea_3['User_idUser_ayudante']) ? $linea_3['User_idUser_ayudante'] : NULL;
		$estado = isset($linea_3['estado']) ? $linea_3['estado'] : NULL;
		$total_cil_ent = isset($linea_3['total_cil_ent']) ? $linea_3['total_cil_ent'] : NULL;
		$total_cil_rec = isset($linea_3['total_cil_rec']) ? $linea_3['total_cil_rec'] : NULL;
		$total_ter_ent = isset($linea_3['total_ter_ent']) ? $linea_3['total_ter_ent'] : NULL;
		$total_ter_rec = isset($linea_3['total_ter_rec']) ? $linea_3['total_ter_rec'] : NULL;
		$total_litros = isset($linea_3['total_litros']) ? $linea_3['total_litros'] : NULL;
		$total_otros_ent = isset($linea_3['total_otros_ent']) ? $linea_3['total_otros_ent'] : NULL;
		$total_otros_rec = isset($linea_3['total_otros_rec']) ? $linea_3['total_otros_rec'] : NULL;
		$total_dinero = isset($linea_3['total_dinero']) ? $linea_3['total_dinero'] : NULL;
		$total_errores = isset($linea_3['total_errores']) ? $linea_3['total_errores'] : NULL;
		$total_clientes = isset($linea_3['total_clientes']) ? $linea_3['total_clientes'] : NULL;

		$consulta_1 =   "SELECT *
						 FROM placa_camion
						 WHERE id_placa_camion = '".$placa."'";
		$result_1 = mysqli_query($con,$consulta_1);

		if(mysqli_num_rows($result_1) > 0)
		{
			$row_1 = mysqli_fetch_assoc($result_1);
			
			$id_placa_camion = $row_1['id_placa_camion'];
			$placa_camion = $row_1['placa_camion'];			
		}mysqli_free_result($result_1);

		$consulta_2 = "SELECT Name, LastName
				       FROM user WHERE idUser = '".$User_idUser_conductor."'";
		$result_2 = mysqli_query($con,$consulta_2);
		if(mysqli_num_rows($result_2) > 0)
		{
			$linea_2 = mysqli_fetch_assoc($result_2);
			$conductor = $linea_2['Name']." ".$linea_2['LastName'];
		}mysqli_free_result($result_2);

		$consulta_3 = "SELECT Name, LastName
				   	   FROM user WHERE idUser = '".$User_idUser_ayudante."'";
		$result_3 = mysqli_query($con,$consulta_3);
		if(mysqli_num_rows($result_3) > 0)
		{
			$linea_3 = mysqli_fetch_assoc($result_3);
			$ayudante = $linea_3['Name']." ".$linea_3['LastName'];
		}mysqli_free_result($result_3);
	}

	$logotipo_empresa = "img/logo-ingegas.png";


/**
*  Clase para la maquetación de Factura
*/
class PDF extends FPDF
{
	function _Arc($x1, $y1, $x2, $y2, $x3, $y3) //Curvatura del rectángulo
	{
		$h = $this->h;
		$this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
							$x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
	}
	// private functions
	function RoundedRect($x, $y, $w, $h, $r, $style = '') //Rectángulo con bordes redondeados 
	{
		$k = $this->k;
		$hp = $this->h;
		if($style=='F')
			$op='f';
		elseif($style=='FD' || $style=='DF')
			$op='B';
		else
			$op='S';
		$MyArc = 4/3 * (sqrt(2) - 1);
		$this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));
		$xc = $x+$w-$r ;
		$yc = $y+$r;
		$this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));

		$this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);
		$xc = $x+$w-$r ;
		$yc = $y+$h-$r;
		$this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
		$this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);
		$xc = $x+$r ;
		$yc = $y+$h-$r;
		$this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
		$this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);
		$xc = $x+$r ;
		$yc = $y+$r;
		$this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
		$this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
		$this->_out($op);
	}
	// public functions
	function sizeOfText( $texte, $largeur )
	{
		$index    = 0;
		$nb_lines = 0;
		$loop     = TRUE;
		while ( $loop )
		{
			$pos = strpos($texte, "\n");
			if (!$pos)
			{
				$loop  = FALSE;
				$ligne = $texte;
			}
			else
			{
				$ligne  = substr( $texte, $index, $pos);
				$texte = substr( $texte, $pos+1 );
			}
			$length = floor( $this->GetStringWidth( $ligne ) );
			$res = 1 + floor( $length / $largeur) ;
			$nb_lines += $res;
		}
		return $nb_lines;
	}
	//ENCABEZADO DE PÁGINA
	function Header()
	{
		$x1 = 140;
		$y1 = 6;
		//Variables de encabezado
		global $logotipo_empresa, $nombre_empresa;
		global $reviso, $aprobo,$fecha_reviso,$fecha_aprobo,$codigo,$version,$pagina;

	    $this->SetFont('Arial','B',12);
		$this->SetTextColor(0,0,0);
		$this->SetLineWidth(.4);
		$this->Rect(18,18,260,24,'D');		
		//.....Logo de la compañia		
		$this->Image($logotipo_empresa,30,19,14,22);
		$this->SetXY(60,18);
		$this->Cell(35,8, utf8_decode('Revisó:'.$reviso), 1,1,'L');
		$this->SetXY(60,26);
		$this->Cell(35,8, utf8_decode('Aprobó:'.$aprobo), 1,1,'L');
		$this->Rect(60,34,35,8,'D');		
		$this->SetXY(95,18);
		$this->Cell(30,8, '21/11/2018', 1,1,'C');
		$this->SetXY(95,26);
		$this->Cell(30,8, '21/11/2018', 1,1,'C');
		$this->Rect(95,34,30,8,'D');
		$this->SetXY(125,18);
		$this->Cell(100,8, 'INGEGAS LTDA', 1,1,'C');
		$this->SetXY(125,26);
		$this->Cell(100,8, 'PROCESO "AGENCIA COMERCIAL TOBERIN"', 1,1,'C');
		$this->SetXY(125,34);
		$this->Cell(100,8, 'HOJA DE RUTA', 1,1,'C');
		$this->SetXY(225,18);
		$this->Cell(25,8, utf8_decode('CÓDIGO'), 1,1,'C');
		$this->SetXY(225,26);
		$this->Cell(25,8, utf8_decode('VERSIÓN'), 1,1,'C');
		$this->SetXY(225,34);
		$this->Cell(25,8, utf8_decode('PÁGINA'), 1,1,'C');
		$this->SetXY(250,18);
		$this->Cell(28,8, utf8_decode('ACT-PL-01'), 1,1,'C');
		$this->SetXY(250,26);
		$this->Cell(28,8, utf8_decode('02'), 1,1,'C');
		$this->SetXY(250,34);
		$this->Cell(28,8, utf8_decode('1 DE 1'), 1,1,'C');
	}
	//PIE DE PÁGINA
	function Footer()
	{
		

	}
}
$pdf = new PDF( 'L', 'mm', 'A4');
$pdf->SetAutoPageBreak(TRUE,40);
$pdf->AliasNbPages();
$pdf->AddPage();

//CONTENIDO DEL CUERPO DE LA HOJA DE RUTA

	$consulta_1 = "SELECT * 
		           FROM hoja_ruta 
		           WHERE idHoja_ruta = '".$idHoja_ruta."'";
	$resultado_1 = mysqli_query($con,$consulta_1);
	if(mysqli_num_rows($resultado_1) > 0)
	{
		$linea_1 = mysqli_fetch_assoc($resultado_1);			
		$fecha = isset($linea_1['fecha']) ? $linea_1['fecha'] : NULL;
		$placa = isset($linea_1['placa']) ? $linea_1['placa'] : NULL;
		$salida = isset($linea_1['salida']) ? $linea_1['salida'] : NULL;
		$llegada = isset($linea_1['llegada']) ? $linea_1['llegada'] : NULL;
		$odometro_salida = isset($linea_1['odometro_salida']) ? $linea_1['odometro_salida'] : NULL;
		$odometro_llegada = isset($linea_1['odometro_llegada']) ? $linea_1['odometro_llegada'] : NULL;
		$recorrido = isset($linea_1['recorrido']) ? $linea_1['recorrido'] : NULL;
		$User_idUser_conductor = isset($linea_1['User_idUser_conductor']) ? $linea_1['User_idUser_conductor'] : NULL;
		$User_idUser_ayudante = isset($linea_1['User_idUser_ayudante']) ? $linea_1['User_idUser_ayudante'] : NULL;
		$estado = isset($linea_1['estado']) ? $linea_1['estado'] : NULL;
		$total_cil_ent = isset($linea_1['total_cil_ent']) ? $linea_1['total_cil_ent'] : NULL;
		$total_cil_rec = isset($linea_1['total_cil_rec']) ? $linea_1['total_cil_rec'] : NULL;
		$total_ter_ent = isset($linea_1['total_ter_ent']) ? $linea_1['total_ter_ent'] : NULL;
		$total_ter_rec = isset($linea_1['total_ter_rec']) ? $linea_1['total_ter_rec'] : NULL;
		$total_litros = isset($linea_1['total_litros']) ? $linea_1['total_litros'] : NULL;
		$total_otros_ent = isset($linea_1['total_otros_ent']) ? $linea_1['total_otros_ent'] : NULL;
		$total_otros_rec = isset($linea_1['total_otros_rec']) ? $linea_1['total_otros_rec'] : NULL;
		$total_dinero = isset($linea_1['total_dinero']) ? $linea_1['total_dinero'] : NULL;
		$total_errores = isset($linea_1['total_errores']) ? $linea_1['total_errores'] : NULL;
		$total_clientes = isset($linea_1['total_clientes']) ? $linea_1['total_clientes'] : NULL;

		
		$consulta_2 =   "SELECT *
						 FROM placa_camion
						 WHERE id_placa_camion = '".$placa."'";
		$result_2 = mysqli_query($con,$consulta_2);

		if(mysqli_num_rows($result_2) > 0)
		{
			$row_2 = mysqli_fetch_assoc($result_2);
			
			$id_placa_camion = $row_2['id_placa_camion'];
			$placa_camion = $row_2['placa_camion'];			
		}mysqli_free_result($result_2);

		$consulta_3 = "SELECT Name, LastName
				       FROM user WHERE idUser = '".$User_idUser_conductor."'";
		$result_3 = mysqli_query($con,$consulta_3);
		if(mysqli_num_rows($result_3) > 0)
		{
			$linea_3 = mysqli_fetch_assoc($result_3);
			$conductor = $linea_3['Name']." ".$linea_3['LastName'];
		}mysqli_free_result($result_3);

		$consulta_4 = "SELECT Name, LastName
				   	   FROM user WHERE idUser = '".$User_idUser_ayudante."'";
		$result_4 = mysqli_query($con,$consulta_4);
		if(mysqli_num_rows($result_4) > 0)
		{
			$linea_4 = mysqli_fetch_assoc($result_4);
			$ayudante = $linea_4['Name']." ".$linea_4['LastName'];
		}mysqli_free_result($result_4);

		$pdf->SetXY(20,53);
		$pdf->Cell(30,5,"Fecha:",0,1);
		$pdf->SetXY(20,61);
		$pdf->Cell(30,5,$fecha,'B',1);
		
		$pdf->SetXY(60,45);
		$pdf->Cell(30,5,"Placa:",0,1);
		$pdf->SetXY(60,53);
		$pdf->Cell(30,5,"Salida:",0,1);
		$pdf->SetXY(60,61);
		$pdf->Cell(30,5,"Llegada:",0,1);
		$pdf->SetXY(80,45);
		$pdf->Cell(20,5,$placa_camion,'B',1);
		$pdf->SetXY(80,53);
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(20,5,$salida,'B',1);
		$pdf->SetXY(80,61);
		$pdf->Cell(20,5,$llegada,'B',1);
		$pdf->SetFont('Arial','B',12);
		$pdf->SetXY(120,45);
		$pdf->Cell(30,5,"Odo.Sal:",0,1);
		$pdf->SetXY(120,53);
		$pdf->Cell(30,5,"Odo.Lleg:",0,1);
		$pdf->SetXY(120,61);
		$pdf->Cell(30,5,"Recorrido:",0,1);
		$pdf->SetXY(150,45);
		$pdf->Cell(25,5,$odometro_llegada."Km",'B',1);
		$pdf->SetXY(150,53);
		$pdf->Cell(25,5,$odometro_salida."Km",'B',1);
		$pdf->SetXY(150,61);
		$pdf->Cell(25,5,$recorrido."Km",'B',1);
		
		$pdf->SetXY(180,45);
		$pdf->Cell(30,5,"Conductor:",0,1);		
		$pdf->SetXY(180,61);
		$pdf->Cell(30,5,"Ayudante:",0,1);
		$pdf->SetXY(205,45);
		$pdf->Cell(70,5,$conductor,'B',1);		
		$pdf->SetXY(205,61);
		$pdf->Cell(70,5,$ayudante,'B',1);
		//ENCABEZADO DE TABLA DECONTENIDO DE HOJA DE RUTA
		$pdf->SetXY(20,70);
		$pdf->Cell(6,10,"#",1,1,'C');
		$pdf->SetXY(26,70);
		$pdf->Cell(70,10,"NOMBRE",1,1,'C');		
		$pdf->SetXY(96,70);
		$pdf->Cell(15,10,"ODOM.",1,1,'C');
		$pdf->SetXY(111,70);
		$pdf->Cell(15,10,"FACT.",1,1,'C');
		$pdf->SetXY(126,70);
		$pdf->Cell(15,10,"CME",1,1,'C');
		$pdf->SetXY(141,70);
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(24,5,"HORA","TR",1,'C');
		$pdf->SetXY(141,75);
		$pdf->Cell(24,5,"LLEGADA","BR",1,'C');
		$pdf->SetXY(161,70);
		$pdf->Cell(28,5,"HORA","T",1,'C');
		$pdf->SetXY(161,75);
		$pdf->Cell(28,5,"SALIDA","B",1,'C');
		$pdf->SetXY(189,70);
		$pdf->Cell(21,5,"CILINDROS",1,1,'C');
		$pdf->SetXY(189,75);
		$pdf->Cell(11,5,"ENT.",1,1,'C');
		$pdf->SetXY(200,75);
		$pdf->Cell(10,5,"REC.",1,1,'C');
		$pdf->SetXY(210,70);
		$pdf->Cell(20,5,"TERMOS",1,1,'C');
		$pdf->SetXY(210,75);
		$pdf->Cell(10,5,"ENT.",1,1,'C');
		$pdf->SetXY(220,75);
		$pdf->Cell(10,5,"REC.",1,1,'C');
		$pdf->SetXY(230,70);
		$pdf->Cell(10,10,"LIN",1,1,'C');
		$pdf->SetXY(240,70);
		$pdf->Cell(20,5,"OTROS",1,1,'C');
		$pdf->SetXY(240,75);
		$pdf->Cell(10,5,"ENT.",1,1,'C');
		$pdf->SetXY(250,75);
		$pdf->Cell(10,5,"REC.",1,1,'C');
		$pdf->SetXY(260,70);
		$pdf->Cell(20,5,"DINERO",1,1,'C');
		$pdf->SetXY(260,75);
		$pdf->Cell(10,5,"VAL.",1,1,'C');
		$pdf->SetXY(270,75);
		$pdf->Cell(10,5,"E/CH",1,1,'C');

			


	}
//CONSULTA DEL CONTENIDO DE LOS RECORRIDOS DE HOJA DE RUTA
	$y = 10;
	$consulta_7 = "SELECT * FROM datos_hoja_ruta
				   WHERE idHoja_ruta = '".$idHoja_ruta."'";

	$resultado_7 = mysqli_query($con,$consulta_7);

	if(mysqli_num_rows($resultado_7)> 0)
	{
		$contador = 0;
		while($linea_7 = mysqli_fetch_assoc($resultado_7))
		{
			$contador++;
			
			$id_datos_hoja_ruta = $linea_7['id_datos_hoja_ruta'];
			$id_cliente = $linea_7['id_cliente'];
			$odometro = $linea_7['odometro'];
			$num_factura = $linea_7['num_factura'];
			$num_cme = $linea_7['num_cme'];
			$hora_llegada = $linea_7['hora_llegada'];
			$hora_salida = $linea_7['hora_salida'];
			$cili_entregado = $linea_7['cili_entregado'];
			$cili_recibido = $linea_7['cili_recibido'];
			$termo_entregado = $linea_7['termo_entregado'];
			$termo_recibido = $linea_7['termo_recibido'];
			$lin_litros = $linea_7['lin_litros'];
			$otros_item = $linea_7['otros_item'];
			$otros_entregado = $linea_7['otros_entregado'];
			$otros_recibido = $linea_7['otros_recibido'];
			$dinero_valor = $linea_7['dinero_valor'];
			$efectivo_cheque = $linea_7['efectivo_cheque'];
			$num_errores = $linea_7['num_errores'];
			$observaciones= $linea_7['observaciones'];

			$salto = '\r\n';
			$hora_llegada_f = substr($hora_llegada,0,5).substr($hora_llegada, 8);
			$hora_salida_f = substr($hora_salida, 0,5).substr($hora_salida, 8);
			$consulta_8 = "SELECT nombre 
			               FROM clientes 
			               WHERE id_cliente = '".$id_cliente."'";
			$resultado_8 = mysqli_query($con,$consulta_8);
			if(mysqli_num_rows($resultado_8) > 0)
			{
				$linea_8 = mysqli_fetch_assoc($resultado_8);
				$nombre_cliente = $linea_8['nombre'];
			}mysqli_free_result($resultado_8);


			if ($efectivo_cheque == "0")
			{
				$efectivo_cheque = "N/A";
			}
			else
			{
				$efectivo_cheque = $efectivo_cheque;	
			}

			$pdf->SetX(20);
			$pdf->Cell(6,10,$contador,1,0,'');
			$pdf->SetX(26);
			$pdf->SetFont('Arial','B',9);
			//$pdf->MultiCell(70,10,$nombre_cliente,1,'J',false);
			$pdf->Cell(70,10,substr($nombre_cliente,0, 34),1,0,'');		
			$pdf->SetFont('Arial','B',12);
			$pdf->SetX(96);
			$pdf->Cell(15,10,$odometro,1,0,'');
			$pdf->SetX(111);
			$pdf->Cell(15,10,$num_factura,1,0,'');
			$pdf->SetX(126);
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(15,10,$num_cme,1,0,'');
			$pdf->SetFont('Arial','B',12);
			$pdf->SetX(141);
			$pdf->SetFont('Arial','B',8);						
			$pdf->Cell(24,10,$hora_llegada_f,1,0,'');
			$pdf->SetX(165);
			$pdf->Cell(24,10,$hora_salida_f,1,0,'');		
			$pdf->SetX(189);
			$pdf->SetFont('Arial','B',12);	
			$pdf->Cell(11,10,$cili_entregado,1,0,'');
			$pdf->SetX(200);
			$pdf->Cell(10,10,$cili_recibido,1,0,'');
			$pdf->SetX(210);
			$pdf->Cell(10,10,$termo_entregado,1,0,'');
			$pdf->SetX(220);
			$pdf->Cell(10,10,$termo_recibido,1,0,'');
			$pdf->SetX(230);
			$pdf->Cell(10,10,$lin_litros,1,0,'');			
			$pdf->SetX(240);
			$pdf->Cell(10,10,$otros_entregado,1,0,'');
			$pdf->SetX(250);
			$pdf->Cell(10,10,$otros_recibido,1,0,'');			
			$pdf->SetX(260);
			$pdf->Cell(10,10,$total_dinero,1,0,'');
			$pdf->SetX(270);
			$pdf->Cell(10,10,substr($efectivo_cheque,0,4),1,0,'');
			
			$pdf->Ln(10);

		}
	}

//Datos de archivo de descarga
$file_name = str_replace(' ','_',$nombre_empresa_ajustado)."_Factura_No._".$num_factura.".pdf";
$file_destination = $path_pdf.$file_name;
$pdf->Output($file_destination,'F');
$pdf->Output($file_name,'I');
?>