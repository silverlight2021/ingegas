<?php
session_start();

if($_SESSION['logged'] == yes)
{
	
	$data = $_POST['data'];
	$data = explode(",", $data);
	//$expansion_total_1 = $data[0];
	$expansion_total_2 = $data[0];
	$expansion_total_3 = $data[1];

	$resultado_prueba =  $expansion_total_3 / $expansion_total_2;
	$porcentaje_prueba = $resultado_prueba * 100;
	$porcentaje_prueba =number_format((float)$porcentaje_prueba, 2, '.', '');
	?>
	<table>		
		<tr>
			<td style="font-size: 20px; padding: 15px;">EXPANSIÓN TOTAL (gr.):</td>
			<td style="font-size: 20px; padding: 15px;"><?php echo $expansion_total_2; ?></td>
		</tr>
		<tr>
			<td style="font-size: 20px; padding: 15px;">EXPANSIÓN PERMANENTE (gr.):</td>
			<td style="font-size: 20px; padding: 15px;"><?php echo $expansion_total_3; ?></td>
		</tr>
		<tr>
			<td style="font-size: 20px; padding: 15px;">RESULTADO (%):</td>
			<td style="font-size: 20px; padding: 15px;"><input type="text" name="porcentaje_prueba" id="porcentaje_prueba" class="form-control" value="<?php echo $porcentaje_prueba; ?>" readonly></td>
		</tr>
		<tr>
			<td align="center" style="font-size: 20px; padding: 15px;"><button type="button" class="btn btn-success" onclick="guardar_prueba();">GUARDAR PRUEBA</button></td>
			<td align="center" style="font-size: 20px; padding: 15px;"><button type="button" class="btn btn-danger" onclick="cancelar();">CANCELAR</button></td>
		</tr>
	</table>
	<?php
}
?>
