<?php
session_start();
date_default_timezone_set("America/Bogota");
$Fecha = date("Y-m-d");
require ("libraries/conexion.php");

$motivo_condena = "NINGUNO";
$estado_prueba_i = "APROBADO";
$prueba_realizada = 0;
if($_SESSION['logged']== 'yes')
{ 
	$id_has_movimiento_cilindro_pev = isset($_REQUEST['id_has_movimiento_cilindro_pev']) ? $_REQUEST['id_has_movimiento_cilindro_pev'] : NULL;
	$idUser =$_SESSION['su'];
	$acc = $_SESSION['acc'];

	require_once("inc/init.php");
	require_once("inc/config.ui.php");
	$page_title = "Inspección Visual Acero";
	$page_css[] = "your_style.css";
	include("inc/header.php");
	$page_nav[""][""][""][""] = true;
	include("inc/nav.php");

	$consulta0 = "SELECT * FROM has_movimiento_cilindro_pev WHERE id_has_movimiento_cilindro_pev = '$id_has_movimiento_cilindro_pev'";
	$resultado0 = mysqli_query($con,$consulta0);

	$linea0 = mysqli_fetch_assoc($resultado0);
	$num_cili = $linea0["num_cili"];

	$nva_consul = "SELECT * FROM cilindro_pev WHERE num_cili_pev = '$num_cili'";
	$resul_consul = mysqli_query($con,$nva_consul);
	$linea_consul = mysqli_fetch_assoc($resul_consul);

	$id_cili = $linea_consul["id_cilindro_pev"];

	if (isset($_POST['g_insp_visual_acero'])) //guardar información de inspección visual de acero
	{
		$estado_prueba = $_POST['estado_prueba'];
		

		if($estado_prueba == "CONDENADO"){
			$consulta_u = "UPDATE cilindro_pev SET id_estado = 15 WHERE id_cilindro_pev = '$id_cili'";
			
		}else if($estado_prueba == "RECHAZADO"){
			$consulta_u = "UPDATE cilindro_pev SET id_estado = 14 WHERE id_cilindro_pev = '$id_cili'";
			
		}else{
			$consulta_u = "UPDATE cilindro_pev SET id_estado = 5 WHERE id_cilindro_pev = '$id_cili'";
			
		}

		$id_has_movimiento_cilindro_pev = $_POST['id_has_movimiento_cilindro_pev'];
		$estado_prueba = $_POST['estado_prueba'];
		$protuberancia = $_POST['protuberancia_h'];
		$abolladuras = $_POST['abolladuras_h'];
		$abombamiento = $_POST['abombamiento_h'];
		$indentacion = $_POST['indentacion_h'];
		$corte_estria = $_POST['corte_estria_h'];
		$grieta = $_POST['grieta_h'];
		$arco_antorcha = $_POST['arco_antorcha_h'];
		$dano_fuego = $_POST['dano_fuego_h'];
		$tapon_cuello = $_POST['tapon_cuello_h'];
		$defecto_cuello = $_POST['defecto_cuello_h'];
		$estampacion = $_POST['estampacion_h'];
		$marcas_sospechosas = $_POST['marcas_sospechosas_h'];
		$estabilidad_vertical = $_POST['estabilidad_h'];
		$prueba_martillo = $_POST["prueba_martillo_h"];
		$corrosion_general = $_POST['corrosion_general_h'];
		$corrosion_local = $_POST['corrosion_local_h'];
		$corrosion_lineal = $_POST['corrosion_lineal_h'];
		$picaduras_aisladas = $_POST['picaduras_aisladas_h'];
		$picaduras_multiples = $_POST['picaduras_multiples_h'];
		$corr_hendidura = $_POST['corr_hendidura_h'];
		$motivo_condena = $_POST['motivo_condena'];
		$observaciones = $_POST['observaciones'];

		$consulta1 = "INSERT INTO insp_vi_ace_alta_pev (
							id_has_movimiento_cilindro_pev , 
							num_cili_pev ,
							estado_prueba ,
							protuberancia ,
							abolladuras ,
							abombamiento ,
							indentacion ,
							corte_estria ,
							grieta ,
							arco_antorcha ,
							fuego ,
							tapon_cuello ,
							defecto_cuello , 
							estampacion ,
							marcas ,
							estabilidad_vertical ,
							prueba_martillo , 
							corrosion_general ,
							corrosion_local ,
							corrosion_lineal ,
							picaduras_aisladas ,
							picaduras_multiples ,
							corrosion_hendidura ,
							motivo_condena,
							observaciones
							) VALUES ($id_has_movimiento_cilindro_pev, 
									'$num_cili',
									'$estado_prueba', 
									$protuberancia, 
									$abolladuras, 
									$abombamiento, 
									$indentacion, 
									$corte_estria, 
									$grieta, 
									$arco_antorcha, 
									$dano_fuego, 
									$tapon_cuello, 
									$defecto_cuello, 
									$estampacion, 
									$marcas_sospechosas, 
									$estabilidad_vertical, 
									$prueba_martillo, 
									$corrosion_general, 
									$corrosion_local, 
									$corrosion_lineal, 
									$picaduras_aisladas,  
									$picaduras_multiples, 
									$corr_hendidura, 
									'$motivo_condena',
									'$observaciones')";

		if(mysqli_query($con,$consulta1))
			{
				?>
					<script type="text/javascript">
						var id_orden_pev = '<?php echo $id_orden_pev; ?>';
						alert("Inspección guardada correctamente.")
						window.location = 'insp_vi_ace_pru_alta_presion.php?id_has_movimiento_cilindro_pev='+<?php echo $id_has_movimiento_cilindro_pev; ?>;
					</script>
				<?php
			}
			else
			{
				echo "Error: " . $consulta1 . "<br>" . mysqli_error($con);
			}
	}

	 //Consulta el responsable de la inspección
	$consulta = "SELECT Name, LastName FROM user WHERE idUser = '".$idUser."'";
	$resultado = mysqli_query($con,$consulta);
	if(mysqli_num_rows($resultado) > 0 )
	{
	  	$linea = mysqli_fetch_assoc($resultado);
		$Name = $linea['Name'];
		$LastName = $linea['LastName'];
		$responsable = $Name." ".$LastName;
	}
	 //Si existe el cilindro se consultan datos
	if(strlen($id_has_movimiento_cilindro_pev) > 0)
	{
		$consulta3 = "SELECT num_cili, id_tipo_gas_pev, id_transporte_pev
						FROM has_movimiento_cilindro_pev 
						WHERE id_has_movimiento_cilindro_pev = '".$id_has_movimiento_cilindro_pev."'";
		$resultado3 = mysqli_query($con,$consulta3);
		if(mysqli_num_rows($resultado3) > 0)
		{
			$linea3 = mysqli_fetch_assoc($resultado3);
			$num_cili = $linea3['num_cili'];
			$id_tipo_gas_pev = $linea3['id_tipo_gas_pev'];
			$id_transporte_pev = $linea3['id_transporte_pev'];

			$consulta10 = "SELECT esp_fab_pev, esp_actu_pev FROM cilindro_pev WHERE num_cili_pev = '".$num_cili."'";
			$resultado10 = mysqli_query($con,$consulta10);
			if(mysqli_num_rows($resultado10) > 0)
			{
				$linea10 = mysqli_fetch_assoc($resultado10);
				$esp_fab_pev = isset($linea10["esp_fab_pev"]) ? $linea10["esp_fab_pev"] : NULL;
				$esp_actu_pev = isset($linea10['esp_actu_pev']) ? $linea10["esp_fab_pev"] : NULL;

				if($esp_fab_pev == NULL){
					$esp_fab_pev = "DESCONOCIDO";
				}
				if($esp_actu_pev == NULL){
					$esp_actu_pev = "DESCONOCIDO";
				}
			}
			$consulta4 = "SELECT id_cliente,fecha FROM transporte_pev WHERE id_transporte_pev = '".$id_transporte_pev."'";
			$resultado4 = mysqli_query($con,$consulta4);
			if(mysqli_num_rows($resultado4) > 0)
			{
				$linea4 = mysqli_fetch_assoc($resultado4);
				$fecha = $linea4['fecha'];
				$id_cliente = $linea4['id_cliente'];

				$consulta8 = "SELECT nombre FROM clientes WHERE id_cliente = '".$id_cliente."'";
				$resultado8 = mysqli_query($con,$consulta8);
				if(mysqli_num_rows($resultado8) > 0)
				{
					$linea8 = mysqli_fetch_assoc($resultado8);
					$nombre_cliente = $linea8['nombre'];
				}
			}
			$consulta5 = "SELECT nombre FROM tipo_gas_pev WHERE id_tipo_gas_pev = '".$id_tipo_gas_pev."'";
			$resultado5 = mysqli_query($con,$consulta5);
			if(mysqli_num_rows($resultado5) > 0)
			{
				$linea5 = mysqli_fetch_assoc($resultado5);
				$nombre = $linea5['nombre'];
			}            
				
			$consulta6 = "SELECT id_especi_pev, especi_pev, num_cili_pev_2 FROM cilindro_pev WHERE num_cili_pev = '".$num_cili."'";
			$resultado6 = mysqli_query($con,$consulta6);
			if(mysqli_num_rows($resultado6) > 0)
			{
				$linea6 = mysqli_fetch_assoc($resultado6);
				$id_especi_pev = $linea6['id_especi_pev'];
				$especi_pev = $linea6['especi_pev'];
				$num_cili_pev_2 = $linea6['num_cili_pev_2'];

				$consulta7 = "SELECT especificacion FROM especificacion WHERE id_especificacion = '".$id_especi_pev."'";
				$resultado7 = mysqli_query($con,$consulta7);
				if(mysqli_num_rows($resultado7))
				{
					$linea7 = mysqli_fetch_assoc($resultado7);
					$especificacion = $linea7['especificacion'];
				}
			}
			$consulta11 = "SELECT * FROM insp_vi_ace_alta_pev WHERE id_has_movimiento_cilindro_pev = '".$id_has_movimiento_cilindro_pev."'";
			$resultado11 = mysqli_query($con,$consulta11);
			if(mysqli_num_rows($resultado11) > 0)
			{
				$prueba_realizada = 1;
				while($linea11 = mysqli_fetch_assoc($resultado11)){

					$id_has_movimiento_cilindro_pev_i = $linea11['id_has_movimiento_cilindro_pev'];
					$estado_prueba_i = $linea11['estado_prueba'];
					$protuberancia_i = $linea11['protuberancia'];
					$abolladuras_i = $linea11['abolladuras'];
					$abombamiento_i = $linea11['abombamiento'];
					$indentacion_i = $linea11['indentacion'];
					$corte_estria_i = $linea11['corte_estria'];
					$grieta_i = $linea11['grieta'];
					$arco_antorcha_i = $linea11['arco_antorcha'];
					$dano_fuego_i = $linea11['fuego'];
					$tapon_cuello_i = $linea11['tapon_cuello'];
					$defecto_cuello_i = $linea11['defecto_cuello'];
					$estampacion_i = $linea11['estampacion'];
					$marcas_sospechosas_i = $linea11['marcas'];
					$estabilidad_vertical_i = $linea11['estabilidad_vertical'];
					$prueba_martillo_i = $linea11["prueba_martillo"];
					$corrosion_general_i = $linea11['corrosion_general'];
					$corrosion_local_i = $linea11['corrosion_local'];
					$corrosion_lineal_i = $linea11['corrosion_lineal'];
					$picaduras_aisladas_i = $linea11['picaduras_aisladas'];
					$picaduras_multiples_i = $linea11['picaduras_multiples'];
					$corr_hendidura_i = $linea11['corrosion_hendidura'];
					$motivo_condena_i = $linea11['motivo_condena'];
					$observaciones = $linea11["observaciones"];

					if($estado_prueba_i == "CONDENADO"){

						$prueba_realizada = 1;
						break;
					}else if($estado_prueba_i == "RECHAZADO"){
						$prueba_realizada = 0;
					}
				}
				
			}
		}

	}
?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
	 <div id="content">
		  <section id="widget-grid" class="">
				<div class="row">
					 <article class="col-md-6 col-md-offset-3">
						  <div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
								<header>
									 <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
									 <h2>INSPECCIÓN VISUAL CILINDRO DE ACERO</h2>
								</header>
								<div>
									 <div class="jarviswidget-editbox">
									 </div>
									 <div class="widget-body no-padding">                                
										  <form name="mezcla" action="insp_vi_ace_pru_alta_presion.php" method="POST" id="mezcla">
												<input type="hidden" name="id_has_movimiento_cilindro_pev" value="<?php echo $id_has_movimiento_cilindro_pev; ?>">
												<input type="hidden" name="motivo_condena" id="motivo_condena" value="<?php echo $motivo_condena; ?>">
												<div class="well well-sm well-primary">
													 <fieldset>
													 <legend><center>IDENTIFICACIÓN DEL CILINDRO</center></legend>
														  <div class="row">
																<div class="col-md-3">
																	 <div class="form-group">
																		  <label for="category">N° Cilindro<br>Ingreso:</label>
																		  <input type="text" class="form-control" placeholder="Numero Cilindro" name="num_cili_al" readonly required value="<?php echo $num_cili; ?>" />
																	 </div>
																</div>
																<div class="col-md-3">
																	 <div class="form-group">
																		  <label for="category">N° Cilindro<br>Verificado:</label>
																		  <input type="text" class="form-control" placeholder="Numero Cilindro" name="num_cili_al_2" readonly required value="<?php echo isset($num_cili_pev_2) ? $num_cili_pev_2 : NULL; ?>"/>
																	 </div>
																</div>
																<div class="col-md-3">
																	 <div class="form-group">
																		  <label for="category">Fecha de<br> Ingreso:</label>
																		  <input type="text" class="form-control" placeholder="Fecha Ingreso" name="fecha_ingreso_al" readonly value="<?php echo isset($fecha) ? $fecha : NULL; ?>"/>                                                    </div>
																</div>
																<div class="col-md-3">
																	 <div class="form-group">
																		  <label for="category">Fecha de<br> Inspección:</label>
																		  <input type="text" class="form-control" placeholder="Fecha Inspección" name="fecha_inspeccion_al" readonly required value="<?php echo $Fecha ?>"/>
																	 </div>
																</div>
														  </div>
														  <div class="row">
																<div class="col-md-6">
																	 <div class="form-group">
																		  <label for="category">GAS:</label>
																		  <input type="text" class="form-control" placeholder="Tipo de GAS" name="gas_al" readonly required value="<?php echo isset($nombre) ? $nombre : NULL; ?>"/>
																	 </div>
																</div>
																<div class="col-md-2">
																	 <div class="form-group">
																		  <label for="category">Especificación:</label>
																		  <input type="text" class="form-control" placeholder="Especificación" name="especificacion_al" readonly value="<?php echo isset($especificacion) ? $especificacion : NULL; ?>"/>
																	 </div>
																</div>
																<div class="col-md-4">
																	 <div class="form-group">
																		<label for="category">Descripción:</label>
																		  <input type="text" class="form-control" placeholder="Especificación" name="especi_pev2" readonly value="<?php echo isset($especi_pev) ? $especi_pev : NULL; ?>"/>
																	 </div>
																</div>
														  </div>
														  <div class="row">
																<div class="col-md-6">
																	 <div class="form-group">
																		<label for="category">Aprobación / Rechazo:</label>
																		  <input type="text" class="form-control" placeholder="ESTADO" name="estado_prueba" id="estado_prueba" readonly value="<?php echo isset($estado_prueba_i) ? $estado_prueba_i : NULL; ?>" />
																	 </div>
																</div>
																<?php
																	if($prueba_realizada == 1){

																		if($estado_prueba_i != "APROBADO"){
																?>
																<div class="col-md-6">
																	 <div class="form-group">
																		  <label for="category">Causa de Condena:</label>
																		  <input type="text" class="form-control" placeholder="Cliente" name="nombre_cliente" readonly value="<?php echo isset($motivo_condena_i) ? $motivo_condena_i : NULL; ?>"/>
																	 </div>
																</div>
																<?php
																	}
																}
																?>
														  </div>
														  <div class="row">
																<div class="col-md-6">
																	 <div class="form-group">
																		<label for="category">Responsable:</label>
																		  <input type="text" class="form-control" placeholder="Responsable" name="responsable" readonly value="<?php echo isset($responsable) ? $responsable : NULL; ?>"/>
																	 </div>
																</div>
																<div class="col-md-6">
																	 <div class="form-group">
																		  <label for="category">Cliente:</label>
																		  <input type="text" class="form-control" placeholder="Cliente" name="nombre_cliente" readonly value="<?php echo isset($nombre_cliente) ? $nombre_cliente : NULL; ?>"/>
																	 </div>
																</div>
														  </div>
														  <?php
														  	if($estado_prueba_i == "CONDENADO"){
														  ?>
														  <div class="row">
																<div class="col-md-6">
																	 <div class="form-group">
																		<label for="category">Certificado Condena:</label>
																		<?php
																		$link = "<a href='pdf_certificado_pev_insp_visual.php?id_has_movimiento_cilindro_pev=".$id_has_movimiento_cilindro_pev."'>Certificado</a>";

																		echo $link;
																		?>
																	 </div>
																</div>
														  </div>
														  <?php
															}
														  ?>
													 </fieldset>
												</div>  <!-- -----------------------DATOS BASICOS------------------------------- -->
												<div class="well well-sm well-primary">
													<legend><center>INSPECCIÓN VISUAL </center></legend>
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Protuberancia: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('protuberancia')"></label>
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="protuberancia" id="protuberancia1" onclick="checkProtuberancia('1');" value="1" <?php if($protuberancia_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="protuberancia" id="protuberancia2" onclick="checkProtuberancia('0');" value="2" <?php if($protuberancia_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="protuberancia_h" id="protuberancia_h">
																	</label>
																</div>
															</div>  
														</div>															
													</div><!-- -----------------------PROTUBERANCIA------------------------------- -->
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Abolladuras: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('abolladuras')"></label>
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="abolladuras" id="abolladuras1" onclick="checkAbolladura('1')" value="1" <?php if($abolladuras_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="abolladuras" id="abolladuras2" onclick="checkAbolladura('0')" value="2" <?php if($abolladuras_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="abolladuras_h" id="abolladuras_h">
																	</label>
																</div>
															</div>  
														</div>
													</div><!-- -----------------------ABOLLADURA------------------------------- -->
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Abombamiento: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('abombamiento')"></label>
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="abombamiento" id="abombamiento1" onclick="checkAbombamiento('1');" value="1" <?php if($abombamiento_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="abombamiento" id="abombamiento2" onclick="checkAbombamiento('0');" value="2" <?php if($abombamiento_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="abombamiento_h" id="abombamiento_h">
																	</label>
																</div>
															</div>  
														</div>
													</div><!-- -----------------------ABOMBAMIENTO------------------------------- -->
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Indentacion: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('indentacion')">
																</label>                            
															
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="indentacion" id="indentacion1" onclick="checkIndentacion('1');" value="1" <?php if($indentacion_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="indentacion" id="indentacion2" onclick="checkIndentacion('0');" value="2" <?php if($indentacion_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="indentacion_h" id="indentacion_h">
																	</label>
																</div>
															</div>  
														</div>
														<div class="row" id="info_indentacion" style="display: none">
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="category"><strong>Información de la Identacion</strong></label>
																	<label class="input"> Diametro Externo del Cilindro:
																		<input type="text" name="diam_ext_cilindro" id="diam_ext_cilindro" size="9">
																	</label>
																	<label class="input">Profundidad de la Indentacion:
																		<input type="text" name="prof_indentacion" id="prof_indentacion" onchange="validar_prof_indent(this.value)" size="9">
																	</label>
																	<label class="input">Diametro de la Indentancion:
																		<input type="text" name="diam_indentacion" id="diam_indentacion" onchange="validar_diam_indent(this.value)" size="9">
																	</label>
																</div>
															</div>
														</div>
													</div><!-- -----------------------INDENTACION------------------------------- -->
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Corte o Estria: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('corte_estria')">
																</label>                            
															
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="corte_estria" id="corte_estria1" onclick="checkCortEstria('1');" value="1" <?php if($corte_estria_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="corte_estria" id="corte_estria2" onclick="checkCortEstria('0');" value="2" <?php if($corte_estria_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="corte_estria_h" id="corte_estria_h">
																	</label>
																</div>
															</div>  
														</div>
														<div class="row" id="info_cort_estria" style="display: none">
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="category"><strong>Información del Corte o Estria</strong></label>
																	<label class="input"> Espesor de Pared Minimo Garantizado:
																		<input type="text" name="espesor_min_cili" id="espesor_min_cili" size="9">
																	</label>
																	<label class="input">Espesor de Pared:
																		<input type="text" name="espesor_cili" id="espesor_cili" onchange="validar_espesor(this.value)" size="10">
																	</label>
																	<label class="input">Profundidad del Defecto:
																		<input type="text" name="prof_defect" id="prof_defect" onchange="validar_prof_corte(this.value)" size="9">
																	</label>
																	<label class="input">Diametro Externo del Cilindro:
																		<input type="text" name="diamExtCilindro" id="diamExtCilindro" size="9">
																	</label>
																	<label class="input">Longitud del Defecto:
																		<input type="text" name="long_defect" id="long_defect" onchange="validar_long_corte(this.value)" size="9">
																	</label>
																</div>
															</div>
														</div>
													</div><!-- -----------------------CORTE O ESTRIA------------------------------- -->
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Grieta: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('grieta')"></label>
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="grieta" id="grieta1" onclick="checkGrieta('1');" value="1" <?php if($grieta_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="grieta" id="grieta2" onclick="checkGrieta('0');" value="2" <?php if($grieta_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="grieta_h" id="grieta_h">
																	</label>
																</div>
															</div>  
														</div>															
													</div><!-- -----------------------GRIETA------------------------------- -->
													<div class="row">
														<div class="col-sm-6">
														  	<div class="form-group">
																<label for="category">Quemaduras por Arco y Antorcha: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('arco_antorcha')"></label>                            
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="arco_antorcha" id="arco_antorcha1" onclick="checkArcoAntorcha('1');" value="1" <?php if($arco_antorcha_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si</label>
																	<label class="radio-inline">
																		<input type="radio" name="arco_antorcha" id="arco_antorcha2" onclick="checkArcoAntorcha('0');" value="2" <?php if($arco_antorcha_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No</label>
																		<input type="hidden" name="arco_antorcha_h" id="arco_antorcha_h">
																</div>
															</div>  
														</div>
													</div><!-- -----------------------QUEMADURAS POR ARCO Y ANTORCHA------------------------------- -->
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Daño por Fuego: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('fuego')">
																</label>                            
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="dano_fuego" id="dano_fuego1" onclick="checkDanoFuego('1');" value="1" <?php if($dano_fuego_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="dano_fuego" id="dano_fuego2" onclick="checkDanoFuego('0');" value="2" <?php if($dano_fuego_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="dano_fuego_h" id="dano_fuego_h">
																	</label>
																</div>
															</div>  
														</div>
													</div><!-- -----------------------DAÑO POR FUEGO------------------------------- -->
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Insertos en Tapon o Cuello: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('tapon_cuello')">
																</label>                            
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="tapon_cuello" id="tapon_cuello1" onclick="checkTaponCuello('1');" value="1" <?php if($tapon_cuello_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?> >
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="tapon_cuello" id="tapon_cuello2" onclick="checkTaponCuello('0');" value="2" <?php if($tapon_cuello_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?> >
																		<i></i>No
																		<input type="hidden" name="tapon_cuello_h" id="tapon_cuello_h">
																	</label>
																</div>
															</div>  
														</div>
													</div><!-- -----------------------INSERTOS EN TAPON O CUELLO------------------------------- -->
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Defectos en el Cuello: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('defecto_cuello')"></label>
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="defecto_cuello" id="defecto_cuello1" onclick="checkCuello('1');" value="1" <?php if($defecto_cuello_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="defecto_cuello" id="defecto_cuello2" onclick="checkCuello('0');" value="2" <?php if($defecto_cuello_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="defecto_cuello_h" id="defecto_cuello_h">
																	</label>
																</div>
															</div>  
														</div>
														<div class="row" id="info_defecto_cuello" style="display: none">
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="category"><strong>Información del Defecto del Cuello</strong></label>
																	<label class="input"> Tipo de Rosca:
																		<select name="tipo_rosca" id="tipo_rosca">
																			<option value="0">Seleccione...</option>
																			<option value="conica">Conica</option>
																			<option value="recta">Recta</option>
																		</select>
																	</label>
																	<label class="input">Numero de Hilos:
																		<input type="text" name="no_hilos" id="no_hilos" onchange="validarHilos(this.value)" size="10">
																	</label>
																	<label class="input">Dañó por Corrosión:
																		<input type="checkbox" name="corrosion_cuello_si" value="si_corr_cuello" id="si_corr_cuello" onclick="corr_cuello()">Si
																		<input type="checkbox" name="corrosion_cuello_no" value="no_corr_cuello" id="no_corr_cuello">No
																		
																	</label>
																</div>
															</div>
														</div>
													</div><!-- -----------------------DEFECTOS EN EL CUELLO------------------------------- -->
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Estampación: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('estampacion')"></label>
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="estampacion" id="estampacion1" onclick="checkEstampacion('1');" value="1" <?php if($estampacion_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="estampacion" id="estampacion2" onclick="checkEstampacion('0');" value="2" <?php if($estampacion_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="estampacion_h" id="estampacion_h">
																	</label>
																</div>
															</div>  
														</div>
													</div><!-- -----------------------ESTAMPACION------------------------------- -->
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Marcas Sospechosas: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('marcas')"></label>
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="marcas_sospechosas" id="marcas1" onclick="checkMarcas('1');" value="1" <?php if($marcas_sospechosas_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="marcas_sospechosas" id="marcas2" onclick="checkMarcas('0');" value="2" <?php if($marcas_sospechosas_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="marcas_sospechosas_h" id="marcas_sospechosas_h">
																	</label>
																</div>
															</div>  
														</div>
													</div><!-- -----------------------MARCAS SOSPECHOSAS------------------------------- -->
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Estabilidad Vertical: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('estabilidad')"></label>
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="estabilidad_vertical" id="estabilidad1" onclick="checkEstabilidadVertical('1');" value="1" <?php if($estabilidad_vertical_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="estabilidad_vertical" id="estabilidad2" onclick="checkEstabilidadVertical('0');" value="2" <?php if($estabilidad_vertical_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="estabilidad_h" id="estabilidad_h">
																	</label>
																</div>
															</div>  
														</div>
													</div><!-- -----------------------ESTABILIDAD VERTICAL------------------------------- -->
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Prueba de Martillo (Ruido Sordo): <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('corrosion_general')">
																</label>  
															</div>
														</div>
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="prueba_martillo" id="prueba_martillo1" onclick="checkMartillo('1');" value="1" <?php if($prueba_martillo_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="prueba_martillo" id="prueba_martillo2" onclick="checkMartillo('0');" value="2" <?php if($prueba_martillo_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="prueba_martillo_h" id="prueba_martillo_h">
																	</label>
																</div>
															</div>  
														</div>
													</div><!-- -----------------------PRUEBA MARTILLO------------------------------- -->
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Corrosion General: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('corrosion_general')">
																</label>                            
															
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="corrosion_general" id="corrosion_general1" onclick="checkCorrosionGeneral('1');" value="1" <?php if($corrosion_general_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="corrosion_general" id="corrosion_general2" onclick="checkCorrosionGeneral('0');" value="2" <?php if($corrosion_general_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="corrosion_general_h" id="corrosion_general_h">
																	</label>
																</div>
															</div>  
														</div>
														<div class="row" id="info_corrosion_general" style="display: none">
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="category"><strong>Información de la Corrosion General</strong></label>
																	<label class="input"> Superficie Original del Cilindro:
																		<input type="checkbox" name="superficie_cil_si" id="superficie_cil_si" value="superficie_cil_si" onclick = "validar_superficie_corr_gene()">Si
																		<input type="checkbox" name="superficie_cil_no" id="superficie_cil_no" value="superficie_cil_no">NO
																	</label>
																	<label class="input">Espesor de Pared:
																		<input type="text" name="espesor_pared1" id="espesor_pared1" size="9">
																	</label>
																	<label class="input">Espesor Minimo Garantizado:
																		<input type="text" name="espesor_min_gar" id="espesor_min_gar" size="9" onchange="validar_espesor_corr_gene(this.value)">
																	</label>
																	<label class="input">Profundidad de Penetración:
																		<input type="text" name="prof_corr_gene" id="prof_corr_gene" onchange="validar_prof_corr_gene(this.value)" size="9">
																	</label>
																</div>
															</div>
														</div>
													</div><!-- -----------------------CORROSION GENERAL------------------------------ -->		
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Corrosion Local: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('corrosion_local')">
																</label>                            
															
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="corrosion_local" id="corrosion_local1" onclick="checkCorrosionLocal('1');" value="1" <?php if($corrosion_lineal_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="corrosion_local" id="corrosion_local2" onclick="checkCorrosionLocal('0');" value="2" <?php if($corrosion_lineal_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="corrosion_local_h" id="corrosion_local_h">
																	</label>
																</div>
															</div>  
														</div>
														<div class="row" id="info_corrosion_local" style="display: none">
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="category"><strong>Información de la Corrosion Local</strong></label>
																	<label class="input">Espesor de Pared:
																		<input type="text" name="espesor_pared2" id="espesor_pared2" size="9">
																	</label>
																	<label class="input">Espesor Minimo Garantizado:
																		<input type="text" name="espesor_min_gar2" id="espesor_min_gar2" size="9" onchange="validar_espesor_corr_local(this.value)">
																	</label>
																	<label class="input">Profundidad de Penetración:
																		<input type="text" name="prof_corr_local2" id="prof_corr_local2" onchange="validar_prof_corr_local(this.value)" size="9">
																	</label>
																</div>
															</div>
														</div>
													</div><!-- -----------------------CORROSION LOCAL------------------------------ -->
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Picadura en Cadena o Corrosión en Linea: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('picadura_cadena')">
																</label>                            
															
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="corrosion_lineal" id="corrosion_lineal1" onclick="checkCorrosionLineal('1');" value="1" <?php if($corrosion_lineal_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="corrosion_lineal" id="corrosion_lineal2" onclick="checkCorrosionLineal('0');" value="2" <?php if($corrosion_lineal_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="corrosion_lineal_h" id="corrosion_lineal_h">
																	</label>
																</div>
															</div>  
														</div>
														<div class="row" id="info_corrosion_lineal" style="display: none">
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="category"><strong>Información de la Corrosion Lineal</strong></label>
																	<label class="input">Diametro del Cilindro:
																		<input type="text" name="diam_cili_corr_lin" id="diam_cili_corr_lin" size="9">
																	</label>
																	<label class="input">Longitud Total de Corrosión:
																		<input type="text" name="long_corr_lin" id="long_corr_lin" size="9" onchange="validar_long_corr_lin(this.value)">
																	</label>
																	<label class="input">Profundidad del Defecto:
																		<input type="text" name="prof_corr_lin" id="prof_corr_lin" onchange="" size="9">
																	</label>
																	<label class="input">Espesor de Pared Original:
																		<input type="text" name="espesor_original" id="espesor_original" onchange="validar_prof_corr_lin(this.value)" size="9">
																	</label>
																	<label class="input">Espesor de Pared:
																		<input type="text" name="espesor_actual_cor_lin" id="espesor_actual_cor_lin" onchange="" size="9">
																	</label>
																	<label class="input">Espesor Minimo Garantizado:
																		<input type="text" name="espesor_min_gar_corlin" id="espesor_min_gar_corlin" onchange="validar_espesor_corr_lin(this.value)" size="9">
																	</label>
																</div>
															</div>
														</div>
													</div><!-- -----------------------CORROSION LINEAL------------------------------ -->	 
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Picaduras Aisladas: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('picaduras_aisladas')">
																</label>                            
															
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="picaduras_aisladas" id="picaduras_aisladas1" onclick="checkPicadurasAisladas('1');" value="1" <?php if($picaduras_aisladas_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="picaduras_aisladas" id="picaduras_aisladas2" onclick="checkPicadurasAisladas('0');" value="2" <?php if($picaduras_aisladas_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="picaduras_aisladas_h" id="picaduras_aisladas_h">
																	</label>
																</div>
															</div>  
														</div>
														<div class="row" id="info_picaduras_aisladas" style="display: none">
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="category"><strong>Información de las Picaduras Aisladas</strong></label>
																	<label class="input">Espesor de Pared:
																		<input type="text" name="espesor_picaduras" id="espesor_picaduras" size="9">
																	</label>
																	<label class="input">Espésor Minimo Garantizado:
																		<input type="text" name="espesor_min_picaduras" id="espesor_min_picaduras" size="9" onchange="validar_espesor_picaduras(this.value)">
																	</label>
																	<label class="input">Diametro del Defecto:
																		<input type="text" name="diametro_picaduras" id="diametro_picaduras" onchange="validar_diam_picaduras(this.value)" size="9">
																	</label>
																</div>
															</div>
														</div>
													</div><!-- -----------------------PICADURAS AISLADAS------------------------------ -->
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Grupo de Picaduras o Picaduras Multiples: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('picaduras_multiples')">
																</label>                            
															
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="picaduras_multi" id="picaduras_multi1" onclick="checkPicadurasMultiples('1');" value="1" <?php if($picaduras_multiples_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="picaduras_multi" id="picaduras_multi2" onclick="checkPicadurasMultiples('0');" value="2" <?php if($picaduras_multiples_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="picaduras_multiples_h" id="picaduras_multiples_h">
																	</label>
																</div>
															</div>  
														</div>
														<div class="row" id="info_picaduras_multiples" style="display: none">
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="category"><strong>Información de las Picaduras Multiples</strong></label>
																	<label class="input">Espesor de Pared Restante:
																		<input type="text" name="espesor_restante" id="espesor_restante" size="9">
																	</label>
																	<label class="input">Espésor Minimo de Diseño:
																		<input type="text" name="espesor_min_picaduras_multi" id="espesor_min_picaduras_multi" size="9" onchange="validar_espesor_picaduras_multi(this.value)">
																	</label>
																</div>
															</div>
														</div>
													</div><!-- -----------------------PICADURAS MULTIPLES------------------------------ -->
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Corrosión con Hendidura: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('corrosion_hendidura')">
																</label>                            
															
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="corr_hendidura" id="corr_hendidura1" onclick="checkCorrosionHendidura('1');" value="1" <?php if($corr_hendidura_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="corr_hendidura" id="corr_hendidura2" onclick="checkCorrosionHendidura('0');" value="2" <?php if($corr_hendidura_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="corr_hendidura_h" id="corr_hendidura_h">
																	</label>
																</div>
															</div>  
														</div>
														<div class="row" id="info_corr_hendidura" style="display: none">
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="category"><strong>Información de la Corrosión con Hendidura</strong></label>
																	<label class="input">Espesor de Pared Original:
																		<input type="text" name="espesor_hendidura" id="espesor_hendidura" size="9">
																	</label>
																	<label class="input">Profundidad de Penetración:
																		<input type="text" name="profundidad_penetracion" id="profundidad_penetracion" size="9" onchange="validar_espesor_hendidura(this.value)">
																	</label>
																</div>
															</div>
														</div>
													</div><!-- -----------------------CORROSION HENDIDURA------------------------------ -->
												</div>
												
												<div class="well well-sm well-primary">     
													<div class="row">
														<div class="form-group">
															<label for="category">Observaciones Generales:</label>
															<textarea class="form-control" name="observaciones"><?php echo $observaciones; ?></textarea>
														</div>
													</div>
												</div> 
												<div class="modal-footer">
													<?php
														if (in_array(22, $acc))
														{
															if($prueba_realizada != 1){
														?>
															<input type="submit" value="Guardar" name="g_insp_visual_acero" id="g_insp_visual_acero" class="btn btn-primary" />
														<?php
															}
														}                   
														?>
												</div>
											</form>
									 </div>
								</div>
						  </div>
					 </article>
				</div>
		  </section>
	 </div>
</div>
<!-- END MAIN PANEL -->
<div class="modal fade" id="modal_info_protuberancia" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">PROTUBERANCIA</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	Hinchazon visible del cilindro.
								</p>
								
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_abolladuras" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">ABOLLADURAS</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	Para un cilindro de 228,6 mm (9 pulgadas) de diámetro x 1295,4 mm (51 pulgadas) de largo, acepta abolladuras hasta de 1,575 mm (0,062 pulgadas) de profundidad, cuando el diámetro mayor de la misma es de 50,8 mm (2 pulgadas) o más.
								</p>
								
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_abombamiento" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">ABOMBAMIENTO</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	Todo cilindro con abombamiento razonablemente visible, como las causadas por incendio.
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_indentacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">INDENTACION</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	Profundidad de la indentación excede el 3% del diámetro externo del cilindro
								</p>
								<p>
									*	Diámetro de indentación sea menor a x 15 su profundidad
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_corte_estria" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">CORTE O ESTRIA </h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	La profundidad del corte o estría excede el 10% del espesor de pared.
								</p>
								<p>
									*	La longitud exceda el 25% del diámetro externo del cilindro.
								</p>
								<p>
									*	El espesor de pared sea inferior al espesor de pared mínimo garantizado.
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_grieta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">GRIETA</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	Hendidura en el metal.
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_arco_antorcha" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">QUEMADURAS POR ARCO Y ANTORCHA</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	Remoción de metal por biselado o cráteres.
								</p>
								<p>
									*	Biselado o quemadura del metal básico.
								</p>
								<p>
									*	Zona fuertemente afectada por el calor
								</p>
								<p>
									*	Depósito de metal soldado o desplazamiento del metal básico.
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_fuego" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">DAÑO POR FUEGO</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	Fusión parcial del cilindro
								</p>
								<p>
									*	Distorsión del cilindro.
								</p>
								<p>
									*	Carbonización o quemadura de la pintura.
								</p>
								<p>
									*	Daño por fuego en la válvula, derretimiento de la protección plástica o anillo de fecha o tapón fusible, si se encuentran acondicionados.
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_tapon_cuello" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">INSERTOS EN TAPON O CUELLO</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	Todos los cilindros, a menos que se establezca claramente que la adición es parte del diseño aprobado
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_defecto_cuello" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">DEFECTOS EN EL CUELLO</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	El número de hilos efectivos de la rosca del cuello se ha reducido materialmente.
								</p>
								<p>
									*	Menos de cinco (5) hilos completos de rosca para roscas cónicas en cilindros de baja presión.
								</p>
								<p>
									*	Menos de siete (7) hilos completos para rosca cónica en cilindros de alta presión.
								</p>
								<p>
									*	Menos de seis (6) hilos para todos los cilindros con rosca recta.
								</p>
								<p>
									*	Grietas visible en los hilos o áreas adyacentes. 
								</p>
								<p>
									*	Daño visible por la corrosión o de otra clase que pueda afectar la estructura integral de la rosca o de la válvula en el momento de la instalación.
								</p>
								<p>
									*	Cilindros fabricados para una especificación con un número de hilos menor a los mencionados que no mantengan sus hilos completos, continuos y sin daños.
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_estampacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">ESTAMPACIÓN</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	Todos los cilindros con marcaciones ilegibles, modificadas o incorrectas.
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_marcas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">MARCAS</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	Marcas diferentes a las formadas en el proceso de manufactura o la reparación aprobada del cilindro.
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_estabilidad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">ESTABILIDAD VERTICAL</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	La desviación de la verticalidad que pueda presentar riesgo durante el servicio (en especial si está acondicionado con anillo de base).
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_corrosion_general" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">CORROSIÓN GENERAL</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	La superficie original del metal ya no es reconocible.
								</p>
								<p>
									*	La profundidad de la penetración excede el 10% del espesor de pared original.
								</p>
								<p>
									*	El espesor de pared es inferior al espesor de pared mínimo garantizado.
								</p>
								<p>
									*	El peso de tara en el momento de la inspección es menor que el 95% del peso tara original estampado..
								</p>
								<p>
									*	El espesor de pared mínimo permisible es desconocido o el espesor de pared restante no es conocido, y la corrosión general cubre más del 20% de la superficie del cilindro.
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_corrosion_local" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">CORROSIÓN LOCAL</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
								<p>
									*	La profundidad de la penetración excede el 20% del espesor original de pared del cilindro.
								</p>
								<p>
									*	El espesor de pared es inferior al espesor de pared mínimo garantizado.
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_picadura_cadena" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">PICADURA EN CADENA O CORROSIÓN EN LINEA</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
					 			<p>
									*	La longitud total de la corrosión en cualquier dirección excede el diámetro del cilindro y la profundidad excede el 10% del espesor de pared original.
								</p>
								<p>
									*	El espesor de pared es inferior que el espesor mínimo garantizado.
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_picaduras_aisladas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">PICADURAS AISLADAS</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
					 			<p>
									*	El diámetro de las picaduras es mayor que 5 mm.
								</p>
								<p>
									*	El diámetro de las picaduras es inferior a 5 mm, se debe verificar que el espesor de pared restante no es inferior al espesor de pared mínimo garantizado
								</p>
								<p>
									*	Picaduras aisladas con profundidad mayor que un tercio (1/3) del espesor mínimo de pared de diseño permisible.
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_picaduras_multiples" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">GRUPO DE PICADURAS O PICADURAS MULTIPLES</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
					 			<p>
									*	El espesor de pared restante en la picadura más profunda debe ser por lo menos el 95% del espesor mínimo de pared de diseño permisible. De lo contrario procede a la condición.
								</p>
								<p>
									*	El diámetro de la envolvente es menor que dos (2) veces el espesor mínimo de pared de diseño permisible.
								</p>
								<p>
									*	El diámetro de la envolvente es mayor que dos (2) veces el espesor mínimo de pared de diseño permisible.
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_corrosion_hendidura" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">CORROSIÓN CON HENDIDURA</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
					 			<p>
									*	Después de limpieza cabal, la profundidad de la penetración excede el 20% del espesor de pared original.
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>
<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->
<?php
	 include("inc/footer.php");
?>
<!-- END PAGE FOOTER -->

<?php 
	 //include required scripts
	 include("inc/scripts.php"); 
?>


<!-- PAGE RELATED PLUGIN(S) 
<script src="..."></script>-->
<!-- Flot Chart Plugin: Flot Engine, Flot Resizer, Flot Tooltip -->
<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>

<!-- Vector Maps Plugin: Vectormap engine, Vectormap language -->
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>

<!-- Full Calendar -->
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/jquery-form/jquery-form.min.js"></script>

<script type="text/javascript">
function info(ventana){
	 $('#modal_info_'+ventana).modal();
}

function checkProtuberancia(valor){
  	valorCheck = valor;

  	if(valorCheck == 1){
  		
  		if(confirm("Al seleccionar esta opción el cilindro será condenado, ¿Desea continuar?")){
  			if(confirm("¿Desea dejar el cilindro en estado Rechazado para poder continuar despues?")){
  				alert('Cilindro Rechazado!');
  				document.getElementById('protuberancia1').disabled = true;
	  			document.getElementById('protuberancia2').disabled = true;
	  			document.getElementById('estado_prueba').value = 'RECHAZADO';
	  			document.getElementById('motivo_condena'). value = 'Protuberancia';
	  			document.getElementById('protuberancia_h').value = '1';
	  			inhabilitarFormulario('protuberancia1','protuberancia_h');
  			}else{
  				alert('Cilindro Condenado!');
  				document.getElementById('protuberancia1').disabled = true;
	  			document.getElementById('protuberancia2').disabled = true;
	  			document.getElementById('estado_prueba').value = 'CONDENADO';
	  			document.getElementById('motivo_condena'). value = 'Protuberancia';
	  			document.getElementById('protuberancia_h').value = '1';
	  			inhabilitarFormulario('protuberancia1','protuberancia_h');
  			}
  			
  		}else{
  			document.getElementById('protuberancia1').checked = false;
  		}
  		
  	}else if(valorCheck == 0){
  		document.getElementById('protuberancia_h').value = '2';
  	}
}

function checkAbolladura(valor){

	var valorCheck = valor;

	if(valorCheck == 1){

		if(confirm("Al seleccionar esta opción el cilindro será condenado, ¿Desea continuar?")){
			if(confirm("¿Desea dejar el cilindro en estado Rechazado para poder continuar despues?")){
  				alert('Cilindro Rechazado!');
  				document.getElementById('abolladuras1').disabled = true;
				document.getElementById('abolladuras2').disabled = true;
				document.getElementById('estado_prueba').value = 'RECHAZADO';
				document.getElementById('motivo_condena'). value = 'Abolladura';
				document.getElementById('abolladuras_h').value = '1';
				inhabilitarFormulario('abolladuras1','abolladuras_h');
  			}else{
  				alert('Cilindro Condenado!');
  				document.getElementById('abolladuras1').disabled = true;
				document.getElementById('abolladuras2').disabled = true;
				document.getElementById('estado_prueba').value = 'CONDENADO';
				document.getElementById('motivo_condena'). value = 'Abolladura';
				document.getElementById('abolladuras_h').value = '1';
				inhabilitarFormulario('abolladuras1','abolladuras_h');
  			}
			
		}else{
			document.getElementById('abolladuras1').checked  = false;
		}
	}else if(valorCheck == 0){
		
		document.getElementById('abolladuras_h').value = '2';
	}
}

function checkAbombamiento(valor){

	var valorCheck = valor;

	if(valorCheck==1){
		if(confirm("Al seleccionar esta opción el cilindro será condenado, ¿Desea continuar?")){
			if(confirm("¿Desea dejar el cilindro en estado Rechazado para poder continuar despues?")){
				alert("Cilindro Rechazado!");
				document.getElementById("abombamiento1").disabled = true;
				document.getElementById("abombamiento2").disabled = true;
				document.getElementById('estado_prueba').value = 'RECHAZADO';
				document.getElementById('motivo_condena'). value = 'abombamiento';
				document.getElementById('abombamiento_h').value = '1';
				inhabilitarFormulario('abombamiento1','abombamiento_h');
			}else{
				alert("Cilindro Condenado!");
				document.getElementById("abombamiento1").disabled = true;
				document.getElementById("abombamiento2").disabled = true;
				document.getElementById('estado_prueba').value = 'CONDENADO';
				document.getElementById('motivo_condena'). value = 'abombamiento';
				document.getElementById('abombamiento_h').value = '1';
				inhabilitarFormulario('abombamiento1','abombamiento_h');
			}
			
		}else{
			document.getElementById("abombamiento1").checked = false;
		}
	}else if (valorCheck == 0){
		document.getElementById('abombamiento_h').value = '2';
	}
}

function checkIndentacion(valor){

	valorCheck = valor;

	if(valorCheck == 1){
		document.getElementById('indentacion_h').value='1';
		document.getElementById('info_indentacion').style.display = 'block'
	}else if(valorCheck == 0){
		document.getElementById('indentacion_h').value='2';
	}
}

function validar_prof_indent(valor){
	profundidad_indentacion = valor;
	diametro_externo_cili = document.getElementById('diam_ext_cilindro').value;
	porc_indentacion = 0;

	porc_indentacion = profundidad_indentacion * 100 / diametro_externo_cili;

	if(porc_indentacion >= 3){
		if(confirm('Con estos datos, el cilindro será condenado, ¿Desea continuar?')){
			alert('Cilindro Condenado!');
			document.getElementById('prof_indentacion').readOnly = true;
			document.getElementById('diam_ext_cilindro').readOnly = true;
			document.getElementById('diam_indentacion').readOnly = true;
			document.getElementById('indentacion1').disabled = true;
			document.getElementById('indentacion2').disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
			document.getElementById('motivo_condena'). value = 'Indentancion';
			inhabilitarFormulario('indentacion1','indentacion_h');
		}else{
			document.getElementById('prof_indentacion').value = "";
		}
	}
}

function validar_diam_indent(valor){
	diametro_indetacion = valor;
	profundidad_indentacion = document.getElementById('prof_indentacion').value;
	valor_condena = profundidad_indentacion*15;

	if(valor_condena<=diametro_indetacion){
		if(confirm("Con estos valores el cilindro será condenado, ¿Desea continuar?")){
			alert('Cilindro Condenado!');
			document.getElementById('prof_indentacion').readOnly = true;
			document.getElementById('diam_ext_cilindro').readOnly = true;
			document.getElementById('diam_indentacion').readOnly = true;
			document.getElementById('indentacion1').disabled = true;
			document.getElementById('indentacion2').disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
			document.getElementById('motivo_condena'). value = 'Indentancion';
			inhabilitarFormulario('indentacion1','indentacion_h');
		}else{
			document.getElementById('diam_indentacion').value = "";	
		}
	}
}

function checkCortEstria(valor){

	valorCheck = valor;

	if(valorCheck == 1){
		document.getElementById('corte_estria_h').value = '1';
		document.getElementById('info_cort_estria').style.display = 'block';
	}else if(valorCheck == 0){
		document.getElementById('info_cort_estria').style.display = 'none';
		document.getElementById('corte_estria_h').value = '2';
	}
}

function validar_espesor(valor){

	espesor_cili = valor;
	espesor_min_cili = document.getElementById('espesor_min_cili').value;

	if(espesor_cili<espesor_min_cili){
		if(confirm("Con estos valores el cilindro será condenado, ¿Desea continuar?")){
			alert("Cilindro Condenado!");
			document.getElementById("espesor_min_cili").readOnly = true;
			document.getElementById("espesor_cili").readOnly = true;
			document.getElementById("prof_defect").readOnly = true;
			document.getElementById("long_defect").readOnly = true;
			document.getElementById("diamExtCilindro").readOnly = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
  			document.getElementById('motivo_condena'). value = 'Corte o Estria';
  			document.getElementById('corte_estria1').disabled = true;
  			document.getElementById('corte_estria2').disabled = true;
  			inhabilitarFormulario('corte_estria1','corte_estria_h');
		}else{
			document.getElementById('prof_defect').value = "";
		}
	}
}

function validar_prof_corte(valor){
	profundidad_corte = valor;
	espesor_cilindro = document.getElementById('espesor_cili').value;
	porcentaje_profundidad = 0;

	porcentaje_profundidad = profundidad_corte*100/espesor_cilindro;

	if(porcentaje_profundidad>=10){
		if(confirm("Con estos valores el cilindro será condenado, ¿Desea continuar?")){
			alert("Cilindro Condenado!");
			document.getElementById("espesor_min_cili").readOnly = true;
			document.getElementById("espesor_cili").readOnly = true;
			document.getElementById("prof_defect").readOnly = true;
			document.getElementById("long_defect").readOnly = true;
			document.getElementById("diamExtCilindro").readOnly = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
  			document.getElementById('motivo_condena'). value = 'Corte o Estria';
  			document.getElementById('corte_estria1').disabled = true;
  			document.getElementById('corte_estria2').disabled = true;
  			inhabilitarFormulario('corte_estria1','corte_estria_h');
		}else{
			document.getElementById('prof_defect').value = "";
		}
	}
}

function validar_long_corte(valor){
	longitud_corte = valor;
	diametro_externo_cilindro = document.getElementById('diamExtCilindro').value;
	porc_longitud_corte = 0;

	porc_longitud_corte = longitud_corte*100/diametro_externo_cilindro;

	if(porc_longitud_corte >= 25){
		if(confirm("Con estos valores el cilindro será condenado,¿Desea continuar?")){
			alert("Cilindro Condenado!");
			document.getElementById("espesor_min_cili").readOnly = true;
			document.getElementById("espesor_cili").readOnly = true;
			document.getElementById("prof_defect").readOnly = true;
			document.getElementById("long_defect").readOnly = true;
			document.getElementById("diamExtCilindro").readOnly = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
  			document.getElementById('motivo_condena'). value = 'Corte o Estria';
  			document.getElementById('corte_estria1').disabled = true;
  			document.getElementById('corte_estria2').disabled = true;
  			inhabilitarFormulario('corte_estria1','corte_estria_h');
		}else{
			document.getElementById('long_defect').value = "";
		}
	}
}

function checkGrieta(valor){
  	valorCheck = valor;

  	if(valorCheck == 1){
  		
  		if(confirm("Al seleccionar esta opción el cilindro será condenado, ¿Desea continuar?")){
  			if(confirm("¿Desea dejar el cilindro en estado Rechazado para poder continuar despues?")){
				alert("Cilindro Rechazado!");
				document.getElementById('grieta1').disabled = true;
	  			document.getElementById('grieta2').disabled = true;
	  			document.getElementById('estado_prueba').value = 'RECHAZADO';
	  			document.getElementById('motivo_condena'). value = 'Grieta';
	  			document.getElementById('grieta_h').value = '1';
	  			inhabilitarFormulario('grieta1','grieta_h');
			}else{
				alert("Cilindro Condenado!");
				document.getElementById('grieta1').disabled = true;
	  			document.getElementById('grieta2').disabled = true;
	  			document.getElementById('estado_prueba').value = 'CONDENADO';
	  			document.getElementById('motivo_condena'). value = 'Grieta';
	  			document.getElementById('grieta_h').value = '1';
	  			inhabilitarFormulario('grieta1','grieta_h');
			}
  			
  		}else{
  			document.getElementById('grieta1').checked = false;
  		}
  		
  	}else if(valorCheck == 0){
  		document.getElementById('grieta_h').value = '2';
  	}
}

function checkArcoAntorcha(valor){
  	valorCheck = valor;

  	if(valorCheck == 1){
  		
  		if(confirm("Al seleccionar esta opción el cilindro será condenado, ¿Desea continuar?")){
  			if(confirm("¿Desea dejar el cilindro en estado Rechazado para poder continuar despues?")){
				alert("Cilindro Rechazado!");
				document.getElementById('arco_antorcha1').disabled = true;
	  			document.getElementById('arco_antorcha2').disabled = true;
	  			document.getElementById('estado_prueba').value = 'RECHAZADO';
	  			document.getElementById('motivo_condena'). value = 'Quemadura por Arco y Antorcha';
	  			document.getElementById('arco_antorcha_h').value = '1';
	  			inhabilitarFormulario('arco_antorcha1','arco_antorcha_h');
			}else{
				alert("Cilindro Condenado!");
				document.getElementById('arco_antorcha1').disabled = true;
	  			document.getElementById('arco_antorcha2').disabled = true;
	  			document.getElementById('estado_prueba').value = 'CONDENADO';
	  			document.getElementById('motivo_condena'). value = 'Quemadura por Arco y Antorcha';
	  			document.getElementById('arco_antorcha_h').value = '1';
	  			inhabilitarFormulario('arco_antorcha1','arco_antorcha_h');
			}
  			
  		}else{
  			document.getElementById('arco_antorcha1').checked = false;
  		}
  		
  	}else if(valorCheck == 0){
  		document.getElementById('arco_antorcha_h').value = '2';
  	}
}

function checkDanoFuego(valor){
  	valorCheck = valor;

  	if(valorCheck == 1){
  		
  		if(confirm("Al seleccionar esta opción el cilindro será condenado, ¿Desea continuar?")){
  			if(confirm("¿Desea dejar el cilindro en estado Rechazado para poder continuar despues?")){
				alert("Cilindro Rechazado!");
				document.getElementById('dano_fuego1').disabled = true;
	  			document.getElementById('dano_fuego2').disabled = true;
	  			document.getElementById('estado_prueba').value = 'RECHAZADO';
	  			document.getElementById('motivo_condena'). value = 'Daño por Fuego';
	  			document.getElementById('dano_fuego_h').value = '1';
  			inhabilitarFormulario('dano_fuego1','dano_fuego_h');
			}else{
				alert("Cilindro Condenado!");
				document.getElementById('dano_fuego1').disabled = true;
	  			document.getElementById('dano_fuego2').disabled = true;
	  			document.getElementById('estado_prueba').value = 'CONDENADO';
	  			document.getElementById('motivo_condena'). value = 'Daño por Fuego';
	  			document.getElementById('dano_fuego_h').value = '1';
	  			inhabilitarFormulario('dano_fuego1','dano_fuego_h');
			}
  			
  		}else{
  			document.getElementById('dano_fuego1').checked = false;
  		}
  		
  	}else if(valorCheck == 0){
  		document.getElementById('dano_fuego_h').value = '2';
  	}
}

function checkTaponCuello(valor){
  	valorCheck = valor;

  	if(valorCheck == 1){
  		
  		if(confirm("Al seleccionar esta opción el cilindro será condenado, ¿Desea continuar?")){
  			if(confirm("¿Desea dejar el cilindro en estado Rechazado para poder continuar despues?")){
				alert("Cilindro Rechazado!");
				document.getElementById('tapon_cuello1').disabled = true;
	  			document.getElementById('tapon_cuello2').disabled = true;
	  			document.getElementById('estado_prueba').value = 'RECHAZADO';
	  			document.getElementById('motivo_condena'). value = 'Insertos en Tapon o Cuello';
	  			document.getElementById('tapon_cuello_h').value = '1';
	  			inhabilitarFormulario('tapon_cuello1','tapon_cuello_h');
			}else{
				alert("Cilindro Condenado!");
				document.getElementById('tapon_cuello1').disabled = true;
	  			document.getElementById('tapon_cuello2').disabled = true;
	  			document.getElementById('estado_prueba').value = 'CONDENADO';
	  			document.getElementById('motivo_condena'). value = 'Insertos en Tapon o Cuello';
	  			document.getElementById('tapon_cuello_h').value = '1';
	  			inhabilitarFormulario('tapon_cuello1','tapon_cuello_h');
			}
  			
  		}else{
  			document.getElementById('tapon_cuello1').checked = false;
  		}
  		
  	}else if(valorCheck == 0){
  		document.getElementById('tapon_cuello_h').value = '2';
  	}
}

function checkCuello(valor){
  	valorCheck = valor;

  	if(valorCheck == 1){
  		document.getElementById('defecto_cuello_h').value = '1';
  		document.getElementById('info_defecto_cuello').style.display = 'block';
  	}else if(valorCheck == 0){
  		document.getElementById('defecto_cuello_h').value = '2';
  		document.getElementById('info_defecto_cuello').style.display = 'none';
  	}
}

function validarHilos(valor){
	numero_hilos = valor;
	tipo_rosca =document.getElementById('tipo_rosca').value;

	if(tipo_rosca == 0){
		alert('Por favor seleccione el tipo de rosca');
		document.getElementById('no_hilos').value = "";
	}else if(tipo_rosca == "conica"){
		if(numero_hilos<7){
			if(confirm("Con estos datos el cilindro será condenado, ¿Desea continuar?")){
				document.getElementById('defecto_cuello1').disabled = true;
				document.getElementById('defecto_cuello2').disabled = true;
				document.getElementById('tipo_rosca').disabled = true;
				document.getElementById('no_hilos').readOnly = true;
				document.getElementById('si_corr_cuello').disabled = true;
				document.getElementById('no_corr_cuello').disabled = true;
				document.getElementById('estado_prueba').value = 'CONDENADO';
  				document.getElementById('motivo_condena'). value = 'Defectos del Cuello';
  				inhabilitarFormulario('defecto_cuello1','defecto_cuello_h');
			}else{
				document.getElementById('no_hilos').value = "";
			}
		}
	}else if(tipo_rosca == "recta"){
		if(numero_hilos<6){
			if(confirm("Con estos datos el cilindro será condenado, ¿Desea continuar?")){
				document.getElementById('defecto_cuello1').disabled = true;
				document.getElementById('defecto_cuello2').disabled = true;
				document.getElementById('tipo_rosca').disabled = true;
				document.getElementById('no_hilos').readOnly = true;
				document.getElementById('si_corr_cuello').disabled = true;
				document.getElementById('no_corr_cuello').disabled = true;
				document.getElementById('estado_prueba').value = 'CONDENADO';
  				document.getElementById('motivo_condena'). value = 'Defectos del Cuello';
  				inhabilitarFormulario('defecto_cuello1','defecto_cuello_h');
			}else{
				document.getElementById('no_hilos').value = "";
			}
		}
	}
}

function corr_cuello(){

	if(confirm("Con estos datos el cilindro será condenado, ¿Desea continuar?")){
		document.getElementById('defecto_cuello1').disabled = true;
		document.getElementById('defecto_cuello2').disabled = true;
		document.getElementById('tipo_rosca').disabled = true;
		document.getElementById('no_hilos').readOnly = true;
		document.getElementById('si_corr_cuello').disabled = true;
		document.getElementById('no_corr_cuello').disabled = true;
		document.getElementById('estado_prueba').value = 'CONDENADO';
  		document.getElementById('motivo_condena'). value = 'Defectos del Cuello';
  		inhabilitarFormulario('defecto_cuello1','defecto_cuello_h');
	}else{
		document.getElementById('si_corr_cuello').checked = false;
	}
}

function checkEstampacion(valor){

	var valorCheck = valor;

	if(valorCheck == 1){
		if(confirm("Al seleccionar esta opción el cilindro será condenado, ¿Desea continuar?")){
			if(confirm("¿Desea dejar el cilindro en estado Rechazado para poder continuar despues?")){
				alert("Cilindro Rechazado!");
				document.getElementById('estampacion1').disabled = true;
				document.getElementById('estampacion2').disabled = true;
				document.getElementById('estado_prueba').value = 'RECHAZADO';
				document.getElementById('motivo_condena'). value = 'Estampación';
				document.getElementById('estampacion_h').value = '1';
				inhabilitarFormulario('estampacion1','estampacion_h');
			}else{
				alert("Cilindro Condenado!");
				document.getElementById('estampacion1').disabled = true;
				document.getElementById('estampacion2').disabled = true;
				document.getElementById('estado_prueba').value = 'CONDENADO';
				document.getElementById('motivo_condena'). value = 'Estampación';
				document.getElementById('estampacion_h').value = '1';
				inhabilitarFormulario('estampacion1','estampacion_h');
			}
			
		}else{
			document.getElementById('estampacion1').checked  = false;
		}
	}else if(valorCheck == 0){
		
		document.getElementById('estampacion_h').value = '2';
	}
}

function checkMarcas(valor){

	var valorCheck = valor;

	if(valorCheck == 1){
		if(confirm("Al seleccionar esta opción el cilindro será condenado, ¿Desea continuar?")){
			if(confirm("¿Desea dejar el cilindro en estado Rechazado para poder continuar despues?")){
				alert("Cilindro Rechazado!");
				document.getElementById('marcas1').disabled = true;
				document.getElementById('marcas2').disabled = true;
				document.getElementById('estado_prueba').value = 'RECHAZADO';
				document.getElementById('motivo_condena'). value = 'Marcas Sospechosas';
				document.getElementById('marcas_sospechosas_h').value = '1';
				inhabilitarFormulario('marcas1','marcas_sospechosas_h');
			}else{
				alert("Cilindro Condenado!");
				document.getElementById('marcas1').disabled = true;
				document.getElementById('marcas2').disabled = true;
				document.getElementById('estado_prueba').value = 'CONDENADO';
				document.getElementById('motivo_condena'). value = 'Marcas Sospechosas';
				document.getElementById('marcas_sospechosas_h').value = '1';
				inhabilitarFormulario('marcas1','marcas_sospechosas_h');
			}
			
		}else{
			document.getElementById('marcas1').checked  = false;
		}
	}else if(valorCheck == 0){
		
		document.getElementById('marcas_sospechosas_h').value = '2';
	}
}

function checkEstabilidadVertical(valor){

	var valorCheck = valor;

	if(valorCheck == 1){
		if(confirm("Al seleccionar esta opción el cilindro será condenado, ¿Desea continuar?")){
			if(confirm("¿Desea dejar el cilindro en estado Rechazado para poder continuar despues?")){
				alert("Cilindro Rechazado!");
				document.getElementById('estabilidad1').disabled = true;
				document.getElementById('estabilidad2').disabled = true;
				document.getElementById('estado_prueba').value = 'RECHAZADO';
				document.getElementById('motivo_condena'). value = 'Estabilidad Vertical';
				document.getElementById('estabilidad_h').value = '1';
				inhabilitarFormulario('estabilidad1','estabilidad_h');
			}else{
				alert("Cilindro Condenado!");
				document.getElementById('estabilidad1').disabled = true;
				document.getElementById('estabilidad2').disabled = true;
				document.getElementById('estado_prueba').value = 'CONDENADO';
				document.getElementById('motivo_condena'). value = 'Estabilidad Vertical';
				document.getElementById('estabilidad_h').value = '1';
				inhabilitarFormulario('estabilidad1','estabilidad_h');
			}
			
		}else{
			document.getElementById('estabilidad1').checked  = false;
		}
	}else if(valorCheck == 0){
		
		document.getElementById('estabilidad_h').value = '2';
	}
}

function checkMartillo(valor){
	var valorCheck = valor;
	if(valorCheck == 1){
		alert("Si escucha el ruido sordo, verifique la corrosión al interior del cilindro");
		document.getElementById('prueba_martillo_h').value = '1';
	}else{
		document.getElementById('prueba_martillo_h').value = '2';
	}
}

function checkCorrosionGeneral(valor){
	valorCheck = valor;

	if(valorCheck == 1){
		document.getElementById('info_corrosion_general').style.display = "block";
		document.getElementById('corrosion_general_h').value = 1;
	}else if (valorCheck == 0) {
		document.getElementById('info_corrosion_general').style.display = "none";
		document.getElementById('corrosion_general_h').value = 2;
	}
}

function validar_superficie_corr_gene(){

	if(confirm("Con esta opción el cilindro será condenado,¿Desea continuar?")){
		alert("Cilindro Condenado!");
		document.getElementById('corrosion_general1').disabled = true;
		document.getElementById('corrosion_general2').disabled = true;
		document.getElementById('superficie_cil_si').disabled = true;
		document.getElementById('superficie_cil_no').disabled = true;
		document.getElementById('superficie_cil_no').checked = false;
		document.getElementById('espesor_pared1').readOnly = true;
		document.getElementById('espesor_min_gar').readOnly = true;
		document.getElementById('prof_corr_gene').readOnly = true;
		document.getElementById('estado_prueba').value = 'CONDENADO';
		document.getElementById('motivo_condena'). value = 'Corrosion General';
		inhabilitarFormulario('corrosion_general1','corrosion_general_h');
	}else{
		document.getElementById('superficie_cil_si').checked = false;
	}
}

function validar_espesor_corr_gene(valor){
	espesor_minimo_garantizado = valor;
	espesor_cilindro = document.getElementById('espesor_pared1').value;

	if(espesor_cilindro>espesor_minimo_garantizado){
		if(confirm("Con esta opción el cilindro será condenado,¿Desea continuar?")){
			alert("Cilindro Condenado!");
			document.getElementById('espesor_pared1').readOnly = true;
			document.getElementById('espesor_min_gar').readOnly = true;
			document.getElementById('prof_corr_gene').readOnly = true;
			document.getElementById('corrosion_general1').disabled = true;
			document.getElementById('corrosion_general2').disabled = true;
			document.getElementById('superficie_cil_si').disabled = true;
			document.getElementById('superficie_cil_no').disabled = true;
			document.getElementById('superficie_cil_no').checked = false;
			document.getElementById('estado_prueba').value = 'CONDENADO';
			document.getElementById('motivo_condena'). value = 'Corrosion General';
			inhabilitarFormulario('corrosion_general1','corrosion_general_h');
		}else{
			document.getElementById('espesor_pared1').value = "";
			document.getElementById('espesor_min_gar').value = "";
		}
	}
}

function validar_prof_corr_gene(valor){
	profundidad_penetracion = valor;
	espesor_pared = document.getElementById('espesor_pared1').value;
	porcentaje_profundidad = 0;

	porcentaje_profundidad = profundidad_penetracion * 100 / espesor_pared;

	if(porcentaje_profundidad>=10){
		if(confirm("Con esta opción el cilindro será condenado,¿Desea continuar?")){
			alert("Cilindro Condenado!");
			document.getElementById('espesor_pared1').readOnly = true;
			document.getElementById('espesor_min_gar').readOnly = true;
			document.getElementById('prof_corr_gene').readOnly = true;
			document.getElementById('corrosion_general1').disabled = true;
			document.getElementById('corrosion_general2').disabled = true;
			document.getElementById('superficie_cil_si').disabled = true;
			document.getElementById('superficie_cil_no').disabled = true;
			document.getElementById('superficie_cil_no').checked = false;
			document.getElementById('estado_prueba').value = 'CONDENADO';
			document.getElementById('motivo_condena'). value = 'Corrosion General';
			inhabilitarFormulario('corrosion_general1','corrosion_general_h');
		}else{
			document.getElementById('prof_corr_gene').value = "";
			
		}
	}
}

function checkCorrosionLocal(valor){
	valorCheck = valor;

	if(valorCheck == 1){
		document.getElementById('info_corrosion_local').style.display = "block";
		document.getElementById('corrosion_local_h').value = "1";
	}else{
		document.getElementById('info_corrosion_local').style.display = "none";
		document.getElementById('corrosion_local_h').value = "2";
	}
}

function validar_espesor_corr_local(valor){
	espesor_minimo_garantizado = valor;
	espesor_cilindro = document.getElementById('espesor_pared2').value;

	if(espesor_cilindro<espesor_minimo_garantizado){
		if(confirm("Con esta opción el cilindro será condenado,¿Desea continuar?")){
			alert("Cilindro Condenado!");
			document.getElementById('espesor_pared2').readOnly = true;
			document.getElementById('espesor_min_gar2').readOnly = true;
			document.getElementById('prof_corr_local2').readOnly = true;
			document.getElementById('corrosion_local1').disabled = true;
			document.getElementById('corrosion_local2').disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
			document.getElementById('motivo_condena'). value = 'Corrosion Local';
			inhabilitarFormulario('corrosion_local1','corrosion_local_h');			
		}else{
			document.getElementById('espesor_pared2').value = "";
			document.getElementById('espesor_min_gar2').value = "";
		}
	}
}

function validar_prof_corr_local(valor){
	profundidad_penetracion = valor;
	espesor_pared = document.getElementById('espesor_pared2').value;
	porcentaje_profundidad = 0;

	porcentaje_profundidad = profundidad_penetracion * 100 / espesor_pared;

	if(porcentaje_profundidad>=20){
		if(confirm("Con esta opción el cilindro será condenado,¿Desea continuar?")){
			alert("Cilindro Condenado!");
			document.getElementById('espesor_pared2').readOnly = true;
			document.getElementById('espesor_min_gar2').readOnly = true;
			document.getElementById('prof_corr_local2').readOnly = true;
			document.getElementById('corrosion_local1').disabled = true;
			document.getElementById('corrosion_local2').disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
			document.getElementById('motivo_condena'). value = 'Corrosion Local';
			inhabilitarFormulario('corrosion_local1','corrosion_local_h');	
		}else{
			document.getElementById('prof_corr_local2').value = "";
			
		}
	}
}

function checkCorrosionLineal(valor){
	valorCheck = valor;

	if(valorCheck == 1){
		document.getElementById('info_corrosion_lineal').style.display = "block";
		document.getElementById('corrosion_lineal_h').value = "1";
	}else{
		document.getElementById('info_corrosion_lineal').style.display = "none";
		document.getElementById('corrosion_lineal_h').value = "2";
	}
}

function validar_long_corr_lin(valor){
	longitud_corrosion = valor;
	diametro_cilindro = document.getElementById('diam_cili_corr_lin').value;

	if(longitud_corrosion<diametro_cilindro){
		if (confirm("Con esta opción el cilindro será condenado,¿Desea continuar?")) {
			document.getElementById('diam_cili_corr_lin').readOnly = true;
			document.getElementById('long_corr_lin').readOnly = true;
			document.getElementById('prof_corr_lin').readOnly = true;
			document.getElementById('espesor_original').readOnly = true;
			document.getElementById('espesor_actual_cor_lin').readOnly = true;
			document.getElementById('espesor_min_gar_corlin').readOnly = true;
			document.getElementById('corrosion_lineal1').disabled = true;
			document.getElementById('corrosion_lineal2').disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
	  		document.getElementById('motivo_condena'). value = 'Corrosion Lineal';
	  		inhabilitarFormulario('corrosion_lineal1','corrosion_lineal_h');
	  	}else{
			document.getElementById('diam_cili_corr_lin').value = "";
			document.getElementById('long_corr_lin').value = "";
		}
	}
}

function validar_prof_corr_lin(valor){
	espesor_original_cilindro = valor;
	profundidad_corrosion_lineal = document.getElementById('prof_corr_lin').value;
	porcentaje_condena = 0;

	porcentaje_condena = profundidad_corrosion_lineal*100/espesor_original_cilindro;

	if(porcentaje_condena>=10){
		if (confirm("Con esta opción el cilindro será condenado,¿Desea continuar?")) {
			document.getElementById('diam_cili_corr_lin').readOnly = true;
			document.getElementById('long_corr_lin').readOnly = true;
			document.getElementById('prof_corr_lin').readOnly = true;
			document.getElementById('espesor_original').readOnly = true;
			document.getElementById('espesor_actual_cor_lin').readOnly = true;
			document.getElementById('espesor_min_gar_corlin').readOnly = true;
			document.getElementById('corrosion_lineal1').disabled = true;
			document.getElementById('corrosion_lineal2').disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
	  		document.getElementById('motivo_condena'). value = 'Corrosion Lineal';
	  		inhabilitarFormulario('corrosion_lineal1','corrosion_lineal_h');
	  	}else{
			document.getElementById('prof_corr_lin').value = "";
			document.getElementById('espesor_original').value = "";
		}
	}
}

function validar_espesor_corr_lin(valor){

	espesor_minimo_garantizado = valor;
	espesor_pared = document.getElementById('espesor_actual_cor_lin').value;

	if(espesor_pared>espesor_minimo_garantizado){
		if (confirm("Con esta opción el cilindro será condenado,¿Desea continuar?")) {
			alert("Cilindro Condenado!");
			document.getElementById('diam_cili_corr_lin').readOnly = true;
			document.getElementById('long_corr_lin').readOnly = true;
			document.getElementById('prof_corr_lin').readOnly = true;
			document.getElementById('espesor_original').readOnly = true;
			document.getElementById('espesor_actual_cor_lin').readOnly = true;
			document.getElementById('espesor_min_gar_corlin').readOnly = true;
			document.getElementById('corrosion_lineal1').disabled = true;
			document.getElementById('corrosion_lineal2').disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
	  		document.getElementById('motivo_condena'). value = 'Corrosion Lineal';
	  		inhabilitarFormulario('corrosion_lineal1','corrosion_lineal_h');
	  	}else{
			document.getElementById('espesor_actual_cor_lin').value = "";
			document.getElementById('espesor_min_gar_corlin').value = "";
		}
	}
}

function checkPicadurasAisladas(valor){
	valorCheck = valor;

	if(valorCheck == 1){
		document.getElementById('info_picaduras_aisladas').style.display = "block";
		document.getElementById('picaduras_aisladas_h').value = "1";
	}else{
		document.getElementById('info_picaduras_aisladas').style.display = "none";
		document.getElementById('picaduras_aisladas_h').value = "2";
	}
}

function validar_espesor_picaduras(valor){

	espesor_minimo_garantizado = valor;
	espesor_pared = document.getElementById("espesor_picaduras").value;

	if(espesor_pared<espesor_minimo_garantizado){
		if (confirm("Con esta opción el cilindro será condenado,¿Desea continuar?")) {
			alert("Cilindro Condenado!");
			document.getElementById('espesor_picaduras').readOnly = true;
			document.getElementById('espesor_min_picaduras').readOnly = true;
			document.getElementById('diametro_picaduras').readOnly = true;
			document.getElementById('picaduras_aisladas1').disabled = true;
			document.getElementById('picaduras_aisladas2').disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
	  		document.getElementById('motivo_condena'). value = 'Picaduras Aisladas';
	  		inhabilitarFormulario('picaduras_aisladas1','picaduras_aisladas_h');
	  	}else{
			document.getElementById('espesor_actual_cor_lin').value = "";
			document.getElementById('espesor_min_gar_corlin').value = "";
		}
	}
}

function validar_diam_picaduras(valor){

	espesor_minimo = document.getElementById("espesor_min_picaduras").value;
	diametro_picaduras = valor;
	tercio = 0;

	tercio = espesor_minimo / 3;

	if(diametro_picaduras>tercio){
		if (confirm("Con esta opción el cilindro será condenado,¿Desea continuar?")) {
			alert("Cilindro Condenado!");
			document.getElementById('espesor_picaduras').readOnly = true;
			document.getElementById('espesor_min_picaduras').readOnly = true;
			document.getElementById('diametro_picaduras').readOnly = true;
			document.getElementById('picaduras_aisladas1').disabled = true;
			document.getElementById('picaduras_aisladas2').disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
	  		document.getElementById('motivo_condena'). value = 'Picaduras Aisladas';
	  		inhabilitarFormulario('picaduras_aisladas1','picaduras_aisladas_h');
	  	}else{
			document.getElementById('diametro_picaduras').value = "";
		}
	}
}

function checkPicadurasMultiples(valor){

	valorCheck = valor;

	if(valorCheck == 1){
		document.getElementById('info_picaduras_multiples').style.display = "block";
		document.getElementById('picaduras_multiples_h').value = "1";
	}else{
		document.getElementById('info_picaduras_multiples').style.display = "none";
		document.getElementById('picaduras_multiples_h').value = "2";
	}
}

function validar_espesor_picaduras_multi(valor){
	espesor_restante = document.getElementById("espesor_restante").value;
	espesor_minimo = valor;
	porcentaje_condena = 0;

	porcentaje_condena = espesor_restante * 100 / espesor_minimo;


	if(porcentaje_condena >= 5){
		if (confirm("Con esta opción el cilindro será condenado,¿Desea continuar?")) {
			alert("Cilindro Condenado!");
			document.getElementById('espesor_restante').readOnly = true;
			document.getElementById('espesor_min_picaduras_multi').readOnly = true;
			document.getElementById('picaduras_multi1').disabled = true;
			document.getElementById('picaduras_multi2').disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
	  		document.getElementById('motivo_condena'). value = 'Picaduras Multiples';
	  		inhabilitarFormulario('picaduras_multi1','picaduras_multiples_h');
	  	}else{
			document.getElementById('espesor_min_picaduras_multi').value = "";
		}
	}
}

function checkCorrosionHendidura(valor){

	valorCheck = valor;

	if(valorCheck == 1){
		document.getElementById('info_corr_hendidura').style.display = "block";
		document.getElementById('corr_hendidura_h').value = "1";
	}else{
		document.getElementById('info_corr_hendidura').style.display = "none";
		document.getElementById('corr_hendidura_h').value = "2";
	}
}

function validar_espesor_hendidura(valor){

	corrosion_hendidura = valor;
	espesor_original = document.getElementById("espesor_hendidura").value;
	porcentaje_condena = 0;

	porcentaje_condena = corrosion_hendidura * 100 / espesor_original;

	if(porcentaje_condena >= 20){
		if (confirm("Con esta opción el cilindro será condenado,¿Desea continuar?")) {
			alert("Cilindro Condenado!");
			document.getElementById('espesor_hendidura').readOnly = true;
			document.getElementById('profundidad_penetracion').readOnly = true;
			document.getElementById('corr_hendidura1').disabled = true;
			document.getElementById('corr_hendidura2').disabled = true;
			document.getElementById('estado_prueba').value = 'CONDENADO';
	  		document.getElementById('motivo_condena'). value = 'Corrosion Hendidura';
	  		inhabilitarFormulario('corr_hendidura1','corr_hendidura_h');
	  	}else{
			document.getElementById('corrosion_hendidura').value = "";
		}
	}
}

function inhabilitarFormulario(check, campo){

	habilitar = check;
	llenar = campo;

	document.getElementById("protuberancia1").disabled = true;
	document.getElementById("protuberancia2").disabled = true;
	document.getElementById("abolladuras1").disabled = true;
	document.getElementById("abolladuras2").disabled = true;
	document.getElementById("abombamiento1").disabled = true;
	document.getElementById("abombamiento2").disabled = true;
	document.getElementById("indentacion1").disabled = true;
	document.getElementById("indentacion2").disabled = true;
	document.getElementById("corte_estria1").disabled = true;
	document.getElementById("corte_estria2").disabled = true;
	document.getElementById("grieta1").disabled = true;
	document.getElementById("grieta2").disabled = true;
	document.getElementById("arco_antorcha1").disabled = true;
	document.getElementById("arco_antorcha2").disabled = true;
	document.getElementById("dano_fuego1").disabled = true;
	document.getElementById("dano_fuego2").disabled = true;
	document.getElementById("tapon_cuello1").disabled = true;
	document.getElementById("tapon_cuello2").disabled = true;
	document.getElementById("defecto_cuello1").disabled = true;
	document.getElementById("defecto_cuello2").disabled = true;
	document.getElementById("estampacion1").disabled = true;
	document.getElementById("estampacion2").disabled = true;
	document.getElementById("marcas1").disabled = true;
	document.getElementById("marcas2").disabled = true;
	document.getElementById("estabilidad1").disabled = true;
	document.getElementById("estabilidad2").disabled = true;
	document.getElementById("corrosion_general1").disabled = true;
	document.getElementById("corrosion_general2").disabled = true;
	document.getElementById("corrosion_local1").disabled = true;
	document.getElementById("corrosion_local2").disabled = true;
	document.getElementById("corrosion_lineal1").disabled = true;
	document.getElementById("corrosion_lineal2").disabled = true;
	document.getElementById("picaduras_aisladas1").disabled = true;
	document.getElementById("picaduras_aisladas2").disabled = true;
	document.getElementById("picaduras_multi1").disabled = true;
	document.getElementById("picaduras_multi2").disabled = true;
	document.getElementById("corr_hendidura1").disabled = true;
	document.getElementById("corr_hendidura2").disabled = true;

	document.getElementById("protuberancia_h").value = 2;
	document.getElementById("abolladuras_h").value = 2;
	document.getElementById("abombamiento_h").value = 2;
	document.getElementById("indentacion_h").value = 2;
	document.getElementById("corte_estria_h").value = 2;
	document.getElementById("grieta_h").value = 2;
	document.getElementById("arco_antorcha_h").value = 2;
	document.getElementById("dano_fuego_h").value = 2;
	document.getElementById("tapon_cuello_h").value = 2;
	document.getElementById("defecto_cuello_h").value = 2;
	document.getElementById("estampacion_h").value = 2;
	document.getElementById("marcas_sospechosas_h").value = 2;
	document.getElementById("estabilidad_h").value = 2;
	document.getElementById("corrosion_general_h").value = 2;
	document.getElementById("corrosion_local_h").value = 2;
	document.getElementById("corrosion_lineal_h").value = 2;
	document.getElementById("picaduras_aisladas_h").value = 2;
	document.getElementById("picaduras_multiples_h").value = 2;
	document.getElementById("corr_hendidura_h").value = 2;

	document.getElementById("protuberancia2").checked = true;	
	document.getElementById("abolladuras2").checked = true;
	document.getElementById("abombamiento2").checked = true;
	document.getElementById("indentacion2").checked = true;
	document.getElementById("abolladuras2").checked = true;
	document.getElementById("corte_estria2").checked = true;
	document.getElementById("grieta2").checked = true;
	document.getElementById("arco_antorcha2").checked = true;
	document.getElementById("dano_fuego2").checked = true;
	document.getElementById("tapon_cuello2").checked = true;
	document.getElementById("defecto_cuello2").checked = true;
	document.getElementById("estampacion2").checked = true;
	document.getElementById("marcas2").checked = true;
	document.getElementById("estabilidad2").checked = true;
	document.getElementById("corrosion_general2").checked = true;
	document.getElementById("corrosion_local2").checked = true;
	document.getElementById("corrosion_lineal2").checked = true;
	document.getElementById("picaduras_aisladas2").checked = true;
	document.getElementById("picaduras_multi2").checked = true;
	document.getElementById("corr_hendidura2").checked = true;


	document.getElementById(habilitar).checked = true;
	document.getElementById(llenar).value = 1;
}
</script>
<?php 
	 //include footer
	 include("inc/google-analytics.php"); 
}
else
{
	 header("Location:index.php");
}
?>