<?php
date_default_timezone_set("America/Bogota");

require ("libraries/conexion.php");
session_start();
if(@$_SESSION['logged']== 'yes')
{ 
	$id_user=$_SESSION['su'];
    $acc = $_SESSION['acc'];
    $letra = isset($_REQUEST['letra']) ? $_REQUEST['letra'] : NULL;
    $id_orden = isset($_REQUEST['id_orden']) ? $_REQUEST['id_orden'] : NULL;
    
	require_once("inc/init.php");
	require_once("inc/config.ui.php");
	$page_title = "Prueba";
	$page_css[] = "your_style.css";
	include("inc/header.php");
	$page_nav["pev"]["sub"]["prueba"]["active"] = true;
	include("inc/nav.php");

	$fecha = date("Y-m-d");
	$hora = date("G:i:s",time());
	
	?>
	<?php
		$habilitacion_verificacion = 0;
		$consulta9 = "SELECT * FROM verificacion_metodo_pev WHERE fecha LIKE '".$fecha."%'";
		$resultado9 = mysqli_query($con,$consulta9);

		if(mysqli_num_rows($resultado9)){
			while ($linea9 = mysqli_fetch_assoc($resultado9)) {
				
				$estado_calibracion = $linea9["estado_calibracion"];

				if($estado_calibracion == 3){
					$habilitacion_verificacion = 1;
					break;
				}
			}
		}
	?>
	<!-- MAIN PANEL -->
	<div id="main" role="main">
	<?php
	if (in_array(72, $acc))
	{
	?>
		<div id="content">	
			<div class="" align="center">
				<h1  class="page-title txt-color-blueDark">PRUEBA DE EXPANSIÓN VOLUMÉTRICA CILINDROS PEV</h1>			
			</div>		
			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">		
				<header><meta charset="gb18030">
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>Seleccione un cilindro disponible para prueba</h2>				
				</header>
				<div>
					<div class="jarviswidget-editbox"></div>
					<div class="widget-body no-padding">
						<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
							<form action="busqueda_cilindros_eto.php" method="POST" name="form1">
								<thead>
									<tr>
										<th>#</th>
										<th>Seleccionar</th>
										<th>Número Cilindro</th>
										<th>Tipo Gas</th>
										<th>Estado Cilindro</th>
										<th>Recepción Cilindro</th>
										<th>Estado Prueba</th>
									</tr>
								</thead>
								<tbody>
									<?php	                                    
		                    			$contador = "0";		                    			
		                    	        $consulta_1 = "SELECT *
		                    	        			   FROM has_movimiento_cilindro_pev 
		                    	        			   WHERE ph_u = '1' AND material_cilindro_u <> 3
		                    	        			   ORDER BY id_has_movimiento_cilindro_pev DESC ";
		                                $resultado_1 = mysqli_query($con,$consulta_1);
		                                if(mysqli_num_rows($resultado_1) > 0)
		                                {
		                                	while ($linea_1 = mysqli_fetch_assoc($resultado_1)) 
		                                	{
		                                		$estado_prueba_exp = "Sin prueba registrada";
		                                		$habilitado_prueba = 0;
		                                		$id_has_movimiento_cilindro_pev = $linea_1['id_has_movimiento_cilindro_pev'];

		                                		$id_transporte_pev = $linea_1['id_transporte_pev'];
		                                		$num_cili = $linea_1['num_cili'];
		                                		$id_tapa = $linea_1['id_tapa'];
		                                		$id_tipo_gas_pev = $linea_1['id_tipo_gas_pev'];
		                                		$material_cilindro = $linea_1['material_cilindro_u'];
		                                		$consulta_2 = "SELECT nombre 
		                                					   FROM tipo_gas_pev
		                                					   WHERE id_tipo_gas_pev = '".$id_tipo_gas_pev."'";
		                                		$resultado_2 = mysqli_query($con,$consulta_2);
		                                		if (mysqli_num_rows($resultado_2) > 0)
		                                		{
		                                			$linea_2 = mysqli_fetch_assoc($resultado_2);
		                                			$nombre = $linea_2['nombre'];
		                                		}		                                		
		                                		else
		                                		{
		                                			echo "Error";
		                                		}
		                                		mysqli_free_result($resultado_2);   
		                                		$id_cliente = $linea_1['id_cliente'];
		                                		$observacion_u = $linea_1['observacion_u'];
		                                		$material_cilindro_u = $linea_1['material_cilindro_u'];

		                                		$consulta5 = "SELECT * FROM cilindro_pev WHERE num_cili_pev = '$num_cili'";
		                                		$resultado5 = mysqli_query($con,$consulta5);

		                                		$linea5 = mysqli_fetch_assoc($resultado5);

		                                		$id_estado = isset($linea5["id_estado"]) ? $linea5["id_estado"] : 1;
		                                		$id_cilindro_eto = isset($linea5["id_cilindro_pev"]) ? $linea5["id_cilindro_pev"] : NULL;
		                                		$pre_pru_eto = isset($linea5["pre_pru_eto"]) ? $linea5["pre_pru_eto"] : "";
		                                		$tipo_cili_pev = isset($linea5["tipo_cili_pev"]) ? $linea5["tipo_cili_pev"] : NULL;
		                                		$fech_crea = isset($linea5["fech_crea"]) ? $linea5["fech_crea"] : NULL;

		                                		$consulta6 = "SELECT * FROM estados_pev WHERE id_estado_pev = '$id_estado'";
		                                		$resultado6 = mysqli_query($con,$consulta6);

		                                		$linea6 = mysqli_fetch_assoc($resultado6);


		                                		$estado_pev = isset($linea6["estado_pev"]) ? $linea6["estado_pev"] : NULL;

		                                		
		                                		

		                                		if($material_cilindro == 1){

		                                			if($tipo_cili_pev == 1){

		                                				$consulta8 = "SELECT * FROM insp_vi_ace_alta_pev WHERE id_has_movimiento_cilindro_pev = $id_has_movimiento_cilindro_pev";

		                                				$resultado8 = mysqli_query($con,$consulta8);

		                                				if(mysqli_num_rows($resultado8)>0){
		                                					while ($linea8 = mysqli_fetch_assoc($resultado8)) {
		                                						
		                                						$estado_prueba = isset($linea8["estado_prueba"]) ? $linea8["estado_prueba"] : NULL;

		                                						if($estado_prueba == "RECHAZADO"){
		                                							$habilitado_prueba = 0;
		                                						}else if($estado_prueba == "CONDENADO"){
		                                							$habilitado_prueba =0;
		                                							break;
		                                						}else{
		                                							$habilitado_prueba =1;
		                                						}
		                                					}
		                                				}else{
		                                					$habilitado_prueba = 0;
		                                				}
		                                			}else if($tipo_cili_pev == 2){
		                                				$consulta8 = "SELECT * FROM insp_vi_ace_baja_pev WHERE id_has_movimiento_cilindro_pev = $id_has_movimiento_cilindro_pev";

		                                				$resultado8 = mysqli_query($con,$consulta8);

		                                				if(mysqli_num_rows($resultado8)>0){
		                                					while ($linea8 = mysqli_fetch_assoc($resultado8)) {
		                                						
		                                						$estado_prueba = isset($linea8["estado_prueba"]) ? $linea8["estado_prueba"] : NULL;

		                                						if($estado_prueba == "RECHAZADO"){
		                                							$habilitado_prueba = 0;
		                                						}else if($estado_prueba == "CONDENADO"){
		                                							$habilitado_prueba =0;
		                                							break;
		                                						}else{
		                                							$habilitado_prueba =1;
		                                						}
		                                					}
		                                				}else{
		                                					$habilitado_prueba = 0;
		                                				}
		                                			}
		                                		}else if($material_cilindro == 2){
		                                			$consulta8 = "SELECT * FROM insp_vi_alu_pev WHERE id_has_movimiento_cilindro_pev = $id_has_movimiento_cilindro_pev";
		                                			$resultado8 = mysqli_query($con,$consulta8);

		                                			if(mysqli_num_rows($resultado8)>0){
		                                				while ($linea8 = mysqli_fetch_assoc($resultado8)) {
		                                					$estado_prueba = isset($linea8["estado_prueba"]) ? $linea8["estado_prueba"] : NULL;

		                                					if($estado_prueba == "RECHAZADO"){
		                                						$habilitado_prueba = 0;
		                                					}else if($estado_prueba == "CONDENADO"){
		                                						$habilitado_prueba =0;
		                                						break;
		                                					}else{
		                                						$habilitado_prueba =1;
		                                					}
		                                				}
		                                			}
		                                		}

		                                		$consulta9 = "SELECT * FROM datos_prueba_pev WHERE id_has_movimiento_cilindro_pev = '$id_has_movimiento_cilindro_pev'";
		                                		$resultado9 = mysqli_query($con,$consulta9);

		                                		if(mysqli_num_rows($resultado9)>0){
		                                			$linea9 = mysqli_fetch_assoc($resultado9);

		                                			$porcentaje_prueba = isset($linea9["porcentaje_prueba"]) ? $linea9["porcentaje_prueba"] : NULL;
		                                			$impresion = isset($linea9["impresion"]) ? $linea9["impresion"] : NULL;

		                                			if($impresion==1){
		                                				$estado_terminacion = "/TERMINADO";
		                                			}else{
		                                				$estado_terminacion = "";
		                                			}

		                                			if($porcentaje_prueba>=10){	

		                                				$estado_prueba_exp = "NO APROBADA".$estado_terminacion;
		                                				$habilitado_prueba = 4;

		                                			}else{

		                                				$estado_prueba_exp = "APROBADA".$estado_terminacion;
		                                				$habilitado_prueba = 4;

		                                			}
		                                		}
		                                		
		                                	
		                                	?>
			                                    <tr>
			                                        <td width="5"><?php echo $contador; ?></td>
			                                        <td width="5">
			                                        	<?php
			                                        	if(($habilitado_prueba == 1)&&($habilitacion_verificacion == 1)){
			                                        	?>
			                                        	<a href="prueba_expansion.php?id_has_movimiento_cilindro_pev=<?php echo $id_has_movimiento_cilindro_pev; ?>&num_cili=<?php echo $num_cili; ?>"><img src="img/comprobado_1.png" width="20" height="20"></a>

			                                        	<div style="display: none;">Icons made by <a href="https://www.flaticon.es/autores/pixel-buddha" title="Pixel Buddha">Pixel Buddha</a> from <a href="https://www.flaticon.es/" 			    title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>

			                                        	<?php
			                                        	}else if($habilitado_prueba == 4){
			                                        	?>
			                                        	<a href="prueba_expansion.php?id_has_movimiento_cilindro_pev=<?php echo $id_has_movimiento_cilindro_pev; ?>&num_cili=<?php echo $num_cili; ?>"><img src="img/comprobado_2.png" width="20" height="20"></a>
			                                        	<?php
			                                        	}else{
			                                        	?>
			                                        	<a href="#"><img src="img/no_comprobado.png" width="20" height="20"></a>
			                                        	<?php
			                                        	}
			                                        	?>
			                                        </td>
			                                        <td><?php echo $num_cili; ?></td>
			                                        <td><?php echo utf8_decode($nombre); ?></td>
			                                        <td><?php echo $estado_pev; ?></td>
			                                        <td><?php echo $fech_crea; ?></td>
			                                        <td><?php echo $estado_prueba_exp; ?></td>	                                                        
			                                	</tr>    
			                                <?php
			                                $contador ++;
			                                }
			                                mysqli_free_result($resultado_1);   
		                                }
	                            	?>
								</tbody>
							</form>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- END MAIN CONTENT -->
	<?php
	}										
	?>

	</div>


<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->
<?php
	include("inc/footer.php");
	include("inc/scripts.php"); 
?>
<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="js/plugin/datatable-responsive/datatables.responsive.min.js"></script>

<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>

<script type="text/javascript">
	// DO NOT REMOVE : GLOBAL FUNCTIONS!

	$(document).ready(function() {
		
		/* // DOM Position key index //
			
		l - Length changing (dropdown)
		f - Filtering input (search)
		t - The Table! (datatable)
		i - Information (records)
		p - Pagination (paging)
		r - pRocessing 
		< and > - div elements
		<"#id" and > - div with an id
		<"class" and > - div with a class
		<"#id.class" and > - div with an id and class
		
		Also see: http://legacy.datatables.net/usage/features
		*/	

		/* BASIC ;*/
			var responsiveHelper_dt_basic = undefined;
			
			var breakpointDefinition = {
				tablet : 1024,
				phone : 480
			};

			$('#dt_basic').dataTable({
				"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
					"t"+
					"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
				"autoWidth" : true,
				"preDrawCallback" : function() {
					// Initialize the responsive datatables helper once.
					if (!responsiveHelper_dt_basic) {
						responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
					}
				},
				"rowCallback" : function(nRow) {
					responsiveHelper_dt_basic.createExpandIcon(nRow);
				},
				"drawCallback" : function(oSettings) {
					responsiveHelper_dt_basic.respond();
				}
			});

		});

		/* END BASIC */


</script>


<?php 
	//include footer
	include("inc/google-analytics.php"); 
}
else
{
    header("Location:index.php");
}
?>