<?php
date_default_timezone_set("America/Bogota");

require ("libraries/conexion.php");
@session_start();

if(@$_SESSION['logged']== 'yes')
{ 
  	$id_cilindro_eto = isset($_REQUEST['id_cilindro_eto']) ? $_REQUEST['id_cilindro_eto'] : NULL;
    $id_user =$_SESSION['su'];
    $acc = $_SESSION['acc'];
    $tipo_eto_pev = isset($_REQUEST['tipo_eto_pev']) ? $_REQUEST['tipo_eto_pev'] : NULL;

	if(isset($_POST['g_cilindro_eto']))
	{
		$num_cili_eto1 = $_POST['num_cili_eto'];  
		$num_cili_eto_2 = $_POST['num_cili_eto_2'];
		$especi_eto1 = $_POST['especi_eto'];
		$pre_tra_eto1 = $_POST['pre_tra_eto'];
		$pre_pru_eto1 = $_POST['pre_pru_eto'];
		$fecha_fab_eto1 = $_POST['fecha_fab_eto'];  
		$fecha_ult_eto1 = $_POST['fecha_ult_eto'];
		$volumen_eto1 = $_POST['volumen_eto'];
		$volumen_actu1 = $_POST['volumen_actu'];
		$tara_esta_eto1 = $_POST['tara_esta_eto'];
		$tara_actu_eto1 = $_POST['tara_actu_eto'];
		$esp_fab_eto1 = $_POST['esp_fab_eto'];
		$esp_actu_eto1 = $_POST['esp_actu_eto'];
		$cone_val_eto1 = $_POST['id_conexion1'];
		$fecha_lav_eto1 = $_POST['fecha_lav_eto'];
		$id_propiedad_cilindro2 = $_POST['id_propiedad_cilindro1'];
		$tipo_cili_eto2 = $_POST['id_tipo_cili1'];
		$id_tipo_cilindro2 = $_POST['id_tipo_cilindro1'];
		$id_tipo_envace2 = $_POST['id_tipo_envace'];		
		$id_cliente1 = $_POST['id_cliente'];
		$id_especi_eto2 = $_POST['id_especificacion1'];
		$id_conversion2 = $_POST['id_conversion1'];
		$pre_trabajo1 = $_POST['pre_tra_eto_1'];
		$id_estado2 = $_POST['id_estado1'];	
		$tipo_eto_pev = $_POST['tipo_eto_pev'];	

		if(strlen($id_cilindro_eto) > 0)
		{
			if ($id_estado2==6) 
			{
				$id_estado3=1;	# code...
			}
			else
			{
				$id_estado3=$id_estado2;	
			}

			if ($id_estado2==11) 
			{
				$consulta = "INSERT INTO no_conformidad
			      		(id_cilindro_eto,origen,fecha_hora) 
			      		VALUES ('".$id_cilindro_eto."','Planta','".date("Y/m/d H:i:s")."')";
			    $resultado = mysqli_query($con,$consulta) ;
			}
			
	    	$consulta= "UPDATE cilindro_eto SET 
	                num_cili_eto = '$num_cili_eto1',
	                num_cili_eto_2 = '$num_cili_eto_2',
	                id_especi_eto = '$id_especi_eto2',
	                especi_eto = '$especi_eto1' ,
	                id_conversion = '$id_conversion2' ,
	                pre_trabajo = '$pre_trabajo1' ,
	                pre_tra_eto = '$pre_tra_eto1' ,
	                pre_pru_eto = '$pre_pru_eto1' ,
	                fecha_fab_eto = '$fecha_fab_eto1' ,
	                fecha_ult_eto = '$fecha_ult_eto1' ,
	                volumen_eto = '$volumen_eto1' ,
	                volumen_actu = '$volumen_actu1' ,
	                tara_esta_eto = '$tara_esta_eto1' ,
	                tara_actu_eto = '$tara_actu_eto1' ,
	                esp_fab_eto = '$esp_fab_eto1' ,
	                esp_actu_eto = '$esp_actu_eto1' ,
	                cone_val_eto = '$cone_val_eto1' ,
	                fecha_lav_eto = '$fecha_lav_eto1' ,
	                propiedad_eto = '$id_propiedad_cilindro2',
	                id_tipo_cilindro = '$id_tipo_cilindro2',
	                id_tipo_envace = '$id_tipo_envace2',
	                tipo_cili_eto = '$tipo_cili_eto2',
	                id_cliente = '$id_cliente1', 
	                id_estado = '$id_estado3'                                              
	                WHERE id_cilindro_eto = $id_cilindro_eto ";
	    	$resultado = mysqli_query($con,$consulta) ;

		    if ($resultado == FALSE)
		    {
		      	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    }
		    else
		    {
		    	seguimiento(1,$id_cilindro_eto,$id_user,1,0);//id_seguimiento,id_cilindro_eto,id_user,id_proceso,id_has_proceso
		      	if($tipo_eto_pev == 1)
			      {
			      	header('Location: busqueda_cilindros_eto.php');	
			      }
			      if($tipo_eto_pev == 2)
			      {
			      	header('Location: busqueda_cilindros_pev.php');	
			      }
		    }
	  	}
	    else
	    {
	    	$consulta2  = "SELECT num_cili_eto FROM cilindro_eto WHERE  num_cili_eto like '".$num_cili_eto1."' ";
		  	$resultado2 = mysqli_query($con,$consulta2) ;
		  	$linea2 = mysqli_fetch_array($resultado2);
		  	$num_cili_eto2 = isset($linea2["num_cili_eto"]) ? $linea2["num_cili_eto"] : NULL;	
		  	if ($num_cili_eto2==$num_cili_eto1) 
		  	{
		  		?>
		      	<script type="text/javascript">
		         	alert("Ya existe este Cilindro ");
		     	</script>
		     	<?php
		  	}
		  	else		  		# code...
		  	{ 
			  	$id_estado=1;
			    $consulta = "INSERT INTO cilindro_eto
			          		(num_cili_eto,
			          		num_cili_eto_2,
			          		id_especi_eto,
			          		especi_eto,
			          		id_conversion,
			          		pre_trabajo, 
			          		pre_tra_eto, 
			          		pre_pru_eto, 
			          		fecha_fab_eto, 
			          		fecha_ult_eto, 
			          		volumen_eto, 
			          		tara_esta_eto, 
			          		tara_actu_eto, 
			          		esp_fab_eto, 
			          		esp_actu_eto, 
			          		cone_val_eto, 
			          		fecha_lav_eto, 
			          		propiedad_eto, 
			          		tipo_cili_eto,
			          		id_tipo_cilindro,
			          		id_tipo_envace,
			          		volumen_actu,
			          		id_estado,
			          		id_cliente,
			          		fech_crea,
			          		tipo) 
			          		VALUES ('".$num_cili_eto1."',
			          				'".$num_cili_eto_2."',
			          				'".$id_especi_eto2."',
					          		'".$especi_eto1."', 
					          		'".$id_conversion2."', 
					          		'".$pre_trabajo1."', 
					          		'".$pre_tra_eto1."', 
					          		'".$pre_pru_eto1."', 
					          		'".$fecha_fab_eto1."', 
					          		'".$fecha_ult_eto1."', 
					          		'".$volumen_eto1."', 
					          		'".$tara_esta_eto1."', 
					          		'".$tara_actu_eto1."', 
					          		'".$esp_fab_eto1."', 
					          		'".$esp_actu_eto1."', 
					          		'".$cone_val_eto1."', 
					          		'".$fecha_lav_eto1."' , 
					          		'".$id_propiedad_cilindro2."' , 
					          		'".$tipo_cili_eto2."' ,
					          		'".$id_tipo_cilindro2."',
					          		'".$id_tipo_envace2."',
					          		'".$volumen_actu1."',
					          		'".$id_estado."',
					          		'".$id_cliente1."',
					          		'".date("Y/m/d")."',
					          		'".$tipo_eto_pev."')";
			    $resultado = mysqli_query($con,$consulta) ;
			    if ($resultado == FALSE)
			    {
			      echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
			    }
			    else
			    {
			      $id_cilindro_eto1 = mysqli_insert_id();
			      seguimiento(2,$id_cilindro_eto1,$id_user,1,0);//id_seguimiento,id_cilindro_eto,id_user,id_proceso,id_has_proceso
			      if($tipo_eto_pev == 1)
			      {
			      	header('Location: busqueda_cilindros_eto.php');	
			      }
			      if($tipo_eto_pev == 2)
			      {
			      	header('Location: busqueda_cilindros_pev.php');	
			      }
			    }
			}
	  	}	    
	}
	if(strlen($id_cilindro_eto) > 0)
	{ 
	  	$consulta  = "SELECT * FROM cilindro_eto WHERE id_cilindro_eto= $id_cilindro_eto";
	  	$resultado = mysqli_query($con,$consulta) ;
	  	$linea = mysqli_fetch_array($resultado);
		$num_cili_eto = isset($linea["num_cili_eto"]) ? $linea["num_cili_eto"] : NULL;  
		$num_cili_eto_2 = isset($linea["num_cili_eto_2"]) ? $linea["num_cili_eto_2"] : NULL;
		$especi_eto = isset($linea["especi_eto"]) ? $linea["especi_eto"] : NULL;
		$pre_tra_eto = isset($linea["pre_tra_eto"]) ? $linea["pre_tra_eto"] : NULL;
		$pre_pru_eto = isset($linea["pre_pru_eto"]) ? $linea["pre_pru_eto"] : NULL;
		$fecha_fab_eto = isset($linea["fecha_fab_eto"]) ? $linea["fecha_fab_eto"] : NULL;  
		$fecha_ult_eto = isset($linea["fecha_ult_eto"]) ? $linea["fecha_ult_eto"] : NULL;
		$volumen_eto = isset($linea["volumen_eto"]) ? $linea["volumen_eto"] : NULL;
		$tara_esta_eto = isset($linea["tara_esta_eto"]) ? $linea["tara_esta_eto"] : NULL;
		$tara_actu_eto = isset($linea["tara_actu_eto"]) ? $linea["tara_actu_eto"] : NULL;
		$esp_fab_eto = isset($linea["esp_fab_eto"]) ? $linea["esp_fab_eto"] : NULL;
		$esp_actu_eto = isset($linea["esp_actu_eto"]) ? $linea["esp_actu_eto"] : NULL;
		$id_conexion1 = isset($linea["cone_val_eto"]) ? $linea["cone_val_eto"] : NULL;
		$fecha_lav_eto = isset($linea["fecha_lav_eto"]) ? $linea["fecha_lav_eto"] : NULL;
		$id_propiedad_cilindro1 = isset($linea["propiedad_eto"]) ? $linea["propiedad_eto"] : NULL;
		$tipo_cili_eto1 = isset($linea["tipo_cili_eto"]) ? $linea["tipo_cili_eto"] : NULL;
		$id_tipo_cilindro1 = isset($linea["id_tipo_cilindro"]) ? $linea["id_tipo_cilindro"] : NULL;
		$id_tipo_envace = isset($linea["id_tipo_envace"]) ? $linea["id_tipo_envace"] : NULL;
		$id_tipo_cili1 = isset($linea["tipo_cili_eto"]) ? $linea["tipo_cili_eto"] : NULL;
		$volumen_actu = isset($linea["volumen_actu"]) ? $linea["volumen_actu"] : NULL;
		$id_estado1 = isset($linea["id_estado"]) ? $linea["id_estado"] : NULL;
		$id_cliente1 = isset($linea["id_cliente"]) ? $linea["id_cliente"] : NULL;
		$id_especificacion1 = isset($linea["id_especi_eto"]) ? $linea["id_especi_eto"] : NULL;
		$id_conversion1 = isset($linea["id_conversion"]) ? $linea["id_conversion"] : NULL;
		$tipo_eto_pev = isset($linea["tipo"]) ? $linea["tipo"] : NULL;
		$id_tipo_gas_pev1 = isset($linea["id_tipo_gas_pev"]) ? $linea["id_tipo_gas_pev"] : NULL;
		$pre_trabajo = isset($linea["pre_trabajo"]) ? $linea["pre_trabajo"] : NULL;
		$fech_crea = isset($linea["fech_crea"]) ? $linea["fech_crea"] : NULL;
		mysqli_free_result($resultado);
		if ($resultado == true)
	    {
	      	$consulta1  = "SELECT * FROM estado_cilindro_eto WHERE id_estado_cilindro = $id_estado1";
		  	$resultado1 = mysqli_query($con,$consulta1) ;
		  	$linea1 = mysqli_fetch_array($resultado1);
		  	$id_estado2 = isset($linea1["estado_cili_eto"]) ? $linea1["estado_cili_eto"] : NULL;
		  	mysqli_free_result($resultado1);

		  	$consulta2  = "SELECT * FROM clientes WHERE id_cliente = $id_cliente1";
		  	$resultado2 = mysqli_query($con,$consulta2) ;
		  	$linea2 = mysqli_fetch_array($resultado2);
		  	$id_cliente3 = isset($linea2["nombre"]) ? $linea2["nombre"] : NULL;
		  	mysqli_free_result($resultado2);

		  	$consulta3  = "SELECT * FROM tipo_envace WHERE id_tipo_envace = ".$id_tipo_envace;
		  	$resultado3 = mysqli_query($con,$consulta3) ;
		  	$linea3 = mysqli_fetch_array($resultado3);
		  	$tipo = isset($linea3["tipo"]) ? $linea3["tipo"] : NULL;

		  	mysqli_free_result($resultado3);	
	    }		
	    
	}

require_once("inc/init.php");
require_once("inc/config.ui.php");
$page_title = "Cilindros-ETO";
$page_css[] = "your_style.css";
include("inc/header.php");
$page_nav[""][""][""][""] = true;
include("inc/nav.php");
?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
	<div id="content">
		<section id="widget-grid" class="">
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-6">
					<div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h6>Datos Básicos Cilindro </h6>			
						</header>
						<div>
							<div class="jarviswidget-editbox"></div>
							<div class="widget-body no-padding">
								<form id="form1" class="smart-form" novalidate="novalidate" action="cilindro_eto.php" method="POST" name="form1">
									<input type="hidden" name="id_cilindro_eto" id="id_cilindro_eto" value="<?php echo $id_cilindro_eto; ?>">
									<input type="hidden" name="id_cliente" id="id_cliente" value="<?php echo $id_cliente; ?>">
									<input type="hidden" name="tipo_eto_pev" id="tipo_eto_pev" value="<?php echo $tipo_eto_pev; ?>">
									<fieldset>
										<div class="row">												
											<section class="col col-5">
												<label class="font-lg">Número Cilindro :</label>
												<label class="input"> 
													<input type="text" class="input-lg" name="num_cili_eto" placeholder="Número Cilindro" title="Ingrese un Número Cilindro" required value="<?php echo isset($num_cili_eto) ? $num_cili_eto : NULL; ?>" >
												</label>
											</section>
											<?php
											if($tipo_eto_pev == 2)
											{
												?>
												<section class="col col-7">
													<label class="font-lg">Número Cilindro Confirmado:</label>
													<label class="input"> 
														<input type="text" class="input-lg" name="num_cili_eto_2" placeholder="Número Cilindro Confirmado" title="Ingrese un Número Cilindro" required value="<?php echo isset($num_cili_eto_2) ? $num_cili_eto_2 : NULL; ?>" >
													</label>
												</section>
												<?php
											}
											?>
											<section class="col col-3">	
												<?php
													$consulta6 ="SELECT * 
						                                      FROM especificacion ORDER BY especificacion ";
						                            $resultado6 = mysqli_query($con,$consulta6) ;	
													echo "<section>";
													echo "<label class='font-lg'>Especificación:</label>";
													echo"<label class='select'>";
													echo "<select name='id_especificacion1' class='input-lg'  onChange='conversion()'  >";
													echo "<option value='0'>Seleccione...</option>";
													while($linea6 = mysqli_fetch_array($resultado6))
					                                    {
					                                      $id_especificacion = $linea6['id_especificacion'];
					                                      $especificacion = $linea6['especificacion'];
					                                      if ($id_especificacion==$id_especificacion1)
					                                      {
					                                          echo "<option value='$id_especificacion' selected >$especificacion</option>"; 
					                                      }
					                                      else 
					                                      {
					                                          echo "<option value='$id_especificacion'>$especificacion</option>"; 
					                                      } 
					                                    }//fin while
													echo "</select>";
													echo "<i></i>";
													echo "</label>";
													echo "</section>";																
												?>
											</section>												
											<section class="col col-3">
												<label class="font-lg">&nbsp  </label>
												<label class="input"> 
													<input type="text" class="input-lg" title="Especificación" class="input-lg" name="especi_eto"  placeholder="Especificación " value="<?php echo isset($especi_eto) ? $especi_eto : NULL; ?>" required>
												</label>
											</section>
								
										</div>
										<div class="row">												
											<section class="col col-6">	
												<section>
													<label class="font-lg">Tipo Gas :</label>
													<label class='select'>
														<select name='id_tipo_cilindro1' class="input-lg" onChange='cilindro(this.value)' title='Ingrese un Número Cilindro' required>
															<option value='0'>Seleccione...</option>
															<?php
															if($tipo_eto_pev == 1)
															{
																echo $consulta6 ="SELECT * FROM tipo_cilindro ";
																$resultado6 = mysqli_query($con,$consulta6) ;
															
																while($linea6 = mysqli_fetch_array($resultado6))
								                                {
								                                  $id_tipo_cilindro = $linea6['id_tipo_cilindro'];
								                                  $tipo_cili = $linea6['tipo_cili'];
								                                  if ($id_tipo_cilindro==$id_tipo_cilindro1)
								                                  {
								                                      echo "<option value='$id_tipo_cilindro' selected >$tipo_cili</option>"; 
								                                  }
								                                  else 
								                                  {
								                                      echo "<option value='$id_tipo_cilindro'>$tipo_cili</option>"; 
								                                  } 
								                                }//fin while
								                            }
								                            if($tipo_eto_pev == 2)
								                            {
								                            	$consulta6 ="SELECT * FROM tipo_gas_pev ORDER BY nombre ASC";
																$resultado6 = mysqli_query($con,$consulta6) ;
															
																while($linea6 = mysqli_fetch_array($resultado6))
								                                {
								                                  $id_tipo_gas_pev = $linea6['id_tipo_gas_pev'];
								                                  $nombre = $linea6['nombre'];
								                                  if ($id_tipo_gas_pev==$id_tipo_gas_pev1)
								                                  {
								                                      echo "<option value='$id_tipo_gas_pev' selected >$nombre</option>"; 
								                                  }
								                                  else 
								                                  {
								                                      echo "<option value='$id_tipo_gas_pev'>$nombre</option>"; 
								                                  } 
								                                }//fin while
								                            }
							                                ?> 
														</select>
														<i></i>
													</label>
												</section>											
												
											</section>
											<section class="col col-6">
												<section>
												<label class="font-lg">Capacidad :</label>
												<label class='select'>
												<select name="id_tipo_envace" class="input-lg">
												<option value="0">--seleccione--</option>
												<?php															
			                                      	if(isset($id_tipo_envace))
													{
														echo "<option selected=\"selected\" value=\"".$id_tipo_envace."\">".$tipo."</option>";
													}
												?>
												</select>
												<i></i>
												</label>
												</section>	
											</section>									
										</div>
										<div class="row">
											<section class="col col-6">	
												<?php
													$consulta6 ="SELECT * 
						                                      FROM conversion";
						                            $resultado6 = mysqli_query($con,$consulta6) ;	
													echo "<section>";
													echo "<label class='font-lg'>Unidad :</label>";
													echo"<label class='select'>";
													echo "<select name='id_conversion1' class='input-lg' onChange='conversion()'  >";
													echo "<option value='0'>Seleccione...</option>";
													while($linea6 = mysqli_fetch_array($resultado6))
					                                    {
					                                      $id_conversion = $linea6['id_conversion'];
					                                      $conversion = $linea6['conversion'];
					                                      if ($id_conversion==$id_conversion1)
					                                      {
					                                          echo "<option value='$id_conversion' selected >$conversion</option>"; 
					                                      }
					                                      else 
					                                      {
					                                          echo "<option value='$id_conversion'>$conversion</option>"; 
					                                      } 
					                                    }//fin while
													echo "</select>";
													echo "<i></i>";
													echo "</label>";
													echo "</section>";																
												?>
											</section>												
											<section class="col col-6">
												<label class="font-lg">Pre Trabajo :</label>
												<label class="input"> 
													<input type="text" name="pre_tra_eto_1" class="input-lg"  placeholder="Presión Trabajo" onKeyUp='conversion()' onkeypress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" value="<?php echo isset($pre_trabajo) ? $pre_trabajo : NULL; ?>">
												</label>
											</section>
										</div>
										<div class="row">												
											<section class="col col-6">
												<label class="font-lg">Presión Trabajo (Psi) :</label>
												<label class="input"> 
													<input type="text" class="input-lg" name="pre_tra_eto" readonly placeholder="Presión Trabajo"  value="<?php echo isset($pre_tra_eto) ? $pre_tra_eto : NULL; ?>" onkeypress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;">
												</label>
											</section>
											<section class="col col-6">
												<label class="font-lg">Presión Prueba (Psi) :</label>
												<label class="input"> 
													<input type="text" class="input-lg" name="pre_pru_eto" readonly placeholder="Presión Prueba"   value="<?php echo isset($pre_pru_eto) ? $pre_pru_eto : NULL; ?>" onkeypress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;">
												</label>
											</section>										
										</div>
										<div class="row">												
											<section class="col col-6">
												<label class="font-lg">Fecha Fabricación :</label>
												<label class="input"> 
													<input type="date" class="input-lg" name="fecha_fab_eto" placeholder="Fecha Fabricación"  title="Ingrese una Fecha de Fabricación" required value="<?php echo isset($fecha_fab_eto) ? $fecha_fab_eto : NULL; ?>">
												</label>
											</section>
											<section class="col col-6">
												<label class="font-lg">Fecha Última Revisión (PH):</label>
												<label class="input"> 
													<input type="date" class="input-lg" name="fecha_ult_eto" placeholder="Fecha Última Revisión"  title="Ingrese una Fecha de Última Revisión" required value="<?php echo isset($fecha_ult_eto) ? $fecha_ult_eto : NULL; ?>">
												</label>
											</section>																						
										</div>
										<div class="row">												
											<section class="col col-6">
												<?php
												if($tipo_eto_pev == 1)
												{
													?>
													<label class="font-lg">Volumen Actual (Lt):</label>	
													<?php
												}
												else
												{
													?>
													<label class="font-lg">Volumen Estampado Cilindro (Lt):</label>	
													<?php	
												}
												?>
												<label class="input"> 
													<input type="text" class="input-lg" name="volumen_actu" placeholder="Volumen"  title="Ingrese el Volumen Actual" required value="<?php echo isset($volumen_actu) ? $volumen_actu : NULL; ?>" onkeypress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;">
												</label>
											</section>
											<section class="col col-6">
												<label class="font-lg">Volumen Revisado(Lt):</label>
												<label class="input"> 
													<input type="text" class="input-lg" name="volumen_eto" placeholder="Volumen"  title="Ingrese el Volumen" required value="<?php echo isset($volumen_eto) ? $volumen_eto : NULL; ?>" onkeypress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;">
												</label>
											</section>									
										</div>
										<div class="row">												
											<section class="col col-6">
												<?php
												if($tipo_eto_pev == 1)
												{
													?>
													<label class="font-lg">Tara Actual (Kg):</label>
													<?php
												}
												else
												{
													?>
													<label class="font-lg">Tara Estampada (Kg):</label>
													<?php	
												}
												?>
												<label class="input"> 
													<input type="text" class="input-lg" name="tara_esta_eto" placeholder="Tara Estampada"  title="Ingrese la Tara Estampada" required value="<?php echo isset($tara_esta_eto) ? $tara_esta_eto : NULL; ?>" onkeypress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;">
												</label>
											</section>
											<section class="col col-6">
												<label class="font-lg">Tara Verificada (Kg):</label>
												<label class="input"> 
													<input type="text" class="input-lg" name="tara_actu_eto" placeholder="Tara Actual"  title="Ingrese la Tara Actual" required value="<?php echo isset($tara_actu_eto) ? $tara_actu_eto : NULL; ?>" onkeypress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;">
												</label>
											</section>										
										</div>
										<div class="row">												
											<section class="col col-6">
												<label class="font-lg">Espesor Fabricación (mm):</label>
												<label class="input"> 
													<input type="text" class="input-lg" name="esp_fab_eto" placeholder="Espesor Fabricación"  title="Ingrese el Espesor Fabricación" required value="<?php echo isset($esp_fab_eto) ? $esp_fab_eto : NULL; ?>" onkeypress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;">
												</label>
											</section>
											<section class="col col-6">
												<label class="font-lg">Espesor Actual (mm):</label>
												<label class="input"> 
													<input type="text" class="input-lg" name="esp_actu_eto" placeholder="Espesor Actual"  title="Ingrese el Espesor Actual" required value="<?php echo isset($esp_actu_eto) ? $esp_actu_eto : NULL; ?>" onkeypress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;">
												</label>
											</section>										
										</div>
										<div class="row">												
											<section class="col col-6">	
												<?php
												$consulta6 ="SELECT * FROM tipo_cili ORDER BY tipo ASC";
												$resultado6 = mysqli_query($con,$consulta6) ;
												echo "<section>";
												echo "<label class='font-lg'>Tipo de Cilindro :</label>";
												echo"<label class='select'>";
												echo "<select class='input-lg' name='id_tipo_cili1' ¿>";
												echo "<option value='0'>Seleccione...</option>";
												while($linea6 = mysqli_fetch_array($resultado6))
												{
													$id_tipo_cili = $linea6['id_tipo_cili'];
													$tipo = $linea6['tipo'];
													if ($id_tipo_cili==$id_tipo_cili1)
													{
															echo "<option value='$id_tipo_cili' selected >$tipo</option>"; 
													}
													else 
													{
															echo "<option value='$id_tipo_cili'>$tipo</option>"; 
													} 
												}//fin while 
												echo "</select>";
												echo "<i></i>";
												echo "</label>";
												echo "</section>";																
												?>
											</section>
											<section class="col col-6">	
												<?php

												if($tipo_eto_pev == 1)
												{
													$consulta6 ="SELECT * FROM conexion_valvula ORDER BY tip_cone ASC";
													$resultado6 = mysqli_query($con,$consulta6) ;
													echo "<section>";
													echo "<label class='font-lg'>Conexión Valvula :</label>";
													echo"<label class='select'>";
													echo "<select class='input-lg' name='id_conexion1'>";
													echo "<option value='0'>Seleccione...</option>";
													while($linea6 = mysqli_fetch_array($resultado6))
													{
														$id_conexion = $linea6['id_conexion'];
														$tip_cone = $linea6['tip_cone'];
														if ($id_conexion==$id_conexion1)
														{
																echo "<option value='$id_conexion' selected >$tip_cone</option>"; 
														}
														else 
														{
																echo "<option value='$id_conexion'>$tip_cone</option>"; 
														} 
													}//fin while 
													echo "</select>";
													echo "<i></i>";
													echo "</label>";
													echo "</section>";	
												}
												elseif ($tipo_eto_pev == 2) 
												{
													?>
													<section>
														<label class="font-lg">Conexión Válvula:</label>
														<label class="select">
															<select class="input-lg" name="id_conexion1">
																<?php
																	$consulta = "SELECT * FROM conexion_valvula_pev";
																	$resultado = mysqli_query($con,$consulta);
																	if(mysqli_num_rows($resultado) > 0)
																	{
																		while($linea = mysqli_fetch_assoc($resultado))
																		{
																			$id_valvula_pev = $linea['id_valvula_pev'];
																			$tipo_valvula = $linea['tipo_valvula'];
																			if($id_valvula_pev == $id_conexion1)
																			{
																				echo "<option value='$id_valvula_pev' selected >$tipo_valvula</option>";
																			}
																			else
																			{
																				echo "<option value='$id_valvula_pev'>$tipo_valvula</option>"; 
																			}
																		}
																	}
																?>
															</select>
														</label>
													</section>


													<?php
												}															
												?>
											</section>												
										</div>										
										<div class="row">
											<section class="col col-6">
												<label class="font-lg">Fecha De Lavado :</label>
												<label class="input"> 
													<input type="date" class="input-lg" name="fecha_lav_eto" placeholder="Conexión Válvula"  value="<?php echo isset($fecha_lav_eto) ? $fecha_lav_eto : NULL; ?>" title="Ingrese la fecha de lavado" required>
												</label>
											</section>												
											<section class="col col-6">	
												<?php
												$consulta6 ="SELECT * FROM propiedad_cilindro ORDER BY propiedad ASC";
												$resultado6 = mysqli_query($con,$consulta6) ;
												echo "<section>";
												echo "<label class='font-lg'>Propiedad :</label>";
												echo"<label class='select'>";
												echo "<select class='input-lg' name='id_propiedad_cilindro1' onChange='cliente(this.value)'>";
												echo "<option value='0'>Seleccione...</option>";
												while($linea6 = mysqli_fetch_array($resultado6))
												{
													$id_propiedad_cilindro = $linea6['id_propiedad_cilindro'];
													$propiedad = $linea6['propiedad'];
													if ($id_propiedad_cilindro==$id_propiedad_cilindro1)
													{
															echo "<option value='$id_propiedad_cilindro' selected >$propiedad</option>"; 
													}
													else 
													{
															echo "<option value='$id_propiedad_cilindro'>$propiedad</option>"; 
													} 
												}//fin while 
												echo "</select>";
												echo "<i></i>";
												echo "</label>";
												echo "</section>";
															
												?>
											</section>												
										</div>
										<div class="row" >
											<section class="col col-6" id="cili">
												<label class="font-lg">Cliente:</label>
												<label class="input"> 
													<input type="text" class="input-lg" name="nombre" placeholder="Cliente"  readonly  value="<?php echo isset($id_cliente3) ? $id_cliente3 : NULL; ?>">
												</label>
											</section>
											<section class="col col-6">
												<label class="font-lg">Creación :</label>
												<label class="input"> 
													<input type="date" class="input-lg" name="fech_crea" placeholder="Cliente" readonly  value="<?php echo isset($fech_crea) ? $fech_crea : NULL; ?>">
												</label>
											</section>													
										</div>
										<?php
										if ($id_cilindro_eto>0) 
										{
										?>
											<div class="row" >
												<section class="col col-6">	
													<?php
													$consulta6 ="SELECT * FROM estado_cilindro_eto ORDER BY estado_cili_eto ASC";
													$resultado6 = mysqli_query($con,$consulta6) ;
													echo "<section>";
													echo "<label class='font-lg'>Estado :</label>";
													echo"<label class='select'>";
													echo "<select class='input-lg' name='id_estado1'>";
													echo "<option value='0'>Seleccione...</option>";
													while($linea6 = mysqli_fetch_array($resultado6))
													{
														$id_estado_cilindro = $linea6['id_estado_cilindro'];
														$estado_cili_eto = $linea6['estado_cili_eto'];
														if ($id_estado_cilindro==$id_estado1)
														{
															echo "<option value='$id_estado_cilindro' selected >$estado_cili_eto</option>"; 
														}
														else 
														{
															echo "<option value='$id_estado_cilindro'>$estado_cili_eto</option>"; 
														} 
													}//fin while 
													echo "</select>";
													echo "<i></i>";
													echo "</label>";
													echo "</section>";
																
													?>
												</section>													
											</div>
										<?php
										}
										?>
									</fieldset>		
									<?php
									if (in_array(14, $acc))
									{
									?>							
										<footer>                    
					                    	<input type="submit" value="Guardar" name="g_cilindro_eto" id="g_cilindro_eto"  onclick="return confirmar()"class="btn btn-primary" />
					                  	</footer>
				                  	<?php
									}										
									?>
                				</form>								
							
             				</div>			              
				        </div>            
         			</div> 
        		</article>
     		</div>
   		</section> 
   		    <!-- Modal -->
	    <div class="modal fade" id="modal_cliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">
			<form name="mezcla" method="POST" id="mezcla">
			<input type="hidden" name="id_cilindro_eto"  value="id_cilindro_eto">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							&times;
							</button>
							<h4 class="modal-title" id="myModalLabel">Seleccione el Cliente</h4>
						</div>
						<div class="modal-body">
							<div class="row">
								<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">						
									<form  name="form2" id="form2">							
										<thead>
											<tr>
												<th>#</th>
												<th>Acción</th>
												<th>Cliente</th>
												<th>Nit</th>									
											</tr>
										</thead>
										<tbody>										
											<?php                                
			                        			$contador = "0";
			                        	        $consulta = "SELECT * FROM clientes ";
			                                    $resultado = mysqli_query($con,$consulta) ;
			                                    while ($linea = mysqli_fetch_array($resultado))
			                                    {
			                                        $contador = $contador + 1;
			                                        $id_cliente = $linea["id_cliente"];
			                                        $nombre = $linea["nombre"];
			                                        $telefono_fijo = $linea["telefono_fijo"];
			                                        $telefono_celular = $linea["telefono_celular"];
			                                        $correo = $linea["correo"];
			                                        $persona_contacto = $linea["persona_contacto"];
			                                        $direccion = $linea["direccion"];
			                                        $observacion = $linea["observacion"];
			                                        $nit = $linea["nit"];
			                             
			                                    ?>
			                                        <tr>                                                	
			                                            <td width="5"><?php echo $contador; ?></td>
			                                            <td width="5"><a href="#" onclick="g_cliente(<?php echo $id_cliente; ?>)"><img src="img/chulo.png" width="20" height="20"></a></td>
			                                            <td><?php echo $nombre; ?></td>
			                                            <td><?php echo $nit; ?></td>                                            
			                                    	</tr>   
			                                    <?php
			                                    }mysqli_free_result($resultado);   
			                                	
			                                ?>
										</tbody>
									</form>
								</table>
							</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">	Cancel</button>	
						</div>
					</div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			</form>
		</div>
		<!-- Fin Modal -->
  	</div>
</div>



<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->
<?php
  include("inc/footer.php");
  include("inc/scripts.php"); 
?>
<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/jquery-form/jquery-form.min.js"></script>
<script src="js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<script type="text/javascript">
function confirmar()
{
	//return false;
	fecha_ult_eto=form1.fecha_ult_eto.value;
	fecha_lav_eto=form1.fecha_lav_eto.value;
	var txt;
    var r = confirm("Esta seguro de Crear el Cilindro ?\n Fecha lavado ="+fecha_lav_eto+"\nFecha PH ="+fecha_ult_eto);
    if (r == true) {
        submit.form1();
        //txt = "You pressed OK!";
    } else {
        return false;
    }
}
</script>
<script type="text/javascript">

  function cilindro(id_tipo_cilindro1)
  {
    document.form1.id_tipo_envace.length=0;
    document.form1.id_tipo_envace.options[0] = new Option("Seleccione...","0","defaultSelected","");
    var indice=1;
    <?php
    $consulta = "SELECT * 
          FROM tipo_envace 
          ORDER BY tipo";
    $resultado = mysqli_query($con,$consulta);
    if(mysqli_num_rows($resultado)>0)
    {
      while($linea = mysqli_fetch_assoc($resultado))
      {
        ?>
        if(id_tipo_cilindro1=='<?php echo $linea["id_tipo_cilindro"]; ?>')
        {
          document.form1.id_tipo_envace.options[indice] = new Option("<?php echo $linea["tipo"]; ?>","<?php echo $linea["id_tipo_envace"]?>");
          indice++;
        }
        <?php
      }
    }
    ?>
  }

</script>
<script type="text/javascript">
function cliente(clientes) 
{	
	
	if (clientes==1) 
	{
		//alert(clientes);
		$("#modal_cliente").modal();
		$("#cili").show();
	}
	else
	{
		$("#cili").hide();
	}
}	

</script>
<script type="text/javascript">


function conversion()
{
		id_conversion=form1.id_conversion1.value;	
		id_especificacion=form1.id_especificacion1.value;
		//var id_especificacion = document.getElementById("id_especificacion1").value;
		//alert(id_especificacion);	
		
		if (id_especificacion==1) 
		{
			valor_espe= (5/3);
		}
		if (id_especificacion==2) 
		{
			valor_espe= (5/3);
		}
		if (id_especificacion==3) 
		{
			valor_espe= 1.5;
		}
		if (id_especificacion==4) 
		{
			valor_espe= 1.5;
		}
		if (id_conversion==1) 
		{
			valor= 14.5038;
		}
		if (id_conversion==2) 
		{
			valor= 0.000145038;
		}
		if (id_conversion==3) 
		{
			valor= 145.037699722;
		}
		if (id_conversion==4) 
		{
			valor= 14.6959;
		}
		if (id_conversion==5) 
		{
			valor= 1;
		}		
		//alert(valor_espe);
		
	    form1.pre_tra_eto.value = (((parseInt(form1.pre_tra_eto_1.value))*valor)).toFixed(2);
	    form1.pre_pru_eto.value = (((parseInt(form1.pre_tra_eto_1.value))*valor)*valor_espe).toFixed(2);
}
function conversion2()
{
		id_conversion1=form1.id_conversion2.value;

		if (id_conversion1==1) 
		{
			valor= 14.5038;
		}
		if (id_conversion1==2) 
		{
			valor= 0.000145038;
		}
		if (id_conversion1==3) 
		{
			valor= 0.0193368;
		}
		if (id_conversion1==4) 
		{
			valor= 14.6959;
		}
		if (id_conversion1==5) 
		{
			valor= 1;
		}	
	     
	    form1.pre_pru_eto.value = ((parseInt(form1.pre_pru_eto_1.value))*valor).toFixed(2);      
	     

	    
}

function g_cliente(num_cili1) 
{
	//alert("hola");
	//var num_cili1 = $('input:text[name=cliente]').val();

	$('#modal_cliente').modal('hide');
	$('input:hidden[name=id_cliente]').val(num_cili1);

	$.ajax({
		url: 'id_cliente.php',
		type: 'POST',
		data: 'dato='+num_cili1,
	})
	.done(function(data) {
		var objeto = JSON.parse(data);							
		
		$("input[name=nombre]").val(objeto.nombre);							
		
		document.getElementById("form2").reset();

	})
	.fail(function() {
		console.log("error");
	});
	
}

</script>



<?php 
  //include footer
  include("inc/google-analytics.php"); 
}
else
{
    header("Location:index.php");
}
?>
					                       
										