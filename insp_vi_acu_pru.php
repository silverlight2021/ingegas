<?php
session_start();
date_default_timezone_set("America/Bogota");
$Fecha = date("Y-m-d");
require ("libraries/conexion.php");

$motivo_condena = "NINGUNO";
$estado_prueba_i = "APROBADO";
$prueba_realizada = 0;
if($_SESSION['logged']== 'yes')
{ 
	$id_has_movimiento_cilindro_pev = isset($_REQUEST['id_has_movimiento_cilindro_pev']) ? $_REQUEST['id_has_movimiento_cilindro_pev'] : NULL;
	$idUser =$_SESSION['su'];
	$acc = $_SESSION['acc'];

	require_once("inc/init.php");
	require_once("inc/config.ui.php");
	$page_title = "Inspección Visual Acumulador";
	$page_css[] = "your_style.css";
	include("inc/header.php");
	$page_nav[""][""][""][""] = true;
	include("inc/nav.php");

	$consulta0 = "SELECT * FROM has_movimiento_cilindro_pev WHERE id_has_movimiento_cilindro_pev = '$id_has_movimiento_cilindro_pev'";
	$resultado0 = mysqli_query($con,$consulta0);

	$linea0 = mysqli_fetch_assoc($resultado0);
	$num_cili = $linea0["num_cili"];

	$nva_consul = "SELECT * FROM cilindro_pev WHERE num_cili_pev = '$num_cili'";
	$resul_consul = mysqli_query($con,$nva_consul);
	$linea_consul = mysqli_fetch_assoc($resul_consul);

	$id_cili = $linea_consul["id_cilindro_pev"];
	$costura = $linea_consul["costura"];

	if (isset($_POST['g_insp_visual_acero'])) //guardar información de inspección visual de acero
	{

		$estado_prueba = $_POST['estado_prueba'];
		

		if($estado_prueba == "INSERVIBLE"){
			$consulta_u = "UPDATE cilindro_pev SET id_estado = 18 WHERE id_cilindro_pev = '$id_cili'";
			
		}else if($estado_prueba == "RECHAZADO"){
			$consulta_u = "UPDATE cilindro_pev SET id_estado = 14 WHERE id_cilindro_pev = '$id_cili'";
			
		}else{
			$consulta_u = "UPDATE cilindro_pev SET id_estado = 5 WHERE id_cilindro_pev = '$id_cili'";
			
		}

		$resultado_u = mysqli_query($con,$consulta_u);

		$id_has_movimiento_cilindro_pev = $_POST['id_has_movimiento_cilindro_pev'];
		$estado_prueba = $_POST['estado_prueba'];
		$bajo_relieve = isset($_POST['bajo_relieve_h']) ? $_POST['bajo_relieve_h'] : 2;
		$marcas_sospechosas = $_POST['marcas_sospechosas_h'];
		$tapon_cuello = $_POST['tapon_cuello_h'];
		$estabilidad_vertical = $_POST['estabilidad_h'];
		$abultamiento = $_POST['abultamiento_h'];
		$grieta = $_POST['grieta_h'];
		$dano_fuego = $_POST['dano_fuego_h'];
		$arco_antorcha = $_POST['arco_antorcha_h'];
		$retroceso = $_POST['retroceso_h'];
		$abolladuras = $_POST['abolladuras_h'];
		$corte_agujero = $_POST['corte_agujero_h'];
		$corrosion_general = $_POST['corrosion_general_h'];
		$corrosion_local = $_POST['corrosion_local_h'];
		$corrosion_cadena = $_POST['corrosion_cadena_h'];
		$corrosion_grieta = $_POST['corrosion_grieta_h'];
		$contaminacion = $_POST['contaminacion_h'];
		$masas = $_POST['masas_h'];
		$motivo_condena = $_POST['motivo_condena'];
		$observaciones = $_POST["observaciones"];
		$fusibles = $_POST["fusibles_h"];
		$estado_fusibles = $_POST["estado_fusibles_h"];
		$roscas_valv = $_POST["roscas_valv_h"];
		$roscas_internas = $_POST["roscas_internas_h"];
		$collarin_collar = $_POST["collarin_collar_h"];
		$defecto_masa = $_POST["defecto_masa"];
		$masas_no = $_POST["masas_no_h"];

		if(strlen($defecto_masa) == 0){
			$defecto_masa = 0;
		}


		$consulta1 = "INSERT INTO insp_vi_acu_pev (
							id_has_movimiento_cilindro_pev , 
							num_cili_pev ,
							estado_prueba ,
							bajo_relieve ,
							marcas ,
							tapon_cuello , 
							estabilidad_vertical ,
							abultamiento ,
							grieta ,
							fuego ,
							arco_antorcha ,
							retroceso ,
							abolladuras , 
							corte_agujero ,
							corrosion_general ,
							corrosion_local ,
							corrosion_cadena ,
							corrosion_grieta ,
							contaminacion ,
							masas ,
							motivo_condena,
							observaciones,
							fusibles,
							estado_fusibles,
							roscas_valv,
							roscas_internas,
							collarin_collar,
							defecto_masa,
							masas_no
							) VALUES ($id_has_movimiento_cilindro_pev, 
									'$num_cili',
									'$estado_prueba', 
									$bajo_relieve, 
									$marcas_sospechosas, 
									$tapon_cuello, 
									$estabilidad_vertical, 
									$abultamiento, 
									$grieta, 
									$dano_fuego, 
									$arco_antorcha, 
									$retroceso, 
									$abolladuras, 
									$corte_agujero, 
									$corrosion_general, 
									$corrosion_local, 
									$corrosion_cadena,
									$corrosion_grieta,
									$contaminacion,
									$masas,
									'$motivo_condena',
									'$observaciones',
									$fusibles,
									$estado_fusibles,
									$roscas_valv,
									$roscas_internas,
									$collarin_collar,
									$defecto_masa,
									$masas_no)";

		if(mysqli_query($con,$consulta1))
			{
				?>
					<script type="text/javascript">
						var id_orden_pev = '<?php echo $id_orden_pev; ?>';
						alert("Inspección guardada correctamente.")
						window.location = 'insp_vi_acu_pru.php?id_has_movimiento_cilindro_pev='+<?php echo $id_has_movimiento_cilindro_pev; ?>;
					</script>
				<?php
			}
			else
			{
				echo "Error: " . $consulta1 . "<br>" . mysqli_error($con);
			}
	}

	 //Consulta el responsable de la inspección
	$consulta = "SELECT Name, LastName FROM user WHERE idUser = '".$idUser."'";
	$resultado = mysqli_query($con,$consulta);
	if(mysqli_num_rows($resultado) > 0 )
	{
	  	$linea = mysqli_fetch_assoc($resultado);
		$Name = $linea['Name'];
		$LastName = $linea['LastName'];
		$responsable = $Name." ".$LastName;
	}
	 //Si existe el cilindro se consultan datos
	if(strlen($id_has_movimiento_cilindro_pev) > 0)
	{
		$consulta3 = "SELECT num_cili, id_tipo_gas_pev, id_transporte_pev
						FROM has_movimiento_cilindro_pev 
						WHERE id_has_movimiento_cilindro_pev = '".$id_has_movimiento_cilindro_pev."'";
		$resultado3 = mysqli_query($con,$consulta3);
		if(mysqli_num_rows($resultado3) > 0)
		{
			$linea3 = mysqli_fetch_assoc($resultado3);
			$num_cili = $linea3['num_cili'];
			$id_tipo_gas_pev = $linea3['id_tipo_gas_pev'];
			$id_transporte_pev = $linea3['id_transporte_pev'];

			$consulta10 = "SELECT esp_fab_pev, esp_actu_pev FROM cilindro_pev WHERE num_cili_pev = '".$num_cili."'";
			$resultado10 = mysqli_query($con,$consulta10);
			if(mysqli_num_rows($resultado10) > 0)
			{
				$linea10 = mysqli_fetch_assoc($resultado10);
				$esp_fab_pev = isset($linea10["esp_fab_pev"]) ? $linea10["esp_fab_pev"] : NULL;
				$esp_actu_pev = isset($linea10['esp_actu_pev']) ? $linea10["esp_fab_pev"] : NULL;

				if($esp_fab_pev == NULL){
					$esp_fab_pev = "DESCONOCIDO";
				}
				if($esp_actu_pev == NULL){
					$esp_actu_pev = "DESCONOCIDO";
				}
			}
			$consulta4 = "SELECT id_cliente,fecha FROM transporte_pev WHERE id_transporte_pev = '".$id_transporte_pev."'";
			$resultado4 = mysqli_query($con,$consulta4);
			if(mysqli_num_rows($resultado4) > 0)
			{
				$linea4 = mysqli_fetch_assoc($resultado4);
				$fecha = $linea4['fecha'];
				$id_cliente = $linea4['id_cliente'];

				$consulta8 = "SELECT nombre FROM clientes WHERE id_cliente = '".$id_cliente."'";
				$resultado8 = mysqli_query($con,$consulta8);
				if(mysqli_num_rows($resultado8) > 0)
				{
					$linea8 = mysqli_fetch_assoc($resultado8);
					$nombre_cliente = $linea8['nombre'];
				}
			}
			$consulta5 = "SELECT nombre FROM tipo_gas_pev WHERE id_tipo_gas_pev = '".$id_tipo_gas_pev."'";
			$resultado5 = mysqli_query($con,$consulta5);
			if(mysqli_num_rows($resultado5) > 0)
			{
				$linea5 = mysqli_fetch_assoc($resultado5);
				$nombre = $linea5['nombre'];
			}            
				
			$consulta6 = "SELECT id_especi_pev, especi_pev, num_cili_pev_2 FROM cilindro_pev WHERE num_cili_pev = '".$num_cili."'";
			$resultado6 = mysqli_query($con,$consulta6);
			if(mysqli_num_rows($resultado6) > 0)
			{
				$linea6 = mysqli_fetch_assoc($resultado6);
				$id_especi_pev = $linea6['id_especi_pev'];
				$especi_pev = $linea6['especi_pev'];
				$num_cili_pev_2 = $linea6['num_cili_pev_2'];

				$consulta7 = "SELECT especificacion FROM especificacion WHERE id_especificacion = '".$id_especi_pev."'";
				$resultado7 = mysqli_query($con,$consulta7);
				if(mysqli_num_rows($resultado7))
				{
					$linea7 = mysqli_fetch_assoc($resultado7);
					$especificacion = $linea7['especificacion'];
				}
			}
			$consulta11 = "SELECT * FROM insp_vi_acu_pev WHERE id_has_movimiento_cilindro_pev = '".$id_has_movimiento_cilindro_pev."'";
			$resultado11 = mysqli_query($con,$consulta11);
			if(mysqli_num_rows($resultado11) > 0)
			{
				$prueba_realizada = 1;
				while($linea11 = mysqli_fetch_assoc($resultado11)){
					$id_has_movimiento_cilindro_pev_i = $linea11['id_has_movimiento_cilindro_pev'];
					$estado_prueba_i = $linea11['estado_prueba'];
					$bajo_relieve_i = $linea11['bajo_relieve'];
					$marcas_sospechosas_i = $linea11['marcas'];
					$tapon_cuello_i = $linea11['tapon_cuello'];
					$estabilidad_vertical_i = $linea11['estabilidad_vertical'];
					$abultamiento_i = $linea11['abultamiento'];
					$grieta_i = $linea11['grieta'];
					$dano_fuego_i = $linea11['fuego'];
					$arco_antorcha_i = $linea11['arco_antorcha'];
					$retroceso_i = $linea11['retroceso'];
					$abolladuras_i = $linea11['abolladuras'];
					$corte_agujero_i = $linea11['corte_agujero'];
					$corrosion_general_i = $linea11['corrosion_general'];
					$corrosion_local_i = $linea11['corrosion_local'];
					$corrosion_cadena_i = $linea11['corrosion_cadena'];
					$corrosion_grieta_i = $linea11['corrosion_grieta'];
					$contaminacion_i = $linea11['contaminacion'];
					$masas_i = $linea11['masas'];
					$motivo_condena_i = $linea11['motivo_condena'];
					$observaciones = $linea11["observaciones"];
					$fusibles_i = $linea11["fusibles"];
					$estado_fusibles_i = $linea11["estado_fusibles"];
					$roscas_valv_i = $linea11["roscas_valv"];
					$roscas_internas_i = $linea11["roscas_internas"];
					$collarin_collar_i = $linea11["collarin_collar"];
					$defecto_masa_i = $linea11["defecto_masa"];
					$masas_no_i = $linea11["masas_no"];

					if(($estado_prueba_i == "INSERVIBLE")||($estado_prueba_i == "APROBADO")||($fusibles_i == 2)){

						$prueba_realizada = 1;
						break;
					}else if($estado_prueba_i == "RECHAZADO"){
						$prueba_realizada = 0;
					}
				}
				
			}
		}

	}
?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
	 <div id="content">
		  <section id="widget-grid" class="">
				<div class="row">
					 <article class="col-md-6 col-md-offset-3">
						  <div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
								<header>
									 <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
									 <h2>INSPECCIÓN VISUAL CILINDRO DE ACERO</h2>
								</header>
								<div>
									 <div class="jarviswidget-editbox">
									 </div>
									 <div class="widget-body no-padding">                                
										  <form name="mezcla" action="insp_vi_acu_pru.php" method="POST" id="mezcla">
												<input type="hidden" name="id_has_movimiento_cilindro_pev" value="<?php echo $id_has_movimiento_cilindro_pev; ?>">
												<input type="hidden" name="motivo_condena" id="motivo_condena" value="<?php echo $motivo_condena; ?>">
												<div class="well well-sm well-primary">
													 <fieldset>
													 <legend><center>IDENTIFICACIÓN DEL CILINDRO</center></legend>
														  <div class="row">
																<div class="col-md-3">
																	 <div class="form-group">
																		  <label for="category">N° Cilindro<br>Ingreso:</label>
																		  <input type="text" class="form-control" placeholder="Numero Cilindro" name="num_cili_al" readonly required value="<?php echo $num_cili; ?>" />
																	 </div>
																</div>
																<div class="col-md-3">
																	 <div class="form-group">
																		  <label for="category">N° Cilindro<br>Verificado:</label>
																		  <input type="text" class="form-control" placeholder="Numero Cilindro" name="num_cili_al_2" readonly required value="<?php echo isset($num_cili_pev_2) ? $num_cili_pev_2 : NULL; ?>"/>
																	 </div>
																</div>
																<div class="col-md-3">
																	 <div class="form-group">
																		  <label for="category">Fecha de<br> Ingreso:</label>
																		  <input type="text" class="form-control" placeholder="Fecha Ingreso" name="fecha_ingreso_al" readonly value="<?php echo isset($fecha) ? $fecha : NULL; ?>"/>                                                    </div>
																</div>
																<div class="col-md-3">
																	 <div class="form-group">
																		  <label for="category">Fecha de<br> Inspección:</label>
																		  <input type="text" class="form-control" placeholder="Fecha Inspección" name="fecha_inspeccion_al" readonly required value="<?php echo $Fecha ?>"/>
																	 </div>
																</div>
														  </div>
														  <div class="row">
																<div class="col-md-6">
																	 <div class="form-group">
																		  <label for="category">GAS:</label>
																		  <input type="text" class="form-control" placeholder="Tipo de GAS" name="gas_al" readonly required value="<?php echo isset($nombre) ? $nombre : NULL; ?>"/>
																	 </div>
																</div>
																<div class="col-md-2">
																	 <div class="form-group">
																		  <label for="category">Especificación:</label>
																		  <input type="text" class="form-control" placeholder="Especificación" name="especificacion_al" readonly value="<?php echo isset($especificacion) ? $especificacion : NULL; ?>"/>
																	 </div>
																</div>
																<div class="col-md-4">
																	 <div class="form-group">
																		<label for="category">Descripción:</label>
																		  <input type="text" class="form-control" placeholder="Especificación" name="especi_pev2" readonly value="<?php echo isset($especi_pev) ? $especi_pev : NULL; ?>"/>
																	 </div>
																</div>
														  </div>
														  <div class="row">
																<div class="col-md-6">
																	 <div class="form-group">
																		<label for="category">Aprobación / Rechazo:</label>
																		  <input type="text" class="form-control" placeholder="ESTADO" name="estado_prueba" id="estado_prueba" readonly value="<?php echo isset($estado_prueba_i) ? $estado_prueba_i : NULL; ?>"  />
																	 </div>
																</div>
																<?php
																	if($prueba_realizada == 1){

																		if($estado_prueba_i != "APROBADO"){
																?>
																<div class="col-md-6">
																	 <div class="form-group">
																		  <label for="category">Causa de Condena:</label>
																		  <input type="text" class="form-control" placeholder="Cliente" name="nombre_cliente" readonly value="<?php echo isset($motivo_condena_i) ? $motivo_condena_i : NULL; ?>"/>
																	 </div>
																</div>
																<?php
																	}
																}
																?>
														  </div>
														  <div class="row">
																<div class="col-md-6">
																	 <div class="form-group">
																		<label for="category">Responsable:</label>
																		  <input type="text" class="form-control" placeholder="Responsable" name="responsable" readonly value="<?php echo isset($responsable) ? $responsable : NULL; ?>"/>
																	 </div>
																</div>
																<div class="col-md-6">
																	 <div class="form-group">
																		  <label for="category">Cliente:</label>
																		  <input type="text" class="form-control" placeholder="Cliente" name="nombre_cliente" readonly value="<?php echo isset($nombre_cliente) ? $nombre_cliente : NULL; ?>"/>
																	 </div>
																</div>
														  </div>
														  <?php
														  	if($prueba_realizada == 1){
														  ?>
														  <div class="row">
																<div class="col-md-6">
																	 <div class="form-group">
																		<label for="category">Certificado:</label>
																		<?php
																		$link = "<a href='pdf_certificado_pev_insp_visual_acumulador.php?id_has_movimiento_cilindro_pev=".$id_has_movimiento_cilindro_pev."'>Certificado</a>";

																		echo $link;
																		?>
																	 </div>
																</div>
														  </div>
														  <?php
															}
														  ?>
													 </fieldset>
												</div>  <!-- -----------------------DATOS BASICOS------------------------------- -->
												<div class="well well-sm well-primary">
													<legend><center>INSPECCIÓN VISUAL </center></legend>
													<?php
													if($costura==1){
														?>
														<div class="row">
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="category">Bajo Relieve: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('bajo_relieve')"></label>
																</div>  
															</div>   
															<div class="col-sm-6">
																<div class="form-group">
																	<div class="inline-group">
																		<label class="radio-inline">
																			<input type="radio" name="bajo_relieve" id="bajo_relieve1" onclick="checkBajoRelieve('1');" value="1" <?php if($bajo_relieve_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																			<i></i>Si
																		</label>
																		<label class="radio-inline">
																			<input type="radio" name="bajo_relieve" id="bajo_relieve2" onclick="checkBajoRelieve('0');" value="2" <?php if($bajo_relieve_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																			<i></i>No
																			<input type="hidden" name="bajo_relieve_h" id="bajo_relieve_h">
																		</label>
																	</div>
																</div>  
															</div>															
														</div> <!-- -----------------------BAJO RELIEVE------------------------------- -->
														<div class="row">
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="category">Presencia de Fusibles: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('fusibles')"></label>
																</div>  
															</div>   
															<div class="col-sm-6">
																<div class="form-group">
																	<div class="inline-group">
																		<label class="radio-inline">
																			<input type="radio" name="fusibles" id="fusibles1" onclick="checkFusibles('1');" value="1" <?php if($fusibles_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																			<i></i>Si
																		</label>
																		<label class="radio-inline">
																			<input type="radio" name="fusibles" id="fusibles2" onclick="checkFusibles('0');" value="2" <?php if($fusibles_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																			<i></i>No
																			<input type="hidden" name="fusibles_h" id="fusibles_h">
																		</label>
																	</div>
																</div>  
															</div>
														</div> <!-- -----------------------PRESENCIA DE FUSIBLES------------------------------- -->
														<div class="row">
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="category">Buen estado de Fusibles: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('estado_fusibles')"></label>
																</div>  
															</div>   
															<div class="col-sm-6">
																<div class="form-group">
																	<div class="inline-group">
																		<label class="radio-inline">
																			<input type="radio" name="estado_fusibles" id="estado_fusibles1" onclick="checkEstadoFusibles('1');" value="1" <?php if($estado_fusibles_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																			<i></i>Si
																		</label>
																		<label class="radio-inline">
																			<input type="radio" name="estado_fusibles" id="estado_fusibles2" onclick="checkEstadoFusibles('0');" value="2" <?php if($estado_fusibles_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																			<i></i>No
																			<input type="hidden" name="estado_fusibles_h" id="estado_fusibles_h">
																		</label>
																	</div>
																</div>  
															</div>
														</div> <!-- -----------------------BUEN ESTADO DE FUSIBLES------------------------------- -->
													<?php
													}
													?>
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Buen estado de las roscas de la válvula del cilindro: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('roscas_valv')"></label>
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="roscas_valv" id="roscas_valv1" onclick="checkRoscasValv('1');" value="1" <?php if($roscas_valv_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="roscas_valv" id="roscas_valv2" onclick="checkRoscasValv('0');" value="2" <?php if($roscas_valv_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="roscas_valv_h" id="roscas_valv_h">
																	</label>
																</div>
															</div>  
														</div>
													</div> <!-- -----------------------ROSCAS VALVULA------------------------------- -->
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Buen estado de las roscas internas: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('roscas_internas')"></label>
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="roscas_internas" id="roscas_internas1" onclick="checkRoscasIntern('1');" value="1" <?php if($roscas_internas_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="roscas_internas" id="roscas_internas2" onclick="checkRoscasIntern('0');" value="2" <?php if($roscas_internas_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="roscas_internas_h" id="roscas_internas_h">
																	</label>
																</div>
															</div>  
														</div>
													</div> <!-- -----------------------ROSCAS INTERNAS------------------------------- -->
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Buen estado de Collarin y Collar: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('collarin_collar')"></label>
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="collarin_collar" id="collarin_collar1" onclick="checkCollarin('1');" value="1" <?php if($collarin_collar_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="collarin_collar" id="collarin_collar2" onclick="checkCollarin('0');" value="2" <?php if($collarin_collar_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="collarin_collar_h" id="collarin_collar_h">
																	</label>
																</div>
															</div>  
														</div>
													</div> <!-- -----------------------ROSCAS INTERNAS------------------------------- -->
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Marcas Sospechosas: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('marcas')"></label>
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="marcas_sospechosas" id="marcas1" onclick="checkMarcas('1');" value="1" <?php if($marcas_sospechosas_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="marcas_sospechosas" id="marcas2" onclick="checkMarcas('0');" value="2" <?php if($marcas_sospechosas_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="marcas_sospechosas_h" id="marcas_sospechosas_h">
																	</label>
																</div>
															</div>  
														</div>
													</div> <!-- -----------------------MARCAS SOSPECHOSAS------------------------------- -->

													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Insertos en Tapon o Cuello: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('tapon_cuello')">
																</label>                            
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="tapon_cuello" id="tapon_cuello1" onclick="checkTaponCuello('1');" value="1" <?php if($tapon_cuello_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?> >
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="tapon_cuello" id="tapon_cuello2" onclick="checkTaponCuello('0');" value="2" <?php if($tapon_cuello_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?> >
																		<i></i>No
																		<input type="hidden" name="tapon_cuello_h" id="tapon_cuello_h">
																	</label>
																</div>
															</div>  
														</div>
													</div> <!-- -----------------------INSERTOS EN TAPON O CUELLO------------------------------- -->

													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Estabilidad Vertical: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('estabilidad')"></label>
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="estabilidad_vertical" id="estabilidad1" onclick="checkEstabilidadVertical('1');" value="1" <?php if($estabilidad_vertical_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="estabilidad_vertical" id="estabilidad2" onclick="checkEstabilidadVertical('0');" value="2" <?php if($estabilidad_vertical_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="estabilidad_h" id="estabilidad_h">
																	</label>
																</div>
															</div>  
														</div>
													</div> <!-- -----------------------ESTABILIDAD VERTICAL------------------------------- -->

													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Abultamiento: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('abultamiento')"></label>
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="abultamiento" id="abultamiento1" onclick="checkAbultamiento('1')" value="1" <?php if($abultamiento_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="abultamiento" id="abultamiento2" onclick="checkAbultamiento('0')" value="2" <?php if($abultamiento_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="abultamiento_h" id="abultamiento_h">
																	</label>
																</div>
															</div>  
														</div>
													</div> <!-- -----------------------ABULTAMIENTO------------------------------- -->

													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Grieta: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('grieta')"></label>
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="grieta" id="grieta1" onclick="checkGrieta('1')" value="1" <?php if($grieta_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="grieta" id="grieta2" onclick="checkGrieta('0')" value="2" <?php if($grieta_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="grieta_h" id="grieta_h">
																	</label>
																</div>
															</div>  
														</div>
													</div> <!-- -----------------------GRIETA------------------------------- -->
													
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Daño por Fuego: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('dano_fuego')">
																</label>                            
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="dano_fuego" id="dano_fuego1" onclick="checkDanoFuego('1');" value="1" <?php if($dano_fuego_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="dano_fuego" id="dano_fuego2" onclick="checkDanoFuego('0');" value="2" <?php if($dano_fuego_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="dano_fuego_h" id="dano_fuego_h">
																	</label>
																</div>
															</div>  
														</div>
													</div> <!-- -----------------------DAÑO POR FUEGO------------------------------- -->

													<div class="row">
														<div class="col-sm-6">
														  	<div class="form-group">
																<label for="category">Quemaduras por Arco y Antorcha: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('arco_antorcha')"></label>                            
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="arco_antorcha" id="arco_antorcha1" onclick="checkArcoAntorcha('1');" value="1" <?php if($arco_antorcha_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si</label>
																	<label class="radio-inline">
																		<input type="radio" name="arco_antorcha" id="arco_antorcha2" onclick="checkArcoAntorcha('0');" value="2" <?php if($arco_antorcha_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No</label>
																		<input type="hidden" name="arco_antorcha_h" id="arco_antorcha_h">
																</div>
															</div>  
														</div>
													</div> <!-- -----------------------QUEMADURAS POR ARCO Y ANTORCHA------------------------------- --> 

													<div class="row">
														<div class="col-sm-6">
														  	<div class="form-group">
																<label for="category">Retroceso de Llama: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('retroceso_llama')"></label>                            
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="retroceso" id="retroceso1" onclick="checkRetroceso('1');" value="1" <?php if($retroceso_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si</label>
																	<label class="radio-inline">
																		<input type="radio" name="retroceso" id="retroceso2" onclick="checkRetroceso('0');" value="2" <?php if($retroceso_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No</label>
																		<input type="hidden" name="retroceso_h" id="retroceso_h">
																</div>
															</div>  
														</div>
													</div> <!-- -----------------------RETROCESO DE LLAMA------------------------------- --> 
													
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Abolladura: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('abolladura')"></label>
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="abolladuras" id="abolladuras1" onclick="checkAbolladura('1');" value="1" <?php if($abolladuras_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="abolladuras" id="abolladuras2" onclick="checkAbolladura('0');" value="2" <?php if($abolladuras_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="abolladuras_h" id="abolladuras_h">
																	</label>
																</div>
															</div> 
															<div class="row" id="info_abolladura" style="display: none">
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="category"><strong>Información de Abolladura</strong></label>
																	<label class="input">Diametro Externo del Cilindro:
																		<input type="text" name="diam_ext_cilindro" id="diam_ext_cilindro" size="9">
																	</label>
																	<label class="input">Profundidad del Defecto:
																		<input type="text" name="prof_abolladura" id="prof_abolladura" onchange="validarProfAbolladura(this.value)" size="10">
																	</label>
																</div>
															</div>
														</div> 
														</div>
													</div> <!-- -----------------------ABOLLADURAS------------------------------- -->

													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Corte o Agujero: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('corte_agujero')"></label>
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="corte_agujero" id="corte_agujero1" onclick="checkCorteAgujero('1');" value="1" <?php if($corte_agujero_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="corte_agujero" id="corte_agujero2" onclick="checkCorteAgujero('0');" value="2" <?php if($corte_agujero_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="corte_agujero_h" id="corte_agujero_h">
																	</label>
																</div>
															</div> 
															<div class="row" id="info_corte_agujero" style="display: none">
																<div class="col-sm-6">
																	<div class="form-group">
																		<label for="category"><strong>Información del Corte o Agujero</strong></label>
																		<label class="input">Tipo de Cilindro:
																			<select name="tipo_cilindro" id="tipo_cilindro">
																				<option value="0">Seleccione...</option>
																				<option value="costura">Con Costura</option>
																				<option value="no_costura">Sin Costura</option>
																			</select>
																		</label>
																		<label class="input">Profundidad del Defecto:
																			<input type="text" name="prof_corte_agujero" id="prof_corte_agujero" size="10">
																		</label>
																		<label class="input">Espesor de Pared:
																			<input type="text" name="espesor_pared_cort_agujero" id="espesor_pared_cort_agujero" size="10">
																		</label>
																		<label class="input">Longitud de Defecto:
																			<input type="text" name="long_cort_agujero" id="long_cort_agujero" size="10">
																		</label>
																		<label class="input">Diametro Externo del Cilindro:
																			<input type="text" name="diam_ext_cilindro_cort" id="diam_ext_cilindro_cort" onchange="validar_corte_agujero(this.value)" size="10">
																		</label>
																	</div>
																</div>
															</div> 
														</div>
													</div> <!-- -----------------------CORTE O AGUJERO------------------------------- -->

													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Corrosion General: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('corrosion_general')">
																</label>                            
															
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="corrosion_general" id="corrosion_general1" onclick="checkCorrosionGeneral('1');" value="1" <?php if($corrosion_general_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="corrosion_general" id="corrosion_general2" onclick="checkCorrosionGeneral('0');" value="2" <?php if($corrosion_general_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="corrosion_general_h" id="corrosion_general_h">
																	</label>
																</div>
															</div>  
														</div>
														<div class="row" id="info_corrosion_general" style="display: none">
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="category"><strong>Información de la Corrosion General</strong></label>
																	<label class="input">Estado de la superficie:</br>
																		<input type="checkbox" name="si_superficie" id="si_superficie" size="9" >Reconocible</br>
																		<input type="checkbox" name="no_superficie" id="no_superficie" size="9" onclick="condenaCorrGene()">No Reconocible
																	</label>
																	<label class="input">Profundidad del Defecto:
																		<input type="text" name="prof_corr_gene" id="prof_corr_gene" size="9">
																	</label>
																	
																	<label class="input">Espesor de Pared Original:
																		<input type="text" name="espesor_orig_corr_gene" id="espesor_orig_corr_gene" size="9">
																	</label>
																	<label class="input">Espesor de Minimo Garantizado:
																		<input type="text" name="espesor_min_corr_gene" id="espesor_min_corr_gene" size="8" onchange="validarCorrosionGeneral(this.value)">
																	</label>
																</div>
															</div>
														</div>
													</div> <!-- -----------------------CORROSION GENERAL------------------------------- -->

													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Corrosion Local: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('corrosion_local')">
																</label>                            
															
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="corrosion_local" id="corrosion_local1" onclick="checkCorrosionLocal('1');" value="1" <?php if($corrosion_general_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="corrosion_local" id="corrosion_local2" onclick="checkCorrosionLocal('0');" value="2" <?php if($corrosion_general_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="corrosion_local_h" id="corrosion_local_h">
																	</label>
																</div>
															</div>  
														</div>
														<div class="row" id="info_corrosion_local" style="display: none">
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="category"><strong>Información de la Corrosion Local</strong></label>
																	<label class="input">Profundidad del Defecto:
																		<input type="text" name="prof_corr_lin" id="prof_corr_lin" size="9">
																	</label>
																	
																	<label class="input">Espesor de Pared Original:
																		<input type="text" name="espesor_orig_corr_lin" id="espesor_orig_corr_lin" size="9">
																	</label>
																	<label class="input">Espesor de Minimo Garantizado:
																		<input type="text" name="espesor_min_corr_lin" id="espesor_min_corr_lin" size="8" onchange="validarCorrosionLineal(this.value)">
																	</label>
																</div>
															</div>
														</div>
													</div> <!-- -----------------------CORROSION LOCAL------------------------------- -->

													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Corrosion en Cadena: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('corrosion_cadena')">
																</label>                            
															
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="corrosion_cadena" id="corrosion_cadena1" onclick="checkCorrosionCadena('1');" value="1" <?php if($corrosion_cadena_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="corrosion_cadena" id="corrosion_cadena2" onclick="checkCorrosionCadena('0');" value="2" <?php if($corrosion_cadena_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="corrosion_cadena_h" id="corrosion_cadena_h">
																	</label>
																</div>
															</div>  
														</div>
														<div class="row" id="info_corrosion_cadena" style="display: none">
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="category"><strong>Información de la Corrosion en Cadena</strong></label>
																	<label class="input">Longitud del Defecto:
																		<input type="text" name="long_corr_cadena" id="long_corr_cadena" size="9">
																	</label>
																	<label class="input">Diametro del Cilindro:
																		<input type="text" name="diam_corr_cadena" id="diam_corr_cadena" size="9">
																	</label>
																	<label class="input">Profundidad del Defecto:
																		<input type="text" name="prof_corr_cadena" id="prof_corr_cadena" size="9">
																	</label>
																	<label class="input">Espesor de Pared Original:
																		<input type="text" name="espesor_orig_corr_cadena" id="espesor_orig_corr_cadena" size="9" onchange="validarCorrosionCadena(this.value)">
																	</label>
																</div>
															</div>
														</div>
													</div> <!-- -----------------------CORROSION EN CADENA------------------------------- -->

													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Corrosion en Grietas: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('corrosion_grieta')">
																</label>                            
															
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="corrosion_grieta" id="corrosion_grieta1" onclick="checkCorrosionGrieta('1');" value="1" <?php if($corrosion_general_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="corrosion_grieta" id="corrosion_grieta2" onclick="checkCorrosionGrieta('0');" value="2" <?php if($corrosion_general_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="corrosion_grieta_h" id="corrosion_grieta_h">
																	</label>
																</div>
															</div>  
														</div>
														<div class="row" id="info_corrosion_grieta" style="display: none">
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="category"><strong>Información de la Corrosion en Grieta</strong></label>
																	<label class="input">Profundidad del Defecto:</br>
																		<input type="text" name="prof_corr_grieta" id="prof_corr_grieta" size="9">
																	</label>
																	<label class="input">Espesor de Pared Minimo Garantizado:
																		<input type="text" name="espesor_min_corr_grieta" id="espesor_min_corr_grieta" size="9" onchange="validarCorrosionGrieta(this.value)">
																	</label>
																</div>
															</div>
														</div>
													</div> <!-- -----------------------CORROSION EN GRIETA------------------------------- -->

													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Contaminación: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('contaminacion')">
																</label>                            
															
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="contaminacion" id="contaminacion1" onclick="checkContaminacion('1');" value="1" <?php if($contaminacion_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="contaminacion" id="contaminacion2" onclick="checkContaminacion('0');" value="2" <?php if($contaminacion_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="contaminacion_h" id="contaminacion_h">
																	</label>
																</div>
															</div>  
														</div>
														<div class="row" id="info_contaminacion" style="display: none">
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="category"><strong>Información de la Contaminacion</strong></label>
																	<label class="input">Defecto:</br>
																		<select id="defecto_contaminacion">
																			<option id="0">Hollin</option>
																			<option id="1">Envejecimiento / Masa Porosa</option>
																		</select>
																	</label>
																</div>
															</div>
														</div>
													</div> <!-- -----------------------CONTAMINACION------------------------------- -->
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Masas Pororosas Monolitcas: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('masas_porosas')">
																</label>                            
															
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="masas" id="masas1" onclick="checkMasas('1');" value="1" <?php if($masas_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="masas" id="masas2" onclick="checkMasas('0');" value="2" <?php if($masas_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="masas_h" id="masas_h">
																	</label>
																</div>
															</div>  
														</div>
														<div class="row" id="info_masas" <?php if($masas_i != 1) echo "style='display: none'"?>>
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="category"><strong>Información de la Contaminacion</strong></label>
																	<label class="input">Numeral del Defecto:
																		<input type="text" name="defecto_masa" id="defecto_masa" size="9" <?php if($defecto_masa_i>0) echo "value='$defecto_masa_i'"; ?>>
																	</label>
																</div>
															</div>
														</div>
													</div> <!-- -----------------------MASAS POROSAS------------------------------- --> 
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label for="category">Masas Pororosas No Monolitcas: <img src="img/informacion.png" style="cursor: pointer;" width="15" height="15" onclick="info('masas_porosas')">
																</label>                            
															
															</div>  
														</div>   
														<div class="col-sm-6">
															<div class="form-group">
																<div class="inline-group">
																	<label class="radio-inline">
																		<input type="radio" name="masas_no" id="masas_no1" onclick="checkMasasNo('1');" value="1" <?php if($masas_no_i == 1) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>Si
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="masas_no" id="masas_no2" onclick="checkMasasNo('0');" value="2" <?php if($masas_no_i == 2) print "checked = true"?> <?php if ($prueba_realizada == 1) print "disabled" ?>>
																		<i></i>No
																		<input type="hidden" name="masas_no_h" id="masas_no_h">
																	</label>
																</div>
															</div>  
														</div>
													</div> <!-- -----------------------MASAS POROSAS NO MONO------------------------------- --> 
												</div>
												
												<div class="well well-sm well-primary">     
													<div class="row">
														<div class="form-group">
															<label for="category">Observaciones Generales:</label>
															<textarea class="form-control" name="observaciones"><?php echo $observaciones; ?></textarea>
														</div>
													</div>
												</div> 
												<div class="modal-footer">
													<?php
														if (in_array(22, $acc))
														{
															if($prueba_realizada != 1){
														?>
															<input type="submit" value="Guardar" name="g_insp_visual_acero" id="g_insp_visual_acero" class="btn btn-primary" />
														<?php
															}
														}                   
														?>
												</div>
											</form>
									 </div>
								</div>
						  </div>
					 </article>
				</div>
		  </section>
	 </div>
</div>
<!-- END MAIN PANEL -->
<div class="modal fade" id="modal_info_bajo_relieve" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">BAJO RELIEVE</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
					 			<p>
									*	Todo cilindro con cualquier marcado ilegible o incorrecto.
								</p>
								<p>
									*	Marcación en bajo relieve en el hombro de cilindros soldados. 
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_marcas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">MARCAS SOSPECHOSAS</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
					 			<p>
									*	Cualquier marca introducida diferente a las del proceso de fabricación del cilindro o por reparación aprobada.
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_tapon_cuello" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">INSERTOS EN TAPON O CUELLO</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
					 			<p>
									*	Cualquier inserto adicional fijado en el cuello o en la base del cilindro que no forme parte del diseño aprobado.
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_estabilidad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">ESTABILIDAD VERTICAL</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
					 			<p>
									*	Para cilindros de acero: cualquier desviación en la verticalidad que pueda representar un riesgo durante el servicio (sobre todo si dispone de un anillo inferior).
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_abultamiento" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">ABULTAMIENTO</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
					 			<p>
									*	Cualquier abultamiento visible del cilindro.
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_grieta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">GRIETA</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
					 			<p>
									*	Cualquier fractura o fisura en el metal.
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_dano_fuego" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">DAÑO FUEGO</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
					 			<p>
									*	Cualquier fusión parcial del cilindro o cualquier deformación del cilindro.
								</p>
								<p>
									*	Cualquier carbonización o quemadura de la pintura.
								</p>
								<p>
									*	Cualquier daño por fuego a la válvula, la fusión de la protección de plástico o del anillo plástico o del tapón fusible, si están instalados.
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_arco_antorcha" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">QUEMADURAS POR ARCO Y ANTORCHA</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
					 			<p>
									*	Cualquier fusión parcial del cilindro, la adición de metal de soldadura o la eliminación de metal mediante escarfado o craterización.
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_retroceso_llama" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">RETROCESO DE LLAMA</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
					 			<p>
									*	Cualquier ignición del acetileno dentro  del cilindro.
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_abolladura" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">ABOLLADURA</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
					 			<p>
									*	Cualquier abolladura con una profundidad mayor al 3% del diámetro externo del cilindro.
								</p>
								<p>
									*	Cualquier abolladura con un diámetro menor a 15 veces su profundidad.
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_corte_agujero" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">CORTE O AGUJERO</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
					 			<p>
									*	Para cuerpos de cilindros de acero sin costura y aleación de aluminio sin costura: </br>

										- Cualquier corte o agujero con una profundidad mayor al 15% del espesor de pared. </br>
										- Cualquier corte o agujero con una longitud mayor al 25% del diámetro externo del cilindro
								</p>
								<p>
									*	Para cuerpos de cilindros de acero soldados:</br>

										- Cualquier corte o agujero con una profundidad mayor al 10% del espesor de pared.</br>
										- Cualquier corte o agujero con una longitud mayor al 25% del diámetro externo del cilindro
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_corrosion_general" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">CORROSION GENERAL</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
					 			<p>
									*	La superficie original del metal ya no es reconocible.
								</p>
								<p>
									*	La profundidad de la penetración es superior al 10% del espesor de pared original.
								</p>
								<p>
									*	El espesor de pared es menor que el espesor mínimo garantizado.
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_corrosion_local" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">CORROSION LOCAL</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
					 			<p>
									*	La profundidad de la penetración es superior al 20% del espesor de pared original.
								</p>
								<p>
									*	El espesor de pared es menor que el espesor mínimo garantizado.
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_corrosion_cadena" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">CORROSION CADENA</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
					 			<p>
									*	La longitud total de la corrosión en cualquier dirección excede el diámetro del cilindro y la posible profundidad es superior al 10% del espesor de pared original.
								</p>
								
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_corrosion_grieta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">CORROSION GRIETA</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
					 			<p>
									*	La profundidad de la penetración es superior al 20% del espesor de pared mínimo garantizado (después de la limpieza).
								</p>
								
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_contaminacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">CONTAMINACION</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
					 			<p>
									*	Hollín visible.
								</p>
								<p>
									*	Cuerpos de agua o aceite visibles. (como envejecimiento de la masa porosa).
								</p>
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>

<div class="modal fade" id="modal_info_masas_porosas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">  
  	<div class="modal-dialog">
	 	<div class="modal-content">
		  	<div class="modal-header">
			 	<h4 class="modal-title" id="myModalLabel">MASAS POROSAS MONOLITICAS</h4>
		  	</div>
		  	<div class="modal-body">
			 	<div class="well well-sm well-primary">
					<div class="row">
				  		<div class="col-md-10">
					 		<div class="form-group">
					 			<p>
									1	Espacio libre superior por encima del espacio libre máximo permitido (cuando es conocido).
								</p>
								<p>
									2	Espacio libre superior a 2 mm para materiales porosos libres de asbesto (cuando no hay especificación del espacio libre máximo permitido).
								</p>
								<p>
									3	Espacio libre superior a 5 mm para todos los otros materiales porosos monolíticos (cuando no hay especificación del espacio libre máximo permitido).
								</p>
								<p>
									4	Las masas porosas con grietas que tienen paredes laterales visibles.
								</p>
								<p>
									5	Los cilindros con masas porosas que presentan un desmoronamiento que excede el espacio libre superior máximo admisible.
								</p>
								<p>
									6	Cualquier cilindro que muestre un movimiento lateral de la masas porosa.
								</p>
								<p>
									7	Cilindros con masa porosa no monolítica que muestran compactación.
								</p>
								
					 		</div>
				  		</div>
					</div>
			 	</div>
		  	</div><!-- /.modal-content -->
		</div>
  	</div><!-- /.modal -->
  	<!-- Fin Modal -->
</div>
<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->
<?php
	 include("inc/footer.php");
?>
<!-- END PAGE FOOTER -->

<?php 
	 //include required scripts
	 include("inc/scripts.php"); 
?>


<!-- PAGE RELATED PLUGIN(S) 
<script src="..."></script>-->
<!-- Flot Chart Plugin: Flot Engine, Flot Resizer, Flot Tooltip -->
<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>

<!-- Vector Maps Plugin: Vectormap engine, Vectormap language -->
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>

<!-- Full Calendar -->
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/jquery-form/jquery-form.min.js"></script>

<script type="text/javascript">
function info(ventana){
	 $('#modal_info_'+ventana).modal();
}

function inhabilitarFormulario(check, campo){

	habilitar = check;
	llenar = campo;

	document.getElementById("bajo_relieve1").disabled = true;
	document.getElementById("bajo_relieve2").disabled = true;
	document.getElementById("marcas1").disabled = true;
	document.getElementById("marcas2").disabled = true;
	document.getElementById("tapon_cuello1").disabled = true;
	document.getElementById("tapon_cuello2").disabled = true;
	document.getElementById("estabilidad1").disabled = true;
	document.getElementById("estabilidad2").disabled = true;
	document.getElementById("abultamiento1").disabled = true;
	document.getElementById("abultamiento2").disabled = true;
	document.getElementById("grieta1").disabled = true;
	document.getElementById("grieta2").disabled = true;
	document.getElementById("dano_fuego1").disabled = true;
	document.getElementById("dano_fuego2").disabled = true;
	document.getElementById("arco_antorcha1").disabled = true;
	document.getElementById("arco_antorcha2").disabled = true;
	document.getElementById("retroceso1").disabled = true;
	document.getElementById("retroceso2").disabled = true;
	document.getElementById("abolladuras1").disabled = true;
	document.getElementById("abolladuras2").disabled = true;
	document.getElementById("corte_agujero1").disabled = true;
	document.getElementById("corte_agujero2").disabled = true;
	document.getElementById("corrosion_general1").disabled = true;
	document.getElementById("corrosion_general2").disabled = true;
	document.getElementById("corrosion_local1").disabled = true;
	document.getElementById("corrosion_local2").disabled = true;
	document.getElementById("corrosion_cadena1").disabled = true;
	document.getElementById("corrosion_cadena2").disabled = true;
	document.getElementById('corrosion_grieta1').disabled = true;
	document.getElementById('corrosion_grieta2').disabled = true;
	document.getElementById('contaminacion1').disabled = true;
	document.getElementById('contaminacion2').disabled = true;
	document.getElementById('masas1').disabled = true;
	document.getElementById('masas2').disabled = true;
	document.getElementById('fusibles1').disabled = true;
	document.getElementById('fusibles2').disabled = true;
	document.getElementById('estado_fusibles1').disabled = true;
	document.getElementById('estado_fusibles2').disabled = true;
	document.getElementById('roscas_valv1').disabled = true;
	document.getElementById('roscas_valv2').disabled = true;
	document.getElementById('roscas_internas1').disabled = true;
	document.getElementById('roscas_internas2').disabled = true;
	document.getElementById('collarin_collar1').disabled = true;
	document.getElementById('collarin_collar2').disabled = true;

	document.getElementById("bajo_relieve_h").value = 2;
	document.getElementById("marcas_sospechosas_h").value = 2;
	document.getElementById("tapon_cuello_h").value = 2;
	document.getElementById("estabilidad_h").value = 2;
	document.getElementById("abultamiento_h").value = 2;
	document.getElementById("grieta_h").value = 2;
	document.getElementById("dano_fuego_h").value = 2;
	document.getElementById("arco_antorcha_h").value = 2;
	document.getElementById("retroceso_h").value = 2;
	document.getElementById("abolladuras_h").value = 2;
	document.getElementById("corte_agujero_h").value = 2
	document.getElementById("corrosion_general_h").value = 2;
	document.getElementById("corrosion_local_h").value = 2;
	document.getElementById("corrosion_cadena_h").value = 2;
	document.getElementById("corrosion_grieta_h").value = 2;
	document.getElementById("contaminacion_h").value = 2;
	document.getElementById("masas_h").value = 2;
	document.getElementById("fusibles_h").value = 2;
	document.getElementById("estado_fusibles_h").value=2;
	document.getElementById("roscas_valv_h").value=2;
	document.getElementById("roscas_internas_h").value=2;
	document.getElementById("collarin_collar_h").value=2;

	document.getElementById("bajo_relieve2").checked = true;
	document.getElementById("marcas2").checked = true;
	document.getElementById("tapon_cuello2").checked = true;
	document.getElementById("estabilidad2").checked = true;	
	document.getElementById("abultamiento2").checked = true;
	document.getElementById("grieta2").checked = true;
	document.getElementById("dano_fuego2").checked = true;
	document.getElementById("arco_antorcha2").checked = true;
	document.getElementById("retroceso2").checked = true;
	document.getElementById("abolladuras2").checked = true;
	document.getElementById("corte_agujero2").checked = true;
	document.getElementById("corrosion_general2").checked = true;
	document.getElementById("corrosion_local2").checked = true;
	document.getElementById("corrosion_cadena2").checked = true;
	document.getElementById("corrosion_grieta2").checked = true;
	document.getElementById("contaminacion2").checked = true;
	document.getElementById("masas2").checked = true;
	document.getElementById("fusibles2").checked = true;
	document.getElementById("estado_fusibles2").checked = true;
	document.getElementById("roscas_valv2").checked = true;
	document.getElementById("roscas_internas2").checked = true;
	document.getElementById("collarin_collar2").checked = true;


	document.getElementById(habilitar).checked = true;

	if((llenar=="fusibles_h") || (llenar = "roscas_valv_h") || (llenar = "roscas_internas_h") || (llenar = "collarin_collar_h")){
		document.getElementById(llenar).value = 2;
	}else{
		document.getElementById(llenar).value = 1;
	}


	
	
}

function checkBajoRelieve(valor){
  	valorCheck = valor;

  	if(valorCheck == 1){
  		
  		if(confirm("Al seleccionar esta opción el cilindro será cambiado a estado Inservible, ¿Desea continuar?")){
  			if(confirm("¿Desea dejar el cilindro en estado Rechazado para poder continuar despues?")){
  				alert('Cilindro Rechazado!');
  				document.getElementById('bajo_relieve1').disabled = true;
	  			document.getElementById('bajo_relieve2').disabled = true;
	  			document.getElementById('estado_prueba').value = 'RECHAZADO';
	  			document.getElementById('motivo_condena'). value = 'Bajo Relieve';
	  			document.getElementById('bajo_relieve_h').value = '1';
	  			inhabilitarFormulario('bajo_relieve1','bajo_relieve_h');
  			}else{
  				alert('Cilindro Inservible!');
  				document.getElementById('bajo_relieve1').disabled = true;
	  			document.getElementById('bajo_relieve2').disabled = true;
	  			document.getElementById('estado_prueba').value = 'INSERVIBLE';
	  			document.getElementById('motivo_condena'). value = 'Bajo Relieve';
	  			document.getElementById('bajo_relieve_h').value = '1';
	  			inhabilitarFormulario('bajo_relieve1','bajo_relieve_h');
  			}
  			
  		}else{
  			document.getElementById('bajo_relieve1').checked = false;
  		}
  		
  	}else if(valorCheck == 0){
  		document.getElementById('bajo_relieve_h').value = '2';
  	}
}

function checkFusibles(valor){
	valorCheck = valor;

	if(valorCheck == 0){
  		
  		if(confirm("Al seleccionar esta opción la revisión será finalizada, ¿Desea continuar?")){
  			
  			alert('revisión Finalizada!');
  			document.getElementById('fusibles1').disabled = true;
	  		document.getElementById('fusibles2').disabled = true;
	  		document.getElementById('estado_prueba').value = 'RECHAZADO';
	  		document.getElementById('motivo_condena'). value = 'NO INSERVIBLE';
	  		document.getElementById('fusibles_h').value = '2';
	  		inhabilitarFormulario('fusibles2','fusibles_h');  			
  			
  		}else{
  			document.getElementById('fusibles2').checked = false;
  		}
  		
  	}else if(valorCheck == 1){
  		document.getElementById('fusibles_h').value = '1';
  	}
}

function checkEstadoFusibles(valor){
	valorCheck = valor;

	if(valorCheck == 0){
  		
  		if(confirm("Al seleccionar esta opción la revisión será finalizada, ¿Desea continuar?")){
  			
  			alert('Revisión Finalizada!');
  			document.getElementById('estado_fusibles1').disabled = true;
	  		document.getElementById('estado_fusibles2').disabled = true;
	  		document.getElementById('estado_prueba').value = 'RECHAZADO';
	  		document.getElementById('motivo_condena'). value = 'NO INSERVIBLE';
	  		document.getElementById('estado_fusibles_h').value = '2';
	  		inhabilitarFormulario('estado_fusibles2','fusibles_h');  			
  			
  		}else{
  			document.getElementById('estado_fusibles2').checked = false;
  		}
  		
  	}else if(valorCheck == 1){
  		document.getElementById('estado_fusibles_h').value = '1';
  	}
}

function checkRoscasValv(valor){
	valorCheck = valor;

	if(valorCheck == 0){
		if(confirm("Al seleccionar esta opción el cilindro será cambiado a estado Inservible, ¿Desea continuar?")){
			if(confirm("¿Desea dejar el cilindro en estado Rechazado para poder continuar despues?")){
  				alert('Cilindro Rechazado!');
  				document.getElementById('roscas_valv1').disabled = true;
				document.getElementById('roscas_valv2').disabled = true;
				document.getElementById('estado_prueba').value = 'RECHAZADO';
				document.getElementById('motivo_condena'). value = 'Mal estado en Roscas de la Válvula';
				document.getElementById('roscas_valv_h').value = '2';
				inhabilitarFormulario('roscas_valv2','roscas_valv_h');
  			}else{
  				alert('Cilindro Inservible!');
  				document.getElementById('roscas_valv1').disabled = true;
				document.getElementById('roscas_valv2').disabled = true;
				document.getElementById('estado_prueba').value = 'INSERVIBLE';
				document.getElementById('motivo_condena'). value = 'Mal estado en Roscas de la Válvula';
				document.getElementById('roscas_valv_h').value = '2';
				inhabilitarFormulario('roscas_valv2','roscas_valv_h');

  			}
			
		}else{
			document.getElementById('roscas_valv2').checked  = false;
		}
	}else if(valorCheck == 1){
		
		document.getElementById('roscas_valv_h').value = '1';
	}
}

function checkRoscasIntern(valor){
	valorCheck = valor;

	if(valorCheck == 0){
		if(confirm("Al seleccionar esta opción el cilindro será cambiado a estado Inservible, ¿Desea continuar?")){
			if(confirm("¿Desea dejar el cilindro en estado Rechazado para poder continuar despues?")){
  				alert('Cilindro Rechazado!');
  				document.getElementById('roscas_internas1').disabled = true;
				document.getElementById('roscas_internas2').disabled = true;
				document.getElementById('estado_prueba').value = 'RECHAZADO';
				document.getElementById('motivo_condena'). value = 'Mal estado en Roscas internas';
				document.getElementById('roscas_internas_h').value = '2';
				inhabilitarFormulario('roscas_internas2','roscas_internas_h');
  			}else{
  				alert('Cilindro Inservible!');
  				document.getElementById('roscas_internas1').disabled = true;
				document.getElementById('roscas_internas1').disabled = true;
				document.getElementById('estado_prueba').value = 'INSERVIBLE';
				document.getElementById('motivo_condena'). value = 'Mal estado en Roscas internas';
				document.getElementById('roscas_internas_h').value = '2';
				inhabilitarFormulario('roscas_internas2','roscas_internas_h');

  			}
			
		}else{
			document.getElementById('roscas_internas2').checked  = false;
		}
	}else if(valorCheck == 1){
		
		document.getElementById('roscas_internas_h').value = '1';
	}
}

function checkCollarin(valor){
	valorCheck = valor;

	if(valorCheck == 0){
		if(confirm("Al seleccionar esta opción el cilindro será cambiado a estado Inservible, ¿Desea continuar?")){
			if(confirm("¿Desea dejar el cilindro en estado Rechazado para poder continuar despues?")){
  				alert('Cilindro Rechazado!');
  				document.getElementById('collarin_collar1').disabled = true;
				document.getElementById('collarin_collar2').disabled = true;
				document.getElementById('estado_prueba').value = 'RECHAZADO';
				document.getElementById('motivo_condena'). value = 'Mal estado en Roscas internas';
				document.getElementById('collarin_collar_h').value = '2';
				inhabilitarFormulario('collarin_collar2','collarin_collar_h');
  			}else{
  				alert('Cilindro Inservible!');
  				document.getElementById('collarin_collar1').disabled = true;
				document.getElementById('collarin_collar2').disabled = true;
				document.getElementById('estado_prueba').value = 'INSERVIBLE';
				document.getElementById('motivo_condena'). value = 'Mal estado en Collarion y Collar';
				document.getElementById('collarin_collar_h').value = '2';
				inhabilitarFormulario('collarin_collar2','collarin_collar_h');

  			}
			
		}else{
			document.getElementById('collarin_collar2').checked  = false;
		}
	}else if(valorCheck == 1){
		
		document.getElementById('collarin_collar_h').value = '1';
	}
}

function checkMarcas(valor){

	var valorCheck = valor;

	if(valorCheck == 1){
		if(confirm("Al seleccionar esta opción el cilindro será cambiado a estado Inservible, ¿Desea continuar?")){
			if(confirm("¿Desea dejar el cilindro en estado Rechazado para poder continuar despues?")){
  				alert('Cilindro Rechazado!');
  				document.getElementById('marcas1').disabled = true;
				document.getElementById('marcas2').disabled = true;
				document.getElementById('estado_prueba').value = 'RECHAZADO';
				document.getElementById('motivo_condena'). value = 'Marcas Sospechosas';
				document.getElementById('marcas_sospechosas_h').value = '1';
				inhabilitarFormulario('marcas1','marcas_sospechosas_h');
  			}else{
  				alert('Cilindro Inservible!');
  				document.getElementById('marcas1').disabled = true;
				document.getElementById('marcas2').disabled = true;
				document.getElementById('estado_prueba').value = 'INSERVIBLE';
				document.getElementById('motivo_condena'). value = 'Marcas Sospechosas';
				document.getElementById('marcas_sospechosas_h').value = '1';
				inhabilitarFormulario('marcas1','marcas_sospechosas_h');

  			}
			
		}else{
			document.getElementById('marcas1').checked  = false;
		}
	}else if(valorCheck == 0){
		
		document.getElementById('marcas_sospechosas_h').value = '2';
	}
}

function checkTaponCuello(valor){
  	valorCheck = valor;

  	if(valorCheck == 1){
  		
  		if(confirm("Al seleccionar esta opción el cilindro será cambiado a estado Inservible, ¿Desea continuar?")){
  			if(confirm("¿Desea dejar el cilindro en estado Rechazado para poder continuar despues?")){
  				alert('Cilindro Rechazado!');
  				document.getElementById('tapon_cuello1').disabled = true;
	  			document.getElementById('tapon_cuello2').disabled = true;
	  			document.getElementById('estado_prueba').value = 'RECHAZADO';
	  			document.getElementById('motivo_condena'). value = 'Insertos en Tapon o Cuello';
	  			document.getElementById('tapon_cuello_h').value = '1';
	  			inhabilitarFormulario('tapon_cuello1','tapon_cuello_h');
  			}else{
  				alert('Cilindro Inservible!');
  				document.getElementById('tapon_cuello1').disabled = true;
	  			document.getElementById('tapon_cuello2').disabled = true;
	  			document.getElementById('estado_prueba').value = 'INSERVIBLE';
	  			document.getElementById('motivo_condena'). value = 'Insertos en Tapon o Cuello';
	  			document.getElementById('tapon_cuello_h').value = '1';
	  			inhabilitarFormulario('tapon_cuello1','tapon_cuello_h');
	  		}
  			
  		}else{
  			document.getElementById('tapon_cuello1').checked = false;
  		}
  		
  	}else if(valorCheck == 0){
  		document.getElementById('tapon_cuello_h').value = '2';
  	}
}

function checkEstabilidadVertical(valor){

	var valorCheck = valor;

	if(valorCheck == 1){
		if(confirm("Al seleccionar esta opción el cilindro será cambiado a estado Inservible, ¿Desea continuar?")){
			if(confirm("¿Desea dejar el cilindro en estado Rechazado para poder continuar despues?")){
  				alert('Cilindro Rechazado!');
  				document.getElementById('estabilidad1').disabled = true;
				document.getElementById('estabilidad2').disabled = true;
				document.getElementById('estado_prueba').value = 'RECHAZADO';
				document.getElementById('motivo_condena'). value = 'Estabilidad Vertical';
				document.getElementById('estabilidad_h').value = '1';
				inhabilitarFormulario('estabilidad1','estabilidad_h');
  			}else{
  				alert('Cilindro Inservible!');
  				document.getElementById('estabilidad1').disabled = true;
				document.getElementById('estabilidad2').disabled = true;
				document.getElementById('estado_prueba').value = 'INSERVIBLE';
				document.getElementById('motivo_condena'). value = 'Estabilidad Vertical';
				document.getElementById('estabilidad_h').value = '1';
				inhabilitarFormulario('estabilidad1','estabilidad_h');
  			}
			
		}else{
			document.getElementById('estabilidad1').checked  = false;
		}
	}else if(valorCheck == 0){
		
		document.getElementById('estabilidad_h').value = '2';
	}
}

function checkAbultamiento(valor){

	var valorCheck = valor;

	if(valorCheck == 1){
		if(confirm("Al seleccionar esta opción el cilindro será cambiado a estado Inservible, ¿Desea continuar?")){
			if(confirm("¿Desea dejar el cilindro en estado Rechazado para poder continuar despues?")){
  				alert('Cilindro Rechazado!');
  				document.getElementById('abultamiento1').disabled = true;
				document.getElementById('abultamiento2').disabled = true;
				document.getElementById('estado_prueba').value = 'RECHAZADO';
				document.getElementById('motivo_condena'). value = 'Abultamiento';
				document.getElementById('abultamiento_h').value = '1';
				inhabilitarFormulario('abultamiento1','abultamiento_h');
  			}else{
  				alert('Cilindro Inservible!');
  				document.getElementById('abultamiento1').disabled = true;
				document.getElementById('abultamiento2').disabled = true;
				document.getElementById('estado_prueba').value = 'INSERVIBLE';
				document.getElementById('motivo_condena'). value = 'Abultamiento';
				document.getElementById('abultamiento_h').value = '1';
				inhabilitarFormulario('abultamiento1','abultamiento_h');
  			}
			
		}else{
			document.getElementById('abultamiento1').checked  = false;
		}
	}else if(valorCheck == 0){
		
		document.getElementById('abultamiento_h').value = '2';
	}
}

function checkGrieta(valor){

	var valorCheck = valor;

	if(valorCheck == 1){
		if(confirm("Al seleccionar esta opción el cilindro será cambiado a estado Inservible, ¿Desea continuar?")){
			if(confirm("¿Desea dejar el cilindro en estado Rechazado para poder continuar despues?")){
  				alert('Cilindro Rechazado!');
  				document.getElementById('grieta1').disabled = true;
				document.getElementById('grieta2').disabled = true;
				document.getElementById('estado_prueba').value = 'RECHAZADO';
				document.getElementById('motivo_condena'). value = 'Grieta';
				document.getElementById('grieta_h').value = '1';
				inhabilitarFormulario('grieta1','grieta_h');
  			}else{
  				alert('Cilindro Inservible!');
  				document.getElementById('grieta1').disabled = true;
				document.getElementById('grieta2').disabled = true;
				document.getElementById('estado_prueba').value = 'INSERVIBLE';
				document.getElementById('motivo_condena'). value = 'Grieta';
				document.getElementById('grieta_h').value = '1';
				inhabilitarFormulario('grieta1','grieta_h');
  			}
			
		}else{
			document.getElementById('grieta1').checked  = false;
		}
	}else if(valorCheck == 0){
		
		document.getElementById('grieta_h').value = '2';
	}
}

function checkDanoFuego(valor){
  	valorCheck = valor;

  	if(valorCheck == 1){
  		
  		if(confirm("Al seleccionar esta opción el cilindro será cambiado a estado Inservible, ¿Desea continuar?")){
  			if(confirm("¿Desea dejar el cilindro en estado Rechazado para poder continuar despues?")){
  				alert('Cilindro Rechazado!');
  				document.getElementById('dano_fuego1').disabled = true;
	  			document.getElementById('dano_fuego2').disabled = true;
	  			document.getElementById('estado_prueba').value = 'RECHAZADO';
	  			document.getElementById('motivo_condena'). value = 'Daño por Fuego';
	  			document.getElementById('dano_fuego_h').value = '1';
	  			inhabilitarFormulario('dano_fuego1','dano_fuego_h');
  			}else{
  				alert('Cilindro Inservible!');
  				document.getElementById('dano_fuego1').disabled = true;
	  			document.getElementById('dano_fuego2').disabled = true;
	  			document.getElementById('estado_prueba').value = 'INSERVIBLE';
	  			document.getElementById('motivo_condena'). value = 'Daño por Fuego';
	  			document.getElementById('dano_fuego_h').value = '1';
	  			inhabilitarFormulario('dano_fuego1','dano_fuego_h');
  			}
  			
  		}else{
  			document.getElementById('dano_fuego1').checked = false;
  		}
  		
  	}else if(valorCheck == 0){
  		document.getElementById('dano_fuego_h').value = '2';
  	}
}

function checkArcoAntorcha(valor){
  	valorCheck = valor;

  	if(valorCheck == 1){
  		
  		if(confirm("Al seleccionar esta opción el cilindro será cambiado a estado Inservible, ¿Desea continuar?")){
  			if(confirm("¿Desea dejar el cilindro en estado Rechazado para poder continuar despues?")){
  				alert('Cilindro Rechazado!');
  				document.getElementById('arco_antorcha1').disabled = true;
	  			document.getElementById('arco_antorcha2').disabled = true;
	  			document.getElementById('estado_prueba').value = 'RECHAZADO';
	  			document.getElementById('motivo_condena'). value = 'Quemadura por Arco y Antorcha';
	  			document.getElementById('arco_antorcha_h').value = '1';
	  			inhabilitarFormulario('arco_antorcha1','arco_antorcha_h');
  			}else{
  				alert('Cilindro Inservible!');
  				document.getElementById('arco_antorcha1').disabled = true;
	  			document.getElementById('arco_antorcha2').disabled = true;
	  			document.getElementById('estado_prueba').value = 'INSERVIBLE';
	  			document.getElementById('motivo_condena'). value = 'Quemadura por Arco y Antorcha';
	  			document.getElementById('arco_antorcha_h').value = '1';
	  			inhabilitarFormulario('arco_antorcha1','arco_antorcha_h');

  			}
  			
  		}else{
  			document.getElementById('arco_antorcha1').checked = false;
  		}
  		
  	}else if(valorCheck == 0){
  		document.getElementById('arco_antorcha_h').value = '2';
  	}
}

function checkRetroceso(valor){
  	valorCheck = valor;

  	if(valorCheck == 1){
  		
  		if(confirm("Al seleccionar esta opción el cilindro será cambiado a estado Inservible, ¿Desea continuar?")){
  			if(confirm("¿Desea dejar el cilindro en estado Rechazado para poder continuar despues?")){
  				alert('Cilindro Rechazado!');
  				document.getElementById('retroceso1').disabled = true;
	  			document.getElementById('retroceso2').disabled = true;
	  			document.getElementById('estado_prueba').value = 'RECHAZADO';
	  			document.getElementById('motivo_condena'). value = 'Retroceso de Llama';
	  			document.getElementById('retroceso_h').value = '1';
	  			inhabilitarFormulario('retroceso1','retroceso_h');
  			}else{
  				alert('Cilindro Inservible!');
  				document.getElementById('retroceso1').disabled = true;
	  			document.getElementById('retroceso2').disabled = true;
	  			document.getElementById('estado_prueba').value = 'INSERVIBLE';
	  			document.getElementById('motivo_condena'). value = 'Retroceso de Llama';
	  			document.getElementById('retroceso_h').value = '1';
	  			inhabilitarFormulario('retroceso1','retroceso_h');
  			}
  			
  		}else{
  			document.getElementById('retroceso1').checked = false;
  		}
  		
  	}else if(valorCheck == 0){
  		document.getElementById('retroceso_h').value = '2';
  	}
}

function checkAbolladura(valor){

	var valorCheck = valor;

	if(valorCheck == 1){
		document.getElementById('info_abolladura').style.display = 'block';
		document.getElementById('abolladuras_h').value = '1';
	}else if(valorCheck == 0){
		
		document.getElementById('info_abolladura').style.display = 'none';
		document.getElementById('abolladuras_h').value = '2';
	}
}

function validarProfAbolladura(valor){

	var profundidad_abolladura = valor;
	var diametro_cilindro = document.getElementById('diam_ext_cilindro').value;
	var porcentaje_condena = 0;

	porcentaje_condena = profundidad_abolladura*100/diametro_cilindro;

	if(porcentaje_condena > 3){
		if(confirm("Con estos datos el cilindro será cambiado a estado Inservible, ¿Desea continuar?")){
			document.getElementById('abolladuras1').disabled = true;
			document.getElementById('abolladuras2').disabled = true;
			document.getElementById('diam_ext_cilindro').readOnly = true;
			document.getElementById('prof_abolladura').readOnly = true;
			document.getElementById('estado_prueba').value = 'INSERVIBLE';
			document.getElementById('motivo_condena'). value = 'Abolladura';
			document.getElementById('abolladuras_h').value = '1';
			inhabilitarFormulario('abolladuras1','abolladuras_h');
		}else{
			document.getElementById('abolladuras1').checked  = false;
		}
	}
}

function checkCorteAgujero(valor){
	var valorCheck = valor;

	if(valorCheck == 1){
		document.getElementById('info_corte_agujero').style.display = 'block';
		document.getElementById('corte_agujero_h').value = '1';
	}else if(valorCheck == 0){
		
		document.getElementById('info_corte_agujero').style.display = 'none';
		document.getElementById('corte_agujero_h').value = '2';
	}
}

function validar_corte_agujero(valor){

	var diametro_externo_cilindro = valor;
	var tipo_cilindro = document.getElementById('tipo_cilindro').value;
	var espesor_pared = document.getElementById('espesor_pared_cort_agujero').value;
	var profundidad_defecto = document.getElementById('prof_corte_agujero').value;
	var longitud_defecto = document.getElementById('long_cort_agujero').value;
	var porcentaje_profundidad_espesor = 0;
	var porcentaje_longitud = 0;
	
	if(tipo_cilindro == 0){
		alert("Por favor seleccione el tipo de cilindro");
		document.getElementById("diam_ext_cilindro_cort").value = "";
		document.getElementById("espesor_pared").value = "";
		document.getElementById("profundidad_defecto").value = "";
	}else if(tipo_cilindro == "costura"){
		porcentaje_profundidad_espesor = profundidad_defecto * 100 / espesor_pared;
		porcentaje_longitud = longitud_defecto * 100 / diametro_externo_cilindro;
		if(porcentaje_profundidad_espesor > 10){
			if(confirm("Con estos datos el cilindro será cambiado a estado Inservible, ¿Desea continuar?")){
				document.getElementById('corte_agujero1').disabled = true;
				document.getElementById('corte_agujero2').disabled = true;
				document.getElementById('diam_ext_cilindro_cort').readOnly = true;
				document.getElementById('espesor_pared').readOnly = true;
				document.getElementById('profundidad_defecto').readOnly = true;
				document.getElementById('long_cort_agujero').readOnly = true;
				document.getElementById('tipo_cilindro').disabled = true;
				document.getElementById('estado_prueba').value = 'INSERVIBLE';
				document.getElementById('motivo_condena'). value = 'Corte o Agujero';
				inhabilitarFormulario('corte_agujero1','corte_agujero_h');
			}else{
				document.getElementById('corte_agujero1').checked  = false;
			}
		}else if(porcentaje_longitud>25){
			if(confirm("Con estos datos el cilindro será cambiado a estado Inservible, ¿Desea continuar?")){
				document.getElementById('corte_agujero1').disabled = true;
				document.getElementById('corte_agujero2').disabled = true;
				document.getElementById('diam_ext_cilindro_cort').readOnly = true;
				document.getElementById('espesor_pared').readOnly = true;
				document.getElementById('profundidad_defecto').readOnly = true;
				document.getElementById('long_cort_agujero').readOnly = true;
				document.getElementById('tipo_cilindro').disabled = true;
				document.getElementById('estado_prueba').value = 'INSERVIBLE';
				document.getElementById('motivo_condena'). value = 'Corte o Agujero';
				inhabilitarFormulario('corte_agujero1','corte_agujero_h');
			}else{
				document.getElementById('corte_agujero1').checked  = false;
			}
		}
	
	}else if(tipo_cilindro == "no_costura"){
		porcentaje_profundidad_espesor = profundidad_defecto * 100 / espesor_pared;
		porcentaje_longitud = longitud_defecto * 100 / diametro_externo_cilindro;
		if(porcentaje_profundidad_espesor > 15){
			if(confirm("Con estos datos el cilindro será cambiado a estado Inservible, ¿Desea continuar?")){
				document.getElementById('corte_agujero1').disabled = true;
				document.getElementById('corte_agujero2').disabled = true;
				document.getElementById('diam_ext_cilindro_cort').readOnly = true;
				document.getElementById('espesor_pared').readOnly = true;
				document.getElementById('profundidad_defecto').readOnly = true;
				document.getElementById('long_cort_agujero').readOnly = true;
				document.getElementById('tipo_cilindro').disabled = true;
				document.getElementById('estado_prueba').value = 'INSERVIBLE';
				document.getElementById('motivo_condena'). value = 'Corte o Agujero';
				inhabilitarFormulario('corte_agujero1','corte_agujero_h');
			}else{
				document.getElementById('corte_agujero1').checked  = false;
			}
		}else if(porcentaje_longitud>25){
			if(confirm("Con estos datos el cilindro será cambiado a estado Inservible, ¿Desea continuar?")){
				document.getElementById('corte_agujero1').disabled = true;
				document.getElementById('corte_agujero2').disabled = true;
				document.getElementById('diam_ext_cilindro_cort').readOnly = true;
				document.getElementById('espesor_pared').readOnly = true;
				document.getElementById('profundidad_defecto').readOnly = true;
				document.getElementById('long_cort_agujero').readOnly = true;
				document.getElementById('tipo_cilindro').disabled = true;
				document.getElementById('estado_prueba').value = 'INSERVIBLE';
				document.getElementById('motivo_condena'). value = 'Corte o Agujero';
				inhabilitarFormulario('corte_agujero1','corte_agujero_h');
			}else{
				document.getElementById('corte_agujero1').checked  = false;
			}
		}
	
	}
}

function checkCorrosionGeneral(valor){
	valorCheck = valor;

	if(valorCheck == 1){
		document.getElementById('info_corrosion_general').style.display = "block";
		document.getElementById('corrosion_general_h').value = 1;
	}else if (valorCheck == 0) {
		document.getElementById('info_corrosion_general').style.display = "none";
		document.getElementById('corrosion_general_h').value = 2;
	}
}

function condenaCorrGene(){
	if(confirm("Al seleccionar esta opción el cilindro será cambiado a estado Inservible, ¿Desea continuar?")){
  		document.getElementById('corrosion_general1').disabled = true;
  		document.getElementById('corrosion_general2').disabled = true;
  		document.getElementById('si_superficie').disabled = true;
  		document.getElementById('no_superficie').disabled = true;
  		document.getElementById('no_superficie').checked = false;
  		document.getElementById('prof_corr_gene').readOnly = true;
  		document.getElementById('espesor_orig_corr_gene').readOnly = true;
  		document.getElementById('espesor_min_corr_gene').readOnly = true;
  		document.getElementById('estado_prueba').value = 'INSERVIBLE';
  		document.getElementById('motivo_condena'). value = 'Corrosion General';
  		inhabilitarFormulario('corrosion_general1','corrosion_general_h');
  	}else{
  		document.getElementById('corrosion_general1').checked = false;
  	}
}

function validarCorrosionGeneral(valor){
	espesor_minimo_garantizado = valor;
	profundidad_defecto = document.getElementById('prof_corr_gene').value;
	espesor_original = document.getElementById('espesor_orig_corr_gene').value;
	porcentaje_condena = profundidad_defecto * 100 / espesor_original;

	if(porcentaje_condena>10){
		if(confirm("Al seleccionar esta opción el cilindro será cambiado a estado Inservible, ¿Desea continuar?")){
	  		document.getElementById('corrosion_general1').disabled = true;
	  		document.getElementById('corrosion_general2').disabled = true;
	  		document.getElementById('si_superficie').disabled = true;
	  		document.getElementById('no_superficie').disabled = true;
	  		document.getElementById('no_superficie').checked = false;
	  		document.getElementById('prof_corr_gene').readOnly = true;
	  		document.getElementById('espesor_orig_corr_gene').readOnly = true;
	  		document.getElementById('espesor_min_corr_gene').readOnly = true;
	  		document.getElementById('estado_prueba').value = 'INSERVIBLE';
	  		document.getElementById('motivo_condena'). value = 'Corrosion General';
	  		inhabilitarFormulario('corrosion_general1','corrosion_general_h');
	  	}else{
	  		document.getElementById('corrosion_general1').checked = false;
	  	}
	}else if(espesor_original < espesor_minimo_garantizado){
		if(confirm("Al seleccionar esta opción el cilindro será cambiado a estado Inservible, ¿Desea continuar?")){
	  		document.getElementById('corrosion_general1').disabled = true;
	  		document.getElementById('corrosion_general2').disabled = true;
	  		document.getElementById('si_superficie').disabled = true;
	  		document.getElementById('no_superficie').disabled = true;
	  		document.getElementById('no_superficie').checked = false;
	  		document.getElementById('prof_corr_gene').readOnly = true;
	  		document.getElementById('espesor_orig_corr_gene').readOnly = true;
	  		document.getElementById('espesor_min_corr_gene').readOnly = true;
	  		document.getElementById('estado_prueba').value = 'INSERVIBLE';
	  		document.getElementById('motivo_condena'). value = 'Corrosion General';
	  		inhabilitarFormulario('corrosion_general1','corrosion_general_h');
	  	}else{
	  		document.getElementById('corrosion_general1').checked = false;
	  	}
	}
}

function checkCorrosionLocal(valor){
	valorCheck = valor;

	if(valorCheck == 1){
		document.getElementById('info_corrosion_local').style.display = "block";
		document.getElementById('corrosion_local_h').value = "1";
	}else{
		document.getElementById('info_corrosion_local').style.display = "none";
		document.getElementById('corrosion_local_h').value = "2";
	}
}

function validarCorrosionLineal(valor){
	espesor_minimo_garantizado = valor;
	profundidad_defecto = document.getElementById('prof_corr_lin').value;
	espesor_original = document.getElementById('espesor_orig_corr_lin').value;
	porcentaje_condena = profundidad_defecto * 100 / espesor_original;

	if(porcentaje_condena>20){
		if(confirm("Al seleccionar esta opción el cilindro será cambiado a estado Inservible, ¿Desea continuar?")){
	  		document.getElementById('corrosion_local1').disabled = true;
	  		document.getElementById('corrosion_local2').disabled = true;
	  		document.getElementById('prof_corr_lin').readOnly = true;
	  		document.getElementById('espesor_orig_corr_lin').readOnly = true;
	  		document.getElementById('espesor_min_corr_lin').readOnly = true;
	  		document.getElementById('estado_prueba').value = 'INSERVIBLE';
	  		document.getElementById('motivo_condena'). value = 'Corrosion Local';
	  		inhabilitarFormulario('corrosion_local1','corrosion_local_h');
	  	}else{
	  		document.getElementById('corrosion_lineal1').checked = false;
	  	}
	}else if(espesor_original < espesor_minimo_garantizado){
		if(confirm("Al seleccionar esta opción el cilindro será cambiado a estado Inservible, ¿Desea continuar?")){
	  		document.getElementById('corrosion_local1').disabled = true;
	  		document.getElementById('corrosion_local2').disabled = true;
	  		document.getElementById('prof_corr_lin').readOnly = true;
	  		document.getElementById('espesor_orig_corr_lin').readOnly = true;
	  		document.getElementById('espesor_min_corr_lin').readOnly = true;
	  		document.getElementById('estado_prueba').value = 'INSERVIBLE';
	  		document.getElementById('motivo_condena'). value = 'Corrosion Local';
	  		inhabilitarFormulario('corrosion_local1','corrosion_local_h');
	  	}else{
	  		document.getElementById('corrosion_local1').checked = false;
	  	}
	}
}

function checkCorrosionCadena(valor){
	valorCheck = valor;

	if(valorCheck == 1){
		document.getElementById('info_corrosion_cadena').style.display = "block";
		document.getElementById('corrosion_cadena_h').value = "1";
	}else{
		document.getElementById('info_corrosion_local').style.display = "none";
		document.getElementById('corrosion_cadena_h').value = "2";
	}
}

function validarCorrosionCadena(valor){
	espesor_original = valor;
	longitud_corrosion = document.getElementById('long_corr_cadena').value;
	diametro_cilindro = document.getElementById('diam_corr_cadena').value;
	profundidad_corrosion = document.getElementById('prof_corr_cadena').value;
	porcentaje_condena = profundidad_corrosion * 100 / espesor_original;

	if(longitud_corrosion>diametro_cilindro){
		if(porcentaje_condena > 10){
			if(confirm("Con estos datos el cilindro será cambiado a estado Inservible, ¿Desea continuar?")){
		  		document.getElementById('corrosion_cadena1').disabled = true;
		  		document.getElementById('corrosion_cadena2').disabled = true;
		  		document.getElementById('long_corr_cadena').readOnly = true;
		  		document.getElementById('diam_corr_cadena').readOnly = true;
		  		document.getElementById('prof_corr_cadena').readOnly = true;
		  		document.getElementById('espesor_orig_corr_cadena').readOnly = true;
		  		document.getElementById('estado_prueba').value = 'INSERVIBLE';
		  		document.getElementById('motivo_condena'). value = 'Corrosion en Cadena';
		  		inhabilitarFormulario('corrosion_cadena1','corrosion_cadena_h');
		  	}else{
		  		document.getElementById('corrosion_cadena1').checked = false;
		  	}
		}
	}
}

function checkCorrosionGrieta(valor){
	valorCheck = valor;

	if(valorCheck == 1){
		document.getElementById('info_corrosion_grieta').style.display = "block";
		document.getElementById('corrosion_grieta_h').value = "1";
	}else{
		document.getElementById('info_corrosion_grieta').style.display = "none";
		document.getElementById('corrosion_grieta_h').value = "2";
	}
}

function validarCorrosionGrieta(valor){
	profundidad_corrosion = valor;
	espesor_minimo = document.getElementById('espesor_min_corr_grieta').value;
	porcentaje_condena = profundidad_corrosion * 100 / espesor_minimo;

	if(porcentaje_condena > 20){
		if(confirm("Con estos datos el cilindro será cambiado a estado Inservible, ¿Desea continuar?")){
		  	document.getElementById('corrosion_grieta1').disabled = true;
		  	document.getElementById('corrosion_grieta2').disabled = true;
		  	document.getElementById('prof_corr_grieta').readOnly = true;
		  	document.getElementById('espesor_min_corr_grieta').readOnly = true;
		  	document.getElementById('estado_prueba').value = 'INSERVIBLE';
		  	document.getElementById('motivo_condena'). value = 'Corrosion en Grieta';
		  	inhabilitarFormulario('corrosion_grieta1','corrosion_grieta_h');
		}else{
			document.getElementById('corrosion_grieta1').checked = false;
	  	}
	}
}

function checkContaminacion(valor){
	valorCheck = valor;

	if(valorCheck == 1){
		if(confirm("Al seleccionar esta opción el cilindro será cambiado a estado Inservible, ¿Desea continuar?")){
			if(confirm("¿Desea dejar el cilindro en estado Rechazado para poder continuar despues?")){
  				alert('Cilindro Rechazado!');
  				document.getElementById('contaminacion1').disabled = true;
			  	document.getElementById('contaminacion2').disabled = true;
			  	document.getElementById('estado_prueba').value = 'RECHAZADO';
			  	document.getElementById('motivo_condena'). value = 'Contaminacion';
			  	inhabilitarFormulario('contaminacion1','contaminacion_h');
			  	document.getElementById('info_contaminacion').style.display = "block";
				document.getElementById('contaminacion_h').value = "1";
  			}else{
  				alert('Cilindro Inservible!');
  				document.getElementById('contaminacion1').disabled = true;
			  	document.getElementById('contaminacion2').disabled = true;
			  	document.getElementById('estado_prueba').value = 'INSERVIBLE';
			  	document.getElementById('motivo_condena'). value = 'Contaminacion';
			  	inhabilitarFormulario('contaminacion1','contaminacion_h');
			  	document.getElementById('info_contaminacion').style.display = "block";
				document.getElementById('contaminacion_h').value = "1";
  			}
		  	
		}else{
			document.getElementById('contaminacion1').checked = false;
	  	}
		
	}else{
		document.getElementById('info_contaminacion').style.display = "none";
		document.getElementById('contaminacion_h').value = "2";
	}
}

function checkMasas(valor){
	valorCheck = valor;

	if(valorCheck == 1){
		if(confirm("Al seleccionar esta opción el cilindro será cambiado a estado Inservible, ¿Desea continuar?")){
			if(confirm("¿Desea dejar el cilindro en estado Rechazado para poder continuar despues?")){
  				alert('Cilindro Rechazado!');
  				document.getElementById('masas1').disabled = true;
			  	document.getElementById('masas2').disabled = true;
			  	document.getElementById('estado_prueba').value = 'RECHAZADO';
			  	document.getElementById('motivo_condena'). value = 'Masas Pororosas';
			  	inhabilitarFormulario('masas1','masas_h');
			  	document.getElementById('info_masas').style.display = "block";
				document.getElementById('masas_h').value = "1";
  			}else{
  				alert('Cilindro Inservible!');
  				document.getElementById('masas1').disabled = true;
			  	document.getElementById('masas2').disabled = true;
			  	document.getElementById('estado_prueba').value = 'INSERVIBLE';
			  	document.getElementById('motivo_condena'). value = 'Masas Pororosas';
			  	inhabilitarFormulario('masas1','masas_h');
			  	document.getElementById('info_masas').style.display = "block";
				document.getElementById('masas_h').value = "1";
  			}
		  	
		}else{
			document.getElementById('masas1').checked = false;
	  	}
		
	}else{
		document.getElementById('info_masas').style.display = "none";
		document.getElementById('masas_h').value = "2";
	}
}

function checkMasasNo(valor){
	valorCheck = valor;

	if(valorCheck == 1){
		if(confirm("Al seleccionar esta opción el cilindro será cambiado a estado Inservible, ¿Desea continuar?")){
			if(confirm("¿Desea dejar el cilindro en estado Rechazado para poder continuar despues?")){
  				alert('Cilindro Rechazado!');
  				document.getElementById('masas_no1').disabled = true;
			  	document.getElementById('masas_no2').disabled = true;
			  	document.getElementById('estado_prueba').value = 'RECHAZADO';
			  	document.getElementById('motivo_condena'). value = 'Masas Pororosas No Monoliticas';
			  	inhabilitarFormulario('masas_no1','masas_no_h');
				document.getElementById('masas_no_h').value = "1";
  			}else{
  				alert('Cilindro Inservible!');
  				document.getElementById('masas_no1').disabled = true;
			  	document.getElementById('masas_no2').disabled = true;
			  	document.getElementById('estado_prueba').value = 'INSERVIBLE';
			  	document.getElementById('motivo_condena'). value = 'Masas Pororosas No Monoliticas';
			  	inhabilitarFormulario('masas_no1','masas_no_h');
				document.getElementById('masas_no_h').value = "1";
  			}
		  	
		}else{
			document.getElementById('masas_no_h').checked = false;
	  	}
		
	}else{
		document.getElementById('masas_no_h').value = "2";
	}
}


</script>
<?php 
	 //include footer
	 include("inc/google-analytics.php"); 
}
else
{
	 header("Location:index.php");
}
?>