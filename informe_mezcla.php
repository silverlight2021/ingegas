<?php
session_start();
date_default_timezone_set("America/Bogota");
require ("libraries/conexion.php");

if(@$_SESSION['logged']== 'yes')
{ 
	$id_user=$_SESSION['su'];
    $acc = $_SESSION['acc'];
    $letra = isset($_REQUEST['letra']) ? $_REQUEST['letra'] : NULL;
    $id_orden = isset($_REQUEST['id_orden']) ? $_REQUEST['id_orden'] : NULL;
    
	require_once("inc/init.php");
	require_once("inc/config.ui.php");
	$page_title = "Informe Producción de Mezclas";
	$page_css[] = "your_style.css";
	include("inc/header.php");
	$page_nav[""][""][""][""] = true;
	include("inc/nav.php");
	if(strlen($id_orden) > 0)
	{ 
	  	$consulta  = "SELECT * FROM has_orden_cilindro WHERE id_orden= $id_orden";
	  	$resultado = mysqli_query($con,$consulta) ;
	  	$linea = mysqli_fetch_array($resultado);

		$fecha = isset($linea["fecha"]) ? $linea["fecha"] : NULL;
		$id_orden = isset($linea["id_orden"]) ? $linea["id_orden"] : NULL;
		$id_tipo_cilindro1 = isset($linea["id_tipo_cilindro"]) ? $linea["id_tipo_cilindro"] : NULL;	
		$id_tipo_envace1 = isset($linea["id_tipo_envace"]) ? $linea["id_tipo_envace"] : NULL;
		$id_finalizar = isset($linea["id_finalizar"]) ? $linea["id_finalizar"] : NULL;
		$total = isset($linea["cantidad"]) ? $linea["cantidad"] : NULL;	
		$cantidad = isset($linea["cantidad"]) ? $linea["cantidad"] : NULL;
		mysqli_free_result($resultado);

		

		$consulta1  = "SELECT * FROM ordenes WHERE id_orden= $id_orden";
	  	$resultado1 = mysqli_query($con,$consulta1) ;
	  	$linea1 = mysqli_fetch_array($resultado1);
	  	$num_ord = isset($linea1["num_ord"]) ? $linea1["num_ord"] : NULL;
	  	mysqli_free_result($resultado1);

	  	$consulta2  = "SELECT * FROM tipo_cilindro WHERE id_tipo_cilindro= $id_tipo_cilindro1";
	  	$resultado2 = mysqli_query($con,$consulta2) ;
	  	$linea2 = mysqli_fetch_array($resultado2);
	  	$id_tipo_cilindro = isset($linea2["tipo_cili"]) ? $linea2["tipo_cili"] : NULL;
	  	mysqli_free_result($resultado2);

	  	$consulta3  = "SELECT * FROM tipo_envace WHERE id_tipo_envace= $id_tipo_envace1";
	  	$resultado3 = mysqli_query($con,$consulta3) ;
	  	$linea3 = mysqli_fetch_array($resultado3);
	  	$id_tipo_envace = isset($linea3["tipo"]) ? $linea3["tipo"] : NULL;		
	    mysqli_free_result($resultado3);	  	

	    $consulta6 = "SELECT COUNT(*) AS count FROM produccion_mezclas WHERE id_orden = $id_orden ";
  		
	    $result = mysqli_query($con,$consulta6); 
		$row = mysqli_fetch_array($result); 
		$count = $row['count'];
		if ($count == $total) 
		{
			$oculto=1;
			$consulta  = "SELECT real_eto, SUM(real_eto) FROM produccion_mezclas WHERE id_orden = $id_orden";
			$resultado = mysqli_query($con,$consulta) ;
			$linea = mysqli_fetch_array($resultado);
			$suma_eto = isset($linea["SUM(real_eto)"]) ? $linea["SUM(real_eto)"] : NULL;

			$consulta1  = "SELECT real_co, SUM(real_co) FROM produccion_mezclas WHERE id_orden = $id_orden";
			$resultado1 = mysqli_query($con,$consulta1) ;
			$linea1 = mysqli_fetch_array($resultado1);
			$suma_co2 = isset($linea1["SUM(real_co)"]) ? $linea1["SUM(real_co)"] : NULL;

			$consulta2  = "SELECT peso_final_1, SUM(peso_final_1) FROM produccion_mezclas WHERE id_orden = $id_orden";
			$resultado2 = mysqli_query($con,$consulta2) ;
			$linea2 = mysqli_fetch_array($resultado2);
			$suma_final = isset($linea2["SUM(peso_final_1)"]) ? $linea2["SUM(peso_final_1)"] : NULL;	

			$consulta4  = "SELECT * FROM produccion_mezclas WHERE id_orden = $id_orden ";
		  	$resultado4 = mysqli_query($con,$consulta4) ;
		  	$linea4 = mysqli_fetch_array($resultado4);

			$lote_pro = isset($linea4["lote_pro"]) ? $linea4["lote_pro"] : NULL;
			mysqli_free_result($resultado);
		}
	}
?>
<!-- MAIN PANEL -->
<div id="main" role="main">
<?php
if (in_array(39, $acc))
{
?>
	<div id="content">
		<section id="widget-grid" class="">
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-6">
					<div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h6 id="eg1">Informe Producción Mezclas</h6>				
						</header>
						<div>				
						<div class="jarviswidget-editbox"></div>
							<div class="widget-body no-padding">																	
								<div class="row">					
									<div class="col-md-3">
										<div class="form-group">
											<label for="category">Num Orden :</label>											 
											<input type="text" readonly class="form-control"  placeholder="Num Orden" value="<?php echo isset($num_ord) ? $num_ord : NULL; ?>" />											
										</div>
									</div>	
									<div class="col-md-3">	
										<div class="form-group">
											<label for="category">Tipo Gas :</label>											 
											<input type="text" readonly class="form-control" placeholder="Tipo Gas" value="<?php echo isset($id_tipo_cilindro) ? $id_tipo_cilindro : NULL; ?>" />											
										</div>
									</div>	
									<div class="col-md-3">	
										<div class="form-group">
											<label for="category">Capacidad :</label>											 
											<input type="text" readonly class="form-control" placeholder="Capacidad" value="<?php echo isset($id_tipo_envace) ? $id_tipo_envace : NULL; ?>" />											
										</div>
									</div>
									<div class="col-md-3">	
										<div class="form-group">
											<label for="category">Cantidad :</label>											 
											<input type="text" readonly class="form-control" placeholder="Cantidad" value="<?php echo isset($cantidad) ? $cantidad : NULL; ?>" />											
										</div>
									</div>							
								</div>	
								<div class="row">
									<div class="col-md-3">
										<div class="form-group">
											<label for="category">Lote :</label>											 
											<input type="text" class="form-control" readonly  placeholder="Consumo ETO" value="<?php echo isset($lote_pro) ? $lote_pro : NULL; ?>" />											
										</div>
									</div>	
									<div class="col-md-3">
										<div class="form-group">
											<label for="category">Cons ETO (Kg) :</label>											 
											<input type="text" class="form-control" readonly name="num_cili"  placeholder="Consumo ETO" value="<?php echo isset($suma_eto) ? $suma_eto : NULL; ?>" />											
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label for="category">Cons CO2 (Kg) :</label>											 
											<input type="text" class="form-control" readonly name="num_cili"  placeholder="Consumo CO2" value="<?php echo isset($suma_co2) ? $suma_co2 : NULL; ?>" />											
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label for="category">Total (Kg) :</label>											 
											<input type="text" class="form-control" readonly name="num_cili"  placeholder="Total Kg" value="<?php echo isset($suma_final) ? $suma_final : NULL; ?>" />											
										</div>
									</div>															
								</div>
								<div class="row">
									<section class="col-md-3">
										<a href="javascript:imprSelec('muestra')"><img src="img/iconos/printer_blue.png"></a>
									</section>
									<section class="col-md-6">
										<label><h3>Fecha actual: <?php $time = time(); echo date("d-m-Y", $time) ?></h3></label>
									</section>
								</div>
							</div>
						</div>					
					</div>				
         		</article>
     		</div>
   		</section> 
		<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">			
			<header>
				<span class="widget-icon"> <i class="fa fa-table"></i> </span>
				<h2>Informe Producción de Mezclas</h2>				
			</header>
			<div>
				<div class="jarviswidget-editbox"></div>							
				<div class="widget-body no-padding">								
					<table class="table table-bordered">							
						<thead>
							<tr>                                              
	                            <th>Cilindro</th>
	                            <th>Fecha P H</th>
	                            <th>Peso Incial</th>
	                            <th>Peso Final</th>
	                            <th>Pre Vacio</th>
	                            <th>Lote Pro</th>
	                            <th>Tara Vacio</th>
	                            <th>Peso Esperado</th>
	                            <th>Pre Inial</th>
	                            <th>Esperado ETO</th>
	                            <th>Real ETO</th>
	                            <th>Esperado CO2</th>
	                            <th>Real CO2</th>
	                            <th>Peso Final</th>
	                            <th>Desviacion</th>
	                            <th>Pre Final</th>
	                            <th>Lote C02</th>
	                            <th>Cilindro ETO</th>
	                            <th>Fecha Vencimiento</th>                
							</tr>
						</thead>
						<tbody>													
						  <?php
	                          $contador = "0";	                
	                          $consulta = "SELECT * FROM produccion_mezclas WHERE id_orden=".$id_orden;
	                          $resultado = mysqli_query($con,$consulta) ;
	                          while ($linea = mysqli_fetch_array($resultado))
	                          {
	                            $contador = $contador + 1;
	                            $id_orden = $linea["id_orden"];
	                            $id_cilindro = $linea["id_cilindro"];
	                            $id_produccion_mezclas = $linea["id_produccion_mezclas"];
								$tara_vacio = $linea["tara_vacio"];
								$fecha_ven_mezcla = $linea["fecha_ven_mezcla"];
								$prue_hidro = $linea["prue_hidro"];
								$pre_inicial = $linea["pre_inicial"];
								$peso_inicial = $linea["peso_inicial"];				                   		                            
								$peso_final = $linea["peso_final"];
								$pre_vacio = $linea["pre_vacio"];
								$lote_pro = $linea["lote_pro"];
								$tara_vacio = $linea["tara_vacio"];
								$peso_esperado = $linea["peso_esperado"];
								$pre_inicial_1 = $linea["pre_inicial_1"];
								$esperado_eto = $linea["esperado_eto"];
								$real_eto = $linea["real_eto"];
								$esperado_co = $linea["esperado_co"];
								$real_co = $linea["real_co"];
								$peso_final_1 = $linea["peso_final_1"];
								$desviacion = $linea["desviacion"];
								$pre_final = $linea["pre_final"];
								$lote_co = $linea["lote_co"];
								$id_lote_eto = $linea["id_lote_eto"];
								$fecha_ven_mezcla = $linea["fecha_ven_mezcla"];
								$time = time();

	                            $consulta1 = "SELECT num_cili_eto FROM cilindro_eto WHERE id_cilindro_eto = $id_cilindro";
	                            $resultado1 = mysqli_query($con,$consulta1) ;
	                            while ($linea1 = mysqli_fetch_array($resultado1))
	                            {
	                            	$num_cili_eto = $linea1["num_cili_eto"];
	                            }
	                            $consulta2 = "SELECT fecha_ven FROM ordenes WHERE id_orden = $id_orden";
	                            $resultado2 = mysqli_query($con,$consulta2) ;
	                            while ($linea2 = mysqli_fetch_array($resultado2))
	                            {
	                            	$fecha_ven = $linea2["fecha_ven"];
	                            }
	                            $consulta3 = "SELECT num_cilindro_eto FROM lote_eto WHERE id_lote_eto = $id_lote_eto";
	                            $resultado3 = mysqli_query($con,$consulta3) ;
	                            while ($linea3 = mysqli_fetch_array($resultado3))
	                            {
	                            	$num_cilindro_eto = $linea3["num_cilindro_eto"];
	                            }
	                            $consulta4 = "SELECT num_cilindro_co2 FROM lote_co2 WHERE id_num_cilindro = $lote_co";
	                            $resultado4 = mysqli_query($con,$consulta4) ;
	                            while ($linea4 = mysqli_fetch_array($resultado4))
	                            {
	                            	$num_cilindro_co2 = $linea4["num_cilindro_co2"];
	                            }
	                            $consulta4 = "SELECT  tipo FROM  tipo_envace WHERE id_tipo_envace = $peso_esperado";
	                            $resultado4 = mysqli_query($con,$consulta4) ;
	                            while ($linea4 = mysqli_fetch_array($resultado4))
	                            {
	                            	$tipo = $linea4["tipo"];
	                            }			                            		                            
	                            ?>
	                            <tr class="odd gradeX">                                
	                              	<td><?php echo $num_cili_eto; ?></td>
	                              	<td><?php echo $prue_hidro; ?></td>
	                              	<td><?php echo $peso_inicial; ?></td>  
	                              	<td><?php echo $peso_final; ?></td>  
	                              	<td><?php echo $pre_vacio; ?></td>  
	                              	<td><?php echo $lote_pro; ?></td>     
	                              	<td><?php echo $tara_vacio; ?></td>  
	                              	<td><?php echo $tipo; ?></td>  
	                              	<td><?php echo $pre_inicial_1; ?></td>  
	                              	<td><?php echo $esperado_eto; ?></td>  
	                              	<td><?php echo $real_eto; ?></td>
	                              	<td><?php echo $esperado_co; ?></td>
	                              	<td><?php echo $real_co; ?></td>
	                              	<td><?php echo $peso_final_1; ?></td>
	                              	<td><?php echo $desviacion; ?></td>
	                              	<td><?php echo $pre_final; ?></td>
	                              	<td><?php echo $num_cilindro_co2; ?></td>
	                              	<td><?php echo $num_cilindro_eto; ?></td>
	                              	<td><?php echo $fecha_ven; ?></td>
	                            </tr>                           
	                            <?php
	                            }mysqli_free_result($resultado);	                                            
                  		    ?>
						</tbody>							
					</table>
				</div>
			</div>		
		</div>
	</div>

	<div id="muestra" style="display:none">
		<p>INGEGAS-INGENIERIA Y GASES LTDA</p>
		<h2 align="center">INFORME PRODUCCIÓN DE MEZCLAS</h2>
		<table class="table table-bordered" style="border: 1px solid black;border-collapse: collapse;" align="center">
			<thead>
				<tr>                                               
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Num Orden </th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Tipo Gas</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Capacidad</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Cantidad</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Lote</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Consumo ETO (kG)</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Consumo CO2 (kG)</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Total (kG)</th>             
				</tr>
			</thead>
			<tbody>
                <tr class="odd gradeX">                                
                  	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $num_ord; ?></td>
                  	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $id_tipo_cilindro; ?></td>
                  	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $id_tipo_envace; ?></td>  
                  	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $cantidad; ?></td>  
                  	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $lote_pro; ?></td>  
                  	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $suma_eto; ?></td>     
                  	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $suma_co2; ?></td>  
                  	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $suma_final; ?></td>  
                </tr>
			</tbody>							
		</table>
		<br>
		<br>
		<table class="table table-bordered" style="border: 1px solid black;border-collapse: collapse;">							
			<thead>
				<tr>
					<th style="border: 1px solid black;border-collapse: collapse;" align="center">#</th>                                                
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Cilindro</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Fecha P H</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Peso Incial</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Peso Final</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Pre Vacio</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Lote Pro</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Tara Vacio</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Peso Esperado</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Pre Inial</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Esperado ETO</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Real ETO</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Esperado CO2</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Real CO2</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Peso Final</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Desv</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Pre Final</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Lote C02</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Cilindro ETO</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Fecha Vencimiento</th>                
				</tr>
			</thead>
			<tbody>													
			  	<?php
                  $contador = "0";	                
                  $consulta = "SELECT * FROM produccion_mezclas WHERE id_orden=".$id_orden;
                  $resultado = mysqli_query($con,$consulta) ;
                  while ($linea = mysqli_fetch_array($resultado))
                  {
                    $contador = $contador + 1;
                    $id_orden = $linea["id_orden"];
                    $id_cilindro = $linea["id_cilindro"];
                    $id_produccion_mezclas = $linea["id_produccion_mezclas"];
					$tara_vacio = $linea["tara_vacio"];
					$fecha_ven_mezcla = $linea["fecha_ven_mezcla"];
					$prue_hidro = $linea["prue_hidro"];
					$pre_inicial = $linea["pre_inicial"];
					$peso_inicial = $linea["peso_inicial"];				                   		                            
					$peso_final = $linea["peso_final"];
					$pre_vacio = $linea["pre_vacio"];
					$lote_pro = $linea["lote_pro"];
					$tara_vacio = $linea["tara_vacio"];
					$peso_esperado = $linea["peso_esperado"];
					$pre_inicial_1 = $linea["pre_inicial_1"];
					$esperado_eto = $linea["esperado_eto"];
					$real_eto = $linea["real_eto"];
					$esperado_co = $linea["esperado_co"];
					$real_co = $linea["real_co"];
					$peso_final_1 = $linea["peso_final_1"];
					$desviacion = $linea["desviacion"];
					$pre_final = $linea["pre_final"];
					$lote_co = $linea["lote_co"];
					$id_lote_eto = $linea["id_lote_eto"];
					$fecha_ven_mezcla = $linea["fecha_ven_mezcla"];


                    $consulta1 = "SELECT num_cili_eto FROM cilindro_eto WHERE id_cilindro_eto = $id_cilindro";
                    $resultado1 = mysqli_query($con,$consulta1) ;
                    while ($linea1 = mysqli_fetch_array($resultado1))
                    {
                    	$num_cili_eto = $linea1["num_cili_eto"];
                    }
                    $consulta2 = "SELECT fecha_ven FROM ordenes WHERE id_orden = $id_orden";
                    $resultado2 = mysqli_query($con,$consulta2) ;
                    while ($linea2 = mysqli_fetch_array($resultado2))
                    {
                    	$fecha_ven = $linea2["fecha_ven"];
                    }
                    $consulta3 = "SELECT num_cilindro_eto FROM lote_eto WHERE id_lote_eto = $id_lote_eto";
                    $resultado3 = mysqli_query($con,$consulta3) ;
                    while ($linea3 = mysqli_fetch_array($resultado3))
                    {
                    	$num_cilindro_eto = $linea3["num_cilindro_eto"];
                    }
                    $consulta4 = "SELECT num_cilindro_co2 FROM lote_co2 WHERE id_num_cilindro = $lote_co";
                    $resultado4 = mysqli_query($con,$consulta4) ;
                    while ($linea4 = mysqli_fetch_array($resultado4))
                    {
                    	$num_cilindro_co2 = $linea4["num_cilindro_co2"];
                    }
                    $consulta4 = "SELECT  tipo FROM  tipo_envace WHERE id_tipo_envace = $peso_esperado";
                    $resultado4 = mysqli_query($con,$consulta4) ;
                    while ($linea4 = mysqli_fetch_array($resultado4))
                    {
                    	$tipo = $linea4["tipo"];
                    }
                    $consulta5 = "SELECT * FROM ordenes WHERE id_orden = $id_orden";
                    $resultado5 = mysqli_query($con,$consulta5);
                   	if(mysqli_num_rows($resultado5) > 0)
                    {
                    	while ($linea5 = mysqli_fetch_assoc($resultado5)) 
                    	{
                    		$fecha_ter = $linea5["fecha_ter"];	
                    	}
                    }
                    ?>
                    <tr class="odd gradeX">
                      	<td width="5" style="border: 1px solid black;border-collapse: collapse;"><?php echo $contador; ?></td>                                  
                      	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $num_cili_eto; ?></td>
                      	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $prue_hidro; ?></td>
                      	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $peso_inicial; ?></td>  
                      	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $peso_final; ?></td>  
                      	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $pre_vacio; ?></td>  
                      	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $lote_pro; ?></td>     
                      	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $tara_vacio; ?></td>  
                      	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $tipo; ?></td>  
                      	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $pre_inicial_1; ?></td>  
                      	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $esperado_eto; ?></td>  
                      	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $real_eto; ?></td>
                      	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $esperado_co; ?></td>
                      	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $real_co; ?></td>
                      	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $peso_final_1; ?></td>
                      	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $desviacion; ?></td>
                      	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $pre_final; ?></td>
                      	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $num_cilindro_co2; ?></td>
                      	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $num_cilindro_eto; ?></td>
                      	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $fecha_ven; ?></td>
                    </tr>                           
                <?php
                }mysqli_free_result($resultado);	                                            
      		    ?>
			</tbody>							
		</table>
		<br>
		<label><h2>Orden fabricada el: <?php echo $fecha_ter ?></h2></label>
		<label><h2>Informe creado el: <?php echo date("d-m-Y",$time)?></h2></label>			
	</div>			
<?php
}										
?>
</div>

<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->
<?php
	include("inc/footer.php");
	include("inc/scripts.php"); 
?>
<script type="text/javascript">
function imprSelec(muestra)
{
	var ficha=document.getElementById(muestra);
	var ventimp=window.open(' ','popimpr');
	ventimp.document.write(ficha.innerHTML);
	ventimp.document.close();
	ventimp.print();
	ventimp.close();
}
</script>

<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<script type="text/javascript">
// DO NOT REMOVE : GLOBAL FUNCTIONS!

$(document).ready(function() {
	
	/* // DOM Position key index //
		
	l - Length changing (dropdown)
	f - Filtering input (search)
	t - The Table! (datatable)
	i - Information (records)
	p - Pagination (paging)
	r - pRocessing 
	< and > - div elements
	<"#id" and > - div with an id
	<"class" and > - div with a class
	<"#id.class" and > - div with an id and class
	
	Also see: http://legacy.datatables.net/usage/features
	*/	

	/* BASIC ;*/
		var responsiveHelper_dt_basic = undefined;
		var responsiveHelper_datatable_fixed_column = undefined;
		var responsiveHelper_datatable_col_reorder = undefined;
		var responsiveHelper_datatable_tabletools = undefined;
		
		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};

		$('#dt_basic').dataTable({
			"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
			"autoWidth" : true,
			"preDrawCallback" : function() {
				// Initialize the responsive datatables helper once.
				if (!responsiveHelper_dt_basic) {
					responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
				}
			},
			"rowCallback" : function(nRow) {
				responsiveHelper_dt_basic.createExpandIcon(nRow);
			},
			"drawCallback" : function(oSettings) {
				responsiveHelper_dt_basic.respond();
			}
		});

	/* END BASIC */
	
	/* COLUMN FILTER  */
    var otable = $('#datatable_fixed_column').DataTable({
    	//"bFilter": false,
    	//"bInfo": false,
    	//"bLengthChange": false
    	//"bAutoWidth": false,
    	//"bPaginate": false,
    	//"bStateSave": true // saves sort state using localStorage
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_fixed_column) {
				responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_fixed_column.respond();
		}		
	
    });
    
    // custom toolbar
    $("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');
    	   
    // Apply the filter
    $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {
    	
        otable
            .column( $(this).parent().index()+':visible' )
            .search( this.value )
            .draw();
            
    } );
    /* END COLUMN FILTER */   

	/* COLUMN SHOW - HIDE */
	$('#datatable_col_reorder').dataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_col_reorder) {
				responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_col_reorder.respond();
		}			
	});
	
	/* END COLUMN SHOW - HIDE */

	/* TABLETOOLS */
	$('#datatable_tabletools').dataTable({
		
		// Tabletools options: 
		//   https://datatables.net/extensions/tabletools/button_options
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        "oTableTools": {
        	 "aButtons": [
             "copy",
             "csv",
             "xls",
                {
                    "sExtends": "pdf",
                    "sTitle": "SmartAdmin_PDF",
                    "sPdfMessage": "SmartAdmin PDF Export",
                    "sPdfSize": "letter"
                },
             	{
                	"sExtends": "print",
                	"sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>"
            	}
             ],
            "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
        },
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_tabletools) {
				responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_tabletools.respond();
		}
	});
	
	/* END TABLETOOLS */

})

</script>
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script type="text/javascript">

 
		
		$(document).ready(function() {
			
			// menu
			$("#menu").menu();
		
			/*
			 * AUTO COMPLETE AJAX
			 */
		
			function log(message) {
				$("<div>").text(message).prependTo("#log");
				$("#log").scrollTop(0);
			}
		
			$("#city").autocomplete({
				source : function(request, response) {
					$.ajax({
						url : "http://ws.geonames.org/searchJSON",
						dataType : "jsonp",
						data : {
							featureClass : "P",
							style : "full",
							maxRows : 12,
							name_startsWith : request.term
						},
						success : function(data) {
							response($.map(data.geonames, function(item) {
								return {
									label : item.name + (item.adminName1 ? ", " + item.adminName1 : "") + ", " + item.countryName,
									value : item.name
								}
							}));
						}
					});
				},
				minLength : 2,
				select : function(event, ui) {
					log(ui.item ? "Selected: " + ui.item.label : "Nothing selected, input was " + this.value);
				}
			});
		
			/*
			 * Spinners
			 */
			$("#spinner").spinner();
			$("#spinner-decimal").spinner({
				step : 0.01,
				numberFormat : "n"
			});
		
			$("#spinner-currency").spinner({
				min : 5,
				max : 2500,
				step : 25,
				start : 1000,
				numberFormat : "C"
			});
		
			/*
			 * CONVERT DIALOG TITLE TO HTML
			 * REF: http://stackoverflow.com/questions/14488774/using-html-in-a-dialogs-title-in-jquery-ui-1-10
			 */
			$.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
				_title : function(title) {
					if (!this.options.title) {
						title.html("&#160;");
					} else {
						title.html(this.options.title);
					}
				}
			}));
		
		
			/*
			* DIALOG SIMPLE
			*/
		
			// Dialog click
			$('#dialog_link').click(function() {
				$('#dialog_simple').dialog('open');
				return false;
		
			});
		
			$('#dialog_simple').dialog({
				autoOpen : false,
				width : 600,
				resizable : false,
				modal : true,
				title : "<div class='widget-header'><h4><i class='fa fa-warning'></i> Empty the recycle bin?</h4></div>",
				buttons : [{
					html : "<i class='fa fa-trash-o'></i>&nbsp; Delete all items",
					"class" : "btn btn-danger",
					click : function() {
						$(this).dialog("close");
					}
				}, {
					html : "<i class='fa fa-times'></i>&nbsp; Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
					}
				}]
			});
		
			/*
			* DIALOG HEADER ICON
			*/
		
			// Modal Link
			$('#modal_link').click(function() {
				$('#dialog-message').dialog('open');
				return false;
			});
		
			$("#dialog-message").dialog({
				autoOpen : false,
				modal : true,
				title : "<div class='widget-header'><h4><i class='icon-ok'></i> jQuery UI Dialog</h4></div>",
				buttons : [{
					html : "Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
					}
				}]
		
			});
		
			/*
			 * Remove focus from buttons
			 */
			$('.ui-dialog :button').blur();
		
			/*
			 * Just Tabs
			 */
		
			$('#tabs').tabs();
		
			/*
			 *  Simple tabs adding and removing
			 */
		
			$('#tabs2').tabs();
		
			// Dynamic tabs
			var tabTitle = $("#tab_title"), tabContent = $("#tab_content"), tabTemplate = "<li style='position:relative;'> <span class='air air-top-left delete-tab' style='top:7px; left:7px;'><button class='btn btn-xs font-xs btn-default hover-transparent'><i class='fa fa-times'></i></button></span></span><a href='#{href}'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; #{label}</a></li>", tabCounter = 2;
		
			var tabs = $("#tabs2").tabs();
		
			// modal dialog init: custom buttons and a "close" callback reseting the form inside
			var dialog = $("#addtab").dialog({
				autoOpen : false,
				width : 600,
				resizable : false,
				modal : true,
				buttons : [{
					html : "<i class='fa fa-times'></i>&nbsp; Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
		
					}
				}]
			});
		
			// addTab form: calls addTab function on submit and closes the dialog
			var form = dialog.find("form").submit(function(event) {
				addTab();
				dialog.dialog("close");
				
			});
		
			// actual addTab function: adds new tab using the input from the form above

			
			function addTab() 
			{
				
				var label = tabTitle.val() || "Tab " + tabCounter, id = "tabs-" + tabCounter, li = $(tabTemplate.replace(/#\{href\}/g, "#" + id).replace(/#\{label\}/g, label)), tabContentHtml = tabContent.val() || "Tab " + tabCounter + " content.";
		
				tabs.find(".ui-tabs-nav").append(li);
				tabs.append("<div id='" + id + "'><p>" + tabContentHtml + "</p></div>");
				tabs.tabs("refresh");
				tabCounter++;
		
				// clear fields
				$("#tab_title").val("");
				$("#tab_content").val("");
			}
		
			// addTab button: just opens the dialog
			$("#add_tab").button().click(function() {
				var idd = document.getElementById('num').firstChild.nodeValue;

				
				dialog.dialog("open");
			});
		
			// close icon: removing the tab on click
					
			/*
			* ACCORDION
			*/
			//jquery accordion
			
		     var accordionIcons = {
		         header: "fa fa-plus",    // custom icon class
		         activeHeader: "fa fa-minus" // custom icon class
		     };
		     
			$("#accordion").accordion({
				autoHeight : false,
				heightStyle : "content",
				collapsible : true,
				animate : 300,
				icons: accordionIcons,
				header : "h4",
			})
		
			/*
			 * PROGRESS BAR
			 */
			$("#progressbar").progressbar({
		     	value: 25,
		     	create: function( event, ui ) {
		     		$(this).removeClass("ui-corner-all").addClass('progress').find(">:first-child").removeClass("ui-corner-left").addClass('progress-bar progress-bar-success');
				}
			});			

		})

	</script>
	


<?php 
	//include footer
	include("inc/google-analytics.php"); 
}
else
{
    header("Location:index.php");
}
?>