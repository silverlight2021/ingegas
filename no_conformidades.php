<?php
date_default_timezone_set("America/Bogota");

require ("libraries/conexion.php");
require ("libraries/seguimiento.php");
session_start();

if(@$_SESSION['logged']== 'yes')
{ 	
	$id_user =$_SESSION['su'];
	$fecha=date("Y/m/d");
	if(isset($_POST['cambiar_estado_cili_eto']))
	{
		$id_cilindro_eto_2 = $_POST['id_cilindro_eto_2'];
		$id_estado1 = $_POST['id_estado1'];
		$consulta= "UPDATE cilindro_eto SET  
	                id_estado = '$id_estado1'                                              
	                WHERE id_cilindro_eto = $id_cilindro_eto_2 ";
    	$resultado = mysqli_query($con,$consulta) ;

	    if ($resultado == FALSE)
	    {
	      	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
	    }
	    else
	    {	      	
		    header('Location: no_conformidades.php');			      
	    }
	}
	if (isset($_GET['devolver'])) 
    {
    	$id = isset($_REQUEST['id_cilindro_eto']) ? $_REQUEST['id_cilindro_eto'] : NULL;
    	$observacion = "Cilindro Devuelto";        
        $consulta  = "INSERT INTO seguimiento_conformidad
        (fecha_hora,obs_con,id_cilindro_eto,id_user) VALUES ('".date("Y/m/d H:i:s")."','".$observacion."','".$id."','".$id_user."')";
        $resultado = mysqli_query($con,$consulta) ;
        if ($resultado == FALSE)
        {
            echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
        }
        else
        {
            $id_seguimiento_conformidad = mysqli_insert_id($con);
            seguimiento(12,$id,$id_user,7,$id_seguimiento_conformidad);//id_seguimiento,id_cilindro_eto,id_user,id_proceso,id_has_proceso
        } 
    }
    if (isset($_GET['reponer'])) 
    {
    	$id = isset($_REQUEST['id_cilindro_eto']) ? $_REQUEST['id_cilindro_eto'] : NULL;
    	$observacion = "Cilindro Repuesto";        
        $consulta  = "INSERT INTO seguimiento_conformidad
        (fecha_hora,obs_con,id_cilindro_eto,id_user) VALUES ('".date("Y/m/d H:i:s")."','".$observacion."','".$id."','".$id_user."')";
        $resultado = mysqli_query($con,$consulta) ;
        if ($resultado == FALSE)
        {
            echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
        }
        else
        {
            $id_seguimiento_conformidad = mysqli_insert_id($con);
            seguimiento(12,$id,$id_user,7,$id_seguimiento_conformidad);//id_seguimiento,id_cilindro_eto,id_user,id_proceso,id_has_proceso
        } 
    }
    if(isset($_POST['g_no_conformidad']))
	{
		$id_no_conformidad =$_POST['id_no_conformidad_3'];
		$id_tipo_registro =$_POST['id_tipo_registro'];		
		$solicitante =$_POST['solicitante'];
		$descripcion =$_POST['descripcion'];
		$correccion =$_POST['correccion'];
		$ana_causas =$_POST['ana_causas'];
		$accion =$_POST['accion'];
		$responsable =$_POST['responsable'];
		$observaciones =$_POST['observaciones'];
		$tapa =$_POST['tapa'];
		$tapon =$_POST['tapon'];
		$id_user_1 =$_POST['id_user_1'];
		$peso_actual =$_POST['peso_actual'];
		$presion =$_POST['presion'];


		$consulta= "UPDATE no_conformidad SET 	                
		id_tipo_registro = '".$id_tipo_registro."',
    	solicitante = '".$solicitante."',
  		descripcion = '".$descripcion."', 
  		correccion = '".$correccion."', 
  		ana_causas = '".$ana_causas."', 
  		accion = '".$accion."',
  		responsable = '".$responsable."',
  		observaciones = '".$observaciones."',
  		tapa = '".$tapa."',
  		tapon = '".$tapon."',
  		id_user_1 = '".$id_user_1."',
  		peso_actual = '".$peso_actual."',
  		presion = '".$presion."'
        WHERE id_no_conformidad = $id_no_conformidad ";
	    $resultado = mysqli_query($con,$consulta) ;
	    if ($resultado == FALSE)
	    {
	      	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
	    }
	    else
	    {
	    	//seguimiento(5,$id_cilindro_eto1,$User_idUser,3,$id_produccion_mezclas);//id_seguimiento,id_cilindro_eto,id_user_1,id_proceso,id_has_proceso
	    	header('Location: no_conformidades.php');
	    }
	}

	require_once("inc/init.php");
	require_once("inc/config.ui.php");
	$page_title = "No Conformidades";
	$page_css[] = "your_style.css";
	include("inc/header.php");
	$page_nav[""][""][""][""] = true;
	include("inc/nav.php");
	
?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<!-- Modal no conformidad-->
<div class="modal fade" id="modal_conformidad" role="dialog">
    <form id="tracking_client" method="POST" action="no_conformidades.php">
        <input type="hidden" name="id_cilindro_eto_2"  id="id_cilindro_eto_2"  value="id_cilindro_eto_2">
		<input type="hidden" name="id_no_conformidad_2"  id="id_no_conformidad_2"  value="id_no_conformidad_2">        
        <div class="modal-dialog">
          	<div class="modal-content">
            	<div class="modal-header">
              		<button type="button" class="close" data-dismiss="modal">&times;</button>
              		<h4 class="modal-title">Seguimiento Conformidad Cilindro</h4>
            	</div>
            	<div style="overflow-y: scroll;height: 200px; ">          
              		<div class='modal-body' id="id02"></div>
            	</div>
            	<div class='modal-body'>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<?php
								$consulta6 ="SELECT * FROM estado_cilindro_eto ORDER BY estado_cili_eto ASC";
								$resultado6 = mysqli_query($con,$consulta6) ;
								echo "<label><div id=\"est\"></div></label>";
								echo"<label class='select'>";
								echo "<select  name='id_estado1'>";
								echo "<option value='0'>Seleccione estado a cambiar..</option>";
								while($linea6 = mysqli_fetch_array($resultado6))
								{
									$id_estado_cilindro = $linea6['id_estado_cilindro'];
									$estado_cili_eto = $linea6['estado_cili_eto'];
									if ($id_estado_cilindro==$id_estado1)
									{
										echo "<option value='$id_estado_cilindro' selected >$estado_cili_eto</option>"; 
									}
									else 
									{
										echo "<option value='$id_estado_cilindro'>$estado_cili_eto</option>"; 
									} 
								}//fin while 
								echo "</select>";
								echo "<i></i>";
								echo "</label>";											
								?>	
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								 <input type="submit" class="btn btn-success" name ="cambiar_estado_cili_eto" value="Cambiar Estado">
							</div>
						</div>
					</div>
              		<label >Observaciones</label>
              		<label class="textarea textarea-resizable">                     
                		<textarea id="observacion" rows="5" cols="78" class="form-control" name="observacion"></textarea> 
              		</label>
            	</div>
            	<div class="modal-footer">
              		<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              		<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="guardar_trk_client()">Guardar</button>
            	</div>
          	</div>

        </div>
    </form>
</div>
<div class="modal fade" id="formulario" role="dialog">
    <form id="form1" name="form1" method="POST" action="no_conformidades.php">
        <input type="hidden" name="id_no_conformidad_3"  id="id_no_conformidad_3"  value="id_no_conformidad_3">
        <div class="modal-dialog">
          	<div class="modal-content">
            	<div class="modal-header">
              		<button type="button" class="close" data-dismiss="modal">&times;</button>
              		<h4 class="modal-title">NO CONFORMIDAD O PUNTO DE MEJORADE PROVEEDOR</h4>
            	</div>
            	<div class='modal-body'>            		
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#iss1" data-toggle="tab">Reporte no conformidad</a>
						</li>
						<li>
							<a href="#iss2" data-toggle="tab">Ingreso a Planta no conforme</a>
						</li>
					</ul>
					<div class="tab-content padding-10">
						<div class="tab-pane active" id="iss1">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="category">Fecha :</label>
										<input type="text" class="form-control" placeholder="Fecha" name="fecha_hora_1" readonly  />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="category">Tipo de Registro :</label>
										<select class="form-control" name='id_tipo_registro' >
											<option value="0">Seleccione ...</option>
											<option value="1">No Conformidad </option>
											<option value="2">Punto de Mejora </option>							
										</select><i></i>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="category">Origen :</label>
										<input type="text" class="form-control" placeholder="Origen" name="origen" readonly  />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="category">Destino :</label>
										<input type="text" class="form-control" placeholder="Destino" name="destino" readonly  />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label for="category">Solicitante :</label>
										<input type="text" class="form-control" placeholder="Solicitante" name="solicitante"  />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label for="category">Descripcion de la no conformidad o punto de mejora</label>
											<label class="textarea textarea-resizable">                     
					                		<textarea id="descripcion" rows="3" cols="78" class="form-control" name="descripcion"></textarea> 
					              		</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label for="category">Correcion</label>
											<label class="textarea textarea-resizable">                     
					                		<textarea id="correccion" rows="3" cols="78" class="form-control" name="correccion"></textarea> 
					              		</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label for="category">Analisis y Causas</label>
											<label class="textarea textarea-resizable">                     
					                		<textarea id="ana_causas" rows="3" cols="78" class="form-control" name="ana_causas"></textarea> 
					              		</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label for="category">Acción correctiva o de mejora</label>
											<label class="textarea textarea-resizable">                     
					                		<textarea id="accion" rows="3" cols="78" class="form-control" name="accion"></textarea> 
					              		</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label for="category">Responsable :</label>
										<input type="text" class="form-control" placeholder="Responsable" name="responsable" />
									</div>
								</div>
							</div>
							<fieldset>
								<legend><strong>Seguimiento</strong></legend>
								<div class="row">								
									<div class="col-md-12">
										<div class="form-group">
											<label for="category">Cumplimiento :</label>
											<input type="text" class="form-control" placeholder="Cumplimiento" name="cumplimiento_seguimiento" id="cumplimiento_seguimiento"/>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<label for="category">Responsable :</label>
											<input type="text" class="form-control" placeholder="Responsables" name="responsable_seguimiento" id="responsable_seguimiento"/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label for="category">Fecha :</label>
											<input type="date" class="form-control" placeholder="Fecha" name="fecha_seguimiento" id="fecha_seguimiento" />
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label for="category">.</label>
											<button type="button" class="btn btn-success" data-dismiss="modal" onclick="guardar_seguimiento()">Agregar</button>
										</div>
									</div>
								</div>
								<div class="row">
									<div class='modal-body' id="id0222"></div>	
								</div>
								</fieldset>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
									<a onclick="abrir()" target="_blank"><img src="img/pdf-icon.png" width="50" height="50"></a>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="iss2">	
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="category">FECHA / HORA RECOGIDA :</label>
										<input type="text" class="form-control" name="fech" id="fech" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="category">CLIENTE :</label>
										<input type="text" class="form-control" name="cliente" id="cliente" readonly/>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-8">
									<div class="form-group">
										<label for="category">RECIBE TRANSPORTE :</label>
										<input type="text" class="form-control" name="recibe" id="recibe" readonly/>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="category">CME :</label>
										<input type="text" class="form-control" name="cme" id="cme" readonly/>
									</div>
								</div>
							</div>						
							<legend>CONDICIONES DE INGRESO</legend>
							<div class="row">
								<div class="col-md-3">
									<div class="form-group">
										<label for="category">CIL. NO. :</label>
										<input type="text" class="form-control" name="num_cili_eto" id="num_cili_eto" readonly  />
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label for="category">PRODUCTO :</label>
										<input type="text" class="form-control" name="producto" id="producto" readonly  />
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<?php
											$consulta7 ="SELECT * FROM si_no ";
											$resultado7 = mysqli_query($con,$consulta7) ;
										?>	
										<label for="category">TAPA :</label>
										<select class="form-control" name='tapa' >
										<option value='0'>Seleccione...</option>
										<?php
										while($linea7 = mysqli_fetch_array($resultado7))
										{
											$id_si_no = $linea7['id_si_no'];
											$valor = $linea7['valor'];
											

											if ($id_si_no==$tapa)
											{
												echo "<option value='$id_si_no' selected >$valor</option>"; 
											}
											else 
											{
												echo "<option value='$id_si_no'>$valor</option>"; 
											} 
										}//fin while 
										?>
										</select>								
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<?php
											$consulta7 ="SELECT * FROM si_no ";
											$resultado7 = mysqli_query($con,$consulta7) ;
										?>	
										<label for="category">TAPÓN :</label>
										<select class="form-control" name='tapon' >
										<option value='0'>Seleccione...</option>
										<?php
										while($linea7 = mysqli_fetch_array($resultado7))
										{
											$id_si_no = $linea7['id_si_no'];
											$valor = $linea7['valor'];
											

											if ($id_si_no==$tapon)
											{
												echo "<option value='$id_si_no' selected >$valor</option>"; 
											}
											else 
											{
												echo "<option value='$id_si_no'>$valor</option>"; 
											} 
										}//fin while 
										?>
										</select>								
									</div>
								</div>
							</div>
							<div class="row">								
								<div class="col-md-4">
									<div class="form-group">
										<label for="category">PESO ACTUAL Kg:</label>
										<input type="text" class="form-control" name="peso_actual" id="peso_actual" />
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="category">PRESIÓN PSI:</label>
										<input type="text" class="form-control" name="presion" id="presion"/>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="category">SALDO PRODUCTO Kg:</label>
										<input type="text" class="form-control" name="kilos" id="kilos" readonly  />
									</div>
								</div>
							</div>							
							<legend>PRODUCCIÓN</legend>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label for="category">FECHA LLENADO :</label>
										<input type="text" class="form-control" name="fecha_llenado" id="fecha_llenado" readonly  />
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="category">PESO SALIDA :</label>
										<input type="text" class="form-control" name="peso_salida" id="peso_salida" readonly  />
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="category">TARA VACÍO :</label>
										<input type="text" class="form-control" name="tara_vacio" id="tara_vacio" readonly  />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label for="category">LOTE :</label>
										<input type="text" class="form-control" name="lote_pro" id="lote_pro" readonly  />
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="category">NUM ORDEN :</label>
										<input type="text" class="form-control" name="num_ord" id="num_ord" readonly  />
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="category">PRESIÓN SALIDA PSI :</label>
										<input type="text" class="form-control" name="pre_final" id="pre_final" readonly  />
									</div>
								</div>
								
							</div>
							<div class="row">
								<div class="form-group">
									<label for="category">OBSERVACIONES</label>
										<label class="textarea textarea-resizable">                     
				                		<textarea id="observaciones" rows="3" cols="78" class="form-control" name="observaciones"></textarea> 
				              		</label>
								</div>
							</div>	
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<?php
											$consulta7 ="SELECT * FROM user_has_profile WHERE profile_idprofile = 7 ";
											$resultado7 = mysqli_query($con,$consulta7) ;
										?>	
										<label for="category">OPERARIO :</label>
										<select class="form-control" name='id_user_1' >
										<option value='0'>Seleccione...</option>
										<?php
										while($linea7 = mysqli_fetch_array($resultado7))
										{
											$User_idUser = $linea7['User_idUser'];

											$consulta1  = "SELECT * FROM user WHERE idUser = '".$User_idUser."'";
											$resultado1 = mysqli_query($con,$consulta1) ;
											$linea1 = mysqli_fetch_array($resultado1);
											$Name = isset($linea1["Name"]) ? $linea1["Name"] : NULL;
											$LastName = isset($linea1["LastName"]) ? $linea1["LastName"] : NULL;
											$nombre = $Name." ".$LastName;
											

											if ($User_idUser==$id_user_1)
											{
												echo "<option value='$User_idUser' selected >$nombre</option>"; 
											}
											else 
											{
												echo "<option value='$User_idUser'>$nombre</option>"; 
											} 
										}//fin while 
										?>
										</select>								
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="category">REVISA :</label>
										<input type="text" class="form-control" name="revisa" id="revisa" value="Juan Pablo Santa M"/>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
									<a onclick="abrir_2()" target="_blank"><img src="img/pdf-icon.png" width="50" height="50"></a>
									</div>
								</div>
							</div>	
														
						</div>
					</div>	
            	</div>
            	<div class="modal-footer">            		
              		<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              		<input type="submit" value="Guardar" name="g_no_conformidad" id="g_no_conformidad" class="btn btn-primary" />
            	</div>
          	</div>

        </div>
    </form>
</div>
<!-- Fin Modal no conformidad-->
<div id="main" role="main">
<?php
if (in_array(23, $acc))
{
?>
	<div id="content">
		<section id="widget-grid" class="">
			<div class="row">
				<article class="col-md-8 col-md-offset-2">
					<div class="jarviswidget" id="wid-id-2" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h2>No Conformidades</h2>			
						</header>
						<div>				
							<div class="jarviswidget-editbox"></div>																	
							<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">						
								<thead>
									<tr>
										<th>#</th>                                            
			                            <th>Cilindro</th>
			                            <th>Fecha Ingreso</th>
			                            <th>Origen</th>
			                            <th>Tipo Gas</th>
										<th>Capacidad (Kg)</th>
										<th>Estado</th>
			                            <th>Acción</th>			                            
									</tr>
								</thead>
								<tbody>													
								  	<?php			                          	
			                            $consulta1 = "SELECT * FROM no_conformidad ORDER BY fecha_hora DESC";
			                            $resultado1 = mysqli_query($con,$consulta1) ;
			                            while ($linea1 = mysqli_fetch_array($resultado1))
			                            {
			                            	$contador1 = $contador1 + 1;
			                            	$id_no_conformidad = $linea1["id_no_conformidad"];			                            	
											$id_cilindro_eto = $linea1["id_cilindro_eto"];
											$fecha_hora = $linea1["fecha_hora"];
											$origen = $linea1["origen"];	
											
											$consulta = "SELECT * FROM cilindro_eto WHERE id_cilindro_eto =".$id_cilindro_eto;
			                                $resultado = mysqli_query($con,$consulta) ;
			                                while ($linea = mysqli_fetch_array($resultado))
			                                {
			                                	$id_tipo_cilindro = $linea["id_tipo_cilindro"];
			                                	$id_tipo_envace = $linea["id_tipo_envace"];
			                                	$id_estado = $linea["id_estado"];
			                                	$num_cili_eto = $linea["num_cili_eto"];
			                                			                                	
			                                }mysqli_free_result($resultado);	

											$consulta3 = "SELECT * FROM tipo_cilindro WHERE id_tipo_cilindro =".$id_tipo_cilindro;
		                                    $resultado3 = mysqli_query($con,$consulta3) ;
		                                    while ($linea3 = mysqli_fetch_array($resultado3))
		                                    {
		                                    	$tipo_cili = $linea3["tipo_cili"];	                                    	
		                                    }mysqli_free_result($resultado3);

		                                    $consulta4 = "SELECT * FROM tipo_envace WHERE id_tipo_envace =".$id_tipo_envace;
		                                    $resultado4 = mysqli_query($con,$consulta4) ;
		                                    while ($linea4 = mysqli_fetch_array($resultado4))
		                                    {
		                                    	$tipo = $linea4["tipo"];
		                                    }mysqli_free_result($resultado4);	

											$consulta6 = "SELECT * FROM estado_cilindro_eto WHERE id_estado_cilindro = $id_estado";
		                                    $resultado6 = mysqli_query($con,$consulta6) ;
		                                    while ($linea6 = mysqli_fetch_array($resultado6))
		                                    {
		                                    	$estado_cili_eto = $linea6["estado_cili_eto"];		                                                	
		                                    }mysqli_free_result($resultado6);
			                            			                            		                            
			                            ?>
				                            <tr class="odd gradeX">
				                              	<td width="5"><?php echo $contador1; ?></td> 
				                              	<td><?php echo $num_cili_eto; ?></td>
				                              	<td><?php echo $fecha_hora; ?></td>
				                              	<td><?php echo $origen; ?></td>
				                              	<td><?php echo $tipo_cili; ?></td>
				                              	<td><?php echo $tipo; ?></td>
				                              	<td><?php echo $estado_cili_eto; ?></td>
				                              	<?php
												if (in_array(24, $acc))
												{
												?>
					                              	<td width="100" style="vertical-align:bottom;">	                              	
					                              		<input type="image" onclick="estado(<?php echo $id_cilindro_eto; ?>);seguimiento_conformidad(<?php echo $id_cilindro_eto; ?>,<?php echo $id_no_conformidad; ?>)" src="img/edit.png" width="30" height="30">	
					                              		<input type="image" onclick="formulario(<?php echo $id_no_conformidad; ?>)" src="img/lupa.png" width="30" height="30">
					                              	</td>
					                            <?php
												}										
												?>   

				                            </tr>                           
			                            <?php
			                            }mysqli_free_result($resultado1);	                                            
		                  		    ?>
								</tbody>							
							</table>
						</div>            
         			</div>
         		</article>
        	</div>      
   		</section> 
    </div>
<?php
}										
?>
</div>

<?php
  include("inc/footer.php");
  include("inc/scripts.php"); 
?>

<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/jquery-form/jquery-form.min.js"></script>
<script type="text/javascript">
function abrir_2() 
{
	var id_no_conformidad=form1.id_no_conformidad_3.value;
	window.open("pdf_conformidades_2.php?id_no_conformidad="+id_no_conformidad+"","logistica","width=auto,height=auto,menubar=no") 
}
function abrir() 
{
	var id_no_conformidad=form1.id_no_conformidad_3.value;
	window.open("pdf_conformidades_1.php?id_no_conformidad="+id_no_conformidad+"","logistica","width=auto,height=auto,menubar=no") 
}
function formulario(id_no_conformidad) 
{
	seguimiento_conformidad_formulario(id_no_conformidad);
	$("#formulario").modal();
	$("input[name=id_no_conformidad_3]").val(id_no_conformidad);
	$.ajax({
		url: 'id_conformidad_formulario.php',
		type: 'POST',
		data: 'dato='+id_no_conformidad,
	})
	.done(function(data) {
		var objeto = JSON.parse(data);							
		$("select[name=id_tipo_registro]").val(objeto.id_tipo_registro);
		$("input[name=fecha_hora_1]").val(objeto.fecha_hora);
		$("input[name=solicitante]").val(objeto.solicitante);
		$("textarea[name=descripcion]").val(objeto.descripcion);
		$("textarea[name=correccion]").val(objeto.correccion);
		$("textarea[name=ana_causas]").val(objeto.ana_causas);
		$("textarea[name=accion]").val(objeto.accion);
		$("input[name=responsable]").val(objeto.responsable);
		$("textarea[name=observaciones]").val(objeto.observaciones);					
		$("input[name=origen]").val(objeto.origen);
		$("input[name=destino]").val(objeto.destino);
		$("input[name=num_cili_eto]").val(objeto.num_cili_eto);
		$("input[name=producto]").val(objeto.producto);
		$("select[name=tapa]").val(objeto.tapa);
		$("select[name=tapon]").val(objeto.tapon);
		$("input[name=fech]").val(objeto.fecha_hora);
		$("input[name=peso_actual]").val(objeto.peso_actual);
		$("input[name=presion]").val(objeto.presion);
		$("input[name=fecha_llenado]").val(objeto.fecha_llenado);
		$("input[name=peso_salida]").val(objeto.peso_salida);
		$("input[name=tara_vacio]").val(objeto.tara_vacio);
		$("input[name=recibe]").val(objeto.recibe);
		$("input[name=cliente]").val(objeto.cliente);
		$("input[name=cme]").val(objeto.cme);
		$("input[name=lote_pro]").val(objeto.lote_pro);
		$("select[name=id_user_1]").val(objeto.id_user_1);
		$("input[name=kilos]").val(objeto.kilos);
		$("input[name=num_ord]").val(objeto.num_ord);
		$("input[name=pre_final]").val(objeto.pre_final);		
	})
	.fail(function() {
		console.log("error");
	});

}
function estado(estado) 
{
	$.ajax({
		url: 'estado_cilindro.php',
		type: 'POST',
		data: 'dato='+estado,
	})
	.done(function(data) {
		var objeto = JSON.parse(data);					
		document.getElementById("est").innerHTML = "Estado actual : " +objeto.id_estado1;				
	})
	.fail(function() {
		console.log("error");
	});
}

function seguimiento_conformidad(id_cilindro_eto,id_no_conformidad) 
{
	$("input[name=id_cilindro_eto_2]").val(id_cilindro_eto);
	$("input[name=id_no_conformidad_2]").val(id_no_conformidad);	
	$("#modal_conformidad").modal();
	$.ajax({
		url: 'id_conformidad.php',
		type: 'POST',
		data: 'dato='+id_no_conformidad,
	})
	.done(function(data) {

		var objeto1 = JSON.parse(data);					
		var i;
		var tabla = "<table class='table table-striped table-bordered table-hover' width='100%'><colgroup> <col width='50%'><col width='50%'></colgroup>";

		for(i = 0; i < objeto1.length; i++) {
			tabla += "<tr><td colspan='1'>Fecha : " + 
			objeto1[i].fecha_hora +
			"</td><td colspan='1'>Usuario : " +
			objeto1[i].id_user +
			"</td></tr><tr><td colspan='6'>" +
			objeto1[i].obs_con +
			"</td></tr>";
		}
		tabla += "</table>";

		document.getElementById("id02").innerHTML = tabla;
	})
	.fail(function() {
		console.log("error");
	});	
}

function guardar_trk_client()
{	
	var formulario = $("#tracking_client").serialize();     
	$.ajax({
		url: 'data_seguimiento.php',
		type: 'POST',
		data: formulario,
	})
	.done(function(resp){			 	
	  	$.smallBox({
	  		title : "Alerta",
	  		content : "<i class='fa fa-clock-o'></i> <i>Seguimiento Creado</i>",
	  		color : "#659265",
	  		iconSmall : "fa fa-check fa-2x fadeInRight animated",
	  		timeout : 4000
	  	});	   
	  	$('#observacion').val('');    		
	})   	
}

function guardar_seguimiento()
{
	var id_no_conformidad=form1.id_no_conformidad_3.value;
	var fecha_seguimiento=form1.fecha_seguimiento.value;
	var cumplimiento_seguimiento=form1.cumplimiento_seguimiento.value;
	var responsable_seguimiento=form1.responsable_seguimiento.value;
	//alert(lol);

	$.ajax({
		url: 'data_seguimiento_conformidad.php',
		type: 'POST',
		data: 'dato1='+id_no_conformidad+'&dato2='+fecha_seguimiento+'&dato3='+cumplimiento_seguimiento+'&dato4='+responsable_seguimiento,
	})
	.done(function(resp){			 	
	  	$.smallBox({
	  		title : "Alerta",
	  		content : "<i class='fa fa-clock-o'></i> <i>Seguimiento Creado</i>",
	  		color : "#659265",
	  		iconSmall : "fa fa-check fa-2x fadeInRight animated",
	  		timeout : 4000
	  	});
		//seguimiento_conformidad_formulario(id_no_conformidad);
		$('#fecha_seguimiento').val('');  
		$('#cumplimiento_seguimiento').val('');  
		$('#responsable_seguimiento').val('');  
		formulario(id_no_conformidad); 	       		
	})  
}

function seguimiento_conformidad_formulario(id_no_conformidad) 
{
	$.ajax({
		url: 'id_conformidad_1.php',
		type: 'POST',
		data: 'dato='+id_no_conformidad,
	})
	.done(function(data) {

		var objeto1 = JSON.parse(data);					
		var i;
		var tabla = "<table class='table table-striped table-bordered table-hover' width='100%'><colgroup> <col width='50%'><col width='50%'></colgroup>";

		for(i = 0; i < objeto1.length; i++) {
			tabla += "<tr><td colspan='1'>Fecha : " + 
			objeto1[i].fecha_seguimiento +
			"</td><td colspan='1'>Responsables : " +
			objeto1[i].responsable_seguimiento +
			"</td></tr><tr><td colspan='6'>Cumplimiento :</br>" +
			objeto1[i].cumplimiento_seguimiento +
			"</td></tr>";
		}
		tabla += "</table>";

		document.getElementById("id0222").innerHTML = tabla;
	})
	.fail(function() {
		console.log("error");
	});	
}
</script>
<script type="text/javascript">
	function editar(cili)
	{		
		$("#modal_lavar").modal();
		//alert(cili);
		//document.getElementById("id_cilindro_eto_1").innerHTML = cili;
		$("input[name=id_cilindro_eto_1]").val(cili);
		//$("input[name=num_cili]").val(num);
	}
</script>
<script type="text/javascript">
 
		
		$(document).ready(function() {
			
			// menu
			$("#menu").menu();
		
			/*
			 * AUTO COMPLETE AJAX
			 */
		
			function log(message) {
				$("<div>").text(message).prependTo("#log");
				$("#log").scrollTop(0);
			}
		
			$("#city").autocomplete({
				source : function(request, response) {
					$.ajax({
						url : "http://ws.geonames.org/searchJSON",
						dataType : "jsonp",
						data : {
							featureClass : "P",
							style : "full",
							maxRows : 12,
							name_startsWith : request.term
						},
						success : function(data) {
							response($.map(data.geonames, function(item) {
								return {
									label : item.name + (item.adminName1 ? ", " + item.adminName1 : "") + ", " + item.countryName,
									value : item.name
								}
							}));
						}
					});
				},
				minLength : 2,
				select : function(event, ui) {
					log(ui.item ? "Selected: " + ui.item.label : "Nothing selected, input was " + this.value);
				}
			});
		
			/*
			 * Spinners
			 */
			$("#spinner").spinner();
			$("#spinner-decimal").spinner({
				step : 0.01,
				numberFormat : "n"
			});
		
			$("#spinner-currency").spinner({
				min : 5,
				max : 2500,
				step : 25,
				start : 1000,
				numberFormat : "C"
			});
		
			/*
			 * CONVERT DIALOG TITLE TO HTML
			 * REF: http://stackoverflow.com/questions/14488774/using-html-in-a-dialogs-title-in-jquery-ui-1-10
			 */
			$.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
				_title : function(title) {
					if (!this.options.title) {
						title.html("&#160;");
					} else {
						title.html(this.options.title);
					}
				}
			}));
		
		
			/*
			* DIALOG SIMPLE
			*/
		
			// Dialog click
			$('#dialog_link').click(function() {
				$('#dialog_simple').dialog('open');
				return false;
		
			});
		
			$('#dialog_simple').dialog({
				autoOpen : false,
				width : 600,
				resizable : false,
				modal : true,
				title : "<div class='widget-header'><h4><i class='fa fa-warning'></i> Empty the recycle bin?</h4></div>",
				buttons : [{
					html : "<i class='fa fa-trash-o'></i>&nbsp; Delete all items",
					"class" : "btn btn-danger",
					click : function() {
						$(this).dialog("close");
					}
				}, {
					html : "<i class='fa fa-times'></i>&nbsp; Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
					}
				}]
			});
		
			/*
			* DIALOG HEADER ICON
			*/
		
			// Modal Link
			$('#modal_link').click(function() {
				$('#dialog-message').dialog('open');
				return false;
			});
		
			$("#dialog-message").dialog({
				autoOpen : false,
				modal : true,
				title : "<div class='widget-header'><h4><i class='icon-ok'></i> jQuery UI Dialog</h4></div>",
				buttons : [{
					html : "Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
					}
				}]
		
			});
		
			/*
			 * Remove focus from buttons
			 */
			$('.ui-dialog :button').blur();
		
			/*
			 * Just Tabs
			 */
		
			$('#tabs').tabs();
		
			/*
			 *  Simple tabs adding and removing
			 */
		
			$('#tabs2').tabs();
		
			// Dynamic tabs
			var tabTitle = $("#tab_title"), tabContent = $("#tab_content"), tabTemplate = "<li style='position:relative;'> <span class='air air-top-left delete-tab' style='top:7px; left:7px;'><button class='btn btn-xs font-xs btn-default hover-transparent'><i class='fa fa-times'></i></button></span></span><a href='#{href}'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; #{label}</a></li>", tabCounter = 2;
		
			var tabs = $("#tabs2").tabs();
		
			// modal dialog init: custom buttons and a "close" callback reseting the form inside
			var dialog = $("#addtab").dialog({
				autoOpen : false,
				width : 600,
				resizable : false,
				modal : true,
				buttons : [{
					html : "<i class='fa fa-times'></i>&nbsp; Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
		
					}
				}]
			});
		
			// addTab form: calls addTab function on submit and closes the dialog
			var form = dialog.find("form").submit(function(event) {
				addTab();
				dialog.dialog("close");
				
			});
		
			// actual addTab function: adds new tab using the input from the form above

			
			function addTab() 
			{
				
				var label = tabTitle.val() || "Tab " + tabCounter, id = "tabs-" + tabCounter, li = $(tabTemplate.replace(/#\{href\}/g, "#" + id).replace(/#\{label\}/g, label)), tabContentHtml = tabContent.val() || "Tab " + tabCounter + " content.";
		
				tabs.find(".ui-tabs-nav").append(li);
				tabs.append("<div id='" + id + "'><p>" + tabContentHtml + "</p></div>");
				tabs.tabs("refresh");
				tabCounter++;
		
				// clear fields
				$("#tab_title").val("");
				$("#tab_content").val("");
			}
		
			// addTab button: just opens the dialog
			$("#add_tab").button().click(function() {
				var idd = document.getElementById('num').firstChild.nodeValue;

				
				dialog.dialog("open");
			});
		
			// close icon: removing the tab on click
					
			/*
			* ACCORDION
			*/
			//jquery accordion
			
		     var accordionIcons = {
		         header: "fa fa-plus",    // custom icon class
		         activeHeader: "fa fa-minus" // custom icon class
		     };
		     
			$("#accordion").accordion({
				autoHeight : false,
				heightStyle : "content",
				collapsible : true,
				animate : 300,
				icons: accordionIcons,
				header : "h4",
			})
		
			/*
			 * PROGRESS BAR
			 */
			$("#progressbar").progressbar({
		     	value: 25,
		     	create: function( event, ui ) {
		     		$(this).removeClass("ui-corner-all").addClass('progress').find(">:first-child").removeClass("ui-corner-left").addClass('progress-bar progress-bar-success');
				}
			});			

		})

	</script>
	<script src="js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<script type="text/javascript">
// DO NOT REMOVE : GLOBAL FUNCTIONS!

$(document).ready(function() {
	
	/* // DOM Position key index //
		
	l - Length changing (dropdown)
	f - Filtering input (search)
	t - The Table! (datatable)
	i - Information (records)
	p - Pagination (paging)
	r - pRocessing 
	< and > - div elements
	<"#id" and > - div with an id
	<"class" and > - div with a class
	<"#id.class" and > - div with an id and class
	
	Also see: http://legacy.datatables.net/usage/features
	*/	

	/* BASIC ;*/
		var responsiveHelper_dt_basic = undefined;
		var responsiveHelper_datatable_fixed_column = undefined;
		var responsiveHelper_datatable_col_reorder = undefined;
		var responsiveHelper_datatable_tabletools = undefined;
		
		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};

		$('#dt_basic').dataTable({
			"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
			"autoWidth" : true,
			"preDrawCallback" : function() {
				// Initialize the responsive datatables helper once.
				if (!responsiveHelper_dt_basic) {
					responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
				}
			},
			"rowCallback" : function(nRow) {
				responsiveHelper_dt_basic.createExpandIcon(nRow);
			},
			"drawCallback" : function(oSettings) {
				responsiveHelper_dt_basic.respond();
			}
		});

	/* END BASIC */
	
	/* COLUMN FILTER  */
    var otable = $('#datatable_fixed_column').DataTable({
    	//"bFilter": false,
    	//"bInfo": false,
    	//"bLengthChange": false
    	//"bAutoWidth": false,
    	//"bPaginate": false,
    	//"bStateSave": true // saves sort state using localStorage
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_fixed_column) {
				responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_fixed_column.respond();
		}		
	
    });
    
    // custom toolbar
    $("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');
    	   
    // Apply the filter
    $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {
    	
        otable
            .column( $(this).parent().index()+':visible' )
            .search( this.value )
            .draw();
            
    } );
    /* END COLUMN FILTER */   

	/* COLUMN SHOW - HIDE */
	$('#datatable_col_reorder').dataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_col_reorder) {
				responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_col_reorder.respond();
		}			
	});
	
	/* END COLUMN SHOW - HIDE */

	/* TABLETOOLS */
	$('#datatable_tabletools').dataTable({
		
		// Tabletools options: 
		//   https://datatables.net/extensions/tabletools/button_options
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        "oTableTools": {
        	 "aButtons": [
             "copy",
             "csv",
             "xls",
                {
                    "sExtends": "pdf",
                    "sTitle": "SmartAdmin_PDF",
                    "sPdfMessage": "SmartAdmin PDF Export",
                    "sPdfSize": "letter"
                },
             	{
                	"sExtends": "print",
                	"sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>"
            	}
             ],
            "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
        },
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_tabletools) {
				responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_tabletools.respond();
		}
	});
	
	/* END TABLETOOLS */

})

</script>
<?php 
include("inc/google-analytics.php"); 
}
else
{
    header("Location:index.php");
}
?>
					                       
										