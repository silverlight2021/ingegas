<?php
date_default_timezone_set("America/Bogota");

require ("libraries/conexion.php");
session_start();
if(@$_SESSION['logged']== 'yes')
{ 
	$id_user=$_SESSION['su'];
    $acc = $_SESSION['acc'];
    $letra = isset($_REQUEST['letra']) ? $_REQUEST['letra'] : NULL;
    //$id_orden = isset($_REQUEST['id_orden']) ? $_REQUEST['id_orden'] : NULL;
    $id_verificacion_mensual = isset($_REQUEST['id_verificacion_mensual']) ? $_REQUEST['id_verificacion_mensual'] : NULL;
    $contador_orden = 0;

	require_once("inc/init.php");
	require_once("inc/config.ui.php");
	$page_title = "Verificación Mensual";
	$page_css[] = "your_style.css";
	include("inc/header.php");
	$page_nav[""][""][""][""] = true;
	include("inc/nav.php");
	$fecha = date("Y-m-d");
	
?>
<?php
	if(strlen($id_verificacion_mensual)==0){
		$consulta1 = "SELECT * FROM user WHERE idUser = '$id_user'";
		$resultado1 = mysqli_query($con,$consulta1);

		$linea1 = mysqli_fetch_array($resultado1);

		$nombre = isset($linea1["Name"]) ? $linea1["Name"] : NULL;
		$apellido = isset($linea1["LastName"]) ? $linea1["LastName"] : NULL;

		$responsable = $nombre." ".$apellido;
	}

	if(isset($_POST['g_datos_basicos'])){

		$fecha_verificacion = $_POST["fecha_verificacion"];
		$ciudad = $_POST["ciudad"];
		$identificacion = $_POST['identificacion'];
		$cargo = "Conductor";
		$area = "Transporte";
		$id_usuario = $_POST["id_usuario"];
		$id_vehiculo = $_POST["id_vehiculo"];
		$kilometraje = $_POST["kilometraje"];

		$consulta3 = "INSERT INTO verificacion_mensual (fecha_verificacion,id_vehiculo, kilometraje, id_usuario, cargo, area, ciudad, estado, identificacion) VALUES ('$fecha_verificacion', '$id_vehiculo', '$kilometraje','$id_usuario' ,'$cargo', '$area', $ciudad, 1, '$identificacion')";
		$resultado3 = mysqli_query($con,$consulta3);

		if($resultado3){
			$id_verificacion_mensual = mysqli_insert_id($con);
			?>
			<script type="text/javascript">
				alert("Datos guardados de manera exitosa. Presione aceptar para continuar");
				var id_verificacion_mensual  = '<?php echo $id_verificacion_mensual;?>';
		        window.location = ("verificacion_mensual_vehiculos.php?id_verificacion_mensual="+id_verificacion_mensual);
			</script>
			<?php
		}else{
			$error = mysqli_error($con);
			?>
			<script type="text/javascript">
				var error  = '<?php echo $error;?>';	                
				alert("Se presentó un error: " + error);					
	            window.location = ("verificacion_mensual_vehiculos.php");
			</script>
			<?php
		}
	}

	if(strlen($id_verificacion_mensual)>0){

		$consulta4 = "SELECT * FROM verificacion_mensual WHERE id_verificacion_mensual = '$id_verificacion_mensual'";
		$resultado4 = mysqli_query($con,$consulta4);

		$linea4 = mysqli_fetch_array($resultado4);
		$fecha_verificacion = isset($linea4["fecha_verificacion"]) ? $linea4["fecha_verificacion"] : $fecha;
		$id_vehiculo = isset($linea4["id_vehiculo"]) ? $linea4["id_vehiculo"] : NULL;
		$kilometraje = isset($linea4["kilometraje"]) ? $linea4["kilometraje"] : NULL;
		$id_usuario = isset($linea4["id_usuario"]) ? $linea4["id_usuario"] : NULL;
		$identificacion = isset($linea4["identificacion"]) ? $linea4["identificacion"] : NULL;
		$cargo = isset($linea4["cargo"]) ? $linea4["cargo"] : NULL;
		$area = isset($linea4["area"]) ? $linea4["area"] : NULL;
		$ciudad = isset($linea4["ciudad"]) ? $linea4["ciudad"] : NULL;
		$conforme_recepcion = isset($linea4["conforme_recepcion"]) ? $linea4["conforme_recepcion"] : NULL;
		$observaciones_recepcion = isset($linea4["observaciones_recepcion"]) ? $linea4["observaciones_recepcion"] : NULL;
		$estado = isset($linea4["estado"]) ? $linea4["estado"] : NULL;

		if($ciudad==1){
			$ciudad = "TOBERÍN";
		}else if($ciudad == 2){
			$salida = "CAZUCÁ";
		}else if($ciudad == 3){
			$ciudad = "MANIZALEZ";
		}elseif ($ciudad == 4) {
			$ciudad = "MEDELLIN";
		}


		$consulta5 = "SELECT * FROM user WHERE idUser = '$id_usuario'";
		$resultado5 = mysqli_query($con,$consulta5);

		$linea5 = mysqli_fetch_array($resultado5);

		$nombre = isset($linea5["Name"]) ? $linea5["Name"] : NULL;
		$apellido = isset($linea5["LastName"]) ? $linea5["LastName"] : NULL;

		$responsable = $nombre." ".$apellido;

		$consulta6 = "SELECT * FROM placa_camion WHERE id_placa_camion = '$id_vehiculo'";
		$resultado6 = mysqli_query($con,$consulta6);

		$linea6 = mysqli_fetch_array($resultado6);

		$placa_camion = isset($linea6["placa_camion"]) ? $linea6["placa_camion"] : NULL;

		if($estado>1){
			$consulta9 = "SELECT * FROM verificacion_vehiculo WHERE id_verificacion_mensual = $id_verificacion_mensual";
			$resultado9 = mysqli_query($con,$consulta9);

			$linea9 = mysqli_fetch_array($resultado9);

			$licencia = isset($linea9["licencia"]) ? $linea9["licencia"] : NULL;
			$soat = isset($linea9["soat"]) ? $linea9["soat"] : NULL;
			$rtm = isset($linea9["rtm"]) ? $linea9["rtm"] : NULL;
			$seguro_dano_rc = isset($linea9["seguro_dano_rc"]) ? $linea9["seguro_dano_rc"] : NULL;
			$manual_seguridad = isset($linea9["manual_seguridad"]) ? $linea9["manual_seguridad"] : NULL;
			$tarjetas_emergencia = isset($linea9["tarjetas_emergencia"]) ? $linea9["tarjetas_emergencia"] : NULL;
			$telefonos_emergencia = isset($linea9["telefonos_emergencia"]) ? $linea9["telefonos_emergencia"] : NULL;
			$limpiabrisas = isset($linea9["limpiabrisas"]) ? $linea9["limpiabrisas"] : NULL;
			$llantas_delanteras = isset($linea9["llantas_delanteras"]) ? $linea9["llantas_delanteras"] : NULL;
			$llantas_traseras = isset($linea9["llantas_traseras"]) ? $linea9["llantas_traseras"] : NULL;
			$llantas_repuesto = isset($linea9["llantas_repuesto"]) ? $linea9["llantas_repuesto"] : NULL;
			$plataforma = isset($linea9["plataforma"]) ? $linea9["plataforma"] : NULL;
			$frenos = isset($linea9["frenos"]) ? $linea9["frenos"] : NULL;
			$aceite = isset($linea9["aceite"]) ? $linea9["aceite"] : NULL;
			$refrigerante = isset($linea9["refrigerante"]) ? $linea9["refrigerante"] : NULL;
			$cinturon_seguridad = isset($linea9["cinturon_seguridad"]) ? $linea9["cinturon_seguridad"] : NULL;
			$fecha_tecnicomecanica = isset($linea9["fecha_tecnicomecanica"]) ? $linea9["fecha_tecnicomecanica"] : NULL;
			$fecha_soat = isset($linea9["fecha_soat"]) ? $linea9["fecha_soat"] : NULL;
			$carro_portatermo = isset($linea9["carro_portatermos"]) ? $linea9["carro_portatermos"] : NULL;
			$riatas = isset($linea9["riatas"]) ? $linea9["riatas"] : NULL;
			$direccionales_delanteras = isset($linea9["direccionales_delanteras"]) ? $linea9["direccionales_delanteras"] : NULL;
			$direccionales_traseras = isset($linea9["direccionales_traseras"]) ? $linea9["direccionales_traseras"] : NULL;
			$luces_altas = isset($linea9["luces_altas"]) ? $linea9["luces_altas"] : NULL;
			$luces_bajas = isset($linea9["luces_bajas"]) ? $linea9["luces_bajas"] : NULL;
			$luces_stops = isset($linea9["luces_stops"]) ? $linea9["luces_stops"] : NULL;
			$luces_reversa = isset($linea9["luces_reversa"]) ? $linea9["luces_reversa"] : NULL;
			$luces_parqueo = isset($linea9["luces_parqueo"]) ? $linea9["luces_parqueo"] : NULL;
			$frenos_principal = isset($linea9["frenos_principal"]) ? $linea9["frenos_principal"] : NULL;
			$frenos_emergencia = isset($linea9["frenos_emergencia"]) ? $linea9["frenos_emergencia"] : NULL;
			$laterales_der_izq = isset($linea9["laterales_der_izq"]) ? $linea9["laterales_der_izq"] : NULL;
			$retrovisor = isset($linea9["retrovisor"]) ? $linea9["retrovisor"] : NULL;
			$pito = isset($linea9["pito"]) ? $linea9["pito"] : NULL;
			$apoya_cabezas_delantero = isset($linea9["apoya_cabezas_delantero"]) ? $linea9["apoya_cabezas_delantero"] : NULL;
			$apoya_cabezas_trasero = isset($linea9["apoya_cabezas_trasero"]) ? $linea9["apoya_cabezas_trasero"] : NULL;
			$cambio_aceite = isset($linea9["cambio_aceite"]) ? $linea9["cambio_aceite"] : NULL;
			$sincronizacion = isset($linea9["sincronizacion"]) ? $linea9["sincronizacion"] : NULL;
			$alineacion_balanceo = isset($linea9["alineacion_balanceo"]) ? $linea9["alineacion_balanceo"] : NULL;
			$cambio_llantas = isset($linea9["cambio_llantas"]) ? $linea9["cambio_llantas"] : NULL;
			$carro_portacilindros = isset($linea9["carro_portacilindros"]) ? $linea9["carro_portacilindros"] : NULL;
			$cadenas = isset($linea9["cadenas"]) ? $linea9["cadenas"] : NULL;

			$observaciones_licencia = isset($linea9["observaciones_licencia"]) ? $linea9["observaciones_licencia"] : NULL;
			$observaciones_soat = isset($linea9["observaciones_soat"]) ? $linea9["observaciones_soat"] : NULL;
			$observaciones_rtm = isset($linea9["observaciones_rtm"]) ? $linea9["observaciones_rtm"] : NULL;
			$observaciones_seguro_dano_rc = isset($linea9["observaciones_seguro_dano_rc"]) ? $linea9["observaciones_seguro_dano_rc"] : NULL;
			$observaciones_manual_seguridad = isset($linea9["observaciones_manual_seguridad"]) ? $linea9["observaciones_manual_seguridad"] : NULL;
			$observaciones_tarjetas_emergencia = isset($linea9["observaciones_tarjetas_emergencia"]) ? $linea9["observaciones_tarjetas_emergencia"] : NULL;
			$observaciones_telefonos_emergencia = isset($linea9["observaciones_telefonos_emergencia"]) ? $linea9["observaciones_telefonos_emergencia"] : NULL;
			$observaciones_limpiabrisas = isset($linea9["observaciones_limpiabrisas"]) ? $linea9["observaciones_limpiabrisas"] : NULL;
			$observaciones_llantas_delanteras = isset($linea9["observaciones_llantas_delanteras"]) ? $linea9["observaciones_llantas_delanteras"] : NULL;
			$observaciones_llantas_traseras = isset($linea9["observaciones_llantas_traseras"]) ? $linea9["observaciones_llantas_traseras"] : NULL;
			$observaciones_llantas_repuesto = isset($linea9["observaciones_llantas_repuesto"]) ? $linea9["observaciones_llantas_repuesto"] : NULL;
			$observaciones_plataforma = isset($linea9["observaciones_plataforma"]) ? $linea9["observaciones_plataforma"] : NULL;
			$observaciones_frenos = isset($linea9["observaciones_frenos"]) ? $linea9["observaciones_frenos"] : NULL;
			$observaciones_aceite = isset($linea9["observaciones_aceite"]) ? $linea9["observaciones_aceite"] : NULL;
			$observaciones_refrigerante = isset($linea9["observaciones_refrigerante"]) ? $linea9["observaciones_refrigerante"] : NULL;
			$observaciones_cinturon_seguridad = isset($linea9["observaciones_cinturon_seguridad"]) ? $linea9["observaciones_cinturon_seguridad"] : NULL;
			$observaciones_fecha_tecnicomecanica = isset($linea9["observaciones_fecha_tecnicomecanica"]) ? $linea9["observaciones_fecha_tecnicomecanica"] : NULL;
			$observaciones_fecha_soat = isset($linea9["observaciones_fecha_soat"]) ? $linea9["observaciones_fecha_soat"] : NULL;
			$observaciones_carro_portatermo = isset($linea9["observaciones_carro_portatermo"]) ? $linea9["observaciones_carro_portatermo"] : NULL;
			$observaciones_cadenas = isset($linea9["observaciones_cadenas"]) ? $linea9["observaciones_cadenas"] : NULL;
			$observaciones_riatas = isset($linea9["observaciones_riatas"]) ? $linea9["observaciones_riatas"] : NULL;
			$observaciones_direccionales_delanteras = isset($linea9["observaciones_direccionales_delanteras"]) ? $linea9["observaciones_direccionales_delanteras"] : NULL;
			$observaciones_direccionales_traseras = isset($linea9["observaciones_direccionales_traseras"]) ? $linea9["observaciones_direccionales_traseras"] : NULL;
			$observaciones_luces_altas = isset($linea9["observaciones_luces_altas"]) ? $linea9["observaciones_luces_altas"] : NULL;
			$observaciones_luces_bajas = isset($linea9["observaciones_luces_bajas"]) ? $linea9["observaciones_luces_bajas"] : NULL;
			$observaciones_luces_stops = isset($linea9["observaciones_luces_stops"]) ? $linea9["observaciones_luces_stops"] : NULL;
			$observaciones_luces_reversa = isset($linea9["observaciones_luces_reversa"]) ? $linea9["observaciones_luces_reversa"] : NULL;
			$observaciones_luces_parqueo = isset($linea9["observaciones_luces_parqueo"]) ? $linea9["observaciones_luces_parqueo"] : NULL;
			$observaciones_frenos_principal = isset($linea9["observaciones_frenos_principal"]) ? $linea9["observaciones_frenos_principal"] : NULL;
			$observaciones_frenos_emergencia = isset($linea9["observaciones_frenos_emergencia"]) ? $linea9["observaciones_frenos_emergencia"] : NULL;
			$observaciones_laterales_der_izq = isset($linea9["observaciones_laterales_der_izq"]) ? $linea9["observaciones_laterales_der_izq"] : NULL;
			$observaciones_retrovisor = isset($linea9["observaciones_retrovisor"]) ? $linea9["observaciones_retrovisor"] : NULL;
			$observaciones_pito = isset($linea9["observaciones_pito"]) ? $linea9["observaciones_pito"] : NULL;
			$observaciones_carro_portacilindros = isset($linea9["observaciones_carro_portacilindros"]) ? $linea9["observaciones_carro_portacilindros"] : NULL;
			$observaciones_apoya_cabezas_delantero = isset($linea9["observaciones_apoya_cabezas_delantero"]) ? $linea9["observaciones_apoya_cabezas_delantero"] : NULL;
			$observaciones_apoya_cabezas_trasero = isset($linea9["observaciones_apoya_cabezas_trasero"]) ? $linea9["observaciones_apoya_cabezas_trasero"] : NULL;


			if($estado>2){
				$consulta12 = "SELECT * FROM verificacion_elementos_emergencia WHERE id_verificacion_mensual = $id_verificacion_mensual";
				$resultado12 = mysqli_query($con,$consulta12);

				$linea12 = mysqli_fetch_array($resultado12);

				$vencimiento_extintor = isset($linea12["vencimiento_extintor"]) ? $linea12["vencimiento_extintor"] : NULL;
				$capacidad_extintor = isset($linea12["capacidad_extintor"]) ? $linea12["capacidad_extintor"] : NULL;
				$conforme_vencimiento_extintor = isset($linea12["conforme_vencimiento_extintor"]) ? $linea12["conforme_vencimiento_extintor"] : NULL;
				$conforme_capacidad_extintor = isset($linea12["conforme_capacidad_extintor"]) ? $linea12["conforme_capacidad_extintor"] : NULL;
				$alicates_destornilladores = isset($linea12["alicates_destornilladores"]) ? $linea12["alicates_destornilladores"] : NULL;
				$llave_fija = isset($linea12["llave_fija"]) ? $linea12["llave_fija"] : NULL;
				$cruceta = isset($linea12["cruceta"]) ? $linea12["cruceta"] : NULL;
				$gato = isset($linea12["gato"]) ? $linea12["gato"] : NULL;
				$conos = isset($linea12["conos"]) ? $linea12["conos"] : NULL;
				$tacos = isset($linea12["tacos"]) ? $linea12["tacos"] : NULL;
				$senales = isset($linea12["senales"]) ? $linea12["senales"] : NULL;
				$chaleco = isset($linea12["chaleco"]) ? $linea12["chaleco"] : NULL;
				$botiquin = isset($linea12["botiquin"]) ? $linea12["botiquin"] : NULL;		

				$observaciones_vencimiento_extintor = isset($linea12["observaciones_vencimiento_extintor"]) ? $linea12["observaciones_vencimiento_extintor"] : NULL;
				$observaciones_capacidad_extintor = isset($linea12["observaciones_capacidad_extintor"]) ? $linea12["observaciones_capacidad_extintor"] : NULL;
				$observaciones_alicates_destornilladores = isset($linea12["observaciones_alicates_destornilladores"]) ? $linea12["observaciones_alicates_destornilladores"] : NULL;
				$observaciones_llave_fija = isset($linea12["observaciones_llave_fija"]) ? $linea12["observaciones_llave_fija"] : NULL;
				$observaciones_cruceta = isset($linea12["observaciones_cruceta"]) ? $linea12["observaciones_cruceta"] : NULL;
				$observaciones_gato = isset($linea12["observaciones_gato"]) ? $linea12["observaciones_gato"] : NULL;
				$observaciones_conos = isset($linea12["observaciones_conos"]) ? $linea12["observaciones_conos"] : NULL;
				$observaciones_tacos = isset($linea12["observaciones_tacos"]) ? $linea12["observaciones_tacos"] : NULL;
				$observaciones_senales = isset($linea12["observaciones_senales"]) ? $linea12["observaciones_senales"] : NULL;
				$observaciones_chaleco = isset($linea12["observaciones_chaleco"]) ? $linea12["observaciones_chaleco"] : NULL;
				$observaciones_botiquin = isset($linea12["observaciones_botiquin"]) ? $linea12["observaciones_botiquin"] : NULL;
			}

		}
	}

	if(isset($_POST['g_elementos_vehiculo'])){

		$id_verificacion_mensual = $_POST["id_verificacion_mensual"];
		$licencia = $_POST["licencia"];
		$soat = $_POST["soat"];
		$rtm = $_POST["rtm"];
		$seguro_dano_rc = $_POST["seguro_dano_rc"];
		$manual_seguridad = $_POST["manual_seguridad"];
		$tarjetas_emergencia = $_POST["tarjetas_emergencia"];
		$telefonos_emergencia = $_POST["telefonos_emergencia"];

		$limpiabrisas = $_POST["limpiabrisas"];

		$llantas_delanteras = $_POST["llantas_delanteras"];
		$llantas_traseras = $_POST["llantas_traseras"];
		$llantas_repuesto = $_POST["llantas_repuesto"];

		$plataforma = $_POST["plataforma"];
		
		$frenos = $_POST["frenos"];
		$aceite = $_POST["aceite"];
		$refrigerante = $_POST["refrigerante"];

		$cinturon_seguridad = $_POST["cinturon_seguridad"];

		$fecha_tecnicomecanica = $_POST["fecha_tecnicomecanica"];
		$fecha_soat = $_POST["fecha_soat"];

		$carro_portatermo = $_POST["carro_portatermo"];

		$riatas = $_POST["riatas"];
		
		$direccionales_delanteras = $_POST["direccionales_delanteras"];
		$direccionales_traseras = $_POST["direccionales_traseras"];
		
		$luces_altas = $_POST["luces_altas"];
		$luces_bajas = $_POST["luces_bajas"];
		$luces_stops = $_POST["luces_stops"];
		$luces_reversa = $_POST["luces_reversa"];
		$luces_parqueo = $_POST["luces_parqueo"];

		$frenos_principal = $_POST["frenos_principal"];
		$frenos_emergencia = $_POST["frenos_emergencia"];
		
		$laterales_der_izq = $_POST["laterales_der_izq"];
		$retrovisor = $_POST["retrovisor"];
		
		$pito = $_POST["pito"];
		
		$apoya_cabezas_delantero = $_POST["apoya_cabezas_delantero"];
		$apoya_cabezas_trasero = $_POST["apoya_cabezas_trasero"];
		
		$cambio_aceite = "No Aplica";
		$sincronizacion = "No Aplica";
		$alineacion_balanceo = "No Aplica";
		$cambio_llantas = "No Aplica";

		$carro_portacilindros = $_POST["carro_portacilindros"];
		$cadenas = $_POST["cadenas"];

		//llllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllll

		$observaciones_licencia = $_POST["observaciones_licencia"];
		$observaciones_soat = $_POST["observaciones_soat"];
		$observaciones_rtm = $_POST["observaciones_rtm"];
		$observaciones_seguro_dano_rc = $_POST["observaciones_seguro_dano_rc"];
		$observaciones_manual_seguridad = $_POST["observaciones_manual_seguridad"];
		$observaciones_tarjetas_emergencia = $_POST["observaciones_tarjetas_emergencia"];
		$observaciones_telefonos_emergencia = $_POST["observaciones_telefonos_emergencia"];

		$observaciones_limpiabrisas = $_POST["observaciones_limpiabrisas"];

		$observaciones_llantas_delanteras = $_POST["observaciones_llantas_delanteras"];
		$observaciones_llantas_traseras = $_POST["observaciones_llantas_traseras"];
		$observaciones_llantas_repuesto = $_POST["observaciones_llantas_repuesto"];

		$observaciones_plataforma = $_POST["observaciones_plataforma"];

		$observaciones_frenos = $_POST["observaciones_frenos"];
		$observaciones_aceite = $_POST["observaciones_aceite"];
		$observaciones_refrigerante = $_POST["observaciones_refrigerante"];

		$observaciones_cinturon_seguridad = $_POST["observaciones_cinturon_seguridad"];

		$observaciones_fecha_tecnicomecanica = $_POST["observaciones_fecha_tecnicomecanica"];
		$observaciones_fecha_soat = $_POST["observaciones_fecha_soat"];

		$observaciones_carro_portatermo = $_POST["observaciones_carro_portatermo"];

		$observaciones_cadenas = $_POST["observaciones_cadenas"];

		$observaciones_riatas = $_POST["observaciones_riatas"];

		$observaciones_direccionales_delanteras = $_POST["observaciones_direccionales_delanteras"];
		$observaciones_direccionales_traseras = $_POST["observaciones_direccionales_traseras"];

		$observaciones_luces_altas = $_POST["observaciones_luces_altas"];
		$observaciones_luces_bajas = $_POST["observaciones_luces_bajas"];
		$observaciones_luces_stops = $_POST["observaciones_luces_stops"];
		$observaciones_luces_reversa = $_POST["observaciones_luces_reversa"];
		$observaciones_luces_parqueo = $_POST["observaciones_luces_parqueo"];

		$observaciones_frenos_principal = $_POST["observaciones_frenos_principal"];
		$observaciones_frenos_emergencia = $_POST["observaciones_frenos_emergencia"];
		
		$observaciones_laterales_der_izq = $_POST["observaciones_laterales_der_izq"];
		$observaciones_retrovisor = $_POST["observaciones_retrovisor"];
		
		$observaciones_pito = $_POST["observaciones_pito"];

		$observaciones_carro_portacilindros = $_POST["observaciones_carro_portacilindros"];
		$observaciones_cadenas = $_POST["observaciones_cadenas"];

		$observaciones_apoya_cabezas_delantero = $_POST["observaciones_apoya_cabezas_delantero"];
		$observaciones_apoya_cabezas_trasero = $_POST["observaciones_apoya_cabezas_trasero"];

		$consulta7 = "INSERT INTO verificacion_vehiculo (id_verificacion_mensual,
														licencia,
														soat,
														rtm,
														seguro_dano_rc,
														manual_seguridad,
														tarjetas_emergencia,
														telefonos_emergencia,
														limpiabrisas,
														llantas_delanteras,
														llantas_traseras,
														llantas_repuesto,
														plataforma,
														frenos,
														aceite,
														refrigerante,
														cinturon_seguridad,
														fecha_tecnicomecanica,
														fecha_soat,
														carro_portatermos,
														riatas,
														direccionales_delanteras,
														direccionales_traseras,
														luces_altas,
														luces_bajas,
														luces_stops,
														luces_reversa,
														luces_parqueo,
														frenos_principal,
														frenos_emergencia,
														laterales_der_izq,
														retrovisor,
														pito,
														apoya_cabezas_delantero,
														apoya_cabezas_trasero,
														cambio_aceite,
														sincronizacion,
														alineacion_balanceo,
														cambio_llantas,
														carro_portacilindros,
														cadenas, 
														observaciones_licencia,
														observaciones_soat,
														observaciones_rtm,
														observaciones_seguro_dano_rc,
														observaciones_manual_seguridad,
														observaciones_tarjetas_emergencia,
														observaciones_telefonos_emergencia,
														observaciones_limpiabrisas,
														observaciones_llantas_delanteras,
														observaciones_llantas_traseras,
														observaciones_llantas_repuesto,
														observaciones_plataforma,
														observaciones_frenos,
														observaciones_aceite,
														observaciones_refrigerante,
														observaciones_cinturon_seguridad,
														observaciones_fecha_tecnicomecanica,
														observaciones_fecha_soat,
														observaciones_carro_portatermo,
														observaciones_riatas,
														observaciones_direccionales_delanteras,
														observaciones_direccionales_traseras,
														observaciones_luces_altas,
														observaciones_luces_bajas,
														observaciones_luces_stops,
														observaciones_luces_reversa,
														observaciones_luces_parqueo,
														observaciones_frenos_principal,
														observaciones_frenos_emergencia,
														observaciones_laterales_der_izq,
														observaciones_retrovisor,
														observaciones_pito,
														observaciones_carro_portacilindros,
														observaciones_cadenas,
														observaciones_apoya_cabezas_delantero,
														observaciones_apoya_cabezas_trasero) 
														VALUES 
														('$id_verificacion_mensual',
														$licencia,
														$soat,
														$rtm,
														$seguro_dano_rc, 
														$manual_seguridad,
														$tarjetas_emergencia,
														$telefonos_emergencia,
														$limpiabrisas,
														$llantas_delanteras,
														$llantas_traseras,
														$llantas_repuesto,
														$plataforma,
														$frenos,
														$aceite,
														$refrigerante,
														$cinturon_seguridad,
														'$fecha_tecnicomecanica',
														'$fecha_soat',
														$carro_portatermo,
														$riatas,
														$direccionales_delanteras,
														$direccionales_traseras,
														$luces_altas,
														$luces_bajas,
														$luces_stops,
														$luces_reversa,
														$luces_parqueo,
														$frenos_principal,
														$frenos_emergencia,
														$laterales_der_izq,
														$retrovisor,
														$pito,
														$apoya_cabezas_delantero,
														$apoya_cabezas_trasero,
														'No Aplica',
														'$sincronizacion',
														'$alineacion_balanceo',
														'$cambio_llantas',
														$carro_portacilindros,
														$cadenas,
														'$observaciones_licencia',
														'$observaciones_soat',
														'$observaciones_rtm',
														'$observaciones_seguro_dano_rc',
														'$observaciones_manual_seguridad',
														'$observaciones_tarjetas_emergencia',
														'$observaciones_telefonos_emergencia',
														'$observaciones_limpiabrisas',
														'$observaciones_llantas_delanteras',
														'$observaciones_llantas_traseras',
														'$observaciones_llantas_repuesto',
														'$observaciones_plataforma',
														'$observaciones_frenos',
														'$observaciones_aceite',
														'$observaciones_refrigerante',
														'$observaciones_cinturon_seguridad',
														'$observaciones_fecha_tecnicomecanica',
														'$observaciones_fecha_soat',
														'$observaciones_carro_portatermo',
														'$observaciones_riatas',
														'$observaciones_direccionales_delanteras',
														'$observaciones_direccionales_traseras',
														'$observaciones_luces_altas',
														'$observaciones_luces_bajas',
														'$observaciones_luces_stops',
														'$observaciones_luces_reversa',
														'$observaciones_luces_parqueo',
														'$observaciones_frenos_principal',
														'$observaciones_frenos_emergencia',
														'$observaciones_laterales_der_izq',
														'$observaciones_retrovisor',
														'$observaciones_pito',
														'$observaciones_carro_portacilindros',
														'$observaciones_cadenas',
														'$observaciones_apoya_cabezas_delantero',
														'$observaciones_apoya_cabezas_trasero')";

		$resultado7 = mysqli_query($con,$consulta7);

		if($resultado7){
			$consulta8 = "UPDATE verificacion_mensual SET estado = 2 WHERE id_verificacion_mensual = $id_verificacion_mensual";
			$resultado8 = mysqli_query($con,$consulta8);

			if($resultado8){
				?>
				<script type="text/javascript">
					var id_verificacion_mensual  = '<?php echo $id_verificacion_mensual;?>';
					alert("Guardado con Exito!");
					window.location = ("verificacion_mensual_vehiculos.php?id_verificacion_mensual="+id_verificacion_mensual);
				</script>
				<?php
			}
		}else{
			echo $consulta7;
		}
	}

	if(isset($_POST["g_elementos_emergencia"])){
		$id_verificacion_mensual = $_POST["id_verificacion_mensual"];
		$vencimiento_extintor = $_POST["vencimiento_extintor"];
		$capacidad_extintor = $_POST["capacidad_extintor"];
		$conforme_vencimiento_extintor = $_POST["conforme_vencimiento_extintor"];
		$conforme_capacidad_extintor = $_POST["conforme_capacidad_extintor"];
		$alicates_destornilladores = $_POST["alicates_destornilladores"];
		$llave_fija = $_POST["llave_fija"];
		$cruceta = $_POST["cruceta"];
		$gato = $_POST["gato"];
		$conos = $_POST["conos"];
		$tacos = $_POST["tacos"];
		$senales = $_POST["senales"];
		$chaleco = $_POST["chaleco"]; 
		$botiquin = $_POST["botiquin"];

		$observaciones_vencimiento_extintor = $_POST["observaciones_vencimiento_extintor"];
		$observaciones_capacidad_extintor = $_POST["observaciones_capacidad_extintor"];
		$observaciones_alicates_destornilladores = $_POST["observaciones_alicates_destornilladores"];
		$observaciones_llave_fija = $_POST["observaciones_llave_fija"];
		$observaciones_cruceta = $_POST["observaciones_cruceta"];
		$observaciones_gato = $_POST["observaciones_gato"];
		$observaciones_conos = $_POST["observaciones_conos"];
		$observaciones_tacos = $_POST["observaciones_tacos"];
		$observaciones_senales = $_POST["observaciones_senales"];
		$observaciones_chaleco = $_POST["observaciones_chaleco"]; 
		$observaciones_botiquin = $_POST["observaciones_botiquin"];

		$consulta10 = "INSERT INTO verificacion_elementos_emergencia(id_verificacion_mensual,vencimiento_extintor,capacidad_extintor,conforme_vencimiento_extintor,conforme_capacidad_extintor,alicates_destornilladores,llave_fija,cruceta,gato,conos,tacos,senales,chaleco,botiquin,observaciones_vencimiento_extintor, observaciones_capacidad_extintor, observaciones_alicates_destornilladores, observaciones_llave_fija, observaciones_cruceta, observaciones_gato, observaciones_conos, observaciones_tacos, observaciones_senales, observaciones_chaleco, observaciones_botiquin) VALUES ($id_verificacion_mensual,'$vencimiento_extintor','$capacidad_extintor',$conforme_vencimiento_extintor,$conforme_capacidad_extintor,$alicates_destornilladores,$llave_fija,$cruceta,$gato,$conos,$tacos,$senales,$chaleco,$botiquin,'$observaciones_vencimiento_extintor', '$observaciones_capacidad_extintor', '$observaciones_alicates_destornilladores', '$observaciones_llave_fija', '$observaciones_cruceta', '$observaciones_gato', '$observaciones_conos', '$observaciones_tacos', '$observaciones_senales', '$observaciones_chaleco', '$observaciones_botiquin')";
		$resultado10 = mysqli_query($con,$consulta10);

		if($resultado10){
			$consulta11 = "UPDATE verificacion_mensual SET estado = 3 WHERE id_verificacion_mensual = $id_verificacion_mensual";
			$resultado11 = mysqli_query($con,$consulta11);
			
			if($resultado11){
				?>
				<script type="text/javascript">
					var id_verificacion_mensual  = '<?php echo $id_verificacion_mensual;?>';
					alert("Finalizado con Exito!");
					//window.location = ("verificacion_mensual_vehiculos.php?id_verificacion_mensual="+id_verificacion_mensual);
				</script>
				<?php
			}
		}else{
			echo $consulta10;
		}
	}

	if(isset($_POST["g_recepcion"])){
		$conforme_recepcion = $_POST["conforme_recepcion"];
		$observaciones_recepcion = $_POST["observaciones_recepcion"];
		$id_verificacion_mensual = $_POST["id_verificacion_mensual"];

		$consulta13 = "UPDATE verificacion_mensual SET conforme_recepcion = $conforme_recepcion, observaciones_recepcion = '$observaciones_recepcion', estado = 4 WHERE id_verificacion_mensual = $id_verificacion_mensual";
		$resultado13 = mysqli_query($con,$consulta13);

		if($resultado13){
			?>
				<script type="text/javascript">
					var id_verificacion_mensual  = '<?php echo $id_verificacion_mensual;?>';
					alert("Verificación Terminada con Exito!");
					window.location = ("verificacion_mensual_vehiculos.php?id_verificacion_mensual="+id_verificacion_mensual);
				</script>
			<?php
		}else{
			echo $consulta13;
		}
	}
?>

<!-- MAIN PANEL -->
<div id="main" role="main">
	<div id="content">
		<div class="" align="center">
			<h6  class="page-title txt-color-blueDark">LISTA DE CHEQUEO DEL VEHICULO ANTES DE LA MARCHA</h6>
			<h7>Elaborada bajo referencia Anexo II de la Resolución 1565 del 6 de junio de 2014, por la cual se expide la Guía metodológica para la elaboración del Plan Estratégico de Seguridad Vial, expedida por el Ministerio de Transporte.</h7>			
		</div>
		<section id="widget-grid" class="">
			<form method="POST" action="verificacion_mensual_vehiculos.php" action="verificacion_mensual_vehiculos.php">
				<div class="row">
					<article class="col-sm-6 col-md-6 col-lg-6">
						<table  class="table table-striped table-bordered table-hover" width="100%">	
							<tbody>
								<tr>
									<input type="hidden" name="fecha_verificacion" value="<?php echo $fecha; ?>">
									<input type="hidden" name="id_usuario" value="<?php echo $id_user; ?>">
									<td width="50%"><strong>Fecha de Realización:</strong></td>
									<td align="center"><?php echo $fecha; ?></td>
								</tr>
								<tr>
									<td width="50%"><strong>Ciudad:</strong></td>
									<td align="center">
										<?php
										if(strlen($id_verificacion_mensual)>0){
											
											echo $ciudad;
											
										}else{
											?>
											<select id="ciudad" name="ciudad">
												<option value="0">Seleccione...</option>
												<option value="1">Toberín</option>
												<option value="2">Cazucá</option>
												<option value="3">Manizales</option>
												<option value="4">Medellin</option>
											</select>
											<?php
										}
										?>
									</td>
								</tr>
								<tr>
									<td width="50%"><strong>Responsable:</strong></td>
									<td align="center"><?php echo $responsable; ?></td>
								</tr>
								<tr>
									<td width="50%"><strong>Identificación:</strong></td>
									<td align="center">
										<?php
										if(strlen($id_verificacion_mensual)>0){
											
											echo $identificacion;
											
										}else{
											?>
											<input type="text" name="identificacion" placeholder="Numero de Identificación" />
											<?php
										}
										?>
									</td>
								</tr>
							</tbody>
						</table>
					</article>

					<article class="col-sm-6 col-md-6 col-lg-6">					
						<table  class="table table-striped table-bordered table-hover" width="100%">	
							<tbody>
								<tr>
									<td width="50%"><strong>Cargo</strong></td>
									<td align="center">
										<?php
										if(strlen($id_verificacion_mensual)>0){
											
											echo $cargo;
											
										}else{
											echo "Conductor";
										}
										?>
									</td>
								</tr>
								<tr>
									<td width="50%"><strong>Area</strong></td>
									<td align="center">
										<?php
										if(strlen($id_verificacion_mensual)>0){
											
											echo $area;
											
										}else{
											echo "Transporte";
										}
										?>
									</td>
								</tr>
								<tr>
									<td width="50%"><strong>Placa:</strong></td>
									<td align="center">
										<?php
										if(strlen($id_verificacion_mensual)>0){
											
											echo $placa_camion;
											
										}else{
											?>
											<select name="id_vehiculo">
												<option value="0">Seleccione...</option>
												<?php
												$consulta2 = "SELECT * FROM placa_camion";
												$resultado2 = mysqli_query($con,$consulta2); 
												while ($linea2 = mysqli_fetch_array($resultado2)) {
													$id_placa_camion = isset($linea2["id_placa_camion"]) ? $linea2["id_placa_camion"] : NULL;
													$placa_camion = isset($linea2["placa_camion"]) ? $linea2["placa_camion"] : NULL;
													if($id_placa_camion != 6){
														echo "<option value='$id_placa_camion'>$placa_camion</option>";
													}
													
												}
												?>
											</select>
											<?php
										}
										?>
									</td>
								</tr>
								<tr>
									<td width="50%"><strong>Kilometraje:</strong></td>
									<td align="center">
										<?php
										if(strlen($id_verificacion_mensual)>0){
											
											echo $kilometraje;
											
										}else{
											?>
											<input type="text" name="kilometraje" placeholder="Kilometraje" />
											<?php
										}
										?>
									</td>
								</tr>
							</tbody>
						</table>
					</article>
				</div>
				<?php
				if(strlen($id_verificacion_mensual)==0){
					?>
					<div class="row">
						<article class="col-sm-6 col-md-6 col-lg-6">
							<table class="table table-striped table-bordered table-hover" width="100%">
								<tbody>
									<tr>
										<td colspan="2" width="100%" align="center">
											<input type="submit" class="btn btn-primary" name="g_datos_basicos" value="GUARDAR">
										</td>
									</tr>
								</tbody>
							</table>
						</article>
					</div>
					<?php
				}
				?>
				
			</form>
		</section>

		<?php
		if(strlen($id_verificacion_mensual)>0){
		?>
			<section id="widget-grid" class="">
				<form name="critetios_vehiculo" id="critetios_vehiculo" method="POST" action="verificacion_mensual_vehiculos.php">
					<div class="row">
						<article class="col-sm-6 col-md-6 col-lg-6">
							<table  class="table table-striped table-bordered table-hover" width="100%">	
								<tbody>
									<tr>
										<input type="hidden" name="id_verificacion_mensual" value="<?php echo $id_verificacion_mensual; ?>">
										<td width="45%"><strong>Documentos</strong></td>
										<td align="center" width="10%"><strong>Conforme</strong></td>
										<td align="center" width="10%"><strong>No Conforme</strong></td>
										<td align="center" width="35%"><strong>Observaciones</strong></td>
									</tr>
									<tr>
										<td>Licencia</td>
										<td align="center">
											<?php
											if($estado>1){
												if($licencia==1){
													?>
													X
													<?php
												}
											}else{
												?>
												<input type="radio" name="licencia" id="licencia1" value="1">
												<?php
											}
											?>
										</td>
										<td align="center">
											<?php
											if($estado>1){
												if($licencia==2){
													?>
													X
													<?php
												}
											}else{
												?>
												<input type="radio" name="licencia" id="licencia2" value="2">
												<?php
											}
											?>
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_licencia;
											}else{
												?>
												<input type="text" name="observaciones_licencia" id="observaciones_licencia" value="">
												<?php
											}
											?>
										</td>
									</tr>
									<tr>
										<td>Soat</td>
										<td align="center">
											<?php
											if($estado>1){
												if($soat==1){
													?>
													X
													<?php
												}
											}else{
												?>
												<input type="radio" name="soat" id="soat1" value="1">
												<?php
											}
											?>
										</td>
										<td align="center">
											<?php
											if($estado>1){
												if($soat==2){
													?>
													X
													<?php
												}
											}else{
												?>
												<input type="radio" name="soat" id="soat2" value="2">
												<?php
											}
											?>
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_soat;
											}else{
												?>
												<input type="text" name="observaciones_soat" id="observaciones_soat" value="">
												<?php
											}
											?>
										</td>
									</tr>
									<tr>
										<td>RTM</td>
										<td align="center">
											<?php
											if($estado>1){
												if($rtm==1){echo "X";}
											}else{
												?>
												<input type="radio" name="rtm" id="rtm1" value="1">
												<?php
											}
											?>
										</td>
										<td align="center">
											<?php
											if($estado>1){
												if($rtm==2){echo "X";}
											}else{
												?>
												<input type="radio" name="rtm" id="rtm2" value="2">
												<?php
											}
											?>
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_rtm;
											}else{
												?>
												<input type="text" name="observaciones_rtm" id="observaciones_rtm" value="">
												<?php
											}
											?>
										</td>
									</tr>
									<tr>
										<td>Seguro de Daños y RC</td>
										<td align="center">
											<?php
											if($estado>1){
												if($seguro_dano_rc==1){echo "X";}
											}else{
												?>
												<input type="radio" name="seguro_dano_rc" id="seguro_dano_rc1" value="1">
												<?php
											}
											?>
										</td>
										<td align="center">
											<?php
											if($estado>1){
												if($seguro_dano_rc==2){echo "X";}
											}else{
												?>
												<input type="radio" name="seguro_dano_rc" id="seguro_dano_rc2" value="2">
												<?php
											}
											?>
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_seguro_dano_rc;
											}else{
												?>
												<input type="text" name="observaciones_seguro_dano_rc" id="observaciones_seguro_dano_rc" value="">
												<?php
											}
											?>
										</td>
									</tr>
									<tr>
										<td>Manual de Seguridad de Transporte de Cilindros</td>
										<td align="center">
											<?php
											if($estado>1){
												if($manual_seguridad==1){echo "X";}
											}else{
												?>
												<input type="radio" name="manual_seguridad" id="manual_seguridad1" value="1">
												<?php
											}
											?>
										</td>
										<td align="center">
											<?php
											if($estado>1){
												if($manual_seguridad==2){echo "X";}
											}else{
												?>
												<input type="radio" name="manual_seguridad" id="manual_seguridad2" value="2">
												<?php
											}
											?>
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_manual_seguridad;
											}else{
												?>
												<input type="text" name="observaciones_manual_seguridad" id="observaciones_manual_seguridad" value="">
												<?php
											}
											?>
										</td>
									</tr>
									<tr>
										<td>Tarjetas de Emergencia de los Gases Transportados</td>
										<td align="center">
											<?php
											if($estado>1){
												if($tarjetas_emergencia==1){echo "X";}
											}else{
												?>
												<input type="radio" name="tarjetas_emergencia" id="tarjetas_emergencia1" value="1">
												<?php
											}
											?>
										</td>
										<td align="center">
											<?php
											if($estado>1){
												if($tarjetas_emergencia==2){echo "X";}
											}else{
												?>
												<input type="radio" name="tarjetas_emergencia" id="tarjetas_emergencia2" value="2">
												<?php
											}
											?>
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_tarjetas_emergencia;
											}else{
												?>
												<input type="text" name="observaciones_tarjetas_emergencia" id="observaciones_tarjetas_emergencia" value="">
												<?php
											}
											?>
										</td>
									</tr>
									<tr>
										<td>Telefonos de Emergencia</td>
										<td align="center">
											<?php
											if($estado>1){
												if($telefonos_emergencia==1){echo "X";}
											}else{
												?>
												<input type="radio" name="telefonos_emergencia" id="telefonos_emergencia1" value="1">
												<?php
											}
											?>
										</td>
										<td align="center">
											<?php
											if($estado>1){
												if($telefonos_emergencia==2){echo "X";}
											}else{
												?>
												<input type="radio" name="telefonos_emergencia" id="telefonos_emergencia2" value="2">
												<?php
											}
											?>
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_telefonos_emergencia;
											}else{
												?>
												<input type="text" name="observaciones_telefonos_emergencia" id="observaciones_telefonos_emergencia" value="">
												<?php
											}
											?>
										</td>
									</tr>
								</tbody>
							</table>
							<table  class="table table-striped table-bordered table-hover" width="100%">	
								<tbody>
									<tr>
										<td width="42%"><strong>Limpiabrisas</strong></td>
										<td align="center" width="10%"><strong>Conforme</strong></td>
										<td align="center" width="10%"><strong>No Conforme</strong></td>
										<td align="center" width="35%"><strong>Observaciones</strong></td>
									</tr>
									<tr>
										<td>Der/Izq/Atrás</td>
										<td align="center">
											<?php
											if($estado>1){
												if($limpiabrisas==1){echo "X";}
											}else{
												?>
												<input type="radio" name="limpiabrisas" id="limpiabrisas1" value="1">
												<?php
											}
											?>
										</td>
										<td align="center">
											<?php
											if($estado>1){
												if($limpiabrisas==2){echo "X";}
											}else{
												?>
												<input type="radio" name="limpiabrisas" id="limpiabrisas2" value="2">
												<?php
											}
											?>
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_limpiabrisas;
											}else{
												?>
												<input type="text" name="observaciones_limpiabrisas" id="observaciones_limpiabrisas" value="">
												<?php
											}
											?>
										</td>
									</tr>
								</tbody>
							</table>
							<table  class="table table-striped table-bordered table-hover" width="100%">	
								<tbody>
									<tr>
										<td width="45%"><strong>Llantas</strong></td>
										<td align="center" width="10%"><strong>Conforme</strong></td>
										<td align="center" width="10%"><strong>No Conforme</strong></td>
										<td align="center" width="35%"><strong>Observaciones</strong></td>
									</tr>
									<tr>
										<td>Delanteras</td>
										<td align="center">
											<?php
											if($estado>1){
												if($llantas_delanteras==1){echo "X";}
											}else{
												?>
												<input type="radio" name="llantas_delanteras" id="llantas_delanteras1" value="1">
												<?php
											}
											?>
										</td>
										<td align="center">
											<?php
											if($estado>1){
												if($llantas_delanteras==2){echo "X";}
											}else{
												?>
												<input type="radio" name="llantas_delanteras" id="llantas_delanteras2" value="2">
												<?php
											}
											?>
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_llantas_delanteras;
											}else{
												?>
												<input type="text" name="observaciones_llantas_delanteras" id="observaciones_llantas_delanteras" value="">
												<?php
											}
											?>
										</td>
									</tr>
									<tr>
										<td>Traseras</td>
										<td align="center">
											<?php
											if($estado>1){
												if($llantas_traseras==1){echo "X";}
											}else{
												?>
												<input type="radio" name="llantas_traseras" id="llantas_traseras1" value="1">
												<?php
											}
											?>
										</td>
										<td align="center">
											<?php
											if($estado>1){
												if($llantas_traseras==2){echo "X";}
											}else{
												?>
												<input type="radio" name="llantas_traseras" id="llantas_traseras2" value="2">
												<?php
											}
											?>
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_llantas_traseras;
											}else{
												?>
												<input type="text" name="observaciones_llantas_traseras" id="observaciones_llantas_traseras" value="">
												<?php
											}
											?>
										</td>
									</tr>
									<tr>
										<td>Repuesto</td>
										<td align="center">
											<?php
											if($estado>1){
												if($llantas_repuesto==1){echo "X";}
											}else{
												?>
												<input type="radio" name="llantas_repuesto" id="llantas_repuesto1" value="1">
												<?php
											}
											?>
										</td>
										<td align="center">
											<?php
											if($estado>1){
												if($llantas_repuesto==2){echo "X";}
											}else{
												?>
												<input type="radio" name="llantas_repuesto" id="llantas_repuesto2" value="2">
												<?php
											}
											?>
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_llantas_repuesto;
											}else{
												?>
												<input type="text" name="observaciones_llantas_repuesto" id="observaciones_llantas_repuesto" value="">
												<?php
											}
											?>
										</td>
									</tr>
								</tbody>
							</table>
							<table  class="table table-striped table-bordered table-hover" width="100%">	
								<tbody>
									<tr>
										<td width="45%"><strong>Plataforma</strong></td>
										<td align="center" width="10%"><strong>Conforme</strong></td>
										<td align="center" width="10%"><strong>No Conforme</strong></td>
										<td align="center" width="35%"><strong>Observaciones</strong></td>
									</tr>
									<tr>
										<td>Plataforma</td>
										<td align="center">
											<?php
											if($estado>1){
												if($plataforma==1){echo "X";}
											}else{
												?>
												<input type="radio" name="plataforma" id="plataforma1" value="1">
												<?php
											}
											?>
										</td>
										<td align="center">
											<?php
											if($estado>1){
												if($plataforma==2){echo "X";}
											}else{
												?>
												<input type="radio" name="plataforma" id="plataforma2" value="2">
												<?php
											}
											?>
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_plataforma;
											}else{
												?>
												<input type="text" name="observaciones_plataforma" id="observaciones_plataforma" value="">
												<?php
											}
											?>
										</td>
									</tr>
								</tbody>
							</table>
							<table  class="table table-striped table-bordered table-hover" width="100%">	
								<tbody>
									<tr>
										<td width="45%"><strong>Niveles de Fluidos</strong></td>
										<td align="center" width="10%"><strong>Conforme</strong></td>
										<td align="center" width="10%"><strong>No Conforme</strong></td>
										<td align="center" width="35%"><strong>Observaciones</strong></td>
									</tr>
									<tr>
										<td>Frenos</td>
										<td align="center">
											<?php
											if($estado>1){
												if($frenos==1){echo "X";}
											}else{
												?>
												<input type="radio" name="frenos" id="frenos1" value="1">
												<?php
											}
											?>
										</td>
										<td align="center">
											<?php
											if($estado>1){
												if($frenos==2){echo "X";}
											}else{
												?>
												<input type="radio" name="frenos" id="frenos2" value="2">
												<?php
											}
											?>
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_frenos;
											}else{
												?>
												<input type="text" name="observaciones_frenos" id="observaciones_frenos" value="">
												<?php
											}
											?>
										</td>
									</tr>
									<tr>
										<td>Aceite</td>
										<td align="center">
											<?php
											if($estado>1){
												if($aceite==1){echo "X";}
											}else{
												?>
												<input type="radio" name="aceite" id="aceite1" value="1">
												<?php
											}
											?>
										</td>
										<td align="center">
											<?php
											if($estado>1){
												if($aceite==2){echo "X";}
											}else{
												?>
												<input type="radio" name="aceite" id="aceite2" value="2">
												<?php
											}
											?>
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_aceite;
											}else{
												?>
												<input type="text" name="observaciones_aceite" id="observaciones_aceite" value="">
												<?php
											}
											?>
										</td>
									</tr>
									<tr>
										<td>Refrigerante</td>
										<td align="center">
											<?php
											if($estado>1){
												if($refrigerante==1){echo "X";}
											}else{
												?>
												<input type="radio" name="refrigerante" id="refrigerante1" value="1">
												<?php
											}
											?>
										</td>
										<td align="center">
											<?php
											if($estado>1){
												if($refrigerante==2){echo "X";}
											}else{
												?>
												<input type="radio" name="refrigerante" id="refrigerante2" value="2">
												<?php
											}
											?>
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_refrigerante;
											}else{
												?>
												<input type="text" name="observaciones_refrigerante" id="observaciones_refrigerante" value="">
												<?php
											}
											?>
										</td>
									</tr>
								</tbody>
							</table>
							<table  class="table table-striped table-bordered table-hover" width="100%">	
								<tbody>
									<tr>
										<td width="45%"><strong>Cinturones de Seguridad Del./Tras.</strong></td>
										<td align="center" width="10%"><strong>Conforme</strong></td>
										<td align="center" width="10%"><strong>No Conforme</strong></td>
										<td align="center" width="35%"><strong>Observaciones</strong></td>
									</tr>
									<tr>
										<td>Cinturones de Seguridad</td>
										<td align="center">
											<?php
											if($estado>1){
												if($cinturon_seguridad==1){echo "X";}
											}else{
												?>
												<input type="radio" name="cinturon_seguridad" id="cinturon_seguridad1" value="1">
												<?php
											}
											?>
										</td>
										<td align="center">
											<?php
											if($estado>1){
												if($cinturon_seguridad==2){echo "X";}
											}else{
												?>
												<input type="radio" name="cinturon_seguridad" id="cinturon_seguridad2" value="2">
												<?php
											}
											?>
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_cinturon_seguridad;
											}else{
												?>
												<input type="text" name="observaciones_cinturon_seguridad" id="observaciones_cinturon_seguridad" value="">
												<?php
											}
											?>
										</td>
									</tr>
								</tbody>
							</table>
							<table  class="table table-striped table-bordered table-hover" width="100%">	
								<tbody>
									<tr>
										<td width="45%"><strong>Fechas de Vencimiento</strong></td>
										<td align="center"><strong>Fecha</strong></td>
										<td align="center" width="35%"><strong>Observaciones</strong></td>
									</tr>
									<tr>
										<td>Revisión Tecnicomecanica</td>
										<td align="center">
											<?php
											if($estado>1){
												echo "$fecha_tecnicomecanica";
											}else{
											?>
												<input type="date" name="fecha_tecnicomecanica">
											<?php
											}
											?>
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_fecha_tecnicomecanica;
											}else{
												?>
												<input type="text" name="observaciones_fecha_tecnicomecanica" id="observaciones_fecha_tecnicomecanica" value="">
												<?php
											}
											?>
										</td>
									</tr>
									<tr>
										<td>Soat</td>
										<td align="center">
											<?php
											if($estado>1){
												echo "$fecha_soat";
											}else{
											?>
												<input type="date" name="fecha_soat">
											<?php
											}
											?>
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_fecha_soat;
											}else{
												?>
												<input type="text" name="observaciones_fecha_soat" id="observaciones_fecha_soat" value="">
												<?php
											}
											?>
										</td>
									</tr>
								</tbody>
							</table>
							<table  class="table table-striped table-bordered table-hover" width="100%">	
								<tbody>
									<tr>
										<td width="45%"><strong>Carro PortaTermos</strong></td>
										<td align="center" width="10%"><strong>Conforme</strong></td>
										<td align="center" width="10%"><strong>No Conforme</strong></td>
										<td align="center" width="35%"><strong>Observaciones</strong></td>
									</tr>
									<tr>
										<td>Carro PortaTermos</td>
										<td align="center">
											<?php
											if($estado>1){
												if($carro_portatermo==1){echo "X";}
											}else{
												?>
												<input type="radio" name="carro_portatermo" id="carro_portatermo1" value="1">
												<?php
											}
											?>
										</td>
										<td align="center">
											<?php
											if($estado>1){
												if($carro_portatermo==2){echo "X";}
											}else{
												?>
												<input type="radio" name="carro_portatermo" id="carro_portatermo2" value="2">
												<?php
											}
											?>
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_carro_portatermo;
											}else{
												?>
												<input type="text" name="observaciones_carro_portatermo" id="observaciones_carro_portatermo" value="">
												<?php
											}
											?>
										</td>
									</tr>
								</tbody>
							</table>
							<table  class="table table-striped table-bordered table-hover" width="100%">	
								<tbody>
									<tr>
										<td width="45%"><strong>Riatas Para Sujetar Cilindros</strong></td>
										<td align="center" width="10%"><strong>Conforme</strong></td>
										<td align="center" width="10%"><strong>No Conforme</strong></td>
										<td align="center" width="35%"><strong>Observaciones</strong></td>
									</tr>
									<tr>
										<td>Riatas Para Sujetar Cilindros</td>
										<td align="center">
											<?php
											if($estado>1){
												if($riatas==1){echo "X";}
											}else{
												?>
												<input type="radio" name="riatas" id="riatas1" value="1">
												<?php
											}
											?>
										</td>
										<td align="center">
											<?php
											if($estado>1){
												if($riatas==2){echo "X";}
											}else{
												?>
												<input type="radio" name="riatas" id="riatas2" value="2">
												<?php
											}
											?>
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_riatas;
											}else{
												?>
												<input type="text" name="observaciones_riatas" id="observaciones_riatas" value="">
												<?php
											}
											?>
										</td>
									</tr>
								</tbody>
							</table>
						</article>

						<article class="col-sm-6 col-md-6 col-lg-6">
							<table  class="table table-striped table-bordered table-hover" width="100%">	
								<tbody>
									<tr>
										<td width="45%"><strong>Direccionales</strong></td>
										<td align="center" width="10%"><strong>Conforme</strong></td>
										<td align="center" width="10%"><strong>No Conforme</strong></td>
										<td align="center" width="35%"><strong>Observaciones</strong></td>
									</tr>
									<tr>
										<td>Delanteras</td>
										<td align="center">
											<?php
											if($estado>1){
												if($direccionales_delanteras==1){echo "X";}
											}else{
												?>
												<input type="radio" name="direccionales_delanteras" id="direccionales_delanteras1" value="1">
												<?php
											}
											?>
										</td>
										<td align="center">
											<?php
											if($estado>1){
												if($direccionales_delanteras==2){echo "X";}
											}else{
												?>
												<input type="radio" name="direccionales_delanteras" id="direccionales_delanteras2" value="2">
												<?php
											}
											?>											
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_direccionales_delanteras;
											}else{
												?>
												<input type="text" name="observaciones_direccionales_delanteras" id="observaciones_direccionales_delanteras" value="">
												<?php
											}
											?>
										</td>
									</tr>
									<tr>
										<td>Traseras</td>
										<td align="center">
											<?php
											if($estado>1){
												if($direccionales_traseras==1){echo "X";}
											}else{
												?>
												<input type="radio" name="direccionales_traseras" id="direccionales_traseras1" value="1">
												<?php
											}
											?>
										</td>
										<td align="center">
											<?php
											if($estado>1){
												if($direccionales_traseras==2){echo "X";}
											}else{
												?>
												<input type="radio" name="direccionales_traseras" id="direccionales_traseras2" value="2">
												<?php
											}
											?>
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_direccionales_traseras;
											}else{
												?>
												<input type="text" name="observaciones_direccionales_traseras" id="observaciones_direccionales_traseras" value="">
												<?php
											}
											?>
										</td>
									</tr>
								</tbody>
							</table>
							<table  class="table table-striped table-bordered table-hover" width="100%">	
								<tbody>
									<tr>
										<td width="45%"><strong>Luces</strong></td>
										<td align="center" width="10%"><strong>Conforme</strong></td>
										<td align="center" width="10%"><strong>No Conforme</strong></td>
										<td align="center" width="35%"><strong>Observaciones</strong></td>
									</tr>
									<tr>
										<td>Altas</td>
										<td align="center">
											<?php
											if($estado>1){
												if($luces_altas==1){echo "X";}
											}else{
												?>
												<input type="radio" name="luces_altas" id="luces_altas1" value="1">
												<?php
											}
											?>
										</td>
										<td align="center">
											<?php
											if($estado>1){
												if($luces_altas==2){echo "X";}
											}else{
												?>
												<input type="radio" name="luces_altas" id="luces_altas2" value="2">
												<?php
											}
											?>
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_luces_altas;
											}else{
												?>
												<input type="text" name="observaciones_luces_altas" id="observaciones_luces_altas" value="">
												<?php
											}
											?>
										</td>
									</tr>
									<tr>
										<td>Bajas</td>
										<td align="center">
											<?php
											if($estado>1){
												if($luces_bajas==1){echo "X";}
											}else{
												?>
												<input type="radio" name="luces_bajas" id="luces_bajas1" value="1">
												<?php
											}
											?>
										</td>
										<td align="center">
											<?php
											if($estado>1){
												if($luces_bajas==2){echo "X";}
											}else{
												?>
												<input type="radio" name="luces_bajas" id="luces_bajas2" value="2">
												<?php
											}
											?>
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_luces_bajas;
											}else{
												?>
												<input type="text" name="observaciones_luces_bajas" id="observaciones_luces_bajas" value="">
												<?php
											}
											?>
										</td>
									</tr>
									<tr>
										<td>Stops</td>
										<td align="center">
											<?php
											if($estado>1){
												if($luces_stops==1){echo "X";}
											}else{
												?>
												<input type="radio" name="luces_stops" id="luces_stops1" value="1">
												<?php
											}
											?>
										</td>
										<td align="center">
											<?php
											if($estado>1){
												if($luces_stops==2){echo "X";}
											}else{
												?>
												<input type="radio" name="luces_stops" id="luces_stops2" value="2">
												<?php
											}
											?>
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_luces_stops;
											}else{
												?>
												<input type="text" name="observaciones_luces_stops" id="observaciones_luces_stops" value="">
												<?php
											}
											?>
										</td>
									</tr>
									<tr>
										<td>Reversa</td>
										<td align="center">
											<?php
											if($estado>1){
												if($luces_reversa==1){echo "X";}
											}else{
												?>
												<input type="radio" name="luces_reversa" id="luces_reversa1" value="1">
												<?php
											}
											?>											
										</td>
										<td align="center">
											<?php
											if($estado>1){
												if($luces_reversa==2){echo "X";}
											}else{
												?>
												<input type="radio" name="luces_reversa" id="luces_reversa2" value="2">
												<?php
											}
											?>
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_luces_reversa;
											}else{
												?>
												<input type="text" name="observaciones_luces_reversa" id="observaciones_luces_reversa" value="">
												<?php
											}
											?>
										</td>
									</tr>
									<tr>
										<td>Parqueo</td>
										<td align="center">
											<?php
											if($estado>1){
												if($luces_parqueo==1){echo "X";}
											}else{
												?>
												<input type="radio" name="luces_parqueo" id="luces_parqueo1" value="1">
												<?php
											}
											?>
										</td>
										<td align="center">
											<?php
											if($estado>1){
												if($luces_parqueo==2){echo "X";}
											}else{
												?>
												<input type="radio" name="luces_parqueo" id="luces_parqueo2" value="2">
												<?php
											}
											?>
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_luces_parqueo;
											}else{
												?>
												<input type="text" name="observaciones_luces_parqueo" id="observaciones_luces_parqueo" value="">
												<?php
											}
											?>
										</td>
									</tr>
								</tbody>
							</table>
							<table  class="table table-striped table-bordered table-hover" width="100%">	
								<tbody>
									<tr>
										<td width="45%"><strong>Frenos</strong></td>
										<td align="center" width="10%"><strong>Conforme</strong></td>
										<td align="center" width="10%"><strong>No Conforme</strong></td>
										<td align="center" width="35%"><strong>Observaciones</strong></td>
									</tr>
									<tr>
										<td>Principal</td>
										<td align="center">
											<?php
											if($estado>1){
												if($frenos_principal==1){echo "X";}
											}else{
												?>
												<input type="radio" name="frenos_principal" id="frenos_principal1" value="1">
												<?php
											}
											?>
										</td>
										<td align="center">
											<?php
											if($estado>1){
												if($frenos_principal==2){echo "X";}
											}else{
												?>
												<input type="radio" name="frenos_principal" id="frenos_principal2" value="2">
												<?php
											}
											?>
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_frenos_principal;
											}else{
												?>
												<input type="text" name="observaciones_frenos_principal" id="observaciones_frenos_principal" value="">
												<?php
											}
											?>
										</td>
									</tr>
									<tr>
										<td>Emergencia</td>
										<td align="center">
											<?php
											if($estado>1){
												if($frenos_emergencia==1){echo "X";}
											}else{
												?>
												<input type="radio" name="frenos_emergencia" id="frenos_emergencia1" value="1">
												<?php
											}
											?>
										</td>
										<td align="center">
											<?php
											if($estado>1){
												if($frenos_emergencia==2){echo "X";}
											}else{
												?>
												<input type="radio" name="frenos_emergencia" id="frenos_emergencia2" value="2">
												<?php
											}
											?>
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_frenos_emergencia;
											}else{
												?>
												<input type="text" name="observaciones_frenos_emergencia" id="observaciones_frenos_emergencia" value="">
												<?php
											}
											?>
										</td>
									</tr>
								</tbody>
							</table>
							<table  class="table table-striped table-bordered table-hover" width="100%">	
								<tbody>
									<tr>
										<td width="45%"><strong>Espejos</strong></td>
										<td align="center" width="10%"><strong>Conforme</strong></td>
										<td align="center" width="10%"><strong>No Conforme</strong></td>
										<td align="center" width="35%"><strong>Observaciones</strong></td>
									</tr>
									<tr>
										<td>Laterales Der/Izq</td>
										<td align="center">
											<?php
											if($estado>1){
												if($laterales_der_izq==1){echo "X";}
											}else{
												?>
												<input type="radio" name="laterales_der_izq" id="laterales_der_izq1" value="1">
												<?php
											}
											?>
										</td>
										<td align="center">
											<?php
											if($estado>1){
												if($laterales_der_izq==2){echo "X";}
											}else{
												?>
												<input type="radio" name="laterales_der_izq" id="laterales_der_izq2" value="2">
												<?php
											}
											?>
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_laterales_der_izq;
											}else{
												?>
												<input type="text" name="observaciones_laterales_der_izq" id="observaciones_laterales_der_izq" value="">
												<?php
											}
											?>
										</td>
									</tr>
									<tr>
										<td>Retrovisor</td>
										<td align="center">
											<?php
											if($estado>1){
												if($retrovisor==1){echo "X";}
											}else{
												?>
												<input type="radio" name="retrovisor" id="retrovisor1" value="1">
												<?php
											}
											?>										
										</td>
										<td align="center">
											<?php
											if($estado>1){
												if($retrovisor==2){echo "X";}
											}else{
												?>
												<input type="radio" name="retrovisor" id="retrovisor2" value="2">
												<?php
											}
											?>
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_retrovisor;
											}else{
												?>
												<input type="text" name="observaciones_retrovisor" id="observaciones_retrovisor" value="">
												<?php
											}
											?>
										</td>
									</tr>
								</tbody>
							</table>
							<table  class="table table-striped table-bordered table-hover" width="100%">	
								<tbody>
									<tr>
										<td width="45%"><strong>Pito</strong></td>
										<td align="center" width="10%"><strong>Conforme</strong></td>
										<td align="center" width="10%"><strong>No Conforme</strong></td>
										<td align="center" width="35%"><strong>Observaciones</strong></td>
									</tr>
									<tr>
										<td>Pito</td>
										<td align="center">
											<?php
											if($estado>1){
												if($pito==1){echo "X";}
											}else{
												?>
												<input type="radio" name="pito" id="pito1" value="1">
												<?php
											}
											?>
										</td>
										<td align="center">
											<?php
											if($estado>1){
												if($pito==2){echo "X";}
											}else{
												?>
												<input type="radio" name="pito" id="pito2" value="2">
												<?php
											}
											?>											
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_pito;
											}else{
												?>
												<input type="text" name="observaciones_pito" id="observaciones_pito" value="">
												<?php
											}
											?>
										</td>
									</tr>
								</tbody>
							</table>
							<table  class="table table-striped table-bordered table-hover" width="100%">	
								<tbody>
									<tr>
										<td width="45%"><strong>Apoya Cabezas</strong></td>
										<td align="center" width="10%"><strong>Conforme</strong></td>
										<td align="center" width="10%"><strong>No Conforme</strong></td>
										<td align="center" width="35%"><strong>Observaciones</strong></td>
									</tr>
									<tr>
										<td>Delanteros</td>
										<td align="center">
											<?php
											if($estado>1){
												if($apoya_cabezas_delantero==1){echo "X";}
											}else{
												?>
												<input type="radio" name="apoya_cabezas_delantero" id="apoya_cabezas_delantero1" value="1">
												<?php
											}
											?>
										</td>
										<td align="center">
											<?php
											if($estado>1){
												if($apoya_cabezas_delantero==2){echo "X";}
											}else{
												?>
												<input type="radio" name="apoya_cabezas_delantero" id="apoya_cabezas_delantero2" value="2">
												<?php
											}
											?>
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_apoya_cabezas_delantero;
											}else{
												?>
												<input type="text" name="observaciones_apoya_cabezas_delantero" id="observaciones_apoya_cabezas_delantero" value="">
												<?php
											}
											?>
										</td>
									</tr>
									<tr>
										<td>Traseros</td>
										<td align="center">
											<?php
											if($estado>1){
												if($apoya_cabezas_trasero==1){echo "X";}
											}else{
												?>
												<input type="radio" name="apoya_cabezas_trasero" id="apoya_cabezas_trasero1" value="1">
												<?php
											}
											?>
										</td>
										<td align="center">
											<?php
											if($estado>1){
												if($apoya_cabezas_trasero==2){echo "X";}
											}else{
												?>
												<input type="radio" name="apoya_cabezas_trasero" id="apoya_cabezas_trasero2" value="2">
												<?php
											}
											?>
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_apoya_cabezas_trasero;
											}else{
												?>
												<input type="text" name="observaciones_apoya_cabezas_trasero" id="observaciones_apoya_cabezas_trasero" value="">
												<?php
											}
											?>
										</td>
									</tr>
								</tbody>
							</table>
							<table  class="table table-striped table-bordered table-hover" width="100%">	
								<tbody>
									<tr>
										<td width="50%"><strong>Ultima Fecha de Mantenimiento</strong></td>
										<td><strong>Fecha</strong></td>
									</tr>
									<tr>
										<td>Cambio de Aceite</td>
										<td align="center">
											<?php
											if($estado>1){
												echo "$cambio_aceite";
											}else{
												echo "No Aplica";
											}
											?>
										</td>
									</tr>
									<tr>
										<td>Sincronización</td>
										<td align="center">
											<?php
											if($estado>1){
												echo "$sincronizacion";
											}else{
												echo "No Aplica";
											}
											?>
										</td>
									</tr>
									<tr>
										<td>Alineación-Balanceo</td>
										<td align="center">
											<?php
											if($estado>1){
												echo "$alineacion_balanceo";
											}else{
												echo "No Aplica";
											}
											?>
										</td>
									</tr>
									<tr>
										<td>Cambio de Llantas</td>
										<td align="center">
											<?php
											if($estado>1){
												echo "$cambio_llantas";
											}else{
												echo "No Aplica";
											}
											?>
										</td>
									</tr>
								</tbody>
							</table>
							<table  class="table table-striped table-bordered table-hover" width="100%">	
								<tbody>
									<tr>
										<td width="45%"><strong>Carro PortaCilindros</strong></td>
										<td align="center" width="10%"><strong>Conforme</strong></td>
										<td align="center" width="10%"><strong>No Conforme</strong></td>
										<td align="center" width="35%"><strong>Observaciones</strong></td>
									</tr>
									<tr>
										<td>Carro PortaCilindros</td>
										<td align="center">
											<?php
											if($estado>1){
												if($carro_portacilindros==1){echo "X";}
											}else{
												?>
												<input type="radio" name="carro_portacilindros" id="carro_portacilindros1" value="1">
												<?php
											}
											?>
										</td>
										<td align="center">
											<?php
											if($estado>1){
												if($carro_portacilindros==2){echo "X";}
											}else{
												?>
												<input type="radio" name="carro_portacilindros" id="carro_portacilindros2" value="2">
												<?php
											}
											?>
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_carro_portacilindros;
											}else{
												?>
												<input type="text" name="observaciones_carro_portacilindros" id="observaciones_carro_portacilindros" value="">
												<?php
											}
											?>
										</td>
									</tr>
								</tbody>
							</table>
							<table  class="table table-striped table-bordered table-hover" width="100%">	
								<tbody>
									<tr>
										<td width="45%"><strong>Cadenas Para Sujetar Cilindros</strong></td>
										<td align="center" width="10%"><strong>Conforme</strong></td>
										<td align="center" width="10%"><strong>No Conforme</strong></td>
										<td align="center" width="35%"><strong>Observaciones</strong></td>
									</tr>
									<tr>
										<td>Cadenas Para Sujetar Cilindros</td>
										<td align="center">
											<?php
											if($estado>1){
												if($cadenas==1){echo "X";}
											}else{
												?>
												<input type="radio" name="cadenas" id="cadenas1" value="1">
												<?php
											}
											?>
										</td>
										<td align="center">
											<?php
											if($estado>1){
												if($cadenas==2){echo "X";}
											}else{
												?>
												<input type="radio" name="cadenas" id="cadenas2" value="2">
												<?php
											}
											?>
										</td>
										<td>
											<?php
											if($estado>1){
												echo $observaciones_cadenas;
											}else{
												?>
												<input type="text" name="observaciones_cadenas" id="observaciones_cadenas" value="">
												<?php
											}
											?>
										</td>
									</tr>
								</tbody>
							</table>
							<?php
							if($estado==1){
								?>
								<table  class="table table-striped table-bordered table-hover" width="100%">	
									<tbody>
										<tr>
											<td align="center">
												<input type="submit" class="btn btn-primary" name="g_elementos_vehiculo" value="GUARDAR">
											</td>
										</tr>
									</tbody>
								</table>
								<?php
							}
							?>
						</article>
					</div>
				</form>
			</section>
		<?php
			if($estado>1){
				?>
				<section id="widget-grid" class="">
					<form method="POST" action="verificacion_mensual_vehiculos.php" action="verificacion_mensual_vehiculos.php">
						<div class="row">
							<article class="col-sm-6 col-md-6 col-lg-6">
								<table  class="table table-striped table-bordered table-hover" width="100%">	
									<tbody>
										<tr>
											<input type="hidden" name="id_verificacion_mensual" value="<?php echo $id_verificacion_mensual; ?>">
											<td width="20%"><strong>Equipo de Carretera</strong></td>
											<td width="40%" align="center"><strong>Criterio</strong></td>
											<td align="center"><strong>Conforme</strong></td>
											<td align="center"><strong>No Conforme</strong></td>
											<td align="center"><strong>Observaciones</strong></td>
										</tr>
										<tr>
											<td rowspan="2">Extintor</td>
											<td align="center">
												Fecha de Vencimiento: 
												<?php
												if($estado>2){
													echo "$vencimiento_extintor";
												}else{
													?>
													<input type="date" name="vencimiento_extintor">
													<?php
												}
												?>
											</td>
											<td align="center">
												<?php
												if($estado>2){
													if($conforme_vencimiento_extintor==1){echo "X";}
												}else{
													?>
													<input type="radio" name="conforme_vencimiento_extintor" id="conforme_vencimiento_extintor1" value="1">
													<?php
												}
												?>
											</td>
											<td align="center">
												<?php
												if($estado>2){
													if($conforme_vencimiento_extintor==2){echo "X";}
												}else{
													?>
													<input type="radio" name="conforme_vencimiento_extintor" id="conforme_vencimiento_extintor2" value="2">
													<?php
												}
												?>
											</td>
											<td>
											<?php
											if($estado>2){
												echo $observaciones_vencimiento_extintor;
											}else{
												?>
												<input type="text" name="observaciones_vencimiento_extintor" size="10" id="observaciones_vencimiento_extintor" value="">
												<?php
											}
											?>
											</td>
										</tr>
										<tr>
											<td align="center">
												Capacidad:
												<?php
												if($estado>2){
													echo "$capacidad_extintor";
												}else{
													?>
													<input type="text" name="capacidad_extintor" size="10"> lbs
													<?php
												}
												?> 
												
											</td>
											<td align="center">
												<?php
												if($estado>2){
													if($conforme_capacidad_extintor==1){echo "X";}
												}else{
													?>
													<input type="radio" name="conforme_capacidad_extintor" id="conforme_capacidad_extintor1" value="1">
													<?php
												}
												?>
											</td>
											<td align="center">
												<?php
												if($estado>2){
													if($conforme_capacidad_extintor==2){echo "X";}
												}else{
													?>
													<input type="radio" name="conforme_capacidad_extintor" id="conforme_capacidad_extintor2" value="2">
													<?php
												}
												?>
											</td>
											<td>
											<?php
											if($estado>2){
												echo $observaciones_capacidad_extintor;
											}else{
												?>
												<input type="text" name="observaciones_capacidad_extintor" size="10" id="observaciones_capacidad_extintor" value="">
												<?php
											}
											?>
											</td>
										</tr>
										<tr>
											<td rowspan="2" align="center">Herramientas</td>
											<td align="center">Alicate, destornillaodres y llaves fijas</td>
											<td align="center">
												<?php
												if($estado>2){
													if($alicates_destornilladores==1){echo "X";}
												}else{
													?>
													<input type="radio" name="alicates_destornilladores" id="alicates_destornilladores1" value="1">
													<?php
												}
												?>
											</td>
											<td align="center">
												<?php
												if($estado>2){
													if($alicates_destornilladores==2){echo "X";}
												}else{
													?>
													<input type="radio" name="alicates_destornilladores" id="alicates_destornilladores2" value="2">
													<?php
												}
												?>
											</td>
											<td>
											<?php
											if($estado>2){
												echo $observaciones_alicates_destornilladores;
											}else{
												?>
												<input type="text" name="observaciones_alicates_destornilladores" size="10" id="observaciones_alicates_destornilladores" value="">
												<?php
											}
											?>
											</td>
										</tr>
										<tr>
											<td align="center">Llave Expansiva</td>
											<td align="center">
												<?php
												if($estado>2){
													if($llave_fija==1){echo "X";}
												}else{
													?>
													<input type="radio" name="llave_fija" id="llave_fija1" value="1">
													<?php
												}
												?>
											</td>
											<td align="center">
												<?php
												if($estado>2){
													if($llave_fija==2){echo "X";}
												}else{
													?>
													<input type="radio" name="llave_fija" id="llave_fija2" value="2">
													<?php
												}
												?>
											</td>
											<td>
											<?php
											if($estado>2){
												echo $observaciones_llave_fija;
											}else{
												?>
												<input type="text" name="observaciones_llave_fija" size="10" id="observaciones_llave_fija" value="">
												<?php
											}
											?>
											</td>
										</tr>
										<tr>
											<td align="center">Señales</td>
											<td>Rombos de seguridad, señal UN, banda amarilla con franjas negras, roja con franjas blancas de material reflectivo</td>
											<td align="center">
												<?php
												if($estado>2){
													if($senales==1){echo "X";}
												}else{
													?>
													<input type="radio" name="senales" id="senales1" value="1">
													<?php
												}
												?>
											</td>
											<td align="center">
												<?php
												if($estado>2){
													if($senales==2){echo "X";}
												}else{
													?>
													<input type="radio" name="senales" id="senales2" value="2">
													<?php
												}
												?>
											</td>
											<td>
											<?php
											if($estado>2){
												echo $observaciones_senales;
											}else{
												?>
												<input type="text" name="observaciones_senales" size="10" id="observaciones_senales" value="">
												<?php
											}
											?>
											</td>
										</tr>
										<tr>
											<td align="center">Botiquin</td>
											<td>Yodopividona solución antiséptico bolsa (120 ml), jabón, gasas, curas, venda elástica, rollo micropore, algodón, sales de rehidratación oral, baja lenguas, bolsa de suero fisiológico, guantes de látex desechables, toallas higiénicas, tijeras y termómetro oral</td>
											<td align="center">
												<?php
												if($estado>2){
													if($botiquin==1){echo "X";}
												}else{
													?>
													<input type="radio" name="botiquin" id="botiquin1" value="1">
													<?php
												}
												?>
											</td>
											<td align="center">
												<?php
												if($estado>2){
													if($botiquin==2){echo "X";}
												}else{
													?>
													<input type="radio" name="botiquin" id="botiquin2" value="2">
													<?php
												}
												?>
											</td>
											<td>
											<?php
											if($estado>2){
												echo $observaciones_botiquin;
											}else{
												?>
												<input type="text" name="observaciones_botiquin" size="10" id="observaciones_botiquin" value="">
												<?php
											}
											?>
											</td>
										</tr>
									</tbody>
								</table>
							</article>
							<article class="col-sm-6 col-md-6 col-lg-6">
								<table  class="table table-striped table-bordered table-hover" width="100%">
									<tbody>
										<tr>
											<td width="20%"><strong>Equipo de Carretera</strong></td>
											<td width="40%" align="center"><strong>Criterio</strong></td>
											<td align="center"><strong>Conforme</strong></td>
											<td align="center"><strong>No Conforme</strong></td>
											<td align="center"><strong>Observaciones</strong></td>
										</tr>
										<tr>
											<td align="center">Cruceta</td>
											<td>Apta para el vehículo</td>
											<td align="center">
												<?php
												if($estado>2){
													if($cruceta==1){echo "X";}
												}else{
													?>
													<input type="radio" name="cruceta" id="cruceta1" value="1">
													<?php
												}
												?>
											</td>
											<td align="center">
												<?php
												if($estado>2){
													if($cruceta==2){echo "X";}
												}else{
													?>
													<input type="radio" name="cruceta" id="cruceta2" value="2">
													<?php
												}
												?>
											</td>
												<td>
											<?php
											if($estado>2){
												echo $observaciones_cruceta;
											}else{
												?>
												<input type="text" name="observaciones_cruceta" size="10" id="observaciones_cruceta" value="">
												<?php
											}
											?>
											</td>
										</tr>
										<tr>
											<td align="center">Gato</td>
											<td>Con capacidad para elevar el vehículo</td>
											<td align="center">
												<?php
												if($estado>2){
													if($gato==1){echo "X";}
												}else{
													?>
													<input type="radio" name="gato" id="gato1" value="1">
													<?php
												}
												?>
											</td>
											<td align="center">
												<?php
												if($estado>2){
													if($cruceta==2){echo "X";}
												}else{
													?>
													<input type="radio" name="gato" id="gato2" value="2">
													<?php
												}
												?>
											</td>
												<td>
											<?php
											if($estado>2){
												echo $observaciones_gato;
											}else{
												?>
												<input type="text" name="observaciones_gato" size="10" id="observaciones_gato" value="">
												<?php
											}
											?>
											</td>
										</tr>
										<tr>
											<td align="center">Conos</td>
											<td>Dos conos con cinta reflectiva</td>
											<td align="center">
												<?php
												if($estado>2){
													if($conos==1){echo "X";}
												}else{
													?>
													<input type="radio" name="conos" id="conos1" value="1">
													<?php
												}
												?>
											</td>
											<td align="center">
												<?php
												if($estado>2){
													if($cruceta==2){echo "X";}
												}else{
													?>
													<input type="radio" name="conos" id="conos2" value="2">
													<?php
												}
												?>
											</td>
												<td>
											<?php
											if($estado>2){
												echo $observaciones_conos;
											}else{
												?>
												<input type="text" name="observaciones_conos" size="10" id="observaciones_conos" value="">
												<?php
											}
											?>
											</td>
										</tr>
										<tr>
											<td align="center">Tacos</td>
											<td>Dos tacos para bloquear el vehículo</td>
											<td align="center">
												<?php
												if($estado>2){
													if($tacos==1){echo "X";}
												}else{
													?>
													<input type="radio" name="tacos" id="tacos1" value="1">
													<?php
												}
												?>
											</td>
											<td align="center">
												<?php
												if($estado>2){
													if($cruceta==2){echo "X";}
												}else{
													?>
													<input type="radio" name="tacos" id="tacos2" value="2">
													<?php
												}
												?>
											</td>
												<td>
											<?php
											if($estado>2){
												echo $observaciones_tacos;
											}else{
												?>
												<input type="text" name="observaciones_tacos" size="10" id="observaciones_tacos" value="">
												<?php
											}
											?>
											</td>
										</tr>
										<tr>
											<td align="center">Chaleco</td>
											<td>Debe ser reflectivo</td>
											<td align="center">
												<?php
												if($estado>2){
													if($chaleco==1){echo "X";}
												}else{
													?>
													<input type="radio" name="chaleco" id="chaleco1" value="1">
													<?php
												}
												?>
											</td>
											<td align="center">
												<?php
												if($estado>2){
													if($cruceta==2){echo "X";}
												}else{
													?>
													<input type="radio" name="chaleco" id="chaleco1" value="2">
													<?php
												}
												?>
											</td>
											<td>
											<?php
											if($estado>2){
												echo $observaciones_chaleco;
											}else{
												?>
												<input type="text" name="observaciones_chaleco" size="10" id="observaciones_chaleco" value="">
												<?php
											}
											?>
											</td>
										</tr>
									</tbody>
								</table>
								<?php
								if($estado==2){
									?>
									<table  class="table table-striped table-bordered table-hover" width="100%">	
										<tbody>
											<tr>
												<td align="center">
													<input type="submit" class="btn btn-primary" name="g_elementos_emergencia" value="TERMINAR VERIFICACIÓN">
												</td>
											</tr>
										</tbody>
									</table>
									<?php
								}else if($estado>2){
									?>
									<form id="form4" name="form4" method="POST" action="verificacion_mensual_vehiculos.php"></form>
										<table  class="table table-striped table-bordered table-hover" width="100%">	
											<tbody>
												<tr>
													<td align="center" colspan="2">
														¿Recibe Conforme?
													</td>
												</tr>
												<tr>
													<td width="50%" align="center">
														Confome
													</td>
													<td align="center">
														<?php
														if($estado>3){
															if($conforme_recepcion==1){echo "X";}
														}else{
															?>
															<input type="radio" name="conforme_recepcion" id="conforme_recepcion1" value="1">
															<?php
														}
														?>
													</td>
												</tr>
												<tr>
													<td align="center">
														No Conforme
													</td>
													<td align="center">
														<?php
														if($estado>3){
															if($conforme_recepcion==2){echo "X";}
														}else{
															?>
															<input type="radio" name="conforme_recepcion" id="conforme_recepcion2" value="2">
															<?php
														}
														?>
													</td>
												</tr>
											</tbody>
										</table>

										<table class="table table-striped table-bordered table-hover" width="100%">
											<tbody>
												<tr>
													<td>Observaciones</td>
												</tr>
												<tr>
													<td>
														<?php
														if($estado>3){
															echo $observaciones_recepcion;
														}else{
															?>
															<textarea name="observaciones_recepcion" cols="60" rows="5"></textarea>
															<input type="hidden" name="id_verificacion_mensual" value="<?php echo $id_verificacion_mensual; ?>">
															<?php
														}
														?>
													</td>
												</tr>
											</tbody>
										</table>
										<?php
										if($estado==3){
											?>
											<table class="table table-striped table-bordered table-hover" width="100%">
												<tbody>
													<tr>
														<td align="center">
															<input type="submit" class="btn btn-primary" name="g_recepcion" value="CONFIRMAR RECEPCIÓN">
														</td>
													</tr>
												</tbody>
											</table>
											<?php
										}
										?>

										
									</form>

									<table  class="table table-striped table-bordered table-hover" width="100%">	
										<tbody>
											<tr>
												<td align="center">
													<a href="pdf_verificacion_mensual.php?id_verificacion_mensual=<?php echo $id_verificacion_mensual; ?>">VER PDF</a>
												</td>
											</tr>
										</tbody>
									</table>
									<?php
								}
								?>
							</article>
						</div>
					</form>
				</section>
				<?php
			}
		}
		?>
	</div>
</div>

<script type="text/javascript">
	function validar_error(valor){

		campo = valor;

		error = document.getElementById('error_absoluto'+campo).value;

		if(error>1){

			if(confirm("El valor del error Absoluto es mayor a 1%, la verificación de la exactitud será rechazada")){
				document.getElementById('rechazada'+valor).value = 1;
				alert("Prueba Calibración rechazada");
			}
		}

	}
</script>
</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->
<?php
	include("inc/footer.php");
	include("inc/scripts.php"); 
?>
<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="js/plugin/datatable-responsive/datatables.responsive.min.js"></script>

<script type="text/javascript">
// DO NOT REMOVE : GLOBAL FUNCTIONS!

$(document).ready(function() {
	
	/* // DOM Position key index //
		
	l - Length changing (dropdown)
	f - Filtering input (search)
	t - The Table! (datatable)
	i - Information (records)
	p - Pagination (paging)
	r - pRocessing 
	< and > - div elements
	<"#id" and > - div with an id
	<"class" and > - div with a class
	<"#id.class" and > - div with an id and class
	
	Also see: http://legacy.datatables.net/usage/features
	*/	

	/* BASIC ;*/
		var responsiveHelper_dt_basic = undefined;
		var responsiveHelper_datatable_fixed_column = undefined;
		var responsiveHelper_datatable_col_reorder = undefined;
		var responsiveHelper_datatable_tabletools = undefined;
		
		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};

		$('#dt_basic').dataTable({
			"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
			"autoWidth" : true,
			"preDrawCallback" : function() {
				// Initialize the responsive datatables helper once.
				if (!responsiveHelper_dt_basic) {
					responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
				}
			},
			"rowCallback" : function(nRow) {
				responsiveHelper_dt_basic.createExpandIcon(nRow);
			},
			"drawCallback" : function(oSettings) {
				responsiveHelper_dt_basic.respond();
			}
		});

	/* END BASIC */
	
	/* COLUMN FILTER  */
    var otable = $('#datatable_fixed_column').DataTable({
    	//"bFilter": false,
    	//"bInfo": false,
    	//"bLengthChange": false
    	//"bAutoWidth": false,
    	//"bPaginate": false,
    	//"bStateSave": true // saves sort state using localStorage
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_fixed_column) {
				responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_fixed_column.respond();
		}		
	
    });
    
    // custom toolbar
    $("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');
    	   
    // Apply the filter
    $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {
    	
        otable
            .column( $(this).parent().index()+':visible' )
            .search( this.value )
            .draw();
            
    } );
    /* END COLUMN FILTER */   

	/* COLUMN SHOW - HIDE */
	$('#datatable_col_reorder').dataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_col_reorder) {
				responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_col_reorder.respond();
		}			
	});
	
	/* END COLUMN SHOW - HIDE */

	/* TABLETOOLS */
	$('#datatable_tabletools').dataTable({
		
		// Tabletools options: 
		//   https://datatables.net/extensions/tabletools/button_options
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        "oTableTools": {
        	 "aButtons": [
             "copy",
             "csv",
             "xls",
                {
                    "sExtends": "pdf",
                    "sTitle": "SmartAdmin_PDF",
                    "sPdfMessage": "SmartAdmin PDF Export",
                    "sPdfSize": "letter"
                },
             	{
                	"sExtends": "print",
                	"sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>"
            	}
             ],
            "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
        },
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_tabletools) {
				responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_tabletools.respond();
		}
	});
	
	/* END TABLETOOLS */

})

</script>
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script type="text/javascript">

 
		
		$(document).ready(function() {
			
			// menu
			$("#menu").menu();
		
			/*
			 * AUTO COMPLETE AJAX
			 */
		
			function log(message) {
				$("<div>").text(message).prependTo("#log");
				$("#log").scrollTop(0);
			}
		
			$("#city").autocomplete({
				source : function(request, response) {
					$.ajax({
						url : "http://ws.geonames.org/searchJSON",
						dataType : "jsonp",
						data : {
							featureClass : "P",
							style : "full",
							maxRows : 12,
							name_startsWith : request.term
						},
						success : function(data) {
							response($.map(data.geonames, function(item) {
								return {
									label : item.name + (item.adminName1 ? ", " + item.adminName1 : "") + ", " + item.countryName,
									value : item.name
								}
							}));
						}
					});
				},
				minLength : 2,
				select : function(event, ui) {
					log(ui.item ? "Selected: " + ui.item.label : "Nothing selected, input was " + this.value);
				}
			});
		
			/*
			 * Spinners
			 */
			$("#spinner").spinner();
			$("#spinner-decimal").spinner({
				step : 0.01,
				numberFormat : "n"
			});
		
			$("#spinner-currency").spinner({
				min : 5,
				max : 2500,
				step : 25,
				start : 1000,
				numberFormat : "C"
			});
		
			/*
			 * CONVERT DIALOG TITLE TO HTML
			 * REF: http://stackoverflow.com/questions/14488774/using-html-in-a-dialogs-title-in-jquery-ui-1-10
			 */
			$.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
				_title : function(title) {
					if (!this.options.title) {
						title.html("&#160;");
					} else {
						title.html(this.options.title);
					}
				}
			}));
		
		
			/*
			* DIALOG SIMPLE
			*/
		
			// Dialog click
			$('#dialog_link').click(function() {
				$('#dialog_simple').dialog('open');
				return false;
		
			});
		
			$('#dialog_simple').dialog({
				autoOpen : false,
				width : 600,
				resizable : false,
				modal : true,
				title : "<div class='widget-header'><h4><i class='fa fa-warning'></i> Empty the recycle bin?</h4></div>",
				buttons : [{
					html : "<i class='fa fa-trash-o'></i>&nbsp; Delete all items",
					"class" : "btn btn-danger",
					click : function() {
						$(this).dialog("close");
					}
				}, {
					html : "<i class='fa fa-times'></i>&nbsp; Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
					}
				}]
			});
		
			/*
			* DIALOG HEADER ICON
			*/
		
			// Modal Link
			$('#modal_link').click(function() {
				$('#dialog-message').dialog('open');
				return false;
			});
		
			$("#dialog-message").dialog({
				autoOpen : false,
				modal : true,
				title : "<div class='widget-header'><h4><i class='icon-ok'></i> jQuery UI Dialog</h4></div>",
				buttons : [{
					html : "Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
					}
				}]
		
			});
		
			/*
			 * Remove focus from buttons
			 */
			$('.ui-dialog :button').blur();
		
			/*
			 * Just Tabs
			 */
		
			$('#tabs').tabs();
		
			/*
			 *  Simple tabs adding and removing
			 */
		
			$('#tabs2').tabs();
		
			// Dynamic tabs
			var tabTitle = $("#tab_title"), tabContent = $("#tab_content"), tabTemplate = "<li style='position:relative;'> <span class='air air-top-left delete-tab' style='top:7px; left:7px;'><button class='btn btn-xs font-xs btn-default hover-transparent'><i class='fa fa-times'></i></button></span></span><a href='#{href}'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; #{label}</a></li>", tabCounter = 2;
		
			var tabs = $("#tabs2").tabs();
		
			// modal dialog init: custom buttons and a "close" callback reseting the form inside
			var dialog = $("#addtab").dialog({
				autoOpen : false,
				width : 600,
				resizable : false,
				modal : true,
				buttons : [{
					html : "<i class='fa fa-times'></i>&nbsp; Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
		
					}
				}]
			});
		
			// addTab form: calls addTab function on submit and closes the dialog
			var form = dialog.find("form").submit(function(event) {
				addTab();
				dialog.dialog("close");
				
			});
		
			// actual addTab function: adds new tab using the input from the form above

			
			function addTab() 
			{
				
				var label = tabTitle.val() || "Tab " + tabCounter, id = "tabs-" + tabCounter, li = $(tabTemplate.replace(/#\{href\}/g, "#" + id).replace(/#\{label\}/g, label)), tabContentHtml = tabContent.val() || "Tab " + tabCounter + " content.";
		
				tabs.find(".ui-tabs-nav").append(li);
				tabs.append("<div id='" + id + "'><p>" + tabContentHtml + "</p></div>");
				tabs.tabs("refresh");
				tabCounter++;
		
				// clear fields
				$("#tab_title").val("");
				$("#tab_content").val("");
			}
		
			// addTab button: just opens the dialog
			$("#add_tab").button().click(function() {
				var idd = document.getElementById('num').firstChild.nodeValue;

				
				dialog.dialog("open");
			});
		
			// close icon: removing the tab on click
					
			/*
			* ACCORDION
			*/
			//jquery accordion
			
		     var accordionIcons = {
		         header: "fa fa-plus",    // custom icon class
		         activeHeader: "fa fa-minus" // custom icon class
		     };
		     
			$("#accordion").accordion({
				autoHeight : false,
				heightStyle : "content",
				collapsible : true,
				animate : 300,
				icons: accordionIcons,
				header : "h4",
			})
		
			/*
			 * PROGRESS BAR
			 */
			$("#progressbar").progressbar({
		     	value: 25,
		     	create: function( event, ui ) {
		     		$(this).removeClass("ui-corner-all").addClass('progress').find(">:first-child").removeClass("ui-corner-left").addClass('progress-bar progress-bar-success');
				}
			});			

		})

	</script>
	


<?php 
	//include footer
	include("inc/google-analytics.php"); 
}
else
{
    header("Location:index.php");
}
?>