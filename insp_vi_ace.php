<?php
session_start();
date_default_timezone_set("America/Bogota");
$Fecha = date("Y-m-d");
require ("libraries/conexion.php");

$estado_prueba = 0;
if($_SESSION['logged']== 'yes')
{ 
    $id_has_movimiento_cilindro_pev = isset($_REQUEST['id_has_movimiento_cilindro_pev']) ? $_REQUEST['id_has_movimiento_cilindro_pev'] : NULL;
    $idUser =$_SESSION['su'];
    $acc = $_SESSION['acc'];

    require_once("inc/init.php");
    require_once("inc/config.ui.php");
    $page_title = "Inspección Visual Acero";
    $page_css[] = "your_style.css";
    include("inc/header.php");
    $page_nav[""][""][""][""] = true;
    include("inc/nav.php");

    if (isset($_POST['g_insp_visual_acero'])) //guardar información de inspección visual de acero
    {
        $id_has_movimiento_cilindro_pev = $_POST['id_has_movimiento_cilindro_pev'];
        $estado_acero = $_POST['estado_acero'];
        $quema_arco = $_POST['quema_arco'];
        $quema_arco_ext = $_POST['quema_arco_ext'];
        $corr_int = $_POST['corr_int'];
        $pica_int = $_POST['pica_int'];
        $grasa_aceite = $_POST['grasa_aceite'];        
        $remo_pintura = $_POST['remo_pintura'];
        $remo_etiquetas = $_POST['remo_etiquetas'];        
        $adul_info = $_POST['adul_info'];
        $corr_ext = $_POST['corr_ext'];
        $fisu_lineal = $_POST['fisu_lineal'];
        $corr_gene = $_POST['corr_gene'];
        $pica_ais = $_POST['pica_ais'];
        $abolladuras = $_POST['abolladuras'];
        $abombamiento = $_POST['abombamiento'];
        $martillo = $_POST['martillo'];
        $rotura = $_POST['rotura'];
        $dobleces = $_POST['dobleces'];
        $valles = $_POST['valles'];
        $hil_ros = $_POST['hil_ros'];
        $hilos_roscas = $_POST['hilos_roscas'];

        $quema_arco_text = $_POST['quema_arco_text'];
        $quema_arco_ext_text = $_POST['quema_arco_ext_text'];
        $corr_int_text = $_POST['corr_int_text'];
        $pica_int_text = $_POST['pica_int_text'];
        $grasa_aceite_text = $_POST['grasa_aceite_text'];        
        $remo_pintura_text = $_POST['remo_pintura_text'];
        $remo_etiquetas_text = $_POST['remo_etiquetas_text'];        
        $adul_info_text = $_POST['adul_info_text'];
        $corr_ext_text = $_POST['corr_ext_text'];
        $fisu_lineal_text = $_POST['fisu_lineal_text'];
        $corr_gene_text = $_POST['corr_gene_text'];
        $pica_ais_text = $_POST['pica_ais_text'];
        $abolladuras_text = $_POST['abolladuras_text'];
        $abombamiento_text = $_POST['abombamiento_text'];
        $martillo_text = $_POST['martillo_text'];
        $rotura_text = $_POST['rotura_text'];
        $dobleces_text = $_POST['dobleces_text'];
        $valles_text = $_POST['valles_text'];
        $hilos_roscas_text = $_POST['hilos_roscas_text'];
        $hilos_roscas_text = $_POST['hilos_roscas_text'];

        if($estado_acero == APROBADO){
          $estado_prueba = 1;
        }else if($estado_acero == RECHAZADO){
          $estado_prueba = 2;
        }
        
        $observaciones = $_POST['observaciones'];

        $consulta12 = "SELECT num_cili, ph_u, llenado_u, pintura_u, valvula_u, granallado_u, cambio_serv_u, lavado_especial_u FROM has_movimiento_cilindro_pev WHERE id_has_movimiento_cilindro_pev = $id_has_movimiento_cilindro_pev";
        $resultado12 = mysqli_query($con,$consulta12);
        $linea12 = mysqli_fetch_assoc($resultado12);

        $num_cili_12 = isset($linea12["num_cili"]) ? $linea12["num_cili"] : NULL;
        $ph_u_12 = isset($linea12["ph_u"]) ? $linea12["ph_u"] : NULL;
        $llenado_u_12 = isset($linea12["llenado_u"]) ? $linea12["llenado_u"] : NULL;
        $pintura_u_12 = isset($linea12["pintura_u"]) ? $linea12["pintura_u"] : NULL;
        $valvula_u_12 = isset($linea12["valvula_u"]) ? $linea12["valvula_u"] : NULL;
        $granallado_u_12 = isset($linea12["granallado_u"]) ? $linea12["granallado_u"] : NULL;
        $cambio_serv_u_12 = isset($linea12["cambio_serv_u"]) ? $linea12["cambio_serv_u"] :  NULL;
        $lavado_especial_u_12 = isset($linea12["lavado_especial_u"]) ? $linea12["lavado_especial_u"] : NULL;

        $consulta13 = "SELECT id_cilindro_eto FROM cilindro_eto WHERE num_cili_eto = '$num_cili_12'";
        $resultado13 = mysqli_query($con,$consulta13);
        $linea13 = mysqli_fetch_assoc($resultado13);

        $id_cilindro_eto_13 = isset($linea13["id_cilindro_eto"]) ? $linea13["id_cilindro_eto"] : NULL;

        if($ph_u_12 == 1){
          $id_estado_13 = 5;
        }else if($granallado_u_12 == 1){
          $id_estado_13 = 7;
        }else if($lavado_especial_u_12 == 1){
          $id_estado_13 = 12;
        }else if($pintura_u_12 == 1){
          $id_estado_13 = 6;
        }else if ($llenado_u_12 == 1) {
          $id_estado_13 = 13;
        }else if ($cambio_serv_u_12 == 1) {
          $id_estado_13 = 8;
        }else if ($cambio_serv_u_12 == 1) {
          $id_estado_13 = 4;
        }

        $consulta14 = "UPDATE cilindro_eto SET id_estado = $id_estado_13 WHERE id_cilindro_eto = $id_cilindro_eto_13";
        $resultado14 = mysqli_query($con,$consulta14);

        $consulta2 = "SELECT id_has_movimiento_cilindro_pev FROM inspeccion_ace WHERE id_has_movimiento_cilindro_pev = '".$id_has_movimiento_cilindro_pev."'";
        $resultado2 = mysqli_query($con,$consulta2);
        if(mysqli_num_rows($resultado2) > 0 ) //se actualizan parametros de inspeccion
        {
            $consulta3 = "UPDATE inspeccion_ace SET
                                 estado_acero = '".$estado_acero."',
                                 quema_arco = '".$quema_arco."',
                                 quema_arco_ext = '".$quema_arco_ext."',
                                 corr_int = '".$corr_int."',
                                 pica_int = '".$pica_int."',
                                 grasa_aceite = '".$grasa_aceite."',
                                 remo_pintura = '".$remo_pintura."',
                                 remo_etiquetas = '".$remo_etiquetas."',                                 
                                 adul_info = '".$adul_info."',
                                 corr_ext = '".$corr_ext."',
                                 fisu_lineal = '".$fisu_lineal."',
                                 corr_gene = '".$corr_gene."',
                                 pica_ais = '".$pica_ais."',
                                 abolladuras = '".$abolladuras."',
                                 abombamiento = '".$abombamiento."',
                                 martillo = '".$martillo."',
                                 rotura = '".$rotura."',
                                 dobleces = '".$dobleces."',
                                 valles = '".$valles."',
                                 hil_ros = '".$hil_ros."',
                                 hilos_roscas = '".$hilos_roscas."',

                                 quema_arco_text= '".$quema_arco_text."',
                                 quema_arco_ext_text= '".$quema_arco_ext_text."',
                                 corr_int_text = '".$corr_int_text."',
                                 pica_int_text = '".$pica_int_text."',
                                 grasa_aceite_text = '".$grasa_aceite_text."',
                                 remo_pintura_text = '".$remo_pintura_text."',
                                 remo_etiquetas_text = '".$remo_etiquetas_text."',
                                 adul_info_text = '".$adul_info_text."',
                                 corr_ext_text = '".$corr_ext_text."',
                                 fisu_lineal_text = '".$fisu_lineal_text."',
                                 corr_gene_text = '".$corr_gene_text."',
                                 pica_ais_text = '".$pica_ais_text."',
                                 abolladuras_text = '".$abolladuras_text."',
                                 abombamiento_text = '".$abombamiento_text."',
                                 martillo_text = '".$martillo_text."',
                                 rotura_text = '".$rotura_text."',
                                 dobleces_text = '".$dobleces_text."',
                                 valles_text = '".$valles_text."',
                                 hilos_roscas_text = '".$hilos_roscas_text."',
                                 estado_prueba = '".$estado_prueba."',
                                 observaciones = '".$observaciones."'
                                 WHERE id_has_movimiento_cilindro_pev = '".$id_has_movimiento_cilindro_pev."'";
            $resultado3 = mysqli_query($con,$consulta3);

            if($resultado3 == FALSE)
            {
                echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
            }
            else
            {
                ?>
                <script type="text/javascript">
                    alert("Inspección actualizada correctamente.");
                    window.location = 'insp_vi_ace.php?id_has_movimiento_cilindro_pev='+<?php echo $id_has_movimiento_cilindro_pev; ?>;
                </script>
                <?php
            }
        }
        else
        {
            $consulta1 = "INSERT INTO inspeccion_ace (estado_acero,
                                                      quema_arco,
                                                      quema_arco_ext,
                                                      corr_int,
                                                      pica_int,
                                                      grasa_aceite,
                                                      remo_pintura,
                                                      remo_etiquetas,
                                                      adul_info,
                                                      corr_ext,
                                                      fisu_lineal,
                                                      corr_gene,
                                                      pica_ais,
                                                      abolladuras,
                                                      abombamiento,
                                                      martillo,
                                                      rotura,
                                                      dobleces,
                                                      valles,
                                                      hil_ros,
                                                      hilos_roscas,
                                                      observaciones,
                                                      id_has_movimiento_cilindro_pev,
                                                      quema_arco_text,
                                                      quema_arco_ext_text,
                                                      corr_int_text,
                                                      pica_int_text,
                                                      grasa_aceite_text,
                                                      remo_pintura_text,
                                                      remo_etiquetas_text,
                                                      adul_info_text,
                                                      corr_ext_text,
                                                      fisu_lineal_text,
                                                      corr_gene_text,
                                                      pica_ais_text,
                                                      abolladuras_text,
                                                      abombamiento_text,
                                                      martillo_text,
                                                      rotura_text,
                                                      dobleces_text,
                                                      valles_text,
                                                      hilos_roscas_text,
                                                      estado_prueba)
                        VALUES ('".$estado_acero."',
                                '".$quema_arco."',
                                '".$quema_arco_ext."',
                                '".$corr_int."',
                                '".$pica_int."',
                                '".$grasa_aceite."',
                                '".$remo_pintura."',
                                '".$remo_etiquetas."',
                                '".$adul_info."',
                                '".$corr_ext."',
                                '".$fisu_lineal."',
                                '".$corr_gene."',
                                '".$pica_ais."',
                                '".$abolladuras."',
                                '".$abombamiento."',
                                '".$martillo."',
                                '".$rotura."', 
                                '".$dobleces."',
                                '".$valles."',
                                '".$hil_ros."',
                                '".$hilos_roscas."',
                                '".$observaciones."',
                                '".$id_has_movimiento_cilindro_pev."',
                                '".$quema_arco_text."',
                                '".$quema_arco_ext_text."',
                                '".$corr_int_text."',
                                '".$pica_int_text."',
                                '".$grasa_aceite_text."',
                                '".$remo_pintura_text."',
                                '".$remo_etiquetas_text."',
                                '".$adul_info_text."',
                                '".$corr_ext_text."',
                                '".$fisu_lineal_text."',
                                '".$corr_gene_text."',
                                '".$pica_ais_text."',
                                '".$abolladuras_text."',
                                '".$abombamiento_text."',
                                '".$martillo_text."',
                                '".$rotura_text."',
                                '".$dobleces_text."',
                                '".$valles_text."',
                                '".$hilos_roscas_text."',
                                '".$estado_prueba."')";
            if(mysqli_query($con,$consulta1))
            {
                ?>
                <script type="text/javascript">
                    var id_orden_pev = '<?php echo $id_orden_pev; ?>';
                    alert("Inspección guardada correctamente.")
                    window.location = 'insp_vi_ace.php?id_has_movimiento_cilindro_pev='+<?php echo $id_has_movimiento_cilindro_pev; ?>;
                </script>
                <?php
            }
            else
            {
                echo "Error: " . $consulta1 . "<br>" . mysqli_error($con);
            }
        }
    }
    //Consulta el responsable de la inspección
    $consulta = "SELECT Name, LastName FROM user WHERE idUser = '".$idUser."'";
    $resultado = mysqli_query($con,$consulta);
    if(mysqli_num_rows($resultado) > 0 )
    {
        $linea = mysqli_fetch_assoc($resultado);
        $Name = $linea['Name'];
        $LastName = $linea['LastName'];
        $responsable = $Name." ".$LastName;
    }
    //Si existe el cilindro se consultan datos
    if(strlen($id_has_movimiento_cilindro_pev) > 0)
    {
        $consulta3 = "SELECT num_cili, id_tipo_gas_pev, id_transporte_pev
                      FROM has_movimiento_cilindro_pev 
                      WHERE id_has_movimiento_cilindro_pev = '".$id_has_movimiento_cilindro_pev."'";
        $resultado3 = mysqli_query($con,$consulta3);
        if(mysqli_num_rows($resultado3) > 0)
        {
            $linea3 = mysqli_fetch_assoc($resultado3);
            $num_cili = $linea3['num_cili'];
            $id_tipo_gas_pev = $linea3['id_tipo_gas_pev'];
            $id_transporte_pev = $linea3['id_transporte_pev'];

            $consulta10 = "SELECT esp_fab_eto, esp_actu_eto FROM cilindro_eto WHERE num_cili_eto = '".$num_cili."'";
            $resultado10 = mysqli_query($con,$consulta10);
            if(mysqli_num_rows($resultado10) > 0)
            {
                $linea10 = mysqli_fetch_assoc($resultado10);
                $esp_fab_eto = $linea10['esp_fab_eto'];
                $esp_actu_eto = $linea10['esp_actu_eto'];
            }
            $consulta4 = "SELECT id_cliente,fecha FROM transporte_pev WHERE id_transporte_pev = '".$id_transporte_pev."'";
            $resultado4 = mysqli_query($con,$consulta4);
            if(mysqli_num_rows($resultado4) > 0)
            {
                $linea4 = mysqli_fetch_assoc($resultado4);
                $fecha = $linea4['fecha'];
                $id_cliente = $linea4['id_cliente'];

                $consulta8 = "SELECT nombre FROM clientes WHERE id_cliente = '".$id_cliente."'";
                $resultado8 = mysqli_query($con,$consulta8);
                if(mysqli_num_rows($resultado8) > 0)
                {
                    $linea8 = mysqli_fetch_assoc($resultado8);
                    $nombre_cliente = $linea8['nombre'];
                }
            }
            $consulta5 = "SELECT nombre FROM tipo_gas_pev WHERE id_tipo_gas_pev = '".$id_tipo_gas_pev."'";
            $resultado5 = mysqli_query($con,$consulta5);
            if(mysqli_num_rows($resultado5) > 0)
            {
                $linea5 = mysqli_fetch_assoc($resultado5);
                $nombre = $linea5['nombre'];
            }            
            
            $consulta6 = "SELECT id_especi_eto, especi_eto, num_cili_eto_2 FROM cilindro_eto WHERE num_cili_eto = '".$num_cili."'";
            $resultado6 = mysqli_query($con,$consulta6);
            if(mysqli_num_rows($resultado6) > 0)
            {
                $linea6 = mysqli_fetch_assoc($resultado6);
                $id_especi_eto = $linea6['id_especi_eto'];
                $especi_eto = $linea6['especi_eto'];
                $num_cili_eto_2 = $linea6['num_cili_eto_2'];

                $consulta7 = "SELECT especificacion FROM especificacion WHERE id_especificacion = '".$id_especi_eto."'";
                $resultado7 = mysqli_query($con,$consulta7);
                if(mysqli_num_rows($resultado7))
                {
                    $linea7 = mysqli_fetch_assoc($resultado7);
                    $especificacion = $linea7['especificacion'];
                }
            }
            $consulta11 = "SELECT * FROM inspeccion_ace WHERE id_has_movimiento_cilindro_pev = '".$id_has_movimiento_cilindro_pev."'";
            $resultado11 = mysqli_query($con,$consulta11);
            if(mysqli_num_rows($resultado11) > 0)
            {
                $linea11 = mysqli_fetch_assoc($resultado11);
                $estado_acero = $linea11['estado_acero'];
                $quema_arco = $linea11['quema_arco'];
                $quema_arco_ext = $linea11['quema_arco_ext'];
                $corr_int = $linea11['corr_int'];
                $pica_int = $linea11['pica_int'];
                $grasa_aceite = $linea11['grasa_aceite'];        
                $remo_pintura = $linea11['remo_pintura'];
                $remo_etiquetas = $linea11['remo_etiquetas'];        
                $adul_info = $linea11['adul_info'];
                $corr_ext = $linea11['corr_ext'];
                $fisu_lineal = $linea11['fisu_lineal'];
                $corr_gene = $linea11['corr_gene'];
                $pica_ais = $linea11['pica_ais'];
                $abolladuras = $linea11['abolladuras'];
                $abombamiento = $linea11['abombamiento'];
                $martillo = $linea11['martillo'];
                $rotura = $linea11['rotura'];
                $dobleces = $linea11['dobleces'];
                $valles = $linea11['valles'];
                $hil_ros = $linea11['hil_ros'];
                $hilos_roscas = $linea11['hilos_roscas'];
                $observaciones = $linea11['observaciones'];

                $quema_arco_text = $linea11['quema_arco_text'];
                $quema_arco_ext_text = $linea11['quema_arco_ext_text'];
                $corr_int_text = $linea11['corr_int_text'];
                $pica_int_text = $linea11['pica_int_text'];
                $grasa_aceite_text = $linea11['grasa_aceite_text'];        
                $remo_pintura_text = $linea11['remo_pintura_text'];
                $remo_etiquetas_text = $linea11['remo_etiquetas_text'];        
                $adul_info_text = $linea11['adul_info_text'];
                $corr_ext_text = $linea11['corr_ext_text'];
                $fisu_lineal_text = $linea11['fisu_lineal_text'];
                $corr_gene_text = $linea11['corr_gene_text'];
                $pica_ais_text = $linea11['pica_ais_text'];
                $abolladuras_text = $linea11['abolladuras_text'];
                $abombamiento_text = $linea11['abombamiento_text'];
                $martillo_text = $linea11['martillo_text'];
                $rotura_text = $linea11['rotura_text'];
                $dobleces_text = $linea11['dobleces_text'];
                $valles_text = $linea11['valles_text'];
                $hilos_roscas_text = $linea11['hilos_roscas_text'];
                $hilos_roscas_text = $linea11['hilos_roscas_text'];
                $estado_prueba = $linea11['estado_prueba'];
                /*if($estado_acero == 1)
                {
                    $estado_acero = "APROBADO";
                }
                else
                {
                    if($estado_acero == 2)
                    {
                        $estado_acero == "NO APROBADO";
                    }
                    else
                    {
                        $estado_acero = "DESCONOCIDO";
                    }
                }*/
            }
        }

    }
?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
    <div id="content">
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-md-6 col-md-offset-3">
                    <div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                            <h2>INSPECCIÓN VISUAL CILINDRO DE ACERO</h2>
                        </header>
                        <div>
                            <div class="jarviswidget-editbox">
                            </div>
                            <div class="widget-body no-padding">                                
                                <form name="mezcla" action="insp_vi_ace.php" method="POST">
                                    <input type="hidden" name="id_has_movimiento_cilindro_pev" value="<?php echo $id_has_movimiento_cilindro_pev; ?>">
                                    <div class="well well-sm well-primary">
                                      <fieldset>
                                        <legend><center>IDENTIFICACIÓN DEL CILINDRO</center></legend>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="category">N° Cilindro<br>Ingreso:</label>
                                                    <input type="text" class="form-control" placeholder="Numero Cilindro" name="num_cili" readonly value="<?php echo isset($num_cili) ? $num_cili : NULL; ?>"/>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="category">N° Cilindro<br> Verificado:</label>
                                                    <input type="text" class="form-control" placeholder="Cilindro Verificado" name="num_cili_eto_2" readonly value="<?php echo isset($num_cili_eto_2) ? $num_cili_eto_2 : NULL; ?>"/>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="category">Fecha de<br> Ingreso:</label>
                                                    <input type="text" class="form-control" placeholder="Fecha Ingreso" name="fecha" readonly value="<?php echo isset($fecha) ? $fecha : NULL; ?>"/>
                                                </div>
                                            </div>
                                            <div class="col-md-3"> 
                                                <div class="form-group">
                                                    <label for="category">Fecha de Inspección:</label>
                                                    <input type="text" class="form-control" placeholder="Fecha Inspección" name="fecha_inspeccion_ac" readonly value="<?php echo $Fecha ?>"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="category">Tipo de gas:</label>
                                                    <input type="text" class="form-control" placeholder="Tipo de GAS" name="nombre" readonly value="<?php echo isset($nombre) ? $nombre : NULL; ?>"/>
                                                </div>
                                            </div>                                            
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="category">Especificación:</label>
                                                    <input type="text" class="form-control" placeholder="Especificación" name="especificacion" readonly value="<?php echo isset($especificacion) ? $especificacion : NULL; ?>"/>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                  <label for="category">Descripción:</label>
                                                    <input type="text" class="form-control" placeholder="Especificación" name="especi_eto" readonly value="<?php echo isset($especi_eto) ? $especi_eto : NULL; ?>"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                  <label for="category">Aprobación / Rechazo:</label>
                                                    <input type="text" class="form-control" placeholder="ESTADO" name="estado_acero" id="p1" readonly value="<?php echo isset($estado_acero) ? $estado_acero : NULL; ?>"/>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="category">Espesor de fabricación :</label>
                                                    <input type="text" class="form-control" placeholder="Espesor de fabricación" name="esp_fab_eto" readonly value="<?php echo isset($esp_fab_eto) ? $esp_fab_eto : NULL; ?>"/>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="category">Espesor de pared actual :</label>
                                                    <input type="text" class="form-control" placeholder="Espesor de pared actual" name="esp_actu_eto" readonly value="<?php echo isset($esp_actu_eto) ? $esp_actu_eto : NULL; ?>"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                  <label for="category">Responsable:</label>
                                                    <input type="text" class="form-control" placeholder="Responsable" name="responsable" readonly value="<?php echo isset($responsable) ? $responsable : NULL; ?>"/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="category">Cliente:</label>
                                                    <input type="text" class="form-control" placeholder="Cliente" name="nombre_cliente" readonly value="<?php echo isset($nombre_cliente) ? $nombre_cliente : NULL; ?>"/>
                                                </div>
                                            </div>
                                        </div>
                                      </fieldset>
                                    </div>    
                                        <div class="well well-sm well-primary">
                                            <fieldset>
                                            <legend><center>INSPECCIÓN VISUAL INTERNA</center></legend>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label for="category">Quemaduras por arco y antorcha :</label>                            
                                                        </div>  
                                                    </div>   
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <div class="inline-group">
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="quema_arco" value="1" id="quema_arco1" onclick="check()" <?php if($quema_arco=='1') print "checked=true"?>>
                                                                <i></i>Si</label>
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="quema_arco" value="2" id="quema_arco2" onclick="check()" <?php if($quema_arco=='2') print "checked=true"?>>
                                                                <i></i>No</label>
                                                                <label class="input">
                                                                    <input type="text" name="quema_arco_text" placeholder="Observación" value="<?php echo isset($quema_arco_text) ? $quema_arco_text : NULL; ?>">
                                                                </label>                    
                                                            </div>
                                                        </div>  
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label for="category">Corrosión interna :</label>                            
                                                        </div>  
                                                    </div>   
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <div class="inline-group">
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="corr_int" id="corr_int1" value="1" onclick="check()" <?php if($corr_int=='1') print "checked=true"?> >
                                                                <i></i>Si</label>
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="corr_int" id="corr_int2" value="2" onclick="check()" <?php if($corr_int=='2') print "checked=true"?> >
                                                                <i></i>No</label>
                                                                 <label class="input">
                                                                    <input type="text" name="corr_int_text" placeholder="Observación" value="<?php echo isset($corr_int_text) ? $corr_int_text : NULL; ?>">
                                                                </label>                     
                                                            </div>
                                                        </div>  
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label for="category">Picaduras internas :</label>                            
                                                        </div>  
                                                    </div>   
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <div class="inline-group">
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="pica_int" id="pica_int1"  value="1" <?php if($pica_int=='1') print "checked=true"?> >
                                                                <i></i>Si</label>
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="pica_int" id="pica_int2" value="2" <?php if($pica_int=='2') print "checked=true"?> >
                                                                <i></i>No</label>
                                                                 <label class="input">
                                                                    <input type="text" name="pica_int_text" placeholder="Observación" value="<?php echo isset($pica_int_text) ? $pica_int_text : NULL; ?>">
                                                                </label>                   
                                                            </div>
                                                        </div>  
                                                    </div>
                                                </div>  
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label for="category">Cilindro con grasa o aceite:</label>                            
                                                        </div>  
                                                    </div>   
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <div class="inline-group">
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="grasa_aceite" value="1" id="grasa_aceite1" onclick="check();" <?php if($grasa_aceite=='1') print "checked=true"?>>
                                                                <i></i>Si</label>
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="grasa_aceite" value="2" id="grasa_aceite2" onclick="check();" <?php if($grasa_aceite=='2') print "checked=true"?>>
                                                                <i></i>No</label>
                                                                <label class="input">
                                                                  <input type="text" name="grasa_aceite_text" placeholder="Observación" value="<?php echo isset($grasa_aceite_text) ? $grasa_aceite_text : NULL; ?>">
                                                                </label>
                                                                <input type="text" class="form-control" name="lavado_especial" id="lavado_especial" readonly value="<?php echo isset($lavado_especial) ? $lavado_especial : NULL; ?>"/>
                                                            </div>
                                                        </div>  
                                                    </div>
                                                </div>
                                              </fieldset>
                                            </div>
                                            <div class="well well-sm well-primary">
                                                <fieldset>
                                                <legend><center>INSPECCIÓN VISUAL EXTERNA</center></legend>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label for="category">Quemaduras por arco y antorcha :</label>                            
                                                        </div>  
                                                    </div>   
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <div class="inline-group">
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="quema_arco_ext" value="1" id="quema_arco_ext1" onclick="check();" <?php if($quema_arco_ext=='1') print "checked=true"?>>
                                                                <i></i>Si</label>
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="quema_arco_ext" value="2" id="quema_arco_ext2" onclick="check();" <?php if($quema_arco_ext=='2') print "checked=true"?>>
                                                                <i></i>No</label>
                                                                <label class="input">
                                                                    <input type="text" name="quema_arco_ext_text" placeholder="Observación" value="<?php echo isset($quema_arco_ext_text) ? $quema_arco_ext_text : NULL; ?>">
                                                                </label>                    
                                                            </div>
                                                        </div>  
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label for="category">Remoción total de pintura :</label>                            
                                                        </div>  
                                                    </div>   
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <div class="inline-group">
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="remo_pintura" id="remo_pintura1" value="1" <?php if($remo_pintura=='1') print "checked=true"?> >
                                                                <i></i>Si</label>
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="remo_pintura" id="remo_pintura2" value="2" <?php if($remo_pintura=='2') print "checked=true"?> >
                                                                <i></i>No</label>
                                                                 <label class="input">
                                                                    <input type="text" name="remo_pintura_text" placeholder="Observación" value="<?php echo isset($remo_pintura_text) ? $remo_pintura_text : NULL; ?>">
                                                                </label>
                                                            </div>
                                                        </div>  
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label for="category">Remoción de etiquetas :</label>                            
                                                        </div>  
                                                    </div>   
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <div class="inline-group">
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="remo_etiquetas" id="remo_etiquetas1" value="1" <?php if($remo_etiquetas=='1') print "checked=true"?> >
                                                                <i></i>Si</label>
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="remo_etiquetas" id="remo_etiquetas2" value="2" <?php if($remo_etiquetas=='2') print "checked=true"?> >
                                                                <i></i>No</label>
                                                                 <label class="input">
                                                                  <input type="text" name="remo_etiquetas_text" placeholder="Observación" value="<?php echo isset($remo_etiquetas_text) ? $hil_ros : NULL; ?>">
                                                                </label>
                                                            </div>
                                                        </div>  
                                                    </div>
                                                </div>
                                                </fieldset>                   
                                                <fieldset>  
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label for="category">Adulteración de la información :</label>
                                                        </div>
                                                    </div>   
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <div class="inline-group">
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="adul_info" id="adul_info1"  value="1" onclick="check();" <?php if($adul_info=='1') print "checked=true"?> >
                                                                <i></i>Si</label>
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="adul_info" id="adul_info2" value="2" onclick="check();" <?php if($adul_info=='2') print "checked=true"?> >
                                                                <i></i>No</label>
                                                                 <label class="input">
                                                                    <input type="text" name="adul_info_text" placeholder="Observación" value="<?php echo isset($adul_info_text) ? $adul_info_text : NULL; ?>">
                                                                </label>                    
                                                            </div>
                                                        </div>  
                                                    </div>
                                                </div>
                                                </fieldset>
                                                <fieldset>  
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label for="category">Corrosión Externa :</label>                            
                                                        </div>  
                                                    </div>   
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <div class="inline-group">
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="corr_ext" id="corr_ext1" value="1" onclick="check();" <?php if($corr_ext=='1') print "checked=true"?> >
                                                                <i></i>Si</label>
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="corr_ext" id="corr_ext2" value="2" onclick="check();" <?php if($corr_ext=='2') print "checked=true"?> >
                                                                <i></i>No</label>
                                                                 <label class="input">
                                                                    <input type="text" name="corr_ext_text" placeholder="Observación" value="<?php echo isset($corr_ext_text) ? $corr_ext_text : NULL; ?>">
                                                                </label>
                                                            </div>
                                                        </div>  
                                                    </div>
                                                </div>
                                                </fieldset> 
                                                <fieldset>  
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label for="category">Fisura y corrosión lineal :</label>                            
                                                        </div>  
                                                    </div>   
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <div class="inline-group">
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="fisu_lineal" id="fisu_lineal1" value="1" onclick="check();" <?php if($fisu_lineal=='1') print "checked=true"?> >
                                                                <i></i>Si</label>
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="fisu_lineal" id="fisu_lineal2" value="2" onclick="check();" <?php if($fisu_lineal=='2') print "checked=true"?> >
                                                                <i></i>No</label>
                                                                 <label class="input">
                                                                    <input type="text" name="fisu_lineal_text" placeholder="Observación" value="<?php echo isset($fisu_lineal_text) ? $fisu_lineal_text : NULL; ?>">
                                                                </label>
                                                            </div>
                                                        </div>  
                                                    </div>
                                                </div>
                                                </fieldset> 
                                                <fieldset> 
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label for="category">Corrosión General :</label>                            
                                                        </div>  
                                                    </div>   
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <div class="inline-group">
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="corr_gene" id="corr_gene1" value="1" onclick="check();" <?php if($corr_gene=='1') print "checked=true"?> >
                                                                <i></i>Si</label>
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="corr_gene" id="corr_gene2" value="2" onclick="check();" <?php if($corr_gene=='2') print "checked=true"?> >
                                                                <i></i>No</label>
                                                                 <label class="input">
                                                                    <input type="text" name="corr_gene_text" placeholder="Observación" value="<?php echo isset($corr_gene_text) ? $corr_gene_text : NULL; ?>">
                                                                </label>
                                                            </div>
                                                        </div>  
                                                    </div>
                                                </div>
                                                </fieldset> 
                                                <fieldset>  
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label for="category">Picaduras aisladas :</label>                            
                                                        </div>  
                                                    </div>   
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <div class="inline-group">
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="pica_ais" id="pica_ais1" value="1" onclick="check();" <?php if($pica_ais=='1') print "checked=true"?> >
                                                                <i></i>Si</label>
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="pica_ais" id="pica_ais2" value="2" onclick="check();" <?php if($pica_ais=='2') print "checked=true"?> >
                                                                <i></i>No</label>
                                                                 <label class="input">
                                                                    <input type="text" name="pica_ais_text" placeholder="Observación" value="<?php echo isset($pica_ais_text) ? $pica_ais_text : NULL; ?>">
                                                                </label>
                                                            </div>
                                                        </div>  
                                                    </div>
                                                </div>
                                                </fieldset>
                                                <fieldset>  
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label for="category">Abolladuras :</label>                            
                                                        </div>  
                                                    </div>   
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <div class="inline-group">
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="abolladuras" id="abolladuras1" value="1" onclick="check();" <?php if($abolladuras=='1') print "checked=true"?> >
                                                                <i></i>Si</label>
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="abolladuras" id="abolladuras2" value="2" onclick="check();" <?php if($abolladuras=='2') print "checked=true"?> >
                                                                <i></i>No</label>
                                                                 <label class="input">
                                                                    <input type="text" name="abolladuras_text" placeholder="Observación" value="<?php echo isset($abolladuras_text) ? $abolladuras_text : NULL; ?>">
                                                                </label>
                                                            </div>
                                                        </div>  
                                                    </div>
                                                </div>
                                                </fieldset> 
                                                <fieldset>  
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label for="category">Abombamiento :</label>                            
                                                        </div>  
                                                    </div>   
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <div class="inline-group">
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="abombamiento" id="abombamiento1" value="1" onclick="check();" <?php if($abombamiento=='1') print "checked=true"?> >
                                                                <i></i>Si</label>
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="abombamiento" id="abombamiento2" value="2" onclick="check();" <?php if($abombamiento=='2') print "checked=true"?> >
                                                                <i></i>No</label>
                                                                 <label class="input">
                                                                    <input type="text" name="abombamiento_text" placeholder="Observación" value="<?php echo isset($abombamiento_text) ? $abombamiento_text : NULL; ?>">
                                                                </label>
                                                            </div>
                                                        </div>  
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label for="category">Prueba de martillo:</label>                            
                                                        </div>  
                                                    </div>   
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <div class="inline-group">
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="martillo" id="martillo1" value="1" onclick="check();" <?php if($martillo=='1') print "checked=true"?> >
                                                                <i></i>Agudo</label>
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="martillo" id="martillo2" value="2" onclick="check();" <?php if($martillo=='2') print "checked=true"?> >
                                                                <i></i>Grave</label>
                                                                 <label class="input">
                                                                  <input type="text" name="martillo_text" placeholder="Observación" value="<?php echo isset($martillo_text) ? $martillo_text : NULL; ?>">
                                                                </label>
                                                            </div>
                                                        </div>  
                                                    </div>
                                                </div>
                                                </fieldset>
                                        </div>
                                        <div class="well well-sm well-primary">
                                            <fieldset>
                                            <legend><center>DEFECTOS DEL CUELLO</center></legend>  
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="category">Fractura:</label>                            
                                                    </div>  
                                                </div>   
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="inline-group">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="rotura" id="rotura1" value="1" <?php if($rotura=='1') print "checked=true"?> >
                                                            <i></i>Si</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="rotura" id="rotura2" value="2" <?php if($rotura=='2') print "checked=true"?> >
                                                            <i></i>No</label>
                                                             <label class="input">
                                                                <input type="text" name="rotura_text" placeholder="Observación" value="<?php echo isset($rotura_text) ? $rotura_text : NULL; ?>">
                                                            </label>
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>     
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="category">Rosca Dañada:</label>                            
                                                    </div>  
                                                </div>   
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="inline-group">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="dobleces" id="dobleces1" value="1" <?php if($dobleces=='1') print "checked=true"?> >
                                                            <i></i>Si</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="dobleces" id="dobleces2" value="2" <?php if($dobleces=='2') print "checked=true"?> >
                                                            <i></i>No</label>
                                                            <label class="input">
                                                                <input type="text" name="dobleces_text" placeholder="Observación" value="<?php echo isset($dobleces_text) ? $dobleces_text : NULL; ?>">
                                                            </label>
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="category">Cuello Suelto:</label>                            
                                                    </div>  
                                                </div>   
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="inline-group">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="valles" id="valles1" value="1" <?php if($valles=='1') print "checked=true"?> >
                                                            <i></i>Si</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="valles" id="valles2" value="2" <?php if($valles=='2') print "checked=true"?> >
                                                            <i></i>No</label>
                                                            <label class="input">
                                                              <input type="text" name="valles_text" placeholder="Observación" value="<?php echo isset($valles_text) ? $valles_text : NULL; ?>">
                                                            </label>
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>
                                            </fieldset>
                                            <fieldset>
                                            <legend><center>ESTADO DE LA ROSCA</center></legend>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="category"># Hilos de la rosca :</label>
                                                    </div>  
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" placeholder="Cantidad" name="hil_ros" id="hil_ros1" required value="<?php echo isset($hil_ros) ? $hil_ros : NULL; ?>"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="category">Estado Hilos de la Rosca :</label>
                                                    </div>  
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="inline-group">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="hilos_roscas" id="hilos_roscas1" value="2" onclick="check();" <?php if($hilos_roscas=='2') print "checked=true"?> >
                                                            <i></i>Buenos</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="hilos_roscas" id="hilos_roscas2" value="1" onclick="check();" <?php if($hilos_roscas=='1') print "checked=true"?> >
                                                            <i></i>Dañados</label>
                                                             <label class="input">
                                                                <input type="text" name="hilos_roscas_text" placeholder="Observación" value="<?php echo isset($hilos_roscas_text) ? $hilos_roscas_text : NULL; ?>">
                                                            </label>                    
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>                                       
                                            </fieldset>
                                        </div>  
                                        <div class="well well-sm well-primary">     
                                            <div class="row">
                                              <div class="form-group">
                                                  <label for="category">Observaciones Generales:</label>
                                                  <textarea class="form-control" name="observaciones"><?php echo $observaciones; ?></textarea>
                                              </div>
                                            </div>
                                        </div> 
                                        <div class="modal-footer">
                                            <?php
                                            if (in_array(22, $acc))
                                            {
                                            ?>
                                              <input type="submit" value="Guardar" name="g_insp_visual_acero" id="g_insp_visual_acero" class="btn btn-primary" />
                                            <?php
                                            }                   
                                            ?>
                                        </div>
                                  </form>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>
</div>

<!-- END MAIN PANEL -->

<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->
<?php
    include("inc/footer.php");
?>
<!-- END PAGE FOOTER -->

<?php 
    //include required scripts
    include("inc/scripts.php"); 
?>


<!-- PAGE RELATED PLUGIN(S) 
<script src="..."></script>-->
<!-- Flot Chart Plugin: Flot Engine, Flot Resizer, Flot Tooltip -->
<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>

<!-- Vector Maps Plugin: Vectormap engine, Vectormap language -->
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>

<!-- Full Calendar -->
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/jquery-form/jquery-form.min.js"></script>

<script type="text/javascript">
    function check()
    {
      if(document.getElementById("corr_ext1").checked == true || 
         document.getElementById("fisu_lineal1").checked == true || 
         document.getElementById("corr_gene1").checked == true || 
         document.getElementById("pica_ais1").checked == true || 
         document.getElementById("abolladuras1").checked == true || 
         document.getElementById("quema_arco1").checked == true || 
         document.getElementById("quema_arco_ext1").checked == true || 
         document.getElementById("abombamiento1").checked == true ||
         document.getElementById("adul_info1").checked == true || 
         document.getElementById("grasa_aceite1").checked == true || 
         document.getElementById("corr_int1").checked == true ||
         document.getElementById("hilos_roscas2").checked == true ||
         document.getElementById("martillo2").checked == true)        
        {
           document.getElementById("p1").value = "NO APROBADO";
           //document.getElementById("p1").innerHTML = "NO APROBADO";
           check_1();
        }
        else
        {
          document.getElementById("p1").value = "APROBADO";
          //document.getElementById("p1").innerHTML = "APROBADO";
          check_1();
        }
    }
    
</script>
<script type="text/javascript">
    function check_1()
    {
      if(document.getElementById("grasa_aceite1").checked == true)        
        {
           document.getElementById("lavado_especial").value = "LAVADO ESPECIAL";
        }
        else
        {
          document.getElementById("lavado_especial").value = "";
          //document.getElementById("p1").innerHTML = "APROBADO";
        }
    }    
</script>
<?php 
    //include footer
    include("inc/google-analytics.php"); 
}
else
{
    header("Location:index.php");
}
?>