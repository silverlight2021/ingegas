<?php
session_start();
date_default_timezone_set("America/Bogota");
$Fecha = date("Y-m-d");
require ("libraries/conexion.php");

$estado_prueba = 0;
if($_SESSION['logged']== 'yes')
{ 
    $id_has_movimiento_cilindro_pev = isset($_REQUEST['id_has_movimiento_cilindro_pev']) ? $_REQUEST['id_has_movimiento_cilindro_pev'] : NULL;
    $idUser =$_SESSION['su'];
    $acc = $_SESSION['acc'];

    require_once("inc/init.php");
    require_once("inc/config.ui.php");
    $page_title = "Inspección Visual Acumulador";
    $page_css[] = "your_style.css";
    include("inc/header.php");
    $page_nav[""][""][""][""] = true;
    include("inc/nav.php");

    if (isset($_POST['g_insp_visual_acero'])) //guardar información de inspección visual de acero
    {
        $id_has_movimiento_cilindro_pev = $_POST['id_has_movimiento_cilindro_pev'];
        $estado_acero = $_POST['estado_acero'];
        $quema_arco = $_POST['quema_arco'];
        $corr_int = $_POST['corr_int'];
        $pica_int = $_POST['pica_int'];
        $grasa_aceite = $_POST['grasa_aceite'];        
        $remo_pintura = $_POST['remo_pintura'];
        $remo_etiquetas = $_POST['remo_etiquetas'];        
        $adul_info = $_POST['adul_info'];
        $corr_ext = $_POST['corr_ext'];
        $fisu_lineal = $_POST['fisu_lineal'];
        $corr_gene = $_POST['corr_gene'];
        $pica_ais = $_POST['pica_ais'];
        $abolladuras = $_POST['abolladuras'];
        $abombamiento = $_POST['abombamiento'];
        $martillo = $_POST['martillo'];
        $rotura = $_POST['rotura'];
        $dobleces = $_POST['dobleces'];
        $valles = $_POST['valles'];
        $hil_ros = $_POST['hil_ros'];
        $hilos_roscas = $_POST['hilos_roscas'];

        $quema_arco_text = $_POST['quema_arco_text'];
        $corr_int_text = $_POST['corr_int_text'];
        $pica_int_text = $_POST['pica_int_text'];
        $grasa_aceite_text = $_POST['grasa_aceite_text'];        
        $remo_pintura_text = $_POST['remo_pintura_text'];
        $remo_etiquetas_text = $_POST['remo_etiquetas_text'];        
        $adul_info_text = $_POST['adul_info_text'];
        $corr_ext_text = $_POST['corr_ext_text'];
        $fisu_lineal_text = $_POST['fisu_lineal_text'];
        $corr_gene_text = $_POST['corr_gene_text'];
        $pica_ais_text = $_POST['pica_ais_text'];
        $abolladuras_text = $_POST['abolladuras_text'];
        $abombamiento_text = $_POST['abombamiento_text'];
        $martillo_text = $_POST['martillo_text'];
        $rotura_text = $_POST['rotura_text'];
        $dobleces_text = $_POST['dobleces_text'];
        $valles_text = $_POST['valles_text'];
        $hilos_roscas_text = $_POST['hilos_roscas_text'];
        $hilos_roscas_text = $_POST['hilos_roscas_text'];

        if($estado_acero == APROBADO){
          $estado_prueba = 1;
        }else if($estado_acero == RECHAZADO){
          $estado_prueba = 2;
        }

        $consulta12 = "SELECT num_cili, ph_u, llenado_u, pintura_u, valvula_u, granallado_u, cambio_serv_u, lavado_especial_u FROM has_movimiento_cilindro_pev WHERE id_has_movimiento_cilindro_pev = $id_has_movimiento_cilindro_pev";
        $resultado12 = mysqli_query($con,$consulta12);
        $linea12 = mysqli_fetch_assoc($resultado12);

        $num_cili_12 = isset($linea12["num_cili"]) ? $linea12["num_cili"] : NULL;
        $ph_u_12 = isset($linea12["ph_u"]) ? $linea12["ph_u"] : NULL;
        $llenado_u_12 = isset($linea12["llenado_u"]) ? $linea12["llenado_u"] : NULL;
        $pintura_u_12 = isset($linea12["pintura_u"]) ? $linea12["pintura_u"] : NULL;
        $valvula_u_12 = isset($linea12["valvula_u"]) ? $linea12["valvula_u"] : NULL;
        $granallado_u_12 = isset($linea12["granallado_u"]) ? $linea12["granallado_u"] : NULL;
        $cambio_serv_u_12 = isset($linea12["cambio_serv_u"]) ? $linea12["cambio_serv_u"] :  NULL;
        $lavado_especial_u_12 = isset($linea12["lavado_especial_u"]) ? $linea12["lavado_especial_u"] : NULL;

        $consulta13 = "SELECT id_cilindro_eto FROM cilindro_eto WHERE num_cili_eto = '$num_cili_12'";
        $resultado13 = mysqli_query($con,$consulta13);
        $linea13 = mysqli_fetch_assoc($resultado13);

        $id_cilindro_eto_13 = isset($linea13["id_cilindro_eto"]) ? $linea13["id_cilindro_eto"] : NULL;

        if($ph_u_12 == 1){
          $id_estado_13 = 5;
        }else if($granallado_u_12 == 1){
          $id_estado_13 = 7;
        }else if($lavado_especial_u_12 == 1){
          $id_estado_13 = 12;
        }else if($pintura_u_12 == 1){
          $id_estado_13 = 6;
        }else if ($llenado_u_12 == 1) {
          $id_estado_13 = 13;
        }else if ($cambio_serv_u_12 == 1) {
          $id_estado_13 = 8;
        }else if ($cambio_serv_u_12 == 1) {
          $id_estado_13 = 4;
        }

        $consulta14 = "UPDATE cilindro_eto SET id_estado = $id_estado_13 WHERE id_cilindro_eto = $id_cilindro_eto_13";
        $resultado14 = mysqli_query($con,$consulta14);
        
        $observaciones = $_POST['observaciones'];

        $consulta2 = "SELECT id_has_movimiento_cilindro_pev FROM inspeccion_ace WHERE id_has_movimiento_cilindro_pev = '".$id_has_movimiento_cilindro_pev."'";
        $resultado2 = mysqli_query($con,$consulta2);
        if(mysqli_num_rows($resultado2) > 0 ) //se actualizan parametros de inspeccion
        {
            $consulta3 = "UPDATE inspeccion_ace SET
                                 estado_acero = '".$estado_acero."',
                                 quema_arco = '".$quema_arco."',
                                 corr_int = '".$corr_int."',
                                 pica_int = '".$pica_int."',
                                 grasa_aceite = '".$grasa_aceite."',
                                 remo_pintura = '".$remo_pintura."',
                                 remo_etiquetas = '".$remo_etiquetas."',                                 
                                 adul_info = '".$adul_info."',
                                 corr_ext = '".$corr_ext."',
                                 fisu_lineal = '".$fisu_lineal."',
                                 corr_gene = '".$corr_gene."',
                                 pica_ais = '".$pica_ais."',
                                 abolladuras = '".$abolladuras."',
                                 abombamiento = '".$abombamiento."',
                                 martillo = '".$martillo."',
                                 rotura = '".$rotura."',
                                 dobleces = '".$dobleces."',
                                 valles = '".$valles."',
                                 hil_ros = '".$hil_ros."',
                                 hilos_roscas = '".$hilos_roscas."',

                                 quema_arco_text= '".$quema_arco_text."',
                                 corr_int_text = '".$corr_int_text."',
                                 pica_int_text = '".$pica_int_text."',
                                 grasa_aceite_text = '".$grasa_aceite_text."',
                                 remo_pintura_text = '".$remo_pintura_text."',
                                 remo_etiquetas_text = '".$remo_etiquetas_text."',
                                 adul_info_text = '".$adul_info_text."',
                                 corr_ext_text = '".$corr_ext_text."',
                                 fisu_lineal_text = '".$fisu_lineal_text."',
                                 corr_gene_text = '".$corr_gene_text."',
                                 pica_ais_text = '".$pica_ais_text."',
                                 abolladuras_text = '".$abolladuras_text."',
                                 abombamiento_text = '".$abombamiento_text."',
                                 martillo_text = '".$martillo_text."',
                                 rotura_text = '".$rotura_text."',
                                 dobleces_text = '".$dobleces_text."',
                                 valles_text = '".$valles_text."',
                                 hilos_roscas_text = '".$hilos_roscas_text."',



                                 observaciones = '".$observaciones."'
                                 WHERE id_has_movimiento_cilindro_pev = '".$id_has_movimiento_cilindro_pev."'";
            $resultado3 = mysqli_query($con,$consulta3);
            if($resultado3 == FALSE)
            {
                echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
            }
            else
            {
                ?>
                <script type="text/javascript">
                    alert("Inspección actualizada correctamente.");
                    window.location = 'insp_vi_acu.php?id_has_movimiento_cilindro_pev='+<?php echo $id_has_movimiento_cilindro_pev; ?>;
                </script>
                <?php
            }
        }
        else
        {
            $consulta1 = "INSERT INTO inspeccion_ace (estado_acero,
                                                      quema_arco,
                                                      corr_int,
                                                      pica_int,
                                                      grasa_aceite,
                                                      remo_pintura,
                                                      remo_etiquetas,
                                                      adul_info,
                                                      corr_ext,
                                                      fisu_lineal,
                                                      corr_gene,
                                                      pica_ais,
                                                      abolladuras,
                                                      abombamiento,
                                                      martillo,
                                                      rotura,
                                                      dobleces,
                                                      valles,
                                                      hil_ros,
                                                      hilos_roscas,
                                                      observaciones,
                                                      id_has_movimiento_cilindro_pev,
                                                      quema_arco_text,
                                                      corr_int_text,
                                                      pica_int_text,
                                                      grasa_aceite_text,
                                                      remo_pintura_text,
                                                      remo_etiquetas_text,
                                                      adul_info_text,
                                                      corr_ext_text,
                                                      fisu_lineal_text,
                                                      corr_gene_text,
                                                      pica_ais_text,
                                                      abolladuras_text,
                                                      abombamiento_text,
                                                      martillo_text,
                                                      rotura_text,
                                                      dobleces_text,
                                                      valles_text,
                                                      hilos_roscas_text)
                        VALUES ('".$estado_acero."',
                                '".$quema_arco."',
                                '".$corr_int."',
                                '".$pica_int."',
                                '".$grasa_aceite."',
                                '".$remo_pintura."',
                                '".$remo_etiquetas."',
                                '".$adul_info."',
                                '".$corr_ext."',
                                '".$fisu_lineal."',
                                '".$corr_gene."',
                                '".$pica_ais."',
                                '".$abolladuras."',
                                '".$abombamiento."',
                                '".$martillo."',
                                '".$rotura."', 
                                '".$dobleces."',
                                '".$valles."',
                                '".$hil_ros."',
                                '".$hilos_roscas."',
                                '".$observaciones."',
                                '".$id_has_movimiento_cilindro_pev."',
                                '".$quema_arco_text."',
                                '".$corr_int_text."',
                                '".$pica_int_text."',
                                '".$grasa_aceite_text."',
                                '".$remo_pintura_text."',
                                '".$remo_etiquetas_text."',
                                '".$adul_info_text."',
                                '".$corr_ext_text."',
                                '".$fisu_lineal_text."',
                                '".$corr_gene_text."',
                                '".$pica_ais_text."',
                                '".$abolladuras_text."',
                                '".$abombamiento_text."',
                                '".$martillo_text."',
                                '".$rotura_text."',
                                '".$dobleces_text."',
                                '".$valles_text."',
                                '".$hilos_roscas_text."')";
            if(mysqli_query($con,$consulta1))
            {
                ?>
                <script type="text/javascript">
                    var id_orden_pev = '<?php echo $id_orden_pev; ?>';
                    alert("Inspección guardada correctamente.")
                    window.location = 'insp_vi_acu.php?id_has_movimiento_cilindro_pev='+<?php echo $id_has_movimiento_cilindro_pev; ?>;
                </script>
                <?php
            }
            else
            {
                echo "Error: " . $consulta1 . "<br>" . mysqli_error($con);
            }
        }
    }
    //Consulta el responsable de la inspección
    $consulta = "SELECT Name, LastName FROM user WHERE idUser = '".$idUser."'";
    $resultado = mysqli_query($con,$consulta);
    if(mysqli_num_rows($resultado) > 0 )
    {
        $linea = mysqli_fetch_assoc($resultado);
        $Name = $linea['Name'];
        $LastName = $linea['LastName'];
        $responsable = $Name." ".$LastName;
    }
    //Si existe el cilindro se consultan datos
    if(strlen($id_has_movimiento_cilindro_pev) > 0)
    {
        $consulta3 = "SELECT num_cili, id_tipo_gas_pev, id_transporte_pev
                      FROM has_movimiento_cilindro_pev 
                      WHERE id_has_movimiento_cilindro_pev = '".$id_has_movimiento_cilindro_pev."'";
        $resultado3 = mysqli_query($con,$consulta3);
        if(mysqli_num_rows($resultado3) > 0)
        {
            $linea3 = mysqli_fetch_assoc($resultado3);
            $num_cili = $linea3['num_cili'];
            $id_tipo_gas_pev = $linea3['id_tipo_gas_pev'];
            $id_transporte_pev = $linea3['id_transporte_pev'];

            $consulta10 = "SELECT esp_fab_eto, esp_actu_eto, fecha_fab_eto FROM cilindro_eto WHERE num_cili_eto = '".$num_cili."'";
            $resultado10 = mysqli_query($con,$consulta10);
            if(mysqli_num_rows($resultado10) > 0)
            {
                $linea10 = mysqli_fetch_assoc($resultado10);
                $esp_fab_eto = $linea10['esp_fab_eto'];
                $esp_actu_eto = $linea10['esp_actu_eto'];
                $fecha_fab_eto = $linea10['fecha_fab_eto'];
            }
            $consulta4 = "SELECT id_cliente,fecha FROM transporte_pev WHERE id_transporte_pev = '".$id_transporte_pev."'";
            $resultado4 = mysqli_query($con,$consulta4);
            if(mysqli_num_rows($resultado4) > 0)
            {
                $linea4 = mysqli_fetch_assoc($resultado4);
                $fecha = $linea4['fecha'];
                $id_cliente = $linea4['id_cliente'];

                $consulta8 = "SELECT nombre FROM clientes WHERE id_cliente = '".$id_cliente."'";
                $resultado8 = mysqli_query($con,$consulta8);
                if(mysqli_num_rows($resultado8) > 0)
                {
                    $linea8 = mysqli_fetch_assoc($resultado8);
                    $nombre_cliente = $linea8['nombre'];
                }
            }
            $consulta5 = "SELECT nombre FROM tipo_gas_pev WHERE id_tipo_gas_pev = '".$id_tipo_gas_pev."'";
            $resultado5 = mysqli_query($con,$consulta5);
            if(mysqli_num_rows($resultado5) > 0)
            {
                $linea5 = mysqli_fetch_assoc($resultado5);
                $nombre = $linea5['nombre'];
            }            
            
            $consulta6 = "SELECT id_especi_eto, especi_eto, num_cili_eto_2 FROM cilindro_eto WHERE num_cili_eto = '".$num_cili."'";
            $resultado6 = mysqli_query($con,$consulta6);
            if(mysqli_num_rows($resultado6) > 0)
            {
                $linea6 = mysqli_fetch_assoc($resultado6);
                $id_especi_eto = $linea6['id_especi_eto'];
                $especi_eto = $linea6['especi_eto'];
                $num_cili_eto_2 = $linea6['num_cili_eto_2'];

                $consulta7 = "SELECT especificacion FROM especificacion WHERE id_especificacion = '".$id_especi_eto."'";
                $resultado7 = mysqli_query($con,$consulta7);
                if(mysqli_num_rows($resultado7))
                {
                    $linea7 = mysqli_fetch_assoc($resultado7);
                    $especificacion = $linea7['especificacion'];
                }
            }
            $consulta11 = "SELECT * FROM inspeccion_ace WHERE id_has_movimiento_cilindro_pev = '".$id_has_movimiento_cilindro_pev."'";
            $resultado11 = mysqli_query($con,$consulta11);
            if(mysqli_num_rows($resultado11) > 0)
            {
                $linea11 = mysqli_fetch_assoc($resultado11);
                $estado_acero = $linea11['estado_acero'];
                $quema_arco = $linea11['quema_arco'];
                $corr_int = $linea11['corr_int'];
                $pica_int = $linea11['pica_int'];
                $grasa_aceite = $linea11['grasa_aceite'];        
                $remo_pintura = $linea11['remo_pintura'];
                $remo_etiquetas = $linea11['remo_etiquetas'];        
                $adul_info = $linea11['adul_info'];
                $corr_ext = $linea11['corr_ext'];
                $fisu_lineal = $linea11['fisu_lineal'];
                $corr_gene = $linea11['corr_gene'];
                $pica_ais = $linea11['pica_ais'];
                $abolladuras = $linea11['abolladuras'];
                $abombamiento = $linea11['abombamiento'];
                $martillo = $linea11['martillo'];
                $rotura = $linea11['rotura'];
                $dobleces = $linea11['dobleces'];
                $valles = $linea11['valles'];
                $hil_ros = $linea11['hil_ros'];
                $hilos_roscas = $linea11['hilos_roscas'];
                $observaciones = $linea11['observaciones'];

                $quema_arco_text = $linea11['quema_arco_text'];
                $corr_int_text = $linea11['corr_int_text'];
                $pica_int_text = $linea11['pica_int_text'];
                $grasa_aceite_text = $linea11['grasa_aceite_text'];        
                $remo_pintura_text = $linea11['remo_pintura_text'];
                $remo_etiquetas_text = $linea11['remo_etiquetas_text'];        
                $adul_info_text = $linea11['adul_info_text'];
                $corr_ext_text = $linea11['corr_ext_text'];
                $fisu_lineal_text = $linea11['fisu_lineal_text'];
                $corr_gene_text = $linea11['corr_gene_text'];
                $pica_ais_text = $linea11['pica_ais_text'];
                $abolladuras_text = $linea11['abolladuras_text'];
                $abombamiento_text = $linea11['abombamiento_text'];
                $martillo_text = $linea11['martillo_text'];
                $rotura_text = $linea11['rotura_text'];
                $dobleces_text = $linea11['dobleces_text'];
                $valles_text = $linea11['valles_text'];
                $hilos_roscas_text = $linea11['hilos_roscas_text'];
                $hilos_roscas_text = $linea11['hilos_roscas_text'];
                if($estado_acero == 1)
                {
                    $estado_acero = "APROBADO";
                }
                else
                {
                    if($estado_acero == 2)
                    {
                        $estado_acero == "NO APROBADO";
                    }
                    else
                    {
                        $estado_acero = "DESCONOCIDO";
                    }
                }
            }
        }

    }
?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
    <div id="content">
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-md-6 col-md-offset-3">
                    <div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                            <h2>INSPECCIÓN VISUAL ACUMULADOR</h2>
                        </header>
                        <div>
                            <div class="jarviswidget-editbox">
                            </div>
                            <div class="widget-body no-padding">                                
                                <form name="mezcla" action="insp_visual.php" method="POST" id="mezcla">
                                    <input type="hidden" name="id_orden_pev" value="<?php echo isset($_REQUEST['id_orden_pev']) ? $_REQUEST['id_orden_pev'] : NULL; ?>">
                                    <input type="hidden" name="id_has_movimiento_cilindro_pev3" id="id_has_movimiento_cilindro_pev3" value="">
                                    <div class="well well-sm well-primary">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="category">Numero Cilindro:</label>
                                                    <input type="text" class="form-control" placeholder="Numero Cilindro" name="num_cili" readonly value="<?php echo isset($num_cili) ? $num_cili : NULL; ?>"/>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="category">Fecha de Ingreso:</label>
                                                    <input type="text" class="form-control" placeholder="Fecha Ingreso" name="fecha" readonly value="<?php echo isset($fecha) ? $fecha : NULL; ?>"/>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="category">Fecha de Inspección:</label>
                                                    <input type="text" class="form-control" placeholder="Fecha Inspección" name="fecha_inspeccion_acu" readonly required value="<?php echo $Fecha ?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="category">Fecha de Fabricación:</label>
                                                    <input type="text" class="form-control" placeholder="Fecha Fabricación" name="fecha_fab_eto" readonly value="<?php echo isset($fecha_fab_eto) ? $fecha_fab_eto : NULL; ?>"/>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="category"><br>Capacidad (kg):</label>
                                                    <input type="text" class="form-control" placeholder="Capacidad (kg)" name="" readonly required />
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="category">TARA Fabricación (kg):</label>
                                                    <input type="text" class="form-control" placeholder="Tara Fabricación" name="tara_esta_eto" readonly required />
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="category"><br>TARA Actual (kg):</label>
                                                    <input type="text" class="form-control" placeholder="Tara Actual" name="tara_actu_eto" readonly required />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                              
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="category">GAS:</label>
                                                    <input type="text" class="form-control" placeholder="Tipo de GAS" name="gas_acu" readonly required />
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="category">Especificación:</label>
                                                    <input type="text" class="form-control" placeholder="Especificación" name="especificacion_acu" readonly required />
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                  <label for="category">Descripción:</label>
                                                    <input type="text" class="form-control" placeholder="Especificación" name="especi_eto3" readonly />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h4>Estado:</h4>
                                            </div>
                                            <div class="col-md-4">
                                                <h4 id="p3"></h4>
                                            </div>
                                        </div>
                                    </div>
                                        <div class="well well-sm well-primary">
                                            <fieldset>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="category">Fusibles:</label>                            
                                                    </div>  
                                                </div>   
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="inline-group">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="fusibles" value="1" id="fusibles1" onclick="check2();" <?php if($fusibles=='1') print "checked=true"?> >
                                                            <i></i>Si</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="fusibles" value="2" id="fusibles2" onclick="check2();"  <?php if($fusibles=='2') print "checked=true"?>  >
                                                            <i></i>No</label>
                                                            <label class="input">
                                                                <input type="text" name="fusibles_text">
                                                            </label>                   
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>
                                            </fieldset>
                                            <fieldset>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="category">Exposición al Fuego :</label>                            
                                                    </div>  
                                                </div>   
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="inline-group">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="id_expo_fuego" value="1" id="id_expo_fuego1" onclick="check2();"  <?php if($id_expo_fue=='1') print "checked=true"?> >
                                                            <i></i>Si</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="id_expo_fuego" value="2" id="id_expo_fuego2" onclick="check2();"  <?php if($id_expo_fue=='2') print "checked=true"?> >
                                                            <i></i>No</label>
                                                            <label class="input">
                                                                <input type="text" name="expo_fuego_text">
                                                            </label>                   
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>
                                            </fieldset>
                                            <fieldset>  
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="category">Retroceso de LLama:</label>                            
                                                    </div>  
                                                </div>   
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="inline-group">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="id_retro_llama"  value="1" id="id_retro_llama1" onclick="check2();"  <?php if($id_retro_llama=='1') print "checked=true"?> >
                                                            <i></i>Si</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="id_retro_llama" value="2" id="id_retro_llama2" onclick="check2();"  <?php if($id_retro_llama=='2') print "checked=true"?> >
                                                            <i></i>No</label>
                                                            <label class="input">
                                                                <input type="text" name="retro_llama_text">
                                                            </label>                     
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>    
                                            </fieldset>
                                            <fieldset>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="category">Remoción Total de Pintura:</label>                            
                                                    </div>  
                                                </div>   
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="inline-group">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="id_remo_pintura_ac"  value="1" id="id_remo_pintura_ac1" onclick="check2();" <?php if($id_remo_pintura_ac=='1') print "checked=true"?> >
                                                            <i></i>Si</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="id_remo_pintura_ac" value="2" id="id_remo_pintura_ac2" onclick="check2();" <?php if($id_remo_pintura_ac=='2') print "checked=true"?> >
                                                            <i></i>No</label>
                                                            <label class="input">
                                                                <input type="text" name="remo_pintura_text">
                                                            </label>                     
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>
                                            </fieldset>                   
                                            <fieldset>  
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="category">Abombamiento:</label>                            
                                                    </div>  
                                                </div>   
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="inline-group">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="id_abomba_ac"  value="1" id="id_abomba_ac1" onclick="check2();"  <?php if($id_abomba_ac=='1') print "checked=true"?> >
                                                            <i></i>Si</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="id_abomba_ac" value="2" id="id_abomba_ac2" onclick="check2();"  <?php if($id_abomba_ac=='2') print "checked=true"?> >
                                                            <i></i>No</label>
                                                            <label class="input">
                                                                <input type="text" name="abomba_ac_text">
                                                            </label>
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>
                                            </fieldset>
                                            <fieldset>  
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="category">Grietas:</label>                            
                                                    </div>  
                                                </div>   
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="inline-group">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="id_grietas" value="1" id="id_grietas1" onclick="check2();"  <?php if($id_grietas=='1') print "checked=true"?> >
                                                            <i></i>Si</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="id_grietas" value="2" id="id_grietas2" onclick="check2();"  <?php if($id_grietas=='2') print "checked=true"?> >
                                                            <i></i>No</label>
                                                            <label class="input">
                                                                <input type="text" name="grietas_text">
                                                            </label>
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>
                                            </fieldset> 
                                            <fieldset>  
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="category">Adulteración de la Información:</label>                            
                                                    </div>  
                                                </div>   
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="inline-group">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="id_adul_info_ac"  value="1" id="id_adul_info_ac1" onclick="check2();"  <?php if($id_adul_info_ac=='1') print "checked=true"?> >
                                                            <i></i>Si</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="id_adul_info_ac" value="2" id="id_adul_info_ac2" onclick="check2();" <?php if($id_adul_info_ac=='2') print "checked=true"?> >
                                                            <i></i>No</label>
                                                            <label class="input">
                                                                <input type="text" name="adul_info_ac_text">
                                                            </label>
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>
                                            </fieldset> 
                                            <fieldset> 
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="category">Quemaduras por Arco o Antorcha:</label>                            
                                                    </div>  
                                                </div>   
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="inline-group">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="id_quema_sople" value="1" id="id_quema_sople1" onclick="check2();" <?php if($id_quema_sople=='1') print "checked=true"?> >
                                                            <i></i>Si</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="id_quema_sople" value="2" id="id_quema_sople2" onclick="check2();"  <?php if($id_quema_sople=='2') print "checked=true"?> >
                                                            <i></i>No</label>
                                                            <label class="input">
                                                                <input type="text" name="quema_sople_text">
                                                            </label>
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>
                                            </fieldset> 
                                            <fieldset>  
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="category">Abolladuras:</label>                            
                                                    </div>  
                                                </div>   
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="inline-group">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="id_abolla" value="1" id="id_abolla1" onclick="check2();"  <?php if($id_abolla=='1') print "checked=true"?> >
                                                            <i></i>Si</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="id_abolla" value="2" id="id_abolla2" onclick="check2();"  <?php if($id_abolla=='2') print "checked=true"?> >
                                                            <i></i>No</label>
                                                            <label class="input">
                                                                <input type="text" name="abolla_text">
                                                            </label>
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>
                                            </fieldset>
                                            <fieldset>  
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="category">Cortes o Muescas:</label>                            
                                                    </div>  
                                                </div>   
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="inline-group">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="id_cort_mues" value="1" id="id_cort_mues1" onclick="check2();"  <?php if($id_cort_mues=='1') print "checked=true"?> >
                                                            <i></i>Si</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="id_cort_mues" value="2" id="id_cort_mues2" onclick="check2();"  <?php if($id_cort_mues=='2') print "checked=true"?> >
                                                            <i></i>No</label>
                                                            <label class="input">
                                                                <input type="text" name="cort_mues_text">
                                                            </label>
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>
                                            </fieldset> 
                                            <fieldset>  
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="category">Corrosión General:</label>                            
                                                    </div>  
                                                </div>   
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="inline-group">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="id_corro_gen" value="1" id="id_corro_gen1" onclick="check2();"  <?php if($id_corro_gen=='1') print "checked=true"?> >
                                                            <i></i>Si</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="id_corro_gen" value="2" id="id_corro_gen2" onclick="check2();" <?php if($id_corro_gen=='2') print "checked=true"?> >
                                                            <i></i>No</label>
                                                            <label class="input">
                                                                <input type="text" name="corro_gen_text">
                                                            </label>
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>
                                            </fieldset> 
                                            <fieldset>     
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="category">Corrosión Local:</label>                            
                                                    </div>  
                                                </div>   
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="inline-group">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="id_corr_loc" value="1" id="id_corr_loc1" onclick="check2();" <?php if($id_corr_loc=='1') print "checked=true"?> >
                                                            <i></i>Si</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="id_corr_loc" value="2" id="id_corr_loc2" onclick="check2();" <?php if($id_corr_loc=='2') print "checked=true"?> >
                                                            <i></i>No</label>
                                                            <label class="input">
                                                                <input type="text" name="corr_loc_text">
                                                            </label>
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>     
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="category">Corrosión en Línea:</label>                            
                                                    </div>  
                                                </div>   
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="inline-group">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="id_corr_linea" value="1" id="id_corr_linea1" onclick="check2();" <?php if($id_corr_linea=='1') print "checked=true"?> >
                                                            <i></i>Si</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="id_corr_linea" value="2" id="id_corr_linea2" onclick="check2();" <?php if($id_corr_linea=='2') print "checked=true"?> >
                                                            <i></i>No</label>
                                                            <label class="input">
                                                                <input type="text" name="corr_linea_text">
                                                            </label>
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="category">Corrosión en Hendiduras:</label>                            
                                                    </div>  
                                                </div>   
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="inline-group">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="id_corr_hen" value="1" id="id_corr_hen1" onclick="check2();" <?php if($id_corr_hen=='1') print "checked=true"?> >
                                                            <i></i>Si</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="id_corr_hen" value="2" id="id_corr_hen2" onclick="check2();" <?php if($id_corr_hen=='2') print "checked=true"?> >
                                                            <i></i>No</label>
                                                            <label class="input">
                                                                <input type="text" name="corr_hen_text">
                                                            </label>
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="category">Limpieza de la Rosca:</label>                            
                                                    </div>  
                                                </div>   
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="inline-group">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="lim_ros" value="1" id="lim_ros1" onclick="check2();" <?php if($lim_ros=='1') print "checked=true"?> >
                                                            <i></i>Si</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="lim_ros" value="2" id="lim_ros2" onclick="check2();" <?php if($lim_ros=='2') print "checked=true"?> >
                                                            <i></i>No</label>
                                                            <label class="input">
                                                                <input type="text" name="lim_ros_text">
                                                            </label>
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="category">Estado de la Rosca:</label>                            
                                                    </div>  
                                                </div>   
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="inline-group">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="est_ros" value="1" id="est_ros1" onclick="check2();" <?php if($est_ros=='1') print "checked=true"?> >
                                                            <i></i>Bueno</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="est_ros" value="2" id="est_ros2" onclick="check2();" <?php if($est_ros=='2') print "checked=true"?> >
                                                            <i></i>Dañado</label>
                                                            <label class="input">
                                                                <input type="text" name="est_ros_text">
                                                            </label>
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="category">Filtros y Mallas:</label>                            
                                                    </div>  
                                                </div>   
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="inline-group">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="id_fill_mall" value="1" id="id_fill_mall1" onclick="check2();" <?php if($id_fill_mall=='1') print "checked=true"?> >
                                                            <i></i>Si</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="id_fill_mall" value="2" id="id_fill_mall2" onclick="check2();" <?php if($id_fill_mall=='2') print "checked=true"?> >
                                                            <i></i>No</label>
                                                            <label class="input">
                                                                <input type="text" name="fill_mall_text">
                                                            </label>
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="category">Firmeza o Vacío de la Masa:</label>                            
                                                    </div>  
                                                </div>   
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="inline-group">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="fir_vacio" value="1" id="fir_vacio1" onclick="check2();" <?php if($fir_vacio=='1') print "checked=true"?> >
                                                            <i></i>Si</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="fir_vacio" value="2" id="fir_vacio2" onclick="check2();" <?php if($fir_vacio=='2') print "checked=true"?> >
                                                            <i></i>No</label>
                                                            <label class="input">
                                                                <input type="text" name="fir_vacio_text">
                                                            </label>
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>
                                            </fieldset>
                                            <fieldset>
                                            <legend>Contaminación:</legend>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="category">Polvo Fino de Carbón:</label>                            
                                                    </div>  
                                                </div>   
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="inline-group">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="polvo_fino" value="1" id="polvo_fino1" onclick="check2();" <?php if($polvo_fino=='1') print "checked=true"?> >
                                                            <i></i>Si</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="polvo_fino" value="2" id="polvo_fino2" onclick="check2();" <?php if($polvo_fino=='2') print "checked=true"?> >
                                                            <i></i>No</label>
                                                            <label class="input">
                                                                <input type="text" name="polvo_fino_text">
                                                            </label>
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="category">Agua:</label>                            
                                                    </div>  
                                                </div>   
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="inline-group">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="agua" value="1" id="agua1" onclick="check2();" <?php if($agua=='1') print "checked=true"?> >
                                                            <i></i>Si</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="agua" value="2" id="agua2" onclick="check2();" <?php if($agua=='2') print "checked=true"?> >
                                                            <i></i>No</label>
                                                            <label class="input">
                                                                <input type="text" name="agua_text">
                                                            </label>
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="category">Depositos de Aceite:</label>                            
                                                    </div>  
                                                </div>   
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="inline-group">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="depo_acei" value="1" id="depo_acei1" onclick="check2();" <?php if($depo_acei=='1') print "checked=true"?> >
                                                            <i></i>Si</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="depo_acei" value="2" id="depo_acei2" onclick="check2();" <?php if($depo_acei=='2') print "checked=true"?> >
                                                            <i></i>No</label>
                                                            <label class="input">
                                                                <input type="text" name="depo_acei_text">
                                                            </label>
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="category">Decoloración Masa:</label>                            
                                                    </div>  
                                                </div>   
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="inline-group">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="deco_masa" value="1" id="deco_masa1" onclick="check2();" <?php if($deco_masa=='1') print "checked=true"?> >
                                                            <i></i>Si</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="deco_masa" value="2" id="deco_masa2" onclick="check2();" <?php if($deco_masa=='2') print "checked=true"?> >
                                                            <i></i>No</label>
                                                            <label class="input">
                                                                <input type="text" name="deco_masa_text">
                                                            </label>
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>
                                            </fieldset>
                                            <fieldset>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="category">Agrietamiento Masa Monolítica:</label>                            
                                                    </div>  
                                                </div>   
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="inline-group">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="id_agri_masa" value="1" id="id_agri_masa1" onclick="check2();" <?php if($id_agri_masa=='1') print "checked=true"?> >
                                                            <i></i>Si</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="id_agri_masa" value="2" id="id_agri_masa2" onclick="check2();" <?php if($id_agri_masa=='2') print "checked=true"?> >
                                                            <i></i>No</label>
                                                            <label class="input">
                                                                <input type="text" name="agri_masa_text">
                                                            </label>
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="category">Desmoronamiento Masa Monolítica:</label>                            
                                                    </div>  
                                                </div>   
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="inline-group">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="id_des_masa" value="1" id="id_des_masa1" onclick="check2();" <?php if($id_des_masa=='1') print "checked=true"?> >
                                                            <i></i>Si</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="id_des_masa" value="2" id="id_des_masa2" onclick="check2();" <?php if($id_des_masa=='2') print "checked=true"?> >
                                                            <i></i>No</label>
                                                            <label class="input">
                                                                <input type="text" name="des_masa_text">
                                                            </label>
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="category">Separación Entre Envase y Masa:</label>                            
                                                    </div>  
                                                </div>   
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="inline-group">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="sep_env" value="1" id="sep_env1" onclick="check2();" <?php if($sep_env=='1') print "checked=true"?> >
                                                            <i></i>Si</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="sep_env" value="2" id="sep_env2" onclick="check2();" <?php if($sep_env=='2') print "checked=true"?> >
                                                            <i></i>No</label>
                                                            <label class="input">
                                                                <input type="text" name="sep_env_text">
                                                            </label>
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>
                                            </fieldset>
                                            <fieldset>
                                            <legend>Movimiento Lateral</legend>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="category">Compactación:</label>                            
                                                    </div>  
                                                </div>   
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="inline-group">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="compacta" value="1" id="compacta1" onclick="check2();" <?php if($compacta=='1') print "checked=true"?> >
                                                            <i></i>Si</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="compacta" value="2" id="compacta2" onclick="check2();" <?php if($compacta=='2') print "checked=true"?> >
                                                            <i></i>No</label>
                                                            <label class="input">
                                                                <input type="text" name="compacta_text">
                                                            </label>
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="category">Cavitación :</label>                            
                                                    </div>  
                                                </div>   
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="inline-group">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="cavita" value="1" id="cavita1" onclick="check2();" <?php if($cavita=='1') print "checked=true"?> >
                                                            <i></i>Si</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="cavita" value="2" id="cavita2" onclick="check2();" <?php if($cavita=='2') print "checked=true"?> >
                                                            <i></i>No</label>
                                                            <label class="input">
                                                                <input type="text" name="cavita_text">
                                                            </label>
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>
                                            </fieldset>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal"> Cancelar</button>
                                            <?php
                                            if (in_array(22, $acc))
                                            {
                                            ?>
                                                <input type="submit" value="Guardar" name="g_insp_visual_acumulador" id="g_cilindro" class="btn btn-primary" />
                                            <?php
                                            }                   
                                            ?>
                                        </div>
                                    </form>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>
</div>
<!-- END MAIN PANEL -->

<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->
<?php
    include("inc/footer.php");
?>
<!-- END PAGE FOOTER -->

<?php 
    //include required scripts
    include("inc/scripts.php"); 
?>


<!-- PAGE RELATED PLUGIN(S) 
<script src="..."></script>-->
<!-- Flot Chart Plugin: Flot Engine, Flot Resizer, Flot Tooltip -->
<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>

<!-- Vector Maps Plugin: Vectormap engine, Vectormap language -->
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>

<!-- Full Calendar -->
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/jquery-form/jquery-form.min.js"></script>


<script type="text/javascript">
  function check2()
  {
    if(document.getElementById("fusibles1").checked == true || document.getElementById("id_expo_fuego1").checked == true || document.getElementById("id_retro_llama1").checked == true || document.getElementById("id_remo_pintura_ac1").checked == true || document.getElementById("id_abomba_ac1").checked == true || document.getElementById("id_grietas1").checked == true || document.getElementById("id_adul_info_ac1").checked == true || document.getElementById("id_quema_sople1").checked == true || document.getElementById("id_abolla1").checked == true || document.getElementById("id_cort_mues1").checked == true || document.getElementById("id_corro_gen1").checked == true || document.getElementById("id_corr_loc1").checked == true || document.getElementById("id_corr_linea1").checked == true || document.getElementById("id_corr_hen1").checked == true || document.getElementById("lim_ros1").checked == true || document.getElementById("est_ros2").checked == true || document.getElementById("id_fill_mall1").checked == true || document.getElementById("fir_vacio1").checked == true || document.getElementById("polvo_fino1").checked == true || document.getElementById("agua1").checked == true || document.getElementById("depo_acei1").checked == true || document.getElementById("deco_masa1").checked == true || document.getElementById("id_agri_masa1").checked == true || document.getElementById("id_des_masa1").checked == true || document.getElementById("sep_env1").checked == true || document.getElementById("compacta1").checked == true || document.getElementById("cavita1").checked == true)
      {
        document.getElementById("p3").value = "NO APROBADO";
      }
    else
    {
      document.getElementById("p3").value = "APROBADO";
    }
  }
</script>

<?php 
    //include footer
    include("inc/google-analytics.php"); 
}
else
{
    header("Location:index.php");
}
?>