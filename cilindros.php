﻿<?php
date_default_timezone_set("America/Bogota");

require ("libraries/conexion.php");@
session_start();

if(@$_SESSION['logged']== 'yes')
{ 
  	$id_cilindro = isset($_REQUEST['id_cilindro']) ? $_REQUEST['id_cilindro'] : NULL;
    $User_idUser =$_SESSION['su'];
    $acc = $_SESSION['acc'];

if(isset($_POST['g_cilindro']))
{
	$serial2 = $_POST['serial'] ;
	$id_codigo_producto2=$_POST['id_codigo_producto1'] ;
	$id_tipo_envace2=$_POST['id_tipo_envace1'] ;
	$id_propiedad_cilindro2=$_POST['id_propiedad_cilindro1'] ;
	$id_tapa2=$_POST['id_tapa1'] ;    

	if(strlen($id_cilindro) > 0)
	{
    $consulta= "UPDATE cilindros SET 
                serial_cilindro = '$serial2' ,
                id_codigo_producto = '$id_codigo_producto2' ,
                id_tipo_envace = '$id_tipo_envace2' , 
                id_propiedad_cilindro = '$id_propiedad_cilindro2'                                 
                WHERE id_cilindro = $id_cilindro ";
    $resultado = mysqli_query($con,$consulta) ;

    if ($resultado == FALSE)
    {
      echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
    }
    else
    {
      header('Location: busqueda_cilindros.php');
    }
  }
  else
  {
    $consulta = "INSERT INTO cilindros
          		(serial_cilindro,id_codigo_producto, id_tipo_envace, id_propiedad_cilindro) 
          		VALUES ('".$serial2."','".$id_codigo_producto2."', '".$id_tipo_envace2."', '".$id_propiedad_cilindro2."' )";
    $resultado = mysqli_query($con,$consulta) ;
    if ($resultado == FALSE)
    {
      echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
    }
    else
    {
      $id_cilindro = mysqli_insert_id($con);
      header('Location: busqueda_cilindros.php');
    }
  }
    
}


if(strlen($id_cilindro) > 0)
{ 
  	$consulta  = "SELECT * FROM cilindros WHERE id_cilindro= $id_cilindro";
  	$resultado = mysqli_query($con,$consulta) ;
  	$linea = mysqli_fetch_array($resultado);

	$id_cilindro1 = isset($linea["id_cilindro"]) ? $linea["id_cilindro"] : NULL;  
	$serial = isset($linea["serial_cilindro"]) ? $linea["serial_cilindro"] : NULL;
	$id_codigo_producto1 = isset($linea["id_codigo_producto"]) ? $linea["id_codigo_producto"] : NULL;
	$id_tipo_envace1 = isset($linea["id_tipo_envace"]) ? $linea["id_tipo_envace"] : NULL;
	$id_propiedad_cilindro1 = isset($linea["id_propiedad_cilindro"]) ? $linea["id_propiedad_cilindro"] : NULL;

    mysqli_free_result($resultado);
}




require_once("inc/init.php");
require_once("inc/config.ui.php");
$page_title = "Lote Co2";
$page_css[] = "your_style.css";
include("inc/header.php");
$page_nav[""][""][""][""] = true;
include("inc/nav.php");
?>

<div id="main" role="main">
	<?php
		include("inc/ribbon.php");
	?>	
	<div id="content">
		<section id="widget-grid" class="">
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-6">
					<div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h2>Cilindro </h2>
						</header>
						<div>
							<div class="jarviswidget-editbox"></div>							
							<div class="widget-body no-padding">
								<form id="checkout-form" class="smart-form" novalidate="novalidate" action="cilindros.php" method="POST">
								<input type="hidden" name="id_cilindro" id="id_cilindro" value="<?php echo $id_cilindro; ?>">
									<fieldset>
										<div class="row">
											<section class="col col-6">	
												<?php
												$consulta6 ="SELECT * FROM codigo_producto ORDER BY codigo ASC";
												$resultado6 = mysqli_query($con,$consulta6) ;
												echo "<section>";
												echo "<label class='label'>Codigo Producto</label>";
												echo"<label class='select'>";
												echo "<select name='id_codigo_producto1' ¿>";
												echo "<option value='0'>Seleccione...</option>";
												while($linea6 = mysqli_fetch_array($resultado6))
												{
													$id_codigo_producto = $linea6['id_codigo_producto'];
													$codigo = $linea6['codigo'];
													if ($id_codigo_producto==$id_codigo_producto1)
													{
															echo "<option value='$id_codigo_producto' selected >$codigo</option>"; 
													}
													else 
													{
															echo "<option value='$id_codigo_producto'>$codigo</option>"; 
													} 
												}//fin while 
												echo "</select>";
												echo "<i></i>";
												echo "</label>";
												echo "</section>";
															
												?>
											</section>
											<section class="col col-6">
												<label class="label">Serial :</label>
												<label class="input"> 
													<input type="text" name="serial" placeholder="Serial"  value="<?php echo isset($serial) ? $serial : NULL; ?>">
												</label>
											</section>										
										</div>
									</fieldset>	
									<fieldset>	
										<div class="row">
											<section class="col col-6">	
												<?php
												$consulta6 ="SELECT * FROM tipo_envace ORDER BY tipo ASC";
												$resultado6 = mysqli_query($con,$consulta6) ;
												echo "<section>";
												echo "<label class='label'>Envase</label>";
												echo"<label class='select'>";
												echo "<select name='id_tipo_envace1' ¿>";
												echo "<option value='0'>Seleccione...</option>";
												while($linea6 = mysqli_fetch_array($resultado6))
												{
													$id_tipo_envace = $linea6['id_tipo_envace'];
													$tipo = $linea6['tipo'];
													if ($id_tipo_envace==$id_tipo_envace1)
													{
															echo "<option value='$id_tipo_envace' selected >$tipo</option>"; 
													}
													else 
													{
															echo "<option value='$id_tipo_envace'>$tipo</option>"; 
													} 
												}//fin while 
												echo "</select>";
												echo "<i></i>";
												echo "</label>";
												echo "</section>";
															
												?>
											</section>
											<section class="col col-6">	
												<?php
												$consulta6 ="SELECT * FROM propiedad_cilindro ORDER BY propiedad ASC";
												$resultado6 = mysqli_query($con,$consulta6) ;
												echo "<section>";
												echo "<label class='label'>Propiedad :</label>";
												echo"<label class='select'>";
												echo "<select name='id_propiedad_cilindro1' ¿>";
												echo "<option value='0'>Seleccione...</option>";
												while($linea6 = mysqli_fetch_array($resultado6))
												{
													$id_propiedad_cilindro = $linea6['id_propiedad_cilindro'];
													$propiedad = $linea6['propiedad'];
													if ($id_propiedad_cilindro==$id_propiedad_cilindro1)
													{
															echo "<option value='$id_propiedad_cilindro' selected >$propiedad</option>"; 
													}
													else 
													{
															echo "<option value='$id_propiedad_cilindro'>$propiedad</option>"; 
													} 
												}//fin while 
												echo "</select>";
												echo "<i></i>";
												echo "</label>";
												echo "</section>";
															
												?>
											</section>											
										</div>
									</fieldset>
									
									<footer>                    
				                    	<input type="submit" value="Save" name="g_cilindro" id="g_cilindro" class="btn btn-primary" />
				                  	</footer>
               					</form>								


          					</div>  
           				</div>
           			</div>
         		</article>
        	</div>
        </section>
  </div>
  <!-- END MAIN CONTENT -->
</div>

<?php
  include("inc/footer.php");
  include("inc/scripts.php"); 
?>

<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>

<!-- Vector Maps Plugin: Vectormap engine, Vectormap language -->
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>

<!-- Full Calendar -->
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/jquery-form/jquery-form.min.js"></script>


<script type="text/javascript">

  $(document).ready(function() 
  {

    var $checkoutForm = $('#checkout-form').validate(
    {
    // Rules for form validation
      rules :
      {
        name1 : {
          required : true
        },
        idgender1 : {
          required : true
        },
        last_name : {
          required : true
        },
        home_phone : {
          required : true
        },
        primary_mail : {
          required : true
        },
        street_adress1 : {
          required : true
        }
      },

      // Messages for form validation
      messages : {
        name1 : {
          required : 'Please enter your first name'
        },
        idgender1 : {
          required : ''
        },
        last_name : {
          required : 'Please enter your last name'
        },
        home_phone : {
          required : 'Please enter your phone'
        },
        primary_mail : {
          required : 'Please enter your mail'
        },
        street_adress1 : {
          required : 'Please enter your Adress'
        }
      },

      // Do not change code below
      errorPlacement : function(error, element) {
        error.insertAfter(element.parent());
      }
    }); 

    }) 
   

</script>


<?php 
  //include footer
  include("inc/google-analytics.php"); 
?>
<?php

}
else
{
    header("Location:index.php");
}
?>
					                       
										