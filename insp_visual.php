<?php
session_start();
date_default_timezone_set("America/Bogota");
$Fecha = date("Y-m-d");
require ("libraries/conexion.php");

if($_SESSION['logged']== 'yes')
{ 
	$id_user=$_SESSION['su'];
    $acc = $_SESSION['acc'];
    $letra = isset($_REQUEST['letra']) ? $_REQUEST['letra'] : NULL;
    $id_orden_pev = isset($_REQUEST['id_orden_pev']) ? $_REQUEST['id_orden_pev'] : NULL;
    
	require_once("inc/init.php");
	require_once("inc/config.ui.php");
	$page_title = "Inspección Visual";
	$page_css[] = "your_style.css";
	include("inc/header.php");
	$page_nav[""][""][""][""] = true;
	include("inc/nav.php");

    if (isset($_POST['g_insp_visual_acero'])) //guardar información de inspección visual de acero
    {
        $id_remo_pintura = $_POST['id_remo_pintura'];
        $id_adul_info = $_POST['id_adul_info'];
        $esp_pared = $_POST['esp_pared'];
        $esp_actual = $_POST['esp_actual'];
        $id_corr_ext = $_POST['id_corr_ext'];
        $id_fisu_lineal = $_POST['id_fisu_lineal'];
        $id_corr_gene = $_POST['id_corr_gene'];
        $id_pica_ais = $_POST['id_pica_ais'];
        $id_abolladu = $_POST['id_abolladu'];
        $id_abombam = $_POST['id_abombam'];
        $hil_ros = $_POST['hil_ros'];
        $rotura = $_POST['rotura'];
        $dobleces = $_POST['dobleces'];
        $id_corr_int = $_POST['id_corr_int'];
        $id_pica_int = $_POST['id_pica_int'];
        $id_quema_arco = $_POST['id_quema_arco'];
        $hilos_roscas = $_POST['hilos_roscas'];
        $id_has_movimiento_cilindro_pev = $_POST['id_has_movimiento_cilindro_pev1'];

        $consulta2 = "SELECT id_has_movimiento_cilindro_pev FROM inspeccion_ace WHERE id_has_movimiento_cilindro_pev = '".$id_has_movimiento_cilindro_pev."'";
        $resultado2 = mysqli_query($con,$consulta2);
        if(mysqli_num_rows($resultado2) > 0 ) //se actualizan paramteros de inspeccion
        {
        	$consulta3 = "UPDATE inspeccion_ace SET
        								id_remo_pintura = '".$id_remo_pintura."',
        								id_adul_info = '".$id_adul_info."',
        								esp_pared = '".$esp_pared."',
        								esp_actual = '".$esp_actual."',
        								id_corr_ext = '".$id_corr_ext."',
        								id_fisu_lineal = '".$id_fisu_lineal."',
        								id_corr_gene = '".$id_corr_gene."',
        								id_pica_ais = '".$id_pica_ais."',
        								id_abolladu = '".$id_abolladu."',
        								id_abombam = '".$id_abombam."',
        								hil_ros = '".$hil_ros."',
        								rotura = '".$rotura."',
        								dobleces = '".$dobleces."',
        								id_corr_int = '".$id_corr_int."',
        								id_pica_int = '".$id_pica_int."',
        								id_quema_arco = '".$id_quema_arco."',
        								hilos_rosca = '".$hilos_roscas."'
        								WHERE id_has_movimiento_cilindro_pev = '".$id_has_movimiento_cilindro_pev."'";
        	$resultado3 = mysqli_query($con,$consulta3);
        	if($resultado3 == FALSE)
        	{
        		echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
        	}
        	else
        	{
        		?>
        		<script type="text/javascript">
        			alert("Inspección actualizada correctamente.");
        			window.location = 'insp_visual.php?id_orden_pev='+<?php echo $id_orden_pev; ?>;
        		</script>
        		<?php
        	}
        }
        else
        {
        	$consulta1 = "INSERT INTO inspeccion_ace (id_remo_pintura, id_adul_info, esp_pared, esp_actual, id_corr_ext, id_fisu_lineal,
                                                  id_corr_gene, id_pica_ais, id_abolladu, id_abombam, hil_ros, rotura, dobleces,
                                                  id_corr_int, id_pica_int, id_quema_arco, hilos_rosca, id_orden_pev,id_has_movimiento_cilindro_pev)
                      VALUES ('".$id_remo_pintura."','".$id_adul_info."','".$esp_pared."','".$esp_actual."','".$id_corr_ext."',
                              '".$id_fisu_lineal."','".$id_corr_gene."','".$id_pica_ais."','".$id_abolladu."','".$id_abombam."',
                              '".$hil_ros."','".$rotura."','".$dobleces."','".$id_corr_int."','".$id_pica_int."','".$id_quema_arco."', 
                              '".$hilos_roscas."','".$id_orden_pev."','".$id_has_movimiento_cilindro_pev."')";
	        if(mysqli_query($con,$consulta1))
	        {
	            ?>
	            <script type="text/javascript">
	                var id_orden_pev = '<?php echo $id_orden_pev; ?>';
	                alert("Inspección guardada correctamente.")
	                window.location = 'insp_visual.php?id_orden_pev='+<?php echo $id_orden_pev; ?>;
	            </script>
	            <?php
	        }
	        else
	        {
	            echo "Error: " . $consulta1 . "<br>" . mysqli_error($con);
	        }
        }
    }
    
    if (isset($_POST['g_insp_visual_acumulador'])) //guardar información de inspección visual de acero
    {
      $id_orden_pev = $_POST['id_orden_pev'];
      $fusibles = $_POST['fusibles'];
      $id_expo_fue = $_POST['id_expo_fuego'];
      $id_retro_llama = $_POST['id_retro_llama'];
      $id_remo_pintura = $_POST['id_remo_pintura_ac'];
      $id_abomba = $_POST['id_abomba_ac'];
      $id_grietas = $_POST['id_grietas'];
      $id_adul_info = $_POST['id_adul_info_ac'];
      $id_quema_sople = $_POST['id_quema_sople'];
      $id_abolla = $_POST['id_abolla'];
      $id_cort_mues = $_POST['id_cort_mues'];
      $id_corro_gen = $_POST['id_corro_gen'];
      $id_corr_loc = $_POST['id_corr_loc'];
      $id_corr_linea = $_POST['id_corr_linea'];
      $id_corr_hen = $_POST['id_corr_hen'];
      $lim_ros = $_POST['lim_ros'];
      $est_ros = $_POST['est_ros'];
      $id_fill_mall = $_POST['id_fill_mall'];
      $fir_vacio = $_POST['fir_vacio'];
      $polvo_fino = $_POST['polvo_fino'];
      $agua = $_POST['agua'];
      $depo_acei = $_POST['depo_acei'];
      $deco_masa = $_POST['deco_masa'];
      $id_agri_masa = $_POST['id_agri_masa'];
      $id_des_masa = $_POST['id_des_masa'];
      $sep_env = $_POST['sep_env'];
      $compacta = $_POST['compacta'];
      $cavita = $_POST['cavita'];

      $id_has_movimiento_cilindro_pev = $_POST['id_has_movimiento_cilindro_pev3'];

      $consulta2 = "SELECT id_has_movimiento_cilindro_pev FROM inspeccion_acu WHERE id_has_movimiento_cilindro_pev = '".$id_has_movimiento_cilindro_pev."'";
      $resultado2 = mysqli_query($con,$consulta2);
      if(mysqli_num_rows($resultado2) > 0 ) //se actualizan paramteros de inspeccion
      {
      	$consulta3 = "UPDATE inspeccion_acu SET
      								fusibles = '".$fusibles."',
      								id_expo_fue = '".$id_expo_fue."',
      								id_retro_llama = '".$id_retro_llama."',
      								id_remo_pintura = '".$id_remo_pintura."',
                      id_abomba = '".$id_abomba."',
                      id_grietas = '".$id_grietas."',
                      id_adul_info = '".$id_adul_info."',
      								id_quema_sople = '".$id_quema_sople."',
      								id_abolla = '".$id_abolla."',
      								id_cort_mues = '".$id_cort_mues."',
      								id_corro_gen = '".$id_corro_gen."',
      								id_corr_loc = '".$id_corr_loc."',
      								id_corr_linea = '".$id_corr_linea."',
      								id_corr_hen = '".$id_corr_hen."',
      								lim_ros = '".$lim_ros."',
      								est_ros = '".$est_ros."',
      								id_fil_mall = '".$id_fill_mall."',
      								fir_vacio = '".$fir_vacio."',
      								polvo_fino = '".$polvo_fino."',
      								agua = '".$agua."',
      								depo_acei = '".$depo_acei."',
      								deco_masa = '".$deco_masa."',
      								id_agri_masa = '".$id_agri_masa."',
      								id_des_masa = '".$id_des_masa."',
      								sep_env = '".$sep_env."',
      								compacta = '".$compacta."',
      								cavita = '".$cavita."'
      								WHERE id_has_movimiento_cilindro_pev = '".$id_has_movimiento_cilindro_pev."'";
      	$resultado3 = mysqli_query($con,$consulta3);
      	if($resultado3 == FALSE)
      	{
      		echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
      	}
      	else
      	{
      		?>
      		<script type="text/javascript">
      			alert("Inspección actualizada correctamente.");
      			window.location = 'insp_visual.php?id_orden_pev='+<?php echo $id_orden_pev; ?>;
      		</script>
      		<?php
      	}
      }
      else
      {
      	$consulta1 = "INSERT INTO inspeccion_acu (fusibles, id_expo_fue, id_retro_llama, id_remo_pintura, id_quema_sople, id_grietas,
                                                id_adul_info, id_abolla, id_cort_mues, id_corro_gen, id_corr_loc, id_corr_linea,
                                                id_corr_hen, lim_ros, est_ros, id_fil_mall, id_orden_pev,id_has_movimiento_cilindro_pev,fir_vacio,
                                                polvo_fino,agua,depo_acei,deco_masa,id_agri_masa,id_des_masa,sep_env,compacta,cavita)
                    			   VALUES ('".$fusibles."','".$id_expo_fuego."','".$id_retro_llama."','".$id_remo_pintura."','".$id_quema_sople."',
                            '".$id_grietas."','".$id_adul_info."','".$id_abolla."','".$id_cort_mues."',
                            '".$id_corro_gen."','".$id_corr_loc."','".$id_corr_linea."','".$id_corr_hen."','".$lim_ros."','".$est_ros."', 
                            '".$id_fil_mall."','".$id_orden_pev."','".$id_has_movimiento_cilindro_pev."','".$fir_vacio."','".$polvo_fino."','".$agua."',
                            '".$depo_acei."','".$deco_masa."','".$id_agri_masa."','".$id_des_masa."','".$sep_env."','".$compacta."','".$cavita."')";
        if(mysqli_query($con,$consulta1))
        {
            ?>
            <script type="text/javascript">
                var id_orden_pev = '<?php echo $id_orden_pev; ?>';
                alert("Inspección guardada correctamente.")
                window.location = 'insp_visual.php?id_orden_pev='+<?php echo $id_orden_pev; ?>;
            </script>
            <?php
        }
        else
        {
            echo "Error: " . $consulta1 . "<br>" . mysqli_error($con);
        }
      }
    }
?>
<style type="text/css">
    .custab{
        border: 1px solid #ccc;
        padding: 5px;
        margin: 5% 0;
        box-shadow: 3px 3px 2px #ccc;
        transition: 0.5s;
        border-spacing: 50px;
        }
    .custab:hover{
        box-shadow: 3px 3px 0px transparent;
        transition: 0.5s;
        }
</style>
<div id="main" role="main">
	<div id="content">	
      	<div class="" align="center">
			<h1  class="page-title txt-color-blueDark">CILINDROS ASIGNADOS A LA ORDEN PEV</h1>				
		</div>		
		<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">			
			<header>
				<span class="widget-icon"> <i class="fa fa-table"></i> </span>
				<h2>Cilindros PEV Disponibles Para Inspección Visual</h2>				
			</header>			
			<div>
				<div class="jarviswidget-editbox"></div>
				<div class="widget-body no-padding">
					<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
						<thead>
							<tr>
								<th>#</th>
								<th>Inspeccionar</th>									
								<th>Número Cilindro</th>
                <th>Material Cilindro</th>
							</tr>
						</thead>
						<tbody>
						<?php
            	$contador = "0";
    	        $consulta = "SELECT * FROM has_movimiento_cilindro_pev WHERE id_transporte_pev = (SELECT id_transporte_pev FROM orden_servicio_pev WHERE id_orden_pev = $id_orden_pev) ORDER BY id_has_movimiento_cilindro_pev ASC ";
                $resultado = mysqli_query($con,$consulta) ;
                while ($linea = mysqli_fetch_array($resultado))
                {                                    
                  $contador = $contador + 1;
                  $id_has_movimiento_cilindro_pev = $linea["id_has_movimiento_cilindro_pev"];
                  $num_cili = $linea["num_cili"];
                  $material_cilindro_u = $linea["material_cilindro_u"];
                  $consulta4 = "SELECT id_estado FROM cilindro_eto WHERE num_cili_eto = '$num_cili'";
                  $resultado4 = mysqli_query($con,$consulta4);
                  $linea4 = mysqli_fetch_assoc($resultado4);

                  $id_estado = isset($linea4["id_estado"]) ? $linea4["id_estado"] : NULL;
                  $consulta2 = "SELECT nombre FROM material_cilindro WHERE id_material = '".$material_cilindro_u."'";
                  $resultado2 = mysqli_query($con,$consulta2);

                  $consulta5 = "SELECT * FROM cilindro_pev WHERE num_cili_pev = '$num_cili'";
                  $resultado5 = mysqli_query($con,$consulta5);
                  $linea5 = mysqli_fetch_assoc($resultado5);

                  $tipo_cili_pev = isset($linea5["tipo_cili_pev"]) ? $linea5["tipo_cili_pev"] : 1;
                  if(mysqli_num_rows($resultado2) > 0)
                  {
                      $linea2 = mysqli_fetch_assoc($resultado2);
                      $nombre_material = $linea2['nombre'];
                  }
                  if($id_estado != 1){
                  ?>
                  <tr>
                      <td width="5"><?php echo $contador; ?></td>
                      <?php
                        if($material_cilindro_u == 1)
                        {
                          if($tipo_cili_pev == 1){
                          ?>
                          <td><a href="insp_vi_ace_pru_alta_presion.php?id_has_movimiento_cilindro_pev=<?php echo $id_has_movimiento_cilindro_pev;?>"><img src="img/edit.png" width="20" height="20"></a></td>
                          <?php
                          }
                          if($tipo_cili_pev == 2){
                          ?>
                          <td><a href="insp_vi_ace_pru_baja_presion.php?id_has_movimiento_cilindro_pev=<?php echo $id_has_movimiento_cilindro_pev;?>"><img src="img/edit.png" width="20" height="20"></a></td>
                          <?php
                          }
                          ?>
                          <!--<td><a href="insp_vi_ace.php?id_has_movimiento_cilindro_pev=<?php echo $id_has_movimiento_cilindro_pev;?>"><img src="img/edit.png" width="20" height="20"></a></td>-->
                          <?php
                        }
                        if($material_cilindro_u == 2)
                        {
                          ?>
                          <td><a href="insp_vi_alu_pru.php?id_has_movimiento_cilindro_pev=<?php echo $id_has_movimiento_cilindro_pev;?>"><img src="img/edit.png" width="20" height="20"></a></td>
                          <?php
                        }
                        if($material_cilindro_u == 3)
                        {
                          ?>
                          <td><a href="insp_vi_acu_pru.php?id_has_movimiento_cilindro_pev=<?php echo $id_has_movimiento_cilindro_pev;?>"><img src="img/edit.png" width="20" height="20"></a></td>
                          <?php
                        }
                      ?>                      
                      <td><?php echo $num_cili; ?></td>
                      <td><?php echo $nombre_material; ?></td>
                  </tr>     
                <?php
                  }
                }mysqli_free_result($resultado);                           	
            ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->
<?php
	include("inc/footer.php");
	include("inc/scripts.php"); 
?>
<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<script type="text/javascript">
// DO NOT REMOVE : GLOBAL FUNCTIONS!
$(document).ready(function() {
	
	/* // DOM Position key index //



		
	l - Length changing (dropdown)
	f - Filtering input (search)
	t - The Table! (datatable)
	i - Information (records)
	p - Pagination (paging)
	r - pRocessing 
	< and > - div elements
	<"#id" and > - div with an id
	<"class" and > - div with a class
	<"#id.class" and > - div with an id and class
	
	Also see: http://legacy.datatables.net/usage/features
	*/	
    // By Default Disable radio button
 
	/* BASIC ;*/
		var responsiveHelper_dt_basic = undefined;
		var responsiveHelper_datatable_fixed_column = undefined;
		var responsiveHelper_datatable_col_reorder = undefined;
		var responsiveHelper_datatable_tabletools = undefined;
		
		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};

		$('#dt_basic').dataTable({
			"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
			"autoWidth" : true,
			"preDrawCallback" : function() {
				// Initialize the responsive datatables helper once.
				if (!responsiveHelper_dt_basic) {
					responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
				}
			},
			"rowCallback" : function(nRow) {
				responsiveHelper_dt_basic.createExpandIcon(nRow);
			},
			"drawCallback" : function(oSettings) {
				responsiveHelper_dt_basic.respond();
			}
		});

	/* END BASIC */
	
	/* COLUMN FILTER  */
    var otable = $('#datatable_fixed_column').DataTable({
    	//"bFilter": false,
    	//"bInfo": false,
    	//"bLengthChange": false
    	//"bAutoWidth": false,
    	//"bPaginate": false,
    	//"bStateSave": true // saves sort state using localStorage
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_fixed_column) {
				responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_fixed_column.respond();
		}		
	
    });
    
    // custom toolbar
    $("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');
    	   
    // Apply the filter
    $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {
    	
        otable
            .column( $(this).parent().index()+':visible' )
            .search( this.value )
            .draw();
            
    } );
    /* END COLUMN FILTER */   

	/* COLUMN SHOW - HIDE */
	$('#datatable_col_reorder').dataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_col_reorder) {
				responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_col_reorder.respond();
		}			
	});
	
	/* END COLUMN SHOW - HIDE */

	/* TABLETOOLS */
	$('#datatable_tabletools').dataTable({
		
		// Tabletools options: 
		//   https://datatables.net/extensions/tabletools/button_options
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        "oTableTools": {
        	 "aButtons": [
             "copy",
             "csv",
             "xls",
                {
                    "sExtends": "pdf",
                    "sTitle": "SmartAdmin_PDF",
                    "sPdfMessage": "SmartAdmin PDF Export",
                    "sPdfSize": "letter"
                },
             	{
                	"sExtends": "print",
                	"sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>"
            	}
             ],
            "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
        },
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_tabletools) {
				responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_tabletools.respond();
		}
	});

	/* END TABLETOOLS */
})
</script>
<script type="text/javascript">
    function check()
    {
      if(document.getElementById("id_quema_arco1").checked == true || document.getElementById("id_corr_int1").checked == true || document.getElementById("id_adul_info1").checked == true || document.getElementById("id_corr_ext1").checked == true || document.getElementById("id_fisu_lineal1").checked == true || document.getElementById("id_corr_gene1").checked == true || document.getElementById("id_pica_ais1").checked == true || document.getElementById("id_abolladu1").checked == true || document.getElementById("id_abombam1").checked == true )
        {
          document.getElementById("p1").value = "NO APROBADO";
        }
      else
      {
        document.getElementById("p1").value = "APROBADO";
      }
    }
    
</script>
<script type="text/javascript">
  function check1()
  {
    if(document.getElementById("remocion_etiquetas1").checked == true || document.getElementById("remocion_pintura1").checked == true || document.getElementById("adulteracion_informacion1").checked == true || document.getElementById("corrosion_general1").checked == true || document.getElementById("corrosion_lineal1").checked == true || document.getElementById("cortes_perforaciones1").checked == true || document.getElementById("abolladuras_al1").checked == true || document.getElementById("abombamiento_al1").checked == true || document.getElementById("fuego_al1").checked == true || document.getElementById("termico_al1").checked == true || document.getElementById("pandeo_al1").checked == true || document.getElementById("grietas_al1").checked == true || document.getElementById("pliegues_al1").checked == true || document.getElementById("valles_al1").checked == true)
      {
        document.getElementById("p2").innerHTML = "NO APROBADO";
      }
    else
    {
      document.getElementById("p2").innerHTML = "APROBADO";
    }
  }
</script>
<script type="text/javascript">
  function check2()
  {
    if(document.getElementById("fusibles1").checked == true || document.getElementById("id_expo_fuego1").checked == true || document.getElementById("id_retro_llama1").checked == true || document.getElementById("id_remo_pintura_ac1").checked == true || document.getElementById("id_abomba_ac1").checked == true || document.getElementById("id_grietas1").checked == true || document.getElementById("id_adul_info_ac1").checked == true || document.getElementById("id_quema_sople1").checked == true || document.getElementById("id_abolla1").checked == true || document.getElementById("id_cort_mues1").checked == true || document.getElementById("id_corro_gen1").checked == true || document.getElementById("id_corr_loc1").checked == true || document.getElementById("id_corr_linea1").checked == true || document.getElementById("id_corr_hen1").checked == true || document.getElementById("lim_ros1").checked == true || document.getElementById("est_ros2").checked == true || document.getElementById("id_fill_mall1").checked == true || document.getElementById("fir_vacio1").checked == true || document.getElementById("polvo_fino1").checked == true || document.getElementById("agua1").checked == true || document.getElementById("depo_acei1").checked == true || document.getElementById("deco_masa1").checked == true || document.getElementById("id_agri_masa1").checked == true || document.getElementById("id_des_masa1").checked == true || document.getElementById("sep_env1").checked == true || document.getElementById("compacta1").checked == true || document.getElementById("cavita1").checked == true)
      {
        document.getElementById("p3").innerHTML = "NO APROBADO";
      }
    else
    {
      document.getElementById("p3").innerHTML = "APROBADO";
    }
  }
</script>
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script type="text/javascript">
		$(document).ready(function() {
			
			// menu
			$("#menu").menu();
		
			/*
			 * AUTO COMPLETE AJAX
			 */
		
			function log(message) {
				$("<div>").text(message).prependTo("#log");
				$("#log").scrollTop(0);
			}
		
			$("#city").autocomplete({
				source : function(request, response) {
					$.ajax({
						url : "http://ws.geonames.org/searchJSON",
						dataType : "jsonp",
						data : {
							featureClass : "P",
							style : "full",
							maxRows : 12,
							name_startsWith : request.term
						},
						success : function(data) {
							response($.map(data.geonames, function(item) {
								return {
									label : item.name + (item.adminName1 ? ", " + item.adminName1 : "") + ", " + item.countryName,
									value : item.name
								}
							}));
						}
					});
				},
				minLength : 2,
				select : function(event, ui) {
					log(ui.item ? "Selected: " + ui.item.label : "Nothing selected, input was " + this.value);
				}
			});
		
			/*
			 * Spinners
			 */
			$("#spinner").spinner();
			$("#spinner-decimal").spinner({
				step : 0.01,
				numberFormat : "n"
			});
		
			$("#spinner-currency").spinner({
				min : 5,
				max : 2500,
				step : 25,
				start : 1000,
				numberFormat : "C"
			});
		
			/*
			 * CONVERT DIALOG TITLE TO HTML
			 * REF: http://stackoverflow.com/questions/14488774/using-html-in-a-dialogs-title-in-jquery-ui-1-10
			 */
			$.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
				_title : function(title) {
					if (!this.options.title) {
						title.html("&#160;");
					} else {
						title.html(this.options.title);
					}
				}
			}));
		
		
			/*
			* DIALOG SIMPLE
			*/
		
			// Dialog click
			$('#dialog_link').click(function() {
				$('#dialog_simple').dialog('open');
				return false;
		
			});
		
			$('#dialog_simple').dialog({
				autoOpen : false,
				width : 600,
				resizable : false,
				modal : true,
				title : "<div class='widget-header'><h4><i class='fa fa-warning'></i> Empty the recycle bin?</h4></div>",
				buttons : [{
					html : "<i class='fa fa-trash-o'></i>&nbsp; Delete all items",
					"class" : "btn btn-danger",
					click : function() {
						$(this).dialog("close");
					}
				}, {
					html : "<i class='fa fa-times'></i>&nbsp; Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
					}
				}]
			});
		
			/*
			* DIALOG HEADER ICON
			*/
		
			// Modal Link
			$('#modal_link').click(function() {
				$('#dialog-message').dialog('open');
				return false;
			});
		
			$("#dialog-message").dialog({
				autoOpen : false,
				modal : true,
				title : "<div class='widget-header'><h4><i class='icon-ok'></i> jQuery UI Dialog</h4></div>",
				buttons : [{
					html : "Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
					}
				}]
		
			});
		
			/*
			 * Remove focus from buttons
			 */
			$('.ui-dialog :button').blur();
		
			/*
			 * Just Tabs
			 */
		
			$('#tabs').tabs();
		
			/*
			 *  Simple tabs adding and removing
			 */
		
			$('#tabs2').tabs();
		
			// Dynamic tabs
			var tabTitle = $("#tab_title"), tabContent = $("#tab_content"), tabTemplate = "<li style='position:relative;'> <span class='air air-top-left delete-tab' style='top:7px; left:7px;'><button class='btn btn-xs font-xs btn-default hover-transparent'><i class='fa fa-times'></i></button></span></span><a href='#{href}'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; #{label}</a></li>", tabCounter = 2;
		
			var tabs = $("#tabs2").tabs();
		
			// modal dialog init: custom buttons and a "close" callback reseting the form inside
			var dialog = $("#addtab").dialog({
				autoOpen : false,
				width : 600,
				resizable : false,
				modal : true,
				buttons : [{
					html : "<i class='fa fa-times'></i>&nbsp; Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
		
					}
				}]
			});
		
			// addTab form: calls addTab function on submit and closes the dialog
			var form = dialog.find("form").submit(function(event) {
				addTab();
				dialog.dialog("close");
				
			});
		
			// actual addTab function: adds new tab using the input from the form above

			
			function addTab() 
			{
				
				var label = tabTitle.val() || "Tab " + tabCounter, id = "tabs-" + tabCounter, li = $(tabTemplate.replace(/#\{href\}/g, "#" + id).replace(/#\{label\}/g, label)), tabContentHtml = tabContent.val() || "Tab " + tabCounter + " content.";
		
				tabs.find(".ui-tabs-nav").append(li);
				tabs.append("<div id='" + id + "'><p>" + tabContentHtml + "</p></div>");
				tabs.tabs("refresh");
				tabCounter++;
		
				// clear fields
				$("#tab_title").val("");
				$("#tab_content").val("");
			}
		
			// addTab button: just opens the dialog
			$("#add_tab").button().click(function() {
				var idd = document.getElementById('num').firstChild.nodeValue;

				
				dialog.dialog("open");
			});
		
			// close icon: removing the tab on click
					
			/*
			* ACCORDION
			*/
			//jquery accordion
			
		     var accordionIcons = {
		         header: "fa fa-plus",    // custom icon class
		         activeHeader: "fa fa-minus" // custom icon class
		     };
		     
			$("#accordion").accordion({
				autoHeight : false,
				heightStyle : "content",
				collapsible : true,
				animate : 300,
				icons: accordionIcons,
				header : "h4",
			})
		
			/*
			 * PROGRESS BAR
			 */
			$("#progressbar").progressbar({
		     	value: 25,
		     	create: function( event, ui ) {
		     		$(this).removeClass("ui-corner-all").addClass('progress').find(">:first-child").removeClass("ui-corner-left").addClass('progress-bar progress-bar-success');
				}
			});			

		})

</script>

<?php 
	//include footer
	include("inc/google-analytics.php"); 
}
else
{
    header("Location:index.php");
}
?>