<?php
require ("../../libraries/conexion.php");
require '../../PHPExcel.php';
date_default_timezone_set('America/Bogota');
$Fecha = date("Y-m-d");

$sucursal = $_REQUEST["sucursal"];
$contador = 0;

//--------------------------------------------------------------------------------------------------------------

$consulta5 = "DELETE FROM comisiones WHERE sCliente = 'sCliente'";
$resultado5 = mysqli_query($con, $consulta5);

$consulta6 = "DELETE FROM comisiones1 WHERE sCliente = 'sCliente'";
$resultado6 = mysqli_query($con, $consulta6);

$consulta7 = "DELETE FROM comisiones2 WHERE sCliente = 'sCliente'";
$resultado7 = mysqli_query($con, $consulta7);

$consulta8 = "DELETE FROM comisiones3 WHERE sCliente = 'sCliente'";
$resultado8 = mysqli_query($con, $consulta8);

//--------------------------------------------------------------------------------------------------------------

$consulta9 = "DELETE FROM comisiones WHERE sTipo_documento NOT IN ('FACE', 'DESAPLICACION')";
$resultado9 = mysqli_query($con, $consulta9);

$consulta10 = "DELETE FROM comisiones1 WHERE sTipo_documento NOT IN ('FACE', 'DESAPLICACION')";
$resultado10 = mysqli_query($con, $consulta10);

$consulta11 = "DELETE FROM comisiones2 WHERE sTipo_documento NOT IN ('FACE', 'DESAPLICACION')";
$resultado11 = mysqli_query($con, $consulta11);

$consulta12 = "DELETE FROM comisiones3 WHERE sTipo_documento NOT IN ('FACE', 'DESAPLICACION')";
$resultado12 = mysqli_query($con, $consulta12);

//--------------------------------------------------------------------------------------------------------------

$consulta13 = "DELETE FROM comisiones WHERE sCodigo_documento_pago <> 'RECIBO' AND sCodigo_documento_pago <> 'NCE'";
$resultado13 = mysqli_query($con, $consulta13);

$consulta14 = "DELETE FROM comisiones1 WHERE sCodigo_documento_pago <> 'RECIBO' AND sCodigo_documento_pago <> 'NCE'";
$resultado14 = mysqli_query($con, $consulta14);

$consulta15 = "DELETE FROM comisiones2 WHERE sCodigo_documento_pago <> 'RECIBO' AND sCodigo_documento_pago <> 'NCE'";
$resultado15 = mysqli_query($con, $consulta15);

$consulta16 = "DELETE FROM comisiones3 WHERE sCodigo_documento_pago <> 'RECIBO' AND sCodigo_documento_pago <> 'NCE'";
$resultado16 = mysqli_query($con, $consulta16);

//--------------------------------------------------------------------------------------------------------------

$consulta2 = "SELECT * FROM comisiones1 WHERE sucursal = '$sucursal'";
$resultado2 = mysqli_query($con, $consulta2);


if(mysqli_num_rows($resultado2)>0){
  while($linea = mysqli_fetch_array($resultado2)){
    $sCliente = $linea["sCliente"];
    $sNombre_cliente = $linea["sNombre_cliente"];
    $nOrden1 = $linea["nOrden1"];
    $nOrden2 = $linea["nOrden2"];
    $sTipo_documento = $linea["sTipo_documento"];
    $sNumero_documento = $linea["sNumero_documento"];
    $sCodigo_documento_pago = $linea["sCodigo_documento_pago"];
    $sNro_recibo_manual = $linea["sNro_recibo_manual"];
    $sNumero_documento_pago = $linea["sNumero_documento_pago"];
    $nItem_documento_pago = $linea["nItem_documento_pago"];
    $dFecha_emision_documento = $linea["dFecha_emision_documento"];
    $dFecha_vencimiento_documento = $linea["dFecha_vencimiento_documento"];
    $dFecha_aplicacion_documento = $linea["dFecha_aplicacion_documento"];
    $sMoneda_aplicacion = $linea["sMoneda_aplicacion"];
    $sMoneda_origen_documento = $linea["sMoneda_origen_documento"];
    $nTipo_de_cambio = $linea["nTipo_de_cambio"];
    $nDebe = $linea["nDebe"];
    $nHaber = $linea["nHaber"];
    $s2DoCorte_control = $linea["s2DoCorte_control"];
    $dFecha_des = $linea["dFecha_des"];
    $sSucursal_destinataria = $linea["sSucursal_destinataria"];
    $nSaldoOrigen = $linea["nSaldoOrigen"];
    $nwSaldoPorBloque = $linea["nwSaldoPorBloque"];
    $dFecha_rechazo = $linea["dFecha_rechazo"];
    $sClienteYMoneda = $linea["sClienteYMoneda"];
    $sImprimoLineaDetalle = $linea["sImprimoLineaDetalle"];
    $sSub_business_area = $linea["sSub_business_area"];
    $sDomicilio_legal = $linea["sDomicilio_legal"];
    $sTelefono_legal = $linea["sTelefono_legal"];
        
    $consulta3 = "SELECT * FROM comisiones WHERE sNumero_documento_pago = '$sNumero_documento_pago' AND sucursal = '$sucursal' AND nHaber = '$nHaber' AND s2DoCorte_control = '$s2DoCorte_control'";
    $resultado3 = mysqli_query($con, $consulta3);
    
    if(mysqli_num_rows($resultado3)==0){
      $consulta4 = "INSERT INTO comisiones(sCliente,	sNombre_cliente,	nOrden1,	nOrden2,	sTipo_documento,	sNumero_documento,	
      sCodigo_documento_pago,	sNro_recibo_manual,	sNumero_documento_pago,	nItem_documento_pago,	dFecha_emision_documento,	
      dFecha_vencimiento_documento,	dFecha_aplicacion_documento,	sMoneda_aplicacion,	sMoneda_origen_documento,	nTipo_de_cambio,	nDebe,	
      nHaber,	s2DoCorte_control,	dFecha_des,	sSucursal_destinataria,	nSaldoOrigen,	nwSaldoPorBloque,	dFecha_rechazo,	sClienteYMoneda,	
      sImprimoLineaDetalle,	sSub_business_area,	sDomicilio_legal,	sTelefono_legal, sucursal) VALUES ('$sCliente', '$sNombre_cliente', '$nOrden1', '$nOrden2', '$sTipo_documento', '$sNumero_documento', 
      '$sCodigo_documento_pago', '$sNro_recibo_manual', '$sNumero_documento_pago', '$nItem_documento_pago', '$dFecha_emision_documento', 
      '$dFecha_vencimiento_documento', '$dFecha_aplicacion_documento', '$sMoneda_aplicacion', '$sMoneda_origen_documento', '$nTipo_de_cambio', 
      '$nDebe', '$nHaber', '$s2DoCorte_control', '$dFecha_des', '$sSucursal_destinataria', '$nSaldoOrigen', '$nwSaldoPorBloque', '$dFecha_rechazo', 
      '$sClienteYMoneda', '$sImprimoLineaDetalle', '$sSub_business_area', '$sDomicilio_legal', '$sTelefono_legal', '$sucursal')";

      $resultado4 = mysqli_query($con, $consulta4);
    }
  }
}



if ($tipo=1) 
{
  $mes = date("m");
  $fila = 2;
  $objPHPExcel = new PHPExcel();
  $objPHPExcel->getProperties()->setCreator("Ingegas")->setDescription("Consolidado Recibos");
  $objPHPExcel->setActiveSheetIndex(0);

  $objPHPExcel->getActiveSheet()->setTitle($sucursal);

  $objPHPExcel->getActiveSheet()->setCellValue('A1', "sCliente");
  $objPHPExcel->getActiveSheet()->setCellValue('B1', "sNombre_cliente");
  $objPHPExcel->getActiveSheet()->setCellValue('C1', "nOrden1");
  $objPHPExcel->getActiveSheet()->setCellValue('D1', "nOrden2");
  $objPHPExcel->getActiveSheet()->setCellValue('E1', "sTipo_documento");
  $objPHPExcel->getActiveSheet()->setCellValue('F1', "sNumero_documento");
  $objPHPExcel->getActiveSheet()->setCellValue('G1', "sCodigo_documento_pago");
  $objPHPExcel->getActiveSheet()->setCellValue('H1', "sNro_recibo_manual");
  $objPHPExcel->getActiveSheet()->setCellValue('I1', "sNumero_documento_pago");
  $objPHPExcel->getActiveSheet()->setCellValue('J1', "nItem_documento_pago");
  $objPHPExcel->getActiveSheet()->setCellValue('K1', "dFecha_emision_documento");
  $objPHPExcel->getActiveSheet()->setCellValue('L1', "dFecha_vencimiento_documento");
  $objPHPExcel->getActiveSheet()->setCellValue('M1', "dFecha_aplicacion_documento");
  $objPHPExcel->getActiveSheet()->setCellValue('N1', "sMoneda_aplicacion");
  $objPHPExcel->getActiveSheet()->setCellValue('O1', "sMoneda_origen_documento");
  $objPHPExcel->getActiveSheet()->setCellValue('P1', "nTipo_de_cambio");
  $objPHPExcel->getActiveSheet()->setCellValue('Q1', "nDebe");
  $objPHPExcel->getActiveSheet()->setCellValue('R1', "nHaber");
  $objPHPExcel->getActiveSheet()->setCellValue('S1', "s2DoCorte_control");
  $objPHPExcel->getActiveSheet()->setCellValue('T1', "dFecha_des");
  $objPHPExcel->getActiveSheet()->setCellValue('U1', "sSucursal_destinataria");
  $objPHPExcel->getActiveSheet()->setCellValue('V1', "nSaldoOrigen");
  $objPHPExcel->getActiveSheet()->setCellValue('W1', "nwSaldoPorBloque");
  $objPHPExcel->getActiveSheet()->setCellValue('X1', "dFecha_rechazo");
  $objPHPExcel->getActiveSheet()->setCellValue('Y1', "sClienteYMoneda");
  $objPHPExcel->getActiveSheet()->setCellValue('Z1', "sImprimoLineaDetalle");
  $objPHPExcel->getActiveSheet()->setCellValue('AA1', "sSub_business_area");
  $objPHPExcel->getActiveSheet()->setCellValue('AB1', "sDomicilio_legal");
  $objPHPExcel->getActiveSheet()->setCellValue('AC1', "sTelefono_legal");

  $consultaDesapli = "SELECT * FROM comisiones WHERE sTipo_documento = 'DESAPLICACION' AND sucursal = '$sucursal'";
  $queryDesapli = mysqli_query($con, $consultaDesapli);
  while ($rowdesapli = mysqli_fetch_array($queryDesapli)) {
    $Tdocu = $rowdesapli['sTipo_documento'];
    $NdocuP = $rowdesapli['sNumero_documento_pago'];
    //echo $Tdocu." - ".$NdocuP."<br>";
  }
  
  $eliminaDesapli = "SELECT * FROM comisiones WHERE sNumero_documento_pago = '$NdocuP'";
  $queryEliDesapli = mysqli_query($con, $eliminaDesapli);
  while ($rowEliDesapli = mysqli_fetch_array($queryEliDesapli)) {
    $Tdocu2 = $rowEliDesapli['sTipo_documento'];
    $NdocuP2 = $rowEliDesapli['sNumero_documento_pago'];
    echo $Tdocu2." - ".$NdocuP2."<br>";

    $trash = "DELETE FROM comisiones WHERE sNumero_documento_pago = '$NdocuP'";
    $queryTrash = mysqli_query($con, $trash);
  }  

  //SELECT * FROM comisiones WHERE sucursal = '$sucursal'
  $consulta = "SELECT * FROM comisiones WHERE sucursal = '$sucursal' AND sTipo_documento != 'DESAPLICACION' ORDER BY sNumero_documento_pago DESC";
  $resultado = mysqli_query($con, $consulta);

  while($linea = mysqli_fetch_array($resultado)){

    $mostrar = 1;
    $contador++;

    $sCliente = $linea["sCliente"];
    $sNombre_cliente = $linea["sNombre_cliente"];
    $nOrden1 = $linea["nOrden1"];
    $nOrden2 = $linea["nOrden2"];
    $sTipo_documento = $linea["sTipo_documento"];
    $sNumero_documento = $linea["sNumero_documento"];
    $sCodigo_documento_pago = $linea["sCodigo_documento_pago"];
    $sNro_recibo_manual = $linea["sNro_recibo_manual"];
    $sNumero_documento_pago = $linea["sNumero_documento_pago"];
    $nItem_documento_pago = $linea["nItem_documento_pago"];
    $dFecha_emision_documento = $linea["dFecha_emision_documento"];
    $dFecha_vencimiento_documento = $linea["dFecha_vencimiento_documento"];
    $dFecha_aplicacion_documento = $linea["dFecha_aplicacion_documento"];
    $sMoneda_aplicacion = $linea["sMoneda_aplicacion"];
    $sMoneda_origen_documento = $linea["sMoneda_origen_documento"];
    $nTipo_de_cambio = $linea["nTipo_de_cambio"];
    $nDebe = $linea["nDebe"];
    $nHaber = $linea["nHaber"];
    $s2DoCorte_control = $linea["s2DoCorte_control"];
    $dFecha_des = $linea["dFecha_des"];
    $sSucursal_destinataria = $linea["sSucursal_destinataria"];
    $nSaldoOrigen = $linea["nSaldoOrigen"];
    $nwSaldoPorBloque = $linea["nwSaldoPorBloque"];
    $dFecha_rechazo = $linea["dFecha_rechazo"];
    $sClienteYMoneda = $linea["sClienteYMoneda"];
    $sImprimoLineaDetalle = $linea["sImprimoLineaDetalle"];
    $sSub_business_area = $linea["sSub_business_area"];
    $sDomicilio_legal = $linea["sDomicilio_legal"];
    $sTelefono_legal = $linea["sTelefono_legal"];

    $sNumero_documento_pago = str_replace(' ', '', $sNumero_documento_pago);

    if($contador == 1){
      $numInicial = $linea["sNumero_documento_pago"];
      $numInicial = intval($numInicial);
      $numAnterior = $numInicial;
    }

    $numActual = $linea["sNumero_documento_pago"];
    $numActual = intval($numActual);

    if($numAnterior == $numActual){
      $numInicial--;
    }

    $nHaber = str_replace(",","", $nHaber);
    $nHaber = floatval($nHaber);

    if($nHaber > 5){
      $mostrar = 1;
    }else{
      $mostrar = 0;
    }

    if($mostrar == 1){
      $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $sCliente);
      $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $sNombre_cliente);
      $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $nOrden1);
      $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $nOrden2);
      $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $sTipo_documento);
      $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $sNumero_documento);
      $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $sCodigo_documento_pago);
      $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $sNro_recibo_manual);
      if($numInicial != $numActual || $numActual != $numAnterior){
        $objPHPExcel->getActiveSheet()->getStyle('I'.$fila)->applyFromArray(
          array(
            'fill' => array(
              'type' => PHPExcel_Style_Fill::FILL_SOLID,
              'color' => array('rgb' => '78CCF9')
            )
          )
        );
        $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $sNumero_documento_pago);
      }else{
        $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $sNumero_documento_pago);
      }
      
      $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, $nItem_documento_pago);
      $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $dFecha_emision_documento);
      $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, $dFecha_vencimiento_documento);
      $objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, $dFecha_aplicacion_documento);
      $objPHPExcel->getActiveSheet()->setCellValue('N'.$fila, $sMoneda_aplicacion);
      $objPHPExcel->getActiveSheet()->setCellValue('O'.$fila, $sMoneda_origen_documento);
      $objPHPExcel->getActiveSheet()->setCellValue('P'.$fila, $nTipo_de_cambio);
      $objPHPExcel->getActiveSheet()->setCellValue('Q'.$fila, $nDebe);
      $objPHPExcel->getActiveSheet()->setCellValue('R'.$fila, $nHaber);
      $objPHPExcel->getActiveSheet()->setCellValue('S'.$fila, $s2DoCorte_control);
      $objPHPExcel->getActiveSheet()->setCellValue('T'.$fila, $dFecha_des);
      $objPHPExcel->getActiveSheet()->setCellValue('U'.$fila, $sSucursal_destinataria);
      $objPHPExcel->getActiveSheet()->setCellValue('V'.$fila, $nSaldoOrigen);
      $objPHPExcel->getActiveSheet()->setCellValue('W'.$fila, $nwSaldoPorBloque);
      $objPHPExcel->getActiveSheet()->setCellValue('X'.$fila, $dFecha_rechazo);
      $objPHPExcel->getActiveSheet()->setCellValue('Y'.$fila, $sClienteYMoneda);
      $objPHPExcel->getActiveSheet()->setCellValue('Z'.$fila, $sImprimoLineaDetalle);
      $objPHPExcel->getActiveSheet()->setCellValue('AA'.$fila, $sSub_business_area);
      $objPHPExcel->getActiveSheet()->setCellValue('AB'.$fila, $sDomicilio_legal);
      $objPHPExcel->getActiveSheet()->setCellValue('AC'.$fila, $sTelefono_legal);
    

      $fila++;
    }

    
    $numInicial++;
    $numAnterior = $numActual;
  }
 
    header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	  header('Content-Disposition: attachment;filename="ConsolidadoRecibosdeCaja.xlsx"');
	  header('Cache-Control: max-age=0');
    $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $writer->save('php://output');
}
?>
