<?php
require ("../../libraries/conexion.php");
require '../../PHPExcel.php';
date_default_timezone_set('America/Bogota');
$Fecha = date("Y-m-d");
$recaudo_gases = 0;
$total_base_comision = 0;
$suma_iva = 0;
$suma_total_con_iva = 0;
$basetra_total = 0;

if ($tipo=1) 
{
  $mes = date("m");
  $fila = 5;
  $objPHPExcel = new PHPExcel();
  $objPHPExcel->getProperties()->setCreator("Ingegas")->setDescription("Comisiones Ingegas");
  $objPHPExcel->setActiveSheetIndex(0);

  $objPHPExcel->getActiveSheet()->setTitle("IG");
  $objPHPExcel->getActiveSheet()->setCellValue('E1', $mes);
  $objPHPExcel->getActiveSheet()->setCellValue('E2', "HH");
  $objPHPExcel->getActiveSheet()->mergeCells('K3:O3');

  $objPHPExcel->getActiveSheet()->setCellValue('K3', "COMISIONES");

  $objPHPExcel->getActiveSheet()->setCellValue('A4', "# Recibo de pago");
  $objPHPExcel->getActiveSheet()->setCellValue('B4', "Fecha de Aplicación del pago");
  $objPHPExcel->getActiveSheet()->setCellValue('C4', "Código Cliente");
  $objPHPExcel->getActiveSheet()->setCellValue('D4', "Cliente");
  $objPHPExcel->getActiveSheet()->setCellValue('E4', "Valor aplicado");
  $objPHPExcel->getActiveSheet()->setCellValue('F4', "Base comisión");
  $objPHPExcel->getActiveSheet()->setCellValue('G4', "Documento");
  $objPHPExcel->getActiveSheet()->setCellValue('H4', "# Documento");
  $objPHPExcel->getActiveSheet()->setCellValue('I4', "fecha_doc");
  $objPHPExcel->getActiveSheet()->setCellValue('J4', "# Días");
  $objPHPExcel->getActiveSheet()->setCellValue('K4', "30%");
  $objPHPExcel->getActiveSheet()->setCellValue('L4', "29,5%");
  $objPHPExcel->getActiveSheet()->setCellValue('M4', "29%");
  $objPHPExcel->getActiveSheet()->setCellValue('N4', "25%");
  $objPHPExcel->getActiveSheet()->setCellValue('O4', "20%");
  $objPHPExcel->getActiveSheet()->setCellValue('P4', "Mercado");
  $objPHPExcel->getActiveSheet()->setCellValue('Q4', "1% casa cobranza");
  $objPHPExcel->getActiveSheet()->setCellValue('R4', "TRANS");
  $objPHPExcel->getActiveSheet()->setCellValue('S4', "TR -IV");
  $objPHPExcel->getActiveSheet()->setCellValue('T4', "IVA");
  $objPHPExcel->getActiveSheet()->setCellValue('U4', "Total ");
  $objPHPExcel->getActiveSheet()->setCellValue('V4', "DIF");
  $objPHPExcel->getActiveSheet()->setCellValue('W4', "Base +tra (com");

  $consulta = "SELECT * FROM comisiones2 WHERE sCodigo_documento_pago = 'RECIBO' AND sSub_business_area = 'HH' AND sucursal = '051' ORDER BY sNumero_documento";
  $resultado = mysqli_query($con, $consulta);

  while($linea = mysqli_fetch_array($resultado)){
    $mostrar = 1;
    $sNumero_documento = $linea["sNumero_documento"];
    $dFecha_aplicacion_documento=$linea['dFecha_aplicacion_documento'];
    $sCliente = $linea["sCliente"];
    $sNombre_cliente = $linea["sNombre_cliente"];
    $nHaber = $linea["nHaber"];
    $sNumero_documento_pago = $linea["sNumero_documento_pago"];
    $sTipo_documento = $linea["sTipo_documento"];
    $sSub_business_area = $linea["sSub_business_area"];

    $nHaber = str_replace(",","", $nHaber);
    $nHaber = floatval($nHaber);

    $sNumero_documento = str_replace(' ', '', $sNumero_documento);

    $trans = 0;
    $trans_iva = 0;
    $trans_total = 0;
    $nHaber_total = $nHaber;
    $res = 0;

    $consulta1 = "SELECT * FROM ventas_producto WHERE numero_legal = '$sNumero_documento' ";
    $resultado1 = mysqli_query($con, $consulta1);

    if(mysqli_num_rows($resultado1)>0){
      while($linea1 = mysqli_fetch_array($resultado1)){
        $nombre_producto_servicio = $linea1["nombre_producto_servicio"];
        $valor_total = $linea1["valor_total"];

        $consulta2 = "SELECT * FROM unneg WHERE descripcion LIKE '%$nombre_producto_servicio%'";
        $resultado2 = mysqli_query($con, $consulta2);

        $linea2 = mysqli_fetch_array($resultado2);

        $tipo = $linea2["tipo"];
        $id_unneg = $linea2["id_unneg"];

        if($tipo == "T"){
          $trans = $trans + $linea1["valor_total"];
          $trans_iva = $trans/1.19;
          $trans_mas_iva = $linea1["valor_total"] * 1.19;
          $trans_total += $trans_mas_iva;
        }else if($tipo == "G 16,5%"){
          $mostrar = 1;
        }

        $pos = strpos($tipo, '%');

        if($pos != false){
          if($id_unneg != "65"){
            $valor_total_iva = $valor_total*1.19;
            $nHaber_total = $nHaber_total - $valor_total_iva;
            $res = 1;
          }
        }
      }
      if($res == 1){
        //$nHaber_total = $nHaber_total*1.19;
        $nHaber = $nHaber_total;
      }
      
    }else{
      $trans = 0;
      $trans_iva = 0;
      $tipo = "";
    }

    $base_comision = ($nHaber-$trans_total)/1.19;

    $consulta3 = "SELECT * FROM ventas_acumuladas WHERE numero_legal = '$sNumero_documento'";
    $resultado3 = mysqli_query($con, $consulta3);

    if(mysqli_num_rows($resultado3)>0){
      $linea3 = mysqli_fetch_array($resultado3);
      
      $fecha_doc_fact = $linea3["fecha_doc"];
      $total_con_iva = $linea3["total_con_iva"];
      $iva = $linea3["iva"];
    }else{
      $fecha_doc_fact = "-";
      $total_con_iva = $nHaber;
      $iva = $nHaber*0.19;
      $mostrar = 0;
    }

    $diferencia_dias = 30;

    $dFecha_aplicacion_documento = explode(" ", $dFecha_aplicacion_documento);

    $dFecha_aplicacion_documento = $dFecha_aplicacion_documento[0];
    $dFecha_aplicacion_documento = str_replace("/","-",$dFecha_aplicacion_documento);

    $dFecha_aplicacion_documento = date("Y-m-d", strtotime($dFecha_aplicacion_documento));

    $fecha_doc_fact = str_replace("/","-",$fecha_doc_fact);

    $fecha_doc_fact = date("Y-m-d", strtotime($fecha_doc_fact));

    if($fecha_doc_fact != "-"){
      $fecha1 = new DateTime($fecha_doc_fact);
      $fecha2 = new DateTime($dFecha_aplicacion_documento);
  
      $diff = $fecha2->diff($fecha1);
  
      $diferencia_dias = $diff->days;
    }else{
      $diferencia_dias = 0;
    }
    

    $treinta = 0;
    $veintinuevecinco = 0;
    $veintinueve = 0;
    $veinticinco = 0;
    $veinte = 0;
    

    if($diferencia_dias == 0){
      $treinta = $base_comision*0.3;
      $veintinuevecinco = 0;
      $veintinueve = 0;
      $veinticinco = 0;
      $veinte = 0;

      $suma_treinta += $treinta;

    }else if($diferencia_dias>=1 && $diferencia_dias <= 30){
      $veintinuevecinco = $base_comision*0.295;
      
      $veintinueve = 0;
      $veinticinco = 0;
      $veinte = 0;

      $suma_veintinuevecinco += $veintinuevecinco;

    }else if($diferencia_dias>=31 && $diferencia_dias <= 75){
      $veintinueve = $base_comision*0.29;
      $veinticinco = 0;
      $veinte = 0;

      $suma_veintinueve += $veintinueve;

    }else if($diferencia_dias>=76 && $diferencia_dias <= 120){
      $veinticinco = $base_comision*0.25;
      $veinte = 0;

      $suma_veinticinco += $veinticinco;

    }else if($diferencia_dias>=121 && $diferencia_dias <= 149){
      $veinte = $base_comision*0.20;

      $suma_veinte += $veinte;
    }

    $total_con_iva = floatval($total_con_iva);

    $dif = $nHaber - $total_con_iva;
    $basetra = $base_comision + 0;

    $recaudo_gases += $nHaber;
    $total_base_comision += $base_comision;

    $iva = str_replace(",","", $iva);
    $iva = floatval($iva);

    
    $suma_iva += $iva;
    
    $total_con_iva = str_replace(",","", $total_con_iva);
    $total_con_iva = floatval($total_con_iva);

    
    $suma_total_con_iva += $total_con_iva;

    $basetra = floatval($basetra);
    $basetra_total += $basetra;

    if(strlen($sNumero_documento_pago)>=6){
      $pagoBta = $base_comision*0.01;
    }else{
      $pagoBta = "";
    }

    if($mostrar == 1){
      $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $sNumero_documento_pago);
      $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $dFecha_aplicacion_documento);
      $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $sCliente);
      $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $sNombre_cliente);
      $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $nHaber);
      $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $base_comision);
      $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $sTipo_documento);
      $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $sNumero_documento);
      $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $fecha_doc_fact);
      $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, $diferencia_dias);
      $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $treinta);
      $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, $veintinuevecinco);
      $objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, $veintinueve);
      $objPHPExcel->getActiveSheet()->setCellValue('N'.$fila, $veinticinco);
      $objPHPExcel->getActiveSheet()->setCellValue('O'.$fila, $veinte);
      $objPHPExcel->getActiveSheet()->setCellValue('P'.$fila, $sSub_business_area);
      $objPHPExcel->getActiveSheet()->setCellValue('Q'.$fila, $tipo);
      $objPHPExcel->getActiveSheet()->setCellValue('R'.$fila, $trans_total);
      $objPHPExcel->getActiveSheet()->setCellValue('S'.$fila, $trans);
      $objPHPExcel->getActiveSheet()->setCellValue('T'.$fila, $iva);
      $objPHPExcel->getActiveSheet()->setCellValue('U'.$fila, $total_con_iva);
      $objPHPExcel->getActiveSheet()->setCellValue('V'.$fila, $dif);
      $objPHPExcel->getActiveSheet()->setCellValue('W'.$fila, $basetra);


      $fila++;
    }

  }

  $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $recaudo_gases);
  $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $total_base_comision);
  
  $objPHPExcel->getActiveSheet()->mergeCells('G'.$fila.':J'.$fila);

  $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, "SUBTOTALES COMISION GASES");

  $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $suma_treinta);
  $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, $suma_veintinuevecinco);
  $objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, $suma_veintinueve);
  $objPHPExcel->getActiveSheet()->setCellValue('N'.$fila, $suma_veinticinco);
  $objPHPExcel->getActiveSheet()->setCellValue('O'.$fila, $suma_veinte);
  $objPHPExcel->getActiveSheet()->setCellValue('R'.$fila, "0");
  $objPHPExcel->getActiveSheet()->setCellValue('S'.$fila, "0");
  $objPHPExcel->getActiveSheet()->setCellValue('T'.$fila, $suma_iva);
  $objPHPExcel->getActiveSheet()->setCellValue('U'.$fila, $suma_total_con_iva);
  $objPHPExcel->getActiveSheet()->setCellValue('W'.$fila, $basetra_total);

  $fila = $fila+2;

  $objPHPExcel->getActiveSheet()->setCellValue('W'.$fila, $basetra_total);
  $objPHPExcel->getActiveSheet()->setCellValue('Y'.$fila, "0");
  

  $fila = $fila+3;

  $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, "#Rec");
  $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, "Fecha de Aplicación del pago");
  $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, "Código Cliente");
  $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, "Cliente");
  $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, "Valor aplicado");
  $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, "Base comisión");
  $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, "Documento");
  $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, "# Documento");
  $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, "fecha_doc");
  $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, "# Días transcurrridos");
  $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, "21%");
  $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, "20,5%");
  $objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, "17%");
  $objPHPExcel->getActiveSheet()->setCellValue('N'.$fila, "12%");
  $objPHPExcel->getActiveSheet()->setCellValue('O'.$fila, "0%");

  $fila++;

  $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, "RECAUDO EQUIPOS");
  $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, "-");
  $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, "-");

  $objPHPExcel->getActiveSheet()->mergeCells('G'.$fila.':J'.$fila);

  $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, "SUBTOTALES COMISION EQUIPOS");

  $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, "0");
  $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, "0");
  $objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, "0");
  $objPHPExcel->getActiveSheet()->setCellValue('N'.$fila, "0");
  $objPHPExcel->getActiveSheet()->setCellValue('O'.$fila, "0");

  $fila++;
  $fila++;

  $fila = $fila+2;
  
  $fila = $fila+2;

  $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, "Rec");
  $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, "Fecha");
  $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, "CODIGO");
  $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, "Cliente");
  $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, "Valor aplicado");
  $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, "Base comisión");
  $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, "tipo_");
  $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, "numero_");
  $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, "fecha_doc");
  $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, "Dias");
  $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, "1%");  
  $fila++;

  $consulta9 = "SELECT * FROM comisiones2 WHERE sCodigo_documento_pago = 'RECIBO' AND sucursal = '051' AND sSub_business_area LIKE '%HH%' ORDER BY sNumero_documento";
  $resultado9 = mysqli_query($con, $consulta9);

  while ($linea9 = mysqli_fetch_array($resultado9)) {
    $idComisiones = $linea9["idComisiones"];
    $sNumero_documento = $linea9["sNumero_documento"];
    $dFecha_aplicacion_documento=$linea9['dFecha_aplicacion_documento'];
    $sCliente = $linea9["sCliente"];
    $sNombre_cliente = $linea9["sNombre_cliente"];
    $nHaber = $linea9["nHaber"];
    $sNumero_documento_pago = $linea9["sNumero_documento_pago"];
    $sTipo_documento = $linea9["sTipo_documento"];
    $sSub_business_area = $linea9["sSub_business_area"];

    $nHaber = str_replace(",","", $nHaber);
    $nHaber = floatval($nHaber);

    $sNumero_documento = str_replace(' ', '', $sNumero_documento);

    $consulta10 = "SELECT * FROM ventas_acumuladas WHERE numero_legal LIKE '%$sNumero_documento%'";
    $resultado10 = mysqli_query($con, $consulta10);

    if(mysqli_num_rows($resultado10) == 0){

      $dFecha_aplicacion_documento = explode(" ", $dFecha_aplicacion_documento);

      $dFecha_aplicacion_documento = $dFecha_aplicacion_documento[0];
      $dFecha_aplicacion_documento = str_replace("/","-",$dFecha_aplicacion_documento);

      $dFecha_aplicacion_documento = date("Y-m-d", strtotime($dFecha_aplicacion_documento));

      $base_comision_extra = $nHaber / 1.19;
      $uno_porciento = $base_comision_extra * 0.01;

      if(strlen($sNumero_documento_pago)!=6){
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $sNumero_documento_pago);
        $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $dFecha_aplicacion_documento);
        $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $sCliente);
        $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $sNombre_cliente);
        $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $nHaber);
        $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $base_comision_extra);
        $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $sTipo_documento);
        $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $base_comision_extra);
        $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $sNumero_documento);
        $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $uno_porciento);  
        $fila++;
      }

      $consulta11 = "SELECT * FROM comisiones2 WHERE sCodigo_documento_pago = 'RECIBO' AND sucursal = '051' AND sNumero_documento LIKE '%84529%' ORDER BY sNumero_documento";
      $resultado11 = mysqli_query($con, $consulta11);

      $linea11 = mysqli_fetch_array($resultado11);

      $idComisiones = $linea11["idComisiones"];
      $sNumero_documento = $linea11["sNumero_documento"];
      $dFecha_aplicacion_documento=$linea11['dFecha_aplicacion_documento'];
      $sCliente = $linea11["sCliente"];
      $sNombre_cliente = $linea11["sNombre_cliente"];
      $nHaber = $linea11["nHaber"];
      $sNumero_documento_pago = $linea11["sNumero_documento_pago"];
      $sTipo_documento = $linea11["sTipo_documento"];
      $sSub_business_area = $linea11["sSub_business_area"];
      
      $nHaber = str_replace(",","", $nHaber);
      $nHaber = floatval($nHaber);

      $sNumero_documento = str_replace(' ', '', $sNumero_documento);

      $dFecha_aplicacion_documento = explode(" ", $dFecha_aplicacion_documento);

      $dFecha_aplicacion_documento = $dFecha_aplicacion_documento[0];
      $dFecha_aplicacion_documento = str_replace("/","-",$dFecha_aplicacion_documento);

      $dFecha_aplicacion_documento = date("Y-m-d", strtotime($dFecha_aplicacion_documento));

      $base_comision_extra = $nHaber / 1.19;
      $uno_porciento = $base_comision_extra * 0.01;

      $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $sNumero_documento_pago);
      $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $dFecha_aplicacion_documento);
      $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $sCliente);
      $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $sNombre_cliente);
      $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $nHaber);
      $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $base_comision_extra);
      $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $sTipo_documento);
      $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $base_comision_extra);
      $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $sNumero_documento);
      $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $uno_porciento);  
      
    }
  }

  $fila = $fila+4;

  $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, "TOTAL RECAUDO");
  $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $recaudo_gases);
  $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $total_base_comision);

  $objPHPExcel->getActiveSheet()->mergeCells('G'.$fila.':I'.$fila);

  $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, "TOTAL COMISION");

  $total_comision = $suma_treinta + $suma_veintinuevecinco + $suma_veintinueve + $suma_veinticinco + $suma_veinte;

  $objPHPExcel->getActiveSheet()->mergeCells('K'.$fila.':L'.$fila);

  $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $total_comision);

  $fila++;

  $iva_final = $total_comision * 0.19;
  $rtefte_final = $total_comision * 0.11;
  $rte_iva = $iva_final * 0.15;
  $rte_ica = 9.66/1000;
  $rte_ica_final = $total_comision * $rte_ica;
  

  $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, "iva");
  $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, "19%");
  $objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, $iva_final);

  $fila++;

  $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, "rte fte");
  $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, "11%");
  $objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, $rtefte_final);

  $fila++;

  $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, "rte iva");
  $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, "15%");
  $objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, $rte_iva);

  $fila++;

  $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, "rte ica");
  $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, $rte_ica);
  $objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, $rte_ica_final);


    header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	  header('Content-Disposition: attachment;filename="Tabla de Comisiones HH.xlsx"');
	  header('Cache-Control: max-age=0');
    $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $writer->save('php://output');
}
?>
