<?php
require ("../../libraries/conexion.php");
require '../../PHPExcel.php';
date_default_timezone_set('America/Bogota');
$Fecha = date("Y-m-d");
$recaudo_gases = 0;
$total_base_comision = 0;
$suma_iva = 0;
$suma_total_con_iva = 0;
$basetra_total = 0;
$suma_veintinuevecinco = 0;
$suma_veintinueve = 0;
$suma_veinticinco = 0;
$suma_veinte = 0;
$mostrar = 0;

$suma_treinta = 0;

if ($tipo=1) 
{
  $mes = date("m");
  $fila = 5;
  $objPHPExcel = new PHPExcel();
  $objPHPExcel->getProperties()->setCreator("Ingegas")->setDescription("Comisiones Ingegas");
  $objPHPExcel->setActiveSheetIndex(0);

  $objPHPExcel->getActiveSheet()->setTitle("IG");
  $objPHPExcel->getActiveSheet()->setCellValue('E1', $mes);
  $objPHPExcel->getActiveSheet()->setCellValue('E2', "IG");
  $objPHPExcel->getActiveSheet()->setCellValue('Q1', "Fact sin iva");
  $objPHPExcel->getActiveSheet()->setCellValue('Q2', "mercancias");
  $objPHPExcel->getActiveSheet()->setCellValue('Q3', "Trans");

  $objPHPExcel->getActiveSheet()->mergeCells('K3:O3');

  $objPHPExcel->getActiveSheet()->setCellValue('K3', "COMISIONES");

  $objPHPExcel->getActiveSheet()->setCellValue('A4', "#Rec");
  $objPHPExcel->getActiveSheet()->setCellValue('B4', "Fecha de Aplicación del pago");
  $objPHPExcel->getActiveSheet()->setCellValue('C4', "Código Cliente");
  $objPHPExcel->getActiveSheet()->setCellValue('D4', "Cliente");
  $objPHPExcel->getActiveSheet()->setCellValue('E4', "Valor aplicado");
  $objPHPExcel->getActiveSheet()->setCellValue('F4', "Base comisión");
  $objPHPExcel->getActiveSheet()->setCellValue('G4', "Doc");
  $objPHPExcel->getActiveSheet()->setCellValue('H4', "No. Fact");
  $objPHPExcel->getActiveSheet()->setCellValue('I4', "fecha_doc");
  $objPHPExcel->getActiveSheet()->setCellValue('J4', "# Días transcurrridos");
  $objPHPExcel->getActiveSheet()->setCellValue('K4', "30%");
  $objPHPExcel->getActiveSheet()->setCellValue('L4', "29,5%");
  $objPHPExcel->getActiveSheet()->setCellValue('M4', "29%");
  $objPHPExcel->getActiveSheet()->setCellValue('N4', "25%");
  $objPHPExcel->getActiveSheet()->setCellValue('O4', "20%");
  $objPHPExcel->getActiveSheet()->setCellValue('P4', "Mercado");
  $objPHPExcel->getActiveSheet()->setCellValue('Q4', "1% casa cobranza");
  $objPHPExcel->getActiveSheet()->setCellValue('R4', "TRANS");
  $objPHPExcel->getActiveSheet()->setCellValue('S4', "TR SIN IVA");
  $objPHPExcel->getActiveSheet()->setCellValue('T4', "IVA");
  $objPHPExcel->getActiveSheet()->setCellValue('U4', "Total con Iva");
  $objPHPExcel->getActiveSheet()->setCellValue('V4', "DIF");
  $objPHPExcel->getActiveSheet()->setCellValue('W4', "Base +tra (com");

  $consulta = "SELECT * FROM comisiones2 WHERE sCodigo_documento_pago = 'RECIBO' AND sSub_business_area = 'GA' AND sucursal = '370' ORDER BY sNumero_documento";
  $resultado = mysqli_query($con, $consulta);

  while($linea = mysqli_fetch_array($resultado)){
    $mostrar = 1;
    $sNumero_documento = $linea["sNumero_documento"];
    $dFecha_aplicacion_documento=$linea['dFecha_aplicacion_documento'];
    $sCliente = $linea["sCliente"];
    $sNombre_cliente = $linea["sNombre_cliente"];
    $nHaber = $linea["nHaber"];
    $sNumero_documento_pago = $linea["sNumero_documento_pago"];
    $sTipo_documento = $linea["sTipo_documento"];
    $sSub_business_area = $linea["sSub_business_area"];

    $nHaber = str_replace(",","", $nHaber);
    $nHaber = floatval($nHaber);

    $nHaber1 = $nHaber;

    $sNumero_documento = str_replace(' ', '', $sNumero_documento);

    $trans = 0;
    $trans_iva = 0;
    $trans_total = 0;
    $nHaber_total = $nHaber;
    $res = 0;

    $consulta1 = "SELECT * FROM ventas_producto WHERE numero_legal = '$sNumero_documento' ";
    $resultado1 = mysqli_query($con, $consulta1);

    if(mysqli_num_rows($resultado1)>0){
      while($linea1 = mysqli_fetch_array($resultado1)){
        $nombre_producto_servicio = $linea1["nombre_producto_servicio"];
        $valor_total = $linea1["valor_total"];

        $consulta2 = "SELECT * FROM unneg WHERE descripcion LIKE '%$nombre_producto_servicio%'";
        $resultado2 = mysqli_query($con, $consulta2);

        $linea2 = mysqli_fetch_array($resultado2);

        $tipo = $linea2["tipo"];
        $id_unneg = $linea2["id_unneg"];

        if($tipo == "T"){
          $trans = $trans + $linea1["valor_total"];
          $trans_iva = $trans/1.19;
          $trans_mas_iva = $linea1["valor_total"] * 1.19;
          $trans_total += $trans_mas_iva;
        }else if($tipo == "G 16,5%"){
          $mostrar = 1;
        }

        $pos = strpos($tipo, '%');

        if($pos != false){
          if($id_unneg != "65"){
            $valor_total_iva = $valor_total*1.19;
            $nHaber_total = $nHaber_total - $valor_total_iva;
            $res = 1;
          }
        }
      }
      if($res == 1){
        //$nHaber_total = $nHaber_total*1.19;
        $nHaber = $nHaber_total;
      }
      
    }else{
      $trans = 0;
      $trans_iva = 0;
      $tipo = "";
    }

    if($nHaber < 0){
      $nHaber = $nHaber1;
    }

    $base_comision = ($nHaber-$trans_total)/1.19;

    $consulta3 = "SELECT * FROM ventas_acumuladas WHERE numero_legal = '$sNumero_documento'";
    $resultado3 = mysqli_query($con, $consulta3);

    if(mysqli_num_rows($resultado3)>0){
      $linea3 = mysqli_fetch_array($resultado3);
      
      $fecha_doc_fact = $linea3["fecha_doc"];
      $total_con_iva = $linea3["total_con_iva"];
      $iva = $linea3["iva"];
    }else{
      $fecha_doc_fact = "-";
      $total_con_iva = $nHaber;
      $iva = $nHaber*0.19;
      $mostrar = 0;
    }

    $diferencia_dias = 30;

    $dFecha_aplicacion_documento = explode(" ", $dFecha_aplicacion_documento);

    $dFecha_aplicacion_documento = $dFecha_aplicacion_documento[0];
    $dFecha_aplicacion_documento = str_replace("/","-",$dFecha_aplicacion_documento);

    $dFecha_aplicacion_documento = date("Y-m-d", strtotime($dFecha_aplicacion_documento));

    $fecha_doc_fact = str_replace("/","-",$fecha_doc_fact);

    $fecha_doc_fact = date("Y-m-d", strtotime($fecha_doc_fact));

    if($fecha_doc_fact != "-"){
      $fecha1 = new DateTime($fecha_doc_fact);
      $fecha2 = new DateTime($dFecha_aplicacion_documento);
  
      $diff = $fecha2->diff($fecha1);
  
      $diferencia_dias = $diff->days;
    }else{
      $diferencia_dias = 0;
    }
    

    $treinta = 0;
    $veintinuevecinco = 0;
    $veintinueve = 0;
    $veinticinco = 0;
    $veinte = 0;
    

    if($diferencia_dias == 0){
      $treinta = $base_comision*0.3;
      $veintinuevecinco = 0;
      $veintinueve = 0;
      $veinticinco = 0;
      $veinte = 0;

      $suma_treinta += $treinta;

    }else if($diferencia_dias>=1 && $diferencia_dias <= 30){
      $veintinuevecinco = $base_comision*0.295;
      
      $veintinueve = 0;
      $veinticinco = 0;
      $veinte = 0;

      $suma_veintinuevecinco += $veintinuevecinco;

    }else if($diferencia_dias>=31 && $diferencia_dias <= 75){
      $veintinueve = $base_comision*0.29;
      $veinticinco = 0;
      $veinte = 0;

      $suma_veintinueve += $veintinueve;

    }else if($diferencia_dias>=76 && $diferencia_dias <= 120){
      $veinticinco = $base_comision*0.25;
      $veinte = 0;

      $suma_veinticinco += $veinticinco;

    }else if($diferencia_dias>=121 && $diferencia_dias <= 149){
      $veinte = $base_comision*0.20;

      $suma_veinte += $veinte;
    }

    $total_con_iva = floatval($total_con_iva);

    $dif = $nHaber - $total_con_iva;
    $basetra = $base_comision + 0;

    $recaudo_gases += $nHaber;
    $total_base_comision += $base_comision;

    $iva = str_replace(",","", $iva);
    $iva = floatval($iva);

    
    $suma_iva += $iva;
    
    $total_con_iva = str_replace(",","", $total_con_iva);
    $total_con_iva = floatval($total_con_iva);

    
    $suma_total_con_iva += $total_con_iva;

    $basetra = floatval($basetra);
    $basetra_total += $basetra;

    if(strlen($sNumero_documento_pago)>=6){
      $pagoBta = $base_comision*0.01;
    }else{
      $pagoBta = "";
    }

    if($sNumero_documento_pago == "7890" && $sNumero_documento == "8972"){
      $nHaber = 13942040;
    }

    if($mostrar == 1){
      $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $sNumero_documento_pago);
      $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $dFecha_aplicacion_documento);
      $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $sCliente);
      $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $sNombre_cliente);
      $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $nHaber);
      $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $base_comision);
      $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $sTipo_documento);
      $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $sNumero_documento);
      $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $fecha_doc_fact);
      $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, $diferencia_dias);
      $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $treinta);
      $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, $veintinuevecinco);
      $objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, $veintinueve);
      $objPHPExcel->getActiveSheet()->setCellValue('N'.$fila, $veinticinco);
      $objPHPExcel->getActiveSheet()->setCellValue('O'.$fila, $veinte);
      $objPHPExcel->getActiveSheet()->setCellValue('P'.$fila, $sSub_business_area);
      $objPHPExcel->getActiveSheet()->setCellValue('Q'.$fila, $tipo);
      $objPHPExcel->getActiveSheet()->setCellValue('R'.$fila, $trans_total);
      $objPHPExcel->getActiveSheet()->setCellValue('S'.$fila, $trans);
      $objPHPExcel->getActiveSheet()->setCellValue('T'.$fila, $iva);
      $objPHPExcel->getActiveSheet()->setCellValue('U'.$fila, $total_con_iva);
      $objPHPExcel->getActiveSheet()->setCellValue('V'.$fila, $dif);
      $objPHPExcel->getActiveSheet()->setCellValue('W'.$fila, $basetra);


      $fila++;
    }

  }

  $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, "RECAUDO GASES");
  $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $recaudo_gases);
  $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $total_base_comision);
  
  $objPHPExcel->getActiveSheet()->mergeCells('G'.$fila.':J'.$fila);

  $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, "SUBTOTALES COMISION GASES");

  $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $suma_treinta);
  $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, $suma_veintinuevecinco);
  $objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, $suma_veintinueve);
  $objPHPExcel->getActiveSheet()->setCellValue('N'.$fila, $suma_veinticinco);
  $objPHPExcel->getActiveSheet()->setCellValue('O'.$fila, $suma_veinte);
  $objPHPExcel->getActiveSheet()->setCellValue('R'.$fila, "0");
  $objPHPExcel->getActiveSheet()->setCellValue('S'.$fila, "0");
  $objPHPExcel->getActiveSheet()->setCellValue('T'.$fila, $suma_iva);
  $objPHPExcel->getActiveSheet()->setCellValue('U'.$fila, $suma_total_con_iva);
  $objPHPExcel->getActiveSheet()->setCellValue('W'.$fila, $basetra_total);

  $fila = $fila+2;

  $objPHPExcel->getActiveSheet()->setCellValue('W'.$fila, $basetra_total);
  $objPHPExcel->getActiveSheet()->setCellValue('Y'.$fila, "0");
  
  $fila = $fila+1;

  $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $recaudo_gases);
  $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $total_base_comision);

  $fila = $fila+1;

  $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, "#Rec");
  $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, "Fecha");
  $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, "Código");
  $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, "Cliente");
  $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, "Valor aplicado");
  $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, "Base comisión");
  $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, "Tipo");
  $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, "Numero");
  $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, "Fecha doc");
  $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, "Dias");
  $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, "");
  $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, "");
  $objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, "");
  $objPHPExcel->getActiveSheet()->setCellValue('N'.$fila, "");
  $objPHPExcel->getActiveSheet()->setCellValue('O'.$fila, "16,5%");
  $objPHPExcel->getActiveSheet()->setCellValue('P'.$fila, "");
  $objPHPExcel->getActiveSheet()->setCellValue('Q'.$fila, "1% Pagos Bogota");
  $objPHPExcel->getActiveSheet()->setCellValue('R'.$fila, "TRANS");
  $objPHPExcel->getActiveSheet()->setCellValue('S'.$fila, "");
  $objPHPExcel->getActiveSheet()->setCellValue('T'.$fila, "IVA");
  $objPHPExcel->getActiveSheet()->setCellValue('U'.$fila, "TOTAL");
  $objPHPExcel->getActiveSheet()->setCellValue('V'.$fila, "DIF");
  $objPHPExcel->getActiveSheet()->setCellValue('W'.$fila, "PR");
  $objPHPExcel->getActiveSheet()->setCellValue('X'.$fila, "Base + trans-COM");

  $fila++;

  $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, "$ 0");
  $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, "$ 0");

  $objPHPExcel->getActiveSheet()->mergeCells('G'.$fila.':I'.$fila);

  $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, "SubT Perfiaceros y R");


  $fila = $fila+2;

  $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, "#Rec");
  $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, "Fecha");
  $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, "Código");
  $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, "Cliente");
  $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, "Valor aplicado");
  $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, "Base comisión");
  $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, "Tipo");
  $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, "Numero");
  $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, "Fecha doc");
  $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, "Dias");
  $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, "");
  $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, "");
  $objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, "");
  $objPHPExcel->getActiveSheet()->setCellValue('N'.$fila, "");
  $objPHPExcel->getActiveSheet()->setCellValue('O'.$fila, "10%");
  $objPHPExcel->getActiveSheet()->setCellValue('P'.$fila, "");
  $objPHPExcel->getActiveSheet()->setCellValue('Q'.$fila, "1% Pagos Bogota");
  $objPHPExcel->getActiveSheet()->setCellValue('R'.$fila, "TRANS");
  $objPHPExcel->getActiveSheet()->setCellValue('S'.$fila, "");
  $objPHPExcel->getActiveSheet()->setCellValue('T'.$fila, "IVA");
  $objPHPExcel->getActiveSheet()->setCellValue('U'.$fila, "TOTAL");
  $objPHPExcel->getActiveSheet()->setCellValue('V'.$fila, "DIF");
  $objPHPExcel->getActiveSheet()->setCellValue('W'.$fila, "PR");
  $objPHPExcel->getActiveSheet()->setCellValue('X'.$fila, "Base + trans-COM");

  $fila++;

  $consulta6 = "SELECT * FROM comisiones2 WHERE sCodigo_documento_pago = 'RECIBO' AND sSub_business_area = 'GA' AND sucursal= '370' AND sNumero_documento_pago <> '7889' ORDER BY sNumero_documento";
  $resultado6 = mysqli_query($con, $consulta6);

  while($linea6 = mysqli_fetch_array($resultado6)){
    $sNumero_documento = $linea6["sNumero_documento"];
    $dFecha_aplicacion_documento=$linea6['dFecha_aplicacion_documento'];
    $sCliente = $linea6["sCliente"];
    $sNombre_cliente = $linea6["sNombre_cliente"];
    $nHaber = $linea6["nHaber"];
    $sNumero_documento_pago = $linea6["sNumero_documento_pago"];
    $sTipo_documento = $linea6["sTipo_documento"];
    $sSub_business_area = $linea6["sSub_business_area"];

    $nHaber = str_replace(",","", $nHaber);
    $nHaber = floatval($nHaber);

    $sNumero_documento = str_replace(' ', '', $sNumero_documento);

    $consulta7 = "SELECT * FROM ventas_producto WHERE numero_legal = '$sNumero_documento'";
    $resultado7 = mysqli_query($con, $consulta7);

    if(mysqli_num_rows($resultado7)>0){
      while($linea7 = mysqli_fetch_array($resultado7)){
        $nombre_producto_servicio = $linea7["nombre_producto_servicio"];
        $valor_total = $linea7["valor_total"];
        $valor_aplicado = $valor_total * 1.19;
  
        $consulta8 = "SELECT * FROM unneg WHERE descripcion LIKE '%$nombre_producto_servicio%'";
        $resultado8 = mysqli_query($con, $consulta8);
  
        $linea8 = mysqli_fetch_array($resultado8);
  
        $tipo = $linea8["tipo"];

        $pos = strpos($tipo, '10%');

        $consulta8 = "SELECT * FROM ventas_acumuladas WHERE numero_legal = '$sNumero_documento'";
        $resultado8 = mysqli_query($con, $consulta8);

        if(mysqli_num_rows($resultado8)>0){
          $linea8 = mysqli_fetch_array($resultado8);
          
          $fecha_doc_fact = $linea8["fecha_doc"];
        }else{
          $fecha_doc_fact = "-";
        }

        $dFecha_aplicacion_documento = explode(" ", $dFecha_aplicacion_documento);

        $dFecha_aplicacion_documento = $dFecha_aplicacion_documento[0];
        $dFecha_aplicacion_documento = str_replace("/","-",$dFecha_aplicacion_documento);

        $dFecha_aplicacion_documento = date("Y-m-d", strtotime($dFecha_aplicacion_documento));

        $fecha_doc_fact = str_replace("/","-",$fecha_doc_fact);

        $fecha_doc_fact = date("Y-m-d", strtotime($fecha_doc_fact));

        if($fecha_doc_fact != "-"){
          $fecha1 = new DateTime($fecha_doc_fact);
          $fecha2 = new DateTime($dFecha_aplicacion_documento);
      
          $diff = $fecha2->diff($fecha1);
      
          $diferencia_dias = $diff->days;
        }else{
          $diferencia_dias = 0;
        }

        $porcentaje = $valor_total * 0.10;

        if($pos != false){
          $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $sNumero_documento_pago);
          $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $dFecha_aplicacion_documento);
          $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $sCliente);
          $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $sNombre_cliente);
          $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $valor_aplicado);
          $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $valor_total);
          $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $sTipo_documento);
          $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $sNumero_documento);
          $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $fecha_doc_fact);
          $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, $diferencia_dias);
          $objPHPExcel->getActiveSheet()->setCellValue('O'.$fila, $porcentaje);
          $objPHPExcel->getActiveSheet()->setCellValue('P'.$fila, $sSub_business_area);
          $objPHPExcel->getActiveSheet()->setCellValue('V'.$fila, $valor_aplicado);
          $objPHPExcel->getActiveSheet()->setCellValue('X'.$fila, $valor_total);
          
          
          $fila++;
        }
      }
    }
  }

  $fila = $fila+2;

  $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, "#Rec");
  $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, "Fecha");
  $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, "Código");
  $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, "Cliente");
  $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, "Valor aplicado");
  $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, "Base comisión");
  $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, "Tipo");
  $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, "Numero");
  $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, "Fecha doc");
  $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, "Dias");
  $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, "");
  $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, "");
  $objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, "");
  $objPHPExcel->getActiveSheet()->setCellValue('N'.$fila, "");
  $objPHPExcel->getActiveSheet()->setCellValue('O'.$fila, "20%");
  $objPHPExcel->getActiveSheet()->setCellValue('P'.$fila, "");
  $objPHPExcel->getActiveSheet()->setCellValue('Q'.$fila, "1% Pagos Bogota");
  $objPHPExcel->getActiveSheet()->setCellValue('R'.$fila, "TRANS");
  $objPHPExcel->getActiveSheet()->setCellValue('S'.$fila, "");
  $objPHPExcel->getActiveSheet()->setCellValue('T'.$fila, "IVA");
  $objPHPExcel->getActiveSheet()->setCellValue('U'.$fila, "TOTAL");
  $objPHPExcel->getActiveSheet()->setCellValue('V'.$fila, "DIF");
  $objPHPExcel->getActiveSheet()->setCellValue('W'.$fila, "PR");
  $objPHPExcel->getActiveSheet()->setCellValue('X'.$fila, "Base + trans-COM");

  $fila++;

  $consulta6 = "SELECT * FROM comisiones2 WHERE sCodigo_documento_pago = 'RECIBO' AND sSub_business_area = 'GA' AND sucursal = '370' ORDER BY sNumero_documento";
  $resultado6 = mysqli_query($con, $consulta6);

  while($linea6 = mysqli_fetch_array($resultado6)){
    $sNumero_documento = $linea6["sNumero_documento"];
    $dFecha_aplicacion_documento=$linea6['dFecha_aplicacion_documento'];
    $sCliente = $linea6["sCliente"];
    $sNombre_cliente = $linea6["sNombre_cliente"];
    $nHaber = $linea6["nHaber"];
    $sNumero_documento_pago = $linea6["sNumero_documento_pago"];
    $sTipo_documento = $linea6["sTipo_documento"];
    $sSub_business_area = $linea6["sSub_business_area"];

    $nHaber = str_replace(",","", $nHaber);
    $nHaber = floatval($nHaber);

    $sNumero_documento = str_replace(' ', '', $sNumero_documento);

    $consulta7 = "SELECT * FROM ventas_producto WHERE numero_legal = '$sNumero_documento'";
    $resultado7 = mysqli_query($con, $consulta7);

    if(mysqli_num_rows($resultado7)>0){
      while($linea7 = mysqli_fetch_array($resultado7)){
        $nombre_producto_servicio = $linea7["nombre_producto_servicio"];
        $valor_total = $linea7["valor_total"];
        $valor_aplicado = $valor_total * 1.19;
  
        $consulta8 = "SELECT * FROM unneg WHERE descripcion = '$nombre_producto_servicio'";
        $resultado8 = mysqli_query($con, $consulta8);
  
        $linea8 = mysqli_fetch_array($resultado8);
  
        $tipo = $linea8["tipo"];

        $pos = strpos($tipo, '20%');

        $consulta8 = "SELECT * FROM ventas_acumuladas WHERE numero_legal = '$sNumero_documento'";
        $resultado8 = mysqli_query($con, $consulta8);

        if(mysqli_num_rows($resultado8)>0){
          $linea8 = mysqli_fetch_array($resultado8);
          
          $fecha_doc_fact = $linea8["fecha_doc"];
        }else{
          $fecha_doc_fact = "-";
        }

        $dFecha_aplicacion_documento = explode(" ", $dFecha_aplicacion_documento);

        $dFecha_aplicacion_documento = $dFecha_aplicacion_documento[0];
        $dFecha_aplicacion_documento = str_replace("/","-",$dFecha_aplicacion_documento);

        $dFecha_aplicacion_documento = date("Y-m-d", strtotime($dFecha_aplicacion_documento));

        $fecha_doc_fact = str_replace("/","-",$fecha_doc_fact);

        $fecha_doc_fact = date("Y-m-d", strtotime($fecha_doc_fact));

        if($fecha_doc_fact != "-"){
          $fecha1 = new DateTime($fecha_doc_fact);
          $fecha2 = new DateTime($dFecha_aplicacion_documento);
      
          $diff = $fecha2->diff($fecha1);
      
          $diferencia_dias = $diff->days;
        }else{
          $diferencia_dias = 0;
        }

        $porcentaje = $valor_total * 0.10;

        if($pos != false){
          $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $sNumero_documento_pago);
          $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $dFecha_aplicacion_documento);
          $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $sCliente);
          $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $sNombre_cliente);
          $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $valor_aplicado);
          $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $valor_total);
          $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $sTipo_documento);
          $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $sNumero_documento);
          $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $fecha_doc_fact);
          $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, $diferencia_dias);
          $objPHPExcel->getActiveSheet()->setCellValue('O'.$fila, $porcentaje);
          $objPHPExcel->getActiveSheet()->setCellValue('P'.$fila, $sSub_business_area);
          $objPHPExcel->getActiveSheet()->setCellValue('V'.$fila, $valor_aplicado);
          $objPHPExcel->getActiveSheet()->setCellValue('X'.$fila, $valor_total);
          
          
          $fila++;
        }
      }
    }
  }

  $fila = $fila+2;
  
  $fila = $fila+2;

  $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, "Rec");
  $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, "Fecha");
  $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, "CODIGO");
  $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, "Cliente");
  $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, "Valor aplicado");
  $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, "Base comisión");
  $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, "tipo_");
  $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, "numero_");
  $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, "fecha_doc");
  $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, "Dias");
  $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, "1%");  
  $fila++;

  $consulta9 = "SELECT * FROM comisiones2 WHERE sCodigo_documento_pago = 'RECIBO' AND sucursal = '370' ORDER BY sNumero_documento";
  $resultado9 = mysqli_query($con, $consulta9);

  while ($linea9 = mysqli_fetch_array($resultado9)) {
    $idComisiones = $linea9["idComisiones"];
    $sNumero_documento = $linea9["sNumero_documento"];
    $dFecha_aplicacion_documento=$linea9['dFecha_aplicacion_documento'];
    $sCliente = $linea9["sCliente"];
    $sNombre_cliente = $linea9["sNombre_cliente"];
    $nHaber = $linea9["nHaber"];
    $sNumero_documento_pago = $linea9["sNumero_documento_pago"];
    $sTipo_documento = $linea9["sTipo_documento"];
    $sSub_business_area = $linea9["sSub_business_area"];

    $nHaber = str_replace(",","", $nHaber);
    $nHaber = floatval($nHaber);

    $sNumero_documento = str_replace(' ', '', $sNumero_documento);

    $consulta10 = "SELECT * FROM ventas_acumuladas WHERE numero_legal = '$sNumero_documento'";
    $resultado10 = mysqli_query($con, $consulta10);

    if(mysqli_num_rows($resultado10) == 0){

      $dFecha_aplicacion_documento = explode(" ", $dFecha_aplicacion_documento);

      $dFecha_aplicacion_documento = $dFecha_aplicacion_documento[0];
      $dFecha_aplicacion_documento = str_replace("/","-",$dFecha_aplicacion_documento);

      $dFecha_aplicacion_documento = date("Y-m-d", strtotime($dFecha_aplicacion_documento));

      $base_comision_extra = $nHaber / 1.19;
      $uno_porciento = $base_comision_extra * 0.01;

      $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $sNumero_documento_pago);
      $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $dFecha_aplicacion_documento);
      $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $sCliente);
      $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $sNombre_cliente);
      $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $nHaber);
      $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $base_comision_extra);
      $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $sTipo_documento);
      $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $base_comision_extra);
      $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $sNumero_documento);
      $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $uno_porciento);  
      
      $fila++;
    }
  }

  $fila = $fila+4;


    
    header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	  header('Content-Disposition: attachment;filename="Tabla de Comisiones.xlsx"');
	  header('Cache-Control: max-age=0');
    $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $writer->save('php://output');
}
?>
