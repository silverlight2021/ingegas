<?php 
require ("../../libraries/conexion.php");
require '../../PHPExcel.php';
date_default_timezone_set('America/Bogota');
$Fecha = date("Y-m-d");

$consultaCargue = "SELECT mes FROM cargue_comisiones_vendedores ORDER BY id_cargue_comisiones_vendedores DESC LIMIT 1"; //hace la consulta y trae el ultimo mes cargado
$resultadoCargue = mysqli_query($con, $consultaCargue);

$linea = mysqli_fetch_array($resultadoCargue);
$mesC = $linea["mes"]; 
$anio = date("Y"); 

//................................................................................................................................................................

if ($tipo = 1) {
  
  $mes1 = date("m");
  $fila = 4;
  $objPHPExcel = new PHPExcel();
  $objPHPExcel->getProperties()->setCreator("Ingegas")->setDescription("Comisiones Vendedores");
  $objPHPExcel->setActiveSheetIndex(0);

  $objPHPExcel->getActiveSheet()->setTitle("Resumen");
  $objPHPExcel->getActiveSheet()->setCellValue('B2', "100%");

  $objPHPExcel->getActiveSheet()->setCellValue('A3', "VTAS VDOR DESGLOSADO");
  $objPHPExcel->getActiveSheet()->setCellValue('B3', "PRESUPUESTO");
  $objPHPExcel->getActiveSheet()->setCellValue('C3', "TOB");
  $objPHPExcel->getActiveSheet()->setCellValue('D3', "SUR");
  $objPHPExcel->getActiveSheet()->setCellValue('E3', "MANZ");
  $objPHPExcel->getActiveSheet()->setCellValue('F3', "MED");
  $objPHPExcel->getActiveSheet()->setCellValue('G3', "SUBTOTAL");
  $objPHPExcel->getActiveSheet()->setCellValue('H3', "NOTAS CIERRE");
  $objPHPExcel->getActiveSheet()->setCellValue('I3', "TOTAL VTA");
  $objPHPExcel->getActiveSheet()->setCellValue('J3', "% CUMPL");
  $objPHPExcel->getActiveSheet()->setCellValue('K3', "COMISION X REC MESSER");
  $objPHPExcel->getActiveSheet()->setCellValue('L3', "COMISION FACT NITRO CON RECAUDO");
  $objPHPExcel->getActiveSheet()->setCellValue('M3', "VTAS INGEGAS VDOR");
  $objPHPExcel->getActiveSheet()->setCellValue('N3', "COMISION ING");
  $objPHPExcel->getActiveSheet()->setCellValue('O3', "TOTAL COMISIONES");
  $objPHPExcel->getActiveSheet()->setCellValue('P3', "ENTREGA INFORMES");
//Construye las columnas del encabezado..........................................................................................................................
// INICIA CONSULTA DE VENDEDORES PARA LISTAR EN EXCEL.............................................................................................

  $consulta_T_vendedores = "SELECT id_vendedor, nombre_vendedor FROM vendedores WHERE estado <> 2 ORDER BY id_vendedor";
  $queryConsultaT = mysqli_query($con, $consulta_T_vendedores);

  while ($queryRow = mysqli_fetch_array($queryConsultaT)) {    
    $id_vendedor = $queryRow["id_vendedor"];
    $nombre_vendedor = $queryRow["nombre_vendedor"];

    $consulta_PP_Vendedor = "SELECT valor FROM presupuesto_vendedor WHERE id_vendedor = $id_vendedor AND mes = '$mesC' AND anio = '$anio'";
    $queryConsultaPP = mysqli_query($con, $consulta_PP_Vendedor);
    $queryRowPP = mysqli_fetch_array($queryConsultaPP);
    $valorPP = $queryRowPP['valor'];

    $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $nombre_vendedor);
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $valorPP);  //imprime los datos del vendedor y el presupuesto del mes

    
    if ($id_vendedor == 1) {

      //CACULA NOTAS CREDITO .........................................................................................................................
           
      $notascreditoEB = "SELECT * FROM notas_credito_vendedores WHERE mes = '$mesC'";
      $resultradoNCEB = mysqli_query($con, $notascreditoEB);

      while ($resultadoNC = mysqli_fetch_array($resultradoNCEB)) {
        $codigoV_CLI_NC = $resultadoNC['codigo_cliente'];
        $length = 9;
        $codClienteNC = substr(str_repeat(0, $length).$codigoV_CLI_NC, - $length);

        if ($resultadoNC['sucursal'] == '051') {
          $sucursalNCE_051 = $resultadoNC['sucursal'];
          $valorNCE_051 = $resultadoNC['valor_sin_iva'];
          $valorTotal_NCE_051 += $valorNCE_051;

        }elseif ($resultadoNC['sucursal'] == '060') {
          $sucursalNCE_060 = $resultadoNC['sucursal'];
          $valorNCE_060 = $resultadoNC['valor_sin_iva'];
          $valorTotal_NCE_060 += $valorNCE_060;

        }elseif ($resultadoNC['sucursal'] == '370') {
          $sucursalNCE_370 = $resultadoNC['sucursal'];
          $valorNCE_370 = $resultadoNC['valor_sin_iva'];
          $valorTotal_NCE_370 += $valorNCE_370;

        }elseif ($resultadoNC['sucursal'] == '590') {
          $sucursalNCE_590 = $resultadoNC['sucursal'];
          $valorNCE_590 = $resultadoNC['valor_sin_iva'];
          $valorTotal_NCE_590 += $valorNCE_590;
        }
        
       $consultaVendedor = "SELECT DISTINCT codigo, cliente, vendedor FROM asignacion_vendedores WHERE codigo = '$codClienteNC'";
        $queryVendedor = mysqli_query($con, $consultaVendedor);

        while ($traeVendedor = mysqli_fetch_array($queryVendedor)) {
          if ($traeVendedor['vendedor'] == 'EB') {
            $codCliNcEB = $traeVendedor['codigo'];
            $vendedorNCEB = $traeVendedor['vendedor'];
            $valorSinIVA_NCEB = $resultadoNC['valor_sin_iva'];
            $sucursalNcEB = $resultadoNC['sucursal'];
            $total_NB_NC += $valorSinIVA_NCEB;
             
            //echo $vendedorNC." / ".$codCliNc." / ".$valorSinIVA_NC." / ".$sucursalNc."<br>";
        
           }elseif ($traeVendedor['vendedor'] == 'JJ') {
            $codCliNcJJ = $traeVendedor['codigo'];
            $vendedorNCJJ = $traeVendedor['vendedor'];
            $valorSinIVA_NCJJ = $resultadoNC['valor_sin_iva'];
            $sucursalNcJJ = $resultadoNC['sucursal'];

            $total_JJ_NC += $valorSinIVA_NCJJ;
             
            //echo $vendedorNC." / ".$codCliNc." / ".$valorSinIVA_NC." / ".$sucursalNc."<br>";

           }elseif ($traeVendedor['vendedor'] == 'GER') {
            $codCliNcGER = $traeVendedor['codigo'];
            $vendedorNCGER = $traeVendedor['vendedor'];
            $valorSinIVA_NCGER = $resultadoNC['valor_sin_iva'];
            $sucursalNcGER = $resultadoNC['sucursal'];
            $total_GER_NC += $valorSinIVA_NCGER;
           
            //echo $vendedorNC." / ".$codCliNc." / ".$valorSinIVA_NC." / ".$sucursalNc."<br>"; 

           }elseif ($traeVendedor['vendedor'] == 'TV') {
            $codCliNcTV = $traeVendedor['codigo'];
            $vendedorNCTV = $traeVendedor['vendedor'];
            $valorSinIVA_NCTV = $resultadoNC['valor_sin_iva'];
            $sucursalNcTV = $resultadoNC['sucursal'];
            $total_TV_NC += $valorSinIVA_NCTV;
            
            //echo $vendedorNC." / ".$codCliNc." / ".$valorSinIVA_NC." / ".$sucursalNc."<br>"; 
             
           }elseif ($traeVendedor['vendedor'] == 'VM') {
            $codCliNcVM = $traeVendedor['codigo'];
            $vendedorNCVM = $traeVendedor['vendedor'];
            $valorSinIVA_NCVM = $resultadoNC['valor_sin_iva'];
            $sucursalNcVM = $resultadoNC['sucursal'];
            $total_VM_NC += $valorSinIVA_NCVM;
            
            //echo $vendedorNC." / ".$codCliNc." / ".$valorSinIVA_NC." / ".$sucursalNc."<br>";             
           }
         }                
      }
      $total_NCE_sucursales = $valorTotal_NCE_051+$valorTotal_NCE_060+$valorTotal_NCE_370+$valorTotal_NCE_590;
      //echo $valorTotal_NCE_051."<br>".$valorTotal_NCE_060."<br>".$valorTotal_NCE_370."<br>".$valorTotal_NCE_590;      
      //echo "Total NCE de EB es ".$total_NB_NC."<br>";
      //echo "Total NCE de JJ es ".$total_JJ_NC."<br>";
      //echo "Total NCE de GER es ".$total_GER_NC."<br>";
      //echo "Total NCE de TV es ".$total_TV_NC."<br>";
      //echo "Total NCE de VM es ".$total_VM_NC."<br>";           

    //TERMINA NOTAS CREDITO ............................................................................................................................

    //CALCULA SUB_TOTAL PARA JORGE......................................................................................................................  
    
    $subTotalJ = "SELECT cantidad, valor_unitario, centro_costos FROM vt_ing";
      $querySubToltalJ = mysqli_query($con, $subTotalJ);
  
      while ($queryRowSuToJ = mysqli_fetch_array($querySubToltalJ)) {
        
        if ($queryRowSuToJ['centro_costos'] == 'Mezclas') {
            $cantidadMez = $queryRowSuToJ['cantidad'];
            $valorUMez = $queryRowSuToJ['valor_unitario'];
            $centroCostoMez = $queryRowSuToJ['centro_costos'];
                        
            $valor_x_mez = $cantidadMez*$valorUMez;
            $valorTotalMez += $valor_x_mez;

        }elseif ($queryRowSuToJ['centro_costos'] == 'Obra de Ingenieria') {
            $cantidad_OI = $queryRowSuToJ['cantidad'];
            $valorU_OI = $queryRowSuToJ['valor_unitario'];
            $centroCosto_OI = $queryRowSuToJ['centro_costos'];
                        
            $valor_x__OI = $cantidad_OI*$valorU_OI;
            $valorTotal_OI += $valor_x__OI;

        }elseif ($queryRowSuToJ['centro_costos'] == 'Esterilizacion') {
            $cantidad_Esz = $queryRowSuToJ['cantidad'];
            $valorU_Esz = $queryRowSuToJ['valor_unitario'];
            $centroCosto_Esz = $queryRowSuToJ['centro_costos'];
                        
            $valor_x__Esz = $cantidad_Esz*$valorU_Esz;
            $valorTotal_Esz += $valor_x__Esz;

        }elseif ($queryRowSuToJ['centro_costos'] == 'Pruebas hidrostaticas') {
            $cantidad_PH = $queryRowSuToJ['cantidad'];
            $valorU_PH = $queryRowSuToJ['valor_unitario'];
            $centroCosto_PH = $queryRowSuToJ['centro_costos'];
                        
            $valor_x__PH = $cantidad_PH*$valorU_PH;
            $valorTotal_PH += $valor_x__PH;
        }elseif ($queryRowSuToJ['centro_costos'] == 'Mercancias') {
            $cantidad_Mer = $queryRowSuToJ['cantidad'];
            $valorU_Mer = $queryRowSuToJ['valor_unitario'];
            $centroCosto_Mer = $queryRowSuToJ['centro_costos'];
                        
            $valor_x__Mer = $cantidad_Mer*$valorU_Mer;
            $valorTotal_Mer += $valor_x__Mer;
        }
        
      }

    $SubTotal_J = $valorTotalMez+$valorTotal_OI+$valorTotal_Esz+$valorTotal_PH+$valorTotal_Mer;

    $consulta6 = "SELECT producto_servicio, valor_total, numero_legal, producto_servicio, nombre_producto_servicio 
                    FROM ventas_producto WHERE anio = '$anio' AND mes = '$mesC' AND sucursal = '051'"; //Hace la consulta a ventas por productos para traer campos de la factura
      $resultado6 = mysqli_query($con, $consulta6);

      while($linea6 = mysqli_fetch_array($resultado6)){
      $producto_servicio = $linea6["producto_servicio"];
      $valor_total = $linea6["valor_total"];
      $numero_legal = $linea6["numero_legal"];
      $producto_servicio = $linea6["producto_servicio"];
      $nombre_producto_servicio = $linea6["nombre_producto_servicio"]; // Trae los datos consultados 

      $consulta7 = "SELECT linea FROM unneg WHERE referencia = '$producto_servicio'";
      $resultado7 = mysqli_query($con, $consulta7); //hace una consulta a la unidad de negocio y le pasa la referencia del producto servicio

      if(mysqli_num_rows($resultado7) == 0){

      $consulta8 = "SELECT linea FROM unneg WHERE descripcion = '$nombre_producto_servicio'";
      $resultado8 = mysqli_query($con, $consulta8); //hace una consulta a la unidad de negocio y le pasa la nombre del producto servicio

      if(mysqli_num_rows($resultado8) == 0){

      $producto_servicio1 = intval($producto_servicio);

      $consulta9 = "SELECT linea FROM unneg WHERE referencia = '$producto_servicio1'";
      $resultado9 = mysqli_query($con, $consulta9); //hace una consulta a la unidad de negocio y le pasa la referencia del producto servicio

      if(mysqli_num_rows($resultado9) > 0){
      $linea9 = mysqli_fetch_array($resultado9);
      $linea = $linea9["linea"];

      if($linea == 'GE'){
        $ge_toberin += $valor_total;
      }else if($linea == 'GI'){
        $gi_toberin += $valor_total;
      }else if($linea == 'LIN'){
        $lin_toberin += $valor_total;
      }else if($linea == 'LOX'){
        $lox_toberin += $valor_total;
      }
      }

      }else{ //Si lo encuentra entonces trae la linea que pertenece a esa referencia
      $linea8 = mysqli_fetch_array($resultado8);
      $linea = $linea8["linea"]; // trae el dato consultado

      if($linea == 'GE'){
      $ge_toberin += $valor_total;
      }else if($linea == 'GI'){
      $gi_toberin += $valor_total;
      }else if($linea == 'LIN'){
      $lin_toberin += $valor_total;
      }else if($linea == 'LOX'){
      $lox_toberin += $valor_total;
      }
      } //dependiendo la line le asigana el valor total a la variable que inicializo en 0 osea gi_toberin

      }else{
      $linea7 = mysqli_fetch_array($resultado7);
      $linea = $linea7["linea"];

      if($linea == 'GE'){
      $ge_toberin += $valor_total;
      }else if($linea == 'GI'){
      $gi_toberin += $valor_total;
      }else if($linea == 'LIN'){
      $lin_toberin += $valor_total;
      }else if($linea == 'LOX'){
      $lox_toberin += $valor_total;
      }
      }
      } 
      $total_toberin = $ge_toberin + $gi_toberin + $lin_toberin + $lox_toberin;

$consulta6 = "SELECT producto_servicio, valor_total, numero_legal, producto_servicio, nombre_producto_servicio 
                    FROM ventas_producto WHERE anio = '$anio' AND mes = '$mesC' AND sucursal = '060'";
$resultado6 = mysqli_query($con, $consulta6); //se hace la consulta para a ventas por producto para traer valores de la sucursal 060

      while($linea6 = mysqli_fetch_array($resultado6)){
        $producto_servicio = $linea6["producto_servicio"];
        $valor_total = $linea6["valor_total"];
        $numero_legal = $linea6["numero_legal"];
        $producto_servicio = $linea6["producto_servicio"];
        $nombre_producto_servicio = $linea6["nombre_producto_servicio"]; //trae los valores de la consulta realizada 

        $consulta7 = "SELECT linea FROM unneg WHERE referencia = '$producto_servicio'";
        $resultado7 = mysqli_query($con, $consulta7);

        if(mysqli_num_rows($resultado7) == 0){

          $consulta8 = "SELECT linea FROM unneg WHERE descripcion = '$nombre_producto_servicio'";
          $resultado8 = mysqli_query($con, $consulta8);

          if(mysqli_num_rows($resultado8) == 0){

            $producto_servicio1 = intval($producto_servicio);

            $consulta9 = "SELECT linea FROM unneg WHERE referencia = '$producto_servicio1'";
            $resultado9 = mysqli_query($con, $consulta9);

            if(mysqli_num_rows($resultado9) > 0){
              $linea9 = mysqli_fetch_array($resultado9);
              $linea = $linea9["linea"];

              if($linea == 'GE'){
                $ge_sur += $valor_total;
              }else if($linea == 'GI'){
                $gi_sur += $valor_total;
              }else if($linea == 'LIN'){
                $lin_sur += $valor_total;
              }else if($linea == 'LOX'){
                $lox_sur += $valor_total;
              }
            }

          }else{
            $linea8 = mysqli_fetch_array($resultado8);
            $linea = $linea8["linea"];

            if($linea == 'GE'){
              $ge_sur += $valor_total;
            }else if($linea == 'GI'){
              $gi_sur += $valor_total;
            }else if($linea == 'LIN'){
              $lin_sur += $valor_total;
            }else if($linea == 'LOX'){
              $lox_sur += $valor_total;
            }
          }

        }else{
          $linea7 = mysqli_fetch_array($resultado7);
          $linea = $linea7["linea"];

          if($linea == 'GE'){
            $ge_sur += $valor_total;
          }else if($linea == 'GI'){
            $gi_sur += $valor_total;
          }else if($linea == 'LIN'){
            $lin_sur += $valor_total;
          }else if($linea == 'LOX'){
            $lox_sur += $valor_total;
          }
        }
      }

      $total_sur = $ge_sur + $gi_sur + $lin_sur + $lox_sur;

      $consulta6 = "SELECT producto_servicio, valor_total, numero_legal, producto_servicio, nombre_producto_servicio 
            FROM ventas_producto WHERE anio = '$anio' AND mes = '$mesC' AND sucursal = '370'";
      $resultado6 = mysqli_query($con, $consulta6);

      while($linea6 = mysqli_fetch_array($resultado6)){
      $producto_servicio = $linea6["producto_servicio"];
      $valor_total = $linea6["valor_total"];
      $numero_legal = $linea6["numero_legal"];
      $producto_servicio = $linea6["producto_servicio"];
      $nombre_producto_servicio = $linea6["nombre_producto_servicio"];

      $consulta7 = "SELECT linea FROM unneg WHERE referencia = '$producto_servicio'";
      $resultado7 = mysqli_query($con, $consulta7);

      if(mysqli_num_rows($resultado7) == 0){

      $consulta8 = "SELECT linea FROM unneg WHERE descripcion = '$nombre_producto_servicio'";
      $resultado8 = mysqli_query($con, $consulta8);

      if(mysqli_num_rows($resultado8) == 0){

      $producto_servicio1 = intval($producto_servicio);

      $consulta9 = "SELECT linea FROM unneg WHERE referencia = '$producto_servicio1'";
      $resultado9 = mysqli_query($con, $consulta9);

      if(mysqli_num_rows($resultado9) > 0){
      $linea9 = mysqli_fetch_array($resultado9);
      $linea = $linea9["linea"];

      if($linea == 'GE'){
        $ge_manizales += $valor_total;
      }else if($linea == 'GI'){
        $gi_manizales += $valor_total;
      }else if($linea == 'LIN'){
        $lin_manizales += $valor_total;
      }else if($linea == 'LOX'){
        $lox_manizales += $valor_total;
      }
      }

      }else{
      $linea8 = mysqli_fetch_array($resultado8);
      $linea = $linea8["linea"];

      if($linea == 'GE'){
      $ge_manizales += $valor_total;
      }else if($linea == 'GI'){
      $gi_manizales += $valor_total;
      }else if($linea == 'LIN'){
      $lin_manizales += $valor_total;
      }else if($linea == 'LOX'){
      $lox_manizales += $valor_total;
      }
      }

      }else{
      $linea7 = mysqli_fetch_array($resultado7);
      $linea = $linea7["linea"];

      if($linea == 'GE'){
      $ge_manizales += $valor_total;
      }else if($linea == 'GI'){
      $gi_manizales += $valor_total;
      }else if($linea == 'LIN'){
      $lin_manizales += $valor_total;
      }else if($linea == 'LOX'){
      $lox_manizales += $valor_total;
      }
      }
      }

      $total_manizales = $ge_manizales + $gi_manizales + $lin_manizales + $lox_manizales;

      $consulta6 = "SELECT producto_servicio, valor_total, numero_legal, producto_servicio, nombre_producto_servicio 
      FROM ventas_producto WHERE anio = '$anio' AND mes = '$mesC' AND sucursal = '590'";
      $resultado6 = mysqli_query($con, $consulta6);

      while($linea6 = mysqli_fetch_array($resultado6)){
      $producto_servicio = $linea6["producto_servicio"];
      $valor_total = $linea6["valor_total"];
      $numero_legal = $linea6["numero_legal"];
      $producto_servicio = $linea6["producto_servicio"];
      $nombre_producto_servicio = $linea6["nombre_producto_servicio"];

      $consulta7 = "SELECT linea FROM unneg WHERE referencia = '$producto_servicio'";
      $resultado7 = mysqli_query($con, $consulta7);

      if(mysqli_num_rows($resultado7) == 0){

      $consulta8 = "SELECT linea FROM unneg WHERE descripcion = '$nombre_producto_servicio'";
      $resultado8 = mysqli_query($con, $consulta8);

      if(mysqli_num_rows($resultado8) == 0){

      $producto_servicio1 = intval($producto_servicio);

      $consulta9 = "SELECT linea FROM unneg WHERE referencia = '$producto_servicio1'";
      $resultado9 = mysqli_query($con, $consulta9);

      if(mysqli_num_rows($resultado9) > 0){
      $linea9 = mysqli_fetch_array($resultado9);
      $linea = $linea9["linea"];

      if($linea == 'GE'){
      $ge_itag += $valor_total;
      }else if($linea == 'GI'){
      $gi_itag += $valor_total;
      }else if($linea == 'LIN'){
      $lin_itag += $valor_total;
      }else if($linea == 'LOX'){
      $lox_itag += $valor_total;
      }
      }

      }else{
      $linea8 = mysqli_fetch_array($resultado8);
      $linea = $linea8["linea"];

      if($linea == 'GE'){
      $ge_itag += $valor_total;
      }else if($linea == 'GI'){
      $gi_itag += $valor_total;
      }else if($linea == 'LIN'){
      $lin_itag += $valor_total;
      }else if($linea == 'LOX'){
      $lox_itag += $valor_total;
      }
      }

      }else{
      $linea7 = mysqli_fetch_array($resultado7);
      $linea = $linea7["linea"];

      if($linea == 'GE'){
      $ge_itag += $valor_total;
      }else if($linea == 'GI'){
      $gi_itag += $valor_total;
      }else if($linea == 'LIN'){
      $lin_itag += $valor_total;
      }else if($linea == 'LOX'){
      $lox_itag += $valor_total;
      }
      }
      }

      $total_itag = $ge_itag + $gi_itag + $lin_itag + $lox_itag;

      $subtotalMesser = $total_toberin+$total_sur+$total_manizales+$total_itag;      
      
      $subTotalJorge = $subtotalMesser + $SubTotal_J;
    
    $ventaReal_J = $subTotalJorge - $total_NCE_sucursales;

    $porcentaje_Cumpli_J = round($ventaReal_J * 100 / $valorPP);

    $recaudoMesserJ = (2000000 * $porcentaje_Cumpli_J) / 100;

    if ($porcentaje_Cumpli_J < 80) {
        $total_parcialJ = 0;
    } elseif ($porcentaje_Cumpli_J > 150) {
        $total_parcialJ = 3000000;
    }

    //TERMINA SUB_TOTAL PARA JORGE.............................................................................................................................

    //IMPRIME JORGE ANDRES FORERO
    $objPHPExcel->getActiveSheet()->setCellValue('G4', $subTotalJorge);
    $objPHPExcel->getActiveSheet()->setCellValue('H4', $total_NCE_sucursales);
    $objPHPExcel->getActiveSheet()->setCellValue('I4', $ventaReal_J);
    $objPHPExcel->getActiveSheet()->setCellValue('J4', $porcentaje_Cumpli_J."%");
    $objPHPExcel->getActiveSheet()->setCellValue('K4', $recaudoMesserJ);
    if ($total_parcialJ == 0) {
        $objPHPExcel->getActiveSheet()->setCellValue('O4', $recaudoMesserJ);
    }else {
        $objPHPExcel->getActiveSheet()->setCellValue('O4', $total_parcialJ);
    }
    
    //...................................INICIA DIEGO....................................................................................................

    }elseif ($id_vendedor == 2) {
      
      $subtotalDiego = $subtotalMesser;
      $total_ventas_reales_Diego = $subtotalDiego - $total_NCE_sucursales;

      $porcentaje_Cumpli_D = round($total_ventas_reales_Diego * 100 / $valorPP);
      
      $existePPCUM = "SELECT * FROM porce_cumpli_vendedor WHERE idVendedor = 2 AND mes LIKE '%$mesC%' AND anio = '$anio'";
      $resultExistePPCUM = mysqli_query($con, $existePPCUM);

      if (mysqli_num_rows($resultExistePPCUM) <= 0) {
          
          $mes_0 = sprintf("%02d", $mesC);
          $registraPPCUM = "INSERT INTO porce_cumpli_vendedor (idVendedor, mes, anio, porcentaje) VALUES (2,'$mes_0','$anio','$porcentaje_Cumpli_D')";
          $ressultRegistroPPCUM = mysqli_query($con, $registraPPCUM);

      }

      //VERIFICA COMISIONES DE RECAUDO INGAS
      $seleccionCobranza = "SELECT * FROM cobranza";
      $resultCobranza = mysqli_query($con, $seleccionCobranza);
      while ($rowCobranza = mysqli_fetch_array($resultCobranza)) {       
        $clienteCobranza = $rowCobranza['cliente'];       
        $facturaCobranza = $rowCobranza['factura'];
        $fac_neta = preg_replace('/[^0-9]/', '', $facturaCobranza);
        $fechaCobranza = $rowCobranza['fecha'];
        $ventaNeta_sinIVA = $rowCobranza['recaudo']/1.19; 
        //echo $fac_neta."<br>";
        $verificaVendedor = "SELECT * FROM vtas_vdor WHERE documento = '$fac_neta'";
        $resultVeriVendedor = mysqli_query($con, $verificaVendedor);
 
        while ($rowVeriVendedor = mysqli_fetch_array($resultVeriVendedor)) {
          $vendedorCobraza = $rowVeriVendedor['vendedor'];
          $fechaVendedorFac = $rowVeriVendedor['fecha'];
          //$ventaNeta_sinIVA = $rowVeriVendedor['venta_neta']; 
          
          if ($vendedorCobraza == 'DIEGO FERNANDO MARTINEZ RAMIREZ') {
           //echo $clienteCobranza." - ".$fac_neta." - ".$fechaVendedorFac." - ".$vendedorCobraza." - ".$fechaCobranza." - ".$ventaNeta_sinIVA."<br>";
           $totalRecaudo += $ventaNeta_sinIVA;
           $porcentajeRecaudo = $totalRecaudo*0.01;          
          }elseif ($vendedorCobraza == 'JUAN CARLOS JIMENEZ CAICEDO') {
           $totalRecaudo_JJ += $ventaNeta_sinIVA;
           $porcentajeRecaudo_JJ = $totalRecaudo_JJ*0.01;
          }elseif ($vendedorCobraza == 'CARLOS EDUARDO ALBA SALAMANCA') {
           $totalRecaudo_EB += $ventaNeta_sinIVA;
           $porcentajeRecaudo_EB = $totalRecaudo_EB*0.01;
          }elseif ($vendedorCobraza == 'JORGE ENRIQUE AVENDAÑO ALVAREZ') {
           $totalRecaudo_TV += $ventaNeta_sinIVA;
           $porcentajeRecaudo_TV = $totalRecaudo_TV*0.01;
          }elseif ($vendedorCobraza == 'CESAR HERNAN HENAO MUÑOZ') {
           $totalRecaudo_VM += $ventaNeta_sinIVA;
           $porcentajeRecaudo_VM = $totalRecaudo_VM*0.01;          
          }
  
        }
 
      }
      /*echo $porcentajeRecaudo."<br>";
      echo $porcentajeRecaudo_JJ."<br>";
      echo $porcentajeRecaudo_EB."<br>";
      echo $porcentajeRecaudo_TV."<br>";
      echo $porcentajeRecaudo_VM."<br>";*/

     //CALCULO COMISIONES MESSER TOBERIN................................................................................................................................................................................
     $consultaConso051 = "SELECT * FROM comisiones_toberin WHERE mes = '$mesC' AND dias != ''";
     $resultadoConso051 = mysqli_query($con, $consultaConso051);

     while ($RowConso051 = mysqli_fetch_array($resultadoConso051)) {       
      if ($RowConso051['dias'] != '' && $RowConso051['porc_3'] != '' && $RowConso051['porc_4'] != '') {
          $numeRac = $RowConso051['rec'];
          $numeFac = $RowConso051['numero_']; 
          $beseComisionApro = $RowConso051['base_trans'];
          $dias051 = $RowConso051['dias'];
          $fecha_doc = $RowConso051['fecha_doc'];
          $sacaMes = date('m', strtotime($fecha_doc)); 

          //echo $numeRac." - ".$numeFac." - ".$beseComisionApro." - ".$dias051."<br>";

          if ($dias051 <= 90) {
             $porcentaje_liqui_recaudo = 100;
          }elseif ($dias051 > 120) {
             $porcentaje_liqui_recaudo = 0;
          } else {
           $porcentaje_liqui_recaudo = 80;
          }

          //echo $numeRac." - ".$numeFac." - ".$beseComisionApro." - ".$dias051." - ".$porcentaje_liqui_recaudo."%"."<br>";

          //echo $sacaMes."<br>";

          $consultaPPVdor = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes' AND anio = '$anio' AND idVendedor = 2";
          $resultadoPPVdor = mysqli_query($con, $consultaPPVdor);

          while ($RowPPVdor = mysqli_fetch_array($resultadoPPVdor)) {

           $porceCumpliVdor = $RowPPVdor['porcentaje'];
                      
           if ($porceCumpliVdor > 100) {
             $liquidacion_PPTO = 0.005;
           }elseif ($porceCumpliVdor <= 80) {
             $liquidacion_PPTO = 0;
           }else {
             $liquidacion_PPTO = 0.004;
           }
           
           //echo $numeRac." - ".$numeFac." - ".$beseComisionApro." - ".$dias051." - "." ( ".$sacaMes." ) "." - ".$porceCumpliVdor."%"." - ".$liquidacion_PPTO." - ".$porcentaje_liqui_recaudo."%"."<br>";

           $comision_PPTO_Diego = $beseComisionApro*$liquidacion_PPTO*($porcentaje_liqui_recaudo/100);
           $Total_comision_PPTO_Diego += $comision_PPTO_Diego;
  
       }

       }elseif ($RowConso051['porcentaje'] == 10) { 

         $numeRac1 = $RowConso051['rec'];
         $numeFac1 = $RowConso051['numero_']; 
         $beseComisionApro1 = $RowConso051['base_trans'];
         $dias0511 = $RowConso051['dias'];
         $fecha_doc1 = $RowConso051['fecha_doc'];
         $sacaMes1 = date('m', strtotime($fecha_doc1));

         if ($dias0511 <= 90) {
           $porcentaje_liqui_recaudo1 = 100;
             }elseif ($dias0511 > 120) {
                 $porcentaje_liqui_recaudo1 = 0;
             } else {
               $porcentaje_liqui_recaudo1 = 80;
           }


          $consultaPPVdor1 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes1' AND anio = '$anio' AND idVendedor = 2";
          $resultadoPPVdor1 = mysqli_query($con, $consultaPPVdor1);

          while ($RowPPVdor1 = mysqli_fetch_array($resultadoPPVdor1)) {

           $porceCumpliVdor1 = $RowPPVdor1['porcentaje'];
                      
           if ($porceCumpliVdor1 > 100) {
             $liquidacion_PPTO1 = 0.005;
           }elseif ($porceCumpliVdor1 <= 80) {
             $liquidacion_PPTO1 = 0;
           }else {
             $liquidacion_PPTO1 = 0.004;
           }

           //echo $numeRac1." - ".$numeFac1." - ".$beseComisionApro1." - ".$dias0511." - "." ( ".$sacaMes1." ) "." - ".$porceCumpliVdor1."%"." - ".$liquidacion_PPTO1." - ".$porcentaje_liqui_recaudo1."%"."<br>";
           
           $comision_PPTO_Diego_10 = ($beseComisionApro1*$liquidacion_PPTO1*($porcentaje_liqui_recaudo1/100))/3;
           $Total_comision_PPTO_Diego_10 += $comision_PPTO_Diego_10;

          //echo $comision_PPTO_Diego_10."<br>";
         
           
         }        
         

       }elseif ($RowConso051['porcentaje'] == 16.5) {               

         $numeRac2 = $RowConso051['rec'];
         $numeFac2 = $RowConso051['numero_']; 
         $beseComisionApro2 = $RowConso051['base_trans'];
         $dias0512 = $RowConso051['dias'];
         $fecha_doc2 = $RowConso051['fecha_doc'];
         $sacaMes2 = date('m', strtotime($fecha_doc2));

         if ($dias0512 <= 90) {
           $porcentaje_liqui_recaudo2 = 100;
             }elseif ($dias0512 > 120) {
                 $porcentaje_liqui_recaudo2 = 0;
             } else {
               $porcentaje_liqui_recaudo2 = 80;
           }


          $consultaPPVdor2 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes2' AND anio = '$anio' AND idVendedor = 2";
          $resultadoPPVdor2 = mysqli_query($con, $consultaPPVdor2);

          while ($RowPPVdor2 = mysqli_fetch_array($resultadoPPVdor2)) {

           $porceCumpliVdor2 = $RowPPVdor2['porcentaje'];
                      
           if ($porceCumpliVdor2 > 100) {
             $liquidacion_PPTO2 = 0.005;
           }elseif ($porceCumpliVdor2 <= 80) {
             $liquidacion_PPTO2 = 0;
           }else {
             $liquidacion_PPTO2 = 0.004;
           }

           //echo $numeRac2." - ".$numeFac2." - ".$beseComisionApro2." - ".$dias0512." - "." ( ".$sacaMes2." ) "." - ".$porceCumpliVdor2."%"." - ".$liquidacion_PPTO2." - ".$porcentaje_liqui_recaudo2."%"."<br>";

           $comision_PPTO_Diego_165 = ($beseComisionApro2*$liquidacion_PPTO2*($porcentaje_liqui_recaudo2/100))/1.81;           
           $Total_comision_PPTO_Diego_165 += $comision_PPTO_Diego_165;        
         
           
         }     

         //echo $numeRac1." - ".$beseComisionApro2."<br>";
         
       }elseif ($RowConso051['porcentaje'] == 5) {

         $numeRac3 = $RowConso051['rec'];
         $numeFac3 = $RowConso051['numero_']; 
         $beseComisionApro3 = $RowConso051['base_trans'];
         $dias0513 = $RowConso051['dias'];
         $fecha_doc3 = $RowConso051['fecha_doc'];
         $sacaMes3 = date('m', strtotime($fecha_doc3));

         if ($dias0513 <= 90) {
           $porcentaje_liqui_recaudo3 = 100;
             }elseif ($dias0513 > 120) {
                 $porcentaje_liqui_recaudo3 = 0;
             } else {
               $porcentaje_liqui_recaudo3 = 80;
           }


          $consultaPPVdor3 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes3' AND anio = '$anio' AND idVendedor = 2";
          $resultadoPPVdor3 = mysqli_query($con, $consultaPPVdor3);

          while ($RowPPVdor3 = mysqli_fetch_array($resultadoPPVdor3)) {

           $porceCumpliVdor3 = $RowPPVdor3['porcentaje'];
                      
           if ($porceCumpliVdor3 > 100) {
             $liquidacion_PPTO3 = 0.005;
           }elseif ($porceCumpliVdor3 <= 80) {
             $liquidacion_PPTO3 = 0;
           }else {
             $liquidacion_PPTO3 = 0.004;
           }

           //echo $dias0512." - ".$numeRac2." - ".$beseComisionApro2." - ".$liquidacion_PPTO2." - ".$porcentaje_liqui_recaudo2."<br>";

           $comision_PPTO_Diego_5 = ($beseComisionApro3*$liquidacion_PPTO3*($porcentaje_liqui_recaudo3/100))/6;
           $Total_comision_PPTO_Diego_5 += $comision_PPTO_Diego_5;

                    
           
         }

         
       }elseif ($RowConso051['porcentaje'] == 20) {

         $numeRac4 = $RowConso051['rec'];
         $numeFac4 = $RowConso051['numero_']; 
         $beseComisionApro4 = $RowConso051['base_trans'];
         $dias0514 = $RowConso051['dias'];
         $fecha_doc4 = $RowConso051['fecha_doc'];
         $sacaMes4 = date('m', strtotime($fecha_doc4));

         if ($dias0514 <= 90) {
           $porcentaje_liqui_recaudo4 = 100;
             }elseif ($dias0514 > 120) {
                 $porcentaje_liqui_recaudo4 = 0;
             } else {
               $porcentaje_liqui_recaudo4 = 80;
           }


          $consultaPPVdor4 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes4' AND anio = '$anio' AND idVendedor = 2";
          $resultadoPPVdor4 = mysqli_query($con, $consultaPPVdor3);

          while ($RowPPVdor4 = mysqli_fetch_array($resultadoPPVdor4)) {

           $porceCumpliVdor4 = $RowPPVdor4['porcentaje'];
                      
           if ($porceCumpliVdor4 > 100) {
             $liquidacion_PPTO4 = 0.005;
           }elseif ($porceCumpliVdor4 <= 80) {
             $liquidacion_PPTO4 = 0;
           }else {
             $liquidacion_PPTO4 = 0.004;
           }

           //echo $dias0512." - ".$numeRac2." - ".$beseComisionApro2." - ".$liquidacion_PPTO2." - ".$porcentaje_liqui_recaudo2."<br>";

           $comision_PPTO_Diego_20 = ($beseComisionApro4*$liquidacion_PPTO4*($porcentaje_liqui_recaudo4/100)/1.5);
           $Total_comision_PPTO_Diego_20 += $comision_PPTO_Diego_20;
                   
         }
       }
     }
        
     /*echo $Total_comision_PPTO_Diego." Normales "."<br>";
     echo $Total_comision_PPTO_Diego_10." Al 10%"."<br>";
     echo $Total_comision_PPTO_Diego_165." Al 16,5%"."<br>";
     echo $Total_comision_PPTO_Diego_5." Al 5%"."<br>";
     echo $Total_comision_PPTO_Diego_20." Al 20%"."<br>";*/     
      
    //TERMINA CALCULO COMISIONES MESSER TOBERIN................................................................................................................................................................................................
        
    //CALCULO COMISIONES MESSER CAZUCA................................................................................................................................................................................

    $consultaConso060 = "SELECT * FROM comisiones_cazuca WHERE mes = '$mesC' AND dias != ''";
    $resultadoConso060 = mysqli_query($con, $consultaConso060);

    while ($RowConso060 = mysqli_fetch_array($resultadoConso060)) {       
     if ($RowConso060['dias'] != '' && $RowConso060['porc_3'] != '' && $RowConso060['porc_4'] != '') {
         $numeRac060 = $RowConso060['recibo'];
         $numeFac060 = $RowConso060['numero']; 
         $beseComisionApro060 = $RowConso060['base_com'];
         $dias060 = $RowConso060['dias'];
         $fecha_doc060 = $RowConso060['fecha_doc'];
         $sacaMes060 = date('m', strtotime($fecha_doc060)); 

         //echo $numeRac." - ".$numeFac." - ".$beseComisionApro." - ".$dias051."<br>";

         if ($dias060 <= 90) {
            $porcentaje_liqui_recaudo060 = 100;
         }elseif ($dias060 > 120) {
            $porcentaje_liqui_recaudo060 = 0;
         } else {
          $porcentaje_liqui_recaudo060 = 80;
         }

         $consultaPPVdor060 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes060' AND anio = '$anio' AND idVendedor = 2";
         $resultadoPPVdor060 = mysqli_query($con, $consultaPPVdor060);

         while ($RowPPVdor060 = mysqli_fetch_array($resultadoPPVdor060)) {

          $porceCumpliVdor060 = $RowPPVdor060['porcentaje'];
                     
          if ($porceCumpliVdor060 > 100) {
            $liquidacion_PPTO060 = 0.005;
          }elseif ($porceCumpliVdor060 <= 80) {
            $liquidacion_PPTO060 = 0;
          }else {
            $liquidacion_PPTO060 = 0.004;
          }

          //echo $numeRac060." - ".$numeFac060." - ".$beseComisionApro060." - ".$dias060." - "." ( ".$sacaMes060." ) "." - ".$porceCumpliVdor060."%"." - ".$liquidacion_PPTO060." - ".$porcentaje_liqui_recaudo060."%"."<br>";

          $comision_PPTO_Diego060 = $beseComisionApro060*$liquidacion_PPTO060*($porcentaje_liqui_recaudo060/100);
          $Total_comision_PPTO_Diego060 += $comision_PPTO_Diego060;
          //echo $comision_PPTO_Diego060."<br>";        

        }  

      

      }elseif ($RowConso060['porcentaje'] == 10) { 

        $numeRac1_060 = $RowConso060['recibo'];
        $numeFac1_060 = $RowConso060['numero']; 
        $beseComisionApro1_060 = $RowConso060['base_com'];
        $dias0601_060 = $RowConso060['dias'];
        $fecha_doc1_060 = $RowConso060['fecha_doc'];
        $sacaMes1_060 = date('m', strtotime($fecha_doc1_060));

        if ($dias0601_060 <= 90) {
          $porcentaje_liqui_recaudo1_060 = 100;
            }elseif ($dias0601_060 > 120) {
                $porcentaje_liqui_recaudo1_060 = 0;
            } else {
              $porcentaje_liqui_recaudo1_060 = 80;
          }


         $consultaPPVdor1_060 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes1_060' AND anio = '$anio' AND idVendedor = 2";
         $resultadoPPVdor1_060 = mysqli_query($con, $consultaPPVdor1_060);

         while ($RowPPVdor1_060 = mysqli_fetch_array($resultadoPPVdor1_060)) {

          $porceCumpliVdor1_060 = $RowPPVdor1_060['porcentaje'];
                     
          if ($porceCumpliVdor1_060 > 100) {
            $liquidacion_PPTO1_060 = 0.005;
          }elseif ($porceCumpliVdor1_060 <= 80) {
            $liquidacion_PPTO1_060 = 0;
          }else {
            $liquidacion_PPTO1_060 = 0.004;
          }

        
          $comision_PPTO_Diego060_10 = ($beseComisionApro1_060*$liquidacion_PPTO1_060*($porcentaje_liqui_recaudo1_060/100))/3;
          $Total_comision_PPTO_Diego060_10 += $comision_PPTO_Diego060_10;
          
        }        
        

      }elseif ($RowConso060['porcentaje'] == 16.5) {               
  
        $numeRac2_060 = $RowConso060['recibo'];
        $numeFac2_060 = $RowConso060['numero']; 
        $beseComisionApro2_060 = $RowConso060['base_com'];
        $dias0602_060 = $RowConso060['dias'];
        $fecha_doc2_060 = $RowConso060['fecha_doc'];
        $sacaMes2_060 = date('m', strtotime($fecha_doc2_060));

        if ($dias0602_060 <= 90) {
          $porcentaje_liqui_recaudo2_060 = 100;
            }elseif ($dias0602_060 > 120) {
                $porcentaje_liqui_recaudo2_060 = 0;
            } else {
              $porcentaje_liqui_recaudo2_060 = 80;
          }


         $consultaPPVdor2_060 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes2_060' AND anio = '$anio' AND idVendedor = 2";
         $resultadoPPVdor2_060 = mysqli_query($con, $consultaPPVdor2_060);

         while ($RowPPVdor2_060 = mysqli_fetch_array($resultadoPPVdor2_060)) {

          $porceCumpliVdor2_060 = $RowPPVdor2_060['porcentaje'];
                     
          if ($porceCumpliVdor2_060 > 100) {
            $liquidacion_PPTO2_060 = 0.005;
          }elseif ($porceCumpliVdor2_060 <= 80) {
            $liquidacion_PPTO2_060 = 0;
          }else {
            $liquidacion_PPTO2_060 = 0.004;
          }

        
          $comision_PPTO_Diego060_165 = ($beseComisionApro2_060*$liquidacion_PPTO2_060*($porcentaje_liqui_recaudo2_060/100))/1.81;
          $Total_comision_PPTO_Diego060_165 += $comision_PPTO_Diego060_165;
          
        } 
          
      }elseif ($RowConso060['porcentaje'] == 5) {

        $numeRac3_060 = $RowConso060['recibo'];
        $numeFac3_060 = $RowConso060['numero']; 
        $beseComisionApro3_060 = $RowConso060['base_com'];
        $dias0603_060 = $RowConso060['dias'];
        $fecha_doc3_060 = $RowConso060['fecha_doc'];
        $sacaMes3_060 = date('m', strtotime($fecha_doc3_060));

        if ($dias0603_060 <= 90) {
          $porcentaje_liqui_recaudo3_060 = 100;
            }elseif ($dias0603_060 > 120) {
                $porcentaje_liqui_recaudo3_060 = 0;
            } else {
              $porcentaje_liqui_recaudo3_060 = 80;
          }


         $consultaPPVdor3_060 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes3_060' AND anio = '$anio' AND idVendedor = 2";
         $resultadoPPVdor3_060 = mysqli_query($con, $consultaPPVdor3_060);

         while ($RowPPVdor3_060 = mysqli_fetch_array($resultadoPPVdor3_060)) {

          $porceCumpliVdor3_060 = $RowPPVdor3_060['porcentaje'];
                     
          if ($porceCumpliVdor3_060 > 100) {
            $liquidacion_PPTO3_060 = 0.005;
          }elseif ($porceCumpliVdor3_060 <= 80) {
            $liquidacion_PPTO3_060 = 0;
          }else {
            $liquidacion_PPTO3_060 = 0.004;
          }

        
          $comision_PPTO_Diego060_5 = ($beseComisionApro3_060*$liquidacion_PPTO3_060*($porcentaje_liqui_recaudo3_060/100))/6;
          $Total_comision_PPTO_Diego060_5 += $comision_PPTO_Diego060_5;
        
          
        }
        
      }elseif ($RowConso060['porcentaje'] == 20) {

        $numeRac4_060 = $RowConso060['recibo'];
        $numeFac4_060 = $RowConso060['numero']; 
        $beseComisionApro4_060 = $RowConso060['base_com'];
        $dias0604_060 = $RowConso060['dias'];
        $fecha_doc4_060 = $RowConso060['fecha_doc'];
        $sacaMes4_060 = date('m', strtotime($fecha_doc4_060));

        if ($dias0604_060 <= 90) {
          $porcentaje_liqui_recaudo4_060 = 100;
            }elseif ($dias0604_060 > 120) {
                $porcentaje_liqui_recaudo4_060 = 0;
            } else {
              $porcentaje_liqui_recaudo4_060 = 80;
          }


         $consultaPPVdor4_060 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes4_060' AND anio = '$anio' AND idVendedor = 2";
         $resultadoPPVdor4_060 = mysqli_query($con, $consultaPPVdor4_060);

         while ($RowPPVdor4_060 = mysqli_fetch_array($resultadoPPVdor4_060)) {

          $porceCumpliVdor4_060 = $RowPPVdor4_060['porcentaje'];
                     
          if ($porceCumpliVdor4_060 > 100) {
            $liquidacion_PPTO4_060 = 0.005;
          }elseif ($porceCumpliVdor4_060 <= 80) {
            $liquidacion_PPTO4_060 = 0;
          }else {
            $liquidacion_PPTO4_060 = 0.004;
          }

        
          $comision_PPTO_Diego060_20 = ($beseComisionApro4_060*$liquidacion_PPTO4_060*($porcentaje_liqui_recaudo4_060/100))/1.5;
          $Total_comision_PPTO_Diego060_20 += $comision_PPTO_Diego060_20;
          
        }

        
      }  

    } 

   /*echo $Total_comision_PPTO_Diego060." Normales"."<br>";
    echo $Total_comision_PPTO_Diego060_10." Al 10%"."<br>";
    echo $Total_comision_PPTO_Diego060_165." Al 16.5%"."<br>";
    echo $Total_comision_PPTO_Diego060_5." Al 16.5%"."<br>";
    echo $Total_comision_PPTO_Diego060_20." Al 20%"."<br>";*/ 
    
  //TERMINA CALCULO COMISIONES MESSER CAZUCA................................................................................................................................................................................................

  //CALCULO COMISIONES MESSER MANIZALES................................................................................................................................................................................

  $consultaConso370 = "SELECT * FROM comisiones_manizales WHERE mes = '$mesC' AND dias_transcurridos != ''";
  $resultadoConso370 = mysqli_query($con, $consultaConso370);

  while ($RowConso370 = mysqli_fetch_array($resultadoConso370)) {       
   if ($RowConso370['dias_transcurridos'] != '' && $RowConso370['porc_3'] != '' && $RowConso370['porc_4'] != '') {
       $numeRac370 = $RowConso370['rec'];
       $numeFac370 = $RowConso370['no_fact']; 
       $beseComisionApro370 = $RowConso370['base_com'];
       $dias370 = $RowConso370['dias_transcurridos'];
       $fecha_doc370 = $RowConso370['fecha_doc'];
       $sacaMes370 = date('m', strtotime($fecha_doc370)); 

       //echo $numeRac." - ".$numeFac." - ".$beseComisionApro." - ".$dias051."<br>";

       if ($dias370 <= 90) {
          $porcentaje_liqui_recaudo370 = 100;
       }elseif ($dias370 > 120) {
          $porcentaje_liqui_recaudo370 = 0;
       } else {
        $porcentaje_liqui_recaudo370 = 80;
       }

       $consultaPPVdor370 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes370' AND anio = '$anio' AND idVendedor = 2";
       $resultadoPPVdor370 = mysqli_query($con, $consultaPPVdor370);

       while ($RowPPVdor370 = mysqli_fetch_array($resultadoPPVdor370)) {

        $porceCumpliVdor370 = $RowPPVdor370['porcentaje'];
                   
        if ($porceCumpliVdor370 > 100) {
          $liquidacion_PPTO370 = 0.005;
        }elseif ($porceCumpliVdor370 <= 80) {
          $liquidacion_PPTO370 = 0;
        }else {
          $liquidacion_PPTO370 = 0.004;
        }

        //echo $numeRac370." - ".$numeFac370." - ".$beseComisionApro370." - ".$dias370." - "." ( ".$sacaMes370." ) "." - ".$porceCumpliVdor370."%"." - ".$liquidacion_PPTO370." - ".$porcentaje_liqui_recaudo370."%"."<br>";

        $comision_PPTO_Diego370 = $beseComisionApro370*$liquidacion_PPTO370*($porcentaje_liqui_recaudo370/100);
        $Total_comision_PPTO_Diego370 += $comision_PPTO_Diego370;
        //echo $comision_PPTO_Diego370."<br>";        

      }  


    }elseif ($RowConso370['porcentaje'] == 10) {

      $numeRac1_370 = $RowConso370['rec'];
      $numeFac1_370 = $RowConso370['no_fact']; 
      $beseComisionApro1_370 = $RowConso370['base_com'];
      $dias0601_370 = $RowConso370['dias_transcurridos'];
      $fecha_doc1_370 = $RowConso370['fecha_doc'];
      $sacaMes1_370 = date('m', strtotime($fecha_doc1_370));

      if ($dias0601_370 <= 90) {
        $porcentaje_liqui_recaudo1_370 = 100;
          }elseif ($dias0601_370 > 120) {
              $porcentaje_liqui_recaudo1_370 = 0;
          } else {
            $porcentaje_liqui_recaudo1_370 = 80;
        }


       $consultaPPVdor1_370 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes1_370' AND anio = '$anio' AND idVendedor = 2";
       $resultadoPPVdor1_370 = mysqli_query($con, $consultaPPVdor1_370);

       while ($RowPPVdor1_370 = mysqli_fetch_array($resultadoPPVdor1_370)) {

        $porceCumpliVdor1_370 = $RowPPVdor1_370['porcentaje'];
                   
        if ($porceCumpliVdor1_370 > 100) {
          $liquidacion_PPTO1_370 = 0.005;
        }elseif ($porceCumpliVdor1_370 <= 80) {
          $liquidacion_PPTO1_370 = 0;
        }else {
          $liquidacion_PPTO1_370 = 0.004;
        }

      
        $comision_PPTO_Diego370_10 = ($beseComisionApro1_370*$liquidacion_PPTO1_370*($porcentaje_liqui_recaudo1_370/100))/3;
        $Total_comision_PPTO_Diego370_10 += $comision_PPTO_Diego370_10;
        
      }       

    }elseif ($RowConso370['porcentaje'] == 16.5) {               

      $numeRac2_370 = $RowConso370['rec'];
      $numeFac2_370 = $RowConso370['no_fact']; 
      $beseComisionApro2_370 = $RowConso370['base_com'];
      $dias0602_370 = $RowConso370['dias_transcurridos'];
      $fecha_doc2_370 = $RowConso370['fecha_doc'];
      $sacaMes2_370 = date('m', strtotime($fecha_doc2_370));

      if ($dias0602_370 <= 90) {
        $porcentaje_liqui_recaudo2_370 = 100;
          }elseif ($dias0602_370 > 120) {
              $porcentaje_liqui_recaudo2_370 = 0;
          } else {
            $porcentaje_liqui_recaudo2_370 = 80;
        }


       $consultaPPVdor2_370 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes2_370' AND anio = '$anio' AND idVendedor = 2";
       $resultadoPPVdor2_370 = mysqli_query($con, $consultaPPVdor2_370);

       while ($RowPPVdor2_370 = mysqli_fetch_array($resultadoPPVdor2_370)) {

        $porceCumpliVdor2_370 = $RowPPVdor2_370['porcentaje'];
                   
        if ($porceCumpliVdor2_370 > 100) {
          $liquidacion_PPTO2_370 = 0.005;
        }elseif ($porceCumpliVdor2_370 <= 80) {
          $liquidacion_PPTO2_370 = 0;
        }else {
          $liquidacion_PPTO2_370 = 0.004;
        }

      
        $comision_PPTO_Diego370_165 = ($beseComisionApro2_370*$liquidacion_PPTO2_370*($porcentaje_liqui_recaudo2_370/100))/1.81;
        $Total_comision_PPTO_Diego370_165 += $comision_PPTO_Diego370_165;
      
        
      }
        
    }elseif ($RowConso370['porcentaje'] == 5) {

      $numeRac3_370 = $RowConso370['rec'];
      $numeFac3_370 = $RowConso370['no_fact']; 
      $beseComisionApro3_370 = $RowConso370['base_com'];
      $dias0603_370 = $RowConso370['dias_transcurridos'];
      $fecha_doc3_370 = $RowConso370['fecha_doc'];
      $sacaMes3_370 = date('m', strtotime($fecha_doc3_370));

      if ($dias0603_370 <= 90) {
        $porcentaje_liqui_recaudo3_370 = 100;
          }elseif ($dias0603_370 > 120) {
              $porcentaje_liqui_recaudo3_370 = 0;
          } else {
            $porcentaje_liqui_recaudo3_370 = 80;
        }


       $consultaPPVdor3_370 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes3_370' AND anio = '$anio' AND idVendedor = 2";
       $resultadoPPVdor3_370 = mysqli_query($con, $consultaPPVdor3_370);

       while ($RowPPVdor3_370 = mysqli_fetch_array($resultadoPPVdor3_370)) {

        $porceCumpliVdor3_370 = $RowPPVdor3_370['porcentaje'];
                   
        if ($porceCumpliVdor3_370 > 100) {
          $liquidacion_PPTO3_370 = 0.005;
        }elseif ($porceCumpliVdor3_370 <= 80) {
          $liquidacion_PPTO3_370 = 0;
        }else {
          $liquidacion_PPTO3_370 = 0.004;
        }

      
        $comision_PPTO_Diego370_5 = ($beseComisionApro3_370*$liquidacion_PPTO3_370*($porcentaje_liqui_recaudo3_370/100))/6;
        $Total_comision_PPTO_Diego370_5 += $comision_PPTO_Diego370_5;
        
      }
      
    }elseif ($RowConso370['porcentaje'] == 20) {

      $numeRac4_370 = $RowConso370['rec'];
      $numeFac4_370 = $RowConso370['no_fact']; 
      $beseComisionApro4_370 = $RowConso370['base_com'];
      $dias0604_370 = $RowConso370['dias_transcurridos'];
      $fecha_doc4_370 = $RowConso370['fecha_doc'];
      $sacaMes4_370 = date('m', strtotime($fecha_doc4_370));

      if ($dias0604_370 <= 90) {
        $porcentaje_liqui_recaudo4_370 = 100;
          }elseif ($dias0604_370 > 120) {
              $porcentaje_liqui_recaudo4_370 = 0;
          } else {
            $porcentaje_liqui_recaudo4_370 = 80;
        }


       $consultaPPVdor4_370 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes4_370' AND anio = '$anio' AND idVendedor = 2";
       $resultadoPPVdor4_370 = mysqli_query($con, $consultaPPVdor4_370);

       while ($RowPPVdor4_370 = mysqli_fetch_array($resultadoPPVdor4_370)) {

        $porceCumpliVdor4_370 = $RowPPVdor4_370['porcentaje'];
                   
        if ($porceCumpliVdor4_370 > 100) {
          $liquidacion_PPTO4_370 = 0.005;
        }elseif ($porceCumpliVdor4_370 <= 80) {
          $liquidacion_PPTO4_370 = 0;
        }else {
          $liquidacion_PPTO4_370 = 0.004;
        }

      
        $comision_PPTO_Diego370_20 = ($beseComisionApro4_370*$liquidacion_PPTO4_370*($porcentaje_liqui_recaudo4_370/100))/1.5;
        $Total_comision_PPTO_Diego370_20 += $comision_PPTO_Diego370_20;
        
      }
  
    }  

  }

  /*echo $Total_comision_PPTO_Diego370." Normales"."<br>";
  echo $Total_comision_PPTO_Diego370_10." 10%"."<br>"; 
  echo $Total_comision_PPTO_Diego370_165." 16.5%"."<br>"; 
  echo $Total_comision_PPTO_Diego370_5." 5%"."<br>"; 
  echo $Total_comision_PPTO_Diego370_20." 20%"."<br>";*/

//TERMINA CALCULO COMISIONES MESSER MANIZALES................................................................................................................................................................................................  

//CALCULO COMISIONES MESSER ITAGUI................................................................................................................................................................................

$consultaConso590 = "SELECT * FROM comisiones_itagui WHERE mes = '$mesC' AND dias != ''";
$resultadoConso590 = mysqli_query($con, $consultaConso590);

while ($RowConso590 = mysqli_fetch_array($resultadoConso590)) {       
 if ($RowConso590['dias'] != '' && $RowConso590['porc_3'] != '' && $RowConso590['porc_4'] != '') {
     $numeRac590 = $RowConso590['recibo'];
     $numeFac590 = $RowConso590['numero_']; 
     $beseComisionApro590 = $RowConso590['base_com'];
     $dias590 = $RowConso590['dias'];
     $fecha_doc590 = $RowConso590['fecha_doc'];
     $sacaMes590 = date('m', strtotime($fecha_doc590)); 

     //echo $numeRac." - ".$numeFac." - ".$beseComisionApro." - ".$dias051."<br>";

     if ($dias590 <= 90) {
        $porcentaje_liqui_recaudo590 = 100;
     }elseif ($dias590 > 120) {
        $porcentaje_liqui_recaudo590 = 0;
     } else {
      $porcentaje_liqui_recaudo590 = 80;
     }

     $consultaPPVdor590 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes590' AND anio = '$anio' AND idVendedor = 2";
     $resultadoPPVdor590 = mysqli_query($con, $consultaPPVdor590);

     while ($RowPPVdor590 = mysqli_fetch_array($resultadoPPVdor590)) {

      $porceCumpliVdor590 = $RowPPVdor590['porcentaje'];
                 
      if ($porceCumpliVdor590 > 100) {
        $liquidacion_PPTO590 = 0.005;
      }elseif ($porceCumpliVdor590 <= 80) {
        $liquidacion_PPTO590 = 0;
      }else {
        $liquidacion_PPTO590 = 0.004;
      }

      //echo $numeRac590." - ".$numeFac590." - ".$beseComisionApro590." - ".$dias590." - "." ( ".$sacaMes590." ) "." - ".$porceCumpliVdor590."%"." - ".$liquidacion_PPTO590." - ".$porcentaje_liqui_recaudo590."%"."<br>";

      $comision_PPTO_Diego590 = $beseComisionApro590*$liquidacion_PPTO590*($porcentaje_liqui_recaudo590/100);
      $Total_comision_PPTO_Diego590 += $comision_PPTO_Diego590;
      
      //echo $comision_PPTO_Diego590."<br>";        

    }  


  }elseif ($RowConso590['porcentaje'] == 10) {

    $numeRac1_590 = $RowConso590['recibo'];
    $numeFac1_590 = $RowConso590['numero_']; 
    $beseComisionApro1_590 = $RowConso590['base_com'];
    $dias0601_590 = $RowConso590['dias'];
    $fecha_doc1_590 = $RowConso590['fecha_doc'];
    $sacaMes1_590 = date('m', strtotime($fecha_doc1_590));

    if ($dias0601_590 <= 90) {
      $porcentaje_liqui_recaudo1_590 = 100;
        }elseif ($dias0601_590 > 120) {
            $porcentaje_liqui_recaudo1_590 = 0;
        } else {
          $porcentaje_liqui_recaudo1_590 = 80;
      }


     $consultaPPVdor1_590 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes1_590' AND anio = '$anio' AND idVendedor = 2";
     $resultadoPPVdor1_590 = mysqli_query($con, $consultaPPVdor1_590);

     while ($RowPPVdor1_590 = mysqli_fetch_array($resultadoPPVdor1_590)) {

      $porceCumpliVdor1_590 = $RowPPVdor1_590['porcentaje'];
                 
      if ($porceCumpliVdor1_590 > 100) {
        $liquidacion_PPTO1_590 = 0.005;
      }elseif ($porceCumpliVdor1_590 <= 80) {
        $liquidacion_PPTO1_590 = 0;
      }else {
        $liquidacion_PPTO1_590 = 0.004;
      }

    
      $comision_PPTO_Diego590_10 = ($beseComisionApro1_590*$liquidacion_PPTO1_590*($porcentaje_liqui_recaudo1_590/100))/3;
      $Total_comision_PPTO_Diego590_10 += $comision_PPTO_Diego590_10;
      //echo $comision_PPTO_Diego590_10."<br>";  
      
    }       

  }elseif ($RowConso590['porcentaje'] == 16.5) { 

    $numeRac2_590 = $RowConso590['recibo'];
    $numeFac2_590 = $RowConso590['numero_']; 
    $beseComisionApro2_590 = $RowConso590['base_com'];
    $dias0602_590 = $RowConso590['dias'];
    $fecha_doc2_590 = $RowConso590['fecha_doc'];
    $sacaMes2_590 = date('m', strtotime($fecha_doc2_590));

    if ($dias0602_590 <= 90) {
      $porcentaje_liqui_recaudo2_590 = 100;
        }elseif ($dias0602_590 > 120) {
            $porcentaje_liqui_recaudo2_590 = 0;
        } else {
          $porcentaje_liqui_recaudo2_590 = 80;
      }


     $consultaPPVdor2_590 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes2_590' AND anio = '$anio' AND idVendedor = 2";
     $resultadoPPVdor2_590 = mysqli_query($con, $consultaPPVdor2_590);

     while ($RowPPVdor2_590 = mysqli_fetch_array($resultadoPPVdor2_590)) {

      $porceCumpliVdor2_590 = $RowPPVdor2_590['porcentaje'];
                 
      if ($porceCumpliVdor2_590 > 100) {
        $liquidacion_PPTO2_590 = 0.005;
      }elseif ($porceCumpliVdor2_590 <= 80) {
        $liquidacion_PPTO2_590 = 0;
      }else {
        $liquidacion_PPTO2_590 = 0.004;
      }

    
      $comision_PPTO_Diego590_165 = ($beseComisionApro2_590*$liquidacion_PPTO2_590*($porcentaje_liqui_recaudo2_590/100))/1.81;
      $Total_comision_PPTO_Diego590_165 += $comision_PPTO_Diego590_165;
      
    }
      
  }elseif ($RowConso590['porcentaje'] == 5) {

    $numeRac3_590 = $RowConso590['recibo'];
    $numeFac3_3590= $RowConso590['numero_']; 
    $beseComisionApro3_590 = $RowConso590['base_com'];
    $dias0603_590 = $RowConso590['dias'];
    $fecha_doc3_590 = $RowConso590['fecha_doc'];
    $sacaMes3_590 = date('m', strtotime($fecha_doc3_590));

    if ($dias0603_590 <= 90) {
      $porcentaje_liqui_recaudo3_590 = 100;
        }elseif ($dias0603_590 > 120) {
            $porcentaje_liqui_recaudo3_590 = 0;
        } else {
          $porcentaje_liqui_recaudo3_590 = 80;
      }


     $consultaPPVdor3_590 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes3_590' AND anio = '$anio' AND idVendedor = 2";
     $resultadoPPVdor3_590 = mysqli_query($con, $consultaPPVdor3_590);

     while ($RowPPVdor3_590 = mysqli_fetch_array($resultadoPPVdor3_590)) {

      $porceCumpliVdor3_590 = $RowPPVdor3_590['porcentaje'];
                 
      if ($porceCumpliVdor3_590 > 100) {
        $liquidacion_PPTO3_590 = 0.005;
      }elseif ($porceCumpliVdor3_590 <= 80) {
        $liquidacion_PPTO3_590 = 0;
      }else {
        $liquidacion_PPTO3_590 = 0.004;
      }

    
      $comision_PPTO_Diego590_5 = ($beseComisionApro3_590*$liquidacion_PPTO3_590*($porcentaje_liqui_recaudo3_590/100))/6;
      $Total_comision_PPTO_Diego590_5 += $comision_PPTO_Diego590_5;
      
    }
    
  }elseif ($RowConso590['porcentaje'] == 20) {

    $numeRac4_590 = $RowConso590['rec'];
    $numeFac4_590 = $RowConso590['no_fact']; 
    $beseComisionApro4_590 = $RowConso590['base_com'];
    $dias0604_590 = $RowConso590['dias_transcurridos'];
    $fecha_doc4_590 = $RowConso590['fecha_doc'];
    $sacaMes4_590 = date('m', strtotime($fecha_doc4_590));

    if ($dias0604_590 <= 90) {
      $porcentaje_liqui_recaudo4_590 = 100;
        }elseif ($dias0604_590 > 120) {
            $porcentaje_liqui_recaudo4_590 = 0;
        } else {
          $porcentaje_liqui_recaudo4_590 = 80;
      }


     $consultaPPVdor4_590 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes4_590' AND anio = '$anio' AND idVendedor = 2";
     $resultadoPPVdor4_590 = mysqli_query($con, $consultaPPVdor4_590);

     while ($RowPPVdor4_590 = mysqli_fetch_array($resultadoPPVdor4_590)) {

      $porceCumpliVdor4_590 = $RowPPVdor4_590['porcentaje'];
                 
      if ($porceCumpliVdor4_590 > 100) {
        $liquidacion_PPTO4_590 = 0.005;
      }elseif ($porceCumpliVdor4_590 <= 80) {
        $liquidacion_PPTO4_590 = 0;
      }else {
        $liquidacion_PPTO4_590 = 0.004;
      }   

      $comision_PPTO_Diego590_20 = ($beseComisionApro4_590*$liquidacion_PPTO4_590*($porcentaje_liqui_recaudo4_590/100))/1.5;
      $Total_comision_PPTO_Diego590_20 += $comision_PPTO_Diego590_20;           
    }

  } 

} 

/*echo $Total_comision_PPTO_Diego590." Normales"."<br>";
echo $Total_comision_PPTO_Diego590_10." 10%"."<br>";
echo $Total_comision_PPTO_Diego590_165." 16.5%"."<br>";
echo $Total_comision_PPTO_Diego590_5." 5%"."<br>";
echo $Total_comision_PPTO_Diego590_20." 20%"."<br>";*/

//TERMINA CALCULO COMISIONES MESSER ITAGUI................................................................................................................................................................................................

      //Suma de comisiones toberin
      $total_messer_Toberin = $Total_comision_PPTO_Diego+$Total_comision_PPTO_Diego_10+$Total_comision_PPTO_Diego_165+$Total_comision_PPTO_Diego_5+$Total_comision_PPTO_Diego_20;

      //Suma de comisiones cazuca
      $total_messer_Cazuca = $Total_comision_PPTO_Diego060+$Total_comision_PPTO_Diego060_10+$Total_comision_PPTO_Diego060_165+$Total_comision_PPTO_Diego060_5+$Total_comision_PPTO_Diego060_20; 

      //Suma de comisiones Manizales
      $total_messer_Manizales = $Total_comision_PPTO_Diego370+$Total_comision_PPTO_Diego370_10+$Total_comision_PPTO_Diego370_165+$Total_comision_PPTO_Diego370_5+$Total_comision_PPTO_Diego370_20;

      //Suma de comisiones Itagui
      $total_messer_Itagui = $Total_comision_PPTO_Diego590+$Total_comision_PPTO_Diego590_10+$Total_comision_PPTO_Diego590_165+$Total_comision_PPTO_Diego590_5+$Total_comision_PPTO_Diego590_20;

      $total_comision_rec_x_messer_Diego = round($total_messer_Toberin+$total_messer_Cazuca+$total_messer_Manizales+$total_messer_Itagui);

      $consulta = "SELECT * FROM vt_ing WHERE tercero_interno = '$nombre_vendedor'";
      $resultado = mysqli_query($con, $consulta);

      while($linea = mysqli_fetch_array($resultado)){
        $cantidad = $linea["cantidad"];
        $valor_unitario = $linea["valor_unitario"];

        $valor_ventas = $cantidad * $valor_unitario;

        $ventas_ingegas_vendedor_D += $valor_ventas;
      }      
      
      $total_comision_D = $total_comision_rec_x_messer_Diego + $porcentajeRecaudo;

      //IMPRIME DIEGO MARTINEZ
      $objPHPExcel->getActiveSheet()->setCellValue('G5', $subtotalDiego);
      $objPHPExcel->getActiveSheet()->setCellValue('H5', $total_NCE_sucursales);
      $objPHPExcel->getActiveSheet()->setCellValue('I5', $total_ventas_reales_Diego);
      $objPHPExcel->getActiveSheet()->setCellValue('J5', $porcentaje_Cumpli_D."%");
      $objPHPExcel->getActiveSheet()->setCellValue('K5', $total_comision_rec_x_messer_Diego);
      $objPHPExcel->getActiveSheet()->setCellValue('M5', $ventas_ingegas_vendedor_D);
      $objPHPExcel->getActiveSheet()->setCellValue('N5', $porcentajeRecaudo);
      $objPHPExcel->getActiveSheet()->setCellValue('O5', $total_comision_D); 
    
  //...........................................INICIA CARLOS ALBA......................................................................................... 
      
    } elseif ($id_vendedor == 3) {

      $consulta_EB = "SELECT * FROM ventas_producto WHERE mes = '$mesC' AND anio = '$anio'";
      $resultado_EB = mysqli_query($con, $consulta_EB);

      while($linea_EB = mysqli_fetch_array($resultado_EB)){
        $promotor_retail = $linea_EB["promotor_retail"];
        $valor_total = $linea_EB["valor_total"];
        $tipo_doc = $linea_EB["tipo_doc"];
        $numero_legal = $linea_EB["numero_legal"];

        $clienteC = $linea_EB["cliente"];
        $valores = 9;
        $cliente = substr(str_repeat(0, $valores).$clienteC, - $valores);

        $sucursal = $linea_EB['sucursal'];        

        //echo $cliente."<br>";

        $consulta1_EB = "SELECT * FROM asignacion_vendedores WHERE codigo = '$cliente'";
        $resultado1_EB = mysqli_query($con, $consulta1_EB);

        while ($linea1_EB = mysqli_fetch_array($resultado1_EB)) {
          $vendedor = $linea1_EB["vendedor"];          
          //echo $cliente." - ".$vendedor." - ".$sucursal."<br>";                     
        }
        if ($sucursal == '051' && $vendedor == 'EB') {
          $suma_EB_VT += $valor_total;
        }

        if ($sucursal == '060' && $vendedor == 'EB') {
          $suma_EB_VS += $valor_total;
        }

        if ($sucursal == '370' && $vendedor == 'EB') {
          $suma_EB_VM += $valor_total;
        }

        if ($sucursal == '590' && $vendedor == 'EB') {
          $suma_EB_VITA += $valor_total;
        }
   
      }

      if($suma_EB_VT == 0){
        $suma_EB_VT = "-";
      }

      if($suma_EB_VS == 0){
        $suma_EB_VS = "-";
      }

      if($suma_EB_VM == 0){
        $suma_EB_VM = "-";
      }

      if($suma_EB_VITA == 0){
        $suma_EB_VITA = "-";
      }
          
      //echo $suma_EB_VT."<br>".$suma_EB_VS; 

      //INICIA CALCULO VENTAS EB TOBERIN.........................................................................................................
      /*$consulta051 = "SELECT * FROM ventas_producto WHERE sucursal = '051' AND mes = '$mesC' AND anio = '$anio' AND promotor_retail = 'EB'";
      $resultado051 = mysqli_query($con, $consulta051);

      while($linea051 = mysqli_fetch_array($resultado051)){
        $promotor_retail051 = $linea051["promotor_retail"];
        $valor_total051 = $linea051["valor_total"];
        $tipo_doc051 = $linea051["tipo_doc"];
        $numero_legal051 = $linea051["numero_legal"];
        $cliente051 = $linea051["cliente"];
                
        $eb_toberin += $valor_total051;
      }*/


      //TERMINA CALCULO VENTA EB TOBERIN.........................................................................................................
           
      $subtotalEB = $suma_EB_VT + $suma_EB_VS + $suma_EB_VM + $suma_EB_VITA;

      $consultaVEB = "SELECT * FROM vt_ing WHERE tercero_interno = '$nombre_vendedor'";
      $resultadoVEB = mysqli_query($con, $consultaVEB);

      while($lineaVEB = mysqli_fetch_array($resultadoVEB)){
        $cantidad = $lineaVEB["cantidad"];
        $valor_unitario = $lineaVEB["valor_unitario"];

        $valor_ventas = $cantidad * $valor_unitario;

        $ventas_ingegas_vendedor_EB += $valor_ventas;
      }

      $total_vta_EB = ($subtotalEB - $total_NB_NC) + $ventas_ingegas_vendedor_EB;
      $porcentaje_Cumpli_EB =  round(($total_vta_EB/$valorPP)*100);
      
    //REGISTRAR PORCENTAJE........................................................................................................................................
    $existePPCUM_EB = "SELECT * FROM porce_cumpli_vendedor WHERE idVendedor = 3 AND mes LIKE '%$mesC%' AND anio = '$anio'";
    $resultExistePPCUM_EB = mysqli_query($con, $existePPCUM_EB);

      if (mysqli_num_rows($resultExistePPCUM_EB) <= 0) {
          
          $mes_0_EB = sprintf("%02d", $mesC);
          $registraPPCUM_EB = "INSERT INTO porce_cumpli_vendedor (idVendedor, mes, anio, porcentaje) VALUES (3,'$mes_0_EB','$anio','$porcentaje_Cumpli_EB')";
          $ressultRegistroPPCUM_EB = mysqli_query($con, $registraPPCUM_EB);

      }

//INICIA CALCULO COMISIONES MESSER TOBERIN................................................................................................................................................................................................
     
      $consultaConso_EB = "SELECT * FROM comisiones_toberin WHERE mes = '$mesC' AND dias != ''";
      $resultadoConso_EB = mysqli_query($con, $consultaConso_EB);
      
      while ($RowConso_EB = mysqli_fetch_array($resultadoConso_EB)) {       
       if ($RowConso_EB['dias'] != '' && $RowConso_EB['porc_3'] != '' && $RowConso_EB['porc_4'] != '') {
           $nomCliente_EB = $RowConso_EB['cliente'];
           
           $codigo051 = $RowConso_EB['codigo'];
           $length = 9;
           $codigoV_CLI_051 = substr(str_repeat(0, $length).$codigo051, - $length);
      
           $numeRac_EB = $RowConso_EB['rec'];
           $numeFac_EB = $RowConso_EB['numero_']; 
           $beseComisionApro_EB = $RowConso_EB['base_trans'];
           $dias_EB = $RowConso_EB['dias'];
           $fecha_doc_EB = $RowConso_EB['fecha_doc'];
           $sacaMes_EB = date('m', strtotime($fecha_doc_EB));
           $sacaAnio_EB = date('Y', strtotime($fecha_doc_EB));
      
           //echo $codigo051."<br>";
           
           $AsVddor_EB = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_051'";
           $resultAsVddor_EB = mysqli_query($con, $AsVddor_EB);
      
           while ($rowVndor_EB = mysqli_fetch_array($resultAsVddor_EB)) {
              $venDor_EB = $rowVndor_EB['vendedor'];
      
              //echo $numeFac_EB." / ".$venDor_EB."<br>";
            }
              if ($venDor_EB == 'EB') {               
                
                   if ($dias_EB <= 90) {
                     $liq_rec_messer_EB = 100;
                   }elseif ($dias_EB > 120) {
                     $liq_rec_messer_EB = 0;
                   }else {
                     $liq_rec_messer_EB = 80;
                   }
      
                   $consultaPPVdor_EB = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_EB' AND anio = '$anio' AND idVendedor = 3";
                   $resultadoPPVdor_EB = mysqli_query($con, $consultaPPVdor_EB);
         
                   while ($RowPPVdor_EB = mysqli_fetch_array($resultadoPPVdor_EB)) {
         
                     $porceCumpliVdor_EB = $RowPPVdor_EB['porcentaje'];
                               
                     if ($porceCumpliVdor_EB  > 100) {
                         $liq_ppto_messer_EB = 0.009;
                     }elseif ($porceCumpliVdor_EB >= 80) {
                         $liq_ppto_messer_EB = 0.007;
                     }elseif ($porceCumpliVdor_EB >= 60 && $sacaAnio_EB == '2021' && $sacaMes_EB == '05' || $porceCumpliVdor_EB >= 60 && $sacaAnio_EB == '2021' && $sacaMes_EB == '06' || $porceCumpliVdor_EB >= 60 && $sacaAnio_EB == '2021' && $sacaMes_EB == '07') {
                       $liq_ppto_messer_EB = 0.0035;
                     }else {
                       $liq_ppto_messer_EB = 0;
                     }
                     
                     //echo $numeRac_EB." - ".$numeFac_EB." - ".$beseComisionApro_EB." - ".$dias051_EB." - ".$nomCliente_EB." - ".$venDor_EB." - ".$porceCumpliVdor_EB." - ".$liq_ppto_messer_EB." - ".$liq_rec_messer_EB."<br>";
                    
                     $comision_PPTO_EB = $beseComisionApro_EB*$liq_ppto_messer_EB*($liq_rec_messer_EB/100);
                     $Total_comision_PPTO_EB += $comision_PPTO_EB;  
      
                     //echo $numeRac_EB." - ".$numeFac_EB." - ".$beseComisionApro_EB." - ".$dias_EB." - ".$nomCliente_EB." - ".$venDor_EB." / ".$comision_PPTO_EB."<br>";
      
                     }
                   }            
      
               }elseif ($RowConso_EB['porcentaje'] == 10) {
      
                $nomCliente_EB = $RowConso_EB['cliente'];
           
           $codigo051 = $RowConso_EB['codigo'];
           $length = 9;
           $codigoV_CLI_051 = substr(str_repeat(0, $length).$codigo051, - $length);
      
           $numeRac_EB = $RowConso_EB['rec'];
           $numeFac_EB = $RowConso_EB['numero_']; 
           $beseComisionApro_EB = $RowConso_EB['base_trans'];
           $dias_EB = $RowConso_EB['dias'];
           $fecha_doc_EB = $RowConso_EB['fecha_doc'];
           $sacaMes_EB = date('m', strtotime($fecha_doc_EB));
           $sacaAnio_EB = date('Y', strtotime($fecha_doc_EB));
      
           //echo $nomCliente_EB."<br>";
           
           $AsVddor_EB = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_051'";
           $resultAsVddor_EB = mysqli_query($con, $AsVddor_EB);
      
           while ($rowVndor_EB = mysqli_fetch_array($resultAsVddor_EB)) {
              $venDor_EB = $rowVndor_EB['vendedor'];
            }
              if ($venDor_EB == 'EB') {               
                
                   if ($dias_EB <= 90) {
                     $liq_rec_messer_EB = 100;
                   }elseif ($dias_EB > 120) {
                     $liq_rec_messer_EB = 0;
                   }else {
                     $liq_rec_messer_EB = 80;
                   }
      
                   $consultaPPVdor_EB = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_EB' AND anio = '$anio' AND idVendedor = 3";
                   $resultadoPPVdor_EB = mysqli_query($con, $consultaPPVdor_EB);
         
                   while ($RowPPVdor_EB = mysqli_fetch_array($resultadoPPVdor_EB)) {
         
                     $porceCumpliVdor_EB = $RowPPVdor_EB['porcentaje'];
                               
                     if ($porceCumpliVdor_EB  > 100) {
                         $liq_ppto_messer_EB = 0.009;
                     }elseif ($porceCumpliVdor_EB >= 80) {
                         $liq_ppto_messer_EB = 0.007;
                     }elseif ($porceCumpliVdor_EB >= 60 && $sacaAnio_EB == '2021' && $sacaMes_EB == '05' || $porceCumpliVdor_EB >= 60 && $sacaAnio_EB == '2021' && $sacaMes_EB == '06' || $porceCumpliVdor_EB >= 60 && $sacaAnio_EB == '2021' && $sacaMes_EB == '07') {
                       $liq_ppto_messer_EB = 0.0035;
                     }else {
                       $liq_ppto_messer_EB = 0;
                     }
                     
                     //echo $numeRac_EB." - ".$numeFac_EB." - ".$beseComisionApro_EB." - ".$dias051_EB." - ".$nomCliente_EB." - ".$venDor_EB." - ".$porceCumpliVdor_EB." - ".$liq_ppto_messer_EB." - ".$liq_rec_messer_EB."<br>";
                    
                     $comision_PPTO_EB = $beseComisionApro_EB*$liq_ppto_messer_EB*($liq_rec_messer_EB/100)/3;
                     $Total_comision_PPTO_EB += $comision_PPTO_EB;  
      
                     //echo $numeRac_EB." - ".$numeFac_EB." - ".$beseComisionApro_EB." - ".$dias_EB." - ".$nomCliente_EB." - ".$venDor_EB." / ".$comision_PPTO_EB."<br>";
      
                     }
                   }
      
             }elseif ($RowConso_EB['porcentaje'] == 16.5) {
      
              $nomCliente_EB = $RowConso_EB['cliente'];
           
           $codigo051 = $RowConso_EB['codigo'];
           $length = 9;
           $codigoV_CLI_051 = substr(str_repeat(0, $length).$codigo051, - $length);
      
           $numeRac_EB = $RowConso_EB['rec'];
           $numeFac_EB = $RowConso_EB['numero_']; 
           $beseComisionApro_EB = $RowConso_EB['base_trans'];
           $dias_EB = $RowConso_EB['dias'];
           $fecha_doc_EB = $RowConso_EB['fecha_doc'];
           $sacaMes_EB = date('m', strtotime($fecha_doc_EB));
           $sacaAnio_EB = date('Y', strtotime($fecha_doc_EB));
      
           //echo $nomCliente_EB."<br>";
           
           $AsVddor_EB = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_051'";
           $resultAsVddor_EB = mysqli_query($con, $AsVddor_EB);
      
           while ($rowVndor_EB = mysqli_fetch_array($resultAsVddor_EB)) {
              $venDor_EB = $rowVndor_EB['vendedor'];
            }
              if ($venDor_EB == 'EB') {               
                
                   if ($dias_EB <= 90) {
                     $liq_rec_messer_EB = 100;
                   }elseif ($dias_EB > 120) {
                     $liq_rec_messer_EB = 0;
                   }else {
                     $liq_rec_messer_EB = 80;
                   }
      
                   $consultaPPVdor_EB = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_EB' AND anio = '$anio' AND idVendedor = 3";
                   $resultadoPPVdor_EB = mysqli_query($con, $consultaPPVdor_EB);
         
                   while ($RowPPVdor_EB = mysqli_fetch_array($resultadoPPVdor_EB)) {
         
                     $porceCumpliVdor_EB = $RowPPVdor_EB['porcentaje'];
                               
                     if ($porceCumpliVdor_EB  > 100) {
                         $liq_ppto_messer_EB = 0.009;
                     }elseif ($porceCumpliVdor_EB >= 80) {
                         $liq_ppto_messer_EB = 0.007;
                     }elseif ($porceCumpliVdor_EB >= 60 && $sacaAnio_EB == '2021' && $sacaMes_EB == '05' || $porceCumpliVdor_EB >= 60 && $sacaAnio_EB == '2021' && $sacaMes_EB == '06' || $porceCumpliVdor_EB >= 60 && $sacaAnio_EB == '2021' && $sacaMes_EB == '07') {
                       $liq_ppto_messer_EB = 0.0035;
                     }else {
                       $liq_ppto_messer_EB = 0;
                     }
                     
                     //echo $numeRac_EB." - ".$numeFac_EB." - ".$beseComisionApro_EB." - ".$dias051_EB." - ".$nomCliente_EB." - ".$venDor_EB." - ".$porceCumpliVdor_EB." - ".$liq_ppto_messer_EB." - ".$liq_rec_messer_EB."<br>";
                    
                     $comision_PPTO_EB = $beseComisionApro_EB*$liq_ppto_messer_EB*($liq_rec_messer_EB/100)/1.81;
                     $Total_comision_PPTO_EB += $comision_PPTO_EB;  
      
                     //echo $numeRac_EB." - ".$numeFac_EB." - ".$beseComisionApro_EB." - ".$dias_EB." - ".$nomCliente_EB." - ".$venDor_EB." / ".$comision_PPTO_EB."<br>";
      
                     }
                   }
      
             }elseif ($RowConso_EB['porcentaje'] == 5) {
      
              $nomCliente_EB = $RowConso_EB['cliente'];
           
           $codigo051 = $RowConso_EB['codigo'];
           $length = 9;
           $codigoV_CLI_051 = substr(str_repeat(0, $length).$codigo051, - $length);
      
           $numeRac_EB = $RowConso_EB['rec'];
           $numeFac_EB = $RowConso_EB['numero_']; 
           $beseComisionApro_EB = $RowConso_EB['base_trans'];
           $dias_EB = $RowConso_EB['dias'];
           $fecha_doc_EB = $RowConso_EB['fecha_doc'];
           $sacaMes_EB = date('m', strtotime($fecha_doc_EB));
           $sacaAnio_EB = date('Y', strtotime($fecha_doc_EB));
      
           //echo $nomCliente_EB."<br>";
           
           $AsVddor_EB = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_051'";
           $resultAsVddor_EB = mysqli_query($con, $AsVddor_EB);
      
           while ($rowVndor_EB = mysqli_fetch_array($resultAsVddor_EB)) {
              $venDor_EB = $rowVndor_EB['vendedor'];
            }
              if ($venDor_EB == 'EB') {               
                
                   if ($dias_EB <= 90) {
                     $liq_rec_messer_EB = 100;
                   }elseif ($dias_EB > 120) {
                     $liq_rec_messer_EB = 0;
                   }else {
                     $liq_rec_messer_EB = 80;
                   }
      
                   $consultaPPVdor_EB = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_EB' AND anio = '$anio' AND idVendedor = 3";
                   $resultadoPPVdor_EB = mysqli_query($con, $consultaPPVdor_EB);
         
                   while ($RowPPVdor_EB = mysqli_fetch_array($resultadoPPVdor_EB)) {
         
                     $porceCumpliVdor_EB = $RowPPVdor_EB['porcentaje'];
                               
                     if ($porceCumpliVdor_EB  > 100) {
                         $liq_ppto_messer_EB = 0.009;
                     }elseif ($porceCumpliVdor_EB >= 80) {
                         $liq_ppto_messer_EB = 0.007;
                     }elseif ($porceCumpliVdor_EB >= 60 && $sacaAnio_EB == '2021' && $sacaMes_EB == '05' || $porceCumpliVdor_EB >= 60 && $sacaAnio_EB == '2021' && $sacaMes_EB == '06' || $porceCumpliVdor_EB >= 60 && $sacaAnio_EB == '2021' && $sacaMes_EB == '07') {
                       $liq_ppto_messer_EB = 0.0035;
                     }else {
                       $liq_ppto_messer_EB = 0;
                     }
                     
                     //echo $numeRac_EB." - ".$numeFac_EB." - ".$beseComisionApro_EB." - ".$dias051_EB." - ".$nomCliente_EB." - ".$venDor_EB." - ".$porceCumpliVdor_EB." - ".$liq_ppto_messer_EB." - ".$liq_rec_messer_EB."<br>";
                    
                     $comision_PPTO_EB = $beseComisionApro_EB*$liq_ppto_messer_EB*($liq_rec_messer_EB/100)/5;
                     $Total_comision_PPTO_EB += $comision_PPTO_EB;  
      
                     //echo $numeRac_EB." - ".$numeFac_EB." - ".$beseComisionApro_EB." - ".$dias_EB." - ".$nomCliente_EB." - ".$venDor_EB." / ".$comision_PPTO_EB."<br>";
      
                     }
                   }
      
             }elseif ($RowConso_EB['porcentaje'] == 20) {
              $nomCliente_EB = $RowConso_EB['cliente'];
           
           $codigo051 = $RowConso_EB['codigo'];
           $length = 9;
           $codigoV_CLI_051 = substr(str_repeat(0, $length).$codigo051, - $length);
      
           $numeRac_EB = $RowConso_EB['rec'];
           $numeFac_EB = $RowConso_EB['numero_']; 
           $beseComisionApro_EB = $RowConso_EB['base_trans'];
           $dias_EB = $RowConso_EB['dias'];
           $fecha_doc_EB = $RowConso_EB['fecha_doc'];
           $sacaMes_EB = date('m', strtotime($fecha_doc_EB));
           $sacaAnio_EB = date('Y', strtotime($fecha_doc_EB));
      
           //echo $nomCliente_EB."<br>";
           
           $AsVddor_EB = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_051'";
           $resultAsVddor_EB = mysqli_query($con, $AsVddor_EB);
      
           while ($rowVndor_EB = mysqli_fetch_array($resultAsVddor_EB)) {
              $venDor_EB = $rowVndor_EB['vendedor'];
            }
              if ($venDor_EB == 'EB') {               
                
                   if ($dias_EB <= 90) {
                     $liq_rec_messer_EB = 100;
                   }elseif ($dias_EB > 120) {
                     $liq_rec_messer_EB = 0;
                   }else {
                     $liq_rec_messer_EB = 80;
                   }
      
                   $consultaPPVdor_EB = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_EB' AND anio = '$anio' AND idVendedor = 3";
                   $resultadoPPVdor_EB = mysqli_query($con, $consultaPPVdor_EB);
         
                   while ($RowPPVdor_EB = mysqli_fetch_array($resultadoPPVdor_EB)) {
         
                     $porceCumpliVdor_EB = $RowPPVdor_EB['porcentaje'];
                               
                     if ($porceCumpliVdor_EB  > 100) {
                         $liq_ppto_messer_EB = 0.009;
                     }elseif ($porceCumpliVdor_EB >= 80) {
                         $liq_ppto_messer_EB = 0.007;
                     }elseif ($porceCumpliVdor_EB >= 60 && $sacaAnio_EB == '2021' && $sacaMes_EB == '05' || $porceCumpliVdor_EB >= 60 && $sacaAnio_EB == '2021' && $sacaMes_EB == '06' || $porceCumpliVdor_EB >= 60 && $sacaAnio_EB == '2021' && $sacaMes_EB == '07') {
                       $liq_ppto_messer_EB = 0.0035;
                     }else {
                       $liq_ppto_messer_EB = 0;
                     }
                     
                     //echo $numeRac_EB." - ".$numeFac_EB." - ".$beseComisionApro_EB." - ".$dias051_EB." - ".$nomCliente_EB." - ".$venDor_EB." - ".$porceCumpliVdor_EB." - ".$liq_ppto_messer_EB." - ".$liq_rec_messer_EB."<br>";
                    
                     $comision_PPTO_EB = $beseComisionApro_EB*$liq_ppto_messer_EB*($liq_rec_messer_EB/100)/1.5;
                     $Total_comision_PPTO_EB += $comision_PPTO_EB;  
      
                     //echo $numeRac_EB." - ".$numeFac_EB." - ".$beseComisionApro_EB." - ".$dias_EB." - ".$nomCliente_EB." - ".$venDor_EB." / ".$comision_PPTO_EB."<br>";
      
                     }
                   }
         }  
      } 
      //echo $Total_comision_PPTO_EB;  
      
      //TRMINA CALCULO COMISIONES MESSER TOBERIN................................................................................................................................................................................................
      
      //INICIA CALCULO COMISIONES MESSER CAZUCA................................................................................................................................................................................................
       
      $consultaConso_EB_060 = "SELECT * FROM comisiones_cazuca WHERE mes = '$mesC'";
      $resultadoConso_EB_060 = mysqli_query($con, $consultaConso_EB_060);
      
      while ($RowConso_EB_060 = mysqli_fetch_array($resultadoConso_EB_060)) {       
      if ($RowConso_EB_060['dias'] != '' && $RowConso_EB_060['porc_3'] != '' && $RowConso_EB_060['porc_4'] != '') {
      
         $nomCliente_EB_060 = $RowConso_EB_060['cliente'];
         
         $codigo060 = $RowConso_EB_060['codigo'];
         $length060 = 9;
         $codigoV_CLI_060 = substr(str_repeat(0, $length060).$codigo060, - $length060); 
      
         $numeRac_EB_060 = $RowConso_EB_060['recibo'];
         $numeFac_EB_060 = $RowConso_EB_060['numero']; 
         $beseComisionApro_EB_060 = $RowConso_EB_060['base_com'];
         $dias_EB_060 = $RowConso_EB_060['dias'];
         $fecha_doc_EB_060 = $RowConso_EB_060['fecha_doc'];
         $sacaMes_EB_060 = date('m', strtotime($fecha_doc_EB_060));
         $sacaAnio_EB_060 = date('Y', strtotime($fecha_doc_EB_060));
      
         //echo $nomCliente_EB."<br>";
         
         $AsVddor_EB_060 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_060'";
         $resultAsVddor_EB_060 = mysqli_query($con, $AsVddor_EB_060);
      
         while ($rowVndor_EB_060 = mysqli_fetch_array($resultAsVddor_EB_060)) {
          $venDor_EB_060 = $rowVndor_EB_060['vendedor']; 
          }
            if ($venDor_EB_060 == 'EB') {                 
                 
                 if ($dias_EB_060 <= 90) {
                   $liq_rec_messer_EB_060 = 100;
                 }elseif ($dias_EB_060 > 120) {
                   $liq_rec_messer_EB_060 = 0;
                 }else {
                   $liq_rec_messer_EB_060 = 80;
                 }
      
                 $consultaPPVdor_EB_060 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_EB_060' AND anio = '$anio' AND idVendedor = 3";
                 $resultadoPPVdor_EB_060 = mysqli_query($con, $consultaPPVdor_EB_060);
       
                 while ($RowPPVdor_EB_060 = mysqli_fetch_array($resultadoPPVdor_EB_060)) {
       
                   $porceCumpliVdor_EB_060 = $RowPPVdor_EB_060['porcentaje'];
                             
                   if ($porceCumpliVdor_EB_060  > 100) {
                       $liq_ppto_messer_EB_060 = 0.009;
                   }elseif ($porceCumpliVdor_EB_060 >= 80) {
                       $liq_ppto_messer_EB_060 = 0.007;
                   }elseif ($porceCumpliVdor_EB_060 >= 60 && $sacaMes_EB_060 == '05' && $sacaAnio_EB_060 == '2021' || $porceCumpliVdor_EB_060 >= 60 && $sacaMes_EB_060 == '06' && $sacaAnio_EB_060 == '2021' || $porceCumpliVdor_EB_060 >= 60 && $sacaMes_EB_060 == '07' && $sacaAnio_EB_060 == '2021') {
                     $liq_ppto_messer_EB_060 = 0.0035;
                   }else {
                     $liq_ppto_messer_EB_060 = 0;
                   }
                   
                   //echo $numeRac_EB." - ".$numeFac_EB." - ".$beseComisionApro_EB." - ".$dias051_EB." - ".$nomCliente_EB." - ".$venDor_EB." - ".$porceCumpliVdor_EB." - ".$liq_ppto_messer_EB." - ".$liq_rec_messer_EB."<br>";
                  
                   $comision_PPTO_EB_060 = $beseComisionApro_EB_060*$liq_ppto_messer_EB_060*($liq_rec_messer_EB_060/100);
                   $Total_comision_PPTO_EB_060 += $comision_PPTO_EB_060;
      
                   //echo $comision_PPTO_EB."<br>"; 
      
                 }
                }        
      
             }elseif ($rowVndor_EB_060['vendedor'] == 'EB' && $RowConso_EB_060['porcentaje'] == 10) {
      
              $nomCliente_EB_060 = $RowConso_EB_060['cliente'];
         
         $codigo060 = $RowConso_EB_060['codigo'];
         $length060 = 9;
         $codigoV_CLI_060 = substr(str_repeat(0, $length060).$codigo060, - $length060); 
      
         $numeRac_EB_060 = $RowConso_EB_060['recibo'];
         $numeFac_EB_060 = $RowConso_EB_060['numero']; 
         $beseComisionApro_EB_060 = $RowConso_EB_060['base_com'];
         $dias_EB_060 = $RowConso_EB_060['dias'];
         $fecha_doc_EB_060 = $RowConso_EB_060['fecha_doc'];
         $sacaMes_EB_060 = date('m', strtotime($fecha_doc_EB_060));
         $sacaAnio_EB_060 = date('Y', strtotime($fecha_doc_EB_060));
      
         //echo $nomCliente_EB."<br>";
         
         $AsVddor_EB_060 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_060'";
         $resultAsVddor_EB_060 = mysqli_query($con, $AsVddor_EB_060);
      
         while ($rowVndor_EB_060 = mysqli_fetch_array($resultAsVddor_EB_060)) {
          $venDor_EB_060 = $rowVndor_EB_060['vendedor']; 
          }
            if ($venDor_EB_060 == 'EB') {                 
                 
                 if ($dias_EB_060 <= 90) {
                   $liq_rec_messer_EB_060 = 100;
                 }elseif ($dias_EB_060 > 120) {
                   $liq_rec_messer_EB_060 = 0;
                 }else {
                   $liq_rec_messer_EB_060 = 80;
                 }
      
                 $consultaPPVdor_EB_060 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_EB_060' AND anio = '$anio' AND idVendedor = 3";
                 $resultadoPPVdor_EB_060 = mysqli_query($con, $consultaPPVdor_EB_060);
       
                 while ($RowPPVdor_EB_060 = mysqli_fetch_array($resultadoPPVdor_EB_060)) {
       
                   $porceCumpliVdor_EB_060 = $RowPPVdor_EB_060['porcentaje'];
                             
                   if ($porceCumpliVdor_EB_060  > 100) {
                       $liq_ppto_messer_EB_060 = 0.009;
                   }elseif ($porceCumpliVdor_EB_060 >= 80) {
                       $liq_ppto_messer_EB_060 = 0.007;
                   }elseif ($porceCumpliVdor_EB_060 >= 60 && $sacaMes_EB_060 == '05' && $sacaAnio_EB_060 == '2021' || $porceCumpliVdor_EB_060 >= 60 && $sacaMes_EB_060 == '06' && $sacaAnio_EB_060 == '2021' || $porceCumpliVdor_EB_060 >= 60 && $sacaMes_EB_060 == '07' && $sacaAnio_EB_060 == '2021') {
                     $liq_ppto_messer_EB_060 = 0.0035;
                   }else {
                     $liq_ppto_messer_EB_060 = 0;
                   }
                   
                   //echo $numeRac_EB." - ".$numeFac_EB." - ".$beseComisionApro_EB." - ".$dias051_EB." - ".$nomCliente_EB." - ".$venDor_EB." - ".$porceCumpliVdor_EB." - ".$liq_ppto_messer_EB." - ".$liq_rec_messer_EB."<br>";
                  
                   $comision_PPTO_EB_060 = $beseComisionApro_EB_060*$liq_ppto_messer_EB_060*($liq_rec_messer_EB_060/100)/3;
                   $Total_comision_PPTO_EB_060 += $comision_PPTO_EB_060;
      
                   //echo $comision_PPTO_EB."<br>"; 
      
                 }
                } 
      
           }elseif ($RowConso_EB_060['porcentaje'] == 16.5) {
      
            $nomCliente_EB_060 = $RowConso_EB_060['cliente'];
         
         $codigo060 = $RowConso_EB_060['codigo'];
         $length060 = 9;
         $codigoV_CLI_060 = substr(str_repeat(0, $length060).$codigo060, - $length060); 
      
         $numeRac_EB_060 = $RowConso_EB_060['recibo'];
         $numeFac_EB_060 = $RowConso_EB_060['numero']; 
         $beseComisionApro_EB_060 = $RowConso_EB_060['base_com'];
         $dias_EB_060 = $RowConso_EB_060['dias'];
         $fecha_doc_EB_060 = $RowConso_EB_060['fecha_doc'];
         $sacaMes_EB_060 = date('m', strtotime($fecha_doc_EB_060));
         $sacaAnio_EB_060 = date('Y', strtotime($fecha_doc_EB_060));
      
         //echo $nomCliente_EB."<br>";
         
         $AsVddor_EB_060 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_060'";
         $resultAsVddor_EB_060 = mysqli_query($con, $AsVddor_EB_060);
      
         while ($rowVndor_EB_060 = mysqli_fetch_array($resultAsVddor_EB_060)) {
          $venDor_EB_060 = $rowVndor_EB_060['vendedor']; 
          }
            if ($venDor_EB_060 == 'EB') {                 
                 
                 if ($dias_EB_060 <= 90) {
                   $liq_rec_messer_EB_060 = 100;
                 }elseif ($dias_EB_060 > 120) {
                   $liq_rec_messer_EB_060 = 0;
                 }else {
                   $liq_rec_messer_EB_060 = 80;
                 }
      
                 $consultaPPVdor_EB_060 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_EB_060' AND anio = '$anio' AND idVendedor = 3";
                 $resultadoPPVdor_EB_060 = mysqli_query($con, $consultaPPVdor_EB_060);
       
                 while ($RowPPVdor_EB_060 = mysqli_fetch_array($resultadoPPVdor_EB_060)) {
       
                   $porceCumpliVdor_EB_060 = $RowPPVdor_EB_060['porcentaje'];
                             
                   if ($porceCumpliVdor_EB_060  > 100) {
                       $liq_ppto_messer_EB_060 = 0.009;
                   }elseif ($porceCumpliVdor_EB_060 >= 80) {
                       $liq_ppto_messer_EB_060 = 0.007;
                   }elseif ($porceCumpliVdor_EB_060 >= 60 && $sacaMes_EB_060 == '05' && $sacaAnio_EB_060 == '2021' || $porceCumpliVdor_EB_060 >= 60 && $sacaMes_EB_060 == '06' && $sacaAnio_EB_060 == '2021' || $porceCumpliVdor_EB_060 >= 60 && $sacaMes_EB_060 == '07' && $sacaAnio_EB_060 == '2021') {
                     $liq_ppto_messer_EB_060 = 0.0035;
                   }else {
                     $liq_ppto_messer_EB_060 = 0;
                   }
                   
                   //echo $numeRac_EB." - ".$numeFac_EB." - ".$beseComisionApro_EB." - ".$dias051_EB." - ".$nomCliente_EB." - ".$venDor_EB." - ".$porceCumpliVdor_EB." - ".$liq_ppto_messer_EB." - ".$liq_rec_messer_EB."<br>";
                  
                   $comision_PPTO_EB_060 = $beseComisionApro_EB_060*$liq_ppto_messer_EB_060*($liq_rec_messer_EB_060/100)/1.81;
                   $Total_comision_PPTO_EB_060 += $comision_PPTO_EB_060;
      
                   //echo $comision_PPTO_EB."<br>"; 
      
                 }
                }
      
           }elseif ($RowConso_EB_060['porcentaje'] == 5) {
      
            $nomCliente_EB_060 = $RowConso_EB_060['cliente'];
         
         $codigo060 = $RowConso_EB_060['codigo'];
         $length060 = 9;
         $codigoV_CLI_060 = substr(str_repeat(0, $length060).$codigo060, - $length060); 
      
         $numeRac_EB_060 = $RowConso_EB_060['recibo'];
         $numeFac_EB_060 = $RowConso_EB_060['numero']; 
         $beseComisionApro_EB_060 = $RowConso_EB_060['base_com'];
         $dias_EB_060 = $RowConso_EB_060['dias'];
         $fecha_doc_EB_060 = $RowConso_EB_060['fecha_doc'];
         $sacaMes_EB_060 = date('m', strtotime($fecha_doc_EB_060));
         $sacaAnio_EB_060 = date('Y', strtotime($fecha_doc_EB_060));
      
         //echo $nomCliente_EB."<br>";
         
         $AsVddor_EB_060 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_060'";
         $resultAsVddor_EB_060 = mysqli_query($con, $AsVddor_EB_060);
      
         while ($rowVndor_EB_060 = mysqli_fetch_array($resultAsVddor_EB_060)) {
          $venDor_EB_060 = $rowVndor_EB_060['vendedor']; 
          }
            if ($venDor_EB_060 == 'EB') {                 
                 
                 if ($dias_EB_060 <= 90) {
                   $liq_rec_messer_EB_060 = 100;
                 }elseif ($dias_EB_060 > 120) {
                   $liq_rec_messer_EB_060 = 0;
                 }else {
                   $liq_rec_messer_EB_060 = 80;
                 }
      
                 $consultaPPVdor_EB_060 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_EB_060' AND anio = '$anio' AND idVendedor = 3";
                 $resultadoPPVdor_EB_060 = mysqli_query($con, $consultaPPVdor_EB_060);
       
                 while ($RowPPVdor_EB_060 = mysqli_fetch_array($resultadoPPVdor_EB_060)) {
       
                   $porceCumpliVdor_EB_060 = $RowPPVdor_EB_060['porcentaje'];
                             
                   if ($porceCumpliVdor_EB_060  > 100) {
                       $liq_ppto_messer_EB_060 = 0.009;
                   }elseif ($porceCumpliVdor_EB_060 >= 80) {
                       $liq_ppto_messer_EB_060 = 0.007;
                   }elseif ($porceCumpliVdor_EB_060 >= 60 && $sacaMes_EB_060 == '05' && $sacaAnio_EB_060 == '2021' || $porceCumpliVdor_EB_060 >= 60 && $sacaMes_EB_060 == '06' && $sacaAnio_EB_060 == '2021' || $porceCumpliVdor_EB_060 >= 60 && $sacaMes_EB_060 == '07' && $sacaAnio_EB_060 == '2021') {
                     $liq_ppto_messer_EB_060 = 0.0035;
                   }else {
                     $liq_ppto_messer_EB_060 = 0;
                   }
                   
                   //echo $numeRac_EB." - ".$numeFac_EB." - ".$beseComisionApro_EB." - ".$dias051_EB." - ".$nomCliente_EB." - ".$venDor_EB." - ".$porceCumpliVdor_EB." - ".$liq_ppto_messer_EB." - ".$liq_rec_messer_EB."<br>";
                  
                   $comision_PPTO_EB_060 = $beseComisionApro_EB_060*$liq_ppto_messer_EB_060*($liq_rec_messer_EB_060/100)/6;
                   $Total_comision_PPTO_EB_060 += $comision_PPTO_EB_060;
      
                   //echo $comision_PPTO_EB."<br>"; 
      
                 }
                }
      
           }elseif ($RowConso_EB_060['porcentaje'] == 20) {
             
            $nomCliente_EB_060 = $RowConso_EB_060['cliente'];
         
         $codigo060 = $RowConso_EB_060['codigo'];
         $length060 = 9;
         $codigoV_CLI_060 = substr(str_repeat(0, $length060).$codigo060, - $length060); 
      
         $numeRac_EB_060 = $RowConso_EB_060['recibo'];
         $numeFac_EB_060 = $RowConso_EB_060['numero']; 
         $beseComisionApro_EB_060 = $RowConso_EB_060['base_com'];
         $dias_EB_060 = $RowConso_EB_060['dias'];
         $fecha_doc_EB_060 = $RowConso_EB_060['fecha_doc'];
         $sacaMes_EB_060 = date('m', strtotime($fecha_doc_EB_060));
         $sacaAnio_EB_060 = date('Y', strtotime($fecha_doc_EB_060));
      
         //echo $nomCliente_EB."<br>";
         
         $AsVddor_EB_060 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_060'";
         $resultAsVddor_EB_060 = mysqli_query($con, $AsVddor_EB_060);
      
         while ($rowVndor_EB_060 = mysqli_fetch_array($resultAsVddor_EB_060)) {
          $venDor_EB_060 = $rowVndor_EB_060['vendedor']; 
          }
            if ($venDor_EB_060 == 'EB') {                 
                 
                 if ($dias_EB_060 <= 90) {
                   $liq_rec_messer_EB_060 = 100;
                 }elseif ($dias_EB_060 > 120) {
                   $liq_rec_messer_EB_060 = 0;
                 }else {
                   $liq_rec_messer_EB_060 = 80;
                 }
      
                 $consultaPPVdor_EB_060 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_EB_060' AND anio = '$anio' AND idVendedor = 3";
                 $resultadoPPVdor_EB_060 = mysqli_query($con, $consultaPPVdor_EB_060);
       
                 while ($RowPPVdor_EB_060 = mysqli_fetch_array($resultadoPPVdor_EB_060)) {
       
                   $porceCumpliVdor_EB_060 = $RowPPVdor_EB_060['porcentaje'];
                             
                   if ($porceCumpliVdor_EB_060  > 100) {
                       $liq_ppto_messer_EB_060 = 0.009;
                   }elseif ($porceCumpliVdor_EB_060 >= 80) {
                       $liq_ppto_messer_EB_060 = 0.007;
                   }elseif ($porceCumpliVdor_EB_060 >= 60 && $sacaMes_EB_060 == '05' && $sacaAnio_EB_060 == '2021' || $porceCumpliVdor_EB_060 >= 60 && $sacaMes_EB_060 == '06' && $sacaAnio_EB_060 == '2021' || $porceCumpliVdor_EB_060 >= 60 && $sacaMes_EB_060 == '07' && $sacaAnio_EB_060 == '2021') {
                     $liq_ppto_messer_EB_060 = 0.0035;
                   }else {
                     $liq_ppto_messer_EB_060 = 0;
                   }
                   
                   //echo $numeRac_EB." - ".$numeFac_EB." - ".$beseComisionApro_EB." - ".$dias051_EB." - ".$nomCliente_EB." - ".$venDor_EB." - ".$porceCumpliVdor_EB." - ".$liq_ppto_messer_EB." - ".$liq_rec_messer_EB."<br>";
                  
                   $comision_PPTO_EB_060 = $beseComisionApro_EB_060*$liq_ppto_messer_EB_060*($liq_rec_messer_EB_060/100)/1.5;
                   $Total_comision_PPTO_EB_060 += $comision_PPTO_EB_060;
      
                   //echo $comision_PPTO_EB."<br>"; 
      
                 }
                }
         }      
      }
      //echo $Total_comision_PPTO_EB_060;
      //TERMINA CALCULO COMISIONES MESSER CAZUCA................................................................................................................................................................................................
      
      //INICIA CALCULO COMISIONES MESSER MANIZALES................................................................................................................................................................................................
       
      $consultaConso_EB_370 = "SELECT * FROM comisiones_manizales WHERE mes = '$mesC'";
      $resultadoConso_EB_370 = mysqli_query($con, $consultaConso_EB_370);
      
      while ($RowConso_EB_370 = mysqli_fetch_array($resultadoConso_EB_370)) {       
      if ($RowConso_EB_370['dias_transcurridos'] != '' && $RowConso_EB_370['porc_3'] != '' && $RowConso_EB_370['porc_4'] != '') {
      
       $nomCliente_EB_370 = $RowConso_EB_370['cliente'];
      
       $codigo370 = $RowConso_EB_370['codigo_cliente'];
       $length370 = 9;
       $codigoV_CLI_370 = substr(str_repeat(0, $length370).$codigo370, - $length370);
      
       $numeRac_EB_370 = $RowConso_EB_370['rec'];
       $numeFac_EB_370 = $RowConso_EB_370['no_fact']; 
       $beseComisionApro_EB_370 = $RowConso_EB_370['base_com'];
       $dias_EB_370 = $RowConso_EB_370['dias_transcurridos'];
       $fecha_doc_EB_370 = $RowConso_EB_370['fecha_doc'];
       $sacaMes_EB_370 = date('m', strtotime($fecha_doc_EB_370));
       $sacaAnio_EB_370 = date('Y', strtotime($fecha_doc_EB_370));
      
       //echo $nomCliente_EB_370."<br>";
       
       $AsVddor_EB_370 = "SELECT * FROM asignacion_vendedores WHERE codigo_cliente = '$codigoV_CLI_370'";
       $resultAsVddor_EB_370 = mysqli_query($con, $AsVddor_EB_370);
      
       while ($rowVndor_EB_370 = mysqli_fetch_array($resultAsVddor_EB_370)) {
            $venDor_EB_370 = $rowVndor_EB_370['vendedor'];
        }
      
          if ($venDor_EB_370 == 'EB') {                 
              
               if ($dias_EB_370 <= 90) {
                 $liq_rec_messer_EB_370 = 100;
               }elseif ($dias_EB_370 > 120) {
                 $liq_rec_messer_EB_370 = 0;
               }else {
                 $liq_rec_messer_EB_370 = 80;
               }
      
               $consultaPPVdor_EB_370 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_EB_370' AND anio = '$anio' AND idVendedor = 3";
               $resultadoPPVdor_EB_370 = mysqli_query($con, $consultaPPVdor_EB_370);
      
               while ($RowPPVdor_EB_370 = mysqli_fetch_array($resultadoPPVdor_EB_370)) {
      
                 $porceCumpliVdor_EB_370 = $RowPPVdor_EB_370['porcentaje'];
                           
                 if ($porceCumpliVdor_EB_370  > 100) {
                     $liq_ppto_messer_EB_370 = 0.009;
                 }elseif ($porceCumpliVdor_EB_370 >= 80) {
                     $liq_ppto_messer_EB_370 = 0.007;
                 }elseif ($porceCumpliVdor_EB_370 >= 60 && $sacaMes_EB_370 == '05' && $sacaAnio_EB_370 == '2021' || $porceCumpliVdor_EB_370 >= 60 && $sacaMes_EB_370 == '06' && $sacaAnio_EB_370 == '2021' || $porceCumpliVdor_EB_370 >= 60 && $sacaMes_EB_370 == '07' && $sacaAnio_EB_370 == '2021') {
                   $liq_ppto_messer_EB_370 = 0.0035;
                 }else {
                   $liq_ppto_messer_EB_370 = 0;
                 }
                 
                 //echo $numeRac_EB." - ".$numeFac_EB." - ".$beseComisionApro_EB." - ".$dias051_EB." - ".$nomCliente_EB." - ".$venDor_EB." - ".$porceCumpliVdor_EB." - ".$liq_ppto_messer_EB." - ".$liq_rec_messer_EB."<br>";
                
                 $comision_PPTO_EB_370 = $beseComisionApro_EB_370*$liq_ppto_messer_EB_370*($liq_rec_messer_EB_370/100);
                 $Total_comision_PPTO_EB_370 += $comision_PPTO_EB_370;
      
                 //echo $comision_PPTO_EB."<br>"; 
      
               }
              }     
                   
           }elseif ($RowConso_EB_370['porcentaje'] == 10) {
      
            $nomCliente_EB_370 = $RowConso_EB_370['cliente'];
      
       $codigo370 = $RowConso_EB_370['codigo_cliente'];
       $length370 = 9;
       $codigoV_CLI_370 = substr(str_repeat(0, $length370).$codigo370, - $length370);
      
       $numeRac_EB_370 = $RowConso_EB_370['rec'];
       $numeFac_EB_370 = $RowConso_EB_370['no_fact']; 
       $beseComisionApro_EB_370 = $RowConso_EB_370['base_com'];
       $dias_EB_370 = $RowConso_EB_370['dias_transcurridos'];
       $fecha_doc_EB_370 = $RowConso_EB_370['fecha_doc'];
       $sacaMes_EB_370 = date('m', strtotime($fecha_doc_EB_370));
       $sacaAnio_EB_370 = date('Y', strtotime($fecha_doc_EB_370));
      
       //echo $nomCliente_EB_370."<br>";
       
       $AsVddor_EB_370 = "SELECT * FROM asignacion_vendedores WHERE codigo_cliente = '$codigoV_CLI_370'";
       $resultAsVddor_EB_370 = mysqli_query($con, $AsVddor_EB_370);
      
       while ($rowVndor_EB_370 = mysqli_fetch_array($resultAsVddor_EB_370)) {
            $venDor_EB_370 = $rowVndor_EB_370['vendedor'];
        }
      
          if ($venDor_EB_370 == 'EB') {                 
              
               if ($dias_EB_370 <= 90) {
                 $liq_rec_messer_EB_370 = 100;
               }elseif ($dias_EB_370 > 120) {
                 $liq_rec_messer_EB_370 = 0;
               }else {
                 $liq_rec_messer_EB_370 = 80;
               }
      
               $consultaPPVdor_EB_370 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_EB_370' AND anio = '$anio' AND idVendedor = 3";
               $resultadoPPVdor_EB_370 = mysqli_query($con, $consultaPPVdor_EB_370);
      
               while ($RowPPVdor_EB_370 = mysqli_fetch_array($resultadoPPVdor_EB_370)) {
      
                 $porceCumpliVdor_EB_370 = $RowPPVdor_EB_370['porcentaje'];
                           
                 if ($porceCumpliVdor_EB_370  > 100) {
                     $liq_ppto_messer_EB_370 = 0.009;
                 }elseif ($porceCumpliVdor_EB_370 >= 80) {
                     $liq_ppto_messer_EB_370 = 0.007;
                 }elseif ($porceCumpliVdor_EB_370 >= 60 && $sacaMes_EB_370 == '05' && $sacaAnio_EB_370 == '2021' || $porceCumpliVdor_EB_370 >= 60 && $sacaMes_EB_370 == '06' && $sacaAnio_EB_370 == '2021' || $porceCumpliVdor_EB_370 >= 60 && $sacaMes_EB_370 == '07' && $sacaAnio_EB_370 == '2021') {
                   $liq_ppto_messer_EB_370 = 0.0035;
                 }else {
                   $liq_ppto_messer_EB_370 = 0;
                 }
                 
                 //echo $numeRac_EB." - ".$numeFac_EB." - ".$beseComisionApro_EB." - ".$dias051_EB." - ".$nomCliente_EB." - ".$venDor_EB." - ".$porceCumpliVdor_EB." - ".$liq_ppto_messer_EB." - ".$liq_rec_messer_EB."<br>";
                
                 $comision_PPTO_EB_370 = $beseComisionApro_EB_370*$liq_ppto_messer_EB_370*($liq_rec_messer_EB_370/100)/3;
                 $Total_comision_PPTO_EB_370 += $comision_PPTO_EB_370;
      
                 //echo $comision_PPTO_EB."<br>"; 
      
               }
              } 
      
         }elseif ($RowConso_EB_370['porcentaje'] == 16.5) {
      
          $nomCliente_EB_370 = $RowConso_EB_370['cliente'];
      
          $codigo370 = $RowConso_EB_370['codigo_cliente'];
          $length370 = 9;
          $codigoV_CLI_370 = substr(str_repeat(0, $length370).$codigo370, - $length370);
         
          $numeRac_EB_370 = $RowConso_EB_370['rec'];
          $numeFac_EB_370 = $RowConso_EB_370['no_fact']; 
          $beseComisionApro_EB_370 = $RowConso_EB_370['base_com'];
          $dias_EB_370 = $RowConso_EB_370['dias_transcurridos'];
          $fecha_doc_EB_370 = $RowConso_EB_370['fecha_doc'];
          $sacaMes_EB_370 = date('m', strtotime($fecha_doc_EB_370));
          $sacaAnio_EB_370 = date('Y', strtotime($fecha_doc_EB_370));
         
          //echo $nomCliente_EB_370."<br>";
          
          $AsVddor_EB_370 = "SELECT * FROM asignacion_vendedores WHERE codigo_cliente = '$codigoV_CLI_370'";
          $resultAsVddor_EB_370 = mysqli_query($con, $AsVddor_EB_370);
         
          while ($rowVndor_EB_370 = mysqli_fetch_array($resultAsVddor_EB_370)) {
               $venDor_EB_370 = $rowVndor_EB_370['vendedor'];
           }
         
             if ($venDor_EB_370 == 'EB') {                 
                 
                  if ($dias_EB_370 <= 90) {
                    $liq_rec_messer_EB_370 = 100;
                  }elseif ($dias_EB_370 > 120) {
                    $liq_rec_messer_EB_370 = 0;
                  }else {
                    $liq_rec_messer_EB_370 = 80;
                  }
         
                  $consultaPPVdor_EB_370 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_EB_370' AND anio = '$anio' AND idVendedor = 3";
                  $resultadoPPVdor_EB_370 = mysqli_query($con, $consultaPPVdor_EB_370);
         
                  while ($RowPPVdor_EB_370 = mysqli_fetch_array($resultadoPPVdor_EB_370)) {
         
                    $porceCumpliVdor_EB_370 = $RowPPVdor_EB_370['porcentaje'];
                              
                    if ($porceCumpliVdor_EB_370  > 100) {
                        $liq_ppto_messer_EB_370 = 0.009;
                    }elseif ($porceCumpliVdor_EB_370 >= 80) {
                        $liq_ppto_messer_EB_370 = 0.007;
                    }elseif ($porceCumpliVdor_EB_370 >= 60 && $sacaMes_EB_370 == '05' && $sacaAnio_EB_370 == '2021' || $porceCumpliVdor_EB_370 >= 60 && $sacaMes_EB_370 == '06' && $sacaAnio_EB_370 == '2021' || $porceCumpliVdor_EB_370 >= 60 && $sacaMes_EB_370 == '07' && $sacaAnio_EB_370 == '2021') {
                      $liq_ppto_messer_EB_370 = 0.0035;
                    }else {
                      $liq_ppto_messer_EB_370 = 0;
                    }
                    
                    //echo $numeRac_EB." - ".$numeFac_EB." - ".$beseComisionApro_EB." - ".$dias051_EB." - ".$nomCliente_EB." - ".$venDor_EB." - ".$porceCumpliVdor_EB." - ".$liq_ppto_messer_EB." - ".$liq_rec_messer_EB."<br>";
                   
                    $comision_PPTO_EB_370 = $beseComisionApro_EB_370*$liq_ppto_messer_EB_370*($liq_rec_messer_EB_370/100)/1.81;
                    $Total_comision_PPTO_EB_370 += $comision_PPTO_EB_370;
         
                    //echo $comision_PPTO_EB."<br>"; 
         
                  }
                 }
      
         }elseif ($RowConso_EB_370['porcentaje'] == 5) {
      
          $nomCliente_EB_370 = $RowConso_EB_370['cliente'];
      
       $codigo370 = $RowConso_EB_370['codigo_cliente'];
       $length370 = 9;
       $codigoV_CLI_370 = substr(str_repeat(0, $length370).$codigo370, - $length370);
      
       $numeRac_EB_370 = $RowConso_EB_370['rec'];
       $numeFac_EB_370 = $RowConso_EB_370['no_fact']; 
       $beseComisionApro_EB_370 = $RowConso_EB_370['base_com'];
       $dias_EB_370 = $RowConso_EB_370['dias_transcurridos'];
       $fecha_doc_EB_370 = $RowConso_EB_370['fecha_doc'];
       $sacaMes_EB_370 = date('m', strtotime($fecha_doc_EB_370));
       $sacaAnio_EB_370 = date('Y', strtotime($fecha_doc_EB_370));
      
       //echo $nomCliente_EB_370."<br>";
       
       $AsVddor_EB_370 = "SELECT * FROM asignacion_vendedores WHERE codigo_cliente = '$codigoV_CLI_370'";
       $resultAsVddor_EB_370 = mysqli_query($con, $AsVddor_EB_370);
      
       while ($rowVndor_EB_370 = mysqli_fetch_array($resultAsVddor_EB_370)) {
            $venDor_EB_370 = $rowVndor_EB_370['vendedor'];
        }
      
          if ($venDor_EB_370 == 'EB') {                 
              
               if ($dias_EB_370 <= 90) {
                 $liq_rec_messer_EB_370 = 100;
               }elseif ($dias_EB_370 > 120) {
                 $liq_rec_messer_EB_370 = 0;
               }else {
                 $liq_rec_messer_EB_370 = 80;
               }
      
               $consultaPPVdor_EB_370 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_EB_370' AND anio = '$anio' AND idVendedor = 3";
               $resultadoPPVdor_EB_370 = mysqli_query($con, $consultaPPVdor_EB_370);
      
               while ($RowPPVdor_EB_370 = mysqli_fetch_array($resultadoPPVdor_EB_370)) {
      
                 $porceCumpliVdor_EB_370 = $RowPPVdor_EB_370['porcentaje'];
                           
                 if ($porceCumpliVdor_EB_370  > 100) {
                     $liq_ppto_messer_EB_370 = 0.009;
                 }elseif ($porceCumpliVdor_EB_370 >= 80) {
                     $liq_ppto_messer_EB_370 = 0.007;
                 }elseif ($porceCumpliVdor_EB_370 >= 60 && $sacaMes_EB_370 == '05' && $sacaAnio_EB_370 == '2021' || $porceCumpliVdor_EB_370 >= 60 && $sacaMes_EB_370 == '06' && $sacaAnio_EB_370 == '2021' || $porceCumpliVdor_EB_370 >= 60 && $sacaMes_EB_370 == '07' && $sacaAnio_EB_370 == '2021') {
                   $liq_ppto_messer_EB_370 = 0.0035;
                 }else {
                   $liq_ppto_messer_EB_370 = 0;
                 }
                 
                 //echo $numeRac_EB." - ".$numeFac_EB." - ".$beseComisionApro_EB." - ".$dias051_EB." - ".$nomCliente_EB." - ".$venDor_EB." - ".$porceCumpliVdor_EB." - ".$liq_ppto_messer_EB." - ".$liq_rec_messer_EB."<br>";
                
                 $comision_PPTO_EB_370 = $beseComisionApro_EB_370*$liq_ppto_messer_EB_370*($liq_rec_messer_EB_370/100)/6;
                 $Total_comision_PPTO_EB_370 += $comision_PPTO_EB_370;
      
                 //echo $comision_PPTO_EB."<br>"; 
      
               }
              }
      
         }elseif ($RowConso_EB_370['porcentaje'] == 20) {
           
          $nomCliente_EB_370 = $RowConso_EB_370['cliente'];
      
       $codigo370 = $RowConso_EB_370['codigo_cliente'];
       $length370 = 9;
       $codigoV_CLI_370 = substr(str_repeat(0, $length370).$codigo370, - $length370);
      
       $numeRac_EB_370 = $RowConso_EB_370['rec'];
       $numeFac_EB_370 = $RowConso_EB_370['no_fact']; 
       $beseComisionApro_EB_370 = $RowConso_EB_370['base_com'];
       $dias_EB_370 = $RowConso_EB_370['dias_transcurridos'];
       $fecha_doc_EB_370 = $RowConso_EB_370['fecha_doc'];
       $sacaMes_EB_370 = date('m', strtotime($fecha_doc_EB_370));
       $sacaAnio_EB_370 = date('Y', strtotime($fecha_doc_EB_370));
      
       //echo $nomCliente_EB_370."<br>";
       
       $AsVddor_EB_370 = "SELECT * FROM asignacion_vendedores WHERE codigo_cliente = '$codigoV_CLI_370'";
       $resultAsVddor_EB_370 = mysqli_query($con, $AsVddor_EB_370);
      
       while ($rowVndor_EB_370 = mysqli_fetch_array($resultAsVddor_EB_370)) {
            $venDor_EB_370 = $rowVndor_EB_370['vendedor'];
        }
      
          if ($venDor_EB_370 == 'EB') {                 
              
               if ($dias_EB_370 <= 90) {
                 $liq_rec_messer_EB_370 = 100;
               }elseif ($dias_EB_370 > 120) {
                 $liq_rec_messer_EB_370 = 0;
               }else {
                 $liq_rec_messer_EB_370 = 80;
               }
      
               $consultaPPVdor_EB_370 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_EB_370' AND anio = '$anio' AND idVendedor = 3";
               $resultadoPPVdor_EB_370 = mysqli_query($con, $consultaPPVdor_EB_370);
      
               while ($RowPPVdor_EB_370 = mysqli_fetch_array($resultadoPPVdor_EB_370)) {
      
                 $porceCumpliVdor_EB_370 = $RowPPVdor_EB_370['porcentaje'];
                           
                 if ($porceCumpliVdor_EB_370  > 100) {
                     $liq_ppto_messer_EB_370 = 0.009;
                 }elseif ($porceCumpliVdor_EB_370 >= 80) {
                     $liq_ppto_messer_EB_370 = 0.007;
                 }elseif ($porceCumpliVdor_EB_370 >= 60 && $sacaMes_EB_370 == '05' && $sacaAnio_EB_370 == '2021' || $porceCumpliVdor_EB_370 >= 60 && $sacaMes_EB_370 == '06' && $sacaAnio_EB_370 == '2021' || $porceCumpliVdor_EB_370 >= 60 && $sacaMes_EB_370 == '07' && $sacaAnio_EB_370 == '2021') {
                   $liq_ppto_messer_EB_370 = 0.0035;
                 }else {
                   $liq_ppto_messer_EB_370 = 0;
                 }
                 
                 //echo $numeRac_EB." - ".$numeFac_EB." - ".$beseComisionApro_EB." - ".$dias051_EB." - ".$nomCliente_EB." - ".$venDor_EB." - ".$porceCumpliVdor_EB." - ".$liq_ppto_messer_EB." - ".$liq_rec_messer_EB."<br>";
                
                 $comision_PPTO_EB_370 = $beseComisionApro_EB_370*$liq_ppto_messer_EB_370*($liq_rec_messer_EB_370/100)/1.5;
                 $Total_comision_PPTO_EB_370 += $comision_PPTO_EB_370;
      
                 //echo $comision_PPTO_EB."<br>"; 
      
               }
              }
       }        
      }
      //echo $Total_comision_PPTO_EB_370;
      //TERMINA CALCULO COMISIONES MESSER MANIZALES................................................................................................................................................................................................
      
      //INICIA CALCULO COMISIONES MESSER ITAGUI................................................................................................................................................................................................
       
      $consultaConso_EB_590 = "SELECT * FROM comisiones_itagui WHERE mes = '$mesC'";
      $resultadoConso_EB_590 = mysqli_query($con, $consultaConso_EB_590);
      
      while ($RowConso_EB_590 = mysqli_fetch_array($resultadoConso_EB_590)) {       
      if ($RowConso_EB_590['dias'] != '' && $RowConso_EB_590['porc_3'] != '' && $RowConso_EB_590['porc_4'] != '') {
       $nomCliente_EB_590 = $RowConso_EB_590['cliente'];
      
       $codigo590 = $RowConso_EB_590['codigo'];
       $length590 = 9;
       $codigoV_CLI_590 = substr(str_repeat(0, $length590).$codigo590, - $length590);
      
       $numeRac_EB_590 = $RowConso_EB_590['recibo'];
       $numeFac_EB_590 = $RowConso_EB_590['numero_']; 
       $beseComisionApro_EB_590 = $RowConso_EB_590['base_com'];
       $dias_EB_590 = $RowConso_EB_590['dias'];
       $fecha_doc_EB_590 = $RowConso_EB_590['fecha_doc'];
       $sacaMes_EB_590 = date('m', strtotime($fecha_doc_EB_590));
       $sacaAnio_EB_590 = date('Y', strtotime($fecha_doc_EB_590));
      
       //echo $codigoV_CLI_590."<br>";
       
       $AsVddor_EB_590 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_590'";
       $resultAsVddor_EB_590 = mysqli_query($con, $AsVddor_EB_590);
      
       while ($rowVndor_EB_590 = mysqli_fetch_array($resultAsVddor_EB_590)) {
            $venDor_EB_590 = $rowVndor_EB_590['vendedor'];
            //echo $nomCliente_EB_590." - ".$venDor_EB_590."<br>";
        }
          if ($venDor_EB_590 == 'EB') {                 
              
               if ($dias_EB_590 <= 90) {
                 $liq_rec_messer_EB_590 = 100;
               }elseif ($dias_EB_590 > 120) {
                 $liq_rec_messer_EB_590 = 0;
               }else {
                 $liq_rec_messer_EB_590 = 80;
               }
      
               $consultaPPVdor_EB_590 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_EB_590' AND anio = '$anio' AND idVendedor = 3";
               $resultadoPPVdor_EB_590 = mysqli_query($con, $consultaPPVdor_EB_590);
      
               while ($RowPPVdor_EB_590 = mysqli_fetch_array($resultadoPPVdor_EB_590)) {
      
                 $porceCumpliVdor_EB_590 = $RowPPVdor_EB_590['porcentaje'];
                           
                 if ($porceCumpliVdor_EB_590  > 100) {
                     $liq_ppto_messer_EB_590 = 0.009;
                 }elseif ($porceCumpliVdor_EB_590 >= 80) {
                     $liq_ppto_messer_EB_590 = 0.007;
                 }elseif ($porceCumpliVdor_EB_590 >= 60 && $sacaAnio_EB_590 == '2021' && $sacaMes_EB_590 == '05' || $porceCumpliVdor_EB_590 >= 60 && $sacaAnio_EB_590 == '2021' && $sacaMes_EB_590 == '06' || $porceCumpliVdor_EB_590 >= 60 && $sacaAnio_EB_590 == '2021' && $sacaMes_EB_590 == '07') {
                   $liq_ppto_messer_EB_590 = 0.0035;
                 }else {
                   $liq_ppto_messer_EB_590 = 0;
                 }
                 
                 //echo $numeRac_EB." - ".$numeFac_EB." - ".$beseComisionApro_EB." - ".$dias051_EB." - ".$nomCliente_EB." - ".$venDor_EB." - ".$porceCumpliVdor_EB." - ".$liq_ppto_messer_EB." - ".$liq_rec_messer_EB."<br>";
                
                 $comision_PPTO_EB_590 = $beseComisionApro_EB_590*$liq_ppto_messer_EB_590*($liq_rec_messer_EB_590/100);
                 $Total_comision_PPTO_EB_590 += $comision_PPTO_EB_590;
      
                 //echo $comision_PPTO_EB."<br>"; 
      
               }
              }          
           }elseif ($RowConso_EB_590['porcentaje'] == 10) {
      
            $nomCliente_EB_590 = $RowConso_EB_590['cliente'];
      
            $codigo590 = $RowConso_EB_590['codigo'];
            $length590 = 9;
            $codigoV_CLI_590 = substr(str_repeat(0, $length590).$codigo590, - $length590);
           
            $numeRac_EB_590 = $RowConso_EB_590['recibo'];
            $numeFac_EB_590 = $RowConso_EB_590['numero_']; 
            $beseComisionApro_EB_590 = $RowConso_EB_590['base_com'];
            $dias_EB_590 = $RowConso_EB_590['dias'];
            $fecha_doc_EB_590 = $RowConso_EB_590['fecha_doc'];
            $sacaMes_EB_590 = date('m', strtotime($fecha_doc_EB_590));
            $sacaAnio_EB_590 = date('Y', strtotime($fecha_doc_EB_590));
           
            //echo $nomCliente_EB_590."<br>";
            
            $AsVddor_EB_590 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_590'";
            $resultAsVddor_EB_590 = mysqli_query($con, $AsVddor_EB_590);
           
            while ($rowVndor_EB_590 = mysqli_fetch_array($resultAsVddor_EB_590)) {
                 $venDor_EB_590 = $rowVndor_EB_590['vendedor'];
             }
               if ($venDor_EB_590 == 'EB') {                 
                   
                    if ($dias_EB_590 <= 90) {
                      $liq_rec_messer_EB_590 = 100;
                    }elseif ($dias_EB_590 > 120) {
                      $liq_rec_messer_EB_590 = 0;
                    }else {
                      $liq_rec_messer_EB_590 = 80;
                    }
           
                    $consultaPPVdor_EB_590 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_EB_590' AND anio = '$anio' AND idVendedor = 3";
                    $resultadoPPVdor_EB_590 = mysqli_query($con, $consultaPPVdor_EB_590);
           
                    while ($RowPPVdor_EB_590 = mysqli_fetch_array($resultadoPPVdor_EB_590)) {
           
                      $porceCumpliVdor_EB_590 = $RowPPVdor_EB_590['porcentaje'];
                                
                      if ($porceCumpliVdor_EB_590  > 100) {
                          $liq_ppto_messer_EB_590 = 0.009;
                      }elseif ($porceCumpliVdor_EB_590 >= 80) {
                          $liq_ppto_messer_EB_590 = 0.007;
                      }elseif ($porceCumpliVdor_EB_590 >= 60 && $sacaAnio_EB_590 == '2021' && $sacaMes_EB_590 == '05' || $porceCumpliVdor_EB_590 >= 60 && $sacaAnio_EB_590 == '2021' && $sacaMes_EB_590 == '06' || $porceCumpliVdor_EB_590 >= 60 && $sacaAnio_EB_590 == '2021' && $sacaMes_EB_590 == '07') {
                        $liq_ppto_messer_EB_590 = 0.0035;
                      }else {
                        $liq_ppto_messer_EB_590 = 0;
                      }
                      
                      //echo $numeRac_EB." - ".$numeFac_EB." - ".$beseComisionApro_EB." - ".$dias051_EB." - ".$nomCliente_EB." - ".$venDor_EB." - ".$porceCumpliVdor_EB." - ".$liq_ppto_messer_EB." - ".$liq_rec_messer_EB."<br>";
                     
                      $comision_PPTO_EB_590 = $beseComisionApro_EB_590*$liq_ppto_messer_EB_590*($liq_rec_messer_EB_590/100)/3;
                      $Total_comision_PPTO_EB_590 += $comision_PPTO_EB_590;
           
                      //echo $comision_PPTO_EB."<br>"; 
           
                    }
                   }
      
         }elseif ($RowConso_EB_590['porcentaje'] == 16.5) {
      
          $nomCliente_EB_590 = $RowConso_EB_590['cliente'];
      
          $codigo590 = $RowConso_EB_590['codigo'];
          $length590 = 9;
          $codigoV_CLI_590 = substr(str_repeat(0, $length590).$codigo590, - $length590);
         
          $numeRac_EB_590 = $RowConso_EB_590['recibo'];
          $numeFac_EB_590 = $RowConso_EB_590['numero_']; 
          $beseComisionApro_EB_590 = $RowConso_EB_590['base_com'];
          $dias_EB_590 = $RowConso_EB_590['dias'];
          $fecha_doc_EB_590 = $RowConso_EB_590['fecha_doc'];
          $sacaMes_EB_590 = date('m', strtotime($fecha_doc_EB_590));
          $sacaAnio_EB_590 = date('Y', strtotime($fecha_doc_EB_590));
         
          //echo $nomCliente_EB_590."<br>";
          
          $AsVddor_EB_590 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_590'";
          $resultAsVddor_EB_590 = mysqli_query($con, $AsVddor_EB_590);
         
          while ($rowVndor_EB_590 = mysqli_fetch_array($resultAsVddor_EB_590)) {
               $venDor_EB_590 = $rowVndor_EB_590['vendedor'];
           }
             if ($venDor_EB_590 == 'EB') {                 
                 
                  if ($dias_EB_590 <= 90) {
                    $liq_rec_messer_EB_590 = 100;
                  }elseif ($dias_EB_590 > 120) {
                    $liq_rec_messer_EB_590 = 0;
                  }else {
                    $liq_rec_messer_EB_590 = 80;
                  }
         
                  $consultaPPVdor_EB_590 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_EB_590' AND anio = '$anio' AND idVendedor = 3";
                  $resultadoPPVdor_EB_590 = mysqli_query($con, $consultaPPVdor_EB_590);
         
                  while ($RowPPVdor_EB_590 = mysqli_fetch_array($resultadoPPVdor_EB_590)) {
         
                    $porceCumpliVdor_EB_590 = $RowPPVdor_EB_590['porcentaje'];
                              
                    if ($porceCumpliVdor_EB_590  > 100) {
                        $liq_ppto_messer_EB_590 = 0.009;
                    }elseif ($porceCumpliVdor_EB_590 >= 80) {
                        $liq_ppto_messer_EB_590 = 0.007;
                    }elseif ($porceCumpliVdor_EB_590 >= 60 && $sacaAnio_EB_590 == '2021' && $sacaMes_EB_590 == '05' || $porceCumpliVdor_EB_590 >= 60 && $sacaAnio_EB_590 == '2021' && $sacaMes_EB_590 == '06' || $porceCumpliVdor_EB_590 >= 60 && $sacaAnio_EB_590 == '2021' && $sacaMes_EB_590 == '07') {
                      $liq_ppto_messer_EB_590 = 0.0035;
                    }else {
                      $liq_ppto_messer_EB_590 = 0;
                    }
                    
                    //echo $numeRac_EB." - ".$numeFac_EB." - ".$beseComisionApro_EB." - ".$dias051_EB." - ".$nomCliente_EB." - ".$venDor_EB." - ".$porceCumpliVdor_EB." - ".$liq_ppto_messer_EB." - ".$liq_rec_messer_EB."<br>";
                   
                    $comision_PPTO_EB_590 = $beseComisionApro_EB_590*$liq_ppto_messer_EB_590*($liq_rec_messer_EB_590/100)/1.81;
                    $Total_comision_PPTO_EB_590 += $comision_PPTO_EB_590;
         
                    //echo $comision_PPTO_EB."<br>"; 
         
                  }
                 }
      
         }elseif ($RowConso_EB_590['porcentaje'] == 5) {
      
          $nomCliente_EB_590 = $RowConso_EB_590['cliente'];
      
          $codigo590 = $RowConso_EB_590['codigo'];
          $length590 = 9;
          $codigoV_CLI_590 = substr(str_repeat(0, $length590).$codigo590, - $length590);
         
          $numeRac_EB_590 = $RowConso_EB_590['recibo'];
          $numeFac_EB_590 = $RowConso_EB_590['numero_']; 
          $beseComisionApro_EB_590 = $RowConso_EB_590['base_com'];
          $dias_EB_590 = $RowConso_EB_590['dias'];
          $fecha_doc_EB_590 = $RowConso_EB_590['fecha_doc'];
          $sacaMes_EB_590 = date('m', strtotime($fecha_doc_EB_590));
          $sacaAnio_EB_590 = date('Y', strtotime($fecha_doc_EB_590));
         
          //echo $nomCliente_EB_590."<br>";
          
          $AsVddor_EB_590 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_590'";
          $resultAsVddor_EB_590 = mysqli_query($con, $AsVddor_EB_590);
         
          while ($rowVndor_EB_590 = mysqli_fetch_array($resultAsVddor_EB_590)) {
               $venDor_EB_590 = $rowVndor_EB_590['vendedor'];
           }
             if ($venDor_EB_590 == 'EB') {                 
                 
                  if ($dias_EB_590 <= 90) {
                    $liq_rec_messer_EB_590 = 100;
                  }elseif ($dias_EB_590 > 120) {
                    $liq_rec_messer_EB_590 = 0;
                  }else {
                    $liq_rec_messer_EB_590 = 80;
                  }
         
                  $consultaPPVdor_EB_590 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_EB_590' AND anio = '$anio' AND idVendedor = 3";
                  $resultadoPPVdor_EB_590 = mysqli_query($con, $consultaPPVdor_EB_590);
         
                  while ($RowPPVdor_EB_590 = mysqli_fetch_array($resultadoPPVdor_EB_590)) {
         
                    $porceCumpliVdor_EB_590 = $RowPPVdor_EB_590['porcentaje'];
                              
                    if ($porceCumpliVdor_EB_590  > 100) {
                        $liq_ppto_messer_EB_590 = 0.009;
                    }elseif ($porceCumpliVdor_EB_590 >= 80) {
                        $liq_ppto_messer_EB_590 = 0.007;
                    }elseif ($porceCumpliVdor_EB_590 >= 60 && $sacaAnio_EB_590 == '2021' && $sacaMes_EB_590 == '05' || $porceCumpliVdor_EB_590 >= 60 && $sacaAnio_EB_590 == '2021' && $sacaMes_EB_590 == '06' || $porceCumpliVdor_EB_590 >= 60 && $sacaAnio_EB_590 == '2021' && $sacaMes_EB_590 == '07') {
                      $liq_ppto_messer_EB_590 = 0.0035;
                    }else {
                      $liq_ppto_messer_EB_590 = 0;
                    }
                    
                    //echo $numeRac_EB." - ".$numeFac_EB." - ".$beseComisionApro_EB." - ".$dias051_EB." - ".$nomCliente_EB." - ".$venDor_EB." - ".$porceCumpliVdor_EB." - ".$liq_ppto_messer_EB." - ".$liq_rec_messer_EB."<br>";
                   
                    $comision_PPTO_EB_590 = $beseComisionApro_EB_590*$liq_ppto_messer_EB_590*($liq_rec_messer_EB_590/100)/6;
                    $Total_comision_PPTO_EB_590 += $comision_PPTO_EB_590;
         
                    //echo $comision_PPTO_EB."<br>"; 
         
                  }
                 }
      
         }elseif ($RowConso_EB_590['porcentaje'] == 20) {
           
          $nomCliente_EB_590 = $RowConso_EB_590['cliente'];
      
          $codigo590 = $RowConso_EB_590['codigo'];
          $length590 = 9;
          $codigoV_CLI_590 = substr(str_repeat(0, $length590).$codigo590, - $length590);
         
          $numeRac_EB_590 = $RowConso_EB_590['recibo'];
          $numeFac_EB_590 = $RowConso_EB_590['numero_']; 
          $beseComisionApro_EB_590 = $RowConso_EB_590['base_com'];
          $dias_EB_590 = $RowConso_EB_590['dias'];
          $fecha_doc_EB_590 = $RowConso_EB_590['fecha_doc'];
          $sacaMes_EB_590 = date('m', strtotime($fecha_doc_EB_590));
          $sacaAnio_EB_590 = date('Y', strtotime($fecha_doc_EB_590));
         
          //echo $nomCliente_EB_590."<br>";
          
          $AsVddor_EB_590 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_590'";
          $resultAsVddor_EB_590 = mysqli_query($con, $AsVddor_EB_590);
         
          while ($rowVndor_EB_590 = mysqli_fetch_array($resultAsVddor_EB_590)) {
               $venDor_EB_590 = $rowVndor_EB_590['vendedor'];
           }
             if ($venDor_EB_590 == 'EB') {                 
                 
                  if ($dias_EB_590 <= 90) {
                    $liq_rec_messer_EB_590 = 100;
                  }elseif ($dias_EB_590 > 120) {
                    $liq_rec_messer_EB_590 = 0;
                  }else {
                    $liq_rec_messer_EB_590 = 80;
                  }
         
                  $consultaPPVdor_EB_590 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_EB_590' AND anio = '$anio' AND idVendedor = 3";
                  $resultadoPPVdor_EB_590 = mysqli_query($con, $consultaPPVdor_EB_590);
         
                  while ($RowPPVdor_EB_590 = mysqli_fetch_array($resultadoPPVdor_EB_590)) {
         
                    $porceCumpliVdor_EB_590 = $RowPPVdor_EB_590['porcentaje'];
                              
                    if ($porceCumpliVdor_EB_590  > 100) {
                        $liq_ppto_messer_EB_590 = 0.009;
                    }elseif ($porceCumpliVdor_EB_590 >= 80) {
                        $liq_ppto_messer_EB_590 = 0.007;
                    }elseif ($porceCumpliVdor_EB_590 >= 60 && $sacaAnio_EB_590 == '2021' && $sacaMes_EB_590 == '05' || $porceCumpliVdor_EB_590 >= 60 && $sacaAnio_EB_590 == '2021' && $sacaMes_EB_590 == '06' || $porceCumpliVdor_EB_590 >= 60 && $sacaAnio_EB_590 == '2021' && $sacaMes_EB_590 == '07') {
                      $liq_ppto_messer_EB_590 = 0.0035;
                    }else {
                      $liq_ppto_messer_EB_590 = 0;
                    }
                    
                    //echo $numeRac_EB." - ".$numeFac_EB." - ".$beseComisionApro_EB." - ".$dias051_EB." - ".$nomCliente_EB." - ".$venDor_EB." - ".$porceCumpliVdor_EB." - ".$liq_ppto_messer_EB." - ".$liq_rec_messer_EB."<br>";
                   
                    $comision_PPTO_EB_590 = $beseComisionApro_EB_590*$liq_ppto_messer_EB_590*($liq_rec_messer_EB_590/100)/1.5;
                    $Total_comision_PPTO_EB_590 += $comision_PPTO_EB_590;
         
                    //echo $comision_PPTO_EB."<br>"; 
         
                  }
                }
          }        
      }
      //echo $Total_comision_PPTO_EB_590;
      //TERMINA CALCULO COMISIONES MESSER ITAGUI................................................................................................................................................................................................
      
      //Suma toberin para EB
      $comisionesTobe_EB = $Total_comision_PPTO_EB;
      
      //Suma cazuca para EB
      $comisionesCazu_EB = $Total_comision_PPTO_EB_060;
      
      //Suma Manizales para EB
      $comisionesManiz_EB = $Total_comision_PPTO_EB_370;
      
      //Suma Itagui para EB
      $comisionesItag_EB = $Total_comision_PPTO_EB_590;
      
      
      $total_comision_rec_x_messer_EB = $comisionesTobe_EB+$comisionesCazu_EB+$comisionesManiz_EB+$comisionesItag_EB;

      $total_comsion_EB = $total_comision_rec_x_messer_EB + $porcentajeRecaudo_EB;

      $objPHPExcel->getActiveSheet()->setCellValue('C6', $suma_EB_VT);
      $objPHPExcel->getActiveSheet()->setCellValue('D6', $suma_EB_VS);
      $objPHPExcel->getActiveSheet()->setCellValue('E6', $suma_EB_VM);
      $objPHPExcel->getActiveSheet()->setCellValue('F6', $suma_EB_VITA);
      $objPHPExcel->getActiveSheet()->setCellValue('G6', $subtotalEB);
      $objPHPExcel->getActiveSheet()->setCellValue('H6', $total_NB_NC);
      $objPHPExcel->getActiveSheet()->setCellValue('I6', $total_vta_EB);
      $objPHPExcel->getActiveSheet()->setCellValue('J6', $porcentaje_Cumpli_EB."%");
      $objPHPExcel->getActiveSheet()->setCellValue('K6', $total_comision_rec_x_messer_EB);
      $objPHPExcel->getActiveSheet()->setCellValue('M6', $ventas_ingegas_vendedor_EB);
      $objPHPExcel->getActiveSheet()->setCellValue('N6', $porcentajeRecaudo_EB);
      $objPHPExcel->getActiveSheet()->setCellValue('O6', $total_comsion_EB);     

    }elseif ($id_vendedor == 4) {

    //INICIA CALCULO VENTAS JJ TOBERIN.........................................................................................................

      $consulta_JJ = "SELECT * FROM ventas_producto WHERE mes = '$mesC' AND anio = '$anio'";
      $resultado_JJ = mysqli_query($con, $consulta_JJ);

      while($linea_JJ = mysqli_fetch_array($resultado_JJ)){
        $promotor_retail = $linea_JJ["promotor_retail"];
        $valor_total = $linea_JJ["valor_total"];
        $tipo_doc = $linea_JJ["tipo_doc"];
        $numero_legal = $linea_JJ["numero_legal"];
        $cliente = $linea_JJ["cliente"];
        $sucursal = $linea_JJ['sucursal'];        

        //echo $cliente."<br>";

        $consulta1_JJ = "SELECT * FROM asignacion_vendedores WHERE codigo = '$cliente'";
        $resultado1_JJ = mysqli_query($con, $consulta1_JJ);
        while ($linea1_JJ = mysqli_fetch_array($resultado1_JJ)) {
          $vendedor = $linea1_JJ["vendedor"];          
          //echo $cliente." - ".$vendedor." - ".$sucursal."<br>";           
        }
        if ($sucursal == '051' && $vendedor == 'JJ') {
          $suma_JJ_VT += $valor_total;
        }

        if ($sucursal == '060' && $vendedor == 'JJ') {
          $suma_JJ_VS += $valor_total;
        }

        if ($sucursal == '370' && $vendedor == 'JJ') {
          $suma_JJ_VM += $valor_total;
        }

        if ($sucursal == '590' && $vendedor == 'JJ') {
          $suma_JJ_VITA += $valor_total;
        }
   
      }

      if($suma_JJ_VT == 0){
        $suma_JJ_VT = "-";
      }

      if($suma_JJ_VS == 0){
        $suma_JJ_VS = "-";
      }

      if($suma_JJ_VM == 0){
        $suma_JJ_VM = "-";
      }

      if($suma_JJ_VITA == 0){
        $suma_JJ_VITA = "-";
      }       
          
      //echo $suma_JJ_VT."<br>".$suma_JJ_VS."<br>".$suma_JJ_VM."<br>".$suma_JJ_VITA; 
      $subtotalJJ = $suma_JJ_VT+$suma_JJ_VS+$suma_JJ_VM+$suma_JJ_VITA;

      //VERIFICA VENTAS INGEGAS
      $consultaVJJ = "SELECT * FROM vt_ing WHERE tercero_interno = '$nombre_vendedor'";
      $resultadoVJJ = mysqli_query($con, $consultaVJJ);

      while($lineaVJJ = mysqli_fetch_array($resultadoVJJ)){
        $cantidadJJ = $lineaVJJ["cantidad"];
        $valor_unitarioJJ = $lineaVJJ["valor_unitario"];

        $valor_ventasJJ = $cantidadJJ * $valor_unitarioJJ;

        $ventas_ingegas_vendedor_JJ += $valor_ventasJJ;
      }
      
      $total_vta_JJ = ($subtotalJJ - $total_JJ_NC) + $ventas_ingegas_vendedor_JJ;

      $porcentaje_Cumpli_JJ =  round(($total_vta_JJ/$valorPP)*100);

      //REGISTRAR PORCENTAJE........................................................................................................................................
        $existePPCUM_JJ = "SELECT * FROM porce_cumpli_vendedor WHERE idVendedor = 4 AND mes LIKE '%$mesC%' AND anio = '$anio'";
        $resultExistePPCUM_JJ = mysqli_query($con, $existePPCUM_JJ);

          if (mysqli_num_rows($resultExistePPCUM_JJ) <= 0) {
              
              $mes_0_JJ = sprintf("%02d", $mesC);
              $registraPPCUM_JJ = "INSERT INTO porce_cumpli_vendedor (idVendedor, mes, anio, porcentaje) VALUES (4,'$mes_0_JJ','$anio','$porcentaje_Cumpli_JJ')";
              $ressultRegistroPPCUM_JJ = mysqli_query($con, $registraPPCUM_JJ);

          }

    //INICIA CALCULO COMISIONES MESSER TOBERIN................................................................................................................................................................................................
  $consultaConso_JJ = "SELECT * FROM comisiones_toberin WHERE mes = '$mesC'";
  $resultadoConso_JJ = mysqli_query($con, $consultaConso_JJ);

  while ($RowConso_JJ = mysqli_fetch_array($resultadoConso_JJ)) {       
   if ($RowConso_JJ['dias'] != '' && $RowConso_JJ['porc_3'] != '' && $RowConso_JJ['porc_4'] != '') {
       $nomCliente_JJ = $RowConso_JJ['cliente'];

       $codigo051_JJ = $RowConso_JJ['codigo'];
       $length051_JJ = 9;
       $codigoV_CLI_051_JJ = substr(str_repeat(0, $length051_JJ).$codigo051_JJ, - $length051_JJ);

       $numeRac_JJ = $RowConso_JJ['rec'];
       $numeFac_JJ = $RowConso_JJ['numero_']; 
       $beseComisionApro_JJ = $RowConso_JJ['base_trans'];
       $dias_JJ = $RowConso_JJ['dias'];
       $fecha_doc_JJ = $RowConso_JJ['fecha_doc'];
       $sacaMes_JJ = date('m', strtotime($fecha_doc_JJ));
       $sacaAnio_JJ = date('Y', strtotime($fecha_doc_JJ));

       //echo $codigoV_CLI_051_JJ."<br>";
       
       $AsVddor_JJ = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_051_JJ'";
       $resultAsVddor_JJ = mysqli_query($con, $AsVddor_JJ);

       while ($rowVndor_JJ = mysqli_fetch_array($resultAsVddor_JJ)) {
           $venDor_JJ = $rowVndor_JJ['vendedor'];  
       }
          if ($venDor_JJ == 'JJ') {                  
               
               if ($dias_JJ <= 90) {
                 $liq_rec_messer_JJ = 100;
               }elseif ($dias_JJ > 120) {
                 $liq_rec_messer_JJ = 0;
               }else {
                 $liq_rec_messer_JJ = 80;
               }

               $consultaPPVdor_JJ = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_JJ' AND anio = '$anio' AND idVendedor = 4";
               $resultadoPPVdor_JJ = mysqli_query($con, $consultaPPVdor_JJ);
     
               while ($RowPPVdor_JJ = mysqli_fetch_array($resultadoPPVdor_JJ)) {
     
                 $porceCumpliVdor_JJ = $RowPPVdor_JJ['porcentaje'];
                           
                 if ($porceCumpliVdor_JJ  > 100) {
                     $liq_ppto_messer_JJ = 0.009;
                 }elseif ($porceCumpliVdor_JJ >= 80) {
                     $liq_ppto_messer_JJ = 0.007;
                 }elseif ($porceCumpliVdor_JJ <= 79 && $sacaMes_JJ == '05' && $sacaAnio_JJ == '2021' || $porceCumpliVdor_JJ <= 79 && $sacaMes_JJ == '06' && $sacaAnio_JJ == '2021' || $porceCumpliVdor_JJ <= 79 && $sacaMes_JJ == '07' && $sacaAnio_JJ == '2021') {
                   $liq_ppto_messer_JJ = 0.0035;
                 }else {
                   $liq_ppto_messer_JJ = 0;
                 }

                 //echo $numeRac_JJ." - ".$numeFac_JJ." - ".$beseComisionApro_JJ." - ".$dias_JJ." - ".$nomCliente_JJ." - ".$venDor_JJ." - ".$porceCumpliVdor_JJ." - ".$liq_ppto_messer_JJ." - ".$liq_rec_messer_JJ."<br>";
                
                 $comision_PPTO_JJ = $beseComisionApro_JJ*$liq_ppto_messer_JJ*($liq_rec_messer_JJ/100);
                 $Total_comision_PPTO_JJ += $comision_PPTO_JJ;

                 //echo $comision_PPTO_JJ."<br>";

               } 
             }    
           }elseif ($RowConso_JJ['porcentaje'] == 10) {

             $nomCliente_JJ = $RowConso_JJ['cliente'];

             $codigo051_JJ = $RowConso_JJ['codigo'];
             $length051_JJ = 9;
             $codigoV_CLI_051_JJ = substr(str_repeat(0, $length051_JJ).$codigo051_JJ, - $length051_JJ);
   
             $numeRac_JJ = $RowConso_JJ['rec'];
             $numeFac_JJ = $RowConso_JJ['numero_']; 
             $beseComisionApro_JJ = $RowConso_JJ['base_trans'];
             $dias_JJ = $RowConso_JJ['dias'];
             $fecha_doc_JJ = $RowConso_JJ['fecha_doc'];
             $sacaMes_JJ = date('m', strtotime($fecha_doc_JJ));
             $sacaAnio_JJ = date('Y', strtotime($fecha_doc_JJ));
   
             //echo $nomCliente_JJ."<br>";
             
             $AsVddor_JJ = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_051_JJ'";
             $resultAsVddor_JJ = mysqli_query($con, $AsVddor_JJ);
   
             while ($rowVndor_JJ = mysqli_fetch_array($resultAsVddor_JJ)) {
                 $venDor_JJ = $rowVndor_JJ['vendedor'];
             }
                if ($venDor_JJ == 'JJ') {                  
                     
                     if ($dias_JJ <= 90) {
                       $liq_rec_messer_JJ = 100;
                     }elseif ($dias_JJ > 120) {
                       $liq_rec_messer_JJ = 0;
                     }else {
                       $liq_rec_messer_JJ = 80;
                     }
   
                     $consultaPPVdor_JJ = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_JJ' AND anio = '$anio' AND idVendedor = 4";
                     $resultadoPPVdor_JJ = mysqli_query($con, $consultaPPVdor_JJ);
           
                     while ($RowPPVdor_JJ = mysqli_fetch_array($resultadoPPVdor_JJ)) {
           
                       $porceCumpliVdor_JJ = $RowPPVdor_JJ['porcentaje'];
                                 
                       if ($porceCumpliVdor_JJ  > 100) {
                           $liq_ppto_messer_JJ = 0.009;
                       }elseif ($porceCumpliVdor_JJ >= 80) {
                           $liq_ppto_messer_JJ = 0.007;
                       }elseif ($porceCumpliVdor_JJ <= 79 && $sacaMes_JJ == '05' && $sacaAnio_JJ == '2021' || $porceCumpliVdor_JJ <= 79 && $sacaMes_JJ == '06' && $sacaAnio_JJ == '2021' || $porceCumpliVdor_JJ <= 79 && $sacaMes_JJ == '07' && $sacaAnio_JJ == '2021') {
                         $liq_ppto_messer_JJ = 0.0035;
                       }else {
                         $liq_ppto_messer_JJ = 0;
                       }
   
                       //echo $numeRac_JJ." - ".$numeFac_JJ." - ".$beseComisionApro_JJ." - ".$dias_JJ." - ".$nomCliente_JJ." - ".$venDor_JJ." - ".$porceCumpliVdor_JJ." - ".$liq_ppto_messer_JJ." - ".$liq_rec_messer_JJ."<br>";
                      
                       $comision_PPTO_JJ = $beseComisionApro_JJ*$liq_ppto_messer_JJ*($liq_rec_messer_JJ/100)/3;
                       $Total_comision_PPTO_JJ += $comision_PPTO_JJ;
   
                       //echo $comision_PPTO_JJ."<br>";
      
                     } 
                   }  

         }elseif ($RowConso_JJ['porcentaje'] == 16.5) {

           $nomCliente_JJ = $RowConso_JJ['cliente'];

           $codigo051_JJ = $RowConso_JJ['codigo'];
           $length051_JJ = 9;
           $codigoV_CLI_051_JJ = substr(str_repeat(0, $length051_JJ).$codigo051_JJ, - $length051_JJ);
 
           $numeRac_JJ = $RowConso_JJ['rec'];
           $numeFac_JJ = $RowConso_JJ['numero_']; 
           $beseComisionApro_JJ = $RowConso_JJ['base_trans'];
           $dias_JJ = $RowConso_JJ['dias'];
           $fecha_doc_JJ = $RowConso_JJ['fecha_doc'];
           $sacaMes_JJ = date('m', strtotime($fecha_doc_JJ));
           $sacaAnio_JJ = date('Y', strtotime($fecha_doc_JJ));
 
           //echo $nomCliente_JJ."<br>";
           
           $AsVddor_JJ = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_051_JJ'";
           $resultAsVddor_JJ = mysqli_query($con, $AsVddor_JJ);
 
           while ($rowVndor_JJ = mysqli_fetch_array($resultAsVddor_JJ)) {
               $venDor_JJ = $rowVndor_JJ['vendedor'];
           }
              if ($venDor_JJ == 'JJ') {                  
                   
                   if ($dias_JJ <= 90) {
                     $liq_rec_messer_JJ = 100;
                   }elseif ($dias_JJ > 120) {
                     $liq_rec_messer_JJ = 0;
                   }else {
                     $liq_rec_messer_JJ = 80;
                   }
 
                   $consultaPPVdor_JJ = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_JJ' AND anio = '$anio' AND idVendedor = 4";
                   $resultadoPPVdor_JJ = mysqli_query($con, $consultaPPVdor_JJ);
         
                   while ($RowPPVdor_JJ = mysqli_fetch_array($resultadoPPVdor_JJ)) {
         
                     $porceCumpliVdor_JJ = $RowPPVdor_JJ['porcentaje'];
                               
                     if ($porceCumpliVdor_JJ  > 100) {
                         $liq_ppto_messer_JJ = 0.009;
                     }elseif ($porceCumpliVdor_JJ >= 80) {
                         $liq_ppto_messer_JJ = 0.007;
                     }elseif ($porceCumpliVdor_JJ <= 79 && $sacaMes_JJ == '05' && $sacaAnio_JJ == '2021' || $porceCumpliVdor_JJ <= 79 && $sacaMes_JJ == '06' && $sacaAnio_JJ == '2021' || $porceCumpliVdor_JJ <= 79 && $sacaMes_JJ == '07' && $sacaAnio_JJ == '2021') {
                       $liq_ppto_messer_JJ = 0.0035;
                     }else {
                       $liq_ppto_messer_JJ = 0;
                     }
 
                     //echo $numeRac_JJ." - ".$numeFac_JJ." - ".$beseComisionApro_JJ." - ".$dias_JJ." - ".$nomCliente_JJ." - ".$venDor_JJ." - ".$porceCumpliVdor_JJ." - ".$liq_ppto_messer_JJ." - ".$liq_rec_messer_JJ."<br>";
                    
                     $comision_PPTO_JJ = $beseComisionApro_JJ*$liq_ppto_messer_JJ*($liq_rec_messer_JJ/100)/1.81;
                     $Total_comision_PPTO_JJ += $comision_PPTO_JJ;
 
                     //echo $comision_PPTO_JJ."<br>";
    
                   } 
                 } 

         }elseif ($RowConso_JJ['porcentaje'] == 5) {

           $nomCliente_JJ = $RowConso_JJ['cliente'];

           $codigo051_JJ = $RowConso_JJ['codigo'];
           $length051_JJ = 9;
           $codigoV_CLI_051_JJ = substr(str_repeat(0, $length051_JJ).$codigo051_JJ, - $length051_JJ);
 
           $numeRac_JJ = $RowConso_JJ['rec'];
           $numeFac_JJ = $RowConso_JJ['numero_']; 
           $beseComisionApro_JJ = $RowConso_JJ['base_trans'];
           $dias_JJ = $RowConso_JJ['dias'];
           $fecha_doc_JJ = $RowConso_JJ['fecha_doc'];
           $sacaMes_JJ = date('m', strtotime($fecha_doc_JJ));
           $sacaAnio_JJ = date('Y', strtotime($fecha_doc_JJ));
 
           //echo $nomCliente_JJ."<br>";
           
           $AsVddor_JJ = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_051_JJ'";
           $resultAsVddor_JJ = mysqli_query($con, $AsVddor_JJ);
 
           while ($rowVndor_JJ = mysqli_fetch_array($resultAsVddor_JJ)) {
               $venDor_JJ = $rowVndor_JJ['vendedor'];
           }
              if ($venDor_JJ == 'JJ') {                  
                   
                   if ($dias_JJ <= 90) {
                     $liq_rec_messer_JJ = 100;
                   }elseif ($dias_JJ > 120) {
                     $liq_rec_messer_JJ = 0;
                   }else {
                     $liq_rec_messer_JJ = 80;
                   }
 
                   $consultaPPVdor_JJ = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_JJ' AND anio = '$anio' AND idVendedor = 4";
                   $resultadoPPVdor_JJ = mysqli_query($con, $consultaPPVdor_JJ);
         
                   while ($RowPPVdor_JJ = mysqli_fetch_array($resultadoPPVdor_JJ)) {
         
                     $porceCumpliVdor_JJ = $RowPPVdor_JJ['porcentaje'];
                               
                     if ($porceCumpliVdor_JJ  > 100) {
                         $liq_ppto_messer_JJ = 0.009;
                     }elseif ($porceCumpliVdor_JJ >= 80) {
                         $liq_ppto_messer_JJ = 0.007;
                     }elseif ($porceCumpliVdor_JJ <= 79 && $sacaMes_JJ == '05' && $sacaAnio_JJ == '2021' || $porceCumpliVdor_JJ <= 79 && $sacaMes_JJ == '06' && $sacaAnio_JJ == '2021' || $porceCumpliVdor_JJ <= 79 && $sacaMes_JJ == '07' && $sacaAnio_JJ == '2021') {
                       $liq_ppto_messer_JJ = 0.0035;
                     }else {
                       $liq_ppto_messer_JJ = 0;
                     }
 
                     //echo $numeRac_JJ." - ".$numeFac_JJ." - ".$beseComisionApro_JJ." - ".$dias_JJ." - ".$nomCliente_JJ." - ".$venDor_JJ." - ".$porceCumpliVdor_JJ." - ".$liq_ppto_messer_JJ." - ".$liq_rec_messer_JJ."<br>";
                    
                     $comision_PPTO_JJ = $beseComisionApro_JJ*$liq_ppto_messer_JJ*($liq_rec_messer_JJ/100)/6;
                     $Total_comision_PPTO_JJ += $comision_PPTO_JJ;
 
                     //echo $comision_PPTO_JJ."<br>";
    
                   } 
                 } 

         }elseif ($RowConso_JJ['porcentaje'] == 20) {
           
           $nomCliente_JJ = $RowConso_JJ['cliente'];

           $codigo051_JJ = $RowConso_JJ['codigo'];
           $length051_JJ = 9;
           $codigoV_CLI_051_JJ = substr(str_repeat(0, $length051_JJ).$codigo051_JJ, - $length051_JJ);
 
           $numeRac_JJ = $RowConso_JJ['rec'];
           $numeFac_JJ = $RowConso_JJ['numero_']; 
           $beseComisionApro_JJ = $RowConso_JJ['base_trans'];
           $dias_JJ = $RowConso_JJ['dias'];
           $fecha_doc_JJ = $RowConso_JJ['fecha_doc'];
           $sacaMes_JJ = date('m', strtotime($fecha_doc_JJ));
           $sacaAnio_JJ = date('Y', strtotime($fecha_doc_JJ));
 
           //echo $nomCliente_JJ."<br>";
           
           $AsVddor_JJ = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_051_JJ'";
           $resultAsVddor_JJ = mysqli_query($con, $AsVddor_JJ);
 
           while ($rowVndor_JJ = mysqli_fetch_array($resultAsVddor_JJ)) {
               $venDor_JJ = $rowVndor_JJ['vendedor'];
           }
              if ($venDor_JJ == 'JJ') {                  
                   
                   if ($dias_JJ <= 90) {
                     $liq_rec_messer_JJ = 100;
                   }elseif ($dias_JJ > 120) {
                     $liq_rec_messer_JJ = 0;
                   }else {
                     $liq_rec_messer_JJ = 80;
                   }
 
                   $consultaPPVdor_JJ = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_JJ' AND anio = '$anio' AND idVendedor = 4";
                   $resultadoPPVdor_JJ = mysqli_query($con, $consultaPPVdor_JJ);
         
                   while ($RowPPVdor_JJ = mysqli_fetch_array($resultadoPPVdor_JJ)) {
         
                     $porceCumpliVdor_JJ = $RowPPVdor_JJ['porcentaje'];
                               
                     if ($porceCumpliVdor_JJ  > 100) {
                         $liq_ppto_messer_JJ = 0.009;
                     }elseif ($porceCumpliVdor_JJ >= 80) {
                         $liq_ppto_messer_JJ = 0.007;
                     }elseif ($porceCumpliVdor_JJ <= 79 && $sacaMes_JJ == '05' && $sacaAnio_JJ == '2021' || $porceCumpliVdor_JJ <= 79 && $sacaMes_JJ == '06' && $sacaAnio_JJ == '2021' || $porceCumpliVdor_JJ <= 79 && $sacaMes_JJ == '07' && $sacaAnio_JJ == '2021') {
                       $liq_ppto_messer_JJ = 0.0035;
                     }else {
                       $liq_ppto_messer_JJ = 0;
                     }
 
                     //echo $numeRac_JJ." - ".$numeFac_JJ." - ".$beseComisionApro_JJ." - ".$dias_JJ." - ".$nomCliente_JJ." - ".$venDor_JJ." - ".$porceCumpliVdor_JJ." - ".$liq_ppto_messer_JJ." - ".$liq_rec_messer_JJ."<br>";
                    
                     $comision_PPTO_JJ = $beseComisionApro_JJ*$liq_ppto_messer_JJ*($liq_rec_messer_JJ/100)/1.5;
                     $Total_comision_PPTO_JJ += $comision_PPTO_JJ;
 
                     //echo $comision_PPTO_JJ."<br>";
    
                   } 
                 } 
       }        
   }
//TRMINA CALCULO COMISIONES MESSER TOBERIN................................................................................................................................................................................................

//INICIA CALCULO COMISIONES MESSER CAZUCA................................................................................................................................................................................................
  
$consultaConso_JJ_060 = "SELECT * FROM comisiones_cazuca WHERE mes = '$mesC'";
$resultadoConso_JJ_060 = mysqli_query($con, $consultaConso_JJ_060);

while ($RowConso_JJ_060 = mysqli_fetch_array($resultadoConso_JJ_060)) {       
if ($RowConso_JJ_060['dias'] != '' && $RowConso_JJ_060['porc_3'] != '' && $RowConso_JJ_060['porc_4'] != '') {
    $nomCliente_JJ_060 = $RowConso_JJ_060['cliente'];

    $codigo060_JJ = $RowConso_JJ_060['codigo'];
    $length060_JJ = 9;
    $codigoV_CLI_060_JJ = substr(str_repeat(0, $length060_JJ).$codigo060_JJ, - $length060_JJ);

    $numeRac_JJ_060 = $RowConso_JJ_060['recibo'];
    $numeFac_JJ_060 = $RowConso_JJ_060['numero']; 
    $beseComisionApro_JJ_060 = $RowConso_JJ_060['base_com'];
    $dias_JJ_060 = $RowConso_JJ_060['dias'];
    $fecha_doc_JJ_060 = $RowConso_JJ_060['fecha_doc'];
    $sacaMes_JJ_060 = date('m', strtotime($fecha_doc_JJ_060));
    $sacaAnio_JJ_060 = date('Y', strtotime($fecha_doc_JJ_060));

        
    $AsVddor_JJ_060 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_060_JJ'";
    $resultAsVddor_JJ_060 = mysqli_query($con, $AsVddor_JJ_060);

    while ($rowVndor_JJ_060 = mysqli_fetch_array($resultAsVddor_JJ_060)) {
         $venDor_JJ_060 = $rowVndor_JJ_060['vendedor']; 
     }
       if ($venDor_JJ_060 == 'JJ') {                  
           
            if ($dias_JJ_060 <= 90) {
              $liq_rec_messer_JJ_060 = 100;
            }elseif ($dias_JJ_060 > 120) {
              $liq_rec_messer_JJ_060 = 0;
            }else {
              $liq_rec_messer_JJ_060 = 80;
            }

            $consultaPPVdor_JJ_060 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_JJ_060' AND anio = '$anio' AND idVendedor = 4";
            $resultadoPPVdor_JJ_060 = mysqli_query($con, $consultaPPVdor_JJ_060);
  
            while ($RowPPVdor_JJ_060 = mysqli_fetch_array($resultadoPPVdor_JJ_060)) {
  
              $porceCumpliVdor_JJ_060 = $RowPPVdor_JJ_060['porcentaje'];
                        
              if ($porceCumpliVdor_JJ_060  > 100) {
                  $liq_ppto_messer_JJ_060 = 0.009;
              }elseif ($porceCumpliVdor_JJ_060 >= 80) {
                  $liq_ppto_messer_JJ_060 = 0.007;
              }elseif ($porceCumpliVdor_JJ_060 <= 79 && $sacaMes_JJ_060 == '05' && $sacaAnio_JJ_060 == '2021' || $porceCumpliVdor_JJ_060 <= 79 && $sacaMes_JJ_060 == '06' && $sacaAnio_JJ_060 == '2021' || $porceCumpliVdor_JJ_060 <= 79 && $sacaMes_JJ_060 == '07' && $sacaAnio_JJ_060 == '2021') {
                $liq_ppto_messer_JJ_060 = 0.0035;
              }else {
                $liq_ppto_messer_JJ_060 = 0;
              }
              
              //echo $numeRac_JJ." - ".$numeFac_JJ." - ".$beseComisionApro_JJ." - ".$dias_JJ." - ".$nomCliente_JJ." - ".$venDor_JJ." - ".$porceCumpliVdor_JJ." - ".$liq_ppto_messer_JJ." - ".$liq_rec_messer_JJ."<br>";
             
              $comision_PPTO_JJ_060 = $beseComisionApro_JJ_060*$liq_ppto_messer_JJ_060*($liq_rec_messer_JJ_060/100);
              $Total_comision_PPTO_JJ_060 += $comision_PPTO_JJ_060;

              //echo $comision_PPTO_JJ."<br>";

            } 
          } 
        }elseif ($RowConso_JJ_060['porcentaje'] == 10) {

         $nomCliente_JJ_060 = $RowConso_JJ_060['cliente'];

    $codigo060_JJ = $RowConso_JJ_060['codigo'];
    $length060_JJ = 9;
    $codigoV_CLI_060_JJ = substr(str_repeat(0, $length060_JJ).$codigo060_JJ, - $length060_JJ);

    $numeRac_JJ_060 = $RowConso_JJ_060['recibo'];
    $numeFac_JJ_060 = $RowConso_JJ_060['numero']; 
    $beseComisionApro_JJ_060 = $RowConso_JJ_060['base_com'];
    $dias_JJ_060 = $RowConso_JJ_060['dias'];
    $fecha_doc_JJ_060 = $RowConso_JJ_060['fecha_doc'];
    $sacaMes_JJ_060 = date('m', strtotime($fecha_doc_JJ_060));
    $sacaAnio_JJ_060 = date('Y', strtotime($fecha_doc_JJ_060));

    //echo $nomCliente_JJ."<br>";
    
    $AsVddor_JJ_060 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_060_JJ'";
    $resultAsVddor_JJ_060 = mysqli_query($con, $AsVddor_JJ_060);

    while ($rowVndor_JJ_060 = mysqli_fetch_array($resultAsVddor_JJ_060)) {
         $venDor_JJ_060 = $rowVndor_JJ_060['vendedor'];
     }
       if ($venDor_JJ_060 == 'JJ') {                  
           
            if ($dias_JJ_060 <= 90) {
              $liq_rec_messer_JJ_060 = 100;
            }elseif ($dias_JJ_060 > 120) {
              $liq_rec_messer_JJ_060 = 0;
            }else {
              $liq_rec_messer_JJ_060 = 80;
            }

            $consultaPPVdor_JJ_060 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_JJ_060' AND anio = '$anio' AND idVendedor = 4";
            $resultadoPPVdor_JJ_060 = mysqli_query($con, $consultaPPVdor_JJ_060);
  
            while ($RowPPVdor_JJ_060 = mysqli_fetch_array($resultadoPPVdor_JJ_060)) {
  
              $porceCumpliVdor_JJ_060 = $RowPPVdor_JJ_060['porcentaje'];
                        
              if ($porceCumpliVdor_JJ_060  > 100) {
                  $liq_ppto_messer_JJ_060 = 0.009;
              }elseif ($porceCumpliVdor_JJ_060 >= 80) {
                  $liq_ppto_messer_JJ_060 = 0.007;
              }elseif ($porceCumpliVdor_JJ_060 <= 79 && $sacaMes_JJ_060 == '05' && $sacaAnio_JJ_060 == '2021' || $porceCumpliVdor_JJ_060 <= 79 && $sacaMes_JJ_060 == '06' && $sacaAnio_JJ_060 == '2021' || $porceCumpliVdor_JJ_060 <= 79 && $sacaMes_JJ_060 == '07' && $sacaAnio_JJ_060 == '2021') {
                $liq_ppto_messer_JJ_060 = 0.0035;
              }else {
                $liq_ppto_messer_JJ_060 = 0;
              }
              
              //echo $numeRac_JJ." - ".$numeFac_JJ." - ".$beseComisionApro_JJ." - ".$dias_JJ." - ".$nomCliente_JJ." - ".$venDor_JJ." - ".$porceCumpliVdor_JJ." - ".$liq_ppto_messer_JJ." - ".$liq_rec_messer_JJ."<br>";
             
              $comision_PPTO_JJ_060 = $beseComisionApro_JJ_060*$liq_ppto_messer_JJ_060*($liq_rec_messer_JJ_060/100)/3;
              $Total_comision_PPTO_JJ_060 += $comision_PPTO_JJ_060;

              //echo $comision_PPTO_JJ."<br>";

            } 
          } 

      }elseif ($RowConso_JJ_060['porcentaje'] == 16.5) {

       $nomCliente_JJ_060 = $RowConso_JJ_060['cliente'];

       $codigo060_JJ = $RowConso_JJ_060['codigo'];
       $length060_JJ = 9;
       $codigoV_CLI_060_JJ = substr(str_repeat(0, $length060_JJ).$codigo060_JJ, - $length060_JJ);

       $numeRac_JJ_060 = $RowConso_JJ_060['recibo'];
       $numeFac_JJ_060 = $RowConso_JJ_060['numero']; 
       $beseComisionApro_JJ_060 = $RowConso_JJ_060['base_com'];
       $dias_JJ_060 = $RowConso_JJ_060['dias'];
       $fecha_doc_JJ_060 = $RowConso_JJ_060['fecha_doc'];
       $sacaMes_JJ_060 = date('m', strtotime($fecha_doc_JJ_060));
       $sacaAnio_JJ_060 = date('Y', strtotime($fecha_doc_JJ_060));

       //echo $nomCliente_JJ."<br>";
       
       $AsVddor_JJ_060 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_060_JJ'";
       $resultAsVddor_JJ_060 = mysqli_query($con, $AsVddor_JJ_060);

       while ($rowVndor_JJ_060 = mysqli_fetch_array($resultAsVddor_JJ_060)) {
            $venDor_JJ_060 = $rowVndor_JJ_060['vendedor'];
        }
          if ($venDor_JJ_060 == 'JJ') {                  
              
               if ($dias_JJ_060 <= 90) {
                 $liq_rec_messer_JJ_060 = 100;
               }elseif ($dias_JJ_060 > 120) {
                 $liq_rec_messer_JJ_060 = 0;
               }else {
                 $liq_rec_messer_JJ_060 = 80;
               }

               $consultaPPVdor_JJ_060 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_JJ_060' AND anio = '$anio' AND idVendedor = 4";
               $resultadoPPVdor_JJ_060 = mysqli_query($con, $consultaPPVdor_JJ_060);
     
               while ($RowPPVdor_JJ_060 = mysqli_fetch_array($resultadoPPVdor_JJ_060)) {
     
                 $porceCumpliVdor_JJ_060 = $RowPPVdor_JJ_060['porcentaje'];
                           
                 if ($porceCumpliVdor_JJ_060  > 100) {
                     $liq_ppto_messer_JJ_060 = 0.009;
                 }elseif ($porceCumpliVdor_JJ_060 >= 80) {
                     $liq_ppto_messer_JJ_060 = 0.007;
                 }elseif ($porceCumpliVdor_JJ_060 <= 79 && $sacaMes_JJ_060 == '05' && $sacaAnio_JJ_060 == '2021' || $porceCumpliVdor_JJ_060 <= 79 && $sacaMes_JJ_060 == '06' && $sacaAnio_JJ_060 == '2021' || $porceCumpliVdor_JJ_060 <= 79 && $sacaMes_JJ_060 == '07' && $sacaAnio_JJ_060 == '2021') {
                   $liq_ppto_messer_JJ_060 = 0.0035;
                 }else {
                   $liq_ppto_messer_JJ_060 = 0;
                 }
                 
                 //echo $numeRac_JJ." - ".$numeFac_JJ." - ".$beseComisionApro_JJ." - ".$dias_JJ." - ".$nomCliente_JJ." - ".$venDor_JJ." - ".$porceCumpliVdor_JJ." - ".$liq_ppto_messer_JJ." - ".$liq_rec_messer_JJ."<br>";
                
                 $comision_PPTO_JJ_060 = $beseComisionApro_JJ_060*$liq_ppto_messer_JJ_060*($liq_rec_messer_JJ_060/100)/1.81;
                 $Total_comision_PPTO_JJ_060 += $comision_PPTO_JJ_060;

                 //echo $comision_PPTO_JJ."<br>";

               } 
             }

      }elseif ($RowConso_JJ_060['porcentaje'] == 5) {

       $nomCliente_JJ_060 = $RowConso_JJ_060['cliente'];

    $codigo060_JJ = $RowConso_JJ_060['codigo'];
    $length060_JJ = 9;
    $codigoV_CLI_060_JJ = substr(str_repeat(0, $length060_JJ).$codigo060_JJ, - $length060_JJ);

    $numeRac_JJ_060 = $RowConso_JJ_060['recibo'];
    $numeFac_JJ_060 = $RowConso_JJ_060['numero']; 
    $beseComisionApro_JJ_060 = $RowConso_JJ_060['base_com'];
    $dias_JJ_060 = $RowConso_JJ_060['dias'];
    $fecha_doc_JJ_060 = $RowConso_JJ_060['fecha_doc'];
    $sacaMes_JJ_060 = date('m', strtotime($fecha_doc_JJ_060));
    $sacaAnio_JJ_060 = date('Y', strtotime($fecha_doc_JJ_060));

    //echo $nomCliente_JJ."<br>";
    
    $AsVddor_JJ_060 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_060_JJ'";
    $resultAsVddor_JJ_060 = mysqli_query($con, $AsVddor_JJ_060);

    while ($rowVndor_JJ_060 = mysqli_fetch_array($resultAsVddor_JJ_060)) {
         $venDor_JJ_060 = $rowVndor_JJ_060['vendedor'];
     }
       if ($venDor_JJ_060 == 'JJ') {                  
           
            if ($dias_JJ_060 <= 90) {
              $liq_rec_messer_JJ_060 = 100;
            }elseif ($dias_JJ_060 > 120) {
              $liq_rec_messer_JJ_060 = 0;
            }else {
              $liq_rec_messer_JJ_060 = 80;
            }

            $consultaPPVdor_JJ_060 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_JJ_060' AND anio = '$anio' AND idVendedor = 4";
            $resultadoPPVdor_JJ_060 = mysqli_query($con, $consultaPPVdor_JJ_060);
  
            while ($RowPPVdor_JJ_060 = mysqli_fetch_array($resultadoPPVdor_JJ_060)) {
  
              $porceCumpliVdor_JJ_060 = $RowPPVdor_JJ_060['porcentaje'];
                        
              if ($porceCumpliVdor_JJ_060  > 100) {
                  $liq_ppto_messer_JJ_060 = 0.009;
              }elseif ($porceCumpliVdor_JJ_060 >= 80) {
                  $liq_ppto_messer_JJ_060 = 0.007;
              }elseif ($porceCumpliVdor_JJ_060 <= 79 && $sacaMes_JJ_060 == '05' && $sacaAnio_JJ_060 == '2021' || $porceCumpliVdor_JJ_060 <= 79 && $sacaMes_JJ_060 == '06' && $sacaAnio_JJ_060 == '2021' || $porceCumpliVdor_JJ_060 <= 79 && $sacaMes_JJ_060 == '07' && $sacaAnio_JJ_060 == '2021') {
                $liq_ppto_messer_JJ_060 = 0.0035;
              }else {
                $liq_ppto_messer_JJ_060 = 0;
              }
              
              //echo $numeRac_JJ." - ".$numeFac_JJ." - ".$beseComisionApro_JJ." - ".$dias_JJ." - ".$nomCliente_JJ." - ".$venDor_JJ." - ".$porceCumpliVdor_JJ." - ".$liq_ppto_messer_JJ." - ".$liq_rec_messer_JJ."<br>";
             
              $comision_PPTO_JJ_060 = $beseComisionApro_JJ_060*$liq_ppto_messer_JJ_060*($liq_rec_messer_JJ_060/100)/6;
              $Total_comision_PPTO_JJ_060 += $comision_PPTO_JJ_060;

              //echo $comision_PPTO_JJ."<br>";

            } 
          }

      }elseif ($RowConso_JJ_060['porcentaje'] == 20) {
        
       $nomCliente_JJ_060 = $RowConso_JJ_060['cliente'];

    $codigo060_JJ = $RowConso_JJ_060['codigo'];
    $length060_JJ = 9;
    $codigoV_CLI_060_JJ = substr(str_repeat(0, $length060_JJ).$codigo060_JJ, - $length060_JJ);

    $numeRac_JJ_060 = $RowConso_JJ_060['recibo'];
    $numeFac_JJ_060 = $RowConso_JJ_060['numero']; 
    $beseComisionApro_JJ_060 = $RowConso_JJ_060['base_com'];
    $dias_JJ_060 = $RowConso_JJ_060['dias'];
    $fecha_doc_JJ_060 = $RowConso_JJ_060['fecha_doc'];
    $sacaMes_JJ_060 = date('m', strtotime($fecha_doc_JJ_060));
    $sacaAnio_JJ_060 = date('Y', strtotime($fecha_doc_JJ_060));

    //echo $nomCliente_JJ."<br>";
    
    $AsVddor_JJ_060 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_060_JJ'";
    $resultAsVddor_JJ_060 = mysqli_query($con, $AsVddor_JJ_060);

    while ($rowVndor_JJ_060 = mysqli_fetch_array($resultAsVddor_JJ_060)) {
         $venDor_JJ_060 = $rowVndor_JJ_060['vendedor'];
     }
       if ($venDor_JJ_060 == 'JJ') {                  
           
            if ($dias_JJ_060 <= 90) {
              $liq_rec_messer_JJ_060 = 100;
            }elseif ($dias_JJ_060 > 120) {
              $liq_rec_messer_JJ_060 = 0;
            }else {
              $liq_rec_messer_JJ_060 = 80;
            }

            $consultaPPVdor_JJ_060 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_JJ_060' AND anio = '$anio' AND idVendedor = 4";
            $resultadoPPVdor_JJ_060 = mysqli_query($con, $consultaPPVdor_JJ_060);
  
            while ($RowPPVdor_JJ_060 = mysqli_fetch_array($resultadoPPVdor_JJ_060)) {
  
              $porceCumpliVdor_JJ_060 = $RowPPVdor_JJ_060['porcentaje'];
                        
              if ($porceCumpliVdor_JJ_060  > 100) {
                  $liq_ppto_messer_JJ_060 = 0.009;
              }elseif ($porceCumpliVdor_JJ_060 >= 80) {
                  $liq_ppto_messer_JJ_060 = 0.007;
              }elseif ($porceCumpliVdor_JJ_060 <= 79 && $sacaMes_JJ_060 == '05' && $sacaAnio_JJ_060 == '2021' || $porceCumpliVdor_JJ_060 <= 79 && $sacaMes_JJ_060 == '06' && $sacaAnio_JJ_060 == '2021' || $porceCumpliVdor_JJ_060 <= 79 && $sacaMes_JJ_060 == '07' && $sacaAnio_JJ_060 == '2021') {
                $liq_ppto_messer_JJ_060 = 0.0035;
              }else {
                $liq_ppto_messer_JJ_060 = 0;
              }
              
              //echo $numeRac_JJ." - ".$numeFac_JJ." - ".$beseComisionApro_JJ." - ".$dias_JJ." - ".$nomCliente_JJ." - ".$venDor_JJ." - ".$porceCumpliVdor_JJ." - ".$liq_ppto_messer_JJ." - ".$liq_rec_messer_JJ."<br>";
             
              $comision_PPTO_JJ_060 = $beseComisionApro_JJ_060*$liq_ppto_messer_JJ_060*($liq_rec_messer_JJ_060/100)/1.5;
              $Total_comision_PPTO_JJ_060 += $comision_PPTO_JJ_060;

              //echo $comision_PPTO_JJ."<br>";

            } 
          }
    } 

}

//TRMINA CALCULO COMISIONES MESSER CAZUCA................................................................................................................................................................................................

//INICIA CALCULO COMISIONES MESSER MANIZALES................................................................................................................................................................................................
  
$consultaConso_JJ_370 = "SELECT * FROM comisiones_manizales WHERE mes = '$mesC'";
$resultadoConso_JJ_370 = mysqli_query($con, $consultaConso_JJ_370);

while ($RowConso_JJ_370 = mysqli_fetch_array($resultadoConso_JJ_370)) {       
if ($RowConso_JJ_370['dias_transcurridos'] != '' && $RowConso_JJ_370['porc_3'] != '' && $RowConso_JJ_370['porc_4'] != '') {
  $nomCliente_JJ_370 = $RowConso_JJ_370['cliente'];

  $codigo370_JJ = $RowConso_JJ_370['codigo_cliente'];
  $length370_JJ = 9;
  $codigoV_CLI_370_JJ = substr(str_repeat(0, $length370_JJ).$codigo370_JJ, - $length370_JJ);

  $numeRac_JJ_370 = $RowConso_JJ_370['rec'];
  $numeFac_JJ_370 = $RowConso_JJ_370['no_fact']; 
  $beseComisionApro_JJ_370 = $RowConso_JJ_370['base_com'];
  $dias_JJ_370 = $RowConso_JJ_370['dias_transcurridos'];
  $fecha_doc_JJ_370 = $RowConso_JJ_370['fecha_doc'];
  $sacaMes_JJ_370 = date('m', strtotime($fecha_doc_JJ_370));
  $sacaAnio_JJ_370 = date('Y', strtotime($fecha_doc_JJ_370));

  //echo $codigoV_CLI_370_JJ."<br>";
  
  $AsVddor_JJ_370 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_370_JJ'";
  $resultAsVddor_JJ_370 = mysqli_query($con, $AsVddor_JJ_370);

  while ($rowVndor_JJ_370 = mysqli_fetch_array($resultAsVddor_JJ_370)) {
   $venDor_JJ_370 = $rowVndor_JJ_370['vendedor'];
   }
     if ($venDor_JJ_370 == 'JJ') { 

          if ($dias_JJ_370 <= 90) {
            $liq_rec_messer_JJ_370 = 100;
          }elseif ($dias_JJ_370 > 120) {
            $liq_rec_messer_JJ_370 = 0;
          }else {
            $liq_rec_messer_JJ_370 = 80;
          }

          $consultaPPVdor_JJ_370 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_JJ_370' AND anio = '$anio' AND idVendedor = 4";
          $resultadoPPVdor_JJ_370 = mysqli_query($con, $consultaPPVdor_JJ_370);

          while ($RowPPVdor_JJ_370 = mysqli_fetch_array($resultadoPPVdor_JJ_370)) {

            $porceCumpliVdor_JJ_370 = $RowPPVdor_JJ_370['porcentaje'];
                      
            if ($porceCumpliVdor_JJ_370  > 100) {
                $liq_ppto_messer_JJ_370 = 0.009;
            }elseif ($porceCumpliVdor_JJ_370 >= 80) {
                $liq_ppto_messer_JJ_370 = 0.007;
            }elseif ($porceCumpliVdor_JJ_370 <= 79 && $sacaMes_JJ_370 == '05' && $sacaAnio_JJ_370 == '2021' || $porceCumpliVdor_JJ_370 <= 79 && $sacaMes_JJ_370 == '06' && $sacaAnio_JJ_370 == '2021' || $porceCumpliVdor_JJ_370 <= 79 && $sacaMes_JJ_370 == '07' && $sacaAnio_JJ_370 == '2021') {
              $liq_ppto_messer_JJ_370 = 0.0035;
            }else {
              $liq_ppto_messer_JJ_370 = 0;
            }
            
            //echo $numeRac_JJ_060." - ".$numeFac_JJ_060." - ".$beseComisionApro_JJ_060." - ".$dias_JJ_060." - ".$nomCliente_JJ_060." - ".$venDor_JJ_060." - ".$porceCumpliVdor_JJ_060." - ".$liq_ppto_messer_JJ_060." - ".$liq_rec_messer_JJ_060."<br>";
           
            $comision_PPTO_JJ_370 = $beseComisionApro_JJ_370*$liq_ppto_messer_JJ_370*($liq_rec_messer_JJ_370/100);
            $Total_comision_PPTO_JJ_370 += $comision_PPTO_JJ_370;

            //echo $comision_PPTO_JJ_060."<br>";

          }
        }     
      }elseif ($RowConso_JJ_370['porcentaje'] == 10) {

       $nomCliente_JJ_370 = $RowConso_JJ_370['cliente'];

       $codigo370_JJ = $RowConso_JJ_370['codigo_cliente'];
       $length370_JJ = 9;
       $codigoV_CLI_370_JJ = substr(str_repeat(0, $length370_JJ).$codigo370_JJ, - $length370_JJ);
  
       $numeRac_JJ_370 = $RowConso_JJ_370['rec'];
       $numeFac_JJ_370 = $RowConso_JJ_370['no_fact']; 
       $beseComisionApro_JJ_370 = $RowConso_JJ_370['base_com'];
       $dias_JJ_370 = $RowConso_JJ_370['dias_transcurridos'];
       $fecha_doc_JJ_370 = $RowConso_JJ_370['fecha_doc'];
       $sacaMes_JJ_370 = date('m', strtotime($fecha_doc_JJ_370));
       $sacaAnio_JJ_370 = date('Y', strtotime($fecha_doc_JJ_370));
  
       //echo $nomCliente_JJ_060."<br>";
       
       $AsVddor_JJ_370 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_370_JJ'";
       $resultAsVddor_JJ_370 = mysqli_query($con, $AsVddor_JJ_370);
  
       while ($rowVndor_JJ_370 = mysqli_fetch_array($resultAsVddor_JJ_370)) {
        $venDor_JJ_370 = $rowVndor_JJ_370['vendedor'];    
        }
          if ($venDor_JJ_370 == 'JJ') {                 
              
               if ($dias_JJ_370 <= 90) {
                 $liq_rec_messer_JJ_370 = 100;
               }elseif ($dias_JJ_370 > 120) {
                 $liq_rec_messer_JJ_370 = 0;
               }else {
                 $liq_rec_messer_JJ_370 = 80;
               }
  
               $consultaPPVdor_JJ_370 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_JJ_370' AND anio = '$anio' AND idVendedor = 4";
               $resultadoPPVdor_JJ_370 = mysqli_query($con, $consultaPPVdor_JJ_370);
     
               while ($RowPPVdor_JJ_370 = mysqli_fetch_array($resultadoPPVdor_JJ_370)) {
     
                 $porceCumpliVdor_JJ_370 = $RowPPVdor_JJ_370['porcentaje'];
                           
                 if ($porceCumpliVdor_JJ_370  > 100) {
                     $liq_ppto_messer_JJ_370 = 0.009;
                 }elseif ($porceCumpliVdor_JJ_370 >= 80) {
                     $liq_ppto_messer_JJ_370 = 0.007;
                 }elseif ($porceCumpliVdor_JJ_370 <= 79 && $sacaMes_JJ_370 == '05' && $sacaAnio_JJ_370 == '2021' || $porceCumpliVdor_JJ_370 <= 79 && $sacaMes_JJ_370 == '06' && $sacaAnio_JJ_370 == '2021' || $porceCumpliVdor_JJ_370 <= 79 && $sacaMes_JJ_370 == '07' && $sacaAnio_JJ_370 == '2021') {
                   $liq_ppto_messer_JJ_370 = 0.0035;
                 }else {
                   $liq_ppto_messer_JJ_370 = 0;
                 }
                 
                 //echo $numeRac_JJ_060." - ".$numeFac_JJ_060." - ".$beseComisionApro_JJ_060." - ".$dias_JJ_060." - ".$nomCliente_JJ_060." - ".$venDor_JJ_060." - ".$porceCumpliVdor_JJ_060." - ".$liq_ppto_messer_JJ_060." - ".$liq_rec_messer_JJ_060."<br>";
                
                 $comision_PPTO_JJ_370 = $beseComisionApro_JJ_370*$liq_ppto_messer_JJ_370*($liq_rec_messer_JJ_370/100)/3;
                 $Total_comision_PPTO_JJ_370 += $comision_PPTO_JJ_370;
  
                 //echo $comision_PPTO_JJ_060."<br>";
  
               }
             } 

    }elseif ($RowConso_JJ_370['porcentaje'] == 16.5) {

     $nomCliente_JJ_370 = $RowConso_JJ_370['cliente'];

     $codigo370_JJ = $RowConso_JJ_370['codigo_cliente'];
     $length370_JJ = 9;
     $codigoV_CLI_370_JJ = substr(str_repeat(0, $length370_JJ).$codigo370_JJ, - $length370_JJ);

     $numeRac_JJ_370 = $RowConso_JJ_370['rec'];
     $numeFac_JJ_370 = $RowConso_JJ_370['no_fact']; 
     $beseComisionApro_JJ_370 = $RowConso_JJ_370['base_com'];
     $dias_JJ_370 = $RowConso_JJ_370['dias_transcurridos'];
     $fecha_doc_JJ_370 = $RowConso_JJ_370['fecha_doc'];
     $sacaMes_JJ_370 = date('m', strtotime($fecha_doc_JJ_370));
     $sacaAnio_JJ_370 = date('Y', strtotime($fecha_doc_JJ_370));

     //echo $nomCliente_JJ_060."<br>";
     
     $AsVddor_JJ_370 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_370_JJ'";
     $resultAsVddor_JJ_370 = mysqli_query($con, $AsVddor_JJ_370);

     while ($rowVndor_JJ_370 = mysqli_fetch_array($resultAsVddor_JJ_370)) {
      $venDor_JJ_370 = $rowVndor_JJ_370['vendedor'];    
      }
        if ($venDor_JJ_370 == 'JJ') {                 
            
             if ($dias_JJ_370 <= 90) {
               $liq_rec_messer_JJ_370 = 100;
             }elseif ($dias_JJ_370 > 120) {
               $liq_rec_messer_JJ_370 = 0;
             }else {
               $liq_rec_messer_JJ_370 = 80;
             }

             $consultaPPVdor_JJ_370 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_JJ_370' AND anio = '$anio' AND idVendedor = 4";
             $resultadoPPVdor_JJ_370 = mysqli_query($con, $consultaPPVdor_JJ_370);
   
             while ($RowPPVdor_JJ_370 = mysqli_fetch_array($resultadoPPVdor_JJ_370)) {
   
               $porceCumpliVdor_JJ_370 = $RowPPVdor_JJ_370['porcentaje'];
                         
               if ($porceCumpliVdor_JJ_370  > 100) {
                   $liq_ppto_messer_JJ_370 = 0.009;
               }elseif ($porceCumpliVdor_JJ_370 >= 80) {
                   $liq_ppto_messer_JJ_370 = 0.007;
               }elseif ($porceCumpliVdor_JJ_370 <= 79 && $sacaMes_JJ_370 == '05' && $sacaAnio_JJ_370 == '2021' || $porceCumpliVdor_JJ_370 <= 79 && $sacaMes_JJ_370 == '06' && $sacaAnio_JJ_370 == '2021' || $porceCumpliVdor_JJ_370 <= 79 && $sacaMes_JJ_370 == '07' && $sacaAnio_JJ_370 == '2021') {
                 $liq_ppto_messer_JJ_370 = 0.0035;
               }else {
                 $liq_ppto_messer_JJ_370 = 0;
               }
               
               //echo $numeRac_JJ_060." - ".$numeFac_JJ_060." - ".$beseComisionApro_JJ_060." - ".$dias_JJ_060." - ".$nomCliente_JJ_060." - ".$venDor_JJ_060." - ".$porceCumpliVdor_JJ_060." - ".$liq_ppto_messer_JJ_060." - ".$liq_rec_messer_JJ_060."<br>";
              
               $comision_PPTO_JJ_370 = $beseComisionApro_JJ_370*$liq_ppto_messer_JJ_370*($liq_rec_messer_JJ_370/100)/1.81;
               $Total_comision_PPTO_JJ_370 += $comision_PPTO_JJ_370;

               //echo $comision_PPTO_JJ_060."<br>";

             }
           }

    }elseif ($RowConso_JJ_370['porcentaje'] == 5) {

     $nomCliente_JJ_370 = $RowConso_JJ_370['cliente'];

     $codigo370_JJ = $RowConso_JJ_370['codigo_cliente'];
     $length370_JJ = 9;
     $codigoV_CLI_370_JJ = substr(str_repeat(0, $length370_JJ).$codigo370_JJ, - $length370_JJ);

     $numeRac_JJ_370 = $RowConso_JJ_370['rec'];
     $numeFac_JJ_370 = $RowConso_JJ_370['no_fact']; 
     $beseComisionApro_JJ_370 = $RowConso_JJ_370['base_com'];
     $dias_JJ_370 = $RowConso_JJ_370['dias_transcurridos'];
     $fecha_doc_JJ_370 = $RowConso_JJ_370['fecha_doc'];
     $sacaMes_JJ_370 = date('m', strtotime($fecha_doc_JJ_370));
     $sacaAnio_JJ_370 = date('Y', strtotime($fecha_doc_JJ_370));

     //echo $nomCliente_JJ_060."<br>";
     
     $AsVddor_JJ_370 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_370_JJ'";
     $resultAsVddor_JJ_370 = mysqli_query($con, $AsVddor_JJ_370);

     while ($rowVndor_JJ_370 = mysqli_fetch_array($resultAsVddor_JJ_370)) {
      $venDor_JJ_370 = $rowVndor_JJ_370['vendedor'];    
      }
        if ($venDor_JJ_370 == 'JJ') {                 
            
             if ($dias_JJ_370 <= 90) {
               $liq_rec_messer_JJ_370 = 100;
             }elseif ($dias_JJ_370 > 120) {
               $liq_rec_messer_JJ_370 = 0;
             }else {
               $liq_rec_messer_JJ_370 = 80;
             }

             $consultaPPVdor_JJ_370 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_JJ_370' AND anio = '$anio' AND idVendedor = 4";
             $resultadoPPVdor_JJ_370 = mysqli_query($con, $consultaPPVdor_JJ_370);
   
             while ($RowPPVdor_JJ_370 = mysqli_fetch_array($resultadoPPVdor_JJ_370)) {
   
               $porceCumpliVdor_JJ_370 = $RowPPVdor_JJ_370['porcentaje'];
                         
               if ($porceCumpliVdor_JJ_370  > 100) {
                   $liq_ppto_messer_JJ_370 = 0.009;
               }elseif ($porceCumpliVdor_JJ_370 >= 80) {
                   $liq_ppto_messer_JJ_370 = 0.007;
               }elseif ($porceCumpliVdor_JJ_370 <= 79 && $sacaMes_JJ_370 == '05' && $sacaAnio_JJ_370 == '2021' || $porceCumpliVdor_JJ_370 <= 79 && $sacaMes_JJ_370 == '06' && $sacaAnio_JJ_370 == '2021' || $porceCumpliVdor_JJ_370 <= 79 && $sacaMes_JJ_370 == '07' && $sacaAnio_JJ_370 == '2021') {
                 $liq_ppto_messer_JJ_370 = 0.0035;
               }else {
                 $liq_ppto_messer_JJ_370 = 0;
               }
               
               //echo $numeRac_JJ_060." - ".$numeFac_JJ_060." - ".$beseComisionApro_JJ_060." - ".$dias_JJ_060." - ".$nomCliente_JJ_060." - ".$venDor_JJ_060." - ".$porceCumpliVdor_JJ_060." - ".$liq_ppto_messer_JJ_060." - ".$liq_rec_messer_JJ_060."<br>";
              
               $comision_PPTO_JJ_370 = $beseComisionApro_JJ_370*$liq_ppto_messer_JJ_370*($liq_rec_messer_JJ_370/100)/6;
               $Total_comision_PPTO_JJ_370 += $comision_PPTO_JJ_370;

               //echo $comision_PPTO_JJ_060."<br>";

             }
           }

    }elseif ($RowConso_JJ_370['porcentaje'] == 20) {
      
     $nomCliente_JJ_370 = $RowConso_JJ_370['cliente'];

     $codigo370_JJ = $RowConso_JJ_370['codigo_cliente'];
     $length370_JJ = 9;
     $codigoV_CLI_370_JJ = substr(str_repeat(0, $length370_JJ).$codigo370_JJ, - $length370_JJ);

     $numeRac_JJ_370 = $RowConso_JJ_370['rec'];
     $numeFac_JJ_370 = $RowConso_JJ_370['no_fact']; 
     $beseComisionApro_JJ_370 = $RowConso_JJ_370['base_com'];
     $dias_JJ_370 = $RowConso_JJ_370['dias_transcurridos'];
     $fecha_doc_JJ_370 = $RowConso_JJ_370['fecha_doc'];
     $sacaMes_JJ_370 = date('m', strtotime($fecha_doc_JJ_370));
     $sacaAnio_JJ_370 = date('Y', strtotime($fecha_doc_JJ_370));

     //echo $nomCliente_JJ_060."<br>";
     
     $AsVddor_JJ_370 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_370_JJ'";
     $resultAsVddor_JJ_370 = mysqli_query($con, $AsVddor_JJ_370);

     while ($rowVndor_JJ_370 = mysqli_fetch_array($resultAsVddor_JJ_370)) {
      $venDor_JJ_370 = $rowVndor_JJ_370['vendedor'];    
      }
        if ($venDor_JJ_370 == 'JJ') {                 
            
             if ($dias_JJ_370 <= 90) {
               $liq_rec_messer_JJ_370 = 100;
             }elseif ($dias_JJ_370 > 120) {
               $liq_rec_messer_JJ_370 = 0;
             }else {
               $liq_rec_messer_JJ_370 = 80;
             }

             $consultaPPVdor_JJ_370 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_JJ_370' AND anio = '$anio' AND idVendedor = 4";
             $resultadoPPVdor_JJ_370 = mysqli_query($con, $consultaPPVdor_JJ_370);
   
             while ($RowPPVdor_JJ_370 = mysqli_fetch_array($resultadoPPVdor_JJ_370)) {
   
               $porceCumpliVdor_JJ_370 = $RowPPVdor_JJ_370['porcentaje'];
                         
               if ($porceCumpliVdor_JJ_370  > 100) {
                   $liq_ppto_messer_JJ_370 = 0.009;
               }elseif ($porceCumpliVdor_JJ_370 >= 80) {
                   $liq_ppto_messer_JJ_370 = 0.007;
               }elseif ($porceCumpliVdor_JJ_370 <= 79 && $sacaMes_JJ_370 == '05' && $sacaAnio_JJ_370 == '2021' || $porceCumpliVdor_JJ_370 <= 79 && $sacaMes_JJ_370 == '06' && $sacaAnio_JJ_370 == '2021' || $porceCumpliVdor_JJ_370 <= 79 && $sacaMes_JJ_370 == '07' && $sacaAnio_JJ_370 == '2021') {
                 $liq_ppto_messer_JJ_370 = 0.0035;
               }else {
                 $liq_ppto_messer_JJ_370 = 0;
               }
               
               //echo $numeRac_JJ_060." - ".$numeFac_JJ_060." - ".$beseComisionApro_JJ_060." - ".$dias_JJ_060." - ".$nomCliente_JJ_060." - ".$venDor_JJ_060." - ".$porceCumpliVdor_JJ_060." - ".$liq_ppto_messer_JJ_060." - ".$liq_rec_messer_JJ_060."<br>";
              
               $comision_PPTO_JJ_370 = $beseComisionApro_JJ_370*$liq_ppto_messer_JJ_370*($liq_rec_messer_JJ_370/100)/1.5;
               $Total_comision_PPTO_JJ_370 += $comision_PPTO_JJ_370;

               //echo $comision_PPTO_JJ_060."<br>";

             }
          }       
    }
}
//TRMINA CALCULO COMISIONES MESSER MANIZALES................................................................................................................................................................................................

//INICIA CALCULO COMISIONES MESSER ITAGUI................................................................................................................................................................................................
  
$consultaConso_JJ_590 = "SELECT * FROM comisiones_itagui WHERE mes = '$mesC'";
$resultadoConso_JJ_590 = mysqli_query($con, $consultaConso_JJ_590);

while ($RowConso_JJ_590 = mysqli_fetch_array($resultadoConso_JJ_590)) {       
if ($RowConso_JJ_590['dias'] != '' && $RowConso_JJ_590['porc_3'] != '' && $RowConso_JJ_590['porc_4'] != '') {
  $nomCliente_JJ_590 = $RowConso_JJ_590['cliente'];

  $codigo590_JJ = $RowConso_JJ_590['codigo'];
  $length590_JJ = 9;
  $codigoV_CLI_590_JJ = substr(str_repeat(0, $length590_JJ).$codigo590_JJ, - $length590_JJ);

  $numeRac_JJ_590 = $RowConso_JJ_590['recibo'];
  $numeFac_JJ_590 = $RowConso_JJ_590['numero_']; 
  $beseComisionApro_JJ_590 = $RowConso_JJ_590['base_com'];
  $dias_JJ_590 = $RowConso_JJ_590['dias'];
  $fecha_doc_JJ_590 = $RowConso_JJ_590['fecha_doc'];
  $sacaMes_JJ_590 = date('m', strtotime($fecha_doc_JJ_590));
  $sacaAnio_JJ_590 = date('Y', strtotime($fecha_doc_JJ_590));

  $AsVddor_JJ_590 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_590_JJ'";
  $resultAsVddor_JJ_590 = mysqli_query($con, $AsVddor_JJ_590);

  while ($rowVndor_JJ_590 = mysqli_fetch_array($resultAsVddor_JJ_590)) {
     $venDor_JJ_590 = $rowVndor_JJ_590['vendedor']; 
     
   }

     if ($venDor_JJ_590 == 'JJ') {                  
          
          if ($dias_JJ_590 <= 90) {
            $liq_rec_messer_JJ_590 = 100;
          }elseif ($dias_JJ_590 > 120) {
            $liq_rec_messer_JJ_590 = 0;
          }else {
            $liq_rec_messer_JJ_590 = 80;
          }          

          $consultaPPVdor_JJ_590 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_JJ_590' AND anio = '$anio' AND idVendedor = 4";
          $resultadoPPVdor_JJ_590 = mysqli_query($con, $consultaPPVdor_JJ_590);

          while ($RowPPVdor_JJ_590 = mysqli_fetch_array($resultadoPPVdor_JJ_590)) {

            $porceCumpliVdor_JJ_590 = $RowPPVdor_JJ_590['porcentaje'];
                      
            if ($porceCumpliVdor_JJ_590  > 100) {
                $liq_ppto_messer_JJ_590 = 0.009;
            }elseif ($porceCumpliVdor_JJ_590 >= 80) {
                $liq_ppto_messer_JJ_590 = 0.007;
            }elseif ($porceCumpliVdor_JJ_590 <= 79 && $sacaMes_JJ_590 == '05' && $sacaAnio_JJ_590 == '2021' || $porceCumpliVdor_JJ_590 <= 79 && $sacaMes_JJ_590 == '06' && $sacaAnio_JJ_590 == '2021' || $porceCumpliVdor_JJ_590 <= 79 && $sacaMes_JJ_590 == '07' && $sacaAnio_JJ_590 == '2021') {
              $liq_ppto_messer_JJ_590 = 0.0035;
            }else {
              $liq_ppto_messer_JJ_590 = 0;
            }
            
           //echo $numeRac_JJ_370." - ".$numeFac_JJ_370." - ".$beseComisionApro_JJ_370." - ".$dias_JJ_370." - ".$nomCliente_JJ_370." - ".$venDor_JJ_370." - ".$porceCumpliVdor_JJ_370." - ".$liq_ppto_messer_JJ_370." - ".$liq_rec_messer_JJ_370."<br>";
           
            $comision_PPTO_JJ_590 = $beseComisionApro_JJ_590*$liq_ppto_messer_JJ_590*($liq_rec_messer_JJ_590/100);
            $Total_comision_PPTO_JJ_590 += $comision_PPTO_JJ_590;

            //echo $comision_PPTO_JJ_370."<br>";

          }
        }    
      }elseif ($RowConso_JJ_590['porcentaje'] == 10) {

       $nomCliente_JJ_590 = $RowConso_JJ_590['cliente'];

  $codigo590_JJ = $RowConso_JJ_590['codigo'];
  $length590_JJ = 9;
  $codigoV_CLI_590_JJ = substr(str_repeat(0, $length590_JJ).$codigo590_JJ, - $length590_JJ);

  $numeRac_JJ_590 = $RowConso_JJ_590['recibo'];
  $numeFac_JJ_590 = $RowConso_JJ_590['numero_']; 
  $beseComisionApro_JJ_590 = $RowConso_JJ_590['base_com'];
  $dias_JJ_590 = $RowConso_JJ_590['dias'];
  $fecha_doc_JJ_590 = $RowConso_JJ_590['fecha_doc'];
  $sacaMes_JJ_590 = date('m', strtotime($fecha_doc_JJ_590));
  $sacaAnio_JJ_590 = date('Y', strtotime($fecha_doc_JJ_590));

  //echo $nomCliente_JJ_370."<br>";
  
  $AsVddor_JJ_590 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_590_JJ'";
  $resultAsVddor_JJ_590 = mysqli_query($con, $AsVddor_JJ_590);

  while ($rowVndor_JJ_590 = mysqli_fetch_array($resultAsVddor_JJ_590)) {
     $venDor_JJ_590 = $rowVndor_JJ_590['vendedor']; 
   }

     if ($venDor_JJ_590 == 'JJ') {                  
          
          if ($dias_JJ_590 <= 90) {
            $liq_rec_messer_JJ_590 = 100;
          }elseif ($dias_JJ_590 > 120) {
            $liq_rec_messer_JJ_590 = 0;
          }else {
            $liq_rec_messer_JJ_590 = 80;
          }

          $consultaPPVdor_JJ_590 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_JJ_590' AND anio = '$anio' AND idVendedor = 4";
          $resultadoPPVdor_JJ_590 = mysqli_query($con, $consultaPPVdor_JJ_590);

          while ($RowPPVdor_JJ_590 = mysqli_fetch_array($resultadoPPVdor_JJ_590)) {

            $porceCumpliVdor_JJ_590 = $RowPPVdor_JJ_590['porcentaje'];
                      
            if ($porceCumpliVdor_JJ_590  > 100) {
                $liq_ppto_messer_JJ_590 = 0.009;
            }elseif ($porceCumpliVdor_JJ_590 >= 80) {
                $liq_ppto_messer_JJ_590 = 0.007;
            }elseif ($porceCumpliVdor_JJ_590 <= 79 && $sacaMes_JJ_590 == '05' && $sacaAnio_JJ_590 == '2021' || $porceCumpliVdor_JJ_590 <= 79 && $sacaMes_JJ_590 == '06' && $sacaAnio_JJ_590 == '2021' || $porceCumpliVdor_JJ_590 <= 79 && $sacaMes_JJ_590 == '07' && $sacaAnio_JJ_590 == '2021') {
              $liq_ppto_messer_JJ_590 = 0.0035;
            }else {
              $liq_ppto_messer_JJ_590 = 0;
            }
            
           //echo $numeRac_JJ_370." - ".$numeFac_JJ_370." - ".$beseComisionApro_JJ_370." - ".$dias_JJ_370." - ".$nomCliente_JJ_370." - ".$venDor_JJ_370." - ".$porceCumpliVdor_JJ_370." - ".$liq_ppto_messer_JJ_370." - ".$liq_rec_messer_JJ_370."<br>";
           
            $comision_PPTO_JJ_590 = $beseComisionApro_JJ_590*$liq_ppto_messer_JJ_590*($liq_rec_messer_JJ_590/100)/3;
            $Total_comision_PPTO_JJ_590 += $comision_PPTO_JJ_590;

            //echo $comision_PPTO_JJ_370."<br>";

          }
        } 

    }elseif ($RowConso_JJ_590['porcentaje'] == 16.5) {

     $nomCliente_JJ_590 = $RowConso_JJ_590['cliente'];

     $codigo590_JJ = $RowConso_JJ_590['codigo'];
     $length590_JJ = 9;
     $codigoV_CLI_590_JJ = substr(str_repeat(0, $length590_JJ).$codigo590_JJ, - $length590_JJ);

     $numeRac_JJ_590 = $RowConso_JJ_590['recibo'];
     $numeFac_JJ_590 = $RowConso_JJ_590['numero_']; 
     $beseComisionApro_JJ_590 = $RowConso_JJ_590['base_com'];
     $dias_JJ_590 = $RowConso_JJ_590['dias'];
     $fecha_doc_JJ_590 = $RowConso_JJ_590['fecha_doc'];
     $sacaMes_JJ_590 = date('m', strtotime($fecha_doc_JJ_590));
     $sacaAnio_JJ_590 = date('Y', strtotime($fecha_doc_JJ_590));

     //echo $nomCliente_JJ_370."<br>";
     
     $AsVddor_JJ_590 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_590_JJ'";
     $resultAsVddor_JJ_590 = mysqli_query($con, $AsVddor_JJ_590);

     while ($rowVndor_JJ_590 = mysqli_fetch_array($resultAsVddor_JJ_590)) {
        $venDor_JJ_590 = $rowVndor_JJ_590['vendedor']; 
      }

        if ($venDor_JJ_590 == 'JJ') {                  
             
             if ($dias_JJ_590 <= 90) {
               $liq_rec_messer_JJ_590 = 100;
             }elseif ($dias_JJ_590 > 120) {
               $liq_rec_messer_JJ_590 = 0;
             }else {
               $liq_rec_messer_JJ_590 = 80;
             }

             $consultaPPVdor_JJ_590 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_JJ_590' AND anio = '$anio' AND idVendedor = 4";
             $resultadoPPVdor_JJ_590 = mysqli_query($con, $consultaPPVdor_JJ_590);
   
             while ($RowPPVdor_JJ_590 = mysqli_fetch_array($resultadoPPVdor_JJ_590)) {
   
               $porceCumpliVdor_JJ_590 = $RowPPVdor_JJ_590['porcentaje'];
                         
               if ($porceCumpliVdor_JJ_590  > 100) {
                   $liq_ppto_messer_JJ_590 = 0.009;
               }elseif ($porceCumpliVdor_JJ_590 >= 80) {
                   $liq_ppto_messer_JJ_590 = 0.007;
               }elseif ($porceCumpliVdor_JJ_590 <= 79 && $sacaMes_JJ_590 == '05' && $sacaAnio_JJ_590 == '2021' || $porceCumpliVdor_JJ_590 <= 79 && $sacaMes_JJ_590 == '06' && $sacaAnio_JJ_590 == '2021' || $porceCumpliVdor_JJ_590 <= 79 && $sacaMes_JJ_590 == '07' && $sacaAnio_JJ_590 == '2021') {
                 $liq_ppto_messer_JJ_590 = 0.0035;
               }else {
                 $liq_ppto_messer_JJ_590 = 0;
               }
               
              //echo $numeRac_JJ_370." - ".$numeFac_JJ_370." - ".$beseComisionApro_JJ_370." - ".$dias_JJ_370." - ".$nomCliente_JJ_370." - ".$venDor_JJ_370." - ".$porceCumpliVdor_JJ_370." - ".$liq_ppto_messer_JJ_370." - ".$liq_rec_messer_JJ_370."<br>";
              
               $comision_PPTO_JJ_590 = $beseComisionApro_JJ_590*$liq_ppto_messer_JJ_590*($liq_rec_messer_JJ_590/100)/1.81;
               $Total_comision_PPTO_JJ_590 += $comision_PPTO_JJ_590;

               //echo $comision_PPTO_JJ_370."<br>";

             }
           }

    }elseif ($RowConso_JJ_590['porcentaje'] == 5) {

     $nomCliente_JJ_590 = $RowConso_JJ_590['cliente'];

     $codigo590_JJ = $RowConso_JJ_590['codigo'];
     $length590_JJ = 9;
     $codigoV_CLI_590_JJ = substr(str_repeat(0, $length590_JJ).$codigo590_JJ, - $length590_JJ);

     $numeRac_JJ_590 = $RowConso_JJ_590['recibo'];
     $numeFac_JJ_590 = $RowConso_JJ_590['numero_']; 
     $beseComisionApro_JJ_590 = $RowConso_JJ_590['base_com'];
     $dias_JJ_590 = $RowConso_JJ_590['dias'];
     $fecha_doc_JJ_590 = $RowConso_JJ_590['fecha_doc'];
     $sacaMes_JJ_590 = date('m', strtotime($fecha_doc_JJ_590));
     $sacaAnio_JJ_590 = date('Y', strtotime($fecha_doc_JJ_590));

     //echo $nomCliente_JJ_370."<br>";
     
     $AsVddor_JJ_590 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_590_JJ'";
     $resultAsVddor_JJ_590 = mysqli_query($con, $AsVddor_JJ_590);

     while ($rowVndor_JJ_590 = mysqli_fetch_array($resultAsVddor_JJ_590)) {
        $venDor_JJ_590 = $rowVndor_JJ_590['vendedor']; 
      }

        if ($venDor_JJ_590 == 'JJ') {                  
             
             if ($dias_JJ_590 <= 90) {
               $liq_rec_messer_JJ_590 = 100;
             }elseif ($dias_JJ_590 > 120) {
               $liq_rec_messer_JJ_590 = 0;
             }else {
               $liq_rec_messer_JJ_590 = 80;
             }

             $consultaPPVdor_JJ_590 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_JJ_590' AND anio = '$anio' AND idVendedor = 4";
             $resultadoPPVdor_JJ_590 = mysqli_query($con, $consultaPPVdor_JJ_590);
   
             while ($RowPPVdor_JJ_590 = mysqli_fetch_array($resultadoPPVdor_JJ_590)) {
   
               $porceCumpliVdor_JJ_590 = $RowPPVdor_JJ_590['porcentaje'];
                         
               if ($porceCumpliVdor_JJ_590  > 100) {
                   $liq_ppto_messer_JJ_590 = 0.009;
               }elseif ($porceCumpliVdor_JJ_590 >= 80) {
                   $liq_ppto_messer_JJ_590 = 0.007;
               }elseif ($porceCumpliVdor_JJ_590 <= 79 && $sacaMes_JJ_590 == '05' && $sacaAnio_JJ_590 == '2021' || $porceCumpliVdor_JJ_590 <= 79 && $sacaMes_JJ_590 == '06' && $sacaAnio_JJ_590 == '2021' || $porceCumpliVdor_JJ_590 <= 79 && $sacaMes_JJ_590 == '07' && $sacaAnio_JJ_590 == '2021') {
                 $liq_ppto_messer_JJ_590 = 0.0035;
               }else {
                 $liq_ppto_messer_JJ_590 = 0;
               }
               
              //echo $numeRac_JJ_370." - ".$numeFac_JJ_370." - ".$beseComisionApro_JJ_370." - ".$dias_JJ_370." - ".$nomCliente_JJ_370." - ".$venDor_JJ_370." - ".$porceCumpliVdor_JJ_370." - ".$liq_ppto_messer_JJ_370." - ".$liq_rec_messer_JJ_370."<br>";
              
               $comision_PPTO_JJ_590 = $beseComisionApro_JJ_590*$liq_ppto_messer_JJ_590*($liq_rec_messer_JJ_590/100)/6;
               $Total_comision_PPTO_JJ_590 += $comision_PPTO_JJ_590;

               //echo $comision_PPTO_JJ_370."<br>";

             }
           }

    }elseif ($RowConso_JJ_590['porcentaje'] == 20) {
      
     $nomCliente_JJ_590 = $RowConso_JJ_590['cliente'];

     $codigo590_JJ = $RowConso_JJ_590['codigo'];
     $length590_JJ = 9;
     $codigoV_CLI_590_JJ = substr(str_repeat(0, $length590_JJ).$codigo590_JJ, - $length590_JJ);

     $numeRac_JJ_590 = $RowConso_JJ_590['recibo'];
     $numeFac_JJ_590 = $RowConso_JJ_590['numero_']; 
     $beseComisionApro_JJ_590 = $RowConso_JJ_590['base_com'];
     $dias_JJ_590 = $RowConso_JJ_590['dias'];
     $fecha_doc_JJ_590 = $RowConso_JJ_590['fecha_doc'];
     $sacaMes_JJ_590 = date('m', strtotime($fecha_doc_JJ_590));
     $sacaAnio_JJ_590 = date('Y', strtotime($fecha_doc_JJ_590));

     //echo $nomCliente_JJ_370."<br>";
     
     $AsVddor_JJ_590 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_590_JJ'";
     $resultAsVddor_JJ_590 = mysqli_query($con, $AsVddor_JJ_590);

     while ($rowVndor_JJ_590 = mysqli_fetch_array($resultAsVddor_JJ_590)) {
        $venDor_JJ_590 = $rowVndor_JJ_590['vendedor']; 
      }

        if ($venDor_JJ_590 == 'JJ') {                  
             
             if ($dias_JJ_590 <= 90) {
               $liq_rec_messer_JJ_590 = 100;
             }elseif ($dias_JJ_590 > 120) {
               $liq_rec_messer_JJ_590 = 0;
             }else {
               $liq_rec_messer_JJ_590 = 80;
             }

             $consultaPPVdor_JJ_590 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_JJ_590' AND anio = '$anio' AND idVendedor = 4";
             $resultadoPPVdor_JJ_590 = mysqli_query($con, $consultaPPVdor_JJ_590);
   
             while ($RowPPVdor_JJ_590 = mysqli_fetch_array($resultadoPPVdor_JJ_590)) {
   
               $porceCumpliVdor_JJ_590 = $RowPPVdor_JJ_590['porcentaje'];
                         
               if ($porceCumpliVdor_JJ_590  > 100) {
                   $liq_ppto_messer_JJ_590 = 0.009;
               }elseif ($porceCumpliVdor_JJ_590 >= 80) {
                   $liq_ppto_messer_JJ_590 = 0.007;
               }elseif ($porceCumpliVdor_JJ_590 <= 79 && $sacaMes_JJ_590 == '05' && $sacaAnio_JJ_590 == '2021' || $porceCumpliVdor_JJ_590 <= 79 && $sacaMes_JJ_590 == '06' && $sacaAnio_JJ_590 == '2021' || $porceCumpliVdor_JJ_590 <= 79 && $sacaMes_JJ_590 == '07' && $sacaAnio_JJ_590 == '2021') {
                 $liq_ppto_messer_JJ_590 = 0.0035;
               }else {
                 $liq_ppto_messer_JJ_590 = 0;
               }
               
              //echo $numeRac_JJ_370." - ".$numeFac_JJ_370." - ".$beseComisionApro_JJ_370." - ".$dias_JJ_370." - ".$nomCliente_JJ_370." - ".$venDor_JJ_370." - ".$porceCumpliVdor_JJ_370." - ".$liq_ppto_messer_JJ_370." - ".$liq_rec_messer_JJ_370."<br>";
              
               $comision_PPTO_JJ_590 = $beseComisionApro_JJ_590*$liq_ppto_messer_JJ_590*($liq_rec_messer_JJ_590/100)/1.5;
               $Total_comision_PPTO_JJ_590 += $comision_PPTO_JJ_590;

               //echo $comision_PPTO_JJ_370."<br>";

             }
           } 
         }    
     }  



//TRMINA CALCULO COMISIONES MESSER ITAGUI................................................................................................................................................................................................

 //Suma toberin para JJ
 $comisionesTobe_JJ = $Total_comision_PPTO_JJ; 
 //Suma Cazuca para JJ
 $comisionesCazu_JJ = $Total_comision_PPTO_JJ_060; 

 //Suma Manizales para JJ
 $comisionesManiz_JJ = $Total_comision_PPTO_JJ_370; 

 //Suma Itagui para JJ
 $comisionesItag_JJ = $Total_comision_PPTO_JJ_590;


 $total_comision_rec_x_messer_JJ = $comisionesTobe_JJ+$comisionesCazu_JJ+$comisionesManiz_JJ+$comisionesItag_JJ;

 $total_comision_JJ = $total_comision_rec_x_messer_JJ + $porcentajeRecaudo_JJ;
    
      $objPHPExcel->getActiveSheet()->setCellValue('C7', $suma_JJ_VT);
      $objPHPExcel->getActiveSheet()->setCellValue('D7', $suma_JJ_VS);
      $objPHPExcel->getActiveSheet()->setCellValue('E7', $suma_JJ_VM);
      $objPHPExcel->getActiveSheet()->setCellValue('F7', $suma_JJ_VITA);
      $objPHPExcel->getActiveSheet()->setCellValue('G7', $subtotalJJ);
      $objPHPExcel->getActiveSheet()->setCellValue('H7', $total_JJ_NC);
      $objPHPExcel->getActiveSheet()->setCellValue('I7', $total_vta_JJ);
      $objPHPExcel->getActiveSheet()->setCellValue('J7', $porcentaje_Cumpli_JJ."%");
      $objPHPExcel->getActiveSheet()->setCellValue('K7', $total_comision_rec_x_messer_JJ);
      $objPHPExcel->getActiveSheet()->setCellValue('M7', $ventas_ingegas_vendedor_JJ);
      $objPHPExcel->getActiveSheet()->setCellValue('N7', $porcentajeRecaudo_JJ);
      $objPHPExcel->getActiveSheet()->setCellValue('O7', $total_comision_JJ);     
      
    } elseif ($id_vendedor == 5) {

     $consulta_TV = "SELECT * FROM ventas_producto WHERE mes = '$mesC' AND anio = '$anio'";
      $resultado_TV = mysqli_query($con, $consulta_TV);

      while($linea_TV = mysqli_fetch_array($resultado_TV)){
        $promotor_retail = $linea_TV["promotor_retail"];
        $valor_total = $linea_TV["valor_total"];
        $tipo_doc = $linea_TV["tipo_doc"];
        $numero_legal = $linea_TV["numero_legal"];
        $cliente = $linea_TV["cliente"];
        $sucursal = $linea_TV['sucursal'];        

        //echo $cliente."<br>";

        $consulta1_TV = "SELECT * FROM asignacion_vendedores WHERE codigo = '$cliente'";
        $resultado1_TV = mysqli_query($con, $consulta1_TV);
        while ($linea1_TV = mysqli_fetch_array($resultado1_TV)) {
          $vendedor = $linea1_TV["vendedor"];          
          //echo $cliente." - ".$vendedor." - ".$sucursal."<br>";              
        }

        if ($sucursal == '051' && $vendedor == 'TV') {
          $suma_TV_VT += $valor_total;
        }

        if ($sucursal == '060' && $vendedor == 'TV') {
          $suma_TV_VS += $valor_total;
        }

        if ($sucursal == '370' && $vendedor == 'TV') {
          $suma_TV_VM += $valor_total;
        }

        if ($sucursal == '590' && $vendedor == 'TV') {
          $suma_TV_VITA += $valor_total;
        }
   
      }
          
      //echo $suma_TV_VT."<br>".$suma_TV_VS."<br>".$suma_TV_VM."<br>".$suma_TV_VITA; 

      if($suma_TV_VT == 0){
        $suma_TV_VT = "-";
      }

      if($suma_TV_VS == 0){
        $suma_TV_VS = "-";
      }

      if($suma_TV_VM == 0){
        $suma_TV_VM = "-";
      }

      if($suma_TV_VITA == 0){
        $suma_TV_VITA = "-";
      }  

      //VERIFICA VENTAS INGEGAS
      $consultaVTV = "SELECT * FROM vt_ing WHERE tercero_interno = '$nombre_vendedor'";
      $resultadoVTV = mysqli_query($con, $consultaVTV);

      while($lineaVTV = mysqli_fetch_array($resultadoVTV)){
        $cantidadTV = $lineaVTV["cantidad"];
        $valor_unitarioTV = $lineaVTV["valor_unitario"];

        $valor_ventasTV = $cantidadTV * $valor_unitarioTV;

        $ventas_ingegas_vendedor_TV += $valor_ventasTV;
      }

     $subtotal_TV = $suma_TV_VT+$suma_TV_VS+$suma_TV_VM+$suma_TV_VITA;

     $total_vta_TV = ($subtotal_TV - $total_TV_NC) + $ventas_ingegas_vendedor_TV;

     $porc_cumplimiento_TV =  round(($total_vta_TV/$valorPP)*100);

    //REGISTRAR PORCENTAJE........................................................................................................................................
    $existePPCUM_TV = "SELECT * FROM porce_cumpli_vendedor WHERE idVendedor = 5 AND mes LIKE '%$mesC%' AND anio = '$anio'";
    $resultExistePPCUM_TV = mysqli_query($con, $existePPCUM_TV);

      if (mysqli_num_rows($resultExistePPCUM_TV) <= 0) {
          
          $mes_0_TV = sprintf("%02d", $mesC);
          $registraPPCUM_TV = "INSERT INTO porce_cumpli_vendedor (idVendedor, mes, anio, porcentaje) VALUES (5,'$mes_0_TV','$anio','$porc_cumplimiento_TV')";
          $ressultRegistroPPCUM_TV = mysqli_query($con, $registraPPCUM_TV);

      }

            //INICIA CALCULO COMISIONES MESSER TOBERIN................................................................................................................................................................................................
            $consultaConso_TV = "SELECT * FROM comisiones_toberin WHERE mes = '$mesC'";
            $resultadoConso_TV = mysqli_query($con, $consultaConso_TV);
        
            while ($RowConso_TV = mysqli_fetch_array($resultadoConso_TV)) {       
             if ($RowConso_TV['dias'] != '' && $RowConso_TV['porc_3'] != '' && $RowConso_TV['porc_4'] != '') {
                 $nomCliente_TV = $RowConso_TV['cliente'];
        
                 $codigoCli_TV = $RowConso_TV['codigo'];
                 $length_TV = 9;
                 $codigoV_CLI_051_TV = substr(str_repeat(0, $length_TV).$codigoCli_TV, - $length_TV);
        
                 $numeRac_TV = $RowConso_TV['rec'];
                 $numeFac_TV = $RowConso_TV['numero_']; 
                 $beseComisionApro_TV = $RowConso_TV['base_trans'];
                 $dias_TV = $RowConso_TV['dias'];
                 $fecha_doc_TV = $RowConso_TV['fecha_doc'];
                 $sacaMes_TV = date('m', strtotime($fecha_doc_TV));
                 $sacaAnio_TV = date('Y', strtotime($fecha_doc_TV));
        
                 //echo $nomCliente_JJ."<br>";
                 
                 $AsVddor_TV = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_051_TV'";
                 $resultAsVddor_TV = mysqli_query($con, $AsVddor_TV);
        
                 while ($rowVndor_TV = mysqli_fetch_array($resultAsVddor_TV)) {
                      $venDor_TV = $rowVndor_TV['vendedor'];  
                  }
                    if ($venDor_TV == 'TV') {                 
                         
                         if ($dias_TV <= 90) {
                           $liq_rec_messer_TV = 100;
                         }elseif ($dias_TV > 120) {
                           $liq_rec_messer_TV = 0;
                         }else {
                           $liq_rec_messer_TV = 80;
                         }
        
                         $consultaPPVdor_TV = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_TV' AND anio = '$anio' AND idVendedor = 5";
                         $resultadoPPVdor_TV = mysqli_query($con, $consultaPPVdor_TV);
               
                         while ($RowPPVdor_TV = mysqli_fetch_array($resultadoPPVdor_TV)) {
               
                           $porceCumpliVdor_TV = $RowPPVdor_TV['porcentaje'];
                                     
                           if ($porceCumpliVdor_TV  > 100) {
                               $liq_ppto_messer_TV = 0.009;
                           }elseif ($porceCumpliVdor_TV >= 80) {
                               $liq_ppto_messer_TV = 0.007;
                           }elseif ($porceCumpliVdor_TV <= 79 && $sacaMes_TV == '05' && $sacaAnio_TV == '2021' || $porceCumpliVdor_TV <= 79 && $sacaMes_TV == '06' && $sacaAnio_TV == '2021' || $porceCumpliVdor_TV <= 79 && $sacaMes_TV == '07' && $sacaAnio_TV == '2021') {
                             $liq_ppto_messer_TV = 0.0035;
                           }else {
                             $liq_ppto_messer_TV = 0;
                           }
                           
                           //echo $numeRac_JJ." - ".$numeFac_JJ." - ".$beseComisionApro_JJ." - ".$dias_JJ." - ".$nomCliente_JJ." - ".$venDor_JJ." - ".$porceCumpliVdor_JJ." - ".$liq_ppto_messer_JJ." - ".$liq_rec_messer_JJ."<br>";
                          
                           $comision_PPTO_TV = $beseComisionApro_TV*$liq_ppto_messer_TV*($liq_rec_messer_TV/100);
                           $Total_comision_PPTO_TV += $comision_PPTO_TV;
        
                           //echo $comision_PPTO_JJ."<br>";
          
                         }
                       }    
                     }elseif ($RowConso_TV['porcentaje'] == 10) {
        
                      $nomCliente_TV = $RowConso_TV['cliente'];
        
                 $codigoCli_TV = $RowConso_TV['codigo'];
                 $length_TV = 9;
                 $codigoV_CLI_051_TV = substr(str_repeat(0, $length_TV).$codigoCli_TV, - $length_TV);
        
                 $numeRac_TV = $RowConso_TV['rec'];
                 $numeFac_TV = $RowConso_TV['numero_']; 
                 $beseComisionApro_TV = $RowConso_TV['base_trans'];
                 $dias_TV = $RowConso_TV['dias'];
                 $fecha_doc_TV = $RowConso_TV['fecha_doc'];
                 $sacaMes_TV = date('m', strtotime($fecha_doc_TV));
                 $sacaAnio_TV = date('Y', strtotime($fecha_doc_TV));
        
                 //echo $nomCliente_JJ."<br>";
                 
                 $AsVddor_TV = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_051_TV'";
                 $resultAsVddor_TV = mysqli_query($con, $AsVddor_TV);
        
                 while ($rowVndor_TV = mysqli_fetch_array($resultAsVddor_TV)) {
                      $venDor_TV = $rowVndor_TV['vendedor'];  
                  }
                    if ($venDor_TV == 'TV') {                 
                         
                         if ($dias_TV <= 90) {
                           $liq_rec_messer_TV = 100;
                         }elseif ($dias_TV > 120) {
                           $liq_rec_messer_TV = 0;
                         }else {
                           $liq_rec_messer_TV = 80;
                         }
        
                         $consultaPPVdor_TV = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_TV' AND anio = '$anio' AND idVendedor = 5";
                         $resultadoPPVdor_TV = mysqli_query($con, $consultaPPVdor_TV);
               
                         while ($RowPPVdor_TV = mysqli_fetch_array($resultadoPPVdor_TV)) {
               
                           $porceCumpliVdor_TV = $RowPPVdor_TV['porcentaje'];
                                     
                           if ($porceCumpliVdor_TV  > 100) {
                               $liq_ppto_messer_TV = 0.009;
                           }elseif ($porceCumpliVdor_TV >= 80) {
                               $liq_ppto_messer_TV = 0.007;
                           }elseif ($porceCumpliVdor_TV <= 79 && $sacaMes_TV == '05' && $sacaAnio_TV == '2021' || $porceCumpliVdor_TV <= 79 && $sacaMes_TV == '06' && $sacaAnio_TV == '2021' || $porceCumpliVdor_TV <= 79 && $sacaMes_TV == '07' && $sacaAnio_TV == '2021') {
                             $liq_ppto_messer_TV = 0.0035;
                           }else {
                             $liq_ppto_messer_TV = 0;
                           }
                           
                           //echo $numeRac_JJ." - ".$numeFac_JJ." - ".$beseComisionApro_JJ." - ".$dias_JJ." - ".$nomCliente_JJ." - ".$venDor_JJ." - ".$porceCumpliVdor_JJ." - ".$liq_ppto_messer_JJ." - ".$liq_rec_messer_JJ."<br>";
                          
                           $comision_PPTO_TV = $beseComisionApro_TV*$liq_ppto_messer_TV*($liq_rec_messer_TV/100)/3;
                           $Total_comision_PPTO_TV += $comision_PPTO_TV;
        
                           //echo $comision_PPTO_JJ."<br>";
          
                         }
                       } 
        
                   }elseif ($RowConso_TV['porcentaje'] == 16.5) {
        
                    $nomCliente_TV = $RowConso_TV['cliente'];
        
                    $codigoCli_TV = $RowConso_TV['codigo'];
                    $length_TV = 9;
                    $codigoV_CLI_051_TV = substr(str_repeat(0, $length_TV).$codigoCli_TV, - $length_TV);
           
                    $numeRac_TV = $RowConso_TV['rec'];
                    $numeFac_TV = $RowConso_TV['numero_']; 
                    $beseComisionApro_TV = $RowConso_TV['base_trans'];
                    $dias_TV = $RowConso_TV['dias'];
                    $fecha_doc_TV = $RowConso_TV['fecha_doc'];
                    $sacaMes_TV = date('m', strtotime($fecha_doc_TV));
                    $sacaAnio_TV = date('Y', strtotime($fecha_doc_TV));
           
                    //echo $nomCliente_JJ."<br>";
                    
                    $AsVddor_TV = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_051_TV'";
                    $resultAsVddor_TV = mysqli_query($con, $AsVddor_TV);
           
                    while ($rowVndor_TV = mysqli_fetch_array($resultAsVddor_TV)) {
                         $venDor_TV = $rowVndor_TV['vendedor'];  
                     }
                       if ($venDor_TV == 'TV') {                 
                            
                            if ($dias_TV <= 90) {
                              $liq_rec_messer_TV = 100;
                            }elseif ($dias_TV > 120) {
                              $liq_rec_messer_TV = 0;
                            }else {
                              $liq_rec_messer_TV = 80;
                            }
           
                            $consultaPPVdor_TV = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_TV' AND anio = '$anio' AND idVendedor = 5";
                            $resultadoPPVdor_TV = mysqli_query($con, $consultaPPVdor_TV);
                  
                            while ($RowPPVdor_TV = mysqli_fetch_array($resultadoPPVdor_TV)) {
                  
                              $porceCumpliVdor_TV = $RowPPVdor_TV['porcentaje'];
                                        
                              if ($porceCumpliVdor_TV  > 100) {
                                  $liq_ppto_messer_TV = 0.009;
                              }elseif ($porceCumpliVdor_TV >= 80) {
                                  $liq_ppto_messer_TV = 0.007;
                              }elseif ($porceCumpliVdor_TV <= 79 && $sacaMes_TV == '05' && $sacaAnio_TV == '2021' || $porceCumpliVdor_TV <= 79 && $sacaMes_TV == '06' && $sacaAnio_TV == '2021' || $porceCumpliVdor_TV <= 79 && $sacaMes_TV == '07' && $sacaAnio_TV == '2021') {
                                $liq_ppto_messer_TV = 0.0035;
                              }else {
                                $liq_ppto_messer_TV = 0;
                              }
                              
                              //echo $numeRac_JJ." - ".$numeFac_JJ." - ".$beseComisionApro_JJ." - ".$dias_JJ." - ".$nomCliente_JJ." - ".$venDor_JJ." - ".$porceCumpliVdor_JJ." - ".$liq_ppto_messer_JJ." - ".$liq_rec_messer_JJ."<br>";
                             
                              $comision_PPTO_TV = $beseComisionApro_TV*$liq_ppto_messer_TV*($liq_rec_messer_TV/100)/1.81;
                              $Total_comision_PPTO_TV += $comision_PPTO_TV;
           
                              //echo $comision_PPTO_JJ."<br>";
             
                            }
                          }
        
                   }elseif ($RowConso_TV['porcentaje'] == 5) {
        
                    $nomCliente_TV = $RowConso_TV['cliente'];
        
                    $codigoCli_TV = $RowConso_TV['codigo'];
                    $length_TV = 9;
                    $codigoV_CLI_051_TV = substr(str_repeat(0, $length_TV).$codigoCli_TV, - $length_TV);
           
                    $numeRac_TV = $RowConso_TV['rec'];
                    $numeFac_TV = $RowConso_TV['numero_']; 
                    $beseComisionApro_TV = $RowConso_TV['base_trans'];
                    $dias_TV = $RowConso_TV['dias'];
                    $fecha_doc_TV = $RowConso_TV['fecha_doc'];
                    $sacaMes_TV = date('m', strtotime($fecha_doc_TV));
                    $sacaAnio_TV = date('Y', strtotime($fecha_doc_TV));
           
                    //echo $nomCliente_JJ."<br>";
                    
                    $AsVddor_TV = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_051_TV'";
                    $resultAsVddor_TV = mysqli_query($con, $AsVddor_TV);
           
                    while ($rowVndor_TV = mysqli_fetch_array($resultAsVddor_TV)) {
                         $venDor_TV = $rowVndor_TV['vendedor'];  
                     }
                       if ($venDor_TV == 'TV') {                 
                            
                            if ($dias_TV <= 90) {
                              $liq_rec_messer_TV = 100;
                            }elseif ($dias_TV > 120) {
                              $liq_rec_messer_TV = 0;
                            }else {
                              $liq_rec_messer_TV = 80;
                            }
           
                            $consultaPPVdor_TV = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_TV' AND anio = '$anio' AND idVendedor = 5";
                            $resultadoPPVdor_TV = mysqli_query($con, $consultaPPVdor_TV);
                  
                            while ($RowPPVdor_TV = mysqli_fetch_array($resultadoPPVdor_TV)) {
                  
                              $porceCumpliVdor_TV = $RowPPVdor_TV['porcentaje'];
                                        
                              if ($porceCumpliVdor_TV  > 100) {
                                  $liq_ppto_messer_TV = 0.009;
                              }elseif ($porceCumpliVdor_TV >= 80) {
                                  $liq_ppto_messer_TV = 0.007;
                              }elseif ($porceCumpliVdor_TV <= 79 && $sacaMes_TV == '05' && $sacaAnio_TV == '2021' || $porceCumpliVdor_TV <= 79 && $sacaMes_TV == '06' && $sacaAnio_TV == '2021' || $porceCumpliVdor_TV <= 79 && $sacaMes_TV == '07' && $sacaAnio_TV == '2021') {
                                $liq_ppto_messer_TV = 0.0035;
                              }else {
                                $liq_ppto_messer_TV = 0;
                              }
                              
                              //echo $numeRac_JJ." - ".$numeFac_JJ." - ".$beseComisionApro_JJ." - ".$dias_JJ." - ".$nomCliente_JJ." - ".$venDor_JJ." - ".$porceCumpliVdor_JJ." - ".$liq_ppto_messer_JJ." - ".$liq_rec_messer_JJ."<br>";
                             
                              $comision_PPTO_TV = $beseComisionApro_TV*$liq_ppto_messer_TV*($liq_rec_messer_TV/100)/6;
                              $Total_comision_PPTO_TV += $comision_PPTO_TV;
           
                              //echo $comision_PPTO_JJ."<br>";
             
                            }
                          }
        
                   }elseif ($RowConso_TV['porcentaje'] == 20) {
                     
                    $nomCliente_TV = $RowConso_TV['cliente'];
        
                    $codigoCli_TV = $RowConso_TV['codigo'];
                    $length_TV = 9;
                    $codigoV_CLI_051_TV = substr(str_repeat(0, $length_TV).$codigoCli_TV, - $length_TV);
           
                    $numeRac_TV = $RowConso_TV['rec'];
                    $numeFac_TV = $RowConso_TV['numero_']; 
                    $beseComisionApro_TV = $RowConso_TV['base_trans'];
                    $dias_TV = $RowConso_TV['dias'];
                    $fecha_doc_TV = $RowConso_TV['fecha_doc'];
                    $sacaMes_TV = date('m', strtotime($fecha_doc_TV));
                    $sacaAnio_TV = date('Y', strtotime($fecha_doc_TV));
           
                    //echo $nomCliente_JJ."<br>";
                    
                    $AsVddor_TV = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_051_TV'";
                    $resultAsVddor_TV = mysqli_query($con, $AsVddor_TV);
           
                    while ($rowVndor_TV = mysqli_fetch_array($resultAsVddor_TV)) {
                         $venDor_TV = $rowVndor_TV['vendedor'];  
                     }
                       if ($venDor_TV == 'TV') {                 
                            
                            if ($dias_TV <= 90) {
                              $liq_rec_messer_TV = 100;
                            }elseif ($dias_TV > 120) {
                              $liq_rec_messer_TV = 0;
                            }else {
                              $liq_rec_messer_TV = 80;
                            }
           
                            $consultaPPVdor_TV = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_TV' AND anio = '$anio' AND idVendedor = 5";
                            $resultadoPPVdor_TV = mysqli_query($con, $consultaPPVdor_TV);
                  
                            while ($RowPPVdor_TV = mysqli_fetch_array($resultadoPPVdor_TV)) {
                  
                              $porceCumpliVdor_TV = $RowPPVdor_TV['porcentaje'];
                                        
                              if ($porceCumpliVdor_TV  > 100) {
                                  $liq_ppto_messer_TV = 0.009;
                              }elseif ($porceCumpliVdor_TV >= 80) {
                                  $liq_ppto_messer_TV = 0.007;
                              }elseif ($porceCumpliVdor_TV <= 79 && $sacaMes_TV == '05' && $sacaAnio_TV == '2021' || $porceCumpliVdor_TV <= 79 && $sacaMes_TV == '06' && $sacaAnio_TV == '2021' || $porceCumpliVdor_TV <= 79 && $sacaMes_TV == '07' && $sacaAnio_TV == '2021') {
                                $liq_ppto_messer_TV = 0.0035;
                              }else {
                                $liq_ppto_messer_TV = 0;
                              }
                              
                              //echo $numeRac_JJ." - ".$numeFac_JJ." - ".$beseComisionApro_JJ." - ".$dias_JJ." - ".$nomCliente_JJ." - ".$venDor_JJ." - ".$porceCumpliVdor_JJ." - ".$liq_ppto_messer_JJ." - ".$liq_rec_messer_JJ."<br>";
                             
                              $comision_PPTO_TV = $beseComisionApro_TV*$liq_ppto_messer_TV*($liq_rec_messer_TV/100)/1.5;
                              $Total_comision_PPTO_TV += $comision_PPTO_TV;
           
                              //echo $comision_PPTO_JJ."<br>";
             
                            }
                          }       
             
              }
         } 
         //TRMINA CALCULO COMISIONES MESSER TOBERIN................................................................................................................................................................................................
         
         //INICIA CALCULO COMISIONES MESSER CAZUCA................................................................................................................................................................................................
         $consultaConso_TV_060 = "SELECT * FROM comisiones_cazuca WHERE mes = '$mesC'";
         $resultadoConso_TV_060 = mysqli_query($con, $consultaConso_TV_060);
        
         while ($RowConso_TV_060 = mysqli_fetch_array($resultadoConso_TV_060)) {       
          if ($RowConso_TV_060['dias'] != '' && $RowConso_TV_060['porc_3'] != '' && $RowConso_TV_060['porc_4'] != '') {
              $nomCliente_TV_060 = $RowConso_TV_060['cliente'];
        
              $codigoCli_TV_060 = $RowConso_TV_060['codigo'];
              $length_TV_060 = 9;
              $codigoV_CLI_051_TV_060 = substr(str_repeat(0, $length_TV_060).$codigoCli_TV_060, - $length_TV_060);
        
              $numeRac_TV_060 = $RowConso_TV_060['recibo'];
              $numeFac_TV_060 = $RowConso_TV_060['numero']; 
              $beseComisionApro_TV_060 = $RowConso_TV_060['base_com'];
              $dias_TV_060 = $RowConso_TV_060['dias'];
              $fecha_doc_TV_060 = $RowConso_TV_060['fecha_doc'];
              $sacaMes_TV_060 = date('m', strtotime($fecha_doc_TV_060));
              $sacaAnio_TV_060 = date('Y', strtotime($fecha_doc_TV_060));
        
              //echo $nomCliente_TV."<br>";
              
              $AsVddor_TV_060 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_051_TV_060'";
              $resultAsVddor_TV_060 = mysqli_query($con, $AsVddor_TV_060);
        
              while ($rowVndor_TV_060 = mysqli_fetch_array($resultAsVddor_TV_060)) {
                $venDor_TV_060 = $rowVndor_TV_060['vendedor'];
              }
                 if ($venDor_TV_060 == 'TV') {                  
                     
                      if ($dias_TV_060 <= 90) {
                        $liq_rec_messer_TV_060 = 100;
                      }elseif ($dias_TV_060 > 120) {
                        $liq_rec_messer_TV_060 = 0;
                      }else {
                        $liq_rec_messer_TV_060 = 80;
                      }
        
                      $consultaPPVdor_TV_060 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_TV_060' AND anio = '$anio' AND idVendedor = 5";
                      $resultadoPPVdor_TV_060 = mysqli_query($con, $consultaPPVdor_TV_060);
            
                      while ($RowPPVdor_TV_060 = mysqli_fetch_array($resultadoPPVdor_TV_060)) {
            
                        $porceCumpliVdor_TV_060 = $RowPPVdor_TV_060['porcentaje'];
                                  
                        if ($porceCumpliVdor_TV_060  > 100) {
                            $liq_ppto_messer_TV_060 = 0.009;
                        }elseif ($porceCumpliVdor_TV_060 >= 80) {
                            $liq_ppto_messer_TV_060 = 0.007;
                        }elseif ($porceCumpliVdor_TV_060 <= 79 && $sacaMes_TV_060 == '05' && $sacaAnio_TV_060 == '2021' || $porceCumpliVdor_TV_060 <= 79 && $sacaMes_TV_060 == '06' && $sacaAnio_TV_060 == '2021' || $porceCumpliVdor_TV_060 <= 79 && $sacaMes_TV_060 == '07' && $sacaAnio_TV_060 == '2021') {
                          $liq_ppto_messer_TV_060 = 0.0035;
                        }else {
                          $liq_ppto_messer_TV_060 = 0;
                        }
                    
                        //echo $numeRac_TV." - ".$numeFac_TV." - ".$beseComisionApro_TV." - ".$dias_TV." - ".$nomCliente_TV." - ".$venDor_TV." - ".$porceCumpliVdor_TV." - ".$liq_ppto_messer_TV." - ".$liq_rec_messer_TV."<br>";
                       
                        $comision_PPTO_TV_060 = $beseComisionApro_TV_060*$liq_ppto_messer_TV_060*($liq_rec_messer_TV_060/100);
                        $Total_comision_PPTO_TV_060 += $comision_PPTO_TV_060;
        
                        //echo $comision_PPTO_TV."<br>";
        
                      } 
                    }   
                  }elseif ($RowConso_TV_060['porcentaje'] == 10) {
        
                    $nomCliente_TV_060 = $RowConso_TV_060['cliente'];
        
              $codigoCli_TV_060 = $RowConso_TV_060['codigo'];
              $length_TV_060 = 9;
              $codigoV_CLI_051_TV_060 = substr(str_repeat(0, $length_TV_060).$codigoCli_TV_060, - $length_TV_060);
        
              $numeRac_TV_060 = $RowConso_TV_060['recibo'];
              $numeFac_TV_060 = $RowConso_TV_060['numero']; 
              $beseComisionApro_TV_060 = $RowConso_TV_060['base_com'];
              $dias_TV_060 = $RowConso_TV_060['dias'];
              $fecha_doc_TV_060 = $RowConso_TV_060['fecha_doc'];
              $sacaMes_TV_060 = date('m', strtotime($fecha_doc_TV_060));
              $sacaAnio_TV_060 = date('Y', strtotime($fecha_doc_TV_060));
        
              //echo $nomCliente_TV."<br>";
              
              $AsVddor_TV_060 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_051_TV_060'";
              $resultAsVddor_TV_060 = mysqli_query($con, $AsVddor_TV_060);
        
              while ($rowVndor_TV_060 = mysqli_fetch_array($resultAsVddor_TV_060)) {
                $venDor_TV_060 = $rowVndor_TV_060['vendedor'];
              }
                 if ($venDor_TV_060 == 'TV') {                  
                     
                      if ($dias_TV_060 <= 90) {
                        $liq_rec_messer_TV_060 = 100;
                      }elseif ($dias_TV_060 > 120) {
                        $liq_rec_messer_TV_060 = 0;
                      }else {
                        $liq_rec_messer_TV_060 = 80;
                      }
        
                      $consultaPPVdor_TV_060 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_TV_060' AND anio = '$anio' AND idVendedor = 5";
                      $resultadoPPVdor_TV_060 = mysqli_query($con, $consultaPPVdor_TV_060);
            
                      while ($RowPPVdor_TV_060 = mysqli_fetch_array($resultadoPPVdor_TV_060)) {
            
                        $porceCumpliVdor_TV_060 = $RowPPVdor_TV_060['porcentaje'];
                                  
                        if ($porceCumpliVdor_TV_060  > 100) {
                            $liq_ppto_messer_TV_060 = 0.009;
                        }elseif ($porceCumpliVdor_TV_060 >= 80) {
                            $liq_ppto_messer_TV_060 = 0.007;
                        }elseif ($porceCumpliVdor_TV_060 <= 79 && $sacaMes_TV_060 == '05' && $sacaAnio_TV_060 == '2021' || $porceCumpliVdor_TV_060 <= 79 && $sacaMes_TV_060 == '06' && $sacaAnio_TV_060 == '2021' || $porceCumpliVdor_TV_060 <= 79 && $sacaMes_TV_060 == '07' && $sacaAnio_TV_060 == '2021') {
                          $liq_ppto_messer_TV_060 = 0.0035;
                        }else {
                          $liq_ppto_messer_TV_060 = 0;
                        }
                    
                        //echo $numeRac_TV." - ".$numeFac_TV." - ".$beseComisionApro_TV." - ".$dias_TV." - ".$nomCliente_TV." - ".$venDor_TV." - ".$porceCumpliVdor_TV." - ".$liq_ppto_messer_TV." - ".$liq_rec_messer_TV."<br>";
                       
                        $comision_PPTO_TV_060 = $beseComisionApro_TV_060*$liq_ppto_messer_TV_060*($liq_rec_messer_TV_060/100)/3;
                        $Total_comision_PPTO_TV_060 += $comision_PPTO_TV_060;
        
                        //echo $comision_PPTO_TV."<br>";
        
                      } 
                    }  
        
                }elseif ($RowConso_TV_060['porcentaje'] == 16.5) {
        
                  $nomCliente_TV_060 = $RowConso_TV_060['cliente'];
        
              $codigoCli_TV_060 = $RowConso_TV_060['codigo'];
              $length_TV_060 = 9;
              $codigoV_CLI_051_TV_060 = substr(str_repeat(0, $length_TV_060).$codigoCli_TV_060, - $length_TV_060);
        
              $numeRac_TV_060 = $RowConso_TV_060['recibo'];
              $numeFac_TV_060 = $RowConso_TV_060['numero']; 
              $beseComisionApro_TV_060 = $RowConso_TV_060['base_com'];
              $dias_TV_060 = $RowConso_TV_060['dias'];
              $fecha_doc_TV_060 = $RowConso_TV_060['fecha_doc'];
              $sacaMes_TV_060 = date('m', strtotime($fecha_doc_TV_060));
              $sacaAnio_TV_060 = date('Y', strtotime($fecha_doc_TV_060));
        
              //echo $nomCliente_TV."<br>";
              
              $AsVddor_TV_060 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_051_TV_060'";
              $resultAsVddor_TV_060 = mysqli_query($con, $AsVddor_TV_060);
        
              while ($rowVndor_TV_060 = mysqli_fetch_array($resultAsVddor_TV_060)) {
                $venDor_TV_060 = $rowVndor_TV_060['vendedor'];
              }
                 if ($venDor_TV_060 == 'TV') {                  
                     
                      if ($dias_TV_060 <= 90) {
                        $liq_rec_messer_TV_060 = 100;
                      }elseif ($dias_TV_060 > 120) {
                        $liq_rec_messer_TV_060 = 0;
                      }else {
                        $liq_rec_messer_TV_060 = 80;
                      }
        
                      $consultaPPVdor_TV_060 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_TV_060' AND anio = '$anio' AND idVendedor = 5";
                      $resultadoPPVdor_TV_060 = mysqli_query($con, $consultaPPVdor_TV_060);
            
                      while ($RowPPVdor_TV_060 = mysqli_fetch_array($resultadoPPVdor_TV_060)) {
            
                        $porceCumpliVdor_TV_060 = $RowPPVdor_TV_060['porcentaje'];
                                  
                        if ($porceCumpliVdor_TV_060  > 100) {
                            $liq_ppto_messer_TV_060 = 0.009;
                        }elseif ($porceCumpliVdor_TV_060 >= 80) {
                            $liq_ppto_messer_TV_060 = 0.007;
                        }elseif ($porceCumpliVdor_TV_060 <= 79 && $sacaMes_TV_060 == '05' && $sacaAnio_TV_060 == '2021' || $porceCumpliVdor_TV_060 <= 79 && $sacaMes_TV_060 == '06' && $sacaAnio_TV_060 == '2021' || $porceCumpliVdor_TV_060 <= 79 && $sacaMes_TV_060 == '07' && $sacaAnio_TV_060 == '2021') {
                          $liq_ppto_messer_TV_060 = 0.0035;
                        }else {
                          $liq_ppto_messer_TV_060 = 0;
                        }
                    
                        //echo $numeRac_TV." - ".$numeFac_TV." - ".$beseComisionApro_TV." - ".$dias_TV." - ".$nomCliente_TV." - ".$venDor_TV." - ".$porceCumpliVdor_TV." - ".$liq_ppto_messer_TV." - ".$liq_rec_messer_TV."<br>";
                       
                        $comision_PPTO_TV_060 = $beseComisionApro_TV_060*$liq_ppto_messer_TV_060*($liq_rec_messer_TV_060/100)/1.81;
                        $Total_comision_PPTO_TV_060 += $comision_PPTO_TV_060;
        
                        //echo $comision_PPTO_TV."<br>";
        
                      } 
                    } 
        
                }elseif ($RowConso_TV_060['porcentaje'] == 5) {
        
                  $nomCliente_TV_060 = $RowConso_TV_060['cliente'];
        
                  $codigoCli_TV_060 = $RowConso_TV_060['codigo'];
                  $length_TV_060 = 9;
                  $codigoV_CLI_051_TV_060 = substr(str_repeat(0, $length_TV_060).$codigoCli_TV_060, - $length_TV_060);
            
                  $numeRac_TV_060 = $RowConso_TV_060['recibo'];
                  $numeFac_TV_060 = $RowConso_TV_060['numero']; 
                  $beseComisionApro_TV_060 = $RowConso_TV_060['base_com'];
                  $dias_TV_060 = $RowConso_TV_060['dias'];
                  $fecha_doc_TV_060 = $RowConso_TV_060['fecha_doc'];
                  $sacaMes_TV_060 = date('m', strtotime($fecha_doc_TV_060));
                  $sacaAnio_TV_060 = date('Y', strtotime($fecha_doc_TV_060));
            
                  //echo $nomCliente_TV."<br>";
                  
                  $AsVddor_TV_060 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_051_TV_060'";
                  $resultAsVddor_TV_060 = mysqli_query($con, $AsVddor_TV_060);
            
                  while ($rowVndor_TV_060 = mysqli_fetch_array($resultAsVddor_TV_060)) {
                    $venDor_TV_060 = $rowVndor_TV_060['vendedor'];
                  }
                     if ($venDor_TV_060 == 'TV') {                  
                         
                          if ($dias_TV_060 <= 90) {
                            $liq_rec_messer_TV_060 = 100;
                          }elseif ($dias_TV_060 > 120) {
                            $liq_rec_messer_TV_060 = 0;
                          }else {
                            $liq_rec_messer_TV_060 = 80;
                          }
            
                          $consultaPPVdor_TV_060 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_TV_060' AND anio = '$anio' AND idVendedor = 5";
                          $resultadoPPVdor_TV_060 = mysqli_query($con, $consultaPPVdor_TV_060);
                
                          while ($RowPPVdor_TV_060 = mysqli_fetch_array($resultadoPPVdor_TV_060)) {
                
                            $porceCumpliVdor_TV_060 = $RowPPVdor_TV_060['porcentaje'];
                                      
                            if ($porceCumpliVdor_TV_060  > 100) {
                                $liq_ppto_messer_TV_060 = 0.009;
                            }elseif ($porceCumpliVdor_TV_060 >= 80) {
                                $liq_ppto_messer_TV_060 = 0.007;
                            }elseif ($porceCumpliVdor_TV_060 <= 79 && $sacaMes_TV_060 == '05' && $sacaAnio_TV_060 == '2021' || $porceCumpliVdor_TV_060 <= 79 && $sacaMes_TV_060 == '06' && $sacaAnio_TV_060 == '2021' || $porceCumpliVdor_TV_060 <= 79 && $sacaMes_TV_060 == '07' && $sacaAnio_TV_060 == '2021') {
                              $liq_ppto_messer_TV_060 = 0.0035;
                            }else {
                              $liq_ppto_messer_TV_060 = 0;
                            }
                        
                            //echo $numeRac_TV." - ".$numeFac_TV." - ".$beseComisionApro_TV." - ".$dias_TV." - ".$nomCliente_TV." - ".$venDor_TV." - ".$porceCumpliVdor_TV." - ".$liq_ppto_messer_TV." - ".$liq_rec_messer_TV."<br>";
                           
                            $comision_PPTO_TV_060 = $beseComisionApro_TV_060*$liq_ppto_messer_TV_060*($liq_rec_messer_TV_060/100)/6;
                            $Total_comision_PPTO_TV_060 += $comision_PPTO_TV_060;
            
                            //echo $comision_PPTO_TV."<br>";
            
                          } 
                        } 
        
                }elseif ($RowConso_TV_060['porcentaje'] == 20) {
                  
                  $nomCliente_TV_060 = $RowConso_TV_060['cliente'];
        
                  $codigoCli_TV_060 = $RowConso_TV_060['codigo'];
                  $length_TV_060 = 9;
                  $codigoV_CLI_051_TV_060 = substr(str_repeat(0, $length_TV_060).$codigoCli_TV_060, - $length_TV_060);
            
                  $numeRac_TV_060 = $RowConso_TV_060['recibo'];
                  $numeFac_TV_060 = $RowConso_TV_060['numero']; 
                  $beseComisionApro_TV_060 = $RowConso_TV_060['base_com'];
                  $dias_TV_060 = $RowConso_TV_060['dias'];
                  $fecha_doc_TV_060 = $RowConso_TV_060['fecha_doc'];
                  $sacaMes_TV_060 = date('m', strtotime($fecha_doc_TV_060));
                  $sacaAnio_TV_060 = date('Y', strtotime($fecha_doc_TV_060));
            
                  //echo $nomCliente_TV."<br>";
                  
                  $AsVddor_TV_060 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_051_TV_060'";
                  $resultAsVddor_TV_060 = mysqli_query($con, $AsVddor_TV_060);
            
                  while ($rowVndor_TV_060 = mysqli_fetch_array($resultAsVddor_TV_060)) {
                    $venDor_TV_060 = $rowVndor_TV_060['vendedor'];
                  }
                     if ($venDor_TV_060 == 'TV') {                  
                         
                          if ($dias_TV_060 <= 90) {
                            $liq_rec_messer_TV_060 = 100;
                          }elseif ($dias_TV_060 > 120) {
                            $liq_rec_messer_TV_060 = 0;
                          }else {
                            $liq_rec_messer_TV_060 = 80;
                          }
            
                          $consultaPPVdor_TV_060 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_TV_060' AND anio = '$anio' AND idVendedor = 5";
                          $resultadoPPVdor_TV_060 = mysqli_query($con, $consultaPPVdor_TV_060);
                
                          while ($RowPPVdor_TV_060 = mysqli_fetch_array($resultadoPPVdor_TV_060)) {
                
                            $porceCumpliVdor_TV_060 = $RowPPVdor_TV_060['porcentaje'];
                                      
                            if ($porceCumpliVdor_TV_060  > 100) {
                                $liq_ppto_messer_TV_060 = 0.009;
                            }elseif ($porceCumpliVdor_TV_060 >= 80) {
                                $liq_ppto_messer_TV_060 = 0.007;
                            }elseif ($porceCumpliVdor_TV_060 <= 79 && $sacaMes_TV_060 == '05' && $sacaAnio_TV_060 == '2021' || $porceCumpliVdor_TV_060 <= 79 && $sacaMes_TV_060 == '06' && $sacaAnio_TV_060 == '2021' || $porceCumpliVdor_TV_060 <= 79 && $sacaMes_TV_060 == '07' && $sacaAnio_TV_060 == '2021') {
                              $liq_ppto_messer_TV_060 = 0.0035;
                            }else {
                              $liq_ppto_messer_TV_060 = 0;
                            }
                        
                            //echo $numeRac_TV." - ".$numeFac_TV." - ".$beseComisionApro_TV." - ".$dias_TV." - ".$nomCliente_TV." - ".$venDor_TV." - ".$porceCumpliVdor_TV." - ".$liq_ppto_messer_TV." - ".$liq_rec_messer_TV."<br>";
                           
                            $comision_PPTO_TV_060 = $beseComisionApro_TV_060*$liq_ppto_messer_TV_060*($liq_rec_messer_TV_060/100)/1.5;
                            $Total_comision_PPTO_TV_060 += $comision_PPTO_TV_060;
            
                            //echo $comision_PPTO_TV."<br>";
            
                          } 
                        }         
           
          }
        } 
        //TRMINA CALCULO COMISIONES MESSER CAZUCA................................................................................................................................................................................................
        
        //INICIA CALCULO COMISIONES MESSER MANIZALES................................................................................................................................................................................................
        $consultaConso_TV_370 = "SELECT * FROM comisiones_manizales WHERE mes = '$mesC'";
        $resultadoConso_TV_370 = mysqli_query($con, $consultaConso_TV_370);
        
        while ($RowConso_TV_370 = mysqli_fetch_array($resultadoConso_TV_370)) {       
         if ($RowConso_TV_370['dias_transcurridos'] != '' && $RowConso_TV_370['porc_3'] != '' && $RowConso_TV_370['porc_4'] != '') {
             $nomCliente_TV_370 = $RowConso_TV_370['cliente'];
        
             $codigoCli_TV_370 = $RowConso_TV_370['codigo_cliente'];
             $length_TV_370 = 9;
             $codigoV_CLI_051_TV_370 = substr(str_repeat(0, $length_TV_370).$codigoCli_TV_370, - $length_TV_370);
        
             $numeRac_TV_370 = $RowConso_TV_370['rec'];
             $numeFac_TV_370 = $RowConso_TV_370['no_fact']; 
             $beseComisionApro_TV_370 = $RowConso_TV_370['base_com'];
             $dias_TV_370 = $RowConso_TV_370['dias_transcurridos'];
             $fecha_doc_TV_370 = $RowConso_TV_370['fecha_doc'];
             $sacaMes_TV_370 = date('m', strtotime($fecha_doc_TV_370));
             $sacaAnio_TV_370 = date('Y', strtotime($fecha_doc_TV_370));
        
             //echo $nomCliente_TV_060."<br>";
             
             $AsVddor_TV_370 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_051_TV_370'";
             $resultAsVddor_TV_370 = mysqli_query($con, $AsVddor_TV_370);
        
             while ($rowVndor_TV_370 = mysqli_fetch_array($resultAsVddor_TV_370)) {
                  $venDor_TV_370 = $rowVndor_TV_370['vendedor']; 
              }
                if ($venDor_TV_370 == 'TV') {                  
                     
                     if ($dias_TV_370 <= 90) {
                       $liq_rec_messer_TV_370 = 100;
                     }elseif ($dias_TV_370 > 120) {
                       $liq_rec_messer_TV_370 = 0;
                     }else {
                       $liq_rec_messer_TV_370 = 80;
                     }
        
                     $consultaPPVdor_TV_370 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_TV_370' AND anio = '$anio' AND idVendedor = 5";
                     $resultadoPPVdor_TV_370 = mysqli_query($con, $consultaPPVdor_TV_370);
           
                     while ($RowPPVdor_TV_370 = mysqli_fetch_array($resultadoPPVdor_TV_370)) {
           
                       $porceCumpliVdor_TV_370 = $RowPPVdor_TV_370['porcentaje'];
                                 
                       if ($porceCumpliVdor_TV_370  > 100) {
                           $liq_ppto_messer_TV_370 = 0.009;
                       }elseif ($porceCumpliVdor_TV_370 >= 80) {
                           $liq_ppto_messer_TV_370 = 0.007;
                       }elseif ($porceCumpliVdor_TV_370 <= 79 && $sacaMes_TV_370 == '05' && $sacaMes_TV_370 == '2021' || $porceCumpliVdor_TV_370 <= 79 && $sacaMes_TV_370 == '06' && $sacaMes_TV_370 == '2021' || $porceCumpliVdor_TV_370 <= 79 && $sacaMes_TV_370 == '07' && $sacaMes_TV_370 == '2021') {
                         $liq_ppto_messer_TV_370 = 0.0035;
                       }else {
                         $liq_ppto_messer_TV_370 = 0;
                       }
                   
                       //echo $numeRac_TV_060." - ".$numeFac_TV_060." - ".$beseComisionApro_TV_060." - ".$dias_TV_060." - ".$nomCliente_TV_060." - ".$venDor_TV_060." - ".$porceCumpliVdor_TV_060." - ".$liq_ppto_messer_TV_060." - ".$liq_rec_messer_TV_060."<br>";
                      
                       $comision_PPTO_TV_370 = $beseComisionApro_TV_370*$liq_ppto_messer_TV_370*($liq_rec_messer_TV_370/100);
                       $Total_comision_PPTO_TV_370 += $comision_PPTO_TV_370;
        
                       //echo $comision_PPTO_TV_060."<br>";
        
                     }
                    }    
                 }elseif ($RowConso_TV_370['porcentaje'] == 10) {
        
                  $nomCliente_TV_370 = $RowConso_TV_370['cliente'];
        
                  $codigoCli_TV_370 = $RowConso_TV_370['codigo_cliente'];
                  $length_TV_370 = 9;
                  $codigoV_CLI_051_TV_370 = substr(str_repeat(0, $length_TV_370).$codigoCli_TV_370, - $length_TV_370);
             
                  $numeRac_TV_370 = $RowConso_TV_370['rec'];
                  $numeFac_TV_370 = $RowConso_TV_370['no_fact']; 
                  $beseComisionApro_TV_370 = $RowConso_TV_370['base_com'];
                  $dias_TV_370 = $RowConso_TV_370['dias_transcurridos'];
                  $fecha_doc_TV_370 = $RowConso_TV_370['fecha_doc'];
                  $sacaMes_TV_370 = date('m', strtotime($fecha_doc_TV_370));
                  $sacaAnio_TV_370 = date('Y', strtotime($fecha_doc_TV_370));
             
                  //echo $nomCliente_TV_060."<br>";
                  
                  $AsVddor_TV_370 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_051_TV_370'";
                  $resultAsVddor_TV_370 = mysqli_query($con, $AsVddor_TV_370);
             
                  while ($rowVndor_TV_370 = mysqli_fetch_array($resultAsVddor_TV_370)) {
                       $venDor_TV_370 = $rowVndor_TV_370['vendedor']; 
                   }
                     if ($venDor_TV_370 == 'TV') {                  
                          
                          if ($dias_TV_370 <= 90) {
                            $liq_rec_messer_TV_370 = 100;
                          }elseif ($dias_TV_370 > 120) {
                            $liq_rec_messer_TV_370 = 0;
                          }else {
                            $liq_rec_messer_TV_370 = 80;
                          }
             
                          $consultaPPVdor_TV_370 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_TV_370' AND anio = '$anio' AND idVendedor = 5";
                          $resultadoPPVdor_TV_370 = mysqli_query($con, $consultaPPVdor_TV_370);
                
                          while ($RowPPVdor_TV_370 = mysqli_fetch_array($resultadoPPVdor_TV_370)) {
                
                            $porceCumpliVdor_TV_370 = $RowPPVdor_TV_370['porcentaje'];
                                      
                            if ($porceCumpliVdor_TV_370  > 100) {
                                $liq_ppto_messer_TV_370 = 0.009;
                            }elseif ($porceCumpliVdor_TV_370 >= 80) {
                                $liq_ppto_messer_TV_370 = 0.007;
                            }elseif ($porceCumpliVdor_TV_370 <= 79 && $sacaMes_TV_370 == '05' && $sacaMes_TV_370 == '2021' || $porceCumpliVdor_TV_370 <= 79 && $sacaMes_TV_370 == '06' && $sacaMes_TV_370 == '2021' || $porceCumpliVdor_TV_370 <= 79 && $sacaMes_TV_370 == '07' && $sacaMes_TV_370 == '2021') {
                              $liq_ppto_messer_TV_370 = 0.0035;
                            }else {
                              $liq_ppto_messer_TV_370 = 0;
                            }
                        
                            //echo $numeRac_TV_060." - ".$numeFac_TV_060." - ".$beseComisionApro_TV_060." - ".$dias_TV_060." - ".$nomCliente_TV_060." - ".$venDor_TV_060." - ".$porceCumpliVdor_TV_060." - ".$liq_ppto_messer_TV_060." - ".$liq_rec_messer_TV_060."<br>";
                           
                            $comision_PPTO_TV_370 = $beseComisionApro_TV_370*$liq_ppto_messer_TV_370*($liq_rec_messer_TV_370/100)/3;
                            $Total_comision_PPTO_TV_370 += $comision_PPTO_TV_370;
             
                            //echo $comision_PPTO_TV_060."<br>";
             
                          }
                         } 
        
               }elseif ($RowConso_TV_370['porcentaje'] == 16.5) {
        
                $nomCliente_TV_370 = $RowConso_TV_370['cliente'];
        
                $codigoCli_TV_370 = $RowConso_TV_370['codigo_cliente'];
                $length_TV_370 = 9;
                $codigoV_CLI_051_TV_370 = substr(str_repeat(0, $length_TV_370).$codigoCli_TV_370, - $length_TV_370);
           
                $numeRac_TV_370 = $RowConso_TV_370['rec'];
                $numeFac_TV_370 = $RowConso_TV_370['no_fact']; 
                $beseComisionApro_TV_370 = $RowConso_TV_370['base_com'];
                $dias_TV_370 = $RowConso_TV_370['dias_transcurridos'];
                $fecha_doc_TV_370 = $RowConso_TV_370['fecha_doc'];
                $sacaMes_TV_370 = date('m', strtotime($fecha_doc_TV_370));
                $sacaAnio_TV_370 = date('Y', strtotime($fecha_doc_TV_370));
           
                //echo $nomCliente_TV_060."<br>";
                
                $AsVddor_TV_370 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_051_TV_370'";
                $resultAsVddor_TV_370 = mysqli_query($con, $AsVddor_TV_370);
           
                while ($rowVndor_TV_370 = mysqli_fetch_array($resultAsVddor_TV_370)) {
                     $venDor_TV_370 = $rowVndor_TV_370['vendedor']; 
                 }
                   if ($venDor_TV_370 == 'TV') {                  
                        
                        if ($dias_TV_370 <= 90) {
                          $liq_rec_messer_TV_370 = 100;
                        }elseif ($dias_TV_370 > 120) {
                          $liq_rec_messer_TV_370 = 0;
                        }else {
                          $liq_rec_messer_TV_370 = 80;
                        }
           
                        $consultaPPVdor_TV_370 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_TV_370' AND anio = '$anio' AND idVendedor = 5";
                        $resultadoPPVdor_TV_370 = mysqli_query($con, $consultaPPVdor_TV_370);
              
                        while ($RowPPVdor_TV_370 = mysqli_fetch_array($resultadoPPVdor_TV_370)) {
              
                          $porceCumpliVdor_TV_370 = $RowPPVdor_TV_370['porcentaje'];
                                    
                          if ($porceCumpliVdor_TV_370  > 100) {
                              $liq_ppto_messer_TV_370 = 0.009;
                          }elseif ($porceCumpliVdor_TV_370 >= 80) {
                              $liq_ppto_messer_TV_370 = 0.007;
                          }elseif ($porceCumpliVdor_TV_370 <= 79 && $sacaMes_TV_370 == '05' && $sacaMes_TV_370 == '2021' || $porceCumpliVdor_TV_370 <= 79 && $sacaMes_TV_370 == '06' && $sacaMes_TV_370 == '2021' || $porceCumpliVdor_TV_370 <= 79 && $sacaMes_TV_370 == '07' && $sacaMes_TV_370 == '2021') {
                            $liq_ppto_messer_TV_370 = 0.0035;
                          }else {
                            $liq_ppto_messer_TV_370 = 0;
                          }
                      
                          //echo $numeRac_TV_060." - ".$numeFac_TV_060." - ".$beseComisionApro_TV_060." - ".$dias_TV_060." - ".$nomCliente_TV_060." - ".$venDor_TV_060." - ".$porceCumpliVdor_TV_060." - ".$liq_ppto_messer_TV_060." - ".$liq_rec_messer_TV_060."<br>";
                         
                          $comision_PPTO_TV_370 = $beseComisionApro_TV_370*$liq_ppto_messer_TV_370*($liq_rec_messer_TV_370/100)/1.81;
                          $Total_comision_PPTO_TV_370 += $comision_PPTO_TV_370;
           
                          //echo $comision_PPTO_TV_060."<br>";
           
                        }
                       }
        
               }elseif ($RowConso_TV_370['porcentaje'] == 5) {
        
                $nomCliente_TV_370 = $RowConso_TV_370['cliente'];
        
                $codigoCli_TV_370 = $RowConso_TV_370['codigo_cliente'];
                $length_TV_370 = 9;
                $codigoV_CLI_051_TV_370 = substr(str_repeat(0, $length_TV_370).$codigoCli_TV_370, - $length_TV_370);
           
                $numeRac_TV_370 = $RowConso_TV_370['rec'];
                $numeFac_TV_370 = $RowConso_TV_370['no_fact']; 
                $beseComisionApro_TV_370 = $RowConso_TV_370['base_com'];
                $dias_TV_370 = $RowConso_TV_370['dias_transcurridos'];
                $fecha_doc_TV_370 = $RowConso_TV_370['fecha_doc'];
                $sacaMes_TV_370 = date('m', strtotime($fecha_doc_TV_370));
                $sacaAnio_TV_370 = date('Y', strtotime($fecha_doc_TV_370));
           
                //echo $nomCliente_TV_060."<br>";
                
                $AsVddor_TV_370 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_051_TV_370'";
                $resultAsVddor_TV_370 = mysqli_query($con, $AsVddor_TV_370);
           
                while ($rowVndor_TV_370 = mysqli_fetch_array($resultAsVddor_TV_370)) {
                     $venDor_TV_370 = $rowVndor_TV_370['vendedor']; 
                 }
                   if ($venDor_TV_370 == 'TV') {                  
                        
                        if ($dias_TV_370 <= 90) {
                          $liq_rec_messer_TV_370 = 100;
                        }elseif ($dias_TV_370 > 120) {
                          $liq_rec_messer_TV_370 = 0;
                        }else {
                          $liq_rec_messer_TV_370 = 80;
                        }
           
                        $consultaPPVdor_TV_370 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_TV_370' AND anio = '$anio' AND idVendedor = 5";
                        $resultadoPPVdor_TV_370 = mysqli_query($con, $consultaPPVdor_TV_370);
              
                        while ($RowPPVdor_TV_370 = mysqli_fetch_array($resultadoPPVdor_TV_370)) {
              
                          $porceCumpliVdor_TV_370 = $RowPPVdor_TV_370['porcentaje'];
                                    
                          if ($porceCumpliVdor_TV_370  > 100) {
                              $liq_ppto_messer_TV_370 = 0.009;
                          }elseif ($porceCumpliVdor_TV_370 >= 80) {
                              $liq_ppto_messer_TV_370 = 0.007;
                          }elseif ($porceCumpliVdor_TV_370 <= 79 && $sacaMes_TV_370 == '05' && $sacaMes_TV_370 == '2021' || $porceCumpliVdor_TV_370 <= 79 && $sacaMes_TV_370 == '06' && $sacaMes_TV_370 == '2021' || $porceCumpliVdor_TV_370 <= 79 && $sacaMes_TV_370 == '07' && $sacaMes_TV_370 == '2021') {
                            $liq_ppto_messer_TV_370 = 0.0035;
                          }else {
                            $liq_ppto_messer_TV_370 = 0;
                          }
                      
                          //echo $numeRac_TV_060." - ".$numeFac_TV_060." - ".$beseComisionApro_TV_060." - ".$dias_TV_060." - ".$nomCliente_TV_060." - ".$venDor_TV_060." - ".$porceCumpliVdor_TV_060." - ".$liq_ppto_messer_TV_060." - ".$liq_rec_messer_TV_060."<br>";
                         
                          $comision_PPTO_TV_370 = $beseComisionApro_TV_370*$liq_ppto_messer_TV_370*($liq_rec_messer_TV_370/100)/6;
                          $Total_comision_PPTO_TV_370 += $comision_PPTO_TV_370;
           
                          //echo $comision_PPTO_TV_060."<br>";
           
                        }
                       }
        
               }elseif ($RowConso_TV_370['porcentaje'] == 20) {
                 
                $nomCliente_TV_370 = $RowConso_TV_370['cliente'];
        
                $codigoCli_TV_370 = $RowConso_TV_370['codigo_cliente'];
                $length_TV_370 = 9;
                $codigoV_CLI_051_TV_370 = substr(str_repeat(0, $length_TV_370).$codigoCli_TV_370, - $length_TV_370);
           
                $numeRac_TV_370 = $RowConso_TV_370['rec'];
                $numeFac_TV_370 = $RowConso_TV_370['no_fact']; 
                $beseComisionApro_TV_370 = $RowConso_TV_370['base_com'];
                $dias_TV_370 = $RowConso_TV_370['dias_transcurridos'];
                $fecha_doc_TV_370 = $RowConso_TV_370['fecha_doc'];
                $sacaMes_TV_370 = date('m', strtotime($fecha_doc_TV_370));
                $sacaAnio_TV_370 = date('Y', strtotime($fecha_doc_TV_370));
           
                //echo $nomCliente_TV_060."<br>";
                
                $AsVddor_TV_370 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_051_TV_370'";
                $resultAsVddor_TV_370 = mysqli_query($con, $AsVddor_TV_370);
           
                while ($rowVndor_TV_370 = mysqli_fetch_array($resultAsVddor_TV_370)) {
                     $venDor_TV_370 = $rowVndor_TV_370['vendedor']; 
                 }
                   if ($venDor_TV_370 == 'TV') {                  
                        
                        if ($dias_TV_370 <= 90) {
                          $liq_rec_messer_TV_370 = 100;
                        }elseif ($dias_TV_370 > 120) {
                          $liq_rec_messer_TV_370 = 0;
                        }else {
                          $liq_rec_messer_TV_370 = 80;
                        }
           
                        $consultaPPVdor_TV_370 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_TV_370' AND anio = '$anio' AND idVendedor = 5";
                        $resultadoPPVdor_TV_370 = mysqli_query($con, $consultaPPVdor_TV_370);
              
                        while ($RowPPVdor_TV_370 = mysqli_fetch_array($resultadoPPVdor_TV_370)) {
              
                          $porceCumpliVdor_TV_370 = $RowPPVdor_TV_370['porcentaje'];
                                    
                          if ($porceCumpliVdor_TV_370  > 100) {
                              $liq_ppto_messer_TV_370 = 0.009;
                          }elseif ($porceCumpliVdor_TV_370 >= 80) {
                              $liq_ppto_messer_TV_370 = 0.007;
                          }elseif ($porceCumpliVdor_TV_370 <= 79 && $sacaMes_TV_370 == '05' && $sacaMes_TV_370 == '2021' || $porceCumpliVdor_TV_370 <= 79 && $sacaMes_TV_370 == '06' && $sacaMes_TV_370 == '2021' || $porceCumpliVdor_TV_370 <= 79 && $sacaMes_TV_370 == '07' && $sacaMes_TV_370 == '2021') {
                            $liq_ppto_messer_TV_370 = 0.0035;
                          }else {
                            $liq_ppto_messer_TV_370 = 0;
                          }
                      
                          //echo $numeRac_TV_060." - ".$numeFac_TV_060." - ".$beseComisionApro_TV_060." - ".$dias_TV_060." - ".$nomCliente_TV_060." - ".$venDor_TV_060." - ".$porceCumpliVdor_TV_060." - ".$liq_ppto_messer_TV_060." - ".$liq_rec_messer_TV_060."<br>";
                         
                          $comision_PPTO_TV_370 = $beseComisionApro_TV_370*$liq_ppto_messer_TV_370*($liq_rec_messer_TV_370/100)/1.5;
                          $Total_comision_PPTO_TV_370 += $comision_PPTO_TV_370;
           
                          //echo $comision_PPTO_TV_060."<br>";
           
                        }
                       }
          } 
        } 
        //TRMINA CALCULO COMISIONES MESSER MANIZALES................................................................................................................................................................................................
        
        //INICIA CALCULO COMISIONES MESSER ITAGUI................................................................................................................................................................................................
        $consultaConso_TV_590 = "SELECT * FROM comisiones_itagui WHERE mes = '$mesC'";
        $resultadoConso_TV_590 = mysqli_query($con, $consultaConso_TV_590);
        
        while ($RowConso_TV_590 = mysqli_fetch_array($resultadoConso_TV_590)) {       
         if ($RowConso_TV_590['dias'] != '' && $RowConso_TV_590['porc_3'] != '' && $RowConso_TV_590['porc_4'] != '') {
             $nomCliente_TV_590 = $RowConso_TV_590['cliente'];
        
             $codigoCli_TV_590 = $RowConso_TV_590['codigo'];
             $length_TV_590 = 9;
             $codigoV_CLI_051_TV_590 = substr(str_repeat(0, $length_TV_590).$codigoCli_TV_590, - $length_TV_590); 
        
             $numeRac_TV_590 = $RowConso_TV_590['recibo'];
             $numeFac_TV_590 = $RowConso_TV_590['numero_']; 
             $beseComisionApro_TV_590 = $RowConso_TV_590['base_com'];
             $dias_TV_590 = $RowConso_TV_590['dias'];
             $fecha_doc_TV_590 = $RowConso_TV_590['fecha_doc'];
             $sacaMes_TV_590 = date('m', strtotime($fecha_doc_TV_590));
             $sacaAnio_TV_590 = date('Y', strtotime($fecha_doc_TV_590));
        
             //echo $nomCliente_TV_370."<br>";
             
             $AsVddor_TV_590 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_051_TV_590'";
             $resultAsVddor_TV_590 = mysqli_query($con, $AsVddor_TV_590);
        
             while ($rowVndor_TV_590 = mysqli_fetch_array($resultAsVddor_TV_590)) {
                      $venDor_TV_590 = $rowVndor_TV_590['vendedor'];
                  }
                if ($venDor_TV_590 == 'TV') {                  
                     
                     if ($dias_TV_590 <= 90) {
                       $liq_rec_messer_TV_590 = 100;
                     }elseif ($dias_TV_590 > 120) {
                       $liq_rec_messer_TV_590 = 0;
                     }else {
                       $liq_rec_messer_TV_590 = 80;
                     }
        
                     $consultaPPVdor_TV_590 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_TV_590' AND anio = '$anio' AND idVendedor = 5";
                     $resultadoPPVdor_TV_590 = mysqli_query($con, $consultaPPVdor_TV_590);
           
                     while ($RowPPVdor_TV_590 = mysqli_fetch_array($resultadoPPVdor_TV_590)) {
           
                       $porceCumpliVdor_TV_590 = $RowPPVdor_TV_590['porcentaje'];
                                 
                       if ($porceCumpliVdor_TV_590  > 100) {
                           $liq_ppto_messer_TV_590 = 0.009;
                       }elseif ($porceCumpliVdor_TV_590 >= 80) {
                           $liq_ppto_messer_TV_590 = 0.007;
                       }elseif ($porceCumpliVdor_TV_590 <= 79 && $sacaMes_TV_590 == '05' && $sacaAnio_TV_590 == '2021' || $porceCumpliVdor_TV_590 <= 79 && $sacaMes_TV_590 == '06' && $sacaAnio_TV_590 == '2021' || $porceCumpliVdor_TV_590 <= 79 && $sacaMes_TV_590 == '07' && $sacaAnio_TV_590 == '2021') {
                         $liq_ppto_messer_TV_590 = 0.0035;
                       }else {
                         $liq_ppto_messer_TV_590 = 0;
                       }
                   
                       //echo $numeRac_TV_370." - ".$numeFac_TV_370." - ".$beseComisionApro_TV_370." - ".$dias_TV_370." - ".$nomCliente_TV_370." - ".$venDor_TV_370." - ".$porceCumpliVdor_TV_370." - ".$liq_ppto_messer_TV_370." - ".$liq_rec_messer_TV_370."<br>";
                      
                       $comision_PPTO_TV_590 = $beseComisionApro_TV_590*$liq_ppto_messer_TV_590*($liq_rec_messer_TV_590/100);
                       $Total_comision_PPTO_TV_590 += $comision_PPTO_TV_590;
        
                       //echo $comision_PPTO_TV_370."<br>";
        
                     }
                   }     
                 }elseif ($RowConso_TV_590['porcentaje'] == 10) {
        
                  $nomCliente_TV_590 = $RowConso_TV_590['cliente'];
        
                  $codigoCli_TV_590 = $RowConso_TV_590['codigo'];
                  $length_TV_590 = 9;
                  $codigoV_CLI_051_TV_590 = substr(str_repeat(0, $length_TV_590).$codigoCli_TV_590, - $length_TV_590); 
             
                  $numeRac_TV_590 = $RowConso_TV_590['recibo'];
                  $numeFac_TV_590 = $RowConso_TV_590['numero_']; 
                  $beseComisionApro_TV_590 = $RowConso_TV_590['base_com'];
                  $dias_TV_590 = $RowConso_TV_590['dias'];
                  $fecha_doc_TV_590 = $RowConso_TV_590['fecha_doc'];
                  $sacaMes_TV_590 = date('m', strtotime($fecha_doc_TV_590));
                  $sacaAnio_TV_590 = date('Y', strtotime($fecha_doc_TV_590));
             
                  //echo $nomCliente_TV_370."<br>";
                  
                  $AsVddor_TV_590 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_051_TV_590'";
                  $resultAsVddor_TV_590 = mysqli_query($con, $AsVddor_TV_590);
             
                  while ($rowVndor_TV_590 = mysqli_fetch_array($resultAsVddor_TV_590)) {
                           $venDor_TV_590 = $rowVndor_TV_590['vendedor'];
                       }
                     if ($venDor_TV_590 == 'TV') {                  
                          
                          if ($dias_TV_590 <= 90) {
                            $liq_rec_messer_TV_590 = 100;
                          }elseif ($dias_TV_590 > 120) {
                            $liq_rec_messer_TV_590 = 0;
                          }else {
                            $liq_rec_messer_TV_590 = 80;
                          }
             
                          $consultaPPVdor_TV_590 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_TV_590' AND anio = '$anio' AND idVendedor = 5";
                          $resultadoPPVdor_TV_590 = mysqli_query($con, $consultaPPVdor_TV_590);
                
                          while ($RowPPVdor_TV_590 = mysqli_fetch_array($resultadoPPVdor_TV_590)) {
                
                            $porceCumpliVdor_TV_590 = $RowPPVdor_TV_590['porcentaje'];
                                      
                            if ($porceCumpliVdor_TV_590  > 100) {
                                $liq_ppto_messer_TV_590 = 0.009;
                            }elseif ($porceCumpliVdor_TV_590 >= 80) {
                                $liq_ppto_messer_TV_590 = 0.007;
                            }elseif ($porceCumpliVdor_TV_590 <= 79 && $sacaMes_TV_590 == '05' && $sacaAnio_TV_590 == '2021' || $porceCumpliVdor_TV_590 <= 79 && $sacaMes_TV_590 == '06' && $sacaAnio_TV_590 == '2021' || $porceCumpliVdor_TV_590 <= 79 && $sacaMes_TV_590 == '07' && $sacaAnio_TV_590 == '2021') {
                              $liq_ppto_messer_TV_590 = 0.0035;
                            }else {
                              $liq_ppto_messer_TV_590 = 0;
                            }
                        
                            //echo $numeRac_TV_370." - ".$numeFac_TV_370." - ".$beseComisionApro_TV_370." - ".$dias_TV_370." - ".$nomCliente_TV_370." - ".$venDor_TV_370." - ".$porceCumpliVdor_TV_370." - ".$liq_ppto_messer_TV_370." - ".$liq_rec_messer_TV_370."<br>";
                           
                            $comision_PPTO_TV_590 = $beseComisionApro_TV_590*$liq_ppto_messer_TV_590*($liq_rec_messer_TV_590/100)/3;
                            $Total_comision_PPTO_TV_590 += $comision_PPTO_TV_590;
             
                            //echo $comision_PPTO_TV_370."<br>";
             
                          }
                        } 
        
               }elseif ($RowConso_TV_590['porcentaje'] == 16.5) {
        
                $nomCliente_TV_590 = $RowConso_TV_590['cliente'];
        
                $codigoCli_TV_590 = $RowConso_TV_590['codigo'];
                $length_TV_590 = 9;
                $codigoV_CLI_051_TV_590 = substr(str_repeat(0, $length_TV_590).$codigoCli_TV_590, - $length_TV_590); 
           
                $numeRac_TV_590 = $RowConso_TV_590['recibo'];
                $numeFac_TV_590 = $RowConso_TV_590['numero_']; 
                $beseComisionApro_TV_590 = $RowConso_TV_590['base_com'];
                $dias_TV_590 = $RowConso_TV_590['dias'];
                $fecha_doc_TV_590 = $RowConso_TV_590['fecha_doc'];
                $sacaMes_TV_590 = date('m', strtotime($fecha_doc_TV_590));
                $sacaAnio_TV_590 = date('Y', strtotime($fecha_doc_TV_590));
           
                //echo $nomCliente_TV_370."<br>";
                
                $AsVddor_TV_590 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_051_TV_590'";
                $resultAsVddor_TV_590 = mysqli_query($con, $AsVddor_TV_590);
           
                while ($rowVndor_TV_590 = mysqli_fetch_array($resultAsVddor_TV_590)) {
                         $venDor_TV_590 = $rowVndor_TV_590['vendedor'];
                     }
                   if ($venDor_TV_590 == 'TV') {                  
                        
                        if ($dias_TV_590 <= 90) {
                          $liq_rec_messer_TV_590 = 100;
                        }elseif ($dias_TV_590 > 120) {
                          $liq_rec_messer_TV_590 = 0;
                        }else {
                          $liq_rec_messer_TV_590 = 80;
                        }
           
                        $consultaPPVdor_TV_590 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_TV_590' AND anio = '$anio' AND idVendedor = 5";
                        $resultadoPPVdor_TV_590 = mysqli_query($con, $consultaPPVdor_TV_590);
              
                        while ($RowPPVdor_TV_590 = mysqli_fetch_array($resultadoPPVdor_TV_590)) {
              
                          $porceCumpliVdor_TV_590 = $RowPPVdor_TV_590['porcentaje'];
                                    
                          if ($porceCumpliVdor_TV_590  > 100) {
                              $liq_ppto_messer_TV_590 = 0.009;
                          }elseif ($porceCumpliVdor_TV_590 >= 80) {
                              $liq_ppto_messer_TV_590 = 0.007;
                          }elseif ($porceCumpliVdor_TV_590 <= 79 && $sacaMes_TV_590 == '05' && $sacaAnio_TV_590 == '2021' || $porceCumpliVdor_TV_590 <= 79 && $sacaMes_TV_590 == '06' && $sacaAnio_TV_590 == '2021' || $porceCumpliVdor_TV_590 <= 79 && $sacaMes_TV_590 == '07' && $sacaAnio_TV_590 == '2021') {
                            $liq_ppto_messer_TV_590 = 0.0035;
                          }else {
                            $liq_ppto_messer_TV_590 = 0;
                          }
                      
                          //echo $numeRac_TV_370." - ".$numeFac_TV_370." - ".$beseComisionApro_TV_370." - ".$dias_TV_370." - ".$nomCliente_TV_370." - ".$venDor_TV_370." - ".$porceCumpliVdor_TV_370." - ".$liq_ppto_messer_TV_370." - ".$liq_rec_messer_TV_370."<br>";
                         
                          $comision_PPTO_TV_590 = $beseComisionApro_TV_590*$liq_ppto_messer_TV_590*($liq_rec_messer_TV_590/100)/1.81;
                          $Total_comision_PPTO_TV_590 += $comision_PPTO_TV_590;
           
                          //echo $comision_PPTO_TV_370."<br>";
           
                        }
                      }
        
               }elseif ($RowConso_TV_590['porcentaje'] == 5) {
        
                $nomCliente_TV_590 = $RowConso_TV_590['cliente'];
        
                $codigoCli_TV_590 = $RowConso_TV_590['codigo'];
                $length_TV_590 = 9;
                $codigoV_CLI_051_TV_590 = substr(str_repeat(0, $length_TV_590).$codigoCli_TV_590, - $length_TV_590); 
           
                $numeRac_TV_590 = $RowConso_TV_590['recibo'];
                $numeFac_TV_590 = $RowConso_TV_590['numero_']; 
                $beseComisionApro_TV_590 = $RowConso_TV_590['base_com'];
                $dias_TV_590 = $RowConso_TV_590['dias'];
                $fecha_doc_TV_590 = $RowConso_TV_590['fecha_doc'];
                $sacaMes_TV_590 = date('m', strtotime($fecha_doc_TV_590));
                $sacaAnio_TV_590 = date('Y', strtotime($fecha_doc_TV_590));
           
                //echo $nomCliente_TV_370."<br>";
                
                $AsVddor_TV_590 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_051_TV_590'";
                $resultAsVddor_TV_590 = mysqli_query($con, $AsVddor_TV_590);
           
                while ($rowVndor_TV_590 = mysqli_fetch_array($resultAsVddor_TV_590)) {
                         $venDor_TV_590 = $rowVndor_TV_590['vendedor'];
                     }
                   if ($venDor_TV_590 == 'TV') {                  
                        
                        if ($dias_TV_590 <= 90) {
                          $liq_rec_messer_TV_590 = 100;
                        }elseif ($dias_TV_590 > 120) {
                          $liq_rec_messer_TV_590 = 0;
                        }else {
                          $liq_rec_messer_TV_590 = 80;
                        }
           
                        $consultaPPVdor_TV_590 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_TV_590' AND anio = '$anio' AND idVendedor = 5";
                        $resultadoPPVdor_TV_590 = mysqli_query($con, $consultaPPVdor_TV_590);
              
                        while ($RowPPVdor_TV_590 = mysqli_fetch_array($resultadoPPVdor_TV_590)) {
              
                          $porceCumpliVdor_TV_590 = $RowPPVdor_TV_590['porcentaje'];
                                    
                          if ($porceCumpliVdor_TV_590  > 100) {
                              $liq_ppto_messer_TV_590 = 0.009;
                          }elseif ($porceCumpliVdor_TV_590 >= 80) {
                              $liq_ppto_messer_TV_590 = 0.007;
                          }elseif ($porceCumpliVdor_TV_590 <= 79 && $sacaMes_TV_590 == '05' && $sacaAnio_TV_590 == '2021' || $porceCumpliVdor_TV_590 <= 79 && $sacaMes_TV_590 == '06' && $sacaAnio_TV_590 == '2021' || $porceCumpliVdor_TV_590 <= 79 && $sacaMes_TV_590 == '07' && $sacaAnio_TV_590 == '2021') {
                            $liq_ppto_messer_TV_590 = 0.0035;
                          }else {
                            $liq_ppto_messer_TV_590 = 0;
                          }
                      
                          //echo $numeRac_TV_370." - ".$numeFac_TV_370." - ".$beseComisionApro_TV_370." - ".$dias_TV_370." - ".$nomCliente_TV_370." - ".$venDor_TV_370." - ".$porceCumpliVdor_TV_370." - ".$liq_ppto_messer_TV_370." - ".$liq_rec_messer_TV_370."<br>";
                         
                          $comision_PPTO_TV_590 = $beseComisionApro_TV_590*$liq_ppto_messer_TV_590*($liq_rec_messer_TV_590/100)/6;
                          $Total_comision_PPTO_TV_590 += $comision_PPTO_TV_590;
           
                          //echo $comision_PPTO_TV_370."<br>";
           
                        }
                      }
        
               }elseif ($RowConso_TV_590['porcentaje'] == 20) {
                 
                $nomCliente_TV_590 = $RowConso_TV_590['cliente'];
        
                $codigoCli_TV_590 = $RowConso_TV_590['codigo'];
                $length_TV_590 = 9;
                $codigoV_CLI_051_TV_590 = substr(str_repeat(0, $length_TV_590).$codigoCli_TV_590, - $length_TV_590); 
           
                $numeRac_TV_590 = $RowConso_TV_590['recibo'];
                $numeFac_TV_590 = $RowConso_TV_590['numero_']; 
                $beseComisionApro_TV_590 = $RowConso_TV_590['base_com'];
                $dias_TV_590 = $RowConso_TV_590['dias'];
                $fecha_doc_TV_590 = $RowConso_TV_590['fecha_doc'];
                $sacaMes_TV_590 = date('m', strtotime($fecha_doc_TV_590));
                $sacaAnio_TV_590 = date('Y', strtotime($fecha_doc_TV_590));
           
                //echo $nomCliente_TV_370."<br>";
                
                $AsVddor_TV_590 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_051_TV_590'";
                $resultAsVddor_TV_590 = mysqli_query($con, $AsVddor_TV_590);
           
                while ($rowVndor_TV_590 = mysqli_fetch_array($resultAsVddor_TV_590)) {
                         $venDor_TV_590 = $rowVndor_TV_590['vendedor'];
                     }
                   if ($venDor_TV_590 == 'TV') {                  
                        
                        if ($dias_TV_590 <= 90) {
                          $liq_rec_messer_TV_590 = 100;
                        }elseif ($dias_TV_590 > 120) {
                          $liq_rec_messer_TV_590 = 0;
                        }else {
                          $liq_rec_messer_TV_590 = 80;
                        }
           
                        $consultaPPVdor_TV_590 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_TV_590' AND anio = '$anio' AND idVendedor = 5";
                        $resultadoPPVdor_TV_590 = mysqli_query($con, $consultaPPVdor_TV_590);
              
                        while ($RowPPVdor_TV_590 = mysqli_fetch_array($resultadoPPVdor_TV_590)) {
              
                          $porceCumpliVdor_TV_590 = $RowPPVdor_TV_590['porcentaje'];
                                    
                          if ($porceCumpliVdor_TV_590  > 100) {
                              $liq_ppto_messer_TV_590 = 0.009;
                          }elseif ($porceCumpliVdor_TV_590 >= 80) {
                              $liq_ppto_messer_TV_590 = 0.007;
                          }elseif ($porceCumpliVdor_TV_590 <= 79 && $sacaMes_TV_590 == '05' && $sacaAnio_TV_590 == '2021' || $porceCumpliVdor_TV_590 <= 79 && $sacaMes_TV_590 == '06' && $sacaAnio_TV_590 == '2021' || $porceCumpliVdor_TV_590 <= 79 && $sacaMes_TV_590 == '07' && $sacaAnio_TV_590 == '2021') {
                            $liq_ppto_messer_TV_590 = 0.0035;
                          }else {
                            $liq_ppto_messer_TV_590 = 0;
                          }
                      
                          //echo $numeRac_TV_370." - ".$numeFac_TV_370." - ".$beseComisionApro_TV_370." - ".$dias_TV_370." - ".$nomCliente_TV_370." - ".$venDor_TV_370." - ".$porceCumpliVdor_TV_370." - ".$liq_ppto_messer_TV_370." - ".$liq_rec_messer_TV_370."<br>";
                         
                          $comision_PPTO_TV_590 = $beseComisionApro_TV_590*$liq_ppto_messer_TV_590*($liq_rec_messer_TV_590/100)/1.5;
                          $Total_comision_PPTO_TV_590 += $comision_PPTO_TV_590;
           
                          //echo $comision_PPTO_TV_370."<br>";
           
                        }
                      }
              }
          } 
        
        //TRMINA CALCULO COMISIONES MESSER IATGUI................................................................................................................................................................................................
         
        
      //Suma toberin para TV
      $comisionesTobe_TV = $Total_comision_PPTO_TV;
      
      //Suma Cazuca para TV
      $comisionesCazu_TV = $Total_comision_PPTO_TV_060;
             
      //Suma MAnizales para TV
      $comisionesManiz_TV = $Total_comision_PPTO_TV_370;
              
      //Suma Itagui para TV
      $comisionesItag_TV = $Total_comision_PPTO_TV_590;
        
      $total_comision_rec_x_messer_TV = $comisionesTobe_TV+$comisionesCazu_TV+$comisionesManiz_TV+$comisionesItag_TV;
        
      //echo $total_comision_rec_x_messer_TV;
        
      $total_comision_TV = $total_comision_rec_x_messer_TV + $porcentajeRecaudo_TV;

      $objPHPExcel->getActiveSheet()->setCellValue('C8', $suma_TV_VT);
      $objPHPExcel->getActiveSheet()->setCellValue('D8', $suma_TV_VS);
      $objPHPExcel->getActiveSheet()->setCellValue('E8', $suma_TV_VM);
      $objPHPExcel->getActiveSheet()->setCellValue('F8', $suma_TV_VITA);
      $objPHPExcel->getActiveSheet()->setCellValue('G8', $subtotal_TV);
      $objPHPExcel->getActiveSheet()->setCellValue('H8', $total_TV_NC);
      $objPHPExcel->getActiveSheet()->setCellValue('I8', $total_vta_TV);
      $objPHPExcel->getActiveSheet()->setCellValue('J8', $porc_cumplimiento_TV."%");
      $objPHPExcel->getActiveSheet()->setCellValue('K8', $total_comision_rec_x_messer_TV);
      $objPHPExcel->getActiveSheet()->setCellValue('M8', $ventas_ingegas_vendedor_TV);
      $objPHPExcel->getActiveSheet()->setCellValue('N8', $porcentajeRecaudo_TV);
      $objPHPExcel->getActiveSheet()->setCellValue('O8', $total_comision_TV);
      
    } elseif ($id_vendedor == 6) {

      
      $consulta_VM = "SELECT * FROM ventas_producto WHERE mes = '$mesC' AND anio = '$anio'";
      $resultado_VM = mysqli_query($con, $consulta_VM);

      while($linea_VM = mysqli_fetch_array($resultado_VM)){
        $promotor_retail = $linea_VM["promotor_retail"];
        $valor_total = $linea_VM["valor_total"];
        $tipo_doc = $linea_VM["tipo_doc"];
        $numero_legal = $linea_VM["numero_legal"];
        $cliente = $linea_VM["cliente"];
        $sucursal = $linea_VM['sucursal'];        

        //echo $cliente."<br>";

        $consulta1_VM = "SELECT * FROM asignacion_vendedores WHERE codigo = '$cliente'";
        $resultado1_VM = mysqli_query($con, $consulta1_VM);
        while ($linea1_VM = mysqli_fetch_array($resultado1_VM)) {
          $vendedor = $linea1_VM["vendedor"];          
          //echo $cliente." - ".$vendedor." - ".$sucursal."<br>";           
        }

        if ($sucursal == '051' && $vendedor == 'VM') {
          $suma_VM_VT += $valor_total;
        }

        if ($sucursal == '060' && $vendedor == 'VM') {
          $suma_VM_VS += $valor_total;
        }

        if ($sucursal == '370' && $vendedor == 'VM') {
          $suma_VM_VM += $valor_total;
        }

        if ($sucursal == '590' && $vendedor == 'VM') {
          $suma_VM_VITA += $valor_total;
        }
   
      }

      if($suma_VM_VT == 0){
        $suma_VM_VT = "-";
      }

      if($suma_VM_VS == 0){
        $suma_VM_VS = "-";
      }

      if($suma_VM_VM == 0){
        $suma_VM_VM = "-";
      }

      if($suma_VM_VITA == 0){
        $suma_VM_VITA = "-";
      }  
          
      //echo $suma_VM_VT."<br>".$suma_VM_VS."<br>".$suma_VM_VM."<br>".$suma_VM_VITA; 
      $subtotal_VM = $suma_VM_VT+$suma_VM_VS+$suma_VM_VM+$suma_VM_VITA;

      //VERIFICA VENTAS INGEGAS
      $consultaVVM = "SELECT * FROM vt_ing WHERE tercero_interno = '$nombre_vendedor'";
      $resultadoVVM = mysqli_query($con, $consultaVVM);

      while($lineaVVM = mysqli_fetch_array($resultadoVVM)){
        $cantidadVM = $lineaVVM["cantidad"];
        $valor_unitarioVM = $lineaVVM["valor_unitario"];

        $valor_ventasVM = $cantidadVM * $valor_unitarioVM;

        $ventas_ingegas_vendedor_VM += $valor_ventasVM;
      }

      $total_vta_VM = ($subtotal_VM - $total_VM_NC) + $ventas_ingegas_vendedor_VM;

      $porc_cumplimiento_VM = round(($total_vta_VM/$valorPP)*100);
      
      //REGISTRAR PORCENTAJE........................................................................................................................................
      $existePPCUM_VM = "SELECT * FROM porce_cumpli_vendedor WHERE idVendedor = 6 AND mes LIKE '%$mesC%' AND anio = '$anio'";
      $resultExistePPCUM_VM = mysqli_query($con, $existePPCUM_VM);

        if (mysqli_num_rows($resultExistePPCUM_VM) <= 0) {
            
            $mes_0_VM = sprintf("%02d", $mesC);
            $registraPPCUM_VM = "INSERT INTO porce_cumpli_vendedor (idVendedor, mes, anio, porcentaje) VALUES (6,'$mes_0_VM','$anio','$porc_cumplimiento_VM')";
            $ressultRegistroPPCUM_VM = mysqli_query($con, $registraPPCUM_VM);

        }

    //INICIA CALCULO COMISIONES MESSER TOBERIN................................................................................................................................................................................................
$consultaConso_VM = "SELECT * FROM comisiones_toberin WHERE mes = '$mesC'";
$resultadoConso_VM = mysqli_query($con, $consultaConso_VM);

while ($RowConso_VM = mysqli_fetch_array($resultadoConso_VM)) {       
 if ($RowConso_VM['dias'] != '' && $RowConso_VM['porc_3'] != '' && $RowConso_VM['porc_4'] != '') {
     $nomCliente_VM = $RowConso_VM['cliente'];
     
     $codigo_VM = $RowConso_VM['codigo'];
     $length_VM = 9;
     $codigoV_CLI_VM = substr(str_repeat(0, $length_VM).$codigo_VM, - $length_VM);

     $numeRac_VM = $RowConso_VM['rec'];
     $numeFac_VM = $RowConso_VM['numero_']; 
     $beseComisionApro_VM = $RowConso_VM['base_trans'];
     $dias_VM = $RowConso_VM['dias'];
     $fecha_doc_VM = $RowConso_VM['fecha_doc'];
     $sacaMes_VM = date('m', strtotime($fecha_doc_VM));
     $sacaAnio_VM = date('Y', strtotime($fecha_doc_VM));

     //echo $nomCliente_JJ."<br>";
     
     $AsVddor_VM = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_VM'";
     $resultAsVddor_VM = mysqli_query($con, $AsVddor_VM);

     while ($rowVndor_VM = mysqli_fetch_array($resultAsVddor_VM)) {
              $venDor_VM = $rowVndor_VM['vendedor']; 
        }
        if ($venDor_VM == 'VM') {                  
            
             if ($dias_VM <= 90) {
               $liq_rec_messer_VM = 100;
             }elseif ($dias_VM > 120) {
               $liq_rec_messer_VM = 0;
             }else {
               $liq_rec_messer_VM = 80;
             }

             $consultaPPVdor_VM = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_VM' AND anio = '$anio' AND idVendedor = 6";
             $resultadoPPVdor_VM = mysqli_query($con, $consultaPPVdor_VM);
   
             while ($RowPPVdor_VM = mysqli_fetch_array($resultadoPPVdor_VM)) {
   
               $porceCumpliVdor_VM = $RowPPVdor_VM['porcentaje'];
                         
               if ($porceCumpliVdor_VM  > 100) {
                   $liq_ppto_messer_VM = 0.009;
               }elseif ($porceCumpliVdor_VM <= 79){
                   $liq_ppto_messer_VM = 0;
               }else {
                 $liq_ppto_messer_VM = 0.007;
               }
               //echo $numeRac_JJ." - ".$numeFac_JJ." - ".$beseComisionApro_JJ." - ".$dias_JJ." - ".$nomCliente_JJ." - ".$venDor_JJ." - ".$porceCumpliVdor_JJ." - ".$liq_ppto_messer_JJ." - ".$liq_rec_messer_JJ."<br>";
              
               $comision_PPTO_VM = $beseComisionApro_VM*$liq_ppto_messer_VM*($liq_rec_messer_VM/100);
               $Total_comision_PPTO_VM += $comision_PPTO_VM;

               //echo $comision_PPTO_JJ."<br>";

             }
          }     
         }elseif ($RowConso_VM['porcentaje'] == 10) {

          $nomCliente_VM = $RowConso_VM['cliente'];
     
          $codigo_VM = $RowConso_VM['codigo'];
          $length_VM = 9;
          $codigoV_CLI_VM = substr(str_repeat(0, $length_VM).$codigo_VM, - $length_VM);
     
          $numeRac_VM = $RowConso_VM['rec'];
          $numeFac_VM = $RowConso_VM['numero_']; 
          $beseComisionApro_VM = $RowConso_VM['base_trans'];
          $dias_VM = $RowConso_VM['dias'];
          $fecha_doc_VM = $RowConso_VM['fecha_doc'];
          $sacaMes_VM = date('m', strtotime($fecha_doc_VM));
          $sacaAnio_VM = date('Y', strtotime($fecha_doc_VM));
     
          //echo $nomCliente_JJ."<br>";
          
          $AsVddor_VM = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_VM'";
          $resultAsVddor_VM = mysqli_query($con, $AsVddor_VM);
     
          while ($rowVndor_VM = mysqli_fetch_array($resultAsVddor_VM)) {
                   $venDor_VM = $rowVndor_VM['vendedor']; 
             }
             if ($venDor_VM == 'VM') {                  
                 
                  if ($dias_VM <= 90) {
                    $liq_rec_messer_VM = 100;
                  }elseif ($dias_VM > 120) {
                    $liq_rec_messer_VM = 0;
                  }else {
                    $liq_rec_messer_VM = 80;
                  }
     
                  $consultaPPVdor_VM = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_VM' AND anio = '$anio' AND idVendedor = 6";
                  $resultadoPPVdor_VM = mysqli_query($con, $consultaPPVdor_VM);
        
                  while ($RowPPVdor_VM = mysqli_fetch_array($resultadoPPVdor_VM)) {
        
                    $porceCumpliVdor_VM = $RowPPVdor_VM['porcentaje'];
                              
                    if ($porceCumpliVdor_VM  > 100) {
                        $liq_ppto_messer_VM = 0.009;
                    }elseif ($porceCumpliVdor_VM <= 79) {
                        $liq_ppto_messer_VM = 0;
                    }else {
                      $liq_ppto_messer_VM = 0.007;
                    }
                    
                    //echo $numeRac_JJ." - ".$numeFac_JJ." - ".$beseComisionApro_JJ." - ".$dias_JJ." - ".$nomCliente_JJ." - ".$venDor_JJ." - ".$porceCumpliVdor_JJ." - ".$liq_ppto_messer_JJ." - ".$liq_rec_messer_JJ."<br>";
                   
                    $comision_PPTO_VM = $beseComisionApro_VM*$liq_ppto_messer_VM*($liq_rec_messer_VM/100)/3;
                    $Total_comision_PPTO_VM += $comision_PPTO_VM;
     
                    //echo $comision_PPTO_JJ."<br>";
     
                  }
               }

       }elseif ($RowConso_VM['porcentaje'] == 16.5) {

        $nomCliente_VM = $RowConso_VM['cliente'];
     
        $codigo_VM = $RowConso_VM['codigo'];
        $length_VM = 9;
        $codigoV_CLI_VM = substr(str_repeat(0, $length_VM).$codigo_VM, - $length_VM);
   
        $numeRac_VM = $RowConso_VM['rec'];
        $numeFac_VM = $RowConso_VM['numero_']; 
        $beseComisionApro_VM = $RowConso_VM['base_trans'];
        $dias_VM = $RowConso_VM['dias'];
        $fecha_doc_VM = $RowConso_VM['fecha_doc'];
        $sacaMes_VM = date('m', strtotime($fecha_doc_VM));
        $sacaAnio_VM = date('Y', strtotime($fecha_doc_VM));
   
        //echo $nomCliente_JJ."<br>";
        
        $AsVddor_VM = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_VM'";
        $resultAsVddor_VM = mysqli_query($con, $AsVddor_VM);
   
        while ($rowVndor_VM = mysqli_fetch_array($resultAsVddor_VM)) {
                 $venDor_VM = $rowVndor_VM['vendedor']; 
           }
           if ($venDor_VM == 'VM') {                  
               
                if ($dias_VM <= 90) {
                  $liq_rec_messer_VM = 100;
                }elseif ($dias_VM > 120) {
                  $liq_rec_messer_VM = 0;
                }else {
                  $liq_rec_messer_VM = 80;
                }
   
                $consultaPPVdor_VM = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_VM' AND anio = '$anio' AND idVendedor = 6";
                $resultadoPPVdor_VM = mysqli_query($con, $consultaPPVdor_VM);
      
                while ($RowPPVdor_VM = mysqli_fetch_array($resultadoPPVdor_VM)) {
      
                  $porceCumpliVdor_VM = $RowPPVdor_VM['porcentaje'];
                            
                  if ($porceCumpliVdor_VM  > 100) {
                      $liq_ppto_messer_VM = 0.009;
                  }elseif ($porceCumpliVdor_VM <= 79) {
                      $liq_ppto_messer_VM = 0;
                  }else {
                    $liq_ppto_messer_VM = 0.007;
                  }
                  
                  //echo $numeRac_JJ." - ".$numeFac_JJ." - ".$beseComisionApro_JJ." - ".$dias_JJ." - ".$nomCliente_JJ." - ".$venDor_JJ." - ".$porceCumpliVdor_JJ." - ".$liq_ppto_messer_JJ." - ".$liq_rec_messer_JJ."<br>";
                 
                  $comision_PPTO_VM = $beseComisionApro_VM*$liq_ppto_messer_VM*($liq_rec_messer_VM/100)/1.81;
                  $Total_comision_PPTO_VM += $comision_PPTO_VM;
   
                  //echo $comision_PPTO_JJ."<br>";
   
                }
             }

       }elseif ($RowConso_VM['porcentaje'] == 5) {

        $nomCliente_VM = $RowConso_VM['cliente'];
     
        $codigo_VM = $RowConso_VM['codigo'];
        $length_VM = 9;
        $codigoV_CLI_VM = substr(str_repeat(0, $length_VM).$codigo_VM, - $length_VM);
   
        $numeRac_VM = $RowConso_VM['rec'];
        $numeFac_VM = $RowConso_VM['numero_']; 
        $beseComisionApro_VM = $RowConso_VM['base_trans'];
        $dias_VM = $RowConso_VM['dias'];
        $fecha_doc_VM = $RowConso_VM['fecha_doc'];
        $sacaMes_VM = date('m', strtotime($fecha_doc_VM));
        $sacaAnio_VM = date('Y', strtotime($fecha_doc_VM));
   
        //echo $nomCliente_JJ."<br>";
        
        $AsVddor_VM = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_VM'";
        $resultAsVddor_VM = mysqli_query($con, $AsVddor_VM);
   
        while ($rowVndor_VM = mysqli_fetch_array($resultAsVddor_VM)) {
                 $venDor_VM = $rowVndor_VM['vendedor']; 
           }
           if ($venDor_VM == 'VM') {                  
               
                if ($dias_VM <= 90) {
                  $liq_rec_messer_VM = 100;
                }elseif ($dias_VM > 120) {
                  $liq_rec_messer_VM = 0;
                }else {
                  $liq_rec_messer_VM = 80;
                }
   
                $consultaPPVdor_VM = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_VM' AND anio = '$anio' AND idVendedor = 6";
                $resultadoPPVdor_VM = mysqli_query($con, $consultaPPVdor_VM);
      
                while ($RowPPVdor_VM = mysqli_fetch_array($resultadoPPVdor_VM)) {
      
                  $porceCumpliVdor_VM = $RowPPVdor_VM['porcentaje'];
                            
                  if ($porceCumpliVdor_VM  > 100) {
                      $liq_ppto_messer_VM = 0.009;
                  }elseif ($porceCumpliVdor_VM <= 79) {
                      $liq_ppto_messer_VM = 0;
                  }else {
                    $liq_ppto_messer_VM = 0.007;
                  }
                  
                  //echo $numeRac_JJ." - ".$numeFac_JJ." - ".$beseComisionApro_JJ." - ".$dias_JJ." - ".$nomCliente_JJ." - ".$venDor_JJ." - ".$porceCumpliVdor_JJ." - ".$liq_ppto_messer_JJ." - ".$liq_rec_messer_JJ."<br>";
                 
                  $comision_PPTO_VM = $beseComisionApro_VM*$liq_ppto_messer_VM*($liq_rec_messer_VM/100)/6;
                  $Total_comision_PPTO_VM += $comision_PPTO_VM;
   
                  //echo $comision_PPTO_JJ."<br>";
   
                }
             }

       }elseif ($rowVndor_VM['vendedor'] == 'VM' && $RowConso_VM['porcentaje'] == 20) {
         
        $nomCliente_VM = $RowConso_VM['cliente'];
     
        $codigo_VM = $RowConso_VM['codigo'];
        $length_VM = 9;
        $codigoV_CLI_VM = substr(str_repeat(0, $length_VM).$codigo_VM, - $length_VM);
   
        $numeRac_VM = $RowConso_VM['rec'];
        $numeFac_VM = $RowConso_VM['numero_']; 
        $beseComisionApro_VM = $RowConso_VM['base_trans'];
        $dias_VM = $RowConso_VM['dias'];
        $fecha_doc_VM = $RowConso_VM['fecha_doc'];
        $sacaMes_VM = date('m', strtotime($fecha_doc_VM));
        $sacaAnio_VM = date('Y', strtotime($fecha_doc_VM));
   
        //echo $nomCliente_JJ."<br>";
        
        $AsVddor_VM = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_VM'";
        $resultAsVddor_VM = mysqli_query($con, $AsVddor_VM);
   
        while ($rowVndor_VM = mysqli_fetch_array($resultAsVddor_VM)) {
                 $venDor_VM = $rowVndor_VM['vendedor']; 
           }
           if ($venDor_VM == 'VM') {                  
               
                if ($dias_VM <= 90) {
                  $liq_rec_messer_VM = 100;
                }elseif ($dias_VM > 120) {
                  $liq_rec_messer_VM = 0;
                }else {
                  $liq_rec_messer_VM = 80;
                }
   
                $consultaPPVdor_VM = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_VM' AND anio = '$anio' AND idVendedor = 6";
                $resultadoPPVdor_VM = mysqli_query($con, $consultaPPVdor_VM);
      
                while ($RowPPVdor_VM = mysqli_fetch_array($resultadoPPVdor_VM)) {
      
                  $porceCumpliVdor_VM = $RowPPVdor_VM['porcentaje'];
                            
                  if ($porceCumpliVdor_VM  > 100) {
                      $liq_ppto_messer_VM = 0.009;
                  }elseif ($porceCumpliVdor_VM <= 79) {
                      $liq_ppto_messer_VM = 0;
                  }else {
                    $liq_ppto_messer_VM = 0.007;
                  }
                  
                  //echo $numeRac_JJ." - ".$numeFac_JJ." - ".$beseComisionApro_JJ." - ".$dias_JJ." - ".$nomCliente_JJ." - ".$venDor_JJ." - ".$porceCumpliVdor_JJ." - ".$liq_ppto_messer_JJ." - ".$liq_rec_messer_JJ."<br>";
                 
                  $comision_PPTO_VM = $beseComisionApro_VM*$liq_ppto_messer_VM*($liq_rec_messer_VM/100)/1.5;
                  $Total_comision_PPTO_VM += $comision_PPTO_VM;
   
                  //echo $comision_PPTO_JJ."<br>";
   
                }
             }
      }
}
//TRMINA CALCULO COMISIONES MESSER TOBERIN................................................................................................................................................................................................  

//INICIA CALCULO COMISIONES MESSER CAZUCA................................................................................................................................................................................................
$consultaConso_VM_060 = "SELECT * FROM comisiones_cazuca WHERE mes = '$mesC'";
$resultadoConso_VM_060 = mysqli_query($con, $consultaConso_VM_060);

while ($RowConso_VM_060 = mysqli_fetch_array($resultadoConso_VM_060)) {       
if ($RowConso_VM_060['dias'] != '' && $RowConso_VM_060['porc_3'] != '' && $RowConso_VM_060['porc_4'] != '') {
  $nomCliente_VM_060 = $RowConso_VM_060['cliente'];

  $codigo_VM_060 = $RowConso_VM_060['codigo'];
  $length_VM_060 = 9;
  $codigoV_CLI_VM_060 = substr(str_repeat(0, $length_VM_060).$codigo_VM_060, - $length_VM_060);

  $numeRac_VM_060 = $RowConso_VM_060['recibo'];
  $numeFac_VM_060 = $RowConso_VM_060['numero']; 
  $beseComisionApro_VM_060 = $RowConso_VM_060['base_com'];
  $dias_VM_060 = $RowConso_VM_060['dias'];
  $fecha_doc_VM_060 = $RowConso_VM_060['fecha_doc'];
  $sacaMes_VM_060 = date('m', strtotime($fecha_doc_VM_060));
  $sacaMes_VM_060 = date('Y', strtotime($fecha_doc_VM_060));

  //echo $nomCliente_VM."<br>";
  
  $AsVddor_VM_060 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_VM_060'";
  $resultAsVddor_VM_060 = mysqli_query($con, $AsVddor_VM_060);

  while ($rowVndor_VM_060 = mysqli_fetch_array($resultAsVddor_VM_060)) {
    $venDor_VM_060 = $rowVndor_VM_060['vendedor']; 
  }
     if ($venDor_VM_060 == 'VM') {                 
        
          if ($dias_VM_060 <= 90) {
            $liq_rec_messer_VM_060 = 100;
          }elseif ($dias_VM_060 > 120) {
            $liq_rec_messer_VM_060 = 0;
          }else {
            $liq_rec_messer_VM_060 = 80;
          }

          $consultaPPVdor_VM_060 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_VM_060' AND anio = '$anio' AND idVendedor = 6";
          $resultadoPPVdor_VM_060 = mysqli_query($con, $consultaPPVdor_VM_060);

          while ($RowPPVdor_VM_060 = mysqli_fetch_array($resultadoPPVdor_VM_060)) {

            $porceCumpliVdor_VM_060 = $RowPPVdor_VM_060['porcentaje'];
                      
            if ($porceCumpliVdor_VM_060  > 100) {
                $liq_ppto_messer_VM_060 = 0.009;
            }elseif ($porceCumpliVdor_VM_060 <= 79) {
                $liq_ppto_messer_VM_060 = 0;
            }else {
              $liq_ppto_messer_VM_060 = 0.007;
            }
            
            //echo $numeRac_JJ." - ".$numeFac_JJ." - ".$beseComisionApro_JJ." - ".$dias_JJ." - ".$nomCliente_JJ." - ".$venDor_JJ." - ".$porceCumpliVdor_JJ." - ".$liq_ppto_messer_JJ." - ".$liq_rec_messer_JJ."<br>";
           
            $comision_PPTO_VM_060 = $beseComisionApro_VM_060*$liq_ppto_messer_VM_060*($liq_rec_messer_VM_060/100);
            $Total_comision_PPTO_VM_060 += $comision_PPTO_VM_060;

            //echo $comision_PPTO_VM_060."<br>";

          }
        }     
      }elseif ($RowConso_VM_060['porcentaje'] == 10) {

        $nomCliente_VM_060 = $RowConso_VM_060['cliente'];

  $codigo_VM_060 = $RowConso_VM_060['codigo'];
  $length_VM_060 = 9;
  $codigoV_CLI_VM_060 = substr(str_repeat(0, $length_VM_060).$codigo_VM_060, - $length_VM_060);

  $numeRac_VM_060 = $RowConso_VM_060['recibo'];
  $numeFac_VM_060 = $RowConso_VM_060['numero']; 
  $beseComisionApro_VM_060 = $RowConso_VM_060['base_com'];
  $dias_VM_060 = $RowConso_VM_060['dias'];
  $fecha_doc_VM_060 = $RowConso_VM_060['fecha_doc'];
  $sacaMes_VM_060 = date('m', strtotime($fecha_doc_VM_060));
  $sacaMes_VM_060 = date('Y', strtotime($fecha_doc_VM_060));

  //echo $nomCliente_VM."<br>";
  
  $AsVddor_VM_060 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_VM_060'";
  $resultAsVddor_VM_060 = mysqli_query($con, $AsVddor_VM_060);

  while ($rowVndor_VM_060 = mysqli_fetch_array($resultAsVddor_VM_060)) {
    $venDor_VM_060 = $rowVndor_VM_060['vendedor']; 
  }
     if ($venDor_VM_060 == 'VM') {                 
        
          if ($dias_VM_060 <= 90) {
            $liq_rec_messer_VM_060 = 100;
          }elseif ($dias_VM_060 > 120) {
            $liq_rec_messer_VM_060 = 0;
          }else {
            $liq_rec_messer_VM_060 = 80;
          }

          $consultaPPVdor_VM_060 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_VM_060' AND anio = '$anio' AND idVendedor = 6";
          $resultadoPPVdor_VM_060 = mysqli_query($con, $consultaPPVdor_VM_060);

          while ($RowPPVdor_VM_060 = mysqli_fetch_array($resultadoPPVdor_VM_060)) {

            $porceCumpliVdor_VM_060 = $RowPPVdor_VM_060['porcentaje'];
                      
            if ($porceCumpliVdor_VM_060  > 100) {
                $liq_ppto_messer_VM_060 = 0.009;
            }elseif ($porceCumpliVdor_VM_060 <= 79) {
                $liq_ppto_messer_VM_060 = 0;
            }else {
              $liq_ppto_messer_VM_060 = 0.007;
            }
            
            //echo $numeRac_JJ." - ".$numeFac_JJ." - ".$beseComisionApro_JJ." - ".$dias_JJ." - ".$nomCliente_JJ." - ".$venDor_JJ." - ".$porceCumpliVdor_JJ." - ".$liq_ppto_messer_JJ." - ".$liq_rec_messer_JJ."<br>";
           
            $comision_PPTO_VM_060 = $beseComisionApro_VM_060*$liq_ppto_messer_VM_060*($liq_rec_messer_VM_060/100)/3;
            $Total_comision_PPTO_VM_060 += $comision_PPTO_VM_060;

            //echo $comision_PPTO_VM_060."<br>";

          }
        }

    }elseif ($RowConso_VM_060['porcentaje'] == 16.5) {

      $nomCliente_VM_060 = $RowConso_VM_060['cliente'];

  $codigo_VM_060 = $RowConso_VM_060['codigo'];
  $length_VM_060 = 9;
  $codigoV_CLI_VM_060 = substr(str_repeat(0, $length_VM_060).$codigo_VM_060, - $length_VM_060);

  $numeRac_VM_060 = $RowConso_VM_060['recibo'];
  $numeFac_VM_060 = $RowConso_VM_060['numero']; 
  $beseComisionApro_VM_060 = $RowConso_VM_060['base_com'];
  $dias_VM_060 = $RowConso_VM_060['dias'];
  $fecha_doc_VM_060 = $RowConso_VM_060['fecha_doc'];
  $sacaMes_VM_060 = date('m', strtotime($fecha_doc_VM_060));
  $sacaMes_VM_060 = date('Y', strtotime($fecha_doc_VM_060));

  //echo $nomCliente_VM."<br>";
  
  $AsVddor_VM_060 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_VM_060'";
  $resultAsVddor_VM_060 = mysqli_query($con, $AsVddor_VM_060);

  while ($rowVndor_VM_060 = mysqli_fetch_array($resultAsVddor_VM_060)) {
    $venDor_VM_060 = $rowVndor_VM_060['vendedor']; 
  }
     if ($venDor_VM_060 == 'VM') {                 
        
          if ($dias_VM_060 <= 90) {
            $liq_rec_messer_VM_060 = 100;
          }elseif ($dias_VM_060 > 120) {
            $liq_rec_messer_VM_060 = 0;
          }else {
            $liq_rec_messer_VM_060 = 80;
          }

          $consultaPPVdor_VM_060 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_VM_060' AND anio = '$anio' AND idVendedor = 6";
          $resultadoPPVdor_VM_060 = mysqli_query($con, $consultaPPVdor_VM_060);

          while ($RowPPVdor_VM_060 = mysqli_fetch_array($resultadoPPVdor_VM_060)) {

            $porceCumpliVdor_VM_060 = $RowPPVdor_VM_060['porcentaje'];
                      
            if ($porceCumpliVdor_VM_060  > 100) {
                $liq_ppto_messer_VM_060 = 0.009;
            }elseif ($porceCumpliVdor_VM_060 <= 79) {
                $liq_ppto_messer_VM_060 = 0;
            }else {
              $liq_ppto_messer_VM_060 = 0.007;
            }
            
            //echo $numeRac_JJ." - ".$numeFac_JJ." - ".$beseComisionApro_JJ." - ".$dias_JJ." - ".$nomCliente_JJ." - ".$venDor_JJ." - ".$porceCumpliVdor_JJ." - ".$liq_ppto_messer_JJ." - ".$liq_rec_messer_JJ."<br>";
           
            $comision_PPTO_VM_060 = $beseComisionApro_VM_060*$liq_ppto_messer_VM_060*($liq_rec_messer_VM_060/100)/1.81;
            $Total_comision_PPTO_VM_060 += $comision_PPTO_VM_060;

            //echo $comision_PPTO_VM_060."<br>";

          }
        }

    }elseif ($RowConso_VM_060['porcentaje'] == 5) {

      $nomCliente_VM_060 = $RowConso_VM_060['cliente'];

  $codigo_VM_060 = $RowConso_VM_060['codigo'];
  $length_VM_060 = 9;
  $codigoV_CLI_VM_060 = substr(str_repeat(0, $length_VM_060).$codigo_VM_060, - $length_VM_060);

  $numeRac_VM_060 = $RowConso_VM_060['recibo'];
  $numeFac_VM_060 = $RowConso_VM_060['numero']; 
  $beseComisionApro_VM_060 = $RowConso_VM_060['base_com'];
  $dias_VM_060 = $RowConso_VM_060['dias'];
  $fecha_doc_VM_060 = $RowConso_VM_060['fecha_doc'];
  $sacaMes_VM_060 = date('m', strtotime($fecha_doc_VM_060));
  $sacaMes_VM_060 = date('Y', strtotime($fecha_doc_VM_060));

  //echo $nomCliente_VM."<br>";
  
  $AsVddor_VM_060 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_VM_060'";
  $resultAsVddor_VM_060 = mysqli_query($con, $AsVddor_VM_060);

  while ($rowVndor_VM_060 = mysqli_fetch_array($resultAsVddor_VM_060)) {
    $venDor_VM_060 = $rowVndor_VM_060['vendedor']; 
  }
     if ($venDor_VM_060 == 'VM') {                 
        
          if ($dias_VM_060 <= 90) {
            $liq_rec_messer_VM_060 = 100;
          }elseif ($dias_VM_060 > 120) {
            $liq_rec_messer_VM_060 = 0;
          }else {
            $liq_rec_messer_VM_060 = 80;
          }

          $consultaPPVdor_VM_060 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_VM_060' AND anio = '$anio' AND idVendedor = 6";
          $resultadoPPVdor_VM_060 = mysqli_query($con, $consultaPPVdor_VM_060);

          while ($RowPPVdor_VM_060 = mysqli_fetch_array($resultadoPPVdor_VM_060)) {

            $porceCumpliVdor_VM_060 = $RowPPVdor_VM_060['porcentaje'];
                      
            if ($porceCumpliVdor_VM_060  > 100) {
                $liq_ppto_messer_VM_060 = 0.009;
            }elseif ($porceCumpliVdor_VM_060 <= 79) {
                $liq_ppto_messer_VM_060 = 0;
            }else {
              $liq_ppto_messer_VM_060 = 0.007;
            }
            
            //echo $numeRac_JJ." - ".$numeFac_JJ." - ".$beseComisionApro_JJ." - ".$dias_JJ." - ".$nomCliente_JJ." - ".$venDor_JJ." - ".$porceCumpliVdor_JJ." - ".$liq_ppto_messer_JJ." - ".$liq_rec_messer_JJ."<br>";
           
            $comision_PPTO_VM_060 = $beseComisionApro_VM_060*$liq_ppto_messer_VM_060*($liq_rec_messer_VM_060/100)/6;
            $Total_comision_PPTO_VM_060 += $comision_PPTO_VM_060;

            //echo $comision_PPTO_VM_060."<br>";

          }
        }

    }elseif ($RowConso_VM_060['porcentaje'] == 20) {
      
      $nomCliente_VM_060 = $RowConso_VM_060['cliente'];

      $codigo_VM_060 = $RowConso_VM_060['codigo'];
      $length_VM_060 = 9;
      $codigoV_CLI_VM_060 = substr(str_repeat(0, $length_VM_060).$codigo_VM_060, - $length_VM_060);
    
      $numeRac_VM_060 = $RowConso_VM_060['recibo'];
      $numeFac_VM_060 = $RowConso_VM_060['numero']; 
      $beseComisionApro_VM_060 = $RowConso_VM_060['base_com'];
      $dias_VM_060 = $RowConso_VM_060['dias'];
      $fecha_doc_VM_060 = $RowConso_VM_060['fecha_doc'];
      $sacaMes_VM_060 = date('m', strtotime($fecha_doc_VM_060));
      $sacaMes_VM_060 = date('Y', strtotime($fecha_doc_VM_060));
    
      //echo $nomCliente_VM."<br>";
      
      $AsVddor_VM_060 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_VM_060'";
      $resultAsVddor_VM_060 = mysqli_query($con, $AsVddor_VM_060);
    
      while ($rowVndor_VM_060 = mysqli_fetch_array($resultAsVddor_VM_060)) {
        $venDor_VM_060 = $rowVndor_VM_060['vendedor']; 
      }
         if ($venDor_VM_060 == 'VM') {                 
            
              if ($dias_VM_060 <= 90) {
                $liq_rec_messer_VM_060 = 100;
              }elseif ($dias_VM_060 > 120) {
                $liq_rec_messer_VM_060 = 0;
              }else {
                $liq_rec_messer_VM_060 = 80;
              }
    
              $consultaPPVdor_VM_060 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_VM_060' AND anio = '$anio' AND idVendedor = 6";
              $resultadoPPVdor_VM_060 = mysqli_query($con, $consultaPPVdor_VM_060);
    
              while ($RowPPVdor_VM_060 = mysqli_fetch_array($resultadoPPVdor_VM_060)) {
    
                $porceCumpliVdor_VM_060 = $RowPPVdor_VM_060['porcentaje'];
                          
                if ($porceCumpliVdor_VM_060  > 100) {
                    $liq_ppto_messer_VM_060 = 0.009;
                }elseif ($porceCumpliVdor_VM_060 <= 79) {
                    $liq_ppto_messer_VM_060 = 0;
                }else {
                  $liq_ppto_messer_VM_060 = 0.007;
                }
                
                //echo $numeRac_JJ." - ".$numeFac_JJ." - ".$beseComisionApro_JJ." - ".$dias_JJ." - ".$nomCliente_JJ." - ".$venDor_JJ." - ".$porceCumpliVdor_JJ." - ".$liq_ppto_messer_JJ." - ".$liq_rec_messer_JJ."<br>";
               
                $comision_PPTO_VM_060 = $beseComisionApro_VM_060*$liq_ppto_messer_VM_060*($liq_rec_messer_VM_060/100)/1.5;
                $Total_comision_PPTO_VM_060 += $comision_PPTO_VM_060;
    
                //echo $comision_PPTO_VM_060."<br>";
    
              }
            }
      }
  }
//TRMINA CALCULO COMISIONES MESSER CAZUCA................................................................................................................................................................................................

//INICIA CALCULO COMISIONES MESSER MANIZALES................................................................................................................................................................................................
$consultaConso_VM_370 = "SELECT * FROM comisiones_manizales WHERE mes = '$mesC'";
$resultadoConso_VM_370 = mysqli_query($con, $consultaConso_VM_370);

while ($RowConso_VM_370 = mysqli_fetch_array($resultadoConso_VM_370)) {       
if ($RowConso_VM_370['dias_transcurridos'] != '' && $RowConso_VM_370['porc_3'] != '' && $RowConso_VM_370['porc_4'] != '') {
$nomCliente_VM_370 = $RowConso_VM_370['cliente'];

$codigo_VM_370 = $RowConso_VM_370['codigo_cliente'];
$length_VM_370 = 9;
$codigoV_CLI_VM_370 = substr(str_repeat(0, $length_VM_370).$codigo_VM_370, - $length_VM_370);

$numeRac_VM_370 = $RowConso_VM_370['rec'];
$numeFac_VM_370 = $RowConso_VM_370['no_fact']; 
$beseComisionApro_VM_370 = $RowConso_VM_370['base_com'];
$dias_VM_370 = $RowConso_VM_370['dias_transcurridos'];
$fecha_doc_VM_370 = $RowConso_VM_370['fecha_doc'];
$sacaMes_VM_370 = date('m', strtotime($fecha_doc_VM_370));

//echo $nomCliente_VM_060."<br>";

$AsVddor_VM_370 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_VM_370'";
$resultAsVddor_VM_370 = mysqli_query($con, $AsVddor_VM_370);

while ($rowVndor_VM_370 = mysqli_fetch_array($resultAsVddor_VM_370)) {
      $venDor_VM_370 = $rowVndor_VM_370['vendedor'];
  }

   if ($venDor_VM_370 == 'VM') {                 
       
        if ($dias_VM_370 <= 90) {
          $liq_rec_messer_VM_370 = 100;
        }elseif ($dias_VM_370 > 120) {
          $liq_rec_messer_VM_370 = 0;
        }else {
          $liq_rec_messer_VM_370 = 80;
        }

        $consultaPPVdor_VM_370 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_VM_370' AND anio = '$anio' AND idVendedor = 6";
        $resultadoPPVdor_VM_370 = mysqli_query($con, $consultaPPVdor_VM_370);

        while ($RowPPVdor_VM_370 = mysqli_fetch_array($resultadoPPVdor_VM_370)) {

          $porceCumpliVdor_VM_370 = $RowPPVdor_VM_370['porcentaje'];
                    
          if ($porceCumpliVdor_VM_370  > 100) {
              $liq_ppto_messer_VM_370 = 0.009;
          }elseif ($porceCumpliVdor_VM_370 <= 79) {
              $liq_ppto_messer_VM_370 = 0;
          }else {
            $liq_ppto_messer_VM_370 = 0.007;
          }
          
          //echo $numeRac_VM_060." - ".$numeFac_VM_060." - ".$beseComisionApro_VM_060." - ".$dias_VM_060." - ".$nomCliente_VM_060." - ".$venDor_VM_060." - ".$porceCumpliVdor_VM_060." - ".$liq_ppto_messer_VM_060." - ".$liq_rec_messer_VM_060."<br>";
         
          $comision_PPTO_VM_370 = $beseComisionApro_VM_370*$liq_ppto_messer_VM_370*($liq_rec_messer_VM_370/100);
          $Total_comision_PPTO_VM_370 += $comision_PPTO_VM_370;

          //echo $comision_PPTO_VM_060."<br>";

        }
      }    
    }elseif ($RowConso_VM_370['porcentaje'] == 10) {

      $nomCliente_VM_370 = $RowConso_VM_370['cliente'];

      $codigo_VM_370 = $RowConso_VM_370['codigo_cliente'];
      $length_VM_370 = 9;
      $codigoV_CLI_VM_370 = substr(str_repeat(0, $length_VM_370).$codigo_VM_370, - $length_VM_370);
      
      $numeRac_VM_370 = $RowConso_VM_370['rec'];
      $numeFac_VM_370 = $RowConso_VM_370['no_fact']; 
      $beseComisionApro_VM_370 = $RowConso_VM_370['base_com'];
      $dias_VM_370 = $RowConso_VM_370['dias_transcurridos'];
      $fecha_doc_VM_370 = $RowConso_VM_370['fecha_doc'];
      $sacaMes_VM_370 = date('m', strtotime($fecha_doc_VM_370));
      
      //echo $nomCliente_VM_060."<br>";
      
      $AsVddor_VM_370 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_VM_370'";
      $resultAsVddor_VM_370 = mysqli_query($con, $AsVddor_VM_370);
      
      while ($rowVndor_VM_370 = mysqli_fetch_array($resultAsVddor_VM_370)) {
            $venDor_VM_370 = $rowVndor_VM_370['vendedor'];
        }
      
         if ($venDor_VM_370 == 'VM') {                 
             
              if ($dias_VM_370 <= 90) {
                $liq_rec_messer_VM_370 = 100;
              }elseif ($dias_VM_370 > 120) {
                $liq_rec_messer_VM_370 = 0;
              }else {
                $liq_rec_messer_VM_370 = 80;
              }
      
              $consultaPPVdor_VM_370 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_VM_370' AND anio = '$anio' AND idVendedor = 6";
              $resultadoPPVdor_VM_370 = mysqli_query($con, $consultaPPVdor_VM_370);
      
              while ($RowPPVdor_VM_370 = mysqli_fetch_array($resultadoPPVdor_VM_370)) {
      
                $porceCumpliVdor_VM_370 = $RowPPVdor_VM_370['porcentaje'];
                          
                if ($porceCumpliVdor_VM_370  > 100) {
                    $liq_ppto_messer_VM_370 = 0.009;
                }elseif ($porceCumpliVdor_VM_370 <= 79) {
                    $liq_ppto_messer_VM_370 = 0;
                }else {
                  $liq_ppto_messer_VM_370 = 0.007;
                }
                
                //echo $numeRac_VM_060." - ".$numeFac_VM_060." - ".$beseComisionApro_VM_060." - ".$dias_VM_060." - ".$nomCliente_VM_060." - ".$venDor_VM_060." - ".$porceCumpliVdor_VM_060." - ".$liq_ppto_messer_VM_060." - ".$liq_rec_messer_VM_060."<br>";
               
                $comision_PPTO_VM_370 = $beseComisionApro_VM_370*$liq_ppto_messer_VM_370*($liq_rec_messer_VM_370/100)/3;
                $Total_comision_PPTO_VM_370 += $comision_PPTO_VM_370;
      
                //echo $comision_PPTO_VM_060."<br>";
      
              }
            }

  }elseif ($RowConso_VM_370['porcentaje'] == 16.5) {

    $nomCliente_VM_370 = $RowConso_VM_370['cliente'];

    $codigo_VM_370 = $RowConso_VM_370['codigo_cliente'];
    $length_VM_370 = 9;
    $codigoV_CLI_VM_370 = substr(str_repeat(0, $length_VM_370).$codigo_VM_370, - $length_VM_370);
    
    $numeRac_VM_370 = $RowConso_VM_370['rec'];
    $numeFac_VM_370 = $RowConso_VM_370['no_fact']; 
    $beseComisionApro_VM_370 = $RowConso_VM_370['base_com'];
    $dias_VM_370 = $RowConso_VM_370['dias_transcurridos'];
    $fecha_doc_VM_370 = $RowConso_VM_370['fecha_doc'];
    $sacaMes_VM_370 = date('m', strtotime($fecha_doc_VM_370));
    
    //echo $nomCliente_VM_060."<br>";
    
    $AsVddor_VM_370 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_VM_370'";
    $resultAsVddor_VM_370 = mysqli_query($con, $AsVddor_VM_370);
    
    while ($rowVndor_VM_370 = mysqli_fetch_array($resultAsVddor_VM_370)) {
          $venDor_VM_370 = $rowVndor_VM_370['vendedor'];
      }
    
       if ($venDor_VM_370 == 'VM') {                 
           
            if ($dias_VM_370 <= 90) {
              $liq_rec_messer_VM_370 = 100;
            }elseif ($dias_VM_370 > 120) {
              $liq_rec_messer_VM_370 = 0;
            }else {
              $liq_rec_messer_VM_370 = 80;
            }
    
            $consultaPPVdor_VM_370 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_VM_370' AND anio = '$anio' AND idVendedor = 6";
            $resultadoPPVdor_VM_370 = mysqli_query($con, $consultaPPVdor_VM_370);
    
            while ($RowPPVdor_VM_370 = mysqli_fetch_array($resultadoPPVdor_VM_370)) {
    
              $porceCumpliVdor_VM_370 = $RowPPVdor_VM_370['porcentaje'];
                        
              if ($porceCumpliVdor_VM_370  > 100) {
                  $liq_ppto_messer_VM_370 = 0.009;
              }elseif ($porceCumpliVdor_VM_370 <= 79) {
                  $liq_ppto_messer_VM_370 = 0;
              }else {
                $liq_ppto_messer_VM_370 = 0.007;
              }
              
              //echo $numeRac_VM_060." - ".$numeFac_VM_060." - ".$beseComisionApro_VM_060." - ".$dias_VM_060." - ".$nomCliente_VM_060." - ".$venDor_VM_060." - ".$porceCumpliVdor_VM_060." - ".$liq_ppto_messer_VM_060." - ".$liq_rec_messer_VM_060."<br>";
             
              $comision_PPTO_VM_370 = $beseComisionApro_VM_370*$liq_ppto_messer_VM_370*($liq_rec_messer_VM_370/100)/1.81;
              $Total_comision_PPTO_VM_370 += $comision_PPTO_VM_370;
    
              //echo $comision_PPTO_VM_060."<br>";
    
            }
          }

  }elseif ($RowConso_VM_370['porcentaje'] == 5) {

    $nomCliente_VM_370 = $RowConso_VM_370['cliente'];

    $codigo_VM_370 = $RowConso_VM_370['codigo_cliente'];
    $length_VM_370 = 9;
    $codigoV_CLI_VM_370 = substr(str_repeat(0, $length_VM_370).$codigo_VM_370, - $length_VM_370);
    
    $numeRac_VM_370 = $RowConso_VM_370['rec'];
    $numeFac_VM_370 = $RowConso_VM_370['no_fact']; 
    $beseComisionApro_VM_370 = $RowConso_VM_370['base_com'];
    $dias_VM_370 = $RowConso_VM_370['dias_transcurridos'];
    $fecha_doc_VM_370 = $RowConso_VM_370['fecha_doc'];
    $sacaMes_VM_370 = date('m', strtotime($fecha_doc_VM_370));
    
    //echo $nomCliente_VM_060."<br>";
    
    $AsVddor_VM_370 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_VM_370'";
    $resultAsVddor_VM_370 = mysqli_query($con, $AsVddor_VM_370);
    
    while ($rowVndor_VM_370 = mysqli_fetch_array($resultAsVddor_VM_370)) {
          $venDor_VM_370 = $rowVndor_VM_370['vendedor'];
      }
    
       if ($venDor_VM_370 == 'VM') {                 
           
            if ($dias_VM_370 <= 90) {
              $liq_rec_messer_VM_370 = 100;
            }elseif ($dias_VM_370 > 120) {
              $liq_rec_messer_VM_370 = 0;
            }else {
              $liq_rec_messer_VM_370 = 80;
            }
    
            $consultaPPVdor_VM_370 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_VM_370' AND anio = '$anio' AND idVendedor = 6";
            $resultadoPPVdor_VM_370 = mysqli_query($con, $consultaPPVdor_VM_370);
    
            while ($RowPPVdor_VM_370 = mysqli_fetch_array($resultadoPPVdor_VM_370)) {
    
              $porceCumpliVdor_VM_370 = $RowPPVdor_VM_370['porcentaje'];
                        
              if ($porceCumpliVdor_VM_370  > 100) {
                  $liq_ppto_messer_VM_370 = 0.009;
              }elseif ($porceCumpliVdor_VM_370 <= 79) {
                  $liq_ppto_messer_VM_370 = 0;
              }else {
                $liq_ppto_messer_VM_370 = 0.007;
              }
              
              //echo $numeRac_VM_060." - ".$numeFac_VM_060." - ".$beseComisionApro_VM_060." - ".$dias_VM_060." - ".$nomCliente_VM_060." - ".$venDor_VM_060." - ".$porceCumpliVdor_VM_060." - ".$liq_ppto_messer_VM_060." - ".$liq_rec_messer_VM_060."<br>";
             
              $comision_PPTO_VM_370 = $beseComisionApro_VM_370*$liq_ppto_messer_VM_370*($liq_rec_messer_VM_370/100)/6;
              $Total_comision_PPTO_VM_370 += $comision_PPTO_VM_370;
    
              //echo $comision_PPTO_VM_060."<br>";
    
            }
          }

  }elseif ($RowConso_VM_370['porcentaje'] == 20) {
    
    $nomCliente_VM_370 = $RowConso_VM_370['cliente'];

    $codigo_VM_370 = $RowConso_VM_370['codigo_cliente'];
    $length_VM_370 = 9;
    $codigoV_CLI_VM_370 = substr(str_repeat(0, $length_VM_370).$codigo_VM_370, - $length_VM_370);
    
    $numeRac_VM_370 = $RowConso_VM_370['rec'];
    $numeFac_VM_370 = $RowConso_VM_370['no_fact']; 
    $beseComisionApro_VM_370 = $RowConso_VM_370['base_com'];
    $dias_VM_370 = $RowConso_VM_370['dias_transcurridos'];
    $fecha_doc_VM_370 = $RowConso_VM_370['fecha_doc'];
    $sacaMes_VM_370 = date('m', strtotime($fecha_doc_VM_370));
    
    //echo $nomCliente_VM_060."<br>";
    
    $AsVddor_VM_370 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_VM_370'";
    $resultAsVddor_VM_370 = mysqli_query($con, $AsVddor_VM_370);
    
    while ($rowVndor_VM_370 = mysqli_fetch_array($resultAsVddor_VM_370)) {
          $venDor_VM_370 = $rowVndor_VM_370['vendedor'];
      }
    
       if ($venDor_VM_370 == 'VM') {                 
           
            if ($dias_VM_370 <= 90) {
              $liq_rec_messer_VM_370 = 100;
            }elseif ($dias_VM_370 > 120) {
              $liq_rec_messer_VM_370 = 0;
            }else {
              $liq_rec_messer_VM_370 = 80;
            }
    
            $consultaPPVdor_VM_370 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_VM_370' AND anio = '$anio' AND idVendedor = 6";
            $resultadoPPVdor_VM_370 = mysqli_query($con, $consultaPPVdor_VM_370);
    
            while ($RowPPVdor_VM_370 = mysqli_fetch_array($resultadoPPVdor_VM_370)) {
    
              $porceCumpliVdor_VM_370 = $RowPPVdor_VM_370['porcentaje'];
                        
              if ($porceCumpliVdor_VM_370  > 100) {
                  $liq_ppto_messer_VM_370 = 0.009;
              }elseif ($porceCumpliVdor_VM_370 <= 79) {
                  $liq_ppto_messer_VM_370 = 0;
              }else {
                $liq_ppto_messer_VM_370 = 0.007;
              }
              
              //echo $numeRac_VM_060." - ".$numeFac_VM_060." - ".$beseComisionApro_VM_060." - ".$dias_VM_060." - ".$nomCliente_VM_060." - ".$venDor_VM_060." - ".$porceCumpliVdor_VM_060." - ".$liq_ppto_messer_VM_060." - ".$liq_rec_messer_VM_060."<br>";
             
              $comision_PPTO_VM_370 = $beseComisionApro_VM_370*$liq_ppto_messer_VM_370*($liq_rec_messer_VM_370/100)/1.5;
              $Total_comision_PPTO_VM_370 += $comision_PPTO_VM_370;
    
              //echo $comision_PPTO_VM_060."<br>";
    
            }
          }

    }
}
//TRMINA CALCULO COMISIONES MESSER MANIZALES................................................................................................................................................................................................

//INICIA CALCULO COMISIONES MESSER ITAGUI................................................................................................................................................................................................
$consultaConso_VM_590 = "SELECT * FROM comisiones_itagui WHERE mes = '$mesC'";
$resultadoConso_VM_590 = mysqli_query($con, $consultaConso_VM_590);

while ($RowConso_VM_590 = mysqli_fetch_array($resultadoConso_VM_590)) {       
if ($RowConso_VM_590['dias'] != '' && $RowConso_VM_590['porc_3'] != '' && $RowConso_VM_590['porc_4'] != '') {
$nomCliente_VM_590 = $RowConso_VM_590['cliente'];

$codigo_VM_590 = $RowConso_VM_590['codigo'];
$length_VM_590 = 9;
$codigoV_CLI_VM_590 = substr(str_repeat(0, $length_VM_590).$codigo_VM_590, - $length_VM_590);

$numeRac_VM_590 = $RowConso_VM_590['recibo'];
$numeFac_VM_590 = $RowConso_VM_590['numero_']; 
$beseComisionApro_VM_590 = $RowConso_VM_590['base_com'];
$dias_VM_590 = $RowConso_VM_590['dias'];
$fecha_doc_VM_590 = $RowConso_VM_590['fecha_doc'];
$sacaMes_VM_590 = date('m', strtotime($fecha_doc_VM_590));

//echo $nomCliente_VM_370."<br>";

$AsVddor_VM_590 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_VM_590'";
$resultAsVddor_VM_590 = mysqli_query($con, $AsVddor_VM_590);

while ($rowVndor_VM_590 = mysqli_fetch_array($resultAsVddor_VM_590)) {
  $venDor_VM_590 = $rowVndor_VM_590['vendedor'];  
}
   if ($venDor_VM_590 == 'VM') {                  
        
        if ($dias_VM_590 <= 90) {
          $liq_rec_messer_VM_590 = 100;
        }elseif ($dias_VM_590 > 120) {
          $liq_rec_messer_VM_590 = 0;
        }else {
          $liq_rec_messer_VM_590 = 80;
        }

        $consultaPPVdor_VM_590 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_VM_590' AND anio = '$anio' AND idVendedor = 6";
        $resultadoPPVdor_VM_590 = mysqli_query($con, $consultaPPVdor_VM_590);

        while ($RowPPVdor_VM_590 = mysqli_fetch_array($resultadoPPVdor_VM_590)) {

          $porceCumpliVdor_VM_590 = $RowPPVdor_VM_590['porcentaje'];
                    
          if ($porceCumpliVdor_VM_590  > 100) {
              $liq_ppto_messer_VM_590 = 0.009;
          }elseif ($porceCumpliVdor_VM_590 <= 79) {
              $liq_ppto_messer_VM_590 = 0;
          }else {
            $liq_ppto_messer_VM_590 = 0.007;
          }
          
          //echo $numeRac_VM_370." - ".$numeFac_VM_370." - ".$beseComisionApro_VM_370." - ".$dias_VM_370." - ".$nomCliente_VM_370." - ".$venDor_VM_370." - ".$porceCumpliVdor_VM_370." - ".$liq_ppto_messer_VM_370." - ".$liq_rec_messer_VM_370."<br>";
         
          $comision_PPTO_VM_590 = $beseComisionApro_VM_590*$liq_ppto_messer_VM_590*($liq_rec_messer_VM_590/100);
          $Total_comision_PPTO_VM_590 += $comision_PPTO_VM_590;

          //echo $comision_PPTO_VM_370."<br>";

        }
      }     
    }elseif ($RowConso_VM_590['porcentaje'] == 10) {

      $nomCliente_VM_590 = $RowConso_VM_590['cliente'];

$codigo_VM_590 = $RowConso_VM_590['codigo'];
$length_VM_590 = 9;
$codigoV_CLI_VM_590 = substr(str_repeat(0, $length_VM_590).$codigo_VM_590, - $length_VM_590);

$numeRac_VM_590 = $RowConso_VM_590['recibo'];
$numeFac_VM_590 = $RowConso_VM_590['numero_']; 
$beseComisionApro_VM_590 = $RowConso_VM_590['base_com'];
$dias_VM_590 = $RowConso_VM_590['dias'];
$fecha_doc_VM_590 = $RowConso_VM_590['fecha_doc'];
$sacaMes_VM_590 = date('m', strtotime($fecha_doc_VM_590));

//echo $nomCliente_VM_370."<br>";

$AsVddor_VM_590 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_VM_590'";
$resultAsVddor_VM_590 = mysqli_query($con, $AsVddor_VM_590);

while ($rowVndor_VM_590 = mysqli_fetch_array($resultAsVddor_VM_590)) {
  $venDor_VM_590 = $rowVndor_VM_590['vendedor'];  
}
   if ($venDor_VM_590 == 'VM') {                  
        
        if ($dias_VM_590 <= 90) {
          $liq_rec_messer_VM_590 = 100;
        }elseif ($dias_VM_590 > 120) {
          $liq_rec_messer_VM_590 = 0;
        }else {
          $liq_rec_messer_VM_590 = 80;
        }

        $consultaPPVdor_VM_590 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_VM_590' AND anio = '$anio' AND idVendedor = 6";
        $resultadoPPVdor_VM_590 = mysqli_query($con, $consultaPPVdor_VM_590);

        while ($RowPPVdor_VM_590 = mysqli_fetch_array($resultadoPPVdor_VM_590)) {

          $porceCumpliVdor_VM_590 = $RowPPVdor_VM_590['porcentaje'];
                    
          if ($porceCumpliVdor_VM_590  > 100) {
              $liq_ppto_messer_VM_590 = 0.009;
          }elseif ($porceCumpliVdor_VM_590 <= 79) {
              $liq_ppto_messer_VM_590 = 0;
          }else {
            $liq_ppto_messer_VM_590 = 0.007;
          }
          
          //echo $numeRac_VM_370." - ".$numeFac_VM_370." - ".$beseComisionApro_VM_370." - ".$dias_VM_370." - ".$nomCliente_VM_370." - ".$venDor_VM_370." - ".$porceCumpliVdor_VM_370." - ".$liq_ppto_messer_VM_370." - ".$liq_rec_messer_VM_370."<br>";
         
          $comision_PPTO_VM_590 = $beseComisionApro_VM_590*$liq_ppto_messer_VM_590*($liq_rec_messer_VM_590/100)/3;
          $Total_comision_PPTO_VM_590 += $comision_PPTO_VM_590;

          //echo $comision_PPTO_VM_370."<br>";

        }
      } 

  }elseif ($RowConso_VM_590['porcentaje'] == 16.5) {

    $nomCliente_VM_590 = $RowConso_VM_590['cliente'];

    $codigo_VM_590 = $RowConso_VM_590['codigo'];
    $length_VM_590 = 9;
    $codigoV_CLI_VM_590 = substr(str_repeat(0, $length_VM_590).$codigo_VM_590, - $length_VM_590);
    
    $numeRac_VM_590 = $RowConso_VM_590['recibo'];
    $numeFac_VM_590 = $RowConso_VM_590['numero_']; 
    $beseComisionApro_VM_590 = $RowConso_VM_590['base_com'];
    $dias_VM_590 = $RowConso_VM_590['dias'];
    $fecha_doc_VM_590 = $RowConso_VM_590['fecha_doc'];
    $sacaMes_VM_590 = date('m', strtotime($fecha_doc_VM_590));
    
    //echo $nomCliente_VM_370."<br>";
    
    $AsVddor_VM_590 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_VM_590'";
    $resultAsVddor_VM_590 = mysqli_query($con, $AsVddor_VM_590);
    
    while ($rowVndor_VM_590 = mysqli_fetch_array($resultAsVddor_VM_590)) {
      $venDor_VM_590 = $rowVndor_VM_590['vendedor'];  
    }
       if ($venDor_VM_590 == 'VM') {                  
            
            if ($dias_VM_590 <= 90) {
              $liq_rec_messer_VM_590 = 100;
            }elseif ($dias_VM_590 > 120) {
              $liq_rec_messer_VM_590 = 0;
            }else {
              $liq_rec_messer_VM_590 = 80;
            }
    
            $consultaPPVdor_VM_590 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_VM_590' AND anio = '$anio' AND idVendedor = 6";
            $resultadoPPVdor_VM_590 = mysqli_query($con, $consultaPPVdor_VM_590);
    
            while ($RowPPVdor_VM_590 = mysqli_fetch_array($resultadoPPVdor_VM_590)) {
    
              $porceCumpliVdor_VM_590 = $RowPPVdor_VM_590['porcentaje'];
                        
              if ($porceCumpliVdor_VM_590  > 100) {
                  $liq_ppto_messer_VM_590 = 0.009;
              }elseif ($porceCumpliVdor_VM_590 <= 79) {
                  $liq_ppto_messer_VM_590 = 0;
              }else {
                $liq_ppto_messer_VM_590 = 0.007;
              }
              
              //echo $numeRac_VM_370." - ".$numeFac_VM_370." - ".$beseComisionApro_VM_370." - ".$dias_VM_370." - ".$nomCliente_VM_370." - ".$venDor_VM_370." - ".$porceCumpliVdor_VM_370." - ".$liq_ppto_messer_VM_370." - ".$liq_rec_messer_VM_370."<br>";
             
              $comision_PPTO_VM_590 = $beseComisionApro_VM_590*$liq_ppto_messer_VM_590*($liq_rec_messer_VM_590/100)/1.81;
              $Total_comision_PPTO_VM_590 += $comision_PPTO_VM_590;
    
              //echo $comision_PPTO_VM_370."<br>";
    
            }
          }

  }elseif ($RowConso_VM_590['porcentaje'] == 5) {

    $nomCliente_VM_590 = $RowConso_VM_590['cliente'];

$codigo_VM_590 = $RowConso_VM_590['codigo'];
$length_VM_590 = 9;
$codigoV_CLI_VM_590 = substr(str_repeat(0, $length_VM_590).$codigo_VM_590, - $length_VM_590);

$numeRac_VM_590 = $RowConso_VM_590['recibo'];
$numeFac_VM_590 = $RowConso_VM_590['numero_']; 
$beseComisionApro_VM_590 = $RowConso_VM_590['base_com'];
$dias_VM_590 = $RowConso_VM_590['dias'];
$fecha_doc_VM_590 = $RowConso_VM_590['fecha_doc'];
$sacaMes_VM_590 = date('m', strtotime($fecha_doc_VM_590));

//echo $nomCliente_VM_370."<br>";

$AsVddor_VM_590 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_VM_590'";
$resultAsVddor_VM_590 = mysqli_query($con, $AsVddor_VM_590);

while ($rowVndor_VM_590 = mysqli_fetch_array($resultAsVddor_VM_590)) {
  $venDor_VM_590 = $rowVndor_VM_590['vendedor'];  
}
   if ($venDor_VM_590 == 'VM') {                  
        
        if ($dias_VM_590 <= 90) {
          $liq_rec_messer_VM_590 = 100;
        }elseif ($dias_VM_590 > 120) {
          $liq_rec_messer_VM_590 = 0;
        }else {
          $liq_rec_messer_VM_590 = 80;
        }

        $consultaPPVdor_VM_590 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_VM_590' AND anio = '$anio' AND idVendedor = 6";
        $resultadoPPVdor_VM_590 = mysqli_query($con, $consultaPPVdor_VM_590);

        while ($RowPPVdor_VM_590 = mysqli_fetch_array($resultadoPPVdor_VM_590)) {

          $porceCumpliVdor_VM_590 = $RowPPVdor_VM_590['porcentaje'];
                    
          if ($porceCumpliVdor_VM_590  > 100) {
              $liq_ppto_messer_VM_590 = 0.009;
          }elseif ($porceCumpliVdor_VM_590 <= 79) {
              $liq_ppto_messer_VM_590 = 0;
          }else {
            $liq_ppto_messer_VM_590 = 0.007;
          }
          
          //echo $numeRac_VM_370." - ".$numeFac_VM_370." - ".$beseComisionApro_VM_370." - ".$dias_VM_370." - ".$nomCliente_VM_370." - ".$venDor_VM_370." - ".$porceCumpliVdor_VM_370." - ".$liq_ppto_messer_VM_370." - ".$liq_rec_messer_VM_370."<br>";
         
          $comision_PPTO_VM_590 = $beseComisionApro_VM_590*$liq_ppto_messer_VM_590*($liq_rec_messer_VM_590/100)/6;
          $Total_comision_PPTO_VM_590 += $comision_PPTO_VM_590;

          //echo $comision_PPTO_VM_370."<br>";

        }
      }

  }elseif ($RowConso_VM_590['porcentaje'] == 20) {
    
    $nomCliente_VM_590 = $RowConso_VM_590['cliente'];

$codigo_VM_590 = $RowConso_VM_590['codigo'];
$length_VM_590 = 9;
$codigoV_CLI_VM_590 = substr(str_repeat(0, $length_VM_590).$codigo_VM_590, - $length_VM_590);

$numeRac_VM_590 = $RowConso_VM_590['recibo'];
$numeFac_VM_590 = $RowConso_VM_590['numero_']; 
$beseComisionApro_VM_590 = $RowConso_VM_590['base_com'];
$dias_VM_590 = $RowConso_VM_590['dias'];
$fecha_doc_VM_590 = $RowConso_VM_590['fecha_doc'];
$sacaMes_VM_590 = date('m', strtotime($fecha_doc_VM_590));

//echo $nomCliente_VM_370."<br>";

$AsVddor_VM_590 = "SELECT * FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_VM_590'";
$resultAsVddor_VM_590 = mysqli_query($con, $AsVddor_VM_590);

while ($rowVndor_VM_590 = mysqli_fetch_array($resultAsVddor_VM_590)) {
  $venDor_VM_590 = $rowVndor_VM_590['vendedor'];  
}
   if ($venDor_VM_590 == 'VM') {                  
        
        if ($dias_VM_590 <= 90) {
          $liq_rec_messer_VM_590 = 100;
        }elseif ($dias_VM_590 > 120) {
          $liq_rec_messer_VM_590 = 0;
        }else {
          $liq_rec_messer_VM_590 = 80;
        }

        $consultaPPVdor_VM_590 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes_VM_590' AND anio = '$anio' AND idVendedor = 6";
        $resultadoPPVdor_VM_590 = mysqli_query($con, $consultaPPVdor_VM_590);

        while ($RowPPVdor_VM_590 = mysqli_fetch_array($resultadoPPVdor_VM_590)) {

          $porceCumpliVdor_VM_590 = $RowPPVdor_VM_590['porcentaje'];
                    
          if ($porceCumpliVdor_VM_590  > 100) {
              $liq_ppto_messer_VM_590 = 0.009;
          }elseif ($porceCumpliVdor_VM_590 <= 79) {
              $liq_ppto_messer_VM_590 = 0;
          }else {
            $liq_ppto_messer_VM_590 = 0.007;
          }
          
          //echo $numeRac_VM_370." - ".$numeFac_VM_370." - ".$beseComisionApro_VM_370." - ".$dias_VM_370." - ".$nomCliente_VM_370." - ".$venDor_VM_370." - ".$porceCumpliVdor_VM_370." - ".$liq_ppto_messer_VM_370." - ".$liq_rec_messer_VM_370."<br>";
         
          $comision_PPTO_VM_590 = $beseComisionApro_VM_590*$liq_ppto_messer_VM_590*($liq_rec_messer_VM_590/100)/1.5;
          $Total_comision_PPTO_VM_590 += $comision_PPTO_VM_590;

          //echo $comision_PPTO_VM_370."<br>";

        }
      }
  }
} 
//TRMINA CALCULO COMISIONES MESSER IATGUI................................................................................................................................................................................................

 //Suma toberin para VM
 $comisionesTobe_VM = $Total_comision_PPTO_VM;

 //Suma Cazuca para VM
 $comisionesCazu_VM = $Total_comision_PPTO_VM_060;

 //Suma manizales para VM
 $comisionesManiz_VM = $Total_comision_PPTO_VM_370;
 
 //Suma Iatagui para VM
 $comisionesItag_VM = $Total_comision_PPTO_VM_590;

 $total_comision_rec_x_messer_VM = $comisionesTobe_VM+$comisionesCazu_VM+$comisionesManiz_VM+$comisionesItag_VM;

 //echo $total_comision_rec_x_messer_VM;

 $total_comision_VM = $total_comision_rec_x_messer_VM + $porcentajeRecaudo_VM;

      $objPHPExcel->getActiveSheet()->setCellValue('C9', $suma_VM_VT);
      $objPHPExcel->getActiveSheet()->setCellValue('D9', $suma_VM_VS);
      $objPHPExcel->getActiveSheet()->setCellValue('E9', $suma_VM_VM);
      $objPHPExcel->getActiveSheet()->setCellValue('F9', $suma_VM_VITA);
      $objPHPExcel->getActiveSheet()->setCellValue('G9', $subtotal_VM);
      $objPHPExcel->getActiveSheet()->setCellValue('H9', $total_VM_NC);
      $objPHPExcel->getActiveSheet()->setCellValue('I9', $total_vta_VM);
      $objPHPExcel->getActiveSheet()->setCellValue('J9', $porc_cumplimiento_VM."%");
      $objPHPExcel->getActiveSheet()->setCellValue('K9', $total_comision_rec_x_messer_VM);
      $objPHPExcel->getActiveSheet()->setCellValue('M9', $ventas_ingegas_vendedor_VM);
      $objPHPExcel->getActiveSheet()->setCellValue('N9', $porcentajeRecaudo_VM);
      $objPHPExcel->getActiveSheet()->setCellValue('O9', $total_comision_VM);

    }elseif ($id_vendedor == 7) {
      
      $total_real_EJ = $total_itag - $valorTotal_NCE_590;
      $porc_cumplimiento_EJ = round(($total_real_EJ/$valorPP)*100);

      if ($porc_cumplimiento_EJ < 80) {
        $comision_x_rec_messer_ita_EJ = 0;
      }elseif ($porc_cumplimiento_EJ > 150) {
        $comision_x_rec_messer_ita_EJ = 225000;
      }else {
        $comision_x_rec_messer_ita_EJ = (150000*($porc_cumplimiento_EJ/100))/1;
      }

      $total_comision_EJ = $comision_x_rec_messer_ita_EJ;
      
      $objPHPExcel->getActiveSheet()->setCellValue('G10', $total_itag);
      $objPHPExcel->getActiveSheet()->setCellValue('H10', $valorTotal_NCE_590);
      $objPHPExcel->getActiveSheet()->setCellValue('I10', $total_real_EJ);
      $objPHPExcel->getActiveSheet()->setCellValue('J10', $porc_cumplimiento_EJ."%");
      $objPHPExcel->getActiveSheet()->setCellValue('K10', $comision_x_rec_messer_ita_EJ);
      $objPHPExcel->getActiveSheet()->setCellValue('O10', $total_comision_EJ); 

    } elseif ($id_vendedor == 8) {

      $total_toberin_sur = $total_toberin + $total_sur;
      $NCE_Bogota = $valorTotal_NCE_051+$valorTotal_NCE_060;

      $total_vta_Bogota = $total_toberin_sur - $NCE_Bogota;
      $porc_cumplimiento_Bogota = round(($total_vta_Bogota/$valorPP)*100);

      if ($porc_cumplimiento_Bogota < 80) {
        $comision_x_rec_messer_bogota = 0;
      }elseif ($porc_cumplimiento_Bogota > 150) {
        $comision_x_rec_messer_bogota = (($porc_cumplimiento_Bogota/100)*225000)/1;
      }else {
        $comision_x_rec_messer_bogota = (($porc_cumplimiento_Bogota/100)*150000)/1;
      }

      $total_comision_bogota1 = $comision_x_rec_messer_bogota;

      $objPHPExcel->getActiveSheet()->setCellValue('G11', $total_toberin_sur);
      $objPHPExcel->getActiveSheet()->setCellValue('H11', $NCE_Bogota);
      $objPHPExcel->getActiveSheet()->setCellValue('I11', $total_vta_Bogota);
      $objPHPExcel->getActiveSheet()->setCellValue('J11', $porc_cumplimiento_Bogota."%");
      $objPHPExcel->getActiveSheet()->setCellValue('K11', $comision_x_rec_messer_bogota);
      $objPHPExcel->getActiveSheet()->setCellValue('O11', $total_comision_bogota1);

    }elseif ($id_vendedor == 9) {

      $consultaComiLIN = "SELECT * FROM comisiones_toberin WHERE mes = '$mesC'";
      $resultComiLIN = mysqli_query($con, $consultaComiLIN);

      while ($rowComiLIN = mysqli_fetch_array($resultComiLIN)) {
        $reciboLIN = $rowComiLIN['rec'];
        $factLIN = $rowComiLIN['numero_'];
        $baseCom = $rowComiLIN['base_trans'];
        
        //echo $factLIN."<br>";
     
      $consultaProductoLIN = "SELECT DISTINCT anio, mes, sucursal, cliente, numero_legal, producto_servicio, envase, cantidad, monto_unitario, valor_total, fecha_doc FROM ventas_producto WHERE numero_legal = '$factLIN'";
      $resultProductoLIN = mysqli_query($con, $consultaProductoLIN);

      while ($rowProductoLIN = mysqli_fetch_array($resultProductoLIN)) {
        $numeroFacLIN = $rowProductoLIN['numero_legal'];
        $valorFactLIN = $rowProductoLIN['valor_total'];
        $producServiLIN = $rowProductoLIN['producto_servicio'];
        //echo $numeroFacLIN."<br>";
        //echo $numeroFacLIN." - ".$valorFactLIN."<br>";

        $consultaLIN = "SELECT DISTINCT referencia, linea, descripcion, tipo FROM unneg WHERE referencia = '$producServiLIN'";
        $resultConsultaLIN = mysqli_query($con, $consultaLIN);

        while ($rowLIN = mysqli_fetch_array($resultConsultaLIN)) {
          $LineaLIN = $rowLIN['linea'];
          if ($LineaLIN == 'LIN') {
            //echo $numeroFacLIN." - ".$baseCom." - ".$LineaLIN."<br>";
            $valorLIN += $baseCom;
            $ValorLIN_rec = round($valorLIN*0.001); 
          }        

        }

      }
    }

    $consultaComiLIN_060 = "SELECT * FROM comisiones_cazuca WHERE mes = '$mesC'";
      $resultComiLIN_060 = mysqli_query($con, $consultaComiLIN_060);

      while ($rowComiLIN_060 = mysqli_fetch_array($resultComiLIN_060)) {
        $reciboLIN_060 = $rowComiLIN_060['recibo'];
        $factLIN_060 = $rowComiLIN_060['numero'];
        $baseCom_060 = $rowComiLIN_060['base_com'];
        
        //echo $factLIN_060."<br>";
     
      $consultaProductoLIN_060 = "SELECT DISTINCT anio, mes, sucursal, cliente, numero_legal, producto_servicio, envase, cantidad, monto_unitario, valor_total, fecha_doc FROM ventas_producto WHERE numero_legal = '$factLIN_060'";
      $resultProductoLIN_060 = mysqli_query($con, $consultaProductoLIN_060);

      while ($rowProductoLIN_060 = mysqli_fetch_array($resultProductoLIN_060)) {
        $numeroFacLIN_060 = $rowProductoLIN_060['numero_legal'];
        $valorFactLIN_060 = $rowProductoLIN_060['valor_total'];
        $producServiLIN_060 = $rowProductoLIN_060['producto_servicio'];
        //echo $numeroFacLIN."<br>";
        //echo $numeroFacLIN_060." - ".$valorFactLIN_060." - ".$producServiLIN_060."<br>";

        $consultaLIN_060 = "SELECT DISTINCT referencia, linea, descripcion, tipo FROM unneg WHERE referencia = '$producServiLIN_060'";
        $resultConsultaLIN_060 = mysqli_query($con, $consultaLIN_060);

        while ($rowLIN_060 = mysqli_fetch_array($resultConsultaLIN_060)) {
          $LineaLIN_060 = $rowLIN_060['linea'];
          if ($LineaLIN_060 == 'LIN') {
            //echo $numeroFacLIN_060." - ".$baseCom_060." - ".$LineaLIN_060."<br>";
            $valorLIN_060 += $baseCom_060;
            $ValorLIN_rec_060 = round($valorLIN_060*0.001); 
          }        

        }

      }
    }

      $total_LIN_nitrog = $ValorLIN_rec + $ValorLIN_rec_060;

      $objPHPExcel->getActiveSheet()->setCellValue('G12', $total_toberin_sur);
      $objPHPExcel->getActiveSheet()->setCellValue('H12', $NCE_Bogota);
      $objPHPExcel->getActiveSheet()->setCellValue('I12', $total_vta_Bogota);
      $objPHPExcel->getActiveSheet()->setCellValue('J12', $porc_cumplimiento_Bogota."%");
      $objPHPExcel->getActiveSheet()->setCellValue('L12', $total_LIN_nitrog);
      $objPHPExcel->getActiveSheet()->setCellValue('O12', $total_LIN_nitrog);

    }elseif ($id_vendedor == 10) {
      
      $total_vta_FH = $total_manizales - $valorTotal_NCE_370;
      $porc_cumplimiento_FH = round(($total_vta_FH/$valorPP)*100);

      if ($porc_cumplimiento_FH < 80) {
        $comision_x_rec_messer_ita_FH = 0;
      }elseif ($porc_cumplimiento_FH > 150) {
        $comision_x_rec_messer_ita_FH = 225000;
      }else {
        $comision_x_rec_messer_ita_FH = (150000*($porc_cumplimiento_FH/100))/1;
      }

      $total_comision_FH = $comision_x_rec_messer_ita_FH;

      $objPHPExcel->getActiveSheet()->setCellValue('G13', $total_manizales);
      $objPHPExcel->getActiveSheet()->setCellValue('H13', $valorTotal_NCE_370);
      $objPHPExcel->getActiveSheet()->setCellValue('I13', $total_vta_FH);
      $objPHPExcel->getActiveSheet()->setCellValue('J13', $porc_cumplimiento_FH."%");
      $objPHPExcel->getActiveSheet()->setCellValue('K13', $comision_x_rec_messer_ita_FH);
      $objPHPExcel->getActiveSheet()->setCellValue('O13', $total_comision_FH);

    }elseif ($id_vendedor == 12) {

        //...........................................................................................................................EDER....................................  
        $ventasAcumuladasCobrador = "SELECT DISTINCT numero_legal, sucursal, total_sin_iva, tipo_doc, operador FROM ventas_acumuladas ORDER BY id_ventas_acumuladas";
        $queryCobrador = mysqli_query($con, $ventasAcumuladasCobrador);

        while ($traeVentasAcu = mysqli_fetch_array($queryCobrador)) {
         
          if ($traeVentasAcu['tipo_doc'] == 'FACE' && $traeVentasAcu['sucursal'] == '051') {
            $sucursal = $traeVentasAcu['sucursal'];
            $tipoFactura = $traeVentasAcu['tipo_doc'];
            $facturaVenAcu = $traeVentasAcu['numero_legal'];
            $totalSinIVA = $traeVentasAcu['total_sin_iva'];

            //echo $sucursal." / ".$tipoFactura." / ".$facturaVenAcu." / ".$totalSinIVA."<br>";
            
          //INICIA TOBERIN
          $sucursal501 = "SELECT * FROM comisiones_toberin WHERE numero_ = '$facturaVenAcu' AND mes = '$mesC'";
          $query051 = mysqli_query($con, $sucursal501);

          while ($datos051 = mysqli_fetch_array($query051)) {
            if (strlen($datos051['rec']) <= 5) { //&& substr($datos051['rec'], 0, 2) != 24 
              $recibo051 = $datos051['rec']; 
              $numeroFac = $datos051['numero_'];
              $diastrans = $datos051['dias'];
              $valorSinIVA = $datos051['base_trans'];  
              
              
              if ($diastrans <= 1) {
                $comisionCobrador += $valorSinIVA*0.0015;
                //echo $numeroFac." - ".$comisionCobrador."<br>";                
              }elseif ($diastrans >= 2 && $diastrans <= 45) {
                $comisionCobrador += $valorSinIVA*0.001;
                //echo $numeroFac." - ".$comisionCobrador."<br>";                
              }elseif ($diastrans >= 46 && $diastrans <= 75) {
                $comisionCobrador += $valorSinIVA*0.0005;
                //echo $numeroFac." - ".$comisionCobrador."<br>";  
              }elseif ($diastrans >= 76 && $diastrans <= 100) {
                $comisionCobrador += $valorSinIVA*0.00025;
                //echo $numeroFac." - ".$comisionCobrador."<br>";  
              }elseif ($diastrans >= 101) {
                $comisionCobrador += $valorSinIVA*0.00015;
                //echo $numeroFac." - ".$comisionCobrador."<br>";  
              } 
              
            }        


          } 
          //ACABA TOBERIN

        }elseif ($traeVentasAcu['tipo_doc'] == 'FACE' && $traeVentasAcu['sucursal'] == '060') {
          
          $sucursal = $traeVentasAcu['sucursal'];
          $tipoFactura = $traeVentasAcu['tipo_doc'];
          $facturaVenAcu = $traeVentasAcu['numero_legal'];
          $totalSinIVA = $traeVentasAcu['total_sin_iva'];
          
          //INICIA SUR
          $sucursal060 = "SELECT * FROM comisiones_cazuca WHERE numero = '$facturaVenAcu' AND mes = '$mesC' AND dias != ''";
          $query060 = mysqli_query($con, $sucursal060);

          while ($datos060 = mysqli_fetch_array($query060)) {              
              if (strlen($datos060['recibo']) <= 5) {
                $recibo060 = $datos060['recibo']; 
                $numeroFac060 = $datos060['numero'];
                $diastrans060 = $datos060['dias'];
                $valorSinIVA060 = $datos060['base_com'];

                //echo $numeroFac060." - ".$sucursal."<br>";
                

                if ($diastrans060 <= 1) {
                  $comisionCobrador060 += $valorSinIVA060*0.0015;
                  //echo $numeroFac060." - ".$comisionCobrador060."<br>";              
                }elseif ($diastrans060 >= 2 && $diastrans060 <= 45) {
                  $comisionCobrador060 += $valorSinIVA060*0.001;
                  //echo $numeroFac060." - ".$comisionCobrador060."<br>";              
                }elseif ($diastrans060 >= 46 && $diastrans060 <= 75) {
                  $comisionCobrador060 += $valorSinIVA060*0.0005;
                  //echo $numeroFac060." - ".$comisionCobrador060."<br>";
                }elseif ($diastrans060 >= 76 && $diastrans060 <= 100) {
                  $comisionCobrador060 += $valorSinIVA060*0.00025;
                  //echo $numeroFac060." - ".$comisionCobrador060."<br>";
                }elseif ($diastrans060 >= 101) {
                  $comisionCobrador060 += $valorSinIVA060*0.00015;
                  //echo $numeroFac060." - ".$comisionCobrador060."<br>";
                }
               
              }     

                             
          }
          //ACABA SUR

        }elseif ($traeVentasAcu['tipo_doc'] == 'FACE' && $traeVentasAcu['sucursal'] == '370') {

          $sucursal = $traeVentasAcu['sucursal'];
          $tipoFactura = $traeVentasAcu['tipo_doc'];
          $facturaVenAcu = $traeVentasAcu['numero_legal'];
          $totalSinIVA = $traeVentasAcu['total_sin_iva'];

           //INICIA MANIZALES
           $sucursal370 = "SELECT * FROM comisiones_manizales WHERE no_fact = '$facturaVenAcu' AND mes = '$mesC'";
           $query370 = mysqli_query($con, $sucursal370);
           while ($datos370 = mysqli_fetch_array($query370)) {
 
               if ($datos370['dias_transcurridos'] != '' && strlen($datos370['rec']) <= 5) {
                 $recibo370 = $datos370['rec'];
                 $numeroFac370 = $datos370['no_fact'];
                 $diastrans370 = $datos370['dias_transcurridos'];
                 $valorSinIVA370 = $datos370['base_com']; 
                // echo $numeroFac370."<br>";                 

                 if ($diastrans370 <= 1) {
                  $comisionCobrador370 += $valorSinIVA370*0.0015;              
                }elseif ($diastrans370 >= 2 && $diastrans370 <= 45) {
                  $comisionCobrador370 += $valorSinIVA370*0.001;              
                }elseif ($diastrans370 >= 46 && $diastrans370 <= 75) {
                  $comisionCobrador370 += $valorSinIVA370*0.0005;
                }elseif ($diastrans370 >= 76 && $diastrans370 <= 100) {
                  $comisionCobrador370 += $valorSinIVA370*0.00025;
                }elseif ($diastrans370 >= 101) {
                  $comisionCobrador370 += $valorSinIVA370*0.00015;
                }

                                 
               }            
           }
           //ACABA MANIZALES

        }elseif ($traeVentasAcu['tipo_doc'] == 'FACE' && $traeVentasAcu['sucursal'] == '590') {
             
          $sucursal = $traeVentasAcu['sucursal'];
          $tipoFactura = $traeVentasAcu['tipo_doc'];
          $facturaVenAcu = $traeVentasAcu['numero_legal'];
          $totalSinIVA = $traeVentasAcu['total_sin_iva'];

            //INICIA MEDELLIN
            $sucursal590 = "SELECT * FROM comisiones_itagui WHERE numero_ = '$facturaVenAcu' AND mes = '$mesC'";
            $query590 = mysqli_query($con, $sucursal590);
            while ($datos590 = mysqli_fetch_array($query590)) {
  
                if ($datos590['dias'] != '' && strlen($datos590['recibo']) <= 5) {
                  $recibo590 = $datos590['recibo'];
                  $numeroFac590 = $datos590['numero_'];
                  $diastrans590 = $datos590['dias'];
                  $valorSinIVA590 = $datos590['base_com'];
                  //echo $recibo590." / ".$diastrans590."<br>";
                  //echo $numeroFac590."<br>";

                  if ($diastrans590 <= 1) {
                    $comisionCobrador590 += $valorSinIVA590*0.0015;              
                  }elseif ($diastrans590 >= 2 && $diastrans590 <= 45) {
                    $comisionCobrador590 += $valorSinIVA590*0.001;              
                  }elseif ($diastrans590 >= 46 && $diastrans590 <= 75) {
                    $comisionCobrador590 += $valorSinIVA590*0.0005;
                  }elseif ($diastrans590 >= 76 && $diastrans590 <= 100) {
                    $comisionCobrador590 += $valorSinIVA590*0.00025;
                  }elseif ($diastrans590 >= 101) {
                    $comisionCobrador590 += $valorSinIVA590*0.00015;
                  }
                }                           
             }
            //ACABA MEDELLIN
        }
     }
        

        $sumaCT = $comisionCobrador;  
        $sumaCS = $comisionCobrador060;
        $sumaCM = $comisionCobrador370;
        $sumaITA = $comisionCobrador590; 
        
        
        $consultaDiferentes = "SELECT * FROM comisiones_toberin WHERE mes = '$mesC' AND dias = ''";
              $queryDiferentes051 = mysqli_query($con, $consultaDiferentes);
    
              while ($diferentes051 = mysqli_fetch_array($queryDiferentes051)) {
                  $facturaDiferente = $diferentes051['numero_']; 
                  $sucur = $diferentes051['sucursal'];     
                  $baseComDif = $diferentes051['base_comision'];
                  $valorFac += $baseComDif*0.000033; 
                  //echo $facturaDiferente." - ".$valorFac."<br>";  
                               
              }

        $consultaDiferentes2 = "SELECT * FROM comisiones_cazuca WHERE mes = '$mesC' AND dias = ''";
              $queryDiferentes060 = mysqli_query($con, $consultaDiferentes2);
    
              while ($diferentes060 = mysqli_fetch_array($queryDiferentes060)) {
                  $facturaDiferente060 = $diferentes060['recibo'];
                  //echo $facturaDiferente060."<br>";
                  $valorFac060 += $diferentes060['base_comision']*0.000033; 
                  //echo $facturaDiferente."  ".$valorFac."<br>";  
                               
              }
              
        $consultaDiferentes3 = "SELECT * FROM comisiones_manizales WHERE mes = '$mesC' AND dias_transcurridos = ''";
              $queryDiferentes370 = mysqli_query($con, $consultaDiferentes3);
    
              while ($diferentes370 = mysqli_fetch_array($queryDiferentes370)) {
                  $facturaDiferente370 = $diferentes370['rec'];
                  //echo $facturaDiferente370."<br>";
                  $valorFac370 += $diferentes370['base_comision']*0.000033; 
                  //echo $facturaDiferente."  ".$valorFac."<br>";  
                               
              }
        
        $consultaDiferentes4 = "SELECT * FROM comisiones_itagui WHERE mes = '$mesC' AND dias = ''";
              $queryDiferentes590 = mysqli_query($con, $consultaDiferentes4);
    
              while ($diferentes590 = mysqli_fetch_array($queryDiferentes590)) {
                  $facturaDiferente590 = $diferentes590['recibo'];
                  //echo $facturaDiferente590."<br>";
                  $valorFac590 += $diferentes590['base_comision']*0.000033; 
                  //echo $facturaDiferente."  ".$valorFac."<br>";  
                              
              }    
      
        $comisionTotalCT= $sumaCT+$valorFac; // Imprime el total de comisiones para cartera Toberin.         
        $comisionTotalCS = $sumaCS+$valorFac060; // Imprime el total de comisiones para cartera Sur      
        $comisionTotalCM = $sumaCM+$valorFac370; //Imprime el total de comisiones para cartera manizales 
        $comisionTotalITA = $sumaITA+$valorFac590; // Imprime el total de cartera para Itagui (Manizales)
        //echo $comisionTotalITA;      
        

      $comision_x_rec_messer = $comisionTotalCT+$comisionTotalCS+$comisionTotalCM+$comisionTotalITA;
      $total_comision = $comision_x_rec_messer;     

      $objPHPExcel->getActiveSheet()->setCellValue('K14', $comision_x_rec_messer);
      $objPHPExcel->getActiveSheet()->setCellValue('O14', $total_comision);

//......................................................ABACA_EDER...........................................................................................   

    }elseif ($id_vendedor == 13) {

      $consultaPagoCobranza = "SELECT fecha, factura, cliente, recaudo, sucursal FROM cobranza";
      $queryPagoCobranza = mysqli_query($con, $consultaPagoCobranza);
      while ($traerDatosCobranza = mysqli_fetch_array($queryPagoCobranza)) {
          $fechaCo = $traerDatosCobranza['fecha'];                
          $facturaCo = $traerDatosCobranza['factura'];
          $facturaCo2 = preg_replace('/[^0-9]/', '', $facturaCo); // Quitarle las letras a las facturas
          $saca_cliente = $traerDatosCobranza['cliente'];
          $identificador = $traerDatosCobranza['sucursal'];
          $venta_neta = $traerDatosCobranza['recaudo']/1.19;
        
          if ($identificador != '1') {                  
            $consultaCobranza = "SELECT fecha, documento, venta_neta FROM vtas_vdor WHERE documento = '$facturaCo2'";
          $queryCobranza = mysqli_query($con, $consultaCobranza);   
          while ($traeDatos = mysqli_fetch_array($queryCobranza)) {
            $fecha = $traeDatos['fecha'];                 
            $factura = $traeDatos['documento'];                  
            $dias = calculaDias($fecha, $fechaCo);
            $diferencia_dia = $dias[11]+1;                  
            //$venta_neta = $traeDatos['venta_neta']; 
            //echo $factura." / ".$diferencia_dia." / ".$venta_neta."<br>";                 

            if ($diferencia_dia <= 1) {
                $comision_ing = $venta_neta*0.0015;
                $total_15 += $comision_ing;                       
              }elseif ($diferencia_dia >= 2 && $diferencia_dia <= 45) {
                $comision_ing = $venta_neta*0.00075;
                $total_75 += $comision_ing;                                                           
              }elseif ($diferencia_dia >= 46 && $diferencia_dia <= 75) {
                $comision_ing = $venta_neta*0.0005;
                $total_5 += $comision_ing;
                //echo $saca_cliente." / $".$venta_neta." - ".$factura." - ".$fecha." - ".$fechaCo." / ".$diferencia_dia." / $". $comision_ing."<br>";                                   
              }elseif ($diferencia_dia >= 76 && $diferencia_dia <= 100) {
                $comision_ing = $venta_neta*0.00025;
                $total_25 += $comision_ing;                                     
              }elseif ($diferencia_dia >= 101) {                    
                $comision_ing = $venta_neta*0.00015;
                $total_015 += $comision_ing;                
              }else {
                $comision_ing = 0;
              } 
            } 
          }
         }
         
        $comision_cobranza_Ingegas = $total_15+$total_75+$total_5+$total_25+$total_015; 

      $objPHPExcel->getActiveSheet()->setCellValue('N15', $comision_cobranza_Ingegas);
      $objPHPExcel->getActiveSheet()->setCellValue('O15', $comision_cobranza_Ingegas); 

    }

    $fila++;
  }



// TERMINA CONSULTA DE VENDEDORES PARA LISTAR EN EXCEL.............................................................................................

header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
header('Content-Disposition: attachment;filename="Resumen.xlsx"');
header('Cache-Control: max-age=0');
$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$writer->save('php://output');
}

function calculaDias($fecha_1, $fecha_2){
  $tomaFecha1 = date_create($fecha_1);
  $tomaFecha2 = date_create($fecha_2);
  $dif_dias = date_diff($tomaFecha1, $tomaFecha2);

  $tiempo = array();
  
  foreach ($dif_dias as $valor) {
    $tiempo[] = $valor;
  }
  return $tiempo;

}



?>