<?php
require('../../libraries/conexion.php');
require '../../PHPExcel.php';
date_default_timezone_set('America/Bogota');
$Fecha = date("Y-m-d");

$consultaCargue = "SELECT mes FROM cargue_comisiones_vendedores ORDER BY id_cargue_comisiones_vendedores DESC LIMIT 1"; //hace la consulta y trae el ultimo mes cargado
$resultadoCargue = mysqli_query($con, $consultaCargue);

while ($linea = mysqli_fetch_array($resultadoCargue)) {
  $mesC = $linea["mes"]; 
  $anio = date("Y");
}

if($tipo = 1){
  $fila = 2;
  $consultaConso051 = "SELECT * FROM comisiones_toberin WHERE mes = '$mesC' AND dias != ''";
  $resultadoConso051 = mysqli_query($con, $consultaConso051);
  $objPHPExcel = new PHPExcel();
  $objPHPExcel->getProperties()->setCreator("Ingegas")->setDescription("Comisiones Vendedores");
  $objPHPExcel->setActiveSheetIndex(0);

  $objPHPExcel->getActiveSheet()->setTitle("Recaudo TV Ms");
  $objPHPExcel->getActiveSheet()->setCellValue('E2', "  Jorge Avendaño");
  $objPHPExcel->getActiveSheet()->setCellValue('E3', 'RECAUDO TV');

  $objPHPExcel->getActiveSheet()->setCellValue('A6', 'PR');
  $objPHPExcel->getActiveSheet()->setCellValue('B6', 'Agencia');
  $objPHPExcel->getActiveSheet()->setCellValue('C6', 'mer');
  $objPHPExcel->getActiveSheet()->setCellValue('D6', 'Mer');
  $objPHPExcel->getActiveSheet()->setCellValue('E6', 'Recibo');
  $objPHPExcel->getActiveSheet()->setCellValue('F6', 'Fecha');
  $objPHPExcel->getActiveSheet()->setCellValue('G6', 'Codigo');
  $objPHPExcel->getActiveSheet()->setCellValue('H6', 'Cliente');
  $objPHPExcel->getActiveSheet()->setCellValue('I6', 'Valor Aplicado');
  $objPHPExcel->getActiveSheet()->setCellValue('J6', 'Base Comision');
  $objPHPExcel->getActiveSheet()->setCellValue('K6', 'tipo_');
  $objPHPExcel->getActiveSheet()->setCellValue('L6', 'numero_');
  $objPHPExcel->getActiveSheet()->setCellValue('M6', 'fecha_doc');
  $objPHPExcel->getActiveSheet()->setCellValue('N6', 'Dias');
  $objPHPExcel->getActiveSheet()->setCellValue('O6', 'Mes');
  $objPHPExcel->getActiveSheet()->setCellValue('P6', '% Cumplimiento mes');
  $objPHPExcel->getActiveSheet()->setCellValue('Q6', '% Liquidac PPTO');
  $objPHPExcel->getActiveSheet()->setCellValue('R6', '% Liquidac Recaudo');
  $objPHPExcel->getActiveSheet()->setCellValue('S6', 'Comision PPTO');
  $objPHPExcel->getActiveSheet()->setCellValue('T6', 'Porcentaje Aplicado');  
  
  //CALCULO COMISIONES MESSER TOBERIN................................................................................................................................................................................
  $fila = 7;
  while ($RowConso051 = mysqli_fetch_array($resultadoConso051)) {     
    if ($RowConso051['dias'] != '' && $RowConso051['porc_3'] != '' && $RowConso051['porc_4'] != '') { 

      $agencia_051 = '051';
      $Umer = $RowConso051['merdo'];
      $mer = $Umer."51";
      $fecha051 = $RowConso051['fecha'];

      $codigo051 = $RowConso051['codigo'];
      $length = 9;
      $codigoV_CLI_051 = substr(str_repeat(0, $length).$codigo051, - $length);

      $cliente051 = $RowConso051['cliente'];
      $valorAplicado051 = $RowConso051['valor_aplicado'];
      $tipo = $RowConso051['tipo'];
      $numeRac = $RowConso051['rec'];
      $numeFac = $RowConso051['numero_']; 
      $beseComisionApro = $RowConso051['base_trans'];
      $dias051 = $RowConso051['dias'];
      $fecha_doc = $RowConso051['fecha_doc'];
      $porceApli = $RowConso051['porcentaje'];

      $sacaMes = date('m', strtotime($fecha_doc));
      $añoFac = date('Y', strtotime($fecha_doc));

      $AsVddor051 = "SELECT DISTINCT codigo, cliente, vendedor FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_051'";
      $resultAsVddor051 = mysqli_query($con, $AsVddor051);

      while ($rowAsVddor051 = mysqli_fetch_array($resultAsVddor051)) {
        $venDor051 = $rowAsVddor051['vendedor'];
      } 

        if ($venDor051 == 'TV') {
          if ($dias051 <= 90) {
            $porcentaje_liqui_recaudo = 100;
        }elseif ($dias051 > 120) {
            $porcentaje_liqui_recaudo = 0;
        } else {
          $porcentaje_liqui_recaudo = 80;
        }          
       

        //echo $sacaMes."<br>";

        $consultaPPVdor = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes' AND anio = '$anio' AND idVendedor = 5";
        $resultadoPPVdor = mysqli_query($con, $consultaPPVdor);

        while ($RowPPVdor = mysqli_fetch_array($resultadoPPVdor)) {

          $porceCumpliVdor = $RowPPVdor['porcentaje'];

          if ($porceCumpliVdor  > 100) {
            $liquidacion_PPTO = 0.009;
        }elseif ($porceCumpliVdor >= 80) {
            $liquidacion_PPTO = 0.007;
        }elseif ($porceCumpliVdor >= 60 && $sacaMes == '05' && $añoFac == '2021' || $porceCumpliVdor >= 60 && $sacaMes == '06' && $añoFac == '2021' || $porceCumpliVdor >= 60 && $sacaMes == '07' && $añoFac == '2021') {
          $liquidacion_PPTO = 0.0035;
        }else {
          $liquidacion_PPTO = 0;
        }

          //echo $numeFac." / ".$beseComisionApro." / ".$fecha_doc." / ".$dias051." / ".$sacaMes." / ".$porceCumpliVdor." / ".$liquidacion_PPTO." / ".$porcentaje_liqui_recaudo."<br>";

          $comision_PPTO_Diego = $beseComisionApro*$liquidacion_PPTO*($porcentaje_liqui_recaudo/100);
          $Total_comision_PPTO_Diego += $comision_PPTO_Diego;

      }
        
      $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $venDor051);  
      $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $agencia_051);
      $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $Umer);
      $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $mer);
      $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $numeRac);
      $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $fecha051);
      $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $codigo051);
      $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $cliente051);
      $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $valorAplicado051);
      $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, $beseComisionApro);    
      $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $tipo);
      $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, $numeFac);
      $objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, $fecha_doc);
      $objPHPExcel->getActiveSheet()->setCellValue('N'.$fila, $dias051);
      $objPHPExcel->getActiveSheet()->setCellValue('O'.$fila, $sacaMes);
      $objPHPExcel->getActiveSheet()->setCellValue('P'.$fila, $porceCumpliVdor."%");
      $objPHPExcel->getActiveSheet()->setCellValue('Q'.$fila, $liquidacion_PPTO);
      $objPHPExcel->getActiveSheet()->setCellValue('R'.$fila, $porcentaje_liqui_recaudo."%");
      $objPHPExcel->getActiveSheet()->setCellValue('S'.$fila, $comision_PPTO_Diego);
      $objPHPExcel->getActiveSheet()->setCellValue('T'.$fila, $porceApli);
            
      $fila++;  

    } 
    
    }elseif ($RowConso051['porcentaje'] == 10) {
      $agencia_051 = '051';
      $Umer = $RowConso051['merdo'];
      $mer = $Umer."51";
      $fecha051 = $RowConso051['fecha'];

      $codigo051 = $RowConso051['codigo'];
      $length = 9;
      $codigoV_CLI_051 = substr(str_repeat(0, $length).$codigo051, - $length);

      $cliente051 = $RowConso051['cliente'];
      $valorAplicado051 = $RowConso051['valor_aplicado'];
      $tipo = $RowConso051['tipo'];
      $numeRac = $RowConso051['rec'];
      $numeFac = $RowConso051['numero_']; 
      $beseComisionApro = $RowConso051['base_trans'];
      $dias051 = $RowConso051['dias'];
      $fecha_doc = $RowConso051['fecha_doc'];
      $porceApli = $RowConso051['porcentaje'];
      $sacaMes = date('m', strtotime($fecha_doc));
      $añoFac = date('Y', strtotime($fecha_doc));

      $AsVddor051 = "SELECT DISTINCT codigo, cliente, vendedor FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_051'";
      $resultAsVddor051 = mysqli_query($con, $AsVddor051);

      while ($rowAsVddor051 = mysqli_fetch_array($resultAsVddor051)) {
        $venDor051 = $rowAsVddor051['vendedor'];
      } 

        if ($venDor051 == 'TV') {
          if ($dias051 <= 90) {
            $porcentaje_liqui_recaudo = 100;
        }elseif ($dias051 > 120) {
            $porcentaje_liqui_recaudo = 0;
        } else {
          $porcentaje_liqui_recaudo = 80;
        }          
       

        //echo $sacaMes."<br>";

        $consultaPPVdor = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes' AND anio = '$anio' AND idVendedor = 5";
        $resultadoPPVdor = mysqli_query($con, $consultaPPVdor);

        while ($RowPPVdor = mysqli_fetch_array($resultadoPPVdor)) {

          $porceCumpliVdor = $RowPPVdor['porcentaje'];

          if ($porceCumpliVdor  > 100) {
            $liquidacion_PPTO = 0.009;
        }elseif ($porceCumpliVdor >= 80) {
            $liquidacion_PPTO = 0.007;
        }elseif ($porceCumpliVdor >= 60 && $sacaMes == '05' && $añoFac == '2021' || $porceCumpliVdor >= 60 && $sacaMes == '06' && $añoFac == '2021' || $porceCumpliVdor >= 60 && $sacaMes == '07' && $añoFac == '2021') {
          $liquidacion_PPTO = 0.0035;
        }else {
          $liquidacion_PPTO = 0;
        }

          //echo $numeFac." / ".$beseComisionApro." / ".$fecha_doc." / ".$dias051." / ".$sacaMes." / ".$porceCumpliVdor." / ".$liquidacion_PPTO." / ".$porcentaje_liqui_recaudo."<br>";

          $comision_PPTO_Diego = $beseComisionApro*$liquidacion_PPTO*($porcentaje_liqui_recaudo/100)/3;
          $Total_comision_PPTO_Diego += $comision_PPTO_Diego;

      }
        
      $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $venDor051);  
      $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $agencia_051);
      $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $Umer);
      $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $mer);
      $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $numeRac);
      $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $fecha051);
      $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $codigo051);
      $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $cliente051);
      $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $valorAplicado051);
      $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, $beseComisionApro);    
      $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $tipo);
      $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, $numeFac);
      $objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, $fecha_doc);
      $objPHPExcel->getActiveSheet()->setCellValue('N'.$fila, $dias051);
      $objPHPExcel->getActiveSheet()->setCellValue('O'.$fila, $sacaMes);
      $objPHPExcel->getActiveSheet()->setCellValue('P'.$fila, $porceCumpliVdor."%");
      $objPHPExcel->getActiveSheet()->setCellValue('Q'.$fila, $liquidacion_PPTO);
      $objPHPExcel->getActiveSheet()->setCellValue('R'.$fila, $porcentaje_liqui_recaudo."%");
      $objPHPExcel->getActiveSheet()->setCellValue('S'.$fila, $comision_PPTO_Diego);
      $objPHPExcel->getActiveSheet()->setCellValue('T'.$fila, $porceApli);
            
      $fila++;  

    } 
    }elseif ($RowConso051['porcentaje'] == 16.5) {
      $agencia_051 = '051';
      $Umer = $RowConso051['merdo'];
      $mer = $Umer."51";
      $fecha051 = $RowConso051['fecha'];

      $codigo051 = $RowConso051['codigo'];
      $length = 9;
      $codigoV_CLI_051 = substr(str_repeat(0, $length).$codigo051, - $length);

      $cliente051 = $RowConso051['cliente'];
      $valorAplicado051 = $RowConso051['valor_aplicado'];
      $tipo = $RowConso051['tipo'];
      $numeRac = $RowConso051['rec'];
      $numeFac = $RowConso051['numero_']; 
      $beseComisionApro = $RowConso051['base_trans'];
      $dias051 = $RowConso051['dias'];
      $fecha_doc = $RowConso051['fecha_doc'];
      $sacaMes = date('m', strtotime($fecha_doc));
      $añoFac = date('Y', strtotime($fecha_doc));
      $porceApli = $RowConso051['porcentaje'];

      $AsVddor051 = "SELECT DISTINCT codigo, cliente, vendedor FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_051'";
      $resultAsVddor051 = mysqli_query($con, $AsVddor051);

      while ($rowAsVddor051 = mysqli_fetch_array($resultAsVddor051)) {
        $venDor051 = $rowAsVddor051['vendedor'];
      } 

        if ($venDor051 == 'TV') {
          if ($dias051 <= 90) {
            $porcentaje_liqui_recaudo = 100;
        }elseif ($dias051 > 120) {
            $porcentaje_liqui_recaudo = 0;
        } else {
          $porcentaje_liqui_recaudo = 80;
        }          
       

        //echo $sacaMes."<br>";

        $consultaPPVdor = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes' AND anio = '$anio' AND idVendedor = 5";
        $resultadoPPVdor = mysqli_query($con, $consultaPPVdor);

        while ($RowPPVdor = mysqli_fetch_array($resultadoPPVdor)) {

          $porceCumpliVdor = $RowPPVdor['porcentaje'];

          if ($porceCumpliVdor  > 100) {
            $liquidacion_PPTO = 0.009;
        }elseif ($porceCumpliVdor >= 80) {
            $liquidacion_PPTO = 0.007;
        }elseif ($porceCumpliVdor >= 60 && $sacaMes == '05' && $añoFac == '2021' || $porceCumpliVdor >= 60 && $sacaMes == '06' && $añoFac == '2021' || $porceCumpliVdor >= 60 && $sacaMes == '07' && $añoFac == '2021') {
          $liquidacion_PPTO = 0.0035;
        }else {
          $liquidacion_PPTO = 0;
        }

          //echo $numeFac." / ".$beseComisionApro." / ".$fecha_doc." / ".$dias051." / ".$sacaMes." / ".$porceCumpliVdor." / ".$liquidacion_PPTO." / ".$porcentaje_liqui_recaudo."<br>";

          $comision_PPTO_Diego = $beseComisionApro*$liquidacion_PPTO*($porcentaje_liqui_recaudo/100)/1.81;
          $Total_comision_PPTO_Diego += $comision_PPTO_Diego;

      }
        
      $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $venDor051);  
      $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $agencia_051);
      $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $Umer);
      $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $mer);
      $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $numeRac);
      $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $fecha051);
      $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $codigo051);
      $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $cliente051);
      $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $valorAplicado051);
      $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, $beseComisionApro);    
      $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $tipo);
      $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, $numeFac);
      $objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, $fecha_doc);
      $objPHPExcel->getActiveSheet()->setCellValue('N'.$fila, $dias051);
      $objPHPExcel->getActiveSheet()->setCellValue('O'.$fila, $sacaMes);
      $objPHPExcel->getActiveSheet()->setCellValue('P'.$fila, $porceCumpliVdor."%");
      $objPHPExcel->getActiveSheet()->setCellValue('Q'.$fila, $liquidacion_PPTO);
      $objPHPExcel->getActiveSheet()->setCellValue('R'.$fila, $porcentaje_liqui_recaudo."%");
      $objPHPExcel->getActiveSheet()->setCellValue('S'.$fila, $comision_PPTO_Diego);
      $objPHPExcel->getActiveSheet()->setCellValue('T'.$fila, $porceApli);
            
      $fila++;  

    }  

    }elseif ($RowConso051['porcentaje'] == 5) {
      $agencia_051 = '051';
      $Umer = $RowConso051['merdo'];
      $mer = $Umer."51";
      $fecha051 = $RowConso051['fecha'];

      $codigo051 = $RowConso051['codigo'];
      $length = 9;
      $codigoV_CLI_051 = substr(str_repeat(0, $length).$codigo051, - $length);

      $cliente051 = $RowConso051['cliente'];
      $valorAplicado051 = $RowConso051['valor_aplicado'];
      $tipo = $RowConso051['tipo'];
      $numeRac = $RowConso051['rec'];
      $numeFac = $RowConso051['numero_']; 
      $beseComisionApro = $RowConso051['base_trans'];
      $dias051 = $RowConso051['dias'];
      $fecha_doc = $RowConso051['fecha_doc'];
      $sacaMes = date('m', strtotime($fecha_doc));
      $añoFac = date('Y', strtotime($fecha_doc));
      $porceApli = $RowConso051['porcentaje'];

      $AsVddor051 = "SELECT DISTINCT codigo, cliente, vendedor FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_051'";
      $resultAsVddor051 = mysqli_query($con, $AsVddor051);

      while ($rowAsVddor051 = mysqli_fetch_array($resultAsVddor051)) {
        $venDor051 = $rowAsVddor051['vendedor'];
      } 

        if ($venDor051 == 'TV') {
          if ($dias051 <= 90) {
            $porcentaje_liqui_recaudo = 100;
        }elseif ($dias051 > 120) {
            $porcentaje_liqui_recaudo = 0;
        } else {
          $porcentaje_liqui_recaudo = 80;
        }          
       

        //echo $sacaMes."<br>";

        $consultaPPVdor = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes' AND anio = '$anio' AND idVendedor = 5";
        $resultadoPPVdor = mysqli_query($con, $consultaPPVdor);

        while ($RowPPVdor = mysqli_fetch_array($resultadoPPVdor)) {

          $porceCumpliVdor = $RowPPVdor['porcentaje'];

          if ($porceCumpliVdor  > 100) {
            $liquidacion_PPTO = 0.009;
        }elseif ($porceCumpliVdor >= 80) {
            $liquidacion_PPTO = 0.007;
        }elseif ($porceCumpliVdor >= 60 && $sacaMes == '05' && $añoFac == '2021' || $porceCumpliVdor >= 60 && $sacaMes == '06' && $añoFac == '2021' || $porceCumpliVdor >= 60 && $sacaMes == '07' && $añoFac == '2021') {
          $liquidacion_PPTO = 0.0035;
        }else {
          $liquidacion_PPTO = 0;
        }

          //echo $numeFac." / ".$beseComisionApro." / ".$fecha_doc." / ".$dias051." / ".$sacaMes." / ".$porceCumpliVdor." / ".$liquidacion_PPTO." / ".$porcentaje_liqui_recaudo."<br>";

          $comision_PPTO_Diego = $beseComisionApro*$liquidacion_PPTO*($porcentaje_liqui_recaudo/100)/6;
          $Total_comision_PPTO_Diego += $comision_PPTO_Diego;

      }
        
      $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $venDor051);  
      $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $agencia_051);
      $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $Umer);
      $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $mer);
      $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $numeRac);
      $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $fecha051);
      $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $codigo051);
      $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $cliente051);
      $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $valorAplicado051);
      $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, $beseComisionApro);    
      $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $tipo);
      $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, $numeFac);
      $objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, $fecha_doc);
      $objPHPExcel->getActiveSheet()->setCellValue('N'.$fila, $dias051);
      $objPHPExcel->getActiveSheet()->setCellValue('O'.$fila, $sacaMes);
      $objPHPExcel->getActiveSheet()->setCellValue('P'.$fila, $porceCumpliVdor."%");
      $objPHPExcel->getActiveSheet()->setCellValue('Q'.$fila, $liquidacion_PPTO);
      $objPHPExcel->getActiveSheet()->setCellValue('R'.$fila, $porcentaje_liqui_recaudo."%");
      $objPHPExcel->getActiveSheet()->setCellValue('S'.$fila, $comision_PPTO_Diego);
      $objPHPExcel->getActiveSheet()->setCellValue('T'.$fila, $porceApli);
            
      $fila++;  

    }  

    }elseif ($RowConso051['porcentaje'] == 20) {
      $agencia_051 = '051';
      $Umer = $RowConso051['merdo'];
      $mer = $Umer."51";
      $fecha051 = $RowConso051['fecha'];

      $codigo051 = $RowConso051['codigo'];
      $length = 9;
      $codigoV_CLI_051 = substr(str_repeat(0, $length).$codigo051, - $length);

      $cliente051 = $RowConso051['cliente'];
      $valorAplicado051 = $RowConso051['valor_aplicado'];
      $tipo = $RowConso051['tipo'];
      $numeRac = $RowConso051['rec'];
      $numeFac = $RowConso051['numero_']; 
      $beseComisionApro = $RowConso051['base_trans'];
      $dias051 = $RowConso051['dias'];
      $fecha_doc = $RowConso051['fecha_doc'];
      $sacaMes = date('m', strtotime($fecha_doc));
      $añoFac = date('Y', strtotime($fecha_doc));
      $porceApli = $RowConso051['porcentaje'];

      $AsVddor051 = "SELECT DISTINCT codigo, cliente, vendedor FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_051'";
      $resultAsVddor051 = mysqli_query($con, $AsVddor051);

      while ($rowAsVddor051 = mysqli_fetch_array($resultAsVddor051)) {
        $venDor051 = $rowAsVddor051['vendedor'];
      } 

        if ($venDor051 == 'TV') {
          if ($dias051 <= 90) {
            $porcentaje_liqui_recaudo = 100;
        }elseif ($dias051 > 120) {
            $porcentaje_liqui_recaudo = 0;
        } else {
          $porcentaje_liqui_recaudo = 80;
        }          
       

        //echo $sacaMes."<br>";

        $consultaPPVdor = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes' AND anio = '$anio' AND idVendedor = 5";
        $resultadoPPVdor = mysqli_query($con, $consultaPPVdor);

        while ($RowPPVdor = mysqli_fetch_array($resultadoPPVdor)) {

          $porceCumpliVdor = $RowPPVdor['porcentaje'];

          if ($porceCumpliVdor  > 100) {
            $liquidacion_PPTO = 0.009;
        }elseif ($porceCumpliVdor >= 80) {
            $liquidacion_PPTO = 0.007;
        }elseif ($porceCumpliVdor >= 60 && $sacaMes == '05' && $añoFac == '2021' || $porceCumpliVdor >= 60 && $sacaMes == '06' && $añoFac == '2021' || $porceCumpliVdor >= 60 && $sacaMes == '07' && $añoFac == '2021') {
          $liquidacion_PPTO = 0.0035;
        }else {
          $liquidacion_PPTO = 0;
        }

          //echo $numeFac." / ".$beseComisionApro." / ".$fecha_doc." / ".$dias051." / ".$sacaMes." / ".$porceCumpliVdor." / ".$liquidacion_PPTO." / ".$porcentaje_liqui_recaudo."<br>";

          $comision_PPTO_Diego = $beseComisionApro*$liquidacion_PPTO*($porcentaje_liqui_recaudo/100)/1.5;
          $Total_comision_PPTO_Diego += $comision_PPTO_Diego;

      }
        
      $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $venDor051);  
      $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $agencia_051);
      $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $Umer);
      $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $mer);
      $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $numeRac);
      $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $fecha051);
      $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $codigo051);
      $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $cliente051);
      $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $valorAplicado051);
      $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, $beseComisionApro);    
      $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $tipo);
      $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, $numeFac);
      $objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, $fecha_doc);
      $objPHPExcel->getActiveSheet()->setCellValue('N'.$fila, $dias051);
      $objPHPExcel->getActiveSheet()->setCellValue('O'.$fila, $sacaMes);
      $objPHPExcel->getActiveSheet()->setCellValue('P'.$fila, $porceCumpliVdor."%");
      $objPHPExcel->getActiveSheet()->setCellValue('Q'.$fila, $liquidacion_PPTO);
      $objPHPExcel->getActiveSheet()->setCellValue('R'.$fila, $porcentaje_liqui_recaudo."%");
      $objPHPExcel->getActiveSheet()->setCellValue('S'.$fila, $comision_PPTO_Diego);
      $objPHPExcel->getActiveSheet()->setCellValue('T'.$fila, $porceApli);
            
      $fila++;  

      }  

     }
    } 
  //TERMINA CALCULO COMISIONES MESSER TOBERIN................................................................................................................................................................................................
  
  //CALCULO COMISIONES MESSER CAZUCA................................................................................................................................................................................

    $consultaConso060 = "SELECT * FROM comisiones_cazuca WHERE mes = '$mesC' AND dias != ''";
    $resultadoConso060 = mysqli_query($con, $consultaConso060);

    while ($RowConso060 = mysqli_fetch_array($resultadoConso060)) {       
     if ($RowConso060['dias'] != '' && $RowConso060['porc_3'] != '' && $RowConso060['porc_4'] != '') {
         
         $agencia060 = '060';
         $Umer060 = $RowConso060['merc'];
         $mer060 = $Umer060."60";
         $fecha060 = $RowConso060['fecha'];

         $codigo060 = $RowConso060['codigo'];
         $length060 = 9;
         $codigoV_CLI_060 = substr(str_repeat(0, $length060).$codigo060, - $length060);


         $cliente060 = $RowConso060['cliente'];
         $valor_aplicado_rec = $RowConso060['valor_aplicado_rec'];
         $tipo_060 = $RowConso060['tipo_'];
         $numeRac060 = $RowConso060['recibo'];
         $numeFac060 = $RowConso060['numero']; 
         $beseComisionApro060 = $RowConso060['base_com'];
         $dias060 = $RowConso060['dias'];
         $fecha_doc060 = $RowConso060['fecha_doc'];
         $sacaMes060 = date('m', strtotime($fecha_doc060));
         $sacaAnio060 = date('Y', strtotime($fecha_doc060));
         $porceApli060 = $RowConso060['porcentaje'];  
        
         $AsVddor060 = "SELECT DISTINCT codigo, cliente, vendedor FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_060'";
         $resultAsVddor060 = mysqli_query($con, $AsVddor060);
   
         while ($rowAsVddor060 = mysqli_fetch_array($resultAsVddor060)) {
          $venDor060 = $rowAsVddor060['vendedor'];
        }
           if ($venDor060 == 'TV') {   
               if ($dias060 <= 90) {
                $porcentaje_liqui_recaudo060 = 100;
             }elseif ($dias060 > 120) {
                $porcentaje_liqui_recaudo060 = 0;
             } else {
              $porcentaje_liqui_recaudo060 = 80;
             }
    
             $consultaPPVdor060 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes060' AND anio = '$anio' AND idVendedor = 5";
             $resultadoPPVdor060 = mysqli_query($con, $consultaPPVdor060);
    
             while ($RowPPVdor060 = mysqli_fetch_array($resultadoPPVdor060)) {
    
              $porceCumpliVdor060 = $RowPPVdor060['porcentaje'];                         
              
              if ($porceCumpliVdor060  > 100) {
                $liquidacion_PPTO060 = 0.009;
                }elseif ($porceCumpliVdor060 >= 80) {
                    $liquidacion_PPTO060 = 0.007;
                }elseif ($porceCumpliVdor060 >= 60 && $sacaMes060 == '05' && $sacaAnio060 == '2021' || $porceCumpliVdor060 >= 60 && $sacaMes060 == '06' && $sacaAnio060 == '2021' || $porceCumpliVdor060 >= 60 && $sacaMes060 == '07' && $sacaAnio060 == '2021') {
                  $liquidacion_PPTO060 = 0.0035;
                }else {
                  $liquidacion_PPTO060 = 0;
                }
        
              //echo $numeRac060." - ".$numeFac060." - ".$beseComisionApro060." - ".$dias060." - "." ( ".$sacaMes060." ) "." - ".$porceCumpliVdor060."%"." - ".$liquidacion_PPTO060." - ".$porcentaje_liqui_recaudo060."%"."<br>";
    
              $comision_PPTO_Diego060 = $beseComisionApro060*$liquidacion_PPTO060*($porcentaje_liqui_recaudo060/100);
              $Total_comision_PPTO_Diego060 += $comision_PPTO_Diego060;
              //echo $comision_PPTO_Diego060."<br>";        
    
            } 

            $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $venDor060);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $agencia060);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $Umer060);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $mer060);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $numeRac060);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $fecha060);
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $codigo060);
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $cliente060);    
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $valor_aplicado_rec);
            $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, $beseComisionApro060);    
            $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $tipo_060);
            $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, $numeFac060);
            $objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, $fecha_doc060);
            $objPHPExcel->getActiveSheet()->setCellValue('N'.$fila, $dias060);
            $objPHPExcel->getActiveSheet()->setCellValue('O'.$fila, $sacaMes060);
            $objPHPExcel->getActiveSheet()->setCellValue('P'.$fila, $porceCumpliVdor060."%");
            $objPHPExcel->getActiveSheet()->setCellValue('Q'.$fila, $liquidacion_PPTO060);
            $objPHPExcel->getActiveSheet()->setCellValue('R'.$fila, $porcentaje_liqui_recaudo060."%");
            $objPHPExcel->getActiveSheet()->setCellValue('S'.$fila, $comision_PPTO_Diego060);
            $objPHPExcel->getActiveSheet()->setCellValue('T'.$fila, $porceApli060);
            
            $fila++;
           }         
         
      }elseif ($RowConso060['porcentaje'] == 10) { 

        $agencia060 = '060';
         $Umer060 = $RowConso060['merc'];
         $mer060 = $Umer060."60";
         $fecha060 = $RowConso060['fecha'];

         $codigo060 = $RowConso060['codigo'];
         $length060 = 9;
         $codigoV_CLI_060 = substr(str_repeat(0, $length060).$codigo060, - $length060);


         $cliente060 = $RowConso060['cliente'];
         $valor_aplicado_rec = $RowConso060['valor_aplicado_rec'];
         $tipo_060 = $RowConso060['tipo_'];
         $numeRac060 = $RowConso060['recibo'];
         $numeFac060 = $RowConso060['numero']; 
         $beseComisionApro060 = $RowConso060['base_com'];
         $dias060 = $RowConso060['dias'];
         $fecha_doc060 = $RowConso060['fecha_doc'];
         $sacaMes060 = date('m', strtotime($fecha_doc060));
         $sacaAnio060 = date('Y', strtotime($fecha_doc060));
         $porceApli060 = $RowConso060['porcentaje'];  
        
         $AsVddor060 = "SELECT DISTINCT codigo, cliente, vendedor FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_060'";
         $resultAsVddor060 = mysqli_query($con, $AsVddor060);
   
         while ($rowAsVddor060 = mysqli_fetch_array($resultAsVddor060)) {
          $venDor060 = $rowAsVddor060['vendedor'];
        }
           if ($venDor060 == 'TV') {   
               if ($dias060 <= 90) {
                $porcentaje_liqui_recaudo060 = 100;
             }elseif ($dias060 > 120) {
                $porcentaje_liqui_recaudo060 = 0;
             } else {
              $porcentaje_liqui_recaudo060 = 80;
             }
    
             $consultaPPVdor060 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes060' AND anio = '$anio' AND idVendedor = 5";
             $resultadoPPVdor060 = mysqli_query($con, $consultaPPVdor060);
    
             while ($RowPPVdor060 = mysqli_fetch_array($resultadoPPVdor060)) {
    
              $porceCumpliVdor060 = $RowPPVdor060['porcentaje'];                         
              
              if ($porceCumpliVdor060  > 100) {
                $liquidacion_PPTO060 = 0.009;
                }elseif ($porceCumpliVdor060 >= 80) {
                    $liquidacion_PPTO060 = 0.007;
                }elseif ($porceCumpliVdor060 >= 60 && $sacaMes060 == '05' && $sacaAnio060 == '2021' || $porceCumpliVdor060 >= 60 && $sacaMes060 == '06' && $sacaAnio060 == '2021' || $porceCumpliVdor060 >= 60 && $sacaMes060 == '07' && $sacaAnio060 == '2021') {
                  $liquidacion_PPTO060 = 0.0035;
                }else {
                  $liquidacion_PPTO060 = 0;
                }
        
              //echo $numeRac060." - ".$numeFac060." - ".$beseComisionApro060." - ".$dias060." - "." ( ".$sacaMes060." ) "." - ".$porceCumpliVdor060."%"." - ".$liquidacion_PPTO060." - ".$porcentaje_liqui_recaudo060."%"."<br>";
    
              $comision_PPTO_Diego060 = $beseComisionApro060*$liquidacion_PPTO060*($porcentaje_liqui_recaudo060/100)/3;
              $Total_comision_PPTO_Diego060 += $comision_PPTO_Diego060;
              //echo $comision_PPTO_Diego060."<br>";        
    
            } 

            $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $venDor060);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $agencia060);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $Umer060);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $mer060);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $numeRac060);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $fecha060);
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $codigo060);
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $cliente060);    
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $valor_aplicado_rec);
            $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, $beseComisionApro060);    
            $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $tipo_060);
            $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, $numeFac060);
            $objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, $fecha_doc060);
            $objPHPExcel->getActiveSheet()->setCellValue('N'.$fila, $dias060);
            $objPHPExcel->getActiveSheet()->setCellValue('O'.$fila, $sacaMes060);
            $objPHPExcel->getActiveSheet()->setCellValue('P'.$fila, $porceCumpliVdor060."%");
            $objPHPExcel->getActiveSheet()->setCellValue('Q'.$fila, $liquidacion_PPTO060);
            $objPHPExcel->getActiveSheet()->setCellValue('R'.$fila, $porcentaje_liqui_recaudo060."%");
            $objPHPExcel->getActiveSheet()->setCellValue('S'.$fila, $comision_PPTO_Diego060);
            $objPHPExcel->getActiveSheet()->setCellValue('T'.$fila, $porceApli060);
            
            $fila++;
           }
      }elseif ($RowConso060['porcentaje'] == 16.5) {               
  
        $agencia060 = '060';
         $Umer060 = $RowConso060['merc'];
         $mer060 = $Umer060."60";
         $fecha060 = $RowConso060['fecha'];

         $codigo060 = $RowConso060['codigo'];
         $length060 = 9;
         $codigoV_CLI_060 = substr(str_repeat(0, $length060).$codigo060, - $length060);


         $cliente060 = $RowConso060['cliente'];
         $valor_aplicado_rec = $RowConso060['valor_aplicado_rec'];
         $tipo_060 = $RowConso060['tipo_'];
         $numeRac060 = $RowConso060['recibo'];
         $numeFac060 = $RowConso060['numero']; 
         $beseComisionApro060 = $RowConso060['base_com'];
         $dias060 = $RowConso060['dias'];
         $fecha_doc060 = $RowConso060['fecha_doc'];
         $sacaMes060 = date('m', strtotime($fecha_doc060));
         $sacaAnio060 = date('Y', strtotime($fecha_doc060));
         $porceApli060 = $RowConso060['porcentaje'];  
        
         $AsVddor060 = "SELECT DISTINCT codigo, cliente, vendedor FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_060'";
         $resultAsVddor060 = mysqli_query($con, $AsVddor060);
   
         while ($rowAsVddor060 = mysqli_fetch_array($resultAsVddor060)) {
          $venDor060 = $rowAsVddor060['vendedor'];
        }
           if ($venDor060 == 'TV') {   
               if ($dias060 <= 90) {
                $porcentaje_liqui_recaudo060 = 100;
             }elseif ($dias060 > 120) {
                $porcentaje_liqui_recaudo060 = 0;
             } else {
              $porcentaje_liqui_recaudo060 = 80;
             }
    
             $consultaPPVdor060 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes060' AND anio = '$anio' AND idVendedor = 5";
             $resultadoPPVdor060 = mysqli_query($con, $consultaPPVdor060);
    
             while ($RowPPVdor060 = mysqli_fetch_array($resultadoPPVdor060)) {
    
              $porceCumpliVdor060 = $RowPPVdor060['porcentaje'];                         
              
              if ($porceCumpliVdor060  > 100) {
                $liquidacion_PPTO060 = 0.009;
                }elseif ($porceCumpliVdor060 >= 80) {
                    $liquidacion_PPTO060 = 0.007;
                }elseif ($porceCumpliVdor060 >= 60 && $sacaMes060 == '05' && $sacaAnio060 == '2021' || $porceCumpliVdor060 >= 60 && $sacaMes060 == '06' && $sacaAnio060 == '2021' || $porceCumpliVdor060 >= 60 && $sacaMes060 == '07' && $sacaAnio060 == '2021') {
                  $liquidacion_PPTO060 = 0.0035;
                }else {
                  $liquidacion_PPTO060 = 0;
                }
        
              //echo $numeRac060." - ".$numeFac060." - ".$beseComisionApro060." - ".$dias060." - "." ( ".$sacaMes060." ) "." - ".$porceCumpliVdor060."%"." - ".$liquidacion_PPTO060." - ".$porcentaje_liqui_recaudo060."%"."<br>";
    
              $comision_PPTO_Diego060 = $beseComisionApro060*$liquidacion_PPTO060*($porcentaje_liqui_recaudo060/100)/1.81;
              $Total_comision_PPTO_Diego060 += $comision_PPTO_Diego060;
              //echo $comision_PPTO_Diego060."<br>";        
    
            } 

            $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $venDor060);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $agencia060);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $Umer060);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $mer060);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $numeRac060);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $fecha060);
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $codigo060);
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $cliente060);    
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $valor_aplicado_rec);
            $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, $beseComisionApro060);    
            $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $tipo_060);
            $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, $numeFac060);
            $objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, $fecha_doc060);
            $objPHPExcel->getActiveSheet()->setCellValue('N'.$fila, $dias060);
            $objPHPExcel->getActiveSheet()->setCellValue('O'.$fila, $sacaMes060);
            $objPHPExcel->getActiveSheet()->setCellValue('P'.$fila, $porceCumpliVdor060."%");
            $objPHPExcel->getActiveSheet()->setCellValue('Q'.$fila, $liquidacion_PPTO060);
            $objPHPExcel->getActiveSheet()->setCellValue('R'.$fila, $porcentaje_liqui_recaudo060."%");
            $objPHPExcel->getActiveSheet()->setCellValue('S'.$fila, $comision_PPTO_Diego060);
            $objPHPExcel->getActiveSheet()->setCellValue('T'.$fila, $porceApli060);
            
            $fila++;
           }  
      }elseif ($RowConso060['porcentaje'] == 5) {

        $agencia060 = '060';
        $Umer060 = $RowConso060['merc'];
        $mer060 = $Umer060."60";
        $fecha060 = $RowConso060['fecha'];

        $codigo060 = $RowConso060['codigo'];
        $length060 = 9;
        $codigoV_CLI_060 = substr(str_repeat(0, $length060).$codigo060, - $length060);


        $cliente060 = $RowConso060['cliente'];
        $valor_aplicado_rec = $RowConso060['valor_aplicado_rec'];
        $tipo_060 = $RowConso060['tipo_'];
        $numeRac060 = $RowConso060['recibo'];
        $numeFac060 = $RowConso060['numero']; 
        $beseComisionApro060 = $RowConso060['base_com'];
        $dias060 = $RowConso060['dias'];
        $fecha_doc060 = $RowConso060['fecha_doc'];
        $sacaMes060 = date('m', strtotime($fecha_doc060));
        $sacaAnio060 = date('Y', strtotime($fecha_doc060));  
        $porceApli060 = $RowConso060['porcentaje'];
       
        $AsVddor060 = "SELECT DISTINCT codigo, cliente, vendedor FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_060'";
        $resultAsVddor060 = mysqli_query($con, $AsVddor060);
  
        while ($rowAsVddor060 = mysqli_fetch_array($resultAsVddor060)) {
         $venDor060 = $rowAsVddor060['vendedor'];
       }
          if ($venDor060 == 'TV') {   
              if ($dias060 <= 90) {
               $porcentaje_liqui_recaudo060 = 100;
            }elseif ($dias060 > 120) {
               $porcentaje_liqui_recaudo060 = 0;
            } else {
             $porcentaje_liqui_recaudo060 = 80;
            }
   
            $consultaPPVdor060 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes060' AND anio = '$anio' AND idVendedor = 5";
            $resultadoPPVdor060 = mysqli_query($con, $consultaPPVdor060);
   
            while ($RowPPVdor060 = mysqli_fetch_array($resultadoPPVdor060)) {
   
             $porceCumpliVdor060 = $RowPPVdor060['porcentaje'];                         
             
             if ($porceCumpliVdor060  > 100) {
               $liquidacion_PPTO060 = 0.009;
               }elseif ($porceCumpliVdor060 >= 80) {
                   $liquidacion_PPTO060 = 0.007;
               }elseif ($porceCumpliVdor060 >= 60 && $sacaMes060 == '05' && $sacaAnio060 == '2021' || $porceCumpliVdor060 >= 60 && $sacaMes060 == '06' && $sacaAnio060 == '2021' || $porceCumpliVdor060 >= 60 && $sacaMes060 == '07' && $sacaAnio060 == '2021') {
                 $liquidacion_PPTO060 = 0.0035;
               }else {
                 $liquidacion_PPTO060 = 0;
               }
       
             //echo $numeRac060." - ".$numeFac060." - ".$beseComisionApro060." - ".$dias060." - "." ( ".$sacaMes060." ) "." - ".$porceCumpliVdor060."%"." - ".$liquidacion_PPTO060." - ".$porcentaje_liqui_recaudo060."%"."<br>";
   
             $comision_PPTO_Diego060 = $beseComisionApro060*$liquidacion_PPTO060*($porcentaje_liqui_recaudo060/100)/6;
             $Total_comision_PPTO_Diego060 += $comision_PPTO_Diego060;
             //echo $comision_PPTO_Diego060."<br>";        
   
           } 

           $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $venDor060);
           $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $agencia060);
           $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $Umer060);
           $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $mer060);
           $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $numeRac060);
           $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $fecha060);
           $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $codigo060);
           $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $cliente060);    
           $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $valor_aplicado_rec);
           $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, $beseComisionApro060);    
           $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $tipo_060);
           $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, $numeFac060);
           $objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, $fecha_doc060);
           $objPHPExcel->getActiveSheet()->setCellValue('N'.$fila, $dias060);
           $objPHPExcel->getActiveSheet()->setCellValue('O'.$fila, $sacaMes060);
           $objPHPExcel->getActiveSheet()->setCellValue('P'.$fila, $porceCumpliVdor060."%");
           $objPHPExcel->getActiveSheet()->setCellValue('Q'.$fila, $liquidacion_PPTO060);
           $objPHPExcel->getActiveSheet()->setCellValue('R'.$fila, $porcentaje_liqui_recaudo060."%");
           $objPHPExcel->getActiveSheet()->setCellValue('S'.$fila, $comision_PPTO_Diego060);
           $objPHPExcel->getActiveSheet()->setCellValue('T'.$fila, $porceApli060);
           
           $fila++;
          }
      }elseif ($RowConso060['porcentaje'] == 20) {

        $agencia060 = '060';
         $Umer060 = $RowConso060['merc'];
         $mer060 = $Umer060."60";
         $fecha060 = $RowConso060['fecha'];

         $codigo060 = $RowConso060['codigo'];
         $length060 = 9;
         $codigoV_CLI_060 = substr(str_repeat(0, $length060).$codigo060, - $length060);


         $cliente060 = $RowConso060['cliente'];
         $valor_aplicado_rec = $RowConso060['valor_aplicado_rec'];
         $tipo_060 = $RowConso060['tipo_'];
         $numeRac060 = $RowConso060['recibo'];
         $numeFac060 = $RowConso060['numero']; 
         $beseComisionApro060 = $RowConso060['base_com'];
         $dias060 = $RowConso060['dias'];
         $fecha_doc060 = $RowConso060['fecha_doc'];
         $sacaMes060 = date('m', strtotime($fecha_doc060));
         $sacaAnio060 = date('Y', strtotime($fecha_doc060)); 
         $porceApli060 = $RowConso060['porcentaje']; 
        
         $AsVddor060 = "SELECT DISTINCT codigo, cliente, vendedor FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_060'";
         $resultAsVddor060 = mysqli_query($con, $AsVddor060);
   
         while ($rowAsVddor060 = mysqli_fetch_array($resultAsVddor060)) {
          $venDor060 = $rowAsVddor060['vendedor'];
        }
           if ($venDor060 == 'TV') {   
               if ($dias060 <= 90) {
                $porcentaje_liqui_recaudo060 = 100;
             }elseif ($dias060 > 120) {
                $porcentaje_liqui_recaudo060 = 0;
             } else {
              $porcentaje_liqui_recaudo060 = 80;
             }
    
             $consultaPPVdor060 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes060' AND anio = '$anio' AND idVendedor = 5";
             $resultadoPPVdor060 = mysqli_query($con, $consultaPPVdor060);
    
             while ($RowPPVdor060 = mysqli_fetch_array($resultadoPPVdor060)) {
    
              $porceCumpliVdor060 = $RowPPVdor060['porcentaje'];                         
              
              if ($porceCumpliVdor060  > 100) {
                $liquidacion_PPTO060 = 0.009;
                }elseif ($porceCumpliVdor060 >= 80) {
                    $liquidacion_PPTO060 = 0.007;
                }elseif ($porceCumpliVdor060 >= 60 && $sacaMes060 == '05' && $sacaAnio060 == '2021' || $porceCumpliVdor060 >= 60 && $sacaMes060 == '06' && $sacaAnio060 == '2021' || $porceCumpliVdor060 >= 60 && $sacaMes060 == '07' && $sacaAnio060 == '2021') {
                  $liquidacion_PPTO060 = 0.0035;
                }else {
                  $liquidacion_PPTO060 = 0;
                }
        
              //echo $numeRac060." - ".$numeFac060." - ".$beseComisionApro060." - ".$dias060." - "." ( ".$sacaMes060." ) "." - ".$porceCumpliVdor060."%"." - ".$liquidacion_PPTO060." - ".$porcentaje_liqui_recaudo060."%"."<br>";
    
              $comision_PPTO_Diego060 = $beseComisionApro060*$liquidacion_PPTO060*($porcentaje_liqui_recaudo060/100)/1.5;
              $Total_comision_PPTO_Diego060 += $comision_PPTO_Diego060;
              //echo $comision_PPTO_Diego060."<br>";        
    
            } 

            $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $venDor060);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $agencia060);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $Umer060);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $mer060);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $numeRac060);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $fecha060);
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $codigo060);
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $cliente060);    
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $valor_aplicado_rec);
            $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, $beseComisionApro060);    
            $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $tipo_060);
            $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, $numeFac060);
            $objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, $fecha_doc060);
            $objPHPExcel->getActiveSheet()->setCellValue('N'.$fila, $dias060);
            $objPHPExcel->getActiveSheet()->setCellValue('O'.$fila, $sacaMes060);
            $objPHPExcel->getActiveSheet()->setCellValue('P'.$fila, $porceCumpliVdor060."%");
            $objPHPExcel->getActiveSheet()->setCellValue('Q'.$fila, $liquidacion_PPTO060);
            $objPHPExcel->getActiveSheet()->setCellValue('R'.$fila, $porcentaje_liqui_recaudo060."%");
            $objPHPExcel->getActiveSheet()->setCellValue('S'.$fila, $comision_PPTO_Diego060);
            $objPHPExcel->getActiveSheet()->setCellValue('T'.$fila, $porceApli060);

            $fila++;
           }
      }      
    } 
  //TERMINA CALCULO COMISIONES MESSER CAZUCA................................................................................................................................................................................................

  //CALCULO COMISIONES MESSER MANIZALES................................................................................................................................................................................

  $consultaConso370 = "SELECT * FROM comisiones_manizales WHERE mes = '$mesC' AND dias_transcurridos != ''";
  $resultadoConso370 = mysqli_query($con, $consultaConso370);

  while ($RowConso370 = mysqli_fetch_array($resultadoConso370)) {       
    if ($RowConso370['dias_transcurridos'] != '' && $RowConso370['porc_3'] != '' && $RowConso370['porc_4'] != '') {
        
        $agencia370 = '370';
        $Umer370 = $RowConso370['mercado'];
        $mer370 = $Umer370."370";
        $fecha370 = $RowConso370['fecha_aplicacion_pago'];
        $cliente370 = $RowConso370['cliente'];
        $codigo370 = $RowConso370['codigo_cliente'];
        $length = 9;
        $valor_aplicado370 = $RowConso370['valor_aplicado'];
        $tipo_370 = $RowConso370['doc'];
        $numeRac370 = $RowConso370['rec'];
        $numeFac370 = $RowConso370['no_fact']; 
        $beseComisionApro370 = $RowConso370['base_com'];
        $dias370 = $RowConso370['dias_transcurridos'];
        $fecha_doc370 = $RowConso370['fecha_doc'];
        $sacaMes370 = date('m', strtotime($fecha_doc370));
        $sacaAnio370 = date('Y', strtotime($fecha_doc370));
        $codigoV_CLI_370 = substr(str_repeat(0, $length).$codigo370, - $length);
        $porceApli370 = $RowConso370['porcentaje'];
 
        $consultaVEndedor = "SELECT DISTINCT codigo, cliente, vendedor FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_370'";
        $resultAsVddor370 = mysqli_query($con, $consultaVEndedor);
 
        while ($rowAsVddor370 = mysqli_fetch_array($resultAsVddor370)) { 
             $vende370 = $rowAsVddor370['vendedor']; 
         }
         if ($vende370 == 'TV') {
               //echo $numeRac." - ".$numeFac." - ".$beseComisionApro." - ".$dias051."<br>";
 
               if ($dias370 <= 90) {
                 $porcentaje_liqui_recaudo370 = 100;
               }elseif ($dias370 > 120) {
                 $porcentaje_liqui_recaudo370 = 0;
               } else {
               $porcentaje_liqui_recaudo370 = 80;
               }
 
               $consultaPPVdor370 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes370' AND anio = '$anio' AND idVendedor = 5";
               $resultadoPPVdor370 = mysqli_query($con, $consultaPPVdor370);
 
               while ($RowPPVdor370 = mysqli_fetch_array($resultadoPPVdor370)) {
 
               $porceCumpliVdor370 = $RowPPVdor370['porcentaje'];
                         
               if ($porceCumpliVdor370 > 100) {
                 $liquidacion_PPTO370 = 0.009;
               }elseif ($porceCumpliVdor370 >= 80) {
                 $liquidacion_PPTO370 = 0.007;
               }elseif ($porceCumpliVdor370 <= 79 && $sacaMes370 == '05' && $sacaAnio370 == '2021' || $porceCumpliVdor370 <= 79 && $sacaMes370 == '06' && $sacaAnio370 == '2021' || $porceCumpliVdor370 <= 79 && $sacaMes370 == '07' && $sacaAnio370 == '2021') {
                 $liquidacion_PPTO370 = 0.0035;
               }else{
                 $liquidacion_PPTO370 = 0;
               }
 
               //echo $numeRac370." - ".$numeFac370." - ".$beseComisionApro370." - ".$dias370." - "." ( ".$sacaMes370." ) "." - ".$porceCumpliVdor370."%"." - ".$liquidacion_PPTO370." - ".$porcentaje_liqui_recaudo370."%"."<br>";
 
               $comision_PPTO_Diego370 = $beseComisionApro370*$liquidacion_PPTO370*($porcentaje_liqui_recaudo370/100);
               $Total_comision_PPTO_Diego370 += $comision_PPTO_Diego370;
               //echo $comision_PPTO_Diego370."<br>";        
 
               }
               $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $vende370);
               $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $agencia370);
               $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $Umer370);
               $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $mer370);
               $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $numeRac370);
               $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $fecha370);
               $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $codigo370);
               $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $cliente370);
               $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $valor_aplicado370);
               $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, $beseComisionApro370);
               $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $tipo_370);
               $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, $numeFac370);
               $objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, $fecha_doc370);
               $objPHPExcel->getActiveSheet()->setCellValue('N'.$fila, $dias370);
               $objPHPExcel->getActiveSheet()->setCellValue('O'.$fila, $sacaMes370);
               $objPHPExcel->getActiveSheet()->setCellValue('P'.$fila, $porceCumpliVdor370."%");
               $objPHPExcel->getActiveSheet()->setCellValue('Q'.$fila, $liquidacion_PPTO370);
               $objPHPExcel->getActiveSheet()->setCellValue('R'.$fila, $porcentaje_liqui_recaudo370."%");
               $objPHPExcel->getActiveSheet()->setCellValue('S'.$fila, $comision_PPTO_Diego370);
               $objPHPExcel->getActiveSheet()->setCellValue('T'.$fila, $porceApli370);
               
               
               $fila++;
           }       
 
     }elseif ($RowConso370['porcentaje'] == 10) {
 
        
       $agencia370 = '370';
       $Umer370 = $RowConso370['mercado'];
       $mer370 = $Umer370."370";
       $fecha370 = $RowConso370['fecha_aplicacion_pago'];
       $cliente370 = $RowConso370['cliente'];
       $codigo370 = $RowConso370['codigo_cliente'];
       $length = 9;
       $valor_aplicado370 = $RowConso370['valor_aplicado'];
       $tipo_370 = $RowConso370['doc'];
       $numeRac370 = $RowConso370['rec'];
       $numeFac370 = $RowConso370['no_fact']; 
       $beseComisionApro370 = $RowConso370['base_com'];
       $dias370 = $RowConso370['dias_transcurridos'];
       $fecha_doc370 = $RowConso370['fecha_doc'];
       $sacaMes370 = date('m', strtotime($fecha_doc370));
       $sacaAnio370 = date('Y', strtotime($fecha_doc370));
       $codigoV_CLI_370 = substr(str_repeat(0, $length).$codigo370, - $length);
       $porceApli370 = $RowConso370['porcentaje'];
 
       $consultaVEndedor = "SELECT DISTINCT codigo, cliente, vendedor FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_370'";
       $resultAsVddor370 = mysqli_query($con, $consultaVEndedor);
 
       while ($rowAsVddor370 = mysqli_fetch_array($resultAsVddor370)) { 
            $vende370 = $rowAsVddor370['vendedor']; 
        }
        if ($vende370 == 'TV') {
              //echo $numeRac." - ".$numeFac." - ".$beseComisionApro." - ".$dias051."<br>";
 
              if ($dias370 <= 90) {
                $porcentaje_liqui_recaudo370 = 100;
              }elseif ($dias370 > 120) {
                $porcentaje_liqui_recaudo370 = 0;
              } else {
              $porcentaje_liqui_recaudo370 = 80;
              }
 
              $consultaPPVdor370 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes370' AND anio = '$anio' AND idVendedor = 5";
              $resultadoPPVdor370 = mysqli_query($con, $consultaPPVdor370);
 
              while ($RowPPVdor370 = mysqli_fetch_array($resultadoPPVdor370)) {
 
              $porceCumpliVdor370 = $RowPPVdor370['porcentaje'];
                        
              if ($porceCumpliVdor370 > 100) {
                $liquidacion_PPTO370 = 0.009;
              }elseif ($porceCumpliVdor370 >= 80) {
                $liquidacion_PPTO370 = 0.007;
              }elseif ($porceCumpliVdor370 <= 79 && $sacaMes370 == '05' && $sacaAnio370 == '2021' || $porceCumpliVdor370 <= 79 && $sacaMes370 == '06' && $sacaAnio370 == '2021' || $porceCumpliVdor370 <= 79 && $sacaMes370 == '07' && $sacaAnio370 == '2021') {
                $liquidacion_PPTO370 = 0.0035;
              }else{
                $liquidacion_PPTO370 = 0;
              }
 
              //echo $numeRac370." - ".$numeFac370." - ".$beseComisionApro370." - ".$dias370." - "." ( ".$sacaMes370." ) "." - ".$porceCumpliVdor370."%"." - ".$liquidacion_PPTO370." - ".$porcentaje_liqui_recaudo370."%"."<br>";
 
              $comision_PPTO_Diego370 = $beseComisionApro370*$liquidacion_PPTO370*($porcentaje_liqui_recaudo370/100)/3;
              $Total_comision_PPTO_Diego370 += $comision_PPTO_Diego370;
              //echo $comision_PPTO_Diego370."<br>";        
 
              }
              $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $vende370);
              $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $agencia370);
              $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $Umer370);
              $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $mer370);
              $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $numeRac370);
              $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $fecha370);
              $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $codigo370);
              $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $cliente370);
              $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $valor_aplicado370);
              $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, $beseComisionApro370);
              $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $tipo_370);
              $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, $numeFac370);
              $objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, $fecha_doc370);
              $objPHPExcel->getActiveSheet()->setCellValue('N'.$fila, $dias370);
              $objPHPExcel->getActiveSheet()->setCellValue('O'.$fila, $sacaMes370);
              $objPHPExcel->getActiveSheet()->setCellValue('P'.$fila, $porceCumpliVdor370."%");
              $objPHPExcel->getActiveSheet()->setCellValue('Q'.$fila, $liquidacion_PPTO370);
              $objPHPExcel->getActiveSheet()->setCellValue('R'.$fila, $porcentaje_liqui_recaudo370."%");
              $objPHPExcel->getActiveSheet()->setCellValue('S'.$fila, $comision_PPTO_Diego370);
              $objPHPExcel->getActiveSheet()->setCellValue('T'.$fila, $porceApli370);
              
              $fila++;
          }    
 
     }elseif ($RowConso370['porcentaje'] == 16.5) {               
 
        
       $agencia370 = '370';
       $Umer370 = $RowConso370['mercado'];
       $mer370 = $Umer370."370";
       $fecha370 = $RowConso370['fecha_aplicacion_pago'];
       $cliente370 = $RowConso370['cliente'];
       $codigo370 = $RowConso370['codigo_cliente'];
       $length = 9;
       $valor_aplicado370 = $RowConso370['valor_aplicado'];
       $tipo_370 = $RowConso370['doc'];
       $numeRac370 = $RowConso370['rec'];
       $numeFac370 = $RowConso370['no_fact']; 
       $beseComisionApro370 = $RowConso370['base_com'];
       $dias370 = $RowConso370['dias_transcurridos'];
       $fecha_doc370 = $RowConso370['fecha_doc'];
       $sacaMes370 = date('m', strtotime($fecha_doc370));
       $sacaAnio370 = date('Y', strtotime($fecha_doc370));
       $codigoV_CLI_370 = substr(str_repeat(0, $length).$codigo370, - $length);
       $porceApli370 = $RowConso370['porcentaje'];
 
       $consultaVEndedor = "SELECT DISTINCT codigo, cliente, vendedor FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_370'";
       $resultAsVddor370 = mysqli_query($con, $consultaVEndedor);
 
       while ($rowAsVddor370 = mysqli_fetch_array($resultAsVddor370)) { 
            $vende370 = $rowAsVddor370['vendedor']; 
        }
        if ($vende370 == 'TV') {
              //echo $numeRac." - ".$numeFac." - ".$beseComisionApro." - ".$dias051."<br>";
 
              if ($dias370 <= 90) {
                $porcentaje_liqui_recaudo370 = 100;
              }elseif ($dias370 > 120) {
                $porcentaje_liqui_recaudo370 = 0;
              } else {
              $porcentaje_liqui_recaudo370 = 80;
              }
 
              $consultaPPVdor370 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes370' AND anio = '$anio' AND idVendedor = 5";
              $resultadoPPVdor370 = mysqli_query($con, $consultaPPVdor370);
 
              while ($RowPPVdor370 = mysqli_fetch_array($resultadoPPVdor370)) {
 
              $porceCumpliVdor370 = $RowPPVdor370['porcentaje'];
                        
              if ($porceCumpliVdor370 > 100) {
                $liquidacion_PPTO370 = 0.009;
              }elseif ($porceCumpliVdor370 >= 80) {
                $liquidacion_PPTO370 = 0.007;
              }elseif ($porceCumpliVdor370 <= 79 && $sacaMes370 == '05' && $sacaAnio370 == '2021' || $porceCumpliVdor370 <= 79 && $sacaMes370 == '06' && $sacaAnio370 == '2021' || $porceCumpliVdor370 <= 79 && $sacaMes370 == '07' && $sacaAnio370 == '2021') {
                $liquidacion_PPTO370 = 0.0035;
              }else{
                $liquidacion_PPTO370 = 0;
              }
 
              //echo $numeRac370." - ".$numeFac370." - ".$beseComisionApro370." - ".$dias370." - "." ( ".$sacaMes370." ) "." - ".$porceCumpliVdor370."%"." - ".$liquidacion_PPTO370." - ".$porcentaje_liqui_recaudo370."%"."<br>";
 
              $comision_PPTO_Diego370 = $beseComisionApro370*$liquidacion_PPTO370*($porcentaje_liqui_recaudo370/100)/1.81;
              $Total_comision_PPTO_Diego370 += $comision_PPTO_Diego370;
              //echo $comision_PPTO_Diego370."<br>";        
 
              }
              $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $vende370);
              $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $agencia370);
              $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $Umer370);
              $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $mer370);
              $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $numeRac370);
              $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $fecha370);
              $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $codigo370);
              $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $cliente370);
              $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $valor_aplicado370);
              $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, $beseComisionApro370);
              $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $tipo_370);
              $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, $numeFac370);
              $objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, $fecha_doc370);
              $objPHPExcel->getActiveSheet()->setCellValue('N'.$fila, $dias370);
              $objPHPExcel->getActiveSheet()->setCellValue('O'.$fila, $sacaMes370);
              $objPHPExcel->getActiveSheet()->setCellValue('P'.$fila, $porceCumpliVdor370."%");
              $objPHPExcel->getActiveSheet()->setCellValue('Q'.$fila, $liquidacion_PPTO370);
              $objPHPExcel->getActiveSheet()->setCellValue('R'.$fila, $porcentaje_liqui_recaudo370."%");
              $objPHPExcel->getActiveSheet()->setCellValue('S'.$fila, $comision_PPTO_Diego370);
              $objPHPExcel->getActiveSheet()->setCellValue('T'.$fila, $porceApli370);
              
              $fila++;
          }          
     }elseif ($RowConso370['porcentaje'] == 5) {
 
        
       $agencia370 = '370';
       $Umer370 = $RowConso370['mercado'];
       $mer370 = $Umer370."370";
       $fecha370 = $RowConso370['fecha_aplicacion_pago'];
       $cliente370 = $RowConso370['cliente'];
       $codigo370 = $RowConso370['codigo_cliente'];
       $length = 9;
       $valor_aplicado370 = $RowConso370['valor_aplicado'];
       $tipo_370 = $RowConso370['doc'];
       $numeRac370 = $RowConso370['rec'];
       $numeFac370 = $RowConso370['no_fact']; 
       $beseComisionApro370 = $RowConso370['base_com'];
       $dias370 = $RowConso370['dias_transcurridos'];
       $fecha_doc370 = $RowConso370['fecha_doc'];
       $sacaMes370 = date('m', strtotime($fecha_doc370));
       $sacaAnio370 = date('Y', strtotime($fecha_doc370));
       $codigoV_CLI_370 = substr(str_repeat(0, $length).$codigo370, - $length);
       $porceApli370 = $RowConso370['porcentaje'];
 
       $consultaVEndedor = "SELECT DISTINCT codigo, cliente, vendedor FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_370'";
       $resultAsVddor370 = mysqli_query($con, $consultaVEndedor);
 
       while ($rowAsVddor370 = mysqli_fetch_array($resultAsVddor370)) { 
            $vende370 = $rowAsVddor370['vendedor']; 
        }
        if ($vende370 == 'TV') {
              //echo $numeRac." - ".$numeFac." - ".$beseComisionApro." - ".$dias051."<br>";
 
              if ($dias370 <= 90) {
                $porcentaje_liqui_recaudo370 = 100;
              }elseif ($dias370 > 120) {
                $porcentaje_liqui_recaudo370 = 0;
              } else {
              $porcentaje_liqui_recaudo370 = 80;
              }
 
              $consultaPPVdor370 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes370' AND anio = '$anio' AND idVendedor = 5";
              $resultadoPPVdor370 = mysqli_query($con, $consultaPPVdor370);
 
              while ($RowPPVdor370 = mysqli_fetch_array($resultadoPPVdor370)) {
 
              $porceCumpliVdor370 = $RowPPVdor370['porcentaje'];
                        
              if ($porceCumpliVdor370 > 100) {
                $liquidacion_PPTO370 = 0.009;
              }elseif ($porceCumpliVdor370 >= 80) {
                $liquidacion_PPTO370 = 0.007;
              }elseif ($porceCumpliVdor370 <= 79 && $sacaMes370 == '05' && $sacaAnio370 == '2021' || $porceCumpliVdor370 <= 79 && $sacaMes370 == '06' && $sacaAnio370 == '2021' || $porceCumpliVdor370 <= 79 && $sacaMes370 == '07' && $sacaAnio370 == '2021') {
                $liquidacion_PPTO370 = 0.0035;
              }else{
                $liquidacion_PPTO370 = 0;
              }
 
              //echo $numeRac370." - ".$numeFac370." - ".$beseComisionApro370." - ".$dias370." - "." ( ".$sacaMes370." ) "." - ".$porceCumpliVdor370."%"." - ".$liquidacion_PPTO370." - ".$porcentaje_liqui_recaudo370."%"."<br>";
 
              $comision_PPTO_Diego370 = $beseComisionApro370*$liquidacion_PPTO370*($porcentaje_liqui_recaudo370/100)/6;
              $Total_comision_PPTO_Diego370 += $comision_PPTO_Diego370;
              //echo $comision_PPTO_Diego370."<br>";        
 
              }
              $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $vende370);
              $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $agencia370);
              $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $Umer370);
              $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $mer370);
              $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $numeRac370);
              $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $fecha370);
              $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $codigo370);
              $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $cliente370);
              $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $valor_aplicado370);
              $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, $beseComisionApro370);
              $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $tipo_370);
              $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, $numeFac370);
              $objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, $fecha_doc370);
              $objPHPExcel->getActiveSheet()->setCellValue('N'.$fila, $dias370);
              $objPHPExcel->getActiveSheet()->setCellValue('O'.$fila, $sacaMes370);
              $objPHPExcel->getActiveSheet()->setCellValue('P'.$fila, $porceCumpliVdor370."%");
              $objPHPExcel->getActiveSheet()->setCellValue('Q'.$fila, $liquidacion_PPTO370);
              $objPHPExcel->getActiveSheet()->setCellValue('R'.$fila, $porcentaje_liqui_recaudo370."%");
              $objPHPExcel->getActiveSheet()->setCellValue('S'.$fila, $comision_PPTO_Diego370);
              $objPHPExcel->getActiveSheet()->setCellValue('T'.$fila, $porceApli370);
              
              $fila++;
          }  
       
     }elseif ($RowConso370['porcentaje'] == 20) {
 
        
       $agencia370 = '370';
       $Umer370 = $RowConso370['mercado'];
       $mer370 = $Umer370."370";
       $fecha370 = $RowConso370['fecha_aplicacion_pago'];
       $cliente370 = $RowConso370['cliente'];
       $codigo370 = $RowConso370['codigo_cliente'];
       $length = 9;
       $valor_aplicado370 = $RowConso370['valor_aplicado'];
       $tipo_370 = $RowConso370['doc'];
       $numeRac370 = $RowConso370['rec'];
       $numeFac370 = $RowConso370['no_fact']; 
       $beseComisionApro370 = $RowConso370['base_com'];
       $dias370 = $RowConso370['dias_transcurridos'];
       $fecha_doc370 = $RowConso370['fecha_doc'];
       $sacaMes370 = date('m', strtotime($fecha_doc370));
       $sacaAnio370 = date('Y', strtotime($fecha_doc370));
       $codigoV_CLI_370 = substr(str_repeat(0, $length).$codigo370, - $length);
       $porceApli370 = $RowConso370['porcentaje'];
 
       $consultaVEndedor = "SELECT DISTINCT codigo, cliente, vendedor FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_370'";
       $resultAsVddor370 = mysqli_query($con, $consultaVEndedor);
 
       while ($rowAsVddor370 = mysqli_fetch_array($resultAsVddor370)) { 
            $vende370 = $rowAsVddor370['vendedor']; 
        }
        if ($vende370 == 'TV') {
              //echo $numeRac." - ".$numeFac." - ".$beseComisionApro." - ".$dias051."<br>";
 
              if ($dias370 <= 90) {
                $porcentaje_liqui_recaudo370 = 100;
              }elseif ($dias370 > 120) {
                $porcentaje_liqui_recaudo370 = 0;
              } else {
              $porcentaje_liqui_recaudo370 = 80;
              }
 
              $consultaPPVdor370 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes370' AND anio = '$anio' AND idVendedor = 5";
              $resultadoPPVdor370 = mysqli_query($con, $consultaPPVdor370);
 
              while ($RowPPVdor370 = mysqli_fetch_array($resultadoPPVdor370)) {
 
              $porceCumpliVdor370 = $RowPPVdor370['porcentaje'];
                        
              if ($porceCumpliVdor370 > 100) {
                $liquidacion_PPTO370 = 0.009;
              }elseif ($porceCumpliVdor370 >= 80) {
                $liquidacion_PPTO370 = 0.007;
              }elseif ($porceCumpliVdor370 <= 79 && $sacaMes370 == '05' && $sacaAnio370 == '2021' || $porceCumpliVdor370 <= 79 && $sacaMes370 == '06' && $sacaAnio370 == '2021' || $porceCumpliVdor370 <= 79 && $sacaMes370 == '07' && $sacaAnio370 == '2021') {
                $liquidacion_PPTO370 = 0.0035;
              }else{
                $liquidacion_PPTO370 = 0;
              }
 
              //echo $numeRac370." - ".$numeFac370." - ".$beseComisionApro370." - ".$dias370." - "." ( ".$sacaMes370." ) "." - ".$porceCumpliVdor370."%"." - ".$liquidacion_PPTO370." - ".$porcentaje_liqui_recaudo370."%"."<br>";
 
              $comision_PPTO_Diego370 = $beseComisionApro370*$liquidacion_PPTO370*($porcentaje_liqui_recaudo370/100)/1.5;
              $Total_comision_PPTO_Diego370 += $comision_PPTO_Diego370;
              //echo $comision_PPTO_Diego370."<br>";        
 
              }
              $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $vende370);
              $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $agencia370);
              $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $Umer370);
              $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $mer370);
              $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $numeRac370);
              $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $fecha370);
              $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $codigo370);
              $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $cliente370);
              $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $valor_aplicado370);
              $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, $beseComisionApro370);
              $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $tipo_370);
              $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, $numeFac370);
              $objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, $fecha_doc370);
              $objPHPExcel->getActiveSheet()->setCellValue('N'.$fila, $dias370);
              $objPHPExcel->getActiveSheet()->setCellValue('O'.$fila, $sacaMes370);
              $objPHPExcel->getActiveSheet()->setCellValue('P'.$fila, $porceCumpliVdor370."%");
              $objPHPExcel->getActiveSheet()->setCellValue('Q'.$fila, $liquidacion_PPTO370);
              $objPHPExcel->getActiveSheet()->setCellValue('R'.$fila, $porcentaje_liqui_recaudo370."%");
              $objPHPExcel->getActiveSheet()->setCellValue('S'.$fila, $comision_PPTO_Diego370);
              $objPHPExcel->getActiveSheet()->setCellValue('T'.$fila, $porceApli370);

              $fila++;
          }
     }
   }
 //TERMINA CALCULO COMISIONES MESSER MANIZALES................................................................................................................................................................................................    
  
 //CALCULO COMISIONES MESSER ITAGUI................................................................................................................................................................................
 
 $consultaConso590 = "SELECT * FROM comisiones_itagui WHERE mes = '$mesC' AND dias != ''";
 $resultadoConso590 = mysqli_query($con, $consultaConso590);
 
 while ($RowConso590 = mysqli_fetch_array($resultadoConso590)) {       
  if ($RowConso590['dias'] != '' && $RowConso590['porc_3'] != '' && $RowConso590['porc_4'] != '') {
      
      $agencia590 = '590';
      $Umer590 = $RowConso590['merc'];
      $mer590 = $Umer590."590";   
      $codigo590 = $RowConso590['codigo'];
      $length590 = 9;
      $codigoV_CLI_590 = substr(str_repeat(0, $length590).$codigo590, - $length590);
      $clienta590 = $RowConso590['cliente'];
      $fecha590 = $RowConso590['fecha'];
      $valor_aplicado_rec060 = $RowConso590['valor_aplicado_rec'];
      $tipo_060 = $RowConso590['tipo_'];
   
      $numeRac590 = $RowConso590['recibo'];
      $numeFac590 = $RowConso590['numero_']; 
      $beseComisionApro590 = $RowConso590['base_com'];
      $dias590 = $RowConso590['dias'];
      $fecha_doc590 = $RowConso590['fecha_doc'];
      $sacaMes590 = date('m', strtotime($fecha_doc590));
      $sacaAnio590 = date('Y', strtotime($fecha_doc590)); 
      $porceApli590 = $RowConso590['porcentaje'];
 
      $consultaVEndedor590 = "SELECT DISTINCT codigo, cliente, vendedor FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_590'";
      $resultAsVddor590 = mysqli_query($con, $consultaVEndedor590);
 
      while ($rowAsVddor590 = mysqli_fetch_array($resultAsVddor590)) { 
           $vendedor = $rowAsVddor590['vendedor'];
      }
       if ($vendedor == 'TV') {
            //echo $numeRac." - ".$numeFac." - ".$beseComisionApro." - ".$dias051."<br>";
 
           if ($dias590 <= 90) {
               $porcentaje_liqui_recaudo590 = 100;
           }elseif ($dias590 > 120) {
               $porcentaje_liqui_recaudo590 = 0;
           } else {
             $porcentaje_liqui_recaudo590 = 80;
           }
 
           $consultaPPVdor590 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes590' AND anio = '$anio' AND idVendedor = 5";
           $resultadoPPVdor590 = mysqli_query($con, $consultaPPVdor590);
 
           while ($RowPPVdor590 = mysqli_fetch_array($resultadoPPVdor590)) {
 
             $porceCumpliVdor590 = $RowPPVdor590['porcentaje'];
                       
             if ($porceCumpliVdor590 > 100) {
               $liquidacion_PPTO590 = 0.009;
             }elseif ($porceCumpliVdor590 >= 80) {
               $liquidacion_PPTO590 = 0.007;
             }elseif ($porceCumpliVdor590 <= 79 && $sacaMes590 == '05' && $sacaAnio590 == '2021' || $porceCumpliVdor590 <= 79 && $sacaMes590 == '07' && $sacaAnio590 == '2021' || $porceCumpliVdor590 <= 79 && $sacaMes590 == '06' && $sacaAnio590 == '2021') {
               $liquidacion_PPTO590 = 0.0035;
             }else {
               $liquidacion_PPTO590 = 0;
             }
 
             //echo $numeRac590." - ".$numeFac590." - ".$beseComisionApro590." - ".$dias590." - "." ( ".$sacaMes590." ) "." - ".$porceCumpliVdor590."%"." - ".$liquidacion_PPTO590." - ".$porcentaje_liqui_recaudo590."%"."<br>";
 
             $comision_PPTO_Diego590 = $beseComisionApro590*$liquidacion_PPTO590*($porcentaje_liqui_recaudo590/100);
             $Total_comision_PPTO_Diego590 += $comision_PPTO_Diego590;
             
             //echo $comision_PPTO_Diego590."<br>";        
 
           }
 
           $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $vendedor);
           $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $agencia590);
           $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $Umer590);
           $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $mer590);
           $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $numeRac590);
           $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $fecha590);
           $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $codigo590);
           $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $clienta590);
           $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $valor_aplicado_rec060);
           $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, $beseComisionApro590);
           $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $tipo_060);
           $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, $numeFac590);
           $objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, $fecha_doc590);
           $objPHPExcel->getActiveSheet()->setCellValue('N'.$fila, $dias590);
           $objPHPExcel->getActiveSheet()->setCellValue('O'.$fila, $sacaMes590);
           $objPHPExcel->getActiveSheet()->setCellValue('P'.$fila, $porceCumpliVdor590."%");
           $objPHPExcel->getActiveSheet()->setCellValue('Q'.$fila, $liquidacion_PPTO590);
           $objPHPExcel->getActiveSheet()->setCellValue('R'.$fila, $porcentaje_liqui_recaudo590."%");
           $objPHPExcel->getActiveSheet()->setCellValue('S'.$fila, $comision_PPTO_Diego590);
           $objPHPExcel->getActiveSheet()->setCellValue('T'.$fila, $porceApli590);
           
           $fila++;
       }   
 
   }elseif ($RowConso590['porcentaje'] == 10) {
 
     $agencia590 = '590';
     $Umer590 = $RowConso590['merc'];
     $mer590 = $Umer590."590";   
     $codigo590 = $RowConso590['codigo'];
     $length590 = 9;
     $codigoV_CLI_590 = substr(str_repeat(0, $length590).$codigo590, - $length590);
     $clienta590 = $RowConso590['cliente'];
     $fecha590 = $RowConso590['fecha'];
     $valor_aplicado_rec060 = $RowConso590['valor_aplicado_rec'];
     $tipo_060 = $RowConso590['tipo_'];
  
     $numeRac590 = $RowConso590['recibo'];
     $numeFac590 = $RowConso590['numero_']; 
     $beseComisionApro590 = $RowConso590['base_com'];
     $dias590 = $RowConso590['dias'];
     $fecha_doc590 = $RowConso590['fecha_doc'];
     $sacaMes590 = date('m', strtotime($fecha_doc590));
     $sacaAnio590 = date('Y', strtotime($fecha_doc590));
     $porceApli590 = $RowConso590['porcentaje']; 
 
     $consultaVEndedor590 = "SELECT DISTINCT codigo, cliente, vendedor FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_590'";
     $resultAsVddor590 = mysqli_query($con, $consultaVEndedor590);
 
     while ($rowAsVddor590 = mysqli_fetch_array($resultAsVddor590)) { 
          $vendedor = $rowAsVddor590['vendedor'];
     }
      if ($vendedor == 'TV') {
           //echo $numeRac." - ".$numeFac." - ".$beseComisionApro." - ".$dias051."<br>";
 
          if ($dias590 <= 90) {
              $porcentaje_liqui_recaudo590 = 100;
          }elseif ($dias590 > 120) {
              $porcentaje_liqui_recaudo590 = 0;
          } else {
            $porcentaje_liqui_recaudo590 = 80;
          }
 
          $consultaPPVdor590 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes590' AND anio = '$anio' AND idVendedor = 5";
          $resultadoPPVdor590 = mysqli_query($con, $consultaPPVdor590);
 
          while ($RowPPVdor590 = mysqli_fetch_array($resultadoPPVdor590)) {
 
            $porceCumpliVdor590 = $RowPPVdor590['porcentaje'];
                      
            if ($porceCumpliVdor590 > 100) {
              $liquidacion_PPTO590 = 0.009;
            }elseif ($porceCumpliVdor590 >= 80) {
              $liquidacion_PPTO590 = 0.007;
            }elseif ($porceCumpliVdor590 <= 79 && $sacaMes590 == '05' && $sacaAnio590 == '2021' || $porceCumpliVdor590 <= 79 && $sacaMes590 == '07' && $sacaAnio590 == '2021' || $porceCumpliVdor590 <= 79 && $sacaMes590 == '06' && $sacaAnio590 == '2021') {
              $liquidacion_PPTO590 = 0.0035;
            }else {
              $liquidacion_PPTO590 = 0;
            }
 
            //echo $numeRac590." - ".$numeFac590." - ".$beseComisionApro590." - ".$dias590." - "." ( ".$sacaMes590." ) "." - ".$porceCumpliVdor590."%"." - ".$liquidacion_PPTO590." - ".$porcentaje_liqui_recaudo590."%"."<br>";
 
            $comision_PPTO_Diego590 = $beseComisionApro590*$liquidacion_PPTO590*($porcentaje_liqui_recaudo590/100)/3;
            $Total_comision_PPTO_Diego590 += $comision_PPTO_Diego590;
            
            //echo $comision_PPTO_Diego590."<br>";        
 
          }
 
          $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $vendedor);
          $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $agencia590);
          $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $Umer590);
          $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $mer590);
          $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $numeRac590);
          $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $fecha590);
          $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $codigo590);
          $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $clienta590);
          $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $valor_aplicado_rec060);
          $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, $beseComisionApro590);
          $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $tipo_060);
          $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, $numeFac590);
          $objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, $fecha_doc590);
          $objPHPExcel->getActiveSheet()->setCellValue('N'.$fila, $dias590);
          $objPHPExcel->getActiveSheet()->setCellValue('O'.$fila, $sacaMes590);
          $objPHPExcel->getActiveSheet()->setCellValue('P'.$fila, $porceCumpliVdor590."%");
          $objPHPExcel->getActiveSheet()->setCellValue('Q'.$fila, $liquidacion_PPTO590);
          $objPHPExcel->getActiveSheet()->setCellValue('R'.$fila, $porcentaje_liqui_recaudo590."%");
          $objPHPExcel->getActiveSheet()->setCellValue('S'.$fila, $comision_PPTO_Diego590);
          $objPHPExcel->getActiveSheet()->setCellValue('T'.$fila, $porceApli590);
          
          $fila++;
      }        
 
   }elseif ($RowConso590['porcentaje'] == 16.5) { 
 
     $agencia590 = '590';
     $Umer590 = $RowConso590['merc'];
     $mer590 = $Umer590."590";   
     $codigo590 = $RowConso590['codigo'];
     $length590 = 9;
     $codigoV_CLI_590 = substr(str_repeat(0, $length590).$codigo590, - $length590);
     $clienta590 = $RowConso590['cliente'];
     $fecha590 = $RowConso590['fecha'];
     $valor_aplicado_rec060 = $RowConso590['valor_aplicado_rec'];
     $tipo_060 = $RowConso590['tipo_'];
  
     $numeRac590 = $RowConso590['recibo'];
     $numeFac590 = $RowConso590['numero_']; 
     $beseComisionApro590 = $RowConso590['base_com'];
     $dias590 = $RowConso590['dias'];
     $fecha_doc590 = $RowConso590['fecha_doc'];
     $sacaMes590 = date('m', strtotime($fecha_doc590));
     $sacaAnio590 = date('Y', strtotime($fecha_doc590)); 
     $porceApli590 = $RowConso590['porcentaje'];
 
     $consultaVEndedor590 = "SELECT DISTINCT codigo, cliente, vendedor FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_590'";
     $resultAsVddor590 = mysqli_query($con, $consultaVEndedor590);
 
     while ($rowAsVddor590 = mysqli_fetch_array($resultAsVddor590)) { 
          $vendedor = $rowAsVddor590['vendedor'];
     }
      if ($vendedor == 'TV') {
           //echo $numeRac." - ".$numeFac." - ".$beseComisionApro." - ".$dias051."<br>";
 
          if ($dias590 <= 90) {
              $porcentaje_liqui_recaudo590 = 100;
          }elseif ($dias590 > 120) {
              $porcentaje_liqui_recaudo590 = 0;
          } else {
            $porcentaje_liqui_recaudo590 = 80;
          }
 
          $consultaPPVdor590 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes590' AND anio = '$anio' AND idVendedor = 5";
          $resultadoPPVdor590 = mysqli_query($con, $consultaPPVdor590);
 
          while ($RowPPVdor590 = mysqli_fetch_array($resultadoPPVdor590)) {
 
            $porceCumpliVdor590 = $RowPPVdor590['porcentaje'];
                      
            if ($porceCumpliVdor590 > 100) {
              $liquidacion_PPTO590 = 0.009;
            }elseif ($porceCumpliVdor590 >= 80) {
              $liquidacion_PPTO590 = 0.007;
            }elseif ($porceCumpliVdor590 <= 79 && $sacaMes590 == '05' && $sacaAnio590 == '2021' || $porceCumpliVdor590 <= 79 && $sacaMes590 == '07' && $sacaAnio590 == '2021' || $porceCumpliVdor590 <= 79 && $sacaMes590 == '06' && $sacaAnio590 == '2021') {
              $liquidacion_PPTO590 = 0.0035;
            }else {
              $liquidacion_PPTO590 = 0;
            }
 
            //echo $numeRac590." - ".$numeFac590." - ".$beseComisionApro590." - ".$dias590." - "." ( ".$sacaMes590." ) "." - ".$porceCumpliVdor590."%"." - ".$liquidacion_PPTO590." - ".$porcentaje_liqui_recaudo590."%"."<br>";
 
            $comision_PPTO_Diego590 = $beseComisionApro590*$liquidacion_PPTO590*($porcentaje_liqui_recaudo590/100)/1.81;
            $Total_comision_PPTO_Diego590 += $comision_PPTO_Diego590;
            
            //echo $comision_PPTO_Diego590."<br>";        
 
          }
 
          $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $vendedor);
          $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $agencia590);
          $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $Umer590);
          $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $mer590);
          $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $numeRac590);
          $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $fecha590);
          $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $codigo590);
          $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $clienta590);
          $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $valor_aplicado_rec060);
          $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, $beseComisionApro590);
          $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $tipo_060);
          $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, $numeFac590);
          $objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, $fecha_doc590);
          $objPHPExcel->getActiveSheet()->setCellValue('N'.$fila, $dias590);
          $objPHPExcel->getActiveSheet()->setCellValue('O'.$fila, $sacaMes590);
          $objPHPExcel->getActiveSheet()->setCellValue('P'.$fila, $porceCumpliVdor590."%");
          $objPHPExcel->getActiveSheet()->setCellValue('Q'.$fila, $liquidacion_PPTO590);
          $objPHPExcel->getActiveSheet()->setCellValue('R'.$fila, $porcentaje_liqui_recaudo590."%");
          $objPHPExcel->getActiveSheet()->setCellValue('S'.$fila, $comision_PPTO_Diego590);
          $objPHPExcel->getActiveSheet()->setCellValue('T'.$fila, $porceApli590);
          
          $fila++;
      } 
       
   }elseif ($RowConso590['porcentaje'] == 5) {
 
     $agencia590 = '590';
     $Umer590 = $RowConso590['merc'];
     $mer590 = $Umer590."590";   
     $codigo590 = $RowConso590['codigo'];
     $length590 = 9;
     $codigoV_CLI_590 = substr(str_repeat(0, $length590).$codigo590, - $length590);
     $clienta590 = $RowConso590['cliente'];
     $fecha590 = $RowConso590['fecha'];
     $valor_aplicado_rec060 = $RowConso590['valor_aplicado_rec'];
     $tipo_060 = $RowConso590['tipo_'];
  
     $numeRac590 = $RowConso590['recibo'];
     $numeFac590 = $RowConso590['numero_']; 
     $beseComisionApro590 = $RowConso590['base_com'];
     $dias590 = $RowConso590['dias'];
     $fecha_doc590 = $RowConso590['fecha_doc'];
     $sacaMes590 = date('m', strtotime($fecha_doc590));
     $sacaAnio590 = date('Y', strtotime($fecha_doc590)); 
     $porceApli590 = $RowConso590['porcentaje'];
 
     $consultaVEndedor590 = "SELECT DISTINCT codigo, cliente, vendedor FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_590'";
     $resultAsVddor590 = mysqli_query($con, $consultaVEndedor590);
 
     while ($rowAsVddor590 = mysqli_fetch_array($resultAsVddor590)) { 
          $vendedor = $rowAsVddor590['vendedor'];
     }
      if ($vendedor == 'TV') {
           //echo $numeRac." - ".$numeFac." - ".$beseComisionApro." - ".$dias051."<br>";
 
          if ($dias590 <= 90) {
              $porcentaje_liqui_recaudo590 = 100;
          }elseif ($dias590 > 120) {
              $porcentaje_liqui_recaudo590 = 0;
          } else {
            $porcentaje_liqui_recaudo590 = 80;
          }
 
          $consultaPPVdor590 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes590' AND anio = '$anio' AND idVendedor = 5";
          $resultadoPPVdor590 = mysqli_query($con, $consultaPPVdor590);
 
          while ($RowPPVdor590 = mysqli_fetch_array($resultadoPPVdor590)) {
 
            $porceCumpliVdor590 = $RowPPVdor590['porcentaje'];
                      
            if ($porceCumpliVdor590 > 100) {
              $liquidacion_PPTO590 = 0.009;
            }elseif ($porceCumpliVdor590 >= 80) {
              $liquidacion_PPTO590 = 0.007;
            }elseif ($porceCumpliVdor590 <= 79 && $sacaMes590 == '05' && $sacaAnio590 == '2021' || $porceCumpliVdor590 <= 79 && $sacaMes590 == '07' && $sacaAnio590 == '2021' || $porceCumpliVdor590 <= 79 && $sacaMes590 == '06' && $sacaAnio590 == '2021') {
              $liquidacion_PPTO590 = 0.0035;
            }else {
              $liquidacion_PPTO590 = 0;
            }
 
            //echo $numeRac590." - ".$numeFac590." - ".$beseComisionApro590." - ".$dias590." - "." ( ".$sacaMes590." ) "." - ".$porceCumpliVdor590."%"." - ".$liquidacion_PPTO590." - ".$porcentaje_liqui_recaudo590."%"."<br>";
 
            $comision_PPTO_Diego590 = $beseComisionApro590*$liquidacion_PPTO590*($porcentaje_liqui_recaudo590/100)/6;
            $Total_comision_PPTO_Diego590 += $comision_PPTO_Diego590;
            
            //echo $comision_PPTO_Diego590."<br>";        
 
          }
 
          $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $vendedor);
          $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $agencia590);
          $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $Umer590);
          $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $mer590);
          $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $numeRac590);
          $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $fecha590);
          $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $codigo590);
          $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $clienta590);
          $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $valor_aplicado_rec060);
          $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, $beseComisionApro590);
          $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $tipo_060);
          $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, $numeFac590);
          $objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, $fecha_doc590);
          $objPHPExcel->getActiveSheet()->setCellValue('N'.$fila, $dias590);
          $objPHPExcel->getActiveSheet()->setCellValue('O'.$fila, $sacaMes590);
          $objPHPExcel->getActiveSheet()->setCellValue('P'.$fila, $porceCumpliVdor590."%");
          $objPHPExcel->getActiveSheet()->setCellValue('Q'.$fila, $liquidacion_PPTO590);
          $objPHPExcel->getActiveSheet()->setCellValue('R'.$fila, $porcentaje_liqui_recaudo590."%");
          $objPHPExcel->getActiveSheet()->setCellValue('S'.$fila, $comision_PPTO_Diego590);
          $objPHPExcel->getActiveSheet()->setCellValue('T'.$fila, $porceApli590);
          
          $fila++;
      } 
     
   }elseif ($RowConso590['porcentaje'] == 20) {
 
     $agencia590 = '590';
     $Umer590 = $RowConso590['merc'];
     $mer590 = $Umer590."590";   
     $codigo590 = $RowConso590['codigo'];
     $length590 = 9;
     $codigoV_CLI_590 = substr(str_repeat(0, $length590).$codigo590, - $length590);
     $clienta590 = $RowConso590['cliente'];
     $fecha590 = $RowConso590['fecha'];
     $valor_aplicado_rec060 = $RowConso590['valor_aplicado_rec'];
     $tipo_060 = $RowConso590['tipo_'];
  
     $numeRac590 = $RowConso590['recibo'];
     $numeFac590 = $RowConso590['numero_']; 
     $beseComisionApro590 = $RowConso590['base_com'];
     $dias590 = $RowConso590['dias'];
     $fecha_doc590 = $RowConso590['fecha_doc'];
     $sacaMes590 = date('m', strtotime($fecha_doc590));
     $sacaAnio590 = date('Y', strtotime($fecha_doc590)); 
     $porceApli590 = $RowConso590['porcentaje'];
 
     $consultaVEndedor590 = "SELECT DISTINCT codigo, cliente, vendedor FROM asignacion_vendedores WHERE codigo = '$codigoV_CLI_590'";
     $resultAsVddor590 = mysqli_query($con, $consultaVEndedor590);
 
     while ($rowAsVddor590 = mysqli_fetch_array($resultAsVddor590)) { 
          $vendedor = $rowAsVddor590['vendedor'];
     }
      if ($vendedor == 'TV') {
           //echo $numeRac." - ".$numeFac." - ".$beseComisionApro." - ".$dias051."<br>";
 
          if ($dias590 <= 90) {
              $porcentaje_liqui_recaudo590 = 100;
          }elseif ($dias590 > 120) {
              $porcentaje_liqui_recaudo590 = 0;
          } else {
            $porcentaje_liqui_recaudo590 = 80;
          }
 
          $consultaPPVdor590 = "SELECT * FROM porce_cumpli_vendedor WHERE mes = '$sacaMes590' AND anio = '$anio' AND idVendedor = 5";
          $resultadoPPVdor590 = mysqli_query($con, $consultaPPVdor590);
 
          while ($RowPPVdor590 = mysqli_fetch_array($resultadoPPVdor590)) {
 
            $porceCumpliVdor590 = $RowPPVdor590['porcentaje'];
                      
            if ($porceCumpliVdor590 > 100) {
              $liquidacion_PPTO590 = 0.009;
            }elseif ($porceCumpliVdor590 >= 80) {
              $liquidacion_PPTO590 = 0.007;
            }elseif ($porceCumpliVdor590 <= 79 && $sacaMes590 == '05' && $sacaAnio590 == '2021' || $porceCumpliVdor590 <= 79 && $sacaMes590 == '07' && $sacaAnio590 == '2021' || $porceCumpliVdor590 <= 79 && $sacaMes590 == '06' && $sacaAnio590 == '2021') {
              $liquidacion_PPTO590 = 0.0035;
            }else {
              $liquidacion_PPTO590 = 0;
            }
 
            //echo $numeRac590." - ".$numeFac590." - ".$beseComisionApro590." - ".$dias590." - "." ( ".$sacaMes590." ) "." - ".$porceCumpliVdor590."%"." - ".$liquidacion_PPTO590." - ".$porcentaje_liqui_recaudo590."%"."<br>";
 
            $comision_PPTO_Diego590 = $beseComisionApro590*$liquidacion_PPTO590*($porcentaje_liqui_recaudo590/100)/1.5;
            $Total_comision_PPTO_Diego590 += $comision_PPTO_Diego590;
            
            //echo $comision_PPTO_Diego590."<br>";        
 
          }
 
          $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $vendedor);
          $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $agencia590);
          $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $Umer590);
          $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $mer590);
          $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $numeRac590);
          $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $fecha590);
          $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $codigo590);
          $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $clienta590);
          $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $valor_aplicado_rec060);
          $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, $beseComisionApro590);
          $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $tipo_060);
          $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, $numeFac590);
          $objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, $fecha_doc590);
          $objPHPExcel->getActiveSheet()->setCellValue('N'.$fila, $dias590);
          $objPHPExcel->getActiveSheet()->setCellValue('O'.$fila, $sacaMes590);
          $objPHPExcel->getActiveSheet()->setCellValue('P'.$fila, $porceCumpliVdor590."%");
          $objPHPExcel->getActiveSheet()->setCellValue('Q'.$fila, $liquidacion_PPTO590);
          $objPHPExcel->getActiveSheet()->setCellValue('R'.$fila, $porcentaje_liqui_recaudo590."%");
          $objPHPExcel->getActiveSheet()->setCellValue('S'.$fila, $comision_PPTO_Diego590);
          $objPHPExcel->getActiveSheet()->setCellValue('T'.$fila, $porceApli590);

          $fila++;
      } 
    
   }
 } 
 //TERMINA CALCULO COMISIONES MESSER ITAGUI................................................................................................................................................................................................

 $sumatoria1 += $Total_comision_PPTO_Diego;
 $sumatoria2 += $Total_comision_PPTO_Diego060;
 $sumatoria3 += $Total_comision_PPTO_Diego370;
 $sumatoria4 += $Total_comision_PPTO_Diego590;
 
 $totalSumado = $sumatoria1+$sumatoria2+$sumatoria3+$sumatoria4;
 
 $objPHPExcel->getActiveSheet()->setCellValue('S'.$fila, $totalSumado);


  header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
  header('Content-Disposition: attachment;filename="TV Ms.xlsx"');
  header('Cache-Control: max-age=0');
  $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
  $writer->save('php://output');
}
?>