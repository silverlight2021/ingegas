<?php
require('../../libraries/conexion.php');
require '../../PHPExcel.php';
date_default_timezone_set('America/Bogota');
$Fecha = date("Y-m-d");

$consultaCargue = "SELECT mes FROM cargue_comisiones_vendedores ORDER BY id_cargue_comisiones_vendedores DESC LIMIT 1"; //hace la consulta y trae el ultimo mes cargado
$resultadoCargue = mysqli_query($con, $consultaCargue);

while ($linea = mysqli_fetch_array($resultadoCargue)) {
  $mesC = $linea["mes"]; 
  $anio = date("Y");

  $mes = date("F", $mesC);
  $mesMayuscula = strtoupper($mes);
}



if($tipo = 1){
  $fila = 2;
 
  $objPHPExcel = new PHPExcel();
  $objPHPExcel->getProperties()->setCreator("Ingegas")->setDescription("Comisiones Vendedores");
  $objPHPExcel->setActiveSheetIndex(0);

  $objPHPExcel->getActiveSheet()->setTitle("Comision Ingegas");
  $objPHPExcel->getActiveSheet()->setCellValue('E1', "$mesMayuscula");
  $objPHPExcel->getActiveSheet()->setCellValue('E3', "INGEGAS LTDA");  
  
  $objPHPExcel->getActiveSheet()->setCellValue('B5', 'Rec');
  $objPHPExcel->getActiveSheet()->setCellValue('C5', 'Fecha');  
  $objPHPExcel->getActiveSheet()->setCellValue('D5', 'Cliente');
  $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Valor Aplicado');
  $objPHPExcel->getActiveSheet()->setCellValue('F5', 'Base Comision');
  $objPHPExcel->getActiveSheet()->setCellValue('G5', 'Num Fact');
  $objPHPExcel->getActiveSheet()->setCellValue('H5', 'Fecha Doc');  
  $objPHPExcel->getActiveSheet()->setCellValue('I5', 'Dias');
  $objPHPExcel->getActiveSheet()->setCellValue('J5', '% de Comision');
  $objPHPExcel->getActiveSheet()->setCellValue('K5', 'Comision');  
    
  $fila = 6;
  $consultaPagoCobranza = "SELECT fecha, factura, cliente, recaudo, rc, sucursal FROM cobranza";
  $queryPagoCobranza = mysqli_query($con, $consultaPagoCobranza);
  while ($traerDatosCobranza = mysqli_fetch_array($queryPagoCobranza)) {
      $fechaCo = $traerDatosCobranza['fecha'];                
      $facturaCo = $traerDatosCobranza['factura'];
      $facturaCo2 = preg_replace('/[^0-9]/', '', $facturaCo); // Quitarle las letras a las facturas
      $saca_cliente = $traerDatosCobranza['cliente'];
      $indntiSucur = $traerDatosCobranza['sucursal'];
      $reciboIng = $traerDatosCobranza['rc'];
      $venta_neta = $traerDatosCobranza['recaudo']/1.19;
      $venta_netaIva = $traerDatosCobranza['recaudo'];
    
     if ($indntiSucur != '1') {                  
        $consultaCobranza = "SELECT fecha, documento, venta_neta FROM vtas_vdor WHERE documento = '$facturaCo2'";
      $queryCobranza = mysqli_query($con, $consultaCobranza);   
      while ($traeDatos = mysqli_fetch_array($queryCobranza)) {
        $fecha = $traeDatos['fecha'];                 
        $factura = $traeDatos['documento'];                  
        $dias = calculaDias($fecha, $fechaCo);
        $diferencia_dia = $dias[11]+1;                  
        //$venta_neta = $traeDatos['venta_neta']; 
        //echo $factura." / ".$diferencia_dia." / ".$venta_neta."<br>";                 

        if ($diferencia_dia <= 1) {
            $comision_ing = $venta_neta*0.0015;
            $total_15 += $comision_ing;   
            
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $reciboIng);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $fechaCo);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $saca_cliente);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $venta_netaIva);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $venta_neta);
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $facturaCo);
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $fecha);
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $diferencia_dia);
            $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, "0.15%");
            $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $comision_ing);
            
            $fila++;

          }elseif ($diferencia_dia >= 2 && $diferencia_dia <= 45) {
            $comision_ing = $venta_neta*0.00075;
            $total_75 += $comision_ing;

            $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $reciboIng);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $fechaCo);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $saca_cliente);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $venta_netaIva);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $venta_neta);
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $facturaCo);
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $fecha);
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $diferencia_dia);
            $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, "0.075%");
            $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $comision_ing);
            
            $fila++;
            
            
          }elseif ($diferencia_dia >= 46 && $diferencia_dia <= 75) {
            $comision_ing = $venta_neta*0.0005;
            $total_5 += $comision_ing;

            $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $reciboIng);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $fechaCo);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $saca_cliente);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $venta_netaIva);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $venta_neta);
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $facturaCo);
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $fecha);
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $diferencia_dia);
            $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, "0.05%");
            $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $comision_ing);
            
            $fila++;
            //echo $saca_cliente." / $".$venta_neta." - ".$factura." - ".$fecha." - ".$fechaCo." / ".$diferencia_dia." / $". $comision_ing."<br>";                                   
          }elseif ($diferencia_dia >= 76 && $diferencia_dia <= 100) {
            $comision_ing = $venta_neta*0.00025;
            $total_25 += $comision_ing;
            
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $reciboIng);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $fechaCo);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $saca_cliente);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $venta_netaIva);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $venta_neta);
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $facturaCo);
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $fecha);
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $diferencia_dia);
            $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, "0.025%");
            $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $comision_ing);
            
            $fila++;
          }elseif ($diferencia_dia >= 101) {                    
            $comision_ing = $venta_neta*0.00015;
            $total_015 += $comision_ing;
            
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $reciboIng);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $fechaCo);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $saca_cliente);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $venta_netaIva);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $venta_neta);
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $facturaCo);
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $fecha);
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $diferencia_dia);
            $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, "0.015%");
            $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $comision_ing);
            
            $fila++;
          }else {
            $comision_ing = 0;
          } 
        } 
      }      

     }
  header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
  header('Content-Disposition: attachment;filename="Cr Ing.xlsx"');
  header('Cache-Control: max-age=0');
  $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
  $writer->save('php://output');
}


function calculaDias($fecha_1, $fecha_2){
  $tomaFecha1 = date_create($fecha_1);
  $tomaFecha2 = date_create($fecha_2);
  $dif_dias = date_diff($tomaFecha1, $tomaFecha2);

  $tiempo = array();
  
  foreach ($dif_dias as $valor) {
    $tiempo[] = $valor;
  }
  return $tiempo;

}
?>