<?php
require('../../libraries/conexion.php');
require '../../PHPExcel.php';
date_default_timezone_set('America/Bogota');
$Fecha = date("Y-m-d");

$consultaCargue = "SELECT mes FROM cargue_comisiones_vendedores ORDER BY id_cargue_comisiones_vendedores DESC LIMIT 1"; //hace la consulta y trae el ultimo mes cargado
$resultadoCargue = mysqli_query($con, $consultaCargue);

while ($linea = mysqli_fetch_array($resultadoCargue)) {
  $mesC = $linea["mes"]; 
  $anio = date("Y");
}



if($tipo = 1){
  $fila = 2;
  $ventasAcumuladasCobrador = "SELECT DISTINCT numero_legal, sucursal, total_sin_iva, tipo_doc, operador FROM ventas_acumuladas ORDER BY id_ventas_acumuladas";
  $queryCobrador = mysqli_query($con, $ventasAcumuladasCobrador);
  $objPHPExcel = new PHPExcel();
  $objPHPExcel->getProperties()->setCreator("Ingegas")->setDescription("Comisiones Vendedores");
  $objPHPExcel->setActiveSheetIndex(0);

  $objPHPExcel->getActiveSheet()->setTitle("Cartera Sur");
  $objPHPExcel->getActiveSheet()->setCellValue('E2', "CARTERA CAZUCA");
  $objPHPExcel->getActiveSheet()->setCellValue('E3', '');

   $objPHPExcel->getActiveSheet()->setCellValue('B5', 'Rec');
  $objPHPExcel->getActiveSheet()->setCellValue('C5', 'Fecha');
  $objPHPExcel->getActiveSheet()->setCellValue('D5', 'Codigo');
  $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Cliente');
  $objPHPExcel->getActiveSheet()->setCellValue('F5', 'Valor Aplicado');
  $objPHPExcel->getActiveSheet()->setCellValue('G5', 'Base Comision');
  $objPHPExcel->getActiveSheet()->setCellValue('H5', 'Tipo Doc');
  $objPHPExcel->getActiveSheet()->setCellValue('I5', 'Num Fact');
  $objPHPExcel->getActiveSheet()->setCellValue('J5', 'Fecha Doc');
  $objPHPExcel->getActiveSheet()->setCellValue('K5', 'Dias');
  $objPHPExcel->getActiveSheet()->setCellValue('L5', 'Comision');  
    
  $fila = 6;
  while ($traeVentasAcu = mysqli_fetch_array($queryCobrador)) {   
    if ($traeVentasAcu['tipo_doc'] == 'FACE' && $traeVentasAcu['sucursal'] == '060') {

      $sucursal = $traeVentasAcu['sucursal'];
      $tipoFactura = $traeVentasAcu['tipo_doc'];
      $facturaVenAcu = $traeVentasAcu['numero_legal'];
      $totalSinIVA = $traeVentasAcu['total_sin_iva'];
      //echo $sucursal." / ".$tipoFactura." / ".$facturaVenAcu." / ".$totalSinIVA."<br>";
      
    //INICIA TOBERIN
    $sucursal501 = "SELECT * FROM comisiones_cazuca WHERE numero = '$facturaVenAcu' AND mes = '$mesC' AND dias != ''";
    $query051 = mysqli_query($con, $sucursal501);
    while ($datos051 = mysqli_fetch_array($query051)) {

      if (strlen($datos051['recibo']) <= 5) { //&& substr($datos051['rec'], 0, 2) != 24 
        $recibo051 = $datos051['recibo']; 
        $numeroFac = $datos051['numero'];
        $diastrans = $datos051['dias'];
        $valorSinIVA = $datos051['base_com'];
        $valorAplic = $datos051['valor_aplicado_rec'];
        $codigo = $datos051['codigo'];
        $cliente = $datos051['cliente'];
        $fechaRec = $datos051['fecha'];
        $fechaFac = $datos051['fecha_doc'];
        //echo $recibo051."<br>";//.$diastrans."<br>";
         
        
        if ($diastrans <= 1) {
          $comisionCobrador = $valorSinIVA*0.0015;              
        }elseif ($diastrans >= 2 && $diastrans <= 45) {
          $comisionCobrador = $valorSinIVA*0.001;              
        }elseif ($diastrans >= 46 && $diastrans <= 75) {
          $comisionCobrador = $valorSinIVA*0.0005;
        }elseif ($diastrans >= 76 && $diastrans <= 100) {
          $comisionCobrador = $valorSinIVA*0.00025;
        }elseif ($diastrans >= 101) {
          $comisionCobrador = $valorSinIVA*0.00015;
        }

        $suma1 += $comisionCobrador;
        
     
      $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $recibo051);
      $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $fechaRec);
      $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $codigo);
      $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $cliente);
      $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $valorAplic);
      $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $valorSinIVA);
      $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $tipoFactura);
      $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $numeroFac);
      $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, $fechaFac);
      $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $diastrans);      
      $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, $comisionCobrador);  
            
      $fila++; 
        
      }        


    } 
    //ACABA TOBERIN    
    
   }
  }

  $consultaDiferentes = "SELECT * FROM comisiones_cazuca WHERE mes = '$mesC' AND dias = ''";
        $queryDiferentes051 = mysqli_query($con, $consultaDiferentes);

        while ($diferentes051 = mysqli_fetch_array($queryDiferentes051)) {
            $facturaDiferente = $diferentes051['recibo'];
            $fechad = $diferentes051['fecha'];
            $codigod = $diferentes051['codigo'];
            $cliented = $diferentes051['cliente'];
            $valorAplid = $diferentes051['valor_aplicado_rec'];
            $baseComd = $diferentes051['base_comision'];
            $tipod = $diferentes051['tipo_'];
            $nufacd = $diferentes051['numero'];
            $valorFac = $baseComd*0.000033; 
            //echo $facturaDiferente."  ".$valorFac."<br>"; 
            $suma2 += $valorFac;  

            $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $facturaDiferente); 
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $fechad);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $codigod);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $cliented);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $valorAplid); 
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $baseComd); 
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $tipod);
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $nufacd);
            $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, $valorFac);            
                  
            $fila++; 
                         
        }

        $sumatoriaExcel = $suma1+$suma2;
        $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, $sumatoriaExcel);

  header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
  header('Content-Disposition: attachment;filename="CS.xlsx"');
  header('Cache-Control: max-age=0');
  $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
  $writer->save('php://output');
}
?>