<?php
require('../../libraries/conexion.php');
require '../../PHPExcel.php';
date_default_timezone_set('America/Bogota');
$Fecha = date("Y-m-d");

$consultaCargue = "SELECT mes FROM cargue_comisiones_vendedores ORDER BY id_cargue_comisiones_vendedores DESC LIMIT 1"; //hace la consulta y trae el ultimo mes cargado
$resultadoCargue = mysqli_query($con, $consultaCargue);

while ($linea = mysqli_fetch_array($resultadoCargue)) {
  $mesC = $linea["mes"]; 
  $anio = date("Y");

  $mes = date("M", $mesC);
}



if($tipo = 1){
  $fila = 2;
  $ventasAcumuladasCobrador = "SELECT DISTINCT numero_legal, sucursal, total_sin_iva, tipo_doc, operador FROM ventas_acumuladas ORDER BY id_ventas_acumuladas";
  $queryCobrador = mysqli_query($con, $ventasAcumuladasCobrador);
  $objPHPExcel = new PHPExcel();
  $objPHPExcel->getProperties()->setCreator("Ingegas")->setDescription("Comisiones Vendedores");
  $objPHPExcel->setActiveSheetIndex(0);

  $objPHPExcel->getActiveSheet()->setTitle("Comision Ingegas");
  $objPHPExcel->getActiveSheet()->setCellValue('E1', "OCTUBRE");
  $objPHPExcel->getActiveSheet()->setCellValue('E3', "RECAUDO GENERAL INGEGAS");
  
  $objPHPExcel->getActiveSheet()->setCellValue('A5', 'Vendedor');
  $objPHPExcel->getActiveSheet()->setCellValue('B5', 'Rec');
  $objPHPExcel->getActiveSheet()->setCellValue('C5', 'Fecha');  
  $objPHPExcel->getActiveSheet()->setCellValue('D5', 'Cliente');
  $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Valor Aplicado');
  $objPHPExcel->getActiveSheet()->setCellValue('F5', 'Base Comision');
  $objPHPExcel->getActiveSheet()->setCellValue('G5', 'Num Fact');
  $objPHPExcel->getActiveSheet()->setCellValue('H5', 'Fecha Doc');  
  $objPHPExcel->getActiveSheet()->setCellValue('I5', 'Comision'); 
    
  $fila = 6;

  //VERIFICA COMISIONES DE RECAUDO INGAS
$seleccionCobranza = "SELECT * FROM cobranza";
$resultCobranza = mysqli_query($con, $seleccionCobranza);
while ($rowCobranza = mysqli_fetch_array($resultCobranza)) {       
  $clienteCobranza = $rowCobranza['cliente'];       
  $facturaCobranza = $rowCobranza['factura'];
  $fac_neta = preg_replace('/[^0-9]/', '', $facturaCobranza);
  $fechaCobranza = $rowCobranza['fecha'];
  $recibo = $rowCobranza['rc'];
  $ventaNeta_sinIVA = $rowCobranza['recaudo']/1.19; 
  $ventaNetaIVA = $rowCobranza['recaudo']; 
  //echo $fac_neta."<br>";
  $verificaVendedor = "SELECT * FROM vtas_vdor WHERE documento = '$fac_neta'";
  $resultVeriVendedor = mysqli_query($con, $verificaVendedor);

  while ($rowVeriVendedor = mysqli_fetch_array($resultVeriVendedor)) {
    $vendedorCobraza = $rowVeriVendedor['vendedor'];
    $fechaVendedorFac = $rowVeriVendedor['fecha'];
    //$ventaNeta_sinIVA = $rowVeriVendedor['venta_neta']; 
    
    if ($vendedorCobraza == 'DIEGO FERNANDO MARTINEZ RAMIREZ') {      
           
     $totalRecaudo = $ventaNeta_sinIVA*0.01;
     //$porcentajeRecaudo = $totalRecaudo*0.01; 

     $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $vendedorCobraza);
     $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $recibo);
     $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $fechaCobranza);
     $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $clienteCobranza);
     $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $ventaNetaIVA);
     $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $ventaNeta_sinIVA);    
      $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $facturaCobranza);
      $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $fechaVendedorFac);       
      $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $totalRecaudo);  
            
      $fila++;      
     
    }elseif ($vendedorCobraza == 'JUAN CARLOS JIMENEZ CAICEDO') {
         
    $totalRecaudo_JJ = $ventaNeta_sinIVA*0.01;
    //$porcentajeRecaudo_JJ = $totalRecaudo_JJ*0.01;

    $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $vendedorCobraza);
     $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $recibo);
     $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $fechaCobranza);
     $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $clienteCobranza);
     $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $ventaNetaIVA);
     $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $ventaNeta_sinIVA);    
      $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $facturaCobranza);
      $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $fechaVendedorFac);
      $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $totalRecaudo_JJ);
     
           
     $fila++;

    }elseif ($vendedorCobraza == 'CARLOS EDUARDO ALBA SALAMANCA') {
     
     $totalRecaudo_EB = $ventaNeta_sinIVA*0.01;
     //$porcentajeRecaudo_EB = $totalRecaudo_EB*0.01;

     $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $vendedorCobraza);
     $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $recibo);
     $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $fechaCobranza);
     $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $clienteCobranza);
     $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $ventaNetaIVA);
     $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $ventaNeta_sinIVA);    
      $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $facturaCobranza);
      $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $fechaVendedorFac);
      $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $totalRecaudo_EB);   

     $fila++;

    }elseif ($vendedorCobraza == 'JORGE ENRIQUE AVENDAÑO ALVAREZ') {

    
     $totalRecaudo_TV = $ventaNeta_sinIVA*0.01;
     //$porcentajeRecaudo_TV = $totalRecaudo_TV*0.01;

     $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $vendedorCobraza);
     $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $recibo);
     $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $fechaCobranza);
     $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $clienteCobranza);
     $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $ventaNetaIVA);
     $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $ventaNeta_sinIVA);    
      $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $facturaCobranza);
      $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $fechaVendedorFac);
      $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $totalRecaudo_TV);   

     $fila++;

    }elseif ($vendedorCobraza == 'CESAR HERNAN HENAO MUÑOZ') {

    
     $totalRecaudo_VM = $ventaNeta_sinIVA*0.01;
     //$porcentajeRecaudo_VM = $totalRecaudo_VM*0.01; 

     $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $vendedorCobraza);
     $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $recibo);
     $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $fechaCobranza);
     $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $clienteCobranza);
     $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $ventaNetaIVA);
     $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $ventaNeta_sinIVA);    
      $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $facturaCobranza);
      $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $fechaVendedorFac);
      $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $totalRecaudo_VM);   
   
     
     $fila++;
     

    }

  }

}
  header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
  header('Content-Disposition: attachment;filename="Com Ing.xlsx"');
  header('Cache-Control: max-age=0');
  $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
  $writer->save('php://output');
}
?>