<?php
session_start();

if($_SESSION['logged'] == yes)
{
	require ("../../libraries/conexion.php"); //Archivo de conexión a base de datos

	//$idHoja_ruta = isset($_REQUEST['idHoja_ruta']) ? $_REQUEST['idHoja_ruta'] : NULL;
	$fecha_inicio = isset($_REQUEST['fecha_inicio']) ? $_REQUEST['fecha_inicio'] : NULL;
	$fecha_fin = isset($_REQUEST['fecha_fin']) ? $_REQUEST['fecha_fin'] : NULL;

	header("Content-type: text/csv");
	//header("Content-type: application/vnd.ms-excel; charset=UTF-8");
	header("Content-Disposition: attachment; filename=reporte_hoja_ruta.xls");  
	header("Pragma: no-cache"); 
	header("Expires: 0");
	
	?>
		<table border="1" width="100%" align="center">
			<thead>
				<tr>
					<th>#</th>
					<th>UN</th>
					<th>Fecha</th>
					<th>Mes</th>
					<th>Placa</th>
					<th>Salida</th>	                            					
					<th>Llegada</th>
					<th>Recorrido</th>
					<th>Cil LLenos</th>
					<th>Cil Vac</th>
					<th>Ter Llenos</th>
					<th>Ter Vac</th>
					<th>Lts</th>
					<th>Otros Ent.</th>
					<th>Otros Rec.</th>
					<th>Conductor</th>
					<th>Visitas</th>
					<th>Error</th>
					<th>Ayudante</th>
					<th>Estado</th>
					<th>NAL</th>
					<th>Velocidad</th>
				</tr>
			</thead>
			<tbody>';
			
			<?php
			$contador = 1;
				$consulta_1 = "SELECT *
							   FROM hoja_ruta d
							   WHERE (d.fecha BETWEEN '$fecha_inicio' AND '$fecha_fin')";
				$resultado_1 = mysqli_query($con,$consulta_1);
				if(mysqli_num_rows($resultado_1) > 0)
				{
					while($linea_1 = mysqli_fetch_assoc($resultado_1))
					{
						$contador++;
						$fecha = $linea_1['fecha'];
						$placa = $linea_1['placa'];
						$salida = $linea_1['salida'];
						$llegada = $linea_1['llegada'];
						$odometro_salida = $linea_1['odometro_salida'];
						$odometro_llegada = $linea_1['odometro_llegada'];
						$recorrido = $linea_1['recorrido'];
						$User_idUser_conductor = $linea_1['User_idUser_conductor'];
						$User_idUser_ayudante = $linea_1['User_idUser_ayudante'];
						$agencia_salida = $linea_1['agencia_salida'];
						$agencia_llegada = $linea_1['agencia_llegada'];
						$estado = $linea_1['estado'];
						$total_cil_ent = $linea_1['total_cil_ent'];
						$total_cil_rec = $linea_1['total_cil_rec'];
						$total_ter_ent = $linea_1['total_ter_ent'];
						$total_ter_rec = $linea_1['total_ter_rec'];
						$total_litros = $linea_1['total_litros'];
						$total_otros_ent = $linea_1['total_otros_ent'];
						$total_otros_rec = $linea_1['total_otros_rec'];
						$total_dinero = $linea_1['total_dinero'];
						$total_errores = $linea_1['total_errores'];
						$total_clientes = $linea_1['total_clientes'];

						$consulta_2 = "SELECT Name, LastName
									   FROM user WHERE idUser = '".$User_idUser_conductor."'";
						$resulado_2 = mysqli_query($con,$consulta_2);
						if(mysqli_num_rows($resulado_2) > 0)
						{
							$linea_2 = mysqli_fetch_assoc($resulado_2);
							$conductor = $linea_2['Name']." ".$linea_2['LastName'];
						}mysqli_free_result($resulado_2);

						$consulta_3 = "SELECT Name, LastName
									   FROM user WHERE idUser = '".$User_idUser_ayudante."'";
						$resulado_3 = mysqli_query($con,$consulta_3);
						if(mysqli_num_rows($resulado_3) > 0)
						{
							$linea_3 = mysqli_fetch_assoc($resulado_3);
							$ayudante = $linea_3['Name']." ".$linea_3['LastName'];
						}mysqli_free_result($resulado_3);

						$query_1 = "SELECT * 
								    FROM placa_camion
								    WHERE id_placa_camion = '".$placa."'";
						$result_1 = mysqli_query($con,$query_1);
						if(mysqli_num_rows($result_1) > 0)
						{
							$row_1 = mysqli_fetch_assoc($result_1);
							$placa_camion = $row_1['placa_camion'];
						}mysqli_free_result($result_1);

						//Filtro para el estado
						if($estado == 1)
						{
							$estado = "Asignando recorridos";
						}
						elseif ($estado == 2)
						{
							$estado = utf8_decode("En revisión");
						}
						elseif ($estado == 3)
						{
							$estado = "Finalizada";
						}

						//Selección del mes según la fecha

						$mes = explode("-", $fecha);
						$mes = $mes[1];
						if($mes == 1)
						{
							$mes = "Enero";
						}
						if($mes == 2)
						{
							$mes = "Febrero";
						}
						if($mes == 3)
						{
							$mes = "Marzo";
						}
						if($mes == 4)
						{
							$mes = "Abril";
						}
						if($mes == 5)
						{
							$mes = "Mayo";
						}
						if($mes == 6)
						{
							$mes = "Junio";
						}
						if($mes == 7)
						{
							$mes = "Julio";
						}
						if($mes == 8)
						{
							$mes = "Agosto";
						}
						if($mes == 9)
						{
							$mes = "Septiembre";
						}
						if($mes == 10)
						{
							$mes = "Octubre";
						}
						if($mes == 11)
						{
							$mes = "Noviembre";
						}
						if($mes == 12)
						{
							$mes = "Diciembre";
						}
						?>
						<tr>
							<td><?php echo $contador; ?></td>
							<td><?php echo $agencia; ?></td>
							<td><?php echo $fecha; ?></td>
							<td><?php echo $mes; ?></td>
							<td><?php echo $placa_camion; ?></td>
							<td><?php echo $salida; ?></td>
							<td><?php echo $llegada; ?></td>
							<td><?php echo $recorrido; ?></td>
							<td><?php echo $total_cil_ent; ?></td>
							<td><?php echo $total_cil_rec; ?></td>
							<td><?php echo $total_ter_ent; ?></td>
							<td><?php echo $total_ter_rec; ?></td>
							<td><?php echo $total_litros; ?></td>
							<td><?php echo $total_otros_ent; ?></td>
							<td><?php echo $total_otros_rec; ?></td>
							<td><?php echo $conductor; ?></td>
							<td><?php echo $total_clientes; ?></td>
							<td><?php echo $total_errores; ?></td>
							<td><?php echo $ayudante; ?></td>
							<td><?php echo $estado; ?></td>

						</tr>

						<?php
					}
				}			
					

			echo "												
			</tbody>
		</table>";		
}
else
{
	header("Location:index.php");
}
?>