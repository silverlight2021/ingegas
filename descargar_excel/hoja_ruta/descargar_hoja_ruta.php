<?php
session_start();

if($_SESSION['logged'] == yes)
{
	require ("../../libraries/conexion.php"); //Archivo de conexión a base de datos

	$idHoja_ruta = isset($_REQUEST['idHoja_ruta']) ? $_REQUEST['idHoja_ruta'] : NULL;

	header("Content-type: text/csv");
	//header("Content-type: application/vnd.ms-excel; charset=UTF-8");
	header("Content-Disposition: attachment; filename=reporte_hoja_ruta.xls");  
	header("Pragma: no-cache"); 
	header("Expires: 0");
	
	$consulta_3 = "SELECT * 
		           FROM hoja_ruta 
		           WHERE idHoja_ruta = '".$idHoja_ruta."'";
	$resultado_3 = mysqli_query($con,$consulta_3);
	if(mysqli_num_rows($resultado_3) > 0)
	{
		$linea_3 = mysqli_fetch_assoc($resultado_3);			
		$fecha = isset($linea_3['fecha']) ? $linea_3['fecha'] : NULL;
		$placa = isset($linea_3['placa']) ? $linea_3['placa'] : NULL;
		$salida = isset($linea_3['salida']) ? $linea_3['salida'] : NULL;
		$llegada = isset($linea_3['llegada']) ? $linea_3['llegada'] : NULL;
		$odometro_salida = isset($linea_3['odometro_salida']) ? $linea_3['odometro_salida'] : NULL;
		$odometro_llegada = isset($linea_3['odometro_llegada']) ? $linea_3['odometro_llegada'] : NULL;
		$recorrido = isset($linea_3['recorrido']) ? $linea_3['recorrido'] : NULL;
		$User_idUser_conductor = isset($linea_3['User_idUser_conductor']) ? $linea_3['User_idUser_conductor'] : NULL;
		$User_idUser_ayudante = isset($linea_3['User_idUser_ayudante']) ? $linea_3['User_idUser_ayudante'] : NULL;
		$estado = isset($linea_3['estado']) ? $linea_3['estado'] : NULL;
		$total_cil_ent = isset($linea_3['total_cil_ent']) ? $linea_3['total_cil_ent'] : NULL;
		$total_cil_rec = isset($linea_3['total_cil_rec']) ? $linea_3['total_cil_rec'] : NULL;
		$total_ter_ent = isset($linea_3['total_ter_ent']) ? $linea_3['total_ter_ent'] : NULL;
		$total_ter_rec = isset($linea_3['total_ter_rec']) ? $linea_3['total_ter_rec'] : NULL;
		$total_litros = isset($linea_3['total_litros']) ? $linea_3['total_litros'] : NULL;
		$total_otros_ent = isset($linea_3['total_otros_ent']) ? $linea_3['total_otros_ent'] : NULL;
		$total_otros_rec = isset($linea_3['total_otros_rec']) ? $linea_3['total_otros_rec'] : NULL;
		$total_dinero = isset($linea_3['total_dinero']) ? $linea_3['total_dinero'] : NULL;
		$total_errores = isset($linea_3['total_errores']) ? $linea_3['total_errores'] : NULL;
		$total_clientes = isset($linea_3['total_clientes']) ? $linea_3['total_clientes'] : NULL;

		$consulta_1 =   "SELECT *
						 FROM placa_camion
						 WHERE id_placa_camion = '".$placa."'";
		$result_1 = mysqli_query($con,$consulta_1);

		if(mysqli_num_rows($result_1) > 0)
		{
			$row_1 = mysqli_fetch_assoc($result_1);
			
			$id_placa_camion = $row_1['id_placa_camion'];
			$placa_camion = $row_1['placa_camion'];			
		}mysqli_free_result($result_1);

		$consulta_2 = "SELECT Name, LastName
				       FROM user WHERE idUser = '".$User_idUser_conductor."'";
		$result_2 = mysqli_query($con,$consulta_2);
		if(mysqli_num_rows($result_2) > 0)
		{
			$linea_2 = mysqli_fetch_assoc($result_2);
			$conductor = $linea_2['Name']." ".$linea_2['LastName'];
		}mysqli_free_result($result_2);

		$consulta_3 = "SELECT Name, LastName
				   	   FROM user WHERE idUser = '".$User_idUser_ayudante."'";
		$result_3 = mysqli_query($con,$consulta_3);
		if(mysqli_num_rows($result_3) > 0)
		{
			$linea_3 = mysqli_fetch_assoc($result_3);
			$ayudante = $linea_3['Name']." ".$linea_3['LastName'];
		}mysqli_free_result($result_3);



	echo	'<table width="100%" align="center">
				<thead>
					<tr>						
						<th>FECHA</th>
						<th>PLACA</th>
						<th>SALIDA</th>
						<th>LLEGADA</th>
						<th>Odo.sal</th>
						<th>Odo.lleg</th>
						<th>Recorrido</th>
						<th>Conductor</th>
						<th>Ayudante</th>
					</tr>
					
				</thead>
				<tbody>
					<tr>
						<td>'.$fecha.'</td>
						<td>'.$placa_camion.'</td>
						<td>'.$salida.'</td>
						<td>'.$llegada.'</td>
						<td>'.$odometro_salida.'</td>
						<td>'.$odometro_llegada.'</td>
						<td>'.$recorrido.'</td>
						<td>'.$conductor.'</td>
						<td>'.$ayudante.'</td>
					</tr>
				</tbody>
			</table>
			<br>
			<br>';
	}




	echo '<table border="1" width="100%" align="center">
			<thead>
				<tr>
					<th rowspan="2">#</th>
					<th rowspan="2">CLIENTE</th>
					<th rowspan="2">ODOMETRO</th>
					<th rowspan="2">FACTURA</th>
					<th rowspan="2">CME</th>
					<th colspan="2" style="text-align: center;">HORA</th>	                            					
					<th colspan="2" style="text-align: center;">CILINDROS</th>
					<th colspan="2" style="text-align: center;">TERMOS</th>
					<th colspan="1" style="text-align: center;">LIN</th>
					<th colspan="3" style="text-align: center;">OTROS</th>
					<th colspan="2" style="text-align: center;">DINERO</th>
					<th rowspan="2" style="text-align: center;">OBSERVACIONES</th>
				</tr>
				<tr>	                            					
					<th>LLEGADA</th>
					<th>SALIDA</th>
					<th>ENT.</th>
					<th>REC.</th>
					<th>ENT.</th>
					<th>REC.</th>
					<th>LITROS</th>
					<th>ITEM</th>
					<th>ENT.</th>
					<th>REC.</th>
					<th>VALOR</th>
					<th>TIPO</th>
				</tr>
			</thead>
			<tbody>';
					
					$consulta_7 = "SELECT * FROM datos_hoja_ruta
								   WHERE idHoja_ruta = '".$idHoja_ruta."'";

					$resultado_7 = mysqli_query($con,$consulta_7);

					if(mysqli_num_rows($resultado_7)> 0)
					{						
						$contador = 0;
						while($linea_7 = mysqli_fetch_assoc($resultado_7))
						{
							$contador++;
							$id_cliente = $linea_7['id_cliente'];
							$odometro = $linea_7['odometro'];
							$num_factura = $linea_7['num_factura'];
							$num_cme = $linea_7['num_cme'];
							$hora_llegada = $linea_7['hora_llegada'];
							$hora_salida = $linea_7['hora_salida'];
							$cili_entregado = $linea_7['cili_entregado'];
							$cili_recibido = $linea_7['cili_recibido'];
							$termo_entregado = $linea_7['termo_entregado'];
							$termo_recibido = $linea_7['termo_recibido'];
							$lin_litros = $linea_7['lin_litros'];
							$otros_item = $linea_7['otros_item'];
							$otros_entregado = $linea_7['otros_entregado'];
							$otros_recibido = $linea_7['otros_recibido'];
							$dinero_valor = $linea_7['dinero_valor'];
							$efectivo_cheque = $linea_7['efectivo_cheque'];
							$observaciones= $linea_7['observaciones'];

							$consulta_8 = "SELECT nombre 
							               FROM clientes 
							               WHERE id_cliente = '".$id_cliente."'";
							$resultado_8 = mysqli_query($con,$consulta_8);
							if(mysqli_num_rows($resultado_8) > 0)
							{
								$linea_8 = mysqli_fetch_assoc($resultado_8);
								$nombre_cliente = $linea_8['nombre'];
							}mysqli_free_result($resultado_8);


							if ($efectivo_cheque == 0)
							{
								$efectivo_cheque = "Vacío";
							}						
							
							echo    "<tr>
										<td>".$contador."</td>
										<td>".utf8_decode($nombre_cliente)."</td>
										<td>".$odometro."</td>
										<td>".$num_factura."</td>
										<td>".$num_cme."</td>
										<td>".$hora_llegada."</td>
										<td>".$hora_salida."</td>
										<td>".$cili_entregado."</td>
										<td>".$cili_recibido."</td>
										<td>".$termo_entregado."</td>
										<td>".$termo_recibido."</td>
										<td>".$lin_litros."</td>
										<td>".$otros_item."</td>
										<td>".$otros_entregado."</td>
										<td>".$otros_recibido."</td>
										<td>".$dinero_valor."</td>
										<td>".utf8_decode($efectivo_cheque)."</td>
										<td>".$observaciones."</td>
									</tr>";							
						}
					}
					else
					{
						echo "ERROR";
					}

			echo "												
			</tbody>
		</table>";
	?>
	<br>
	<table>	

		<tr>
			<td>Clientes totales:</td>
			<td><?php echo $total_clientes; ?></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Totales:</td>
			<td><?php echo $total_cil_ent; ?></td>
			<td><?php echo $total_cil_rec; ?></td>
			<td><?php echo $total_ter_ent;  ?></td>
			<td><?php echo $total_ter_rec;  ?></td>
			<td><?php echo $total_litros;  ?></td>
			<td></td>
			<td><?php echo $total_otros_ent;  ?></td>
			<td><?php echo $total_otros_rec;  ?></td>
			<td><?php echo $total_dinero;  ?></td>
		</tr>
	</table>
	<?php


		
}
else
{
	header("Location:index.php");
}
?>