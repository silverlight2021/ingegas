<?php
require ("../../libraries/conexion.php");
require '../../PHPExcel.php';
$Fecha = date("Y-m-d");
$fecha1 = isset($_REQUEST['fecha1']) ? $_REQUEST['fecha1'] : NULL;
$fecha2 = isset($_REQUEST['fecha2']) ? $_REQUEST['fecha2'] : NULL;
$id_cliente = isset($_REQUEST['id_cliente']) ? $_REQUEST['id_cliente'] : NULL;

//$fecha1 = date("Y-m-d", strtotime($fecha1));
//$fecha2 = date("Y-m-d", strtotime($fecha2));

if ($tipo=1) 
{
    $consulta = "SELECT h.idHoja_ruta, h.fecha, u.Name, u.LastName FROM hoja_ruta h
                INNER JOIN user u
                ON h.User_idUser_conductor = u.idUser
                WHERE h.fecha >= '$fecha1'
                AND h.fecha <= '$fecha2'";
    $resultado = mysqli_query($con,$consulta) ;
    $fila = 2;
    $objPHPExcel  = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("Ingegas")->setDescription("Recorridos por cliente");
    $objPHPExcel->setActiveSheetIndex(0);
    $objPHPExcel->getActiveSheet()->setTitle("Recorridos por Cliente");
    $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Fecha Hoja de Ruta');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Hora Recorrido');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Fecha del Recorrido');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Nombre del Cliente'); 
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Nit'); 
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Numero de Factura'); 
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Numero de CME'); 
    $objPHPExcel->getActiveSheet()->setCellValue('H1', 'Cilindros Entregados'); 
    $objPHPExcel->getActiveSheet()->setCellValue('I1', 'Cilindros Recibidos'); 
    $objPHPExcel->getActiveSheet()->setCellValue('J1', 'Termos Entregados'); 
    $objPHPExcel->getActiveSheet()->setCellValue('K1', 'Termos Recibidos');
    $objPHPExcel->getActiveSheet()->setCellValue('L1', 'Litros');
    $objPHPExcel->getActiveSheet()->setCellValue('M1', 'Otros Entregados'); 
    $objPHPExcel->getActiveSheet()->setCellValue('N1', 'Otros Recibidos');
    $objPHPExcel->getActiveSheet()->setCellValue('O1', 'Dinero');
    $objPHPExcel->getActiveSheet()->setCellValue('P1', 'Observaciones');
    $objPHPExcel->getActiveSheet()->setCellValue('Q1', 'Conductor');
    
    
    
    
    if(mysqli_num_rows($resultado) > 0)
    {
        while ($rows = mysqli_fetch_array($resultado))
        {
            $idHoja_ruta = $rows["idHoja_ruta"];
            $fechaHojaRuta = $rows["fecha"];
            $conductor = $rows["Name"]." ".$rows["LastName"];
            
            $consulta1 = "SELECT d.hora_llegada, d.fecha_llegada, c.nombre, c.nit, d.num_factura, d.num_cme, d.cili_entregado, d.cili_recibido, d.termo_entregado, d.termo_recibido,
                        d.otros_entregado, d.otros_recibido, d.observaciones, d.lin_litros, dinero_valor FROM datos_hoja_ruta d
                        INNER JOIN clientes c
                        ON d.id_cliente = c.id_cliente
                        WHERE d.idHoja_ruta = $idHoja_ruta";
                        
            $resultado1 = mysqli_query($con, $consulta1);
            
            //$objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $id_cliente);
            
            if(mysqli_num_rows($resultado1)>0){
                
                while($linea1 = mysqli_fetch_array($resultado1)){
                
                    $hora_llegada = $linea1["hora_llegada"];
                    $fecha_llegada = $linea1["fecha_llegada"];
                    $nombre = $linea1["nombre"];
                    $nit = $linea1["nit"];
                    $num_factura = $linea1["num_factura"];
                    $num_cme = $linea1["num_cme"];
                    $cili_entregado = $linea1["cili_entregado"];
                    $cili_recibido = $linea1["cili_recibido"];
                    $termo_entregado = $linea1["termo_entregado"];
                    $termo_recibido = $linea1["termo_recibido"];
                    $otros_entregado = $linea1["otros_entregado"];
                    $otros_recibido = $linea1["otros_recibido"];
                    $observaciones = $linea1["observaciones"];
                    $lin_litros = $linea1["lin_litros"];
                    $dinero_valor = $linea1["dinero_valor"];

                    if($fecha_llegada == '0000-00-00'){
                        $pos = strpos($hora_llegada, '(');
                        if($pos !== false){
                            $hora_llegada = explode("(", $hora_llegada);
                            
                            $fecha_llegada = $hora_llegada[1];
                            $hora_llegada = $hora_llegada[0];

                            $fecha_llegada = str_replace(")", "", $fecha_llegada);
                        }
                    }
                    
                    
                    
                    $objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $fechaHojaRuta);
                    $objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $hora_llegada);
                    $objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $fecha_llegada);
                    $objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $nombre);
                    $objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $nit);
                    $objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $num_factura);
                    $objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $num_cme);
                    $objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $cili_entregado);
                    $objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $cili_recibido);
                    $objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, $termo_entregado);
                    $objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $termo_recibido);
                    $objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, $lin_litros);
                    $objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, $otros_entregado);
                    $objPHPExcel->getActiveSheet()->setCellValue('N'.$fila, $otros_recibido);
                    $objPHPExcel->getActiveSheet()->setCellValue('O'.$fila, $dinero_valor);
                    $objPHPExcel->getActiveSheet()->setCellValue('P'.$fila, $observaciones);
                    $objPHPExcel->getActiveSheet()->setCellValue('Q'.$fila, $conductor);
                    
                    $fila++; //Sumamos 1 para pasar a la siguiente fila
                }
            }
         
            
            
        }
    }
        
    header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header('Content-Disposition: attachment;filename="Informe Ruta Cliente.xlsx"');
	header('Cache-Control: max-age=0');
    $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $writer->save('php://output');
}
?>
