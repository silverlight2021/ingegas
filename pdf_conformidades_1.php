<?php
require ("libraries/conexion.php");
date_default_timezone_set("America/Bogota");

$id_no_conformidad = isset($_REQUEST['id_no_conformidad']) ? $_REQUEST['id_no_conformidad'] : NULL;
//$id_no_conformidad=25;
$consulta  = "SELECT * FROM no_conformidad WHERE id_no_conformidad= $id_no_conformidad";
$resultado = mysqli_query($con,$consulta) ;
$linea = mysqli_fetch_array($resultado);
$origen = isset($linea["origen"]) ? $linea["origen"] : NULL;
$fecha_hora = isset($linea["fecha_hora"]) ? $linea["fecha_hora"] : NULL;
$id_tipo_registro = isset($linea["id_tipo_registro"]) ? $linea["id_tipo_registro"] : NULL;	
$descripcion = isset($linea["descripcion"]) ? $linea["descripcion"] : NULL;	
$solicitante = isset($linea["solicitante"]) ? $linea["solicitante"] : NULL;
$correccion = isset($linea["correccion"]) ? $linea["correccion"] : NULL;	
$ana_causas = isset($linea["ana_causas"]) ? $linea["ana_causas"] : NULL;
$accion = isset($linea["accion"]) ? $linea["accion"] : NULL;
$responsable = isset($linea["responsable"]) ? $linea["responsable"] : NULL;	

////////////////////
if ($id_tipo_registro==1) 
{
	$tipo_registro ="No Conformidad";
}
elseif ($id_tipo_registro==2) 
{
	$tipo_registro ="Punto de Mejora";
}
////////////////////
if ($origen=="cliente") 
{
	$origen_origen ="Cliente";
	$destino ="Planta";
}
elseif ($origen=="Planta") 
{
	$origen_origen ="Planta";
	$destino ="Origen";
}
///////////
$path_imagenes = "img/ingegas_pdf.png";
$path_pdf = "Uploads/pdf/certificado/";
$nombre_empresa = utf8_decode("INGEGAS");
$nombre_empresa_ajustado = utf8_decode(substr($nombre_empresa, 0, 47));
$nombre_empresa_1 = utf8_decode("INGENIERIA LTDA");
$nombre_empresa_ajustado_1 = utf8_decode(substr($nombre_empresa_1, 0, 47));

require('funcion_pdf_conformidades_1.php');

$pdf = new PDF_Invoice( 'P', 'mm', 'A4' );
$pdf->AddPage();
$pdf->Image($path_imagenes,3,3,0,0,'');
$pdf->fact_dev1("Reviso: GG");
$pdf->fact_dev2("Aprobo: GG");
$pdf->fact_dev3($nombre_empresa_1);
$pdf->fact_dev4("PROCESO \"SISTEMA DE GESTION DE CALIDAD\"");
$pdf->fact_dev5("NO CONFORMIDAD O PUNTO DE MEJORA DEL PROVEEDOR");
$pdf->fact_dev6("CODIGO   SGC-PL-20");
$pdf->fact_dev7("VERSION      02");
$pdf->fact_dev8("PAGINA     1 DE 2");
$pdf->fact_dev9($fecha_hora);
$pdf->fact_dev10($tipo_registro);
$pdf->fact_dev11($origen_origen);
$pdf->fact_dev12($destino);
$pdf->fact_dev13($descripcion);
$pdf->fact_dev14($solicitante);
$pdf->fact_dev15($correccion);
$pdf->fact_dev16($ana_causas);
$pdf->fact_dev17($accion);
$pdf->fact_dev18($responsable);
$pdf->fact_dev19();
$pdf->fact_dev20($responsable);


$cols=array( "FECHA"   => 45,
             "CUMPLIMIENTO"    => 70,
             "RESPONSABLE"          => 70);
$pdf->addCols($cols);
$cols1=array( "FECHA"  => "L",
             "CUMPLIMIENTO"    => "L",
             "RESPONSABLE"          => "L");
$pdf->addLineFormat( $cols1);
$filas = 0;
$y    = 185;


$num_cili="Numero Cilindro";
$consulta3 = "SELECT *  
				FROM seguimiento_conformidad_formulario  
				WHERE id_no_conformidad = ".$id_no_conformidad;
$resultado3 = mysqli_query($con,$consulta3) ;
while ($linea3 = mysqli_fetch_array($resultado3))
{
	$filas += 1;	
	$fecha_seguimiento = $linea3["fecha_seguimiento"];
	$cumplimiento_seguimiento = $linea3["cumplimiento_seguimiento"];
	$responsable_seguimiento = $linea3["responsable_seguimiento"];	

	

	$line = array( "FECHA"    => $fecha_seguimiento,
				   "CUMPLIMIENTO" => $cumplimiento_seguimiento,
				   "RESPONSABLE"           => $responsable_seguimiento);
	$size = $pdf->addLine( $y, $line );
	$y   += $size + 2;
}
mysqli_free_result($resultado3);


$file_name = str_replace(' ','_',$nombre_empresa_ajustado)."_Certificado_No._".$id_orden.".pdf";
$file_destination = $path_pdf.$file_name;
$pdf->Output($file_destination,'F');
$pdf->Output($file_name,'I');

?>