<?php
date_default_timezone_set("America/Bogota");
require ("libraries/conexion.php");
session_start();

if(@$_SESSION['logged']== 'yes')
{ 
    $acc = $_SESSION['acc'];
    $id_codigo_producto=null; 

	if(isset($_POST['g_codigo']))
	{
	  	if(strlen($_POST['id_codigo_producto']) > 0)
	  	{
	    	$consulta  = "UPDATE codigo_producto
	        SET codigo = '".$_POST['codigo']."',des_cod = '".$_POST['des_cod']."' 
	        WHERE id_codigo_producto = ".$_POST['id_codigo_producto'];
	    	$resultado = mysqli_query($con,$consulta) ;
	    	if ($resultado == FALSE)
	    	{
	    		echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
	    	}
		    else
		    {
		      	?>
		      	<script type="text/javascript">
		    	    alert("Parametro actualizado correctamente");
		      	</script>
		      	<?php
		    }
	  	}
	  	else
	  	{
	  		$consulta  = "INSERT INTO codigo_producto
	        (codigo,des_cod) 
	        VALUES ('".$_POST['codigo']."','".$_POST['des_cod']."')";
	    	$resultado = mysqli_query($con,$consulta) ;
		    if ($resultado == FALSE)
		    {
		      	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    }
		    else
		    {
		      	?>
		      	<script type="text/javascript">
		        	alert("Parametro creado correctamente");
		      	</script>
		      	<?php
		    }
	  	}
	}

	else if(isset($_POST['g_tipo']))
	{
	  
	  	if(strlen($_POST['id_tipo_envace']) > 0)
	  	{
	    	$consulta= "UPDATE tipo_envace
	        SET tipo = '".$_POST['tipo']."' , des_tipo = '".$_POST['des_tipo']."', id_tipo_cilindro = '".$_POST['id_tipo_cilindro1']."'
	        WHERE id_tipo_envace = ".$_POST['id_tipo_envace'];
	    	$resultado = mysqli_query($con,$consulta) ;
		    if ($resultado == FALSE)
		    {
		      	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    }
		    else
		    {
		    	?>
		      	<script type="text/javascript">
		        	alert("Parametro actualizado correctamente");
		      	</script>
		      	<?php
		    }
	  	}
	  	else
	  	{
	    	$consulta  = "INSERT INTO tipo_envace
	        (tipo,des_tipo,id_tipo_cilindro) 
	        VALUES ('".$_POST['tipo']."','".$_POST['des_tipo']."','".$_POST['id_tipo_cilindro1']."')";
	    	$resultado = mysqli_query($con,$consulta) ;
		    if ($resultado == FALSE)
		    {
		      echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    }
		    else
		    {
		      	?>
		      	<script type="text/javascript">
		        	alert("Parametro creado correctamente");
		      	</script>
		    	<?php
		    }
	  	}	
	}
	else if(isset($_POST['g_consecutivo']))
	{
	  
		if(strlen($_POST['id_consecutivo']) > 0)
	  	{
	    	$consulta  = "UPDATE consecutivos
	        SET consecutivo = '".$_POST['consecutivo']."',des_con = '".$_POST['des_con']."'  
	        WHERE id_consecutivo = ".$_POST['id_consecutivo'];
	    	$resultado = mysqli_query($con,$consulta) ;
		    if ($resultado == FALSE)
		    {
			    echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    }
		    else
		    {
		    	?>
		      	<script type="text/javascript">
		        	alert("Parametro actualizado correctamente");
		      	</script>
		      	<?php
		    }
	 	}
	  	else
	  	{
	    	$consulta  = "INSERT INTO consecutivos
	        (consecutivo,des_con) 
	        VALUES ('".$_POST['consecutivo']."','".$_POST['des_con']."')";
	    	$resultado = mysqli_query($con,$consulta) ;
	    	if ($resultado == FALSE)
	    	{
	      		echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    }
		    else
		    {
		    	?>
		      	<script type="text/javascript">
		        	alert("Parametro creado correctamente");
		      	</script>
		      	<?php
		    }
	  }
	}
	else if(isset($_POST['g_servicio']))
	{  
	  	if(strlen($_POST['id_tipo_servicio']) > 0)
	  	{
		    $consulta  = "UPDATE tipo_servicio
		    SET servicio = '".$_POST['servicio']."',des_ser = '".$_POST['des_ser']."'  
		    WHERE id_tipo_servicio = ".$_POST['id_tipo_servicio'];
		    $resultado = mysqli_query($con,$consulta) ;
		    if ($resultado == FALSE)
		    {
		      	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    }
		    else
		    {
		      	?>
		      	<script type="text/javascript">
		        	alert("Parametro actualizado correctamente");
		      	</script>
		      	<?php
		    }
	  	}
	  	else
	  	{
	    	$consulta  = "INSERT INTO tipo_servicio
	        (servicio,des_ser) 
	        VALUES ('".$_POST['servicio']."','".$_POST['des_ser']."')";
	    	$resultado = mysqli_query($con,$consulta) ;
		    if ($resultado == FALSE)
		    {
		    	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    }
		    else
		    {
		      	?>
		      	<script type="text/javascript">
		       		alert("Parametro creado correctamente");
		      	</script>
		      	<?php
		    }
	 	}
	}
	else if(isset($_POST['g_propiedad']))
	{
	  
	 	if(strlen($_POST['id_propiedad_cilindro']) > 0)
	  	{
	    	$consulta  = "UPDATE propiedad_cilindro
	        SET propiedad = '".$_POST['propiedad']."',des_pro = '".$_POST['des_pro']."'  
	        WHERE id_propiedad_cilindro = ".$_POST['id_propiedad_cilindro'];
	    	$resultado = mysqli_query($con,$consulta) ;
		    if ($resultado == FALSE)
		    {
		      	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    }
		    else
		    {
		      	?>
		      	<script type="text/javascript">
		        	alert("Parametro actualizado correctamente");
		      	</script>
		      	<?php
		    }
	  	}
	  	else
	  	{
	    	$consulta  = "INSERT INTO propiedad_cilindro
	        (propiedad,des_pro) 
	        VALUES ('".$_POST['propiedad']."','".$_POST['des_pro']."')";
	    	$resultado = mysqli_query($con,$consulta) ;
		    if ($resultado == FALSE)
		    {
		      echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    }
		    else
		    {
		      ?>
		      <script type="text/javascript">
		        alert("Parametro creado correctamente");
		      </script>
		      <?php
		    }
	 	} 
	}	 
	else if(isset($_POST['g_tipo_gas']))
	{
	  	if(strlen($_POST['id_tipo_cilindro']) > 0)
	  	{
	    	$consulta  = "UPDATE tipo_cilindro
	        SET tipo_cili = '".$_POST['tipo_cili']."',obs_cili = '".$_POST['obs_cili']."' 
	        WHERE id_tipo_cilindro = ".$_POST['id_tipo_cilindro'];
	    	$resultado = mysqli_query($con,$consulta) ;
		    if ($resultado == FALSE)
		    {
		      	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    }
		    else
		    {
		      	?>
		      	<script type="text/javascript">
		        	alert("Parametro actualizado correctamente");
		      	</script>
		      	<?php
		    }
	  	}
	  	else
	  	{
	    	$consulta  = "INSERT INTO tipo_cilindro
	        (tipo_cili,obs_cili) 
	        VALUES ('".$_POST['tipo_cili']."','".$_POST['obs_cili']."')";
	    	$resultado = mysqli_query($con,$consulta) ;
		    if ($resultado == FALSE)
		    {
		      echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    }
		    else
		    {
		      ?>
		      <script type="text/javascript">
		        alert("Parametro creado correctamente");
		      </script>
		      <?php
		    }
	 	}  
	}
	else if(isset($_POST['g_conexion']))
	{
	  
	 	if(strlen($_POST['id_conexion']) > 0)
	  	{
	    	$consulta  = "UPDATE conexion_valvula
	        SET tip_cone = '".$_POST['tip_cone']."',des_cone = '".$_POST['des_cone']."'  
	        WHERE id_conexion = ".$_POST['id_conexion'];
	    	$resultado = mysqli_query($con,$consulta) ;
		    if ($resultado == FALSE)
		    {
		      	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    }
		    else
		    {
		      	?>
		      	<script type="text/javascript">
		        	alert("Parametro actualizado correctamente");
		      	</script>
		      	<?php
		    }
	  	}
	  	else
	  	{
	    	$consulta  = "INSERT INTO conexion_valvula
	        (tip_cone,des_cone) 
	        VALUES ('".$_POST['tip_cone']."','".$_POST['des_cone']."')";
	    	$resultado = mysqli_query($con,$consulta) ;
		    if ($resultado == FALSE)
		    {
		      echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    }
		    else
		    {
		      ?>
		      <script type="text/javascript">
		        alert("Parametro creado correctamente");
		      </script>
		      <?php
		    }
	 	} 
	} 
	else if(isset($_POST['g_lote_pro']))
	{
	  
	 	if(strlen($_POST['id_lote_pro']) > 0)
	  	{
	    	$consulta  = "UPDATE lote_pro
	        SET num_lote_pro = '".$_POST['num_lote_pro']."',des_lote = '".$_POST['des_lote']."'  
	        WHERE id_lote_pro = ".$_POST['id_lote_pro'];
	    	$resultado = mysqli_query($con,$consulta) ;
		    if ($resultado == FALSE)
		    {
		      	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    }
		    else
		    {
		      	?>
		      	<script type="text/javascript">
		        	alert("Parametro actualizado correctamente");
		      	</script>
		      	<?php
		    }
	  	}	  	
	  	else
	  	{
	    	?>
		    <script type="text/javascript">
		    	alert("Debe seleccionar item ");
		    </script>
		    <?php
	 	} 
	  	
	} 
	else if(isset($_POST['g_incert']))
	{
	  
	 	if(strlen($_POST['id_incertidumbre']) > 0)
	  	{
	    	$consulta  = "UPDATE incertidumbre
	        SET incertidumbre = '".$_POST['incertidumbre']."',des_incer = '".$_POST['des_incer']."', resolucion_escala_balanza = '".$_POST['resolucion_escala_balanza']."'  
	        WHERE id_incertidumbre = ".$_POST['id_incertidumbre'];
	    	$resultado = mysqli_query($con,$consulta) ;
		    if ($resultado == FALSE)
		    {
		      	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    }
		    else
		    {
		      	?>
		      	<script type="text/javascript">
		        	alert("Parametro actualizado correctamente");
		      	</script>
		      	<?php
		    }
	  	}
	  	else
	  	{
	    	?>
		    <script type="text/javascript">
		    	alert("Debe seleccionar item ");
		    </script>
		    <?php
	 	} 
	}
	else if(isset($_POST['g_incert_mano'])){

		if(strlen($_POST['id_incertidumbre_manometro']) > 0){
			$consulta = "UPDATE incertidumbre_manometro 
			SET incertidumbre_manometro = '".$_POST['incertidumbre_manometro']."', descrip_incertidumbre = '".$_POST['descrip_incertidumbre']."', resolucion_escala_manometro = '".$_POST['resolucion_escala_manometro']."'
			WHERE id_incertidumbre_manometro = ".$_POST['id_incertidumbre_manometro'];
			$resultado = mysqli_query($con,$consulta);
			if($resultado == FALSE){
				echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
			}
			else{
				?>
		      	<script type="text/javascript">
		        	alert("Parametro actualizado correctamente");
		      	</script>
		      	<?php
			}
		}
	}
	else if(isset($_POST['g_mano_eto']))
	{
	  
	 	if(strlen($_POST['id_manometro_eto']) > 0)
	  	{
	    	$consulta  = "UPDATE manometro_eto
	        SET manometro_eto = '".$_POST['manometro_eto']."',des_man_eto = '".$_POST['des_man_eto']."'  
	        WHERE id_manometro_eto = ".$_POST['id_manometro_eto'];
	    	$resultado = mysqli_query($con,$consulta) ;
		    if ($resultado == FALSE)
		    {
		      	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    }
		    else
		    {
		      	?>
		      	<script type="text/javascript">
		        	alert("Parametro actualizado correctamente");
		      	</script>
		      	<?php
		    }
	  	}
	  	else
	  	{
	    	?>
		    <script type="text/javascript">
		    	alert("Debe seleccionar item ");
		    </script>
		    <?php
	 	} 
	}
	else if(isset($_POST['g_mano_co2']))
	{
	  
	 	if(strlen($_POST['id_manometro_co2']) > 0)
	  	{
	    	$consulta  = "UPDATE manometro_co2
	        SET manometro_co2 = '".$_POST['manometro_co2']."',des_man_co2 = '".$_POST['des_man_co2']."'  
	        WHERE id_manometro_co2 = ".$_POST['id_manometro_co2'];
	    	$resultado = mysqli_query($con,$consulta) ;
		    if ($resultado == FALSE)
		    {
		      	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    }
		    else
		    {
		      	?>
		      	<script type="text/javascript">
		        	alert("Parametro actualizado correctamente");
		      	</script>
		      	<?php
		    }
	  	}
	  	else
	  	{	    	
		    ?>
		    <script type="text/javascript">
		    	alert("Debe seleccionar item ");
		    </script>
		    <?php		    
	 	} 
	}
	else if(isset($_POST['g_firmas']))
	{
	  
	 	if(strlen($_POST['id_formas']) > 0)
	  	{
	    	$consulta  = "UPDATE firmas
	        SET firmas = '".$_POST['firmas']."',cargo = '".$_POST['cargo']."'
	        WHERE id_formas = ".$_POST['id_formas'];
	    	$resultado = mysqli_query($con,$consulta) ;
		    if ($resultado == FALSE)
		    {
		      	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    }
		    else
		    {
		      	?>
		      	<script type="text/javascript">
		        	alert("Parametro actualizado correctamente");
		      	</script>
		      	<?php
		    }
	  	}
	  	else
	  	{
	    	$consulta  = "INSERT INTO firmas
	        (firmas,cargo) 
	        VALUES ('".$_POST['firmas']."','".$_POST['cargo']."')";
	    	$resultado = mysqli_query($con,$consulta) ;
		    if ($resultado == FALSE)
		    {
		      echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    }
		    else
		    {
		      ?>
		      <script type="text/javascript">
		        alert("Parametro creado correctamente");
		      </script>
		      <?php
		    }
	 	} 
	}
	else if(isset($_POST['g_placa_camion']))
	{
	  
	 	if(strlen($_POST['id_placa_camion']) > 0)
	  	{
	    	$consulta  = "UPDATE placa_camion
	        SET placa_camion = '".$_POST['placa_camion']."',des_placa_camion = '".$_POST['des_placa_camion']."'
	        WHERE id_placa_camion = ".$_POST['id_placa_camion'];
	    	$resultado = mysqli_query($con,$consulta) ;
		    if ($resultado == FALSE)
		    {
		      	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    }
		    else
		    {
		      	?>
		      	<script type="text/javascript">
		        	alert("Parametro actualizado correctamente");
		      	</script>
		      	<?php
		    }
	  	}
	  	else
	  	{
	    	$consulta  = "INSERT INTO placa_camion
	        (placa_camion,des_placa_camion) 
	        VALUES ('".$_POST['placa_camion']."','".$_POST['des_placa_camion']."')";
	    	$resultado = mysqli_query($con,$consulta) ;
		    if ($resultado == FALSE)
		    {
		      echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    }
		    else
		    {
		      ?>
		      <script type="text/javascript">
		        alert("Parametro creado correctamente");
		      </script>
		      <?php
		    }
	 	} 
	}
	else if(isset($_POST['g_gas_pev']))
	{
	  
	 	if(strlen($_POST['id_tipo_gas_pev']) > 0)
	  	{
	    	$consulta  = "UPDATE tipo_gas_pev
	        SET nombre = '".$_POST['nombre_gas']."'
	        WHERE id_tipo_gas_pev = ".$_POST['id_tipo_gas_pev'];
	    	$resultado = mysqli_query($con,$consulta) ;
		    if ($resultado == FALSE)
		    {
		      	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    }
		    else
		    {
		      	?>
		      	<script type="text/javascript">
		        	alert("Parametro actualizado correctamente");
		      	</script>
		      	<?php
		    }
	  	}
	  	else
	  	{
	    	$consulta  = "INSERT INTO tipo_gas_pev
	        (nombre) 
	        VALUES ('".$_POST['nombre_gas']."')";
	    	$resultado = mysqli_query($con,$consulta) ;
		    if ($resultado == FALSE)
		    {
		      echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    }
		    else
		    {
		      ?>
		      <script type="text/javascript">
		        alert("Parametro creado correctamente");
		      </script>
		      <?php
		    }
	 	} 
	}
	else if(isset($_POST['g_material_cilindro']))
	{
	  
	 	if(strlen($_POST['id_material']) > 0)
	  	{
	    	$consulta  = "UPDATE material_cilindro
	        SET nombre = '".$_POST['nombre_cilindro']."'
	        WHERE id_material = ".$_POST['id_material'];
	    	$resultado = mysqli_query($con,$consulta) ;
		    if ($resultado == FALSE)
		    {
		      	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    }
		    else
		    {
		      	?>
		      	<script type="text/javascript">
		        	alert("Parametro actualizado correctamente");
		      	</script>
		      	<?php
		    }
	  	}
	  	else
	  	{
	    	$consulta  = "INSERT INTO material_cilindro
	        (nombre) 
	        VALUES ('".$_POST['nombre_cilindro']."')";
	    	$resultado = mysqli_query($con,$consulta) ;
		    if ($resultado == FALSE)
		    {
		      echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    }
		    else
		    {
		      ?>
		      <script type="text/javascript">
		        alert("Parametro creado correctamente");
		      </script>
		      <?php
		    }
	 	} 
	}else if(isset($_POST['g_firma'])){
	    
	    $nombre_firmante = $_POST["nombre_firmante"];
	    $archivo_firma = $_FILES["archivo_firma"]["name"];
	    $rutaFirma = $_FILES["archivo_firma"]["tmp_name"];
	    $destinoFirma = "Uploads/FirmasCertificados/prueba.png";
	    $firma = "prueba.png";
	    
	    $consulta = "INSERT INTO firmasCertificados (nombre_firmante, firma) VALUES ('$nombre_firmante', '$firma')";
	    $resultado = mysqli_query($con, $consulta);
	    if($resultado == FALSE){
	        echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
	    }else{
	        ?>
		      <script type="text/javascript">
		        alert("Parametro creado correctamente");
		      </script>
		      <?php
	    }
	}

	require_once("inc/init.php");
	require_once("inc/config.ui.php");
	$page_title = "Parametros";
	$page_css[] = "your_style.css";
	include("inc/header.php");
	$page_nav[""][""][""][""] = true;
	include("inc/nav.php");
?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
<?php
if (in_array(5, $acc))
{
?>
	<style type="text/css">
	  h2 {display:inline}
	</style>
	<!-- MAIN CONTENT -->
	<div id="content">
		<div class="row">
			<div class="">
				<h1  class="page-title txt-color-blueDark"> <?php echo $page_title; ?>:</h1>				  				
			</div>
		</div>
		<!-- widget grid -->  
		<section id="widget-grid" class="">
			<!-- START ROW -->
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-6">
					<div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h6>Producto </h6>	
						</header>
						<div>							
							<div class="jarviswidget-editbox"></div>
							<div class="widget-body no-padding">
								<form id="checkout-form" class="smart-form" novalidate="novalidate" action="parametros.php#STATE" method="POST">
								<input type="hidden" name="id_codigo_producto" id="id_codigo_producto" value="<?php echo isset($_GET['id_codigo_producto']) ? $_GET['id_codigo_producto'] : NULL; ?>">
                 					<fieldset>
										<div class="row">
											<section class="col col-6">
												<label class="label">Codigo producto :</label>
												<label class="input"> <i class="icon-append fa fa-briefcase"></i>
													<input type="text" name="codigo" placeholder="Codigo producto" value="<?php echo isset($_GET['codigo']) ? $_GET['codigo'] : NULL; ?>">
												</label>
											</section>
											<section class="col col-6">
												<label class="label">Descripción :</label>
												<label class="input"> <i class="icon-append fa fa-briefcase"></i>
													<input type="text" name="des_cod" placeholder="Descripción" value="<?php echo isset($_GET['des_cod']) ? $_GET['des_cod'] : NULL; ?>">
												</label>
											</section>
										</div>
										<?php
										if (in_array(6, $acc))
										{
										?>									
	  									<footer>										
	  											<input type="submit" value="Enviar" name="g_codigo" id="g_codigo" class="btn btn-primary" />
	  									</footer>
	  									<?php
										}										
										?>
                					</fieldset>
									<fieldset>
										<div class="widget-body">											
											<table class="table table-bordered">
												<thead>
													<tr>
														<th>#</th>
							                            <th>Codigo</th>
							                            <th>Descripción</th>                                                
							                            <th>Acción</th>
													</tr>
												</thead>
												<tbody>													
													<?php
							                          	$contador = "0";			                
							                         	$consulta = "SELECT * FROM codigo_producto ORDER BY codigo";
							                          	$resultado = mysqli_query($con,$consulta) ;
							                          	while ($linea = mysqli_fetch_array($resultado))
							                          	{
							                            	$contador = $contador + 1;
							                            	$id_codigo_producto = $linea["id_codigo_producto"];
							                            	$codigo = $linea["codigo"];
							                            	$des_cod = $linea["des_cod"];
							                              
							                            	?>
							                            	<tr class="odd gradeX">
							                               		<td width="5"><?php echo $contador; ?></td>                                  
							                                	<td width="200"><?php echo $codigo; ?></td>
							                                	<td><?php echo $des_cod; ?></td>
							                                	<td width="70">
							                                  		<a class="btn btn-info btn-block" href="parametros.php?id_codigo_producto=<?php echo $id_codigo_producto; ?>&codigo=<?php echo $codigo; ?>&des_cod=<?php echo $des_cod; ?>#STATE">Editar</a>
							                                	</td>
							                            	</tr>
							                          	<?php
							                          	}mysqli_free_result($resultado);                      
							                          	?>
												</tbody>
											</table>
										</div>
									</fieldset>	
								</form>
							</div>
						</div>
					</div>
					<div class="jarviswidget" id="wid-id-2" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h6>Tipo Gas </h6>		
						</header>
            			<div>
							<div class="jarviswidget-editbox"></div>
							<div class="widget-body no-padding">								
								<form id="checkout-form-2" class="smart-form" novalidate="novalidate" action="parametros.php#TIPO_GAS" method="POST">
								<input type="hidden" name="id_tipo_cilindro" id="id_tipo_cilindro" value="<?php echo isset($_GET['id_tipo_cilindro']) ? $_GET['id_tipo_cilindro'] : NULL; ?>">
									<fieldset>
										<div class="row">
											<section class="col col-6">
												<label class="label">Tipo Gas :</label>
												<label class="input"> <i class="icon-append fa fa-briefcase"></i>
													<input type="text" name="tipo_cili" placeholder="Tipo Gas " value="<?php echo isset($_GET['tipo_cili']) ? $_GET['tipo_cili'] : NULL; ?>">
												</label>
											</section>
											<section class="col col-6">
												<label class="label">Descripción :</label>
												<label class="input"> <i class="icon-append fa fa-briefcase"></i>
													<input type="text" name="obs_cili" placeholder="Descripción" value="<?php echo isset($_GET['obs_cili']) ? $_GET['obs_cili'] : NULL; ?>">
												</label>
											</section>
										</div>
										<?php
										if (in_array(6, $acc))
										{
										?>									
										<footer>										
												<input type="submit" value="Enviar" name="g_tipo_gas" id="g_tipo_gas" class="btn btn-primary" />
										</footer>
										<?php
										}										
										?>
	                				</fieldset>
	                  				<fieldset>                  
										<div class="widget-body">											
											<table class="table table-bordered">
												<thead>
													<tr>
														<th>#</th>                                                
							                            <th>Tipo Gas</th>
							                            <th>Descripción</th> 
							                            <th>Acción</th>
													</tr>
												</thead>
												<tbody>													
													<?php
								                        $contador = "0";	                
								                        $consulta = "SELECT * FROM tipo_cilindro ORDER BY id_tipo_cilindro";
								                        $resultado = mysqli_query($con,$consulta) ;
								                        while ($linea = mysqli_fetch_array($resultado))
								                        {
								                          	$contador = $contador + 1;
								                            $id_tipo_cilindro = $linea["id_tipo_cilindro"];
								                            $tipo_cili = $linea["tipo_cili"];
								                            $obs_cili = $linea["obs_cili"];                              
								                            ?>
								                            <tr class="odd gradeX">
								                             	<td width="5"><?php echo $contador; ?></td>                                  
								                              	<td width="200"><?php echo $tipo_cili; ?></td>
								                              	<td><?php echo $obs_cili; ?></td>                                
								                              	<td width="70">
								                                 	<a class="btn btn-info btn-block" href="parametros.php?id_tipo_cilindro=<?php echo $id_tipo_cilindro; ?>&tipo_cili=<?php echo $tipo_cili; ?>&obs_cili=<?php echo $obs_cili; ?>#TIPO_GAS">Editar</a>							                                                                    
								                              </td>
								                            </tr>                           
								                        <?php
								                        }mysqli_free_result($resultado);	                                            
							                  		?>
												</tbody>
											</table>
										</div>
								</fieldset>	
								</form>
							</div>
						</div>
					</div>
<!-- ************************************* end widget div -->
<!-- ************************************* end widget div -->
					<div class="jarviswidget" id="wid-id-3" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h6>Capacidad </h6>		
						</header>
            			<div>
							<div class="jarviswidget-editbox"></div>
							<div class="widget-body no-padding">								
								<form id="checkout-form-3" class="smart-form" novalidate="novalidate" action="parametros.php#TIPO" method="POST">
								<input type="hidden" name="id_tipo_envace" id="id_tipo_envace" value="<?php echo isset($_GET['id_tipo_envace']) ? $_GET['id_tipo_envace'] : NULL; ?>">
									<fieldset>
										<div class="row">
											<section class="col col-4">	
												<?php
												$consulta6 ="SELECT * FROM tipo_cilindro ORDER BY id_tipo_cilindro ASC";
												$resultado6 = mysqli_query($con,$consulta6) ;
												echo "<section>";
												echo "<label class='label'>Tipo Gas :</label>";
												echo"<label class='select'>";
												echo "<select name='id_tipo_cilindro1' >";
												echo "<option value='0'>Seleccione...</option>";
												while($linea6 = mysqli_fetch_array($resultado6))
												{
													$id_tipo_cilindro = $linea6['id_tipo_cilindro'];
													$tipo_cili = $linea6['tipo_cili'];
													if ($id_tipo_cilindro==$id_tipo_cilindro1)
													{
															echo "<option value='$id_tipo_cilindro' selected >$tipo_cili</option>"; 
													}
													else 
													{
															echo "<option value='$id_tipo_cilindro'>$tipo_cili</option>"; 
													} 
												}//fin while 
												echo "</select>";
												echo "<i></i>";
												echo "</label>";
												echo "</section>";																
												?>
											</section>
											<section class="col col-4">
												<label class="label">Capacidad :</label>
												<label class="input"> <i class="icon-append fa fa-briefcase"></i>
													<input type="text" name="tipo" placeholder="Capacidad " value="<?php echo isset($_GET['tipo']) ? $_GET['tipo'] : NULL; ?>">
												</label>
											</section>
											<section class="col col-4">
												<label class="label">Descripción :</label>
												<label class="input"> <i class="icon-append fa fa-briefcase"></i>
													<input type="text" name="des_tipo" placeholder="Descripción" value="<?php echo isset($_GET['des_tipo']) ? $_GET['des_tipo'] : NULL; ?>">
												</label>
											</section>
										</div>
										<?php
										if (in_array(6, $acc))
										{
										?>									
										<footer>										
												<input type="submit" value="Enviar" name="g_tipo" id="g_tipo" class="btn btn-primary" />
										</footer>
										<?php
										}										
										?>
	                				</fieldset>
	                  				<fieldset>                  
										<div class="widget-body">											
											<table class="table table-bordered">
												<thead>
													<tr>
														<th>#</th>   
														<th>Tipo Gas</th>                                             
							                            <th>Capacidad</th>							                            
							                            <th>Descripción</th>
							                            <th>Acción</th>
													</tr>
												</thead>
												<tbody>													
													<?php
								                        $contador = "0";	                
								                        $consulta = "SELECT * FROM tipo_envace ORDER BY id_tipo_cilindro";
								                        $resultado = mysqli_query($con,$consulta) ;
								                        while ($linea = mysqli_fetch_array($resultado))
								                        {
								                          	$contador = $contador + 1;
								                            $id_tipo_envace = $linea["id_tipo_envace"];
								                            $tipo = $linea["tipo"];
								                            $des_tipo = $linea["des_tipo"];
								                            $id_tipo_cilindro1 = $linea["id_tipo_cilindro"];

								                            $consulta1 = "SELECT * FROM tipo_cilindro WHERE id_tipo_cilindro=".$id_tipo_cilindro1;
									                        $resultado1 = mysqli_query($con,$consulta1) ;
									                        while ($linea1 = mysqli_fetch_array($resultado1))
									                        {
									                        	$tipo_cili = $linea1["tipo_cili"];

									                        }mysqli_free_result($resultado1);


								                            ?>
								                            <tr class="odd gradeX">
								                             	<td width="5"><?php echo $contador; ?></td>
								                             	<td><?php echo $tipo_cili; ?></td>                                  
								                              	<td width="200"><?php echo $tipo; ?></td>
								                              	<td><?php echo $des_tipo; ?></td>  
								                              	                                
								                              	<td width="70">
								                                 	<a class="btn btn-info btn-block" href="parametros.php?id_tipo_envace=<?php echo $id_tipo_envace; ?>&tipo=<?php echo $tipo; ?>&des_tipo=<?php echo $des_tipo; ?>&id_tipo_cilindro1=<?php echo $id_tipo_cilindro1; ?>#TIPO">Editar</a>							                                                                    
								                              </td>
								                            </tr>                           
								                        <?php
								                        }mysqli_free_result($resultado);	                                            
							                  		?>
												</tbody>
											</table>
										</div>
								</fieldset>	
								</form>
							</div>
						</div>
					</div>
<!-- ************************************* end widget div -->
					<div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h6>Conexión  Valvula </h6>	
						</header>
						<div>							
							<div class="jarviswidget-editbox"></div>
							<div class="widget-body no-padding">
								<form id="checkout-form-4" class="smart-form" novalidate="novalidate" action="parametros.php#con" method="POST">
								<input type="hidden" name="id_conexion" id="id_conexion" value="<?php echo isset($_GET['id_conexion']) ? $_GET['id_conexion'] : NULL; ?>">
                 					<fieldset>
										<div class="row">
											<section class="col col-6">
												<label class="label">Tipo Conexión :</label>
												<label class="input"> <i class="icon-append fa fa-briefcase"></i>
													<input type="text" name="tip_cone" placeholder="Tipo Conexióno" value="<?php echo isset($_GET['tip_cone']) ? $_GET['tip_cone'] : NULL; ?>">
												</label>
											</section>
											<section class="col col-6">
												<label class="label">Descripción :</label>
												<label class="input"> <i class="icon-append fa fa-briefcase"></i>
													<input type="text" name="des_cone" placeholder="Descripción" value="<?php echo isset($_GET['des_cone']) ? $_GET['des_cone'] : NULL; ?>">
												</label>
											</section>
										</div>
										<?php
										if (in_array(6, $acc))
										{
										?>									
	  									<footer>										
	  											<input type="submit" value="Enviar" name="g_conexion" id="g_conexion" class="btn btn-primary" />
	  									</footer>
	  									<?php
										}										
										?>
                					</fieldset>
									<fieldset>
										<div class="widget-body">											
											<table class="table table-bordered">
												<thead>
													<tr>
														<th>#</th>
							                            <th>Tipo Conexión</th>
							                            <th>Descripción</th>                                                
							                            <th>Acción</th>
													</tr>
												</thead>
												<tbody>													
													<?php
							                          	$contador = "0";			                
							                         	$consulta = "SELECT * FROM conexion_valvula ORDER BY tip_cone";
							                          	$resultado = mysqli_query($con,$consulta) ;
							                          	while ($linea = mysqli_fetch_array($resultado))
							                          	{
							                            	$contador = $contador + 1;
							                            	$id_conexion = $linea["id_conexion"];
							                            	$tip_cone = $linea["tip_cone"];
							                            	$des_cone = $linea["des_cone"];
							                              
							                            	?>
							                            	<tr class="odd gradeX">
							                               		<td width="5"><?php echo $contador; ?></td>                                  
							                                	<td width="200"><?php echo $tip_cone; ?></td>
							                                	<td><?php echo $des_cone; ?></td>
							                                	<td width="70">
							                                  		<a class="btn btn-info btn-block" href="parametros.php?id_conexion=<?php echo $id_conexion; ?>&tip_cone=<?php echo $tip_cone; ?>&des_cone=<?php echo $des_cone; ?>#con">Editar</a>
							                                	</td>
							                            	</tr>
							                          	<?php
							                          	}mysqli_free_result($resultado);                      
							                          	?>
												</tbody>
											</table>
										</div>
									</fieldset>	
								</form>
							</div>
						</div>
					</div>
				
					<div class="jarviswidget" id="wid-id-5" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h6>Consecutivos</h6>				
						</header>
          					<div>
							<div class="jarviswidget-editbox"></div>
							<div class="widget-body no-padding">								
								<form id="checkout-form-5" class="smart-form" novalidate="novalidate" action="parametros.php#CONSECUTIVO" method="POST">
								<input type="hidden" name="id_consecutivo" id="id_consecutivo" value="<?php echo isset($_GET['id_consecutivo']) ? $_GET['id_consecutivo'] : NULL; ?>">
									<fieldset>
										<div class="row">
											<section class="col col-6">
												<label class="label">Consecutivo :</label>
												<label class="input"> <i class="icon-append fa fa-briefcase"></i>
													<input type="text" name="consecutivo" placeholder="Consecutivo" value="<?php echo isset($_GET['consecutivo']) ? $_GET['consecutivo'] : NULL; ?>">
												</label>
											</section>
											<section class="col col-6">
												<label class="label">Descripción :</label>
												<label class="input"> <i class="icon-append fa fa-briefcase"></i>
													<input type="text" name="des_con" placeholder="Descripción" value="<?php echo isset($_GET['des_con']) ? $_GET['des_con'] : NULL; ?>">
												</label>
											</section>
										</div>
										<?php
										if (in_array(6, $acc))
										{
										?>									
										<footer>										
												<input type="submit" value="Enviar" name="g_consecutivo" id="g_consecutivo" class="btn btn-primary" />
										</footer>
										<?php
										}										
										?>
				                    </fieldset>
				                    <fieldset>                  
										<div class="widget-body">											
											<table class="table table-bordered">
												<thead>
													<tr>
														<th>#</th>                                                
							                            <th>Consecutivo</th>
							                            <th>Descripción</th> 
							                            <th>Acción</th>
													</tr>
												</thead>
												<tbody>													
  												<?php
							                        $contador = "0";                
							                        $consulta = "SELECT * 
							                                      FROM consecutivos 
							                                      ORDER BY consecutivo";
							                        $resultado = mysqli_query($con,$consulta) ;
							                        while ($linea = mysqli_fetch_array($resultado))
							                        {
							                            $contador = $contador + 1;
							                            $consecutivo = $linea["consecutivo"];
							                            $id_consecutivo = $linea["id_consecutivo"];
							                            $des_con = $linea["des_con"];                              
							                            ?>
							                            <tr class="odd gradeX">
							                              	<td width="5"><?php echo $contador; ?></td>
							                              	<td width="200"><?php echo $consecutivo; ?></td> 
							                              	<td ><?php echo $des_con; ?></td>                            
							                              	<td width="70">
							                                  	<a class="btn btn-info btn-block" href="parametros.php?id_consecutivo=<?php echo $id_consecutivo; ?>&consecutivo=<?php echo $consecutivo; ?>&des_con=<?php echo $des_con; ?>#CONSECUTIVO">Editar</a>                              
							                             	</td>
							                            </tr>                           
							                        <?php
							                        }mysqli_free_result($resultado);                                           
						                  		?>
												</tbody>
											</table>
										</div>
									</fieldset>	
								</form>
							</div>
						</div>
					</div>
					<!-- ************************************* end widget div -->
					<div class="jarviswidget" id="wid-id-6" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h6>Servicios </h6>		
						</header>
         				<div>
						<div class="jarviswidget-editbox"></div>
              				<div class="widget-body no-padding">								
								<form id="checkout-form-6" class="smart-form" novalidate="novalidate" action="parametros.php#SERVICIO" method="POST">
								<input type="hidden" name="id_tipo_servicio" id="id_tipo_servicio" value="<?php echo isset($_GET['id_tipo_servicio']) ? $_GET['id_tipo_servicio'] : NULL; ?>">
									<fieldset>
										<div class="row">
											<section class="col col-6">
												<label class="label">Servicio :</label>
												<label class="input"> <i class="icon-append fa fa-briefcase"></i>
													<input type="text" name="servicio" placeholder="Servicio" value="<?php echo isset($_GET['servicio']) ? $_GET['servicio'] : NULL; ?>">
												</label>
											</section>
											<section class="col col-6">
												<label class="label">Descripción :</label>
												<label class="input"> <i class="icon-append fa fa-briefcase"></i>
													<input type="text" name="des_ser" placeholder="Descripción" value="<?php echo isset($_GET['des_ser']) ? $_GET['des_ser'] : NULL; ?>">
												</label>
											</section>
										</div>
										<?php
										if (in_array(6, $acc))
										{
										?>									
										<footer>										
												<input type="submit" value="Enviar" name="g_servicio" id="g_servicio" class="btn btn-primary" />
										</footer>
										<?php
										}										
										?>
					                </fieldset>
					                <fieldset>									
										<div class="widget-body">											
											<table class="table table-bordered">
												<thead>
													<tr>
														<th>#</th>                                                
							                            <th>Servicio</th>
							                            <th>Descripción</th> 
							                            <th>Acción</th>
													</tr>
												</thead>
												<tbody>													
												<?php
                         							$contador = "0";	                
						                         	$consulta = "SELECT * FROM tipo_servicio ORDER BY servicio";
						                          	$resultado = mysqli_query($con,$consulta) ;
						                          	while ($linea = mysqli_fetch_array($resultado))
						                          	{
						                            	$contador = $contador + 1;
						                            	$id_tipo_servicio = $linea["id_tipo_servicio"];
						                            	$servicio = $linea["servicio"];
						                            	$des_ser = $linea["des_ser"];                                                              
						                            	?>
						                              	<tr class="odd gradeX">
						                                	<td width="5"><?php echo $contador; ?></td>
						                                	<td width="200"><?php echo $servicio; ?></td>  
						                                	<td><?php echo $des_ser; ?></td>                                
						                                	<td width="70">
						                                  		<a class="btn btn-info btn-block" href="parametros.php?id_tipo_servicio=<?php echo $id_tipo_servicio; ?>&servicio=<?php echo $servicio; ?>&des_ser=<?php echo $des_ser; ?>#SERVICIO">Editar</a>
						                                </td>
						                              	</tr>                             
						                            	<?php
						                            }mysqli_free_result($resultado);	                                            
						                    	?>
												</tbody>
											</table>
										</div>
									</fieldset>	
								</form>
							</div>
						</div>
					</div>
					<!-- ************************************* end widget div -->
					<div class="jarviswidget" id="wid-id-7" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h6>Propiedad Cilindro </h6>	
						</header>
						<div>
							<div class="jarviswidget-editbox"></div>
							<div class="widget-body no-padding">
								<form id="checkout-form-7" class="smart-form" novalidate="novalidate" action="parametros.php#PROPIEDAD" method="POST">
								<input type="hidden" name="id_propiedad_cilindro" id="id_propiedad_cilindro" value="<?php echo isset($_GET['id_propiedad_cilindro']) ? $_GET['id_propiedad_cilindro'] : NULL; ?>">
									<fieldset>
										<div class="row">
											<section class="col col-6">
												<label class="label">Propiedad Cilindro :</label>
												<label class="input"> <i class="icon-append fa fa-briefcase"></i>
													<input type="text" name="propiedad" placeholder="Propiedad Cilindro" value="<?php echo isset($_GET['propiedad']) ? $_GET['propiedad'] : NULL; ?>">
												</label>
											</section>
											<section class="col col-6">
												<label class="label">Descripción :</label>
												<label class="input"> <i class="icon-append fa fa-briefcase"></i>
													<input type="text" name="des_pro" placeholder="Descripción" value="<?php echo isset($_GET['des_pro']) ? $_GET['des_pro'] : NULL; ?>">
												</label>
											</section>
										</div>
										<?php
										if (in_array(6, $acc))
										{
										?>									
										<footer>										
												<input type="submit" value="Enviar" name="g_propiedad" id="g_propiedad" class="btn btn-primary" />
										</footer>
										<?php
										}										
										?>
									</fieldset>
                 					<fieldset>
										<div class="widget-body">											
											<table class="table table-bordered">
												<thead>
													<th>#</th>                                                
							                        <th>Propiedad </th>
							                        <th>Descripción</th> 
							                        <th>Acción</th>
												</thead>
												<tbody>													
												<?php
													$contador = "0";
	                          						$consulta = "SELECT * FROM propiedad_cilindro ORDER BY propiedad";
							                        $resultado = mysqli_query($con,$consulta) ;
							                        while ($linea = mysqli_fetch_array($resultado))
							                        {
							                          	$contador = $contador + 1;
							                         	$id_propiedad_cilindro = $linea["id_propiedad_cilindro"];
							                          	$propiedad = $linea["propiedad"];
							                          	$des_pro = $linea["des_pro"];                                                            
							                          	?>
							                          	<tr class="odd gradeX">
							                            	<td width="5"><?php echo $contador; ?></td>
							                            	<td width="200"><?php echo $propiedad; ?></td>
							                            	<td><?php echo $des_pro; ?></td>                            
							                             	<td width="70">
							                                	<a class="btn btn-info btn-block" href="parametros.php?id_propiedad_cilindro=<?php echo $id_propiedad_cilindro; ?>&propiedad=<?php echo $propiedad; ?>&des_pro=<?php echo $des_pro; ?>#PROPIEDAD">Editar</a>
							                            	</td>
							                          	</tr>                             
							                        <?php
							                        }mysqli_free_result($resultado);	                                            
						                  		  ?>
												</tbody>
											</table>
										</div>
									</fieldset>	
								</form>
							</div>
						</div>
					</div>
					<!-- ************************************* end widget div -->
					<div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h6>Consecutivo Lote Producción </h6>	
						</header>
						<div>
							<div class="jarviswidget-editbox"></div>
							<div class="widget-body no-padding">
								<form id="checkout-form-8" class="smart-form" novalidate="novalidate" action="parametros.php#LOTE" method="POST">
								<input type="hidden" name="id_lote_pro" id="id_lote_pro" value="<?php echo isset($_GET['id_lote_pro']) ? $_GET['id_lote_pro'] : NULL; ?>">
									<fieldset>
										<div class="row">
											<section class="col col-6">
												<label class="label">Lote Producción :</label>
												<label class="input"> <i class="icon-append fa fa-briefcase"></i>
													<input type="text" name="num_lote_pro" placeholder="Lote Producción" value="<?php echo isset($_GET['num_lote_pro']) ? $_GET['num_lote_pro'] : NULL; ?>">
												</label>
											</section>
											<section class="col col-6">
												<label class="label">Descripción :</label>
												<label class="input"> <i class="icon-append fa fa-briefcase"></i>
													<input type="text" name="des_lote" placeholder="Descripción" value="<?php echo isset($_GET['des_lote']) ? $_GET['des_lote'] : NULL; ?>">
												</label>
											</section>
										</div>
										<?php
										if (in_array(6, $acc))
										{
										?>									
										<footer>										
												<input type="submit" value="Enviar" name="g_lote_pro" id="g_lote_pro" class="btn btn-primary" />
										</footer>
										<?php
										}										
										?>
									</fieldset>
                 					<fieldset>
										<div class="widget-body">											
											<table class="table table-bordered">
												<thead>
													<th>#</th>                                                
							                        <th>Propiedad </th>
							                        <th>Descripción</th> 
							                        <th>Acción</th>
												</thead>
												<tbody>													
												<?php
													$contador = "0";
	                          						$consulta = "SELECT * FROM lote_pro ORDER BY num_lote_pro";
							                        $resultado = mysqli_query($con,$consulta) ;
							                        while ($linea = mysqli_fetch_array($resultado))
							                        {
							                          	$contador = $contador + 1;
							                         	$id_lote_pro = $linea["id_lote_pro"];
							                          	$num_lote_pro = $linea["num_lote_pro"];
							                          	$des_lote = $linea["des_lote"];                                                            
							                          	?>
							                          	<tr class="odd gradeX">
							                            	<td width="5"><?php echo $contador; ?></td>
							                            	<td width="200"><?php echo $num_lote_pro; ?></td>
							                            	<td><?php echo $des_lote; ?></td>                            
							                             	<td width="70">
							                                	<a class="btn btn-info btn-block" href="parametros.php?id_lote_pro=<?php echo $id_lote_pro; ?>&num_lote_pro=<?php echo $num_lote_pro; ?>&des_lote=<?php echo $des_lote; ?>#LOTE">Editar</a>
							                            	</td>
							                          	</tr>                             
							                        <?php
							                        }mysqli_free_result($resultado);	                                            
						                  		  ?>
												</tbody>
											</table>
										</div>
									</fieldset>	
								</form>
							</div>
						</div>
					</div>
					<!-- ************************************* end widget div -->
					<div class="jarviswidget" id="wid-id-9" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h6>Incertidumbre Báscula</h6>	
						</header>
						<div>
							<div class="jarviswidget-editbox"></div>
							<div class="widget-body no-padding">
								<form id="checkout-form-9" class="smart-form" novalidate="novalidate" action="parametros.php#INSERT" method="POST">
								<input type="hidden" name="id_incertidumbre" id="id_incertidumbre" value="<?php echo isset($_GET['id_incertidumbre']) ? $_GET['id_incertidumbre'] : NULL; ?>">
									<fieldset>
										<div class="row">
											<section class="col col-6">
												<label class="label">Incertidumbre :</label>
												<label class="input"> <i class="icon-append fa fa-briefcase"></i>
													<input type="text" name="incertidumbre" placeholder="Incertidumbre" value="<?php echo isset($_GET['incertidumbre']) ? $_GET['incertidumbre'] : NULL; ?>">
												</label>
											</section>
											<section class="col col-6">
												<label class="label">Resolucion Escala Balanza :</label>
												<label class="input"> <i class="icon-append fa fa-briefcase"></i>
													<input type="text" name="resolucion_escala_balanza" placeholder="Resolucion Escala Balanza" value="<?php echo isset($_GET['resolucion_escala_balanza']) ? $_GET['resolucion_escala_balanza'] : NULL; ?>">
												</label>
											</section>
											<section class="col col-6">
												<label class="label">Descripción :</label>
												<label class="input"> <i class="icon-append fa fa-briefcase"></i>
													<input type="text" name="des_incer" placeholder="Descripción" value="<?php echo isset($_GET['des_incer']) ? $_GET['des_incer'] : NULL; ?>">
												</label>
											</section>
										</div>
										<?php
										if (in_array(6, $acc))
										{
										?>									
										<footer>										
												<input type="submit" value="Enviar" name="g_incert" id="g_incert" class="btn btn-primary" />
										</footer>
										<?php
										}										
										?>
									</fieldset>
                 					<fieldset>
										<div class="widget-body">											
											<table class="table table-bordered">
												<thead>
													<th>#</th>                                                
							                        <th>Incertidumbre </th>
							                        <th>Resolución Escala Balanza</th>
							                        <th>Descripción</th> 
							                        <th>Acción</th>
												</thead>
												<tbody>													
												<?php
													$contador = "0";
	                          						$consulta = "SELECT * FROM incertidumbre ORDER BY incertidumbre";
							                        $resultado = mysqli_query($con,$consulta) ;
							                        while ($linea = mysqli_fetch_array($resultado))
							                        {
							                          	$contador = $contador + 1;
							                         	$id_incertidumbre = $linea["id_incertidumbre"];
							                          	$incertidumbre = $linea["incertidumbre"];
							                          	$resolucion_escala_balanza = $linea["resolucion_escala_balanza"];
							                          	$des_incer = $linea["des_incer"];

							                          	?>
							                          	<tr class="odd gradeX">
							                            	<td width="5"><?php echo $contador; ?></td>
							                            	<td width="200"><?php echo $incertidumbre; ?></td>
							                            	<td width="200"><?php echo $resolucion_escala_balanza; ?></td>
							                            	<td><?php echo $des_incer; ?></td>                            
							                             	<td width="70">
							                                	<a class="btn btn-info btn-block" href="parametros.php?id_incertidumbre=<?php echo $id_incertidumbre; ?>&incertidumbre=<?php echo $incertidumbre; ?>&des_incer=<?php echo $des_incer; ?>#INSERT">Editar</a>
							                            	</td>
							                          	</tr>                             
							                        <?php
							                        }mysqli_free_result($resultado);	                                            
						                  		  ?>
												</tbody>
											</table>
										</div>
									</fieldset>	
								</form>
							</div>
						</div>
					</div>
					<!-- ************************************* end widget div -->
					<!-- ************************************* end widget div -->
					<div class="jarviswidget" id="wid-id-9" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h6>Incertidumbre Manometro</h6>	
						</header>
						<div>
							<div class="jarviswidget-editbox"></div>
							<div class="widget-body no-padding">
								<form id="checkout-form-9" class="smart-form" novalidate="novalidate" action="parametros.php#INSERT" method="POST">
								<input type="hidden" name="id_incertidumbre_manometro" id="id_incertidumbre_manometro" value="<?php echo isset($_GET['id_incertidumbre_manometro']) ? $_GET['id_incertidumbre_manometro'] : NULL; ?>">
									<fieldset>
										<div class="row">
											<section class="col col-6">
												<label class="label">Incertidumbre :</label>
												<label class="input"> <i class="icon-append fa fa-briefcase"></i>
													<input type="text" name="incertidumbre_manometro" placeholder="Incertidumbre" value="<?php echo isset($_GET['incertidumbre_manometro']) ? $_GET['incertidumbre_manometro'] : NULL; ?>">
												</label>
											</section>
											<section class="col col-6">
												<label class="label">Resolución Escala Manometro :</label>
												<label class="input"> <i class="icon-append fa fa-briefcase"></i>
													<input type="text" name="resolucion_escala_manometro" placeholder="Resolución Escala Manometro" value="<?php echo isset($_GET['resolucion_escala_manometro']) ? $_GET['resolucion_escala_manometro'] : NULL; ?>">
												</label>
											</section>
											<section class="col col-6">
												<label class="label">Descripción :</label>
												<label class="input"> <i class="icon-append fa fa-briefcase"></i>
													<input type="text" name="descrip_incertidumbre" placeholder="Descripción" value="<?php echo isset($_GET['descrip_incertidumbre']) ? $_GET['descrip_incertidumbre'] : NULL; ?>">
												</label>
											</section>
										</div>
										<?php
										if (in_array(6, $acc))
										{
										?>									
										<footer>										
												<input type="submit" value="Enviar" name="g_incert_mano" id="g_incert_mano" class="btn btn-primary" />
										</footer>
										<?php
										}										
										?>
									</fieldset>
                 					<fieldset>
										<div class="widget-body">											
											<table class="table table-bordered">
												<thead>
													<th>#</th>                                                
							                        <th>Incertidumbre </th>
							                        <th>Resolución Escala Manometro</th>
							                        <th>Descripción</th>
							                        <th>Acción</th>
												</thead>
												<tbody>													
												<?php
													$contador = "0";
	                          						$consulta = "SELECT * FROM incertidumbre_manometro ORDER BY id_incertidumbre_manometro";
							                        $resultado = mysqli_query($con,$consulta) ;
							                        while ($linea = mysqli_fetch_array($resultado))
							                        {
							                          	$contador = $contador + 1;
							                         	$id_incertidumbre_manometro = $linea["id_incertidumbre_manometro"];
							                          	$incertidumbre_manometro = $linea["incertidumbre_manometro"];
							                          	$descrip_incertidumbre = $linea["descrip_incertidumbre"];                                                            
							                          	$resolucion_escala_manometro = $linea["resolucion_escala_manometro"];
							                          	?>
							                          	<tr class="odd gradeX">
							                            	<td width="5"><?php echo $contador; ?></td>
							                            	<td width="200"><?php echo $incertidumbre_manometro; ?></td>
							                            	<td><?php echo $resolucion_escala_manometro ?></td> 
							                            	<td><?php echo $descrip_incertidumbre; ?></td> 
							                             	<td width="70">
							                                	<a class="btn btn-info btn-block" href="parametros.php?id_incertidumbre_manometro=<?php echo $id_incertidumbre_manometro; ?>&incertidumbre_manometro=<?php echo $incertidumbre_manometro; ?>&descrip_incertidumbre=<?php echo $descrip_incertidumbre; ?>#INSERT">Editar</a>
							                            	</td>
							                          	</tr>                             
							                        <?php
							                        }mysqli_free_result($resultado);	                                            
						                  		  ?>
												</tbody>
											</table>
										</div>
									</fieldset>	
								</form>
							</div>
						</div>
					</div>
					<!-- ************************************* end widget div -->
					<!-- ************************************* end widget div -->
					<div class="jarviswidget" id="wid-id-10" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h6>Manometro ETO </h6>	
						</header>
						<div>
							<div class="jarviswidget-editbox"></div>
							<div class="widget-body no-padding">
								<form id="checkout-form-10" class="smart-form" novalidate="novalidate" action="parametros.php#INSERT" method="POST">
								<input type="hidden" name="id_manometro_eto" id="id_manometro_eto" value="<?php echo isset($_GET['id_manometro_eto']) ? $_GET['id_manometro_eto'] : NULL; ?>">
									<fieldset>
										<div class="row">
											<section class="col col-6">
												<label class="label">Manometro ETO :</label>
												<label class="input"> <i class="icon-append fa fa-briefcase"></i>
													<input type="text" name="manometro_eto" placeholder="Manometro ETO" value="<?php echo isset($_GET['manometro_eto']) ? $_GET['manometro_eto'] : NULL; ?>">
												</label>
											</section>
											<section class="col col-6">
												<label class="label">Descripción :</label>
												<label class="input"> <i class="icon-append fa fa-briefcase"></i>
													<input type="text" name="des_man_eto" placeholder="Descripción" value="<?php echo isset($_GET['des_man_eto']) ? $_GET['des_man_eto'] : NULL; ?>">
												</label>
											</section>
										</div>
										<?php
										if (in_array(6, $acc))
										{
										?>									
										<footer>										
												<input type="submit" value="Enviar" name="g_mano_eto" id="g_mano_eto" class="btn btn-primary" />
										</footer>
										<?php
										}										
										?>
									</fieldset>
                 					<fieldset>
										<div class="widget-body">											
											<table class="table table-bordered">
												<thead>
													<th>#</th>                                                
							                        <th>Manometro ETO</th>
							                        <th>Descripción</th> 
							                        <th>Acción</th>
												</thead>
												<tbody>													
												<?php
													$contador = "0";
	                          						$consulta = "SELECT * FROM manometro_eto ORDER BY manometro_eto";
							                        $resultado = mysqli_query($con,$consulta) ;
							                        while ($linea = mysqli_fetch_array($resultado))
							                        {
							                          	$contador = $contador + 1;
							                         	$id_manometro_eto = $linea["id_manometro_eto"];
							                          	$manometro_eto = $linea["manometro_eto"];
							                          	$des_man_eto = $linea["des_man_eto"];                                                            
							                          	?>
							                          	<tr class="odd gradeX">
							                            	<td width="5"><?php echo $contador; ?></td>
							                            	<td width="200"><?php echo $manometro_eto; ?></td>
							                            	<td><?php echo $des_man_eto; ?></td>                            
							                             	<td width="70">
							                                	<a class="btn btn-info btn-block" href="parametros.php?id_manometro_eto=<?php echo $id_manometro_eto; ?>&manometro_eto=<?php echo $manometro_eto; ?>&des_man_eto=<?php echo $des_man_eto; ?>#INSERT">Editar</a>
							                            	</td>
							                          	</tr>                             
							                        <?php
							                        }mysqli_free_result($resultado);	                                            
						                  		  ?>
												</tbody>
											</table>
										</div>
									</fieldset>	
								</form>
							</div>
						</div>
					</div>
					<!-- ************************************* end widget div -->
					<!-- ************************************* end widget div -->
					<div class="jarviswidget" id="wid-id-11" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i></span>
							<h6>Manometro CO2 </h6>	
						</header>
						<div>
							<div class="jarviswidget-editbox"></div>
							<div class="widget-body no-padding">
								<form id="checkout-form-11" class="smart-form" novalidate="novalidate" action="parametros.php#INSERT" method="POST">
								<input type="hidden" name="id_manometro_co2" id="id_manometro_co2" value="<?php echo isset($_GET['id_manometro_co2']) ? $_GET['id_manometro_co2'] : NULL; ?>">
									<fieldset>
										<div class="row">
											<section class="col col-6">
												<label class="label">Manometro CO2 :</label>
												<label class="input"> <i class="icon-append fa fa-briefcase"></i>
													<input type="text" name="manometro_co2" placeholder="Manometro C02" value="<?php echo isset($_GET['manometro_co2']) ? $_GET['manometro_co2'] : NULL; ?>">
												</label>
											</section>
											<section class="col col-6">
												<label class="label">Descripción :</label>
												<label class="input"> <i class="icon-append fa fa-briefcase"></i>
													<input type="text" name="des_man_co2" placeholder="Descripción" value="<?php echo isset($_GET['des_man_co2']) ? $_GET['des_man_co2'] : NULL; ?>">
												</label>
											</section>
										</div>
										<?php
										if (in_array(6, $acc))
										{
										?>									
										<footer>										
												<input type="submit" value="Enviar" name="g_mano_co2" id="g_mano_co2" class="btn btn-primary" />
										</footer>
										<?php
										}										
										?>
									</fieldset>
                 					<fieldset>
										<div class="widget-body">											
											<table class="table table-bordered">
												<thead>
													<th>#</th>                                                
							                        <th>Manometro CO2</th>
							                        <th>Descripción</th> 
							                        <th>Acción</th>
												</thead>
												<tbody>													
												<?php
													$contador = "0";
	                          						$consulta = "SELECT * FROM manometro_co2 ORDER BY manometro_co2";
							                        $resultado = mysqli_query($con,$consulta) ;
							                        while ($linea = mysqli_fetch_array($resultado))
							                        {
							                          	$contador = $contador + 1;
							                         	$id_manometro_co2	 = $linea["id_manometro_co2"];
							                          	$manometro_co2 = $linea["manometro_co2"];
							                          	$des_man_co2 = $linea["des_man_co2"];                                                            
							                          	?>
							                          	<tr class="odd gradeX">
							                            	<td width="5"><?php echo $contador; ?></td>
							                            	<td width="200"><?php echo $manometro_co2; ?></td>
							                            	<td><?php echo $des_man_co2; ?></td>                            
							                             	<td width="70">
							                                	<a class="btn btn-info btn-block" href="parametros.php?id_manometro_co2	=<?php echo $id_manometro_co2	; ?>&manometro_co2=<?php echo $manometro_co2; ?>&des_man_co2=<?php echo $des_man_co2; ?>#INSERT">Editar</a>
							                            	</td>
							                          	</tr>                             
							                        <?php
							                        }mysqli_free_result($resultado);	                                            
						                  		  ?>
												</tbody>
											</table>
										</div>
									</fieldset>	
								</form>
							</div>
						</div>
					</div>
					<!-- ************************************* end widget div -->
					<!-- ************************************* end widget div -->
					<div class="jarviswidget" id="wid-id-12" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h6>Certificado </h6>	
						</header>
						<div>
							<div class="jarviswidget-editbox"></div>
							<div class="widget-body no-padding">
								<form id="checkout-form-12" class="smart-form" novalidate="novalidate" action="parametros.php#INSERT" method="POST">
								<input type="hidden" name="id_formas" id="id_formas" value="<?php echo isset($_GET['id_formas']) ? $_GET['id_formas'] : NULL; ?>">
									<fieldset>
										<div class="row">
											<section class="col col-6">
												<label class="label">Nombre  :</label>
												<label class="input"> <i class="icon-append fa fa-briefcase"></i>
													<input type="text" name="firmas" placeholder="Nombre" value="<?php echo isset($_GET['firmas']) ? $_GET['firmas'] : NULL; ?>">
												</label>
											</section>
											<section class="col col-6">
												<label class="label">Cargo  :</label>
												<label class="input"> <i class="icon-append fa fa-briefcase"></i>
													<input type="text" name="cargo" placeholder="Cargo" value="<?php echo isset($_GET['cargo']) ? $_GET['cargo'] : NULL; ?>">
												</label>
											</section>
										</div>
										<?php
										if (in_array(6, $acc))
										{
										?>									
										<footer>										
												<input type="submit" value="Enviar" name="g_firmas" id="g_firmas" class="btn btn-primary" />
										</footer>
										<?php
										}										
										?>
									</fieldset>
                 					<fieldset>
										<div class="widget-body">											
											<table class="table table-bordered">
												<thead>
													<th>#</th>                                                
							                        <th>Nombre</th> 
							                        <th>Cargo</th> 
							                        <th>Acción</th>
												</thead>
												<tbody>													
												<?php
													$contador = "0";
	                          						$consulta = "SELECT * FROM firmas ORDER BY firmas";
							                        $resultado = mysqli_query($con,$consulta) ;
							                        while ($linea = mysqli_fetch_array($resultado))
							                        {
							                          	$contador = $contador + 1;
							                         	$id_formas	 = $linea["id_formas"];
							                          	$firmas = $linea["firmas"];
							                          	$cargo = $linea["cargo"];                                                            
							                          	?>
							                          	<tr class="odd gradeX">
							                            	<td width="5"><?php echo $contador; ?></td>
							                            	<td width="100"><?php echo $firmas; ?></td>
							                            	<td width="100"><?php echo $cargo; ?></td>                          
							                             	<td width="70">
							                                	<a class="btn btn-info btn-block" href="parametros.php?id_formas=<?php echo $id_formas	; ?>&firmas=<?php echo $firmas; ?>#INSERT">Editar</a>
							                            	</td>
							                          	</tr>                             
							                        <?php
							                        }mysqli_free_result($resultado);	                                            
						                  		  ?>
												</tbody>
											</table>
										</div>
									</fieldset>	
								</form>
							</div>
						</div>
					</div>
					<!-- ************************************* end widget div -->
					<!-- ************************************* end widget div -->
					<div class="jarviswidget" id="wid-id-13" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h6>Placa Camión </h6>	
						</header>
						<div>
							<div class="jarviswidget-editbox"></div>
							<div class="widget-body no-padding">
								<form id="checkout-form-13" class="smart-form" novalidate="novalidate" action="parametros.php#INSERT" method="POST">
								<input type="hidden" name="id_placa_camion" id="id_placa_camion" value="<?php echo isset($_GET['id_placa_camion']) ? $_GET['id_placa_camion'] : NULL; ?>">
									<fieldset>
										<div class="row">
											<section class="col col-6">
												<label class="label">Placa Camión :</label>
												<label class="input"> <i class="icon-append fa fa-briefcase"></i>
													<input type="text" name="placa_camion" placeholder="Placa Camión" value="<?php echo isset($_GET['placa_camion']) ? $_GET['placa_camion'] : NULL; ?>">
												</label>
											</section>
											<section class="col col-6">
												<label class="label">Descripción :</label>
												<label class="input"> <i class="icon-append fa fa-briefcase"></i>
													<input type="text" name="des_placa_camion" placeholder="Descripción" value="<?php echo isset($_GET['des_placa_camion']) ? $_GET['des_placa_camion'] : NULL; ?>">
												</label>
											</section>
										</div>
										<?php
										if (in_array(6, $acc))
										{
										?>									
										<footer>										
												<input type="submit" value="Enviar" name="g_placa_camion" id="g_placa_camion" class="btn btn-primary" />
										</footer>
										<?php
										}										
										?>
									</fieldset>
                 					<fieldset>
										<div class="widget-body">											
											<table class="table table-bordered">
												<thead>
													<th>#</th>                                                
							                        <th>Placa</th> 
							                        <th>Descripción</th> 
							                        <th>Acción</th>
												</thead>
												<tbody>													
												<?php
													$contador = "0";
	                          						$consulta = "SELECT * FROM placa_camion ORDER BY placa_camion";
							                        $resultado = mysqli_query($con,$consulta) ;
							                        while ($linea = mysqli_fetch_array($resultado))
							                        {
							                          	$contador = $contador + 1;
							                         	$id_placa_camion	 = $linea["id_placa_camion"];
							                          	$placa_camion = $linea["placa_camion"];
							                          	$des_placa_camion = $linea["des_placa_camion"];                                                            
							                          	?>
							                          	<tr class="odd gradeX">
							                            	<td width="5"><?php echo $contador; ?></td>
							                            	<td width="100"><?php echo $placa_camion; ?></td>
							                            	<td width="100"><?php echo $des_placa_camion; ?></td>                          
							                             	<td width="70">
							                                	<a class="btn btn-info btn-block" href="parametros.php?id_placa_camion=<?php echo $id_placa_camion	; ?>&placa_camion=<?php echo $placa_camion; ?>&des_placa_camion=<?php echo $des_placa_camion; ?>#INSERT">Editar</a>
							                            	</td>
							                          	</tr>                             
							                        <?php
							                        }mysqli_free_result($resultado);	                                            
						                  		  ?>
												</tbody>
											</table>
										</div>
									</fieldset>	
								</form>
							</div>
						</div>
					</div>
					<!-- ************************************* end widget div -->
					<!-- ************************************* end widget div -->
					<div class="jarviswidget" id="wid-id-7" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h6>Tipo Gas PEV </h6>	
						</header>
						<div>
							<div class="jarviswidget-editbox"></div>
							<div class="widget-body no-padding">
								<form id="checkout-form-7" class="smart-form" novalidate="novalidate" action="parametros.php#GAS" method="POST">
								<input type="hidden" name="id_tipo_gas_pev" id="id_tipo_gas_pev" value="<?php echo isset($_GET['id_tipo_gas_pev']) ? $_GET['id_tipo_gas_pev'] : NULL; ?>">
									<fieldset>
										<div class="row">
											<section class="col col-6">
												<label class="label">Nombre GAS:</label>
												<label class="input"> <i class="icon-append fa fa-briefcase"></i>
													<input type="text" name="nombre_gas" placeholder="Nombre GAS" value="<?php echo isset($_GET['nombre_gas']) ? $_GET['nombre_gas'] : NULL; ?>">
												</label>
											</section>
										</div>
										<?php
										if (in_array(6, $acc))
										{
										?>									
										<footer>										
												<input type="submit" value="Enviar" name="g_gas_pev" id="g_gas_pev" class="btn btn-primary" />
										</footer>
										<?php
										}										
										?>
									</fieldset>
                 					<fieldset>
										<div class="widget-body">											
											<table class="table table-bordered">
												<thead>
													<th>#</th>                                                
							                        <th>Nombre GAS</th>
							                        <th>Acción</th>
												</thead>
												<tbody>													
												<?php
													$contador = "0";
	                          						$consulta = "SELECT * FROM tipo_gas_pev ORDER BY id_tipo_gas_pev";
							                        $resultado = mysqli_query($con,$consulta) ;
							                        while ($linea = mysqli_fetch_array($resultado))
							                        {
							                          	$contador = $contador + 1;
							                         	$id_tipo_gas_pev = $linea["id_tipo_gas_pev"];
							                          	$nombre_gas = $linea["nombre"];
							                          	?>
							                          	<tr class="odd gradeX">
							                            	<td width="5"><?php echo $contador; ?></td>
							                            	<td width="200"><?php echo $nombre_gas; ?></td>
							                             	<td width="70">
							                                	<a class="btn btn-info btn-block" href="parametros.php?id_tipo_gas_pev=<?php echo $id_tipo_gas_pev; ?>#GAS">Editar</a>
							                            	</td>
							                          	</tr>                             
							                        <?php
							                        }mysqli_free_result($resultado);	                                            
						                  		  ?>
												</tbody>
											</table>
										</div>
									</fieldset>	
								</form>
							</div>
						</div>
					</div>
				</article>
				<!-- NEW COL START -->
				<article class="col-sm-12 col-md-12 col-lg-6">
					<!--Sección Pensiones.-->
					<div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h6>Edición Material Cilindro PEV</h6>	
						</header>
						<div>							
							<div class="jarviswidget-editbox"></div>
							<div class="widget-body no-padding">
								<form id="checkout-form" class="smart-form" novalidate="novalidate" action="parametros.php" method="POST">
								<input type="hidden" name="id_material" id="id_material" value="<?php echo isset($_GET['id_material']) ? $_GET['id_material'] : NULL; ?>">
                 					<fieldset>
										<div class="row">
											<section class="col col-6">
												<label class="font-lg">Tipo Cilindro:</label>
												<label class="input"> <i class="icon-append fa fa-briefcase"></i>
													<input type="text" name="nombre_cilindro" placeholder="Nombre Tipo" value="<?php echo isset($_GET['nombre_cilindro']) ? $_GET['nombre_cilindro'] : NULL; ?>">
												</label>
											</section>
										</div>
										<?php
										if (in_array(6, $acc))
										{
										?>									
		  									<footer>										
		  											<input type="submit" value="GUARDAR" name="g_material_cilindro" id="g_codigo" class="btn btn-primary" />
		  									</footer>
	  									<?php
										}										
										?>
                					</fieldset>
									<fieldset>
										<div class="widget-body">											
											<table id="dt_basic" class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>#</th>
							                            <th>Tipo Material Cilindro</th>
							                            <th>Acción</th>
													</tr>
												</thead>
												<tbody>													
													<?php
							                          	$contador = "0";			                
							                         	$consulta = "SELECT * FROM material_cilindro ORDER BY id_material";
							                          	$resultado = mysqli_query($con,$consulta);
							                          	while ($linea = mysqli_fetch_array($resultado))
							                          	{
							                            	$contador = $contador + 1;
							                            	$nombre = $linea["nombre"];
							                            	$id_material = $linea["id_material"];
							                            	?>
							                            	<tr class="odd gradeX">
								                            	<td width="5"><?php echo $contador; ?></td>
								                            	<td width="200"><?php echo $nombre; ?></td>
								                             	<td width="70">
								                                	<a class="btn btn-info btn-block" href="parametros.php?id_material=<?php echo $id_material; ?>">Editar</a>
								                            	</td>
								                          	</tr>
								                          	<?php
							                          	}
							                          	mysqli_free_result($resultado);                      
							                        ?>
												</tbody>
											</table>
										</div>
									</fieldset>	
								</form>
							</div>
						</div>
					</div>
				</article>
			</div>
			<!-- END ROW -->
		</section>
		<!-- end widget grid -->

	</div>
	<!-- END MAIN CONTENT -->
<?php
}										
?>
</div>
<!-- END MAIN PANEL -->
<!-- PAGE FOOTER -->
<?php
	include("inc/footer.php");
	include("inc/scripts.php"); 
?>

<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<?php 
	include("inc/google-analytics.php"); 
}
else
{
    header("Location:index.php");
}
?>