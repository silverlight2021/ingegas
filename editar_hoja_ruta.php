<?php
session_start();
date_default_timezone_set("America/Bogota");

if ($_SESSION['logged'] == 'yes')
{
	require ("libraries/conexion.php"); 
	$id_datos_hoja_ruta = isset($_REQUEST['id_datos_hoja_ruta']) ? $_REQUEST['id_datos_hoja_ruta'] : NULL;

	$id_user=$_SESSION['su'];

	$consulta_1 = "SELECT Name, LastName
				   FROM user WHERE idUser = '".$id_user."'";
	$resulado_1 = mysqli_query($con,$consulta_1);
	if(mysqli_num_rows($resulado_1) > 0)
	{
		$linea_1 = mysqli_fetch_assoc($resulado_1);
		$conductor = $linea_1['Name']." ".$linea_1['LastName'];
	}

    $acc = $_SESSION['acc'];

	require_once("inc/init.php");
	require_once("inc/config.ui.php");
	$page_title = "Hojas-Ruta";
	$page_css[] = "your_style.css";
	include("inc/header.php");
	$page_nav["rutero"]["sub"]["hoja_ruta"]["active"] = true;
	include("inc/nav.php");


	//Botón guarda cambios de cada uno de los registros correspondientes a la hoja de ruta
	if(isset($_POST['actualizar_datos_hoja_ruta']))
	{
		$idHoja_ruta = $_POST['idHoja_ruta'];
		$id_datos_datos_ruta = $_POST['id_datos_datos_ruta'];	
		$id_cliente = $_POST['id_cliente'];
		$odometro = $_POST['odometro'];
		$num_factura = $_POST['num_factura'];
		$num_cme = $_POST['num_cme'];
		$hora_llegada = $_POST['hora_llegada'];
		$hora_salida = $_POST['hora_salida'];
		$cili_entregado = $_POST['cili_entregado'];
		$cili_recibido = $_POST['cili_recibido'];
		$termo_entregado = $_POST['termo_entregado'];
		$termo_recibido = $_POST['termo_recibido'];
		$lin_litros = $_POST['lin_litros'];
		$otros_item = $_POST['otros_item'];
		$otros_entregado = $_POST['otros_entregado'];
		$otros_recibido = $_POST['otros_recibido'];
		$dinero_valor = $_POST['dinero_valor'];
		$efectivo_cheque = $_POST['efectivo_cheque'];
		$observaciones = $_POST['observaciones'];
		$num_errores = $_POST['num_errores'];

		//Se insertan los datos correspondientes a la ruta realizada
		$consulta_2 = "UPDATE datos_hoja_ruta 
			           SET id_cliente = '".$id_cliente."',
			           	   odometro = '".$odometro."',
			           	   num_factura = '".$num_factura."',
			           	   num_cme = '".$num_cme."',
			           	   hora_llegada = '".$hora_llegada."',
			           	   hora_salida = '".$hora_salida."',
			           	   cili_entregado = '".$cili_entregado."',
			           	   cili_recibido = '".$cili_recibido."',
			           	   termo_entregado = '".$termo_entregado."',
			           	   termo_recibido = '".$termo_recibido."',
			           	   lin_litros = '".$lin_litros."',
			           	   otros_item = '".$otros_item."',
			           	   otros_entregado = '".$otros_entregado."',
			           	   otros_recibido = '".$otros_recibido."',
			           	   dinero_valor = '".$dinero_valor."',
			           	   efectivo_cheque = '".$efectivo_cheque."',
			           	   observaciones = '".$observaciones."',
			           	   num_errores = '".$num_errores."'
			           WHERE id_datos_hoja_ruta = '".$id_datos_hoja_ruta."'";
		
		if (mysqli_query($con,$consulta_2))
		{
			?>
			<script type="text/javascript">
				alert("Datos actualizados exitosamente.")
				var idHoja_ruta = '<?php echo $idHoja_ruta; ?>';
				window.location = ('http://ingegas.info/hoja_ruta.php?idHoja_ruta='+idHoja_ruta);
			</script>
			<?php
			//header("Location:http://ingegas.info/hoja_ruta.php?idHoja_ruta=$idHoja_ruta");
		}
		else
		{
			echo "Error: ".$consulta_2."<br>".mysqli_error($con);
		}
		//mysqli_close($con);
		
		
	}
	
	//SE CONSULTAN DATOS DE HOJA DE RUTA EXISTENTE
	if(isset($id_datos_hoja_ruta) > 0)
	{

		$consulta_7 = "SELECT * FROM datos_hoja_ruta
					   WHERE id_datos_hoja_ruta = '".$id_datos_hoja_ruta."'";

		$resultado_7 = mysqli_query($con,$consulta_7);

		if(mysqli_num_rows($resultado_7)> 0)
		{
			$contador = 0;
			while($linea_7 = mysqli_fetch_assoc($resultado_7))
			{
				$contador++;
				$id_datos_hoja_ruta = isset($linea_7['id_datos_hoja_ruta']) ? $linea_7['id_datos_hoja_ruta'] : NULL;
				$id_cliente = isset($linea_7['id_cliente']) ? $linea_7['id_cliente'] : NULL;
				$odometro = isset($linea_7['odometro']) ? $linea_7['odometro'] : NULL;
				$num_factura = isset($linea_7['num_factura']) ? $linea_7['num_factura'] : NULL;
				$idHoja_ruta = $linea_7['idHoja_ruta'];
				
				$num_cme = $linea_7['num_cme'];
				$hora_llegada = $linea_7['hora_llegada'];
				$hora_salida = $linea_7['hora_salida'];
				$cili_entregado = $linea_7['cili_entregado'];
				$cili_recibido = $linea_7['cili_recibido'];
				$termo_entregado = $linea_7['termo_entregado'];
				$termo_recibido = $linea_7['termo_recibido'];
				$lin_litros = $linea_7['lin_litros'];
				$otros_item = $linea_7['otros_item'];
				$otros_entregado = $linea_7['otros_entregado'];
				$otros_recibido = $linea_7['otros_recibido'];
				$dinero_valor = $linea_7['dinero_valor'];
				$efectivo_cheque = $linea_7['efectivo_cheque'];
				$observaciones= $linea_7['observaciones'];

				$consulta_8 = "SELECT nombre 
				               FROM clientes 
				               WHERE id_cliente = '".$id_cliente."'";
				$resultado_8 = mysqli_query($con,$consulta_8);
				if(mysqli_num_rows($resultado_8) > 0)
				{
					$linea_8 = mysqli_fetch_assoc($resultado_8);
					$nombre = isset($linea_8['nombre']) ? $linea_8['nombre'] : NULL;					
				}
				else
				{
					$nombre = "error";
				}
			}
		}
	}



	?>
		<!-- ==========================CONTENT STARTS HERE ========================== -->
		<!-- MAIN PANEL -->
		<div id="main" role="main">
		    
		    <!-- MAIN CONTENT -->
		    <div id="content">        
		        <!-- widget grid -->
		        <section id="widget-grid">
		            <!-- START ROW -->
		            <div class="row">
		                <!-- NEW COL START -->
		                <article class="col-md-8 col-md-offset-2"

		                	<!-- Widget ID (each widget will need unique ID)-->
		                    <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false" style="align-self: center;">
		                        <header>
		                            <h2>EDICIÓN DE RECORRIDO</h2>             
		                        </header>
		                        <!-- widget div-->
		                        <div>
		                            <!-- widget content -->
		                            <div class="widget-body no-padding">
		                            	<!--FORMULARIO PARA EL ALMACENAMIENTO DE DATOS BÁSICOS DE LA HOJA DE RUTA -->
					                	<!--FORMULARIO PARA EL ALMACENAMIENTO DE RECORRIDOS DE LA HOJA DE RUTA -->
			                            <form id="form2" class="smart-form" action="editar_hoja_ruta.php" method="POST" name="form2">
			                            	<input type="hidden" name="User_idUser_conductor" value="<?php echo isset($id_user) ? $id_user : NULL; ?>">
			                    			<input type="hidden" name="id_datos_hoja_ruta" value="<?php echo isset($id_datos_hoja_ruta) ? $id_datos_hoja_ruta : NULL; ?>">
			                    			<input type="hidden" name="salida" value="<?php echo isset($hora) ? $hora : NULL; ?>">
			                    			<input type="hidden" name="idHoja_ruta" value="<?php echo isset($idHoja_ruta) ? $idHoja_ruta : NULL; ?>">
			                    			<input type="hidden" name="id_cliente" value="<?php echo isset($id_cliente) ? $id_cliente : NULL; ?>">
			                    			
			                            	<fieldset>
			                            		<div>
			                            			<div class="row">
			                            				<div class="page-header" style="padding: 15px;">
													    	<h1 style="font-size: 40px; font-weight: bold; color: red;">Datos del Cliente</h1>      
													  	</div>
														<section class="col col-12">
															<label class="font-lg">Nombre Cliente:</label>
															<div class="input-group">
																<label class="input">
																	<input type="text" name="nombre" class="input-lg" id="nombre" placeholder="Nombre o Razón Social"  required value="<?php echo $nombre; ?>" >																
																</label>
																<span class="input-group-btn">
																	<button type="button" name="cambiar_cliente" class="btn btn-warning btn-lg" data-toggle="modal" data-target="#modal_cliente">Seleccionar Cliente</button>
																</span>
															</div>
														</section>
													</div>	                                			
								  					<div class="row">
								  						<section class="col col-4">
								  						<label class="font-lg">Número Factura:</label>
									  						<label class="input">
									  							<input type="text" name="num_factura" class="input-lg" id="num_factura" placeholder="Número de Factura" title="Ingrese el número de factura" value="<?php echo isset($num_factura) ? $num_factura : NULL; ?>" required>
									  						</label>				  							
								  						</section>
								  						<section class="col col-4">
									  					<label class="font-lg">CME:</label>
									  						<label class="input">
									  							<input type="text" class="input-lg" name="num_cme" id="num_cme" placeholder="Número CME" title="Ingrese el CME asignado" required value="<?php echo isset($num_cme) ? $num_cme : NULL; ?>" >
									  						</label>
									  					</section>				  						
								  					</div>
								  					<div class="row">
								  						<section class="col col-4">
															<label class="font-lg">Hora Llegada:</label>
															<div class="input-group">
																<label class="input"><input type="text" name="hora_llegada" class="input-lg" required value="<?php echo $hora_llegada; ?>">
																</label>
																<span class="input-group-btn">
																	<button type="button" name="registro_llegada" class="btn btn-warning btn-lg" onclick="reg_hora_llegada();">Registrar</button>
																</span>
															</div>														
														</section>
								  						<section class="col col-4">
															<label class="font-lg">Hora Salida:</label>
															<div class="input-group">
																<label class="input"><input type="text" name="hora_salida" class="input-lg" required value="<?php echo $hora_salida; ?>">
																</label>
																<span class="input-group-btn">
																	<button type="button" name="registro_salida" class="btn btn-warning btn-lg" onclick="reg_hora_salida();">Registrar</button>
																</span>
															</div>														
														</section>				  						
								  					</div>
								  					<div class="row">							  						
								  						<section class="col col-4">
									  					<label class="font-lg">Odómetro:</label>
									  						<label class="input">
									  							<input type="number" class="input-lg" name="odometro" id="odometro" placeholder="Odómetro Salida" title="Ingrese el kilometraje de salida" required value="<?php echo isset($odometro) ? $odometro : NULL; ?>" >
									  						</label>
									  					</section>				  						
								  					</div>
								  					<div class="row">
								  						<div class="page-header" style="padding: 15px;">
													    	<h1 style="font-size: 40px; font-weight: bold; color: red;">Cilindros</h1>      
													  	</div>
							  							<section class="col col-4">
								  						<label class="font-lg">Entregados:</label>
									  						<label class="input">
									  							<input type="number" name="cili_entregado" class="input-lg" id="cili_entregado" placeholder="Entregados" value="<?php echo isset($cili_entregado) ? $cili_entregado : NULL; ?>">
									  						</label>				  							
								  						</section>
								  						<section class="col col-4">
									  					<label class="font-lg">Recibidos:</label>
									  						<label class="input">
									  							<input type="number" class="input-lg" name="cili_recibido" id="cili_recibido" placeholder="Recibidos" title="Ingrese el kilometraje de salida" value="<?php echo isset($cili_recibido) ? $cili_recibido : NULL; ?>" >
									  						</label>
									  					</section>
								  					</div>
								  					<div class="row">
								  						<div class="page-header" style="padding: 15px;">
													    	<h1 style="font-size: 40px; font-weight: bold; color: red;">Termos</h1>      
													  	</div>
							  							<section class="col col-4">
								  						<label class="font-lg">Entregados:</label>
									  						<label class="input">
									  							<input type="number" name="termo_entregado" class="input-lg" id="termo_entregado" placeholder="Entregados" value="<?php echo isset($termo_entregado) ? $termo_entregado : NULL; ?>">
									  						</label>				  							
								  						</section>
								  						<section class="col col-4">
									  					<label class="font-lg">Recibidos:</label>
									  						<label class="input">
									  							<input type="number" class="input-lg" name="termo_recibido" id="termo_recibido" placeholder="Recibidos" title="Ingrese el kilometraje de salida"  value="<?php echo isset($termo_recibido) ? $termo_recibido : NULL; ?>" >
									  						</label>
									  					</section>
								  					</div>
								  					<div class="row">
								  						<div class="page-header" style="padding: 15px;">
													    	<h1 style="font-size: 40px; font-weight: bold; color: red;">LIN</h1>      
													  	</div>
							  							<section class="col col-4">
								  						<label class="font-lg">Litros:</label>
									  						<label class="input">
									  							<input type="number" name="lin_litros" class="input-lg" id="lin_litros" placeholder="Litros" value="<?php echo isset($lin_litros) ? $lin_litros : NULL; ?>">
									  						</label>				  							
								  						</section>							  						
								  					</div>
								  					<div class="row">
								  						<div class="page-header" style="padding: 15px;">
													    	<h1 style="font-size: 40px; font-weight: bold; color: red;">Otros</h1>      
													  	</div>
													  	<section class="col col-4">
								  						<label class="font-lg">Item:</label>
									  						<label class="input">
									  							<input type="text" name="otros_item" class="input-lg" id="otros_item" placeholder="Elemento" value="<?php echo isset($otros_item) ? $otros_item : NULL; ?>">
									  						</label>				  							
								  						</section>
							  							<section class="col col-4">
								  						<label class="font-lg">Entregados:</label>
									  						<label class="input">
									  							<input type="number" name="otros_entregado" class="input-lg" id="otros_entregado" placeholder="Entregados" value="<?php echo isset($otros_entregado) ? $otros_entregado : NULL; ?>">
									  						</label>				  							
								  						</section>
								  						<section class="col col-4">
									  					<label class="font-lg">Recibidos:</label>
									  						<label class="input">
									  							<input type="number" class="input-lg" name="otros_recibido" id="otros_recibido" placeholder="Recibidos" title="Ingrese el kilometraje de salida" value="<?php echo isset($otros_recibido) ? $otros_recibido : NULL; ?>" >
									  						</label>
									  					</section>
								  					</div>
								  					<div class="row">
								  						<div class="page-header" style="padding: 15px;">
													    	<h1 style="font-size: 40px; font-weight: bold; color: red;">Dinero</h1>      
													  	</div>
							  							<section class="col col-4">
								  						<label class="font-lg">Valor:</label>
									  						<label class="input">
									  							<input type="number" name="dinero_valor" class="input-lg" id="dinero_valor" placeholder="Recibido" value="<?php echo isset($dinero_valor) ? $dinero_valor : NULL; ?>">
									  						</label>				  							
								  						</section>
								  						<section class="col col-4">
									  					<label class="font-lg">Efectivo/Cheque:</label>
									  						<label class="select" class="">								  							
									  							<select name="efectivo_cheque" class="input-lg">
									  								<?php
									  								if ($efectivo_cheque == "0")
																	{
																		echo '<option value="0">Seleccione...</option>';
																		echo '<option value="Efectivo">Efectivo</option>';
																		echo '<option value="Cheque">Cheque</option>';
																	}
																	else
																	{
																		?>																		
																		<option selected value="<?php echo $efectivo_cheque; ?>"><?php echo $efectivo_cheque; ?></option>
																		<option value="Efectivo">Efectivo</option>
																		<option value="Cheque">Cheque</option>
																		<?php
																	}
									  								?>									  								
									  							</select><i></i>
									  						</label>
									  					</section>
								  					</div>
								  					<div class="row">
								  						<div class="page-header" style="padding: 15px;">
													    	<h1 style="font-size: 40px; font-weight: bold; color: red;">Errores</h1>      
													  	</div>
													  	<section class="col col-4">
								  						<label class="font-lg">Cantidad:</label>
									  						<label class="input">
									  							<input type="number" name="num_errores" class="input-lg" id="num_errores" placeholder="Elemento" value="<?php echo isset($num_errores) ? $num_errores : NULL; ?>">
									  						</label>				  							
								  						</section>
								  					</div>
								  					<div class="row">
								  						<div class="page-header" style="padding: 15px;">
													    	<h1 style="font-size: 40px; font-weight: bold; color: red;">Observaciones</h1>      
													  	</div>
								  						<section class="col col-4">
									  						<label class="input">
									  							<textarea class="form-control" rows="5" id="comment" name="observaciones"><?php echo isset($observaciones) ? $observaciones : NULL; ?></textarea>
									  						</label>
									  					</section>
								  					</div>
			                            		</div>		                            		
			                            	</fieldset>
			                                <?php
			                            	if(in_array(58, $acc))
			                            	{
			                            		?>
			                            		<footer>
			                                		<input type="submit" name="actualizar_datos_hoja_ruta" class="btn btn-primary" value="ACTUALIZAR RECORRIDO">
			                                	</footer>
			                                	<?php
			                            	}
			                                ?>
			                            </form>
			                        </div>
			                    </div>
			                </div>

		                </article>
		            </div>
		        </section>
		    </div>
		</div>
		<!-- Modal para la seleccion del cliente asociado a la Hoja de ruta actual -->
	    <div class="modal fade" id="modal_cliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">
			<form name="mezcla" method="POST" id="mezcla">
			<input type="hidden" name="id_cilindro_eto"  value="id_cilindro_eto">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							&times;
							</button>
							<h4 class="modal-title" id="myModalLabel">Seleccione el Cliente</h4>
						</div>
						<div class="modal-body">
							<div class="row">
								<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">						
									<form  name="form2" id="form2">							
										<thead>
											<tr>
												<th>#</th>
												<th>Seleccionar</th>
												<th>Cliente</th>
												<th>Nit</th>									
											</tr>
										</thead>
										<tbody>										
											<?php                                
			                        			$contador = "0";
			                        	        $consulta = "SELECT * FROM clientes ";
			                                    $resultado = mysqli_query($con,$consulta);
			                                    if(mysqli_num_rows($resultado) > 0 )
			                                    {
			                                    	while ($linea = mysqli_fetch_assoc($resultado))
				                                    {
				                                        $contador = $contador + 1;
				                                        $id_cliente = $linea["id_cliente"];
				                                        $nombre = $linea["nombre"];				                                        
				                                        $nit = $linea["nit"];
				                                    ?>
				                                        <tr>                                                	
				                                            <td width="5"><?php echo $contador; ?></td>
				                                            <td align="center"><a href="#" onclick="g_cliente('<?php echo $id_cliente; ?>','<?php echo $nombre; ?>')"><img src="img/point-at.png" width="20" height="20"></a></td>
				                                            <td><strong><?php echo $nombre; ?></strong></td>
				                                            <td><strong><?php echo $nit; ?></strong></td>
				                                    	</tr>   
				                                    <?php
				                                    }mysqli_free_result($resultado); 
			                                    }			                                    
			                                ?>
										</tbody>
									</form>
								</table>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">	Cancel</button>	
						</div>
					</div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			</form>
		</div>	
		<!-- Fin Modal -->
		<script type="text/javascript">
			function g_cliente(id_cliente,nombre_cliente) 
			{
				$('#modal_cliente').modal('hide');
				//$('input:hidden[name=id_cliente]').val(num_cili1);
				$("input[name=id_cliente]").val(id_cliente);
				$("input[name=nombre]").val(nombre_cliente);		
			}
		</script>
		<?php
		include("inc/footer.php");
		include("inc/scripts.php"); 

}
else
{
    header("Location:index.php");
}
?>

?>