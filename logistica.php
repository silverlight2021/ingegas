<?php
date_default_timezone_set("America/Bogota");
require ("libraries/conexion.php");
require ("libraries/seguimiento.php");
session_start();

if(@$_SESSION['logged']== 'yes')
{ 
  	$id_logistica_eto = isset($_REQUEST['id_logistica_eto']) ? $_REQUEST['id_logistica_eto'] : NULL;
    $User_idUser =$_SESSION['su'];
    $acc = $_SESSION['acc'];
    $time = time();
    $user_agent = $_SERVER['HTTP_USER_AGENT'];
    function getPlatform($user_agent) {
   	$plataformas = array(
    	'Windows 10' => 'Windows NT 10.0+',
      	'Windows 8.1' => 'Windows NT 6.3+',
      	'Windows 8' => 'Windows NT 6.2+',
      	'Windows 7' => 'Windows NT 6.1+',
      	'Windows Vista' => 'Windows NT 6.0+',
      	'Windows XP' => 'Windows NT 5.1+',
      	'Windows 2003' => 'Windows NT 5.2+',
      	'Windows' => 'Windows otros',
      	'iPhone' => 'iPhone',
      	'iPad' => 'iPad',
      	'Mac OS X' => '(Mac OS X+)|(CFNetwork+)',
      	'Mac otros' => 'Macintosh',
      	'Android' => 'Android',
      	'BlackBerry' => 'BlackBerry',
      	'Linux' => 'Linux',
   	);
	   foreach($plataformas as $plataforma=>$pattern){
	      if (preg_match('/'.$pattern.'/', $user_agent))
	         return $plataforma;
	   }
	   return 'Otras';
	}

	$SO = getPlatform($user_agent);
  
	if(isset($_GET['id_cliente']))
	{
		$id_cliente1 = isset($_REQUEST['id_cliente']) ? $_REQUEST['id_cliente'] : NULL;   

	    if(strlen($id_cliente1) > 0)
	    {
		    $consulta  = "INSERT INTO logistica_eto
		          (id_cliente,fecha,id_estado,id_finalizar) 
		          VALUES ('".$id_cliente1."', '".date("Y/m/d")."','1','1')";
		    $resultado = mysqli_query($con,$consulta) ;
		    if ($resultado == FALSE)
		    {
		      echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    }
		    else
		    {
		      $id_logistica_eto1 = mysqli_insert_id($con);
		      header('Location: logistica.php?id_logistica_eto='.$id_logistica_eto1);
		    }
		}
		else
		{
			header('Location: logistica.php');
		}
	} 
	if(isset($_POST['guardar_logistica']))
	{
	    $num_logistica1 = $_POST['num_logistica'] ; 
	    $cme1 = $_POST['cme'] ; 
	    if(strlen($id_logistica_eto) > 0)
		{
		    $consulta= "UPDATE logistica_eto SET 
		                num_logistica = '$num_logistica1' , cme = '$cme1'     
		                WHERE id_logistica_eto = $id_logistica_eto ";
		    $resultado = mysqli_query($con,$consulta) ;

		    if ($resultado == FALSE)
		    {
		      echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    }
		    else
		    {
		    	header('Location: busqueda_logistica.php');
		    }
	    }
	  	else
	  	{
		    $consulta = "INSERT INTO movimiento
		          		(num_logistica) 
		          		VALUES ('".$num_logistica1."')";
		    $resultado = mysqli_query($con,$consulta) ;
		    if ($resultado == FALSE)
		    {
		      echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    }
		    else
		    {
		      $id_logistica_eto = mysqli_insert_id($con);
		      
		    }
	  	}
	}
	if(isset($_POST['num_cili']))
	{
		$contador=0;
		$contador2=0;
		$contadort = 0;
	    $num_cili_eto = $_POST['num_cili'] ;

	    $consulta  = "SELECT * FROM cilindro_eto WHERE  id_estado = 3 AND num_cili_eto like '".$num_cili_eto."' ";
	  	$resultado = mysqli_query($con,$consulta) ;
	  	$linea = mysqli_fetch_array($resultado);	
		if ($linea > 0)
	    {
	      	$id_cilindro_eto = isset($linea["id_cilindro_eto"]) ? $linea["id_cilindro_eto"] : NULL;
			$consulta = "INSERT INTO has_logistica_cilindro
		          		(id_logistica_eto,id_cilindro_eto) 
		          		VALUES ('".$id_logistica_eto."','".$id_cilindro_eto."')";
		    $resultado = mysqli_query($con,$consulta) ;
		    if ($resultado == FALSE)
		    {
		      echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    }
		    else
		    {
		    	$id_has_logistica_cilindro = mysqli_insert_id($con);
		    	seguimiento(7,$id_cilindro_eto,$User_idUser,5,$id_has_logistica_cilindro);//id_seguimiento,id_cilindro_eto,id_user,id_proceso,id_has_proceso
	    		$consulta  = "SELECT * FROM has_logistica_cilindro WHERE id_logistica_eto= $id_logistica_eto";
			  	$resultado = mysqli_query($con,$consulta) ;
			  	while ($linea = mysqli_fetch_array($resultado))
		        {	
		        	$id_cilindro_eto = $linea["id_cilindro_eto"];
		        	
		        	$consulta5  = "SELECT * FROM cilindro_eto WHERE id_cilindro_eto= $id_cilindro_eto";
				  	$resultado5 = mysqli_query($con,$consulta5) ;
				  	while ($linea5 = mysqli_fetch_array($resultado5))
		            {
		            	$propiedad_eto = $linea5["propiedad_eto"]; 	
		            	if ($propiedad_eto==1) 
		            	{
		            		$contador = $contador+1;
		            	}
		            	if ($propiedad_eto==2) 
		            	{
		            		$contador2 = $contador2+1;
		            	}
		            }  
		        }
		        $consulta7= "UPDATE logistica_eto SET 
			                total_cilindros_cliente = '$contador' ,
			                total_cilindros_ingegas = '$contador2'               
			                WHERE id_logistica_eto = $id_logistica_eto ";
			    $resultado7 = mysqli_query($con,$consulta7) ;

			    if ($resultado7 == FALSE)
			    {
			      echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
			    }
			    else
			    {
			    	$consulta7= "UPDATE cilindro_eto SET 
			                id_estado = '4'             
			                WHERE id_cilindro_eto = $id_cilindro_eto ";
				    $resultado7 = mysqli_query($con,$consulta7) ;

				    if ($resultado7 == FALSE)
				    {
				      echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
				    }
				    else
				    {
				    	header('Location: logistica.php?id_logistica_eto='.$id_logistica_eto);
				    }	
			    }
			} 
	    }
	    else
	    {
	    	?>
			  	<script type="text/javascript">
			  		alert("Cilindro No disponible")
			  	</script>
			<?php 
	    }  
	}

	if(isset($_POST['g_cilindro_movimiento']))
	{
		$serial2 = $_POST['serial'] ;
		$id_codigo_producto2=$_POST['id_codigo_producto1'] ;
		$id_tipo_envace2=$_POST['id_tipo_envace1'] ;
		$id_propiedad_cilindro2=$_POST['id_propiedad_cilindro1'] ;
		$id_tapa2=$_POST['id_tapa2'] ;

	    $consulta = "INSERT INTO cilindros
	          		(serial_cilindro,id_codigo_producto, id_tipo_envace, id_propiedad_cilindro) 
	          		VALUES ('".$serial2."','".$id_codigo_producto2."', '".$id_tipo_envace2."', '".$id_propiedad_cilindro2."')";
	    $resultado = mysqli_query($con,$consulta) ;
	    if ($resultado == FALSE)
	    {
	      echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
	    }
	    else
	    {
	      	$id_cilindro22 = mysqli_insert_id($con); 

	    	$consulta  = "SELECT * FROM has_movimiento_cilindro WHERE id_cilindro= $id_cilindro22 AND id_logistica_eto= $id_logistica_eto ";
	  		$resultado = mysqli_query($con,$consulta) ;
	  		$linea = mysqli_fetch_array($resultado);

			$id_cilindro2 = isset($linea["id_cilindro"]) ? $linea["id_cilindro"] : NULL;
			if ($id_cilindro22 ==$id_cilindro2) 
		    {
		      	?>
		      	<script type="text/javascript">
		         	alert("Ya existe este Cilindro ");
		     	</script>
		     	<?php
		    }
		    else
		    {    
			    $consulta = "INSERT INTO has_movimiento_cilindro
			          		(id_logistica_eto,id_cilindro, id_tapa) 
			          		VALUES ('".$id_logistica_eto."','".$id_cilindro22."', '".$id_tapa2."')";
			    $resultado = mysqli_query($con,$consulta) ;
			    if ($resultado == FALSE)
			    {
			      echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
			    }
			    else
			    {	
					$consulta  = "SELECT * FROM has_movimiento_cilindro WHERE id_logistica_eto= $id_logistica_eto";
				  	$resultado = mysqli_query($con,$consulta) ;
				  	while ($linea = mysqli_fetch_array($resultado))
			        {	
			        	$id_cilindro = $linea["id_cilindro"];
			        	$id_tapa = $linea["id_tapa"]; 	
			        	if ($id_tapa==1) 
			        	{
			        		$contadort = $contadort+1;
			        	} 
			        	$consulta5  = "SELECT * FROM cilindros WHERE id_cilindro= $id_cilindro";
					  	$resultado5 = mysqli_query($con,$consulta5) ;
					  	while ($linea5 = mysqli_fetch_array($resultado5))
			            {
			            	$id_propiedad_cilindro = $linea5["id_propiedad_cilindro"]; 	
			            	if ($id_propiedad_cilindro==1) 
			            	{
			            		$contador = $contador+1;
			            	}
			            	if ($id_propiedad_cilindro==2) 
			            	{
			            		$contador2 = $contador2+1;
			            	}
			            }  
			        }
			        $consulta7= "UPDATE movimiento SET 
				                total_cilindros_cliente = '$contador' ,
				                total_cilindros_ingegas = '$contador2' , 
				                total_tapas = '$contadort'                
				                WHERE id_logistica_eto = $id_logistica_eto ";
				    $resultado7 = mysqli_query($con,$consulta7) ;

				    if ($resultado7 == FALSE)
				    {
				      echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
				    }
				    else
				    {
				    	header('Location: logistica.php?id_logistica_eto='.$id_logistica_eto);
				    }		    
				}	      
		    }
	    }   
	}

	if(strlen($id_logistica_eto) > 0)
	{ 
	  	$consulta  = "SELECT * FROM logistica_eto WHERE id_logistica_eto= $id_logistica_eto";
	  	$resultado = mysqli_query($con,$consulta) ;
	  	$linea = mysqli_fetch_array($resultado);
		$id_cliente1 = isset($linea["id_cliente"]) ? $linea["id_cliente"] : NULL;  
		$fecha = isset($linea["fecha"]) ? $linea["fecha"] : NULL;
		$num_logistica = isset($linea["num_logistica"]) ? $linea["num_logistica"] : NULL;
		$cme = isset($linea["cme"]) ? $linea["cme"] : NULL;
		$id_finalizar = isset($linea["id_finalizar"]) ? $linea["id_finalizar"] : NULL;		

		//if ($num_logistica = "" || $cme = "") 
	  	//{
	  		//$id_finalizar=0;
	  	//}
	    mysqli_free_result($resultado);

	    $consulta1  = "SELECT id_cliente,nombre,nit FROM clientes WHERE id_cliente= $id_cliente1";
	  	$resultado1 = mysqli_query($con,$consulta1) ;
	  	$linea1 = mysqli_fetch_array($resultado1);
	  	$nombre = isset($linea1["nombre"]) ? $linea1["nombre"] : NULL; 
	  	$nit1 = isset($linea1["nit"]) ? $linea1["nit"] : NULL; 
	  	mysqli_free_result($resultado1);

	  	
	}

	require_once("inc/init.php");
	require_once("inc/config.ui.php");
	$page_title = "Logística";
	$page_css[] = "your_style.css";
	include("inc/header.php");
	$page_nav[""][""][""][""] = true;
	include("inc/nav.php");
?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">	
	<div id="content">
		<?php
		if ($id_logistica_eto=="") 
		{		
		?>
		<div id="content">	    
			<div class="row">		
				<div class="row" align="center"></div>
			    <div class="">
					<h1  class="page-title txt-color-blueDark"> Seleccione el Cliente: 				                  
					</h1>
				</div>
			</div>
			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">			
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>Clientes</h2>
				</header>	
				<div>
					<div class="jarviswidget-editbox"></div>
					<div class="widget-body no-padding">							
						<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
							<form action="logistica.php" name="form1">						
								<thead>
									<tr>
										<th>#</th>
										<th>Acción</th>
										<th>Cliente</th>
										<th>Nit</th>
										<th>Telefono</th>
										<th>Celular</th>
										<th>E-Mail</th>
										<th>Direccion</th>
										<th>Observacion</th>
									</tr>
								</thead>
								<tbody>										
									<?php
                                    
                            			$contador2 = "0";
                            	        $consulta = "SELECT * FROM clientes ";
                                        $resultado = mysqli_query($con,$consulta) ;
                                        while ($linea = mysqli_fetch_array($resultado))
                                        {
                                            $contador2 = $contador2 + 1;
                                            $id_cliente = $linea["id_cliente"];
                                            $nombre = $linea["nombre"];
                                            $telefono_fijo = $linea["telefono_fijo"];
                                            $telefono_celular = $linea["telefono_celular"];
                                            $correo = $linea["correo"];
                                            $persona_contacto = $linea["persona_contacto"];
                                            $direccion = $linea["direccion"];
                                            $observacion = $linea["observacion"];
                                            $nit = $linea["nit"];
                                 
                                        ?>
                                            <tr>	                                                	
                                                <td width="5"><?php echo $contador2; ?></td>
                                                <td><a href="logistica.php?id_cliente=<?php echo $id_cliente; ?>"><img src="img/chulo.png" width="20" height="20"></a></td>
                                                <td><?php echo $nombre; ?></td>
                                                <td><?php echo $nit; ?></td>
                                                <td><?php echo $telefono_fijo; ?></td>
                                                <td><?php echo $telefono_celular; ?></td>
                                                <td><?php echo $correo; ?></td>
                                                <td><?php echo $direccion; ?></td>
                                                <td><?php echo $observacion; ?></td>
                                        	</tr>   
                                        <?php
                                        }
                                        mysqli_free_result($resultado); 	                                        	
                                    ?>
								</tbody>
							</form>
						</table>
					</div>
				</div>
			</div>
		</div>
		<?php
		}
		if ($id_logistica_eto>0) 
		{		
		?>
		<section id="widget-grid" class="">
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-6">
					<div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h2>Datos Basicos </h2>				
						</header>
						<div>				
						<div class="jarviswidget-editbox"></div>
							<div class="widget-body no-padding">
							<form id="checkout-form" class="smart-form" novalidate="novalidate" action="logistica.php" method="POST">						
							<input type="hidden" name="id_logistica_eto" id="id_logistica_eto" value="<?php echo $id_logistica_eto; ?>">
								<fieldset>
									<div class="row">
										<section class="col col-6">
											<label class="font-lg">Cliente :</label>
											<label class="input"> 
												<input type="text" class="input-lg" name="nombre" readonly placeholder="Nombre" value="<?php echo isset($nombre) ? $nombre : NULL; ?>">
											</label>
										</section>
										<section class="col col-6">
											<label class="font-lg">Fecha Asignación:</label>
											<label class="input"> 
												<input type="date" class="input-lg" name="fecha" readonly placeholder="Fecha" value="<?php echo isset($fecha) ? $fecha : NULL; ?>">
											</label>
										</section>																						
									</div>
									<div class="row">
										<section class="col col-6">
											<label class="font-lg">CME :</label>
											<label class="input"> 
												<input type="text" class="input-lg" name="cme" placeholder="CME" required title="Dato Incompleto" value="<?php echo isset($cme) ? $cme : NULL; ?>">
											</label>
										</section>
										<section class="col col-6">
											<label class="font-lg">Num Factura :</label>
											<label class="input"> 
												<input type="text" class="input-lg" name="num_logistica" placeholder="Numero Factura" value="<?php echo isset($num_logistica) ? $num_logistica : NULL; ?>">
											</label>
										</section>																														
									</div>
								</fieldset>	
								<footer>
									<?php
									if($SO=="Android")
									{	
									?>
									<a href="com.fidelier.printfromweb://
									$biguhw$INGEGAS-INGENIERIA Y GASES LTDA$intro$
									$small$------------------------------------------$intro$
									$small$Fecha   : <?echo date("d-m-Y (H:i:s)", $time); ?>$intro$
									$small$Cliente : <? echo $nombre;?>$intro$
									$small$Nit     : <? echo $nit1;?>$intro$
									$small$CME     : <? echo $cme;?>$intro$
									$small$Usuario : <?=@$_SESSION['name']?>$intro$
									$small$------------------------------------------$intro$
									$big$#  Cilindro   Tipo   Kg$intro$
									$small$------------------------------------------$intro$
									$small$<?php
									          	$contador = "0";	                
									          	$consulta = "SELECT * FROM has_logistica_cilindro WHERE id_logistica_eto= $id_logistica_eto  ORDER BY id_cilindro_eto";
									          	$resultado = mysqli_query($con,$consulta) ;
									          	$aux="";
									          	while ($linea = mysqli_fetch_array($resultado))
									          	{
										            $contador = $contador + 1;
										            $id_cilindro_eto = $linea["id_cilindro_eto"];		                           
										            $id_has_logistica_eto = $linea["id_has_logistica_eto"];		                            

										            $consulta1 = "SELECT * FROM cilindro_eto WHERE id_cilindro_eto = $id_cilindro_eto";
										            $resultado1 = mysqli_query($con,$consulta1) ;
										            while ($linea1 = mysqli_fetch_array($resultado1))
										            {
										            	$num_cili_eto = $linea1["num_cili_eto"];
										            	$id_tipo_cilindro = $linea1["id_tipo_cilindro"];
										            	$id_tipo_envace = $linea1["id_tipo_envace"];
										            	$consulta3 = "SELECT * FROM tipo_cilindro WHERE id_tipo_cilindro =".$id_tipo_cilindro;
					                                    $resultado3 = mysqli_query($con,$consulta3) ;
					                                    while ($linea3 = mysqli_fetch_array($resultado3))
					                                    {
					                                    	$tipo_cili = $linea3["tipo_cili"];	                                    	
					                                    }mysqli_free_result($resultado3);

					                                    $consulta4 = "SELECT * FROM tipo_envace WHERE id_tipo_envace =".$id_tipo_envace;
					                                    $resultado4 = mysqli_query($con,$consulta4) ;
					                                    while ($linea4 = mysqli_fetch_array($resultado4))
					                                    {
					                                    	$tipo = $linea4["tipo"];
					                                    }mysqli_free_result($resultado4);
										            }	                            
										            echo $contador.")  ".$num_cili_eto."   ".$tipo_cili."   ".$tipo;?>$intro$$small$<?php
												}mysqli_free_result($resultado);	                                            
											?>$intro$
											$small$------------------------------------------$intro$
											$small$Aprobado Por$intro$
											$intro$
											$intro$
											$intro$
											$small$------------------------------------------$intro$
											$small$Sujeto Auditoria Y Verificacion $intro$
											$cut$$intro$$intro$$intro$"><img src="/img/iconos/printer_black.png"></a>
									<?php
									}
									if ($SO!="Android") 
									{
										?>
											<a href="javascript:imprSelec('muestra')"><img src="/img/iconos/printer_blue.png"></a>
										<?php
									}	
									?>	
									<?php
									if (in_array(27, $acc))
									{
									?>							
									<input type="submit" value="Guardar" name="guardar_logistica" id="guardar_logistica" class="btn btn-primary" />
									<?php
									}
									?>
								</footer>	
							</form>	
							</div>						
						</div>					
					</div>				
				</article>
				<?php				
				if ($id_finalizar==1) 
				{			
				?>
				<article class="col-sm-12 col-md-12 col-lg-6">
					<div class="jarviswidget" id="wid-id-2" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h2>Asignación de Cilindros </h2>				
						</header>
						<div>				
							<div class="jarviswidget-editbox"></div>
							<div class="widget-body no-padding">
								<form id="checkout-form-2" class="smart-form" novalidate="novalidate" action="logistica.php" method="POST">
									<input type="hidden" name="id_logistica_eto" id="id_logistica_eto" value="<?php echo $id_logistica_eto; ?>">
									<fieldset>
										<div class="row">
											<section class="col col-6">
												<label class="font-lg">Cilindro :</label>
												<label class="input"> 
													<input type="text" class="input-lg" class="form-control" name="num_cili" placeholder="Numero Cilindro" value="<?php echo isset($num_cili) ? $num_cili : NULL; ?>" id="cilindro">
												</label>
											</section>																		
										</div>
									</fieldset>								
								<footer>											
									<?php				
									if ($id_finalizar==1) 
									{			
										?>
											<input type="button" onclick="validar()" value="Cerrar Movimiento" class="btn btn-danger btn-lg" >
										<?php				
									}											
									?>	
								</footer>			
								</form>
							</div>						
						</div>					
					</div>				
				</article>
				<?php				
				}											
				?>
				<article class="col-sm-12 col-md-12 col-lg-6">
					<div class="jarviswidget" id="wid-id-3" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h2>Cilindros Asignados </h2>				
						</header>
						<div>				
							<div class="jarviswidget-editbox"></div>																	
							<table class="table table-bordered">
								<thead>
									<tr>
										<th>#</th>                                                
			                            <th>Cilindro</th>
			                            <th>Tipo Gas</th>
			                            <?php				
										if ($id_finalizar==1) 
										{			
										?>
			                            <th>Acción</th>
			                            <?php				
										}			
										?>
									</tr>
								</thead>
								<tbody>													
								  <?php
		                          $contador1 = "0";	                
		                          $consulta = "SELECT * FROM has_logistica_cilindro WHERE id_logistica_eto= $id_logistica_eto  ORDER BY id_cilindro_eto";
		                          $resultado = mysqli_query($con,$consulta) ;
		                          while ($linea = mysqli_fetch_array($resultado))
		                          {
		                            $contador1 = $contador1 + 1;
		                            $id_cilindro_eto = $linea["id_cilindro_eto"];		                           
		                            $id_has_logistica_eto = $linea["id_has_logistica_eto"];		                            

		                            $consulta1 = "SELECT * FROM cilindro_eto WHERE id_cilindro_eto = $id_cilindro_eto";
		                            $resultado1 = mysqli_query($con,$consulta1) ;
		                            while ($linea1 = mysqli_fetch_array($resultado1))
		                            {
		                            	$num_cili_eto = $linea1["num_cili_eto"];
		                            	$id_tipo_cilindro = $linea1["id_tipo_cilindro"];

		                            	$consulta3 = "SELECT * FROM tipo_cilindro WHERE id_tipo_cilindro =".$id_tipo_cilindro;
	                                    $resultado3 = mysqli_query($con,$consulta3) ;
	                                    while ($linea3 = mysqli_fetch_array($resultado3))
	                                    {
	                                    	$tipo_cili = $linea3["tipo_cili"];	                                    	
	                                    }mysqli_free_result($resultado3);

		                            }
		                            ?>
		                            <tr class="odd gradeX">
		                              <td width="40"><?php echo $contador1; ?></td>                                  
		                              <td><?php echo $num_cili_eto; ?></td>
		                              <td><?php echo $tipo_cili; ?></td>
		                              <?php				
										if ($id_finalizar==1) 
										{			
										?>
		                              <td width="100" style="vertical-align:bottom;">
			                            <a href="eliminar_cili_logis.php?id_has_logistica_eto=<?php echo $id_has_logistica_eto; ?>&id_cilindro_eto=<?php echo $id_cilindro_eto; ?>&id_logistica_eto=<?php echo $id_logistica_eto; ?>"><input type="image"  src="img/eliminar.png" width="35" height="35"></a>			                              
			                              	</td>
			                            <?php
		                            }	                                            
		                  		    ?>  	

		                            </tr>                           
		                            <?php
		                            }mysqli_free_result($resultado);	                                            
		                  		    ?>
								</tbody>
							</table>												
						</div>					
					</div>				
				</article>				
				<div id="muestra" style="display:none">
					<p>INGEGAS-INGENIERIA Y GASES LTDA</p>
					<p>Fecha :<?echo date("d-m-Y (H:i:s)", $time); ?></p>
					<p>Cliente :<? echo $nombre;?></p>
					<p>Nit :<? echo $nit1;?></p>
					<p>CME :<? echo $cme;?></p>
					<p>Usuario: <?=@$_SESSION['name']?></p>
					<table>
						<thead>
							<tr>
								<th align="left">#</th>                                                
	                            <th>Cilindro</th>
	                            <th>Tipo Gas</th>
								<th>(Kg)</th>
							</tr>
						</thead>
						<tbody>													
						  <?php
	                      $contador = "0";	                
	                      $consulta = "SELECT * FROM has_logistica_cilindro WHERE id_logistica_eto= $id_logistica_eto  ORDER BY id_cilindro_eto";
	                      $resultado = mysqli_query($con,$consulta) ;
	                      $aux="";
	                      while ($linea = mysqli_fetch_array($resultado))
	                      {
	                        $contador = $contador + 1;
	                        $id_cilindro_eto = $linea["id_cilindro_eto"];		                           
	                        $id_has_logistica_eto = $linea["id_has_logistica_eto"];		                            

	                        $consulta1 = "SELECT * FROM cilindro_eto WHERE id_cilindro_eto = $id_cilindro_eto";
	                        $resultado1 = mysqli_query($con,$consulta1) ;
	                        while ($linea1 = mysqli_fetch_array($resultado1))
	                        {
	                        	$num_cili_eto = $linea1["num_cili_eto"];
	                        	$id_tipo_cilindro = $linea1["id_tipo_cilindro"];
				            	$id_tipo_envace = $linea1["id_tipo_envace"];
				            	$consulta3 = "SELECT * FROM tipo_cilindro WHERE id_tipo_cilindro =".$id_tipo_cilindro;
                                $resultado3 = mysqli_query($con,$consulta3) ;
                                while ($linea3 = mysqli_fetch_array($resultado3))
                                {
                                	$tipo_cili = $linea3["tipo_cili"];	                                    	
                                }mysqli_free_result($resultado3);

                                $consulta4 = "SELECT * FROM tipo_envace WHERE id_tipo_envace =".$id_tipo_envace;
                                $resultado4 = mysqli_query($con,$consulta4) ;
                                while ($linea4 = mysqli_fetch_array($resultado4))
                                {
                                	$tipo = $linea4["tipo"];
                                }mysqli_free_result($resultado4);
	                        }	                            
	                         	?>
					            <tr class="odd gradeX">
					              	<td width="40"><?php echo $contador; ?></td>                                  
					              	<td><?php echo $num_cili_eto; ?></td> 
					              	<td><?php echo $tipo_cili; ?></td> 
					              	<td><?php echo $tipo; ?></td> 	                      
					           	</tr>                           
					            <?php
	                        }mysqli_free_result($resultado);	                                            
	              		    ?>
						</tbody>
					</table>
					<br>
					<br>
					<br>
					<br>
					<p>Sujeto Auditoria Y Verificacion</p>
				</div>
				<?php
				}
				?>
			</div>
		</section>
	</div>
</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->
<!-- PAGE FOOTER -->
<?php
	include("inc/footer.php");
	include("inc/scripts.php"); 
?>
<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/jquery-form/jquery-form.min.js"></script>
<script type="text/javascript">
function imprSelec(muestra)
{var ficha=document.getElementById(muestra);var ventimp=window.open(' ','popimpr');ventimp.document.write(ficha.innerHTML);ventimp.document.close();ventimp.print();ventimp.close();}
</script>
<script type="text/javascript">
$(document).ready(function() {
	jQuery("#myBtn").click(function(){
    	var dato;	
		$("#myModal").modal();
	});
})
</script>
<script type="text/javascript">
function validar()
{ 
	var r = confirm("¿Esta seguro de finalizar el movimiento?");
	if (r == true) 
	{
		$(document).ready(function()
        {
			var formulario = $("#checkout-form").serialize();        
        	$.ajax({
    		url: 'finalizar_logistica.php',
	       	type: 'POST',
	       	data: formulario,
	       	})
    		.done(function(resp){
    		  	alert("Guardado Correctamente...!");
    		  	window.location="busqueda_logistica.php";
		    })

        });
	} 
}
</script>
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<script type="text/javascript">
// DO NOT REMOVE : GLOBAL FUNCTIONS!
$(document).ready(function() {	
	/* // DOM Position key index //		
	l - Length changing (dropdown)
	f - Filtering input (search)
	t - The Table! (datatable)
	i - Information (records)
	p - Pagination (paging)
	r - pRocessing 
	< and > - div elements
	<"#id" and > - div with an id
	<"class" and > - div with a class
	<"#id.class" and > - div with an id and class
	
	Also see: http://legacy.datatables.net/usage/features
	*/	

	/* BASIC ;*/
		var responsiveHelper_dt_basic = undefined;
		var responsiveHelper_datatable_fixed_column = undefined;
		var responsiveHelper_datatable_col_reorder = undefined;
		var responsiveHelper_datatable_tabletools = undefined;
		
		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};

		$('#dt_basic').dataTable({
			"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
			"autoWidth" : true,
			"preDrawCallback" : function() {
				// Initialize the responsive datatables helper once.
				if (!responsiveHelper_dt_basic) {
					responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
				}
			},
			"rowCallback" : function(nRow) {
				responsiveHelper_dt_basic.createExpandIcon(nRow);
			},
			"drawCallback" : function(oSettings) {
				responsiveHelper_dt_basic.respond();
			}
		});

	/* END BASIC */
	
	/* COLUMN FILTER  */
    var otable = $('#datatable_fixed_column').DataTable({
    	//"bFilter": false,
    	//"bInfo": false,
    	//"bLengthChange": false
    	//"bAutoWidth": false,
    	//"bPaginate": false,
    	//"bStateSave": true // saves sort state using localStorage
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_fixed_column) {
				responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_fixed_column.respond();
		}		
	
    });
    
    // custom toolbar
    $("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');
    	   
    // Apply the filter
    $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {
    	
        otable
            .column( $(this).parent().index()+':visible' )
            .search( this.value )
            .draw();
            
    } );
    /* END COLUMN FILTER */   

	/* COLUMN SHOW - HIDE */
	$('#datatable_col_reorder').dataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_col_reorder) {
				responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_col_reorder.respond();
		}			
	});
	
	/* END COLUMN SHOW - HIDE */

	/* TABLETOOLS */
	$('#datatable_tabletools').dataTable({
		
		// Tabletools options: 
		//   https://datatables.net/extensions/tabletools/button_options
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        "oTableTools": {
        	 "aButtons": [
             "copy",
             "csv",
             "xls",
                {
                    "sExtends": "pdf",
                    "sTitle": "SmartAdmin_PDF",
                    "sPdfMessage": "SmartAdmin PDF Export",
                    "sPdfSize": "letter"
                },
             	{
                	"sExtends": "print",
                	"sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>"
            	}
             ],
            "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
        },
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_tabletools) {
				responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_tabletools.respond();
		}
	});
	
	/* END TABLETOOLS */

})
</script>
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script type="text/javascript">		
		$(document).ready(function() {	
			// menu
			$("#menu").menu();
			/*
			 * AUTO COMPLETE AJAX
			 */
		
			function log(message) {
				$("<div>").text(message).prependTo("#log");
				$("#log").scrollTop(0);
			}
		
			$("#city").autocomplete({
				source : function(request, response) {
					$.ajax({
						url : "http://ws.geonames.org/searchJSON",
						dataType : "jsonp",
						data : {
							featureClass : "P",
							style : "full",
							maxRows : 12,
							name_startsWith : request.term
						},
						success : function(data) {
							response($.map(data.geonames, function(item) {
								return {
									label : item.name + (item.adminName1 ? ", " + item.adminName1 : "") + ", " + item.countryName,
									value : item.name
								}
							}));
						}
					});
				},
				minLength : 2,
				select : function(event, ui) {
					log(ui.item ? "Selected: " + ui.item.label : "Nothing selected, input was " + this.value);
				}
			});
		
			/*
			 * Spinners
			 */
			$("#spinner").spinner();
			$("#spinner-decimal").spinner({
				step : 0.01,
				numberFormat : "n"
			});
		
			$("#spinner-currency").spinner({
				min : 5,
				max : 2500,
				step : 25,
				start : 1000,
				numberFormat : "C"
			});
		
			/*
			 * CONVERT DIALOG TITLE TO HTML
			 * REF: http://stackoverflow.com/questions/14488774/using-html-in-a-dialogs-title-in-jquery-ui-1-10
			 */
			$.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
				_title : function(title) {
					if (!this.options.title) {
						title.html("&#160;");
					} else {
						title.html(this.options.title);
					}
				}
			}));
		
		
			/*
			* DIALOG SIMPLE
			*/
		
			// Dialog click
			$('#dialog_link').click(function() {
				$('#dialog_simple').dialog('open');
				return false;
		
			});
		
			$('#dialog_simple').dialog({
				autoOpen : false,
				width : 600,
				resizable : false,
				modal : true,
				title : "<div class='widget-header'><h4><i class='fa fa-warning'></i> Empty the recycle bin?</h4></div>",
				buttons : [{
					html : "<i class='fa fa-trash-o'></i>&nbsp; Delete all items",
					"class" : "btn btn-danger",
					click : function() {
						$(this).dialog("close");
					}
				}, {
					html : "<i class='fa fa-times'></i>&nbsp; Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
					}
				}]
			});
		
			/*
			* DIALOG HEADER ICON
			*/
		
			// Modal Link
			$('#modal_link').click(function() {
				$('#dialog-message').dialog('open');
				return false;
			});
		
			$("#dialog-message").dialog({
				autoOpen : false,
				modal : true,
				title : "<div class='widget-header'><h4><i class='icon-ok'></i> jQuery UI Dialog</h4></div>",
				buttons : [{
					html : "Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
					}
				}]
		
			});
		
			/*
			 * Remove focus from buttons
			 */
			$('.ui-dialog :button').blur();
		
			/*
			 * Just Tabs
			 */
		
			$('#tabs').tabs();
		
			/*
			 *  Simple tabs adding and removing
			 */
		
			$('#tabs2').tabs();
		
			// Dynamic tabs
			var tabTitle = $("#tab_title"), tabContent = $("#tab_content"), tabTemplate = "<li style='position:relative;'> <span class='air air-top-left delete-tab' style='top:7px; left:7px;'><button class='btn btn-xs font-xs btn-default hover-transparent'><i class='fa fa-times'></i></button></span></span><a href='#{href}'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; #{label}</a></li>", tabCounter = 2;
		
			var tabs = $("#tabs2").tabs();
		
			// modal dialog init: custom buttons and a "close" callback reseting the form inside
			var dialog = $("#addtab").dialog({
				autoOpen : false,
				width : 600,
				resizable : false,
				modal : true,
				buttons : [{
					html : "<i class='fa fa-times'></i>&nbsp; Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
		
					}
				}]
			});
		
			// addTab form: calls addTab function on submit and closes the dialog
			var form = dialog.find("form").submit(function(event) {
				addTab();
				dialog.dialog("close");
				
			});
		
			// actual addTab function: adds new tab using the input from the form above

			
			function addTab() 
			{
				
				var label = tabTitle.val() || "Tab " + tabCounter, id = "tabs-" + tabCounter, li = $(tabTemplate.replace(/#\{href\}/g, "#" + id).replace(/#\{label\}/g, label)), tabContentHtml = tabContent.val() || "Tab " + tabCounter + " content.";
		
				tabs.find(".ui-tabs-nav").append(li);
				tabs.append("<div id='" + id + "'><p>" + tabContentHtml + "</p></div>");
				tabs.tabs("refresh");
				tabCounter++;
		
				// clear fields
				$("#tab_title").val("");
				$("#tab_content").val("");
			}
		
			// addTab button: just opens the dialog
			$("#add_tab").button().click(function() {
				var idd = document.getElementById('num').firstChild.nodeValue;

				
				dialog.dialog("open");
			});
		
			// close icon: removing the tab on click
					
			/*
			* ACCORDION
			*/
			//jquery accordion
			
		     var accordionIcons = {
		         header: "fa fa-plus",    // custom icon class
		         activeHeader: "fa fa-minus" // custom icon class
		     };
		     
			$("#accordion").accordion({
				autoHeight : false,
				heightStyle : "content",
				collapsible : true,
				animate : 300,
				icons: accordionIcons,
				header : "h4",
			})
		
			/*
			 * PROGRESS BAR
			 */
			$("#progressbar").progressbar({
		     	value: 25,
		     	create: function( event, ui ) {
		     		$(this).removeClass("ui-corner-all").addClass('progress').find(">:first-child").removeClass("ui-corner-left").addClass('progress-bar progress-bar-success');
				}
			});			

		})

	</script>		







<script type="text/javascript">

	$(document).ready(function() 
	{

		var $checkoutForm = $('#checkout-form').validate(
		{
		// Rules for form validation
			rules : 
			 {
				nombre : 
				{
					required : true
				},
				nit : 
				{
					required : true
				},
				telefono_fijo : 
				{
					required : true				
				},
		        correo : 
		        {
		          required : true,
		          email : true
		          
		        },
		        direccion : 
		        {
		          required : true
		          
		        }
			},

			// Messages for form validation
			messages : {
				nombre : {
					required : 'Ingrese el nombre de la empresa'
				},
				nit : {
					required : 'Ingrese el NIT de la empresa'
				},
		        telefono_fijo : {
		          required : 'Ingrese un numero de telefono fijo'
		        },
				correo : {
					required : 'Ingrese un correo Electronico',
					email : 'Ingrese un correo Electronico valido'
				}
				,
				direccion : {
					required : 'Ingrese una direccion'
				}
			},

			// Do not change code below
			errorPlacement : function(error, element) {
				error.insertAfter(element.parent());
			}
		});
		
		
		
				
	})

</script>


<?php 
	//include footer
	include("inc/google-analytics.php"); 
?>
<?php

}
else
{
    header("Location:index.php");
}
?>	    	