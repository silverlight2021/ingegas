<?php
//header('Content-Type: text/html; charset=ISO-8859-1');
//CONFIGURATION for SmartAdmin UI
require ("libraries/conexion.php");
if(@$_SESSION['logged']== 'yes')
{
@$acc = @$_SESSION['acc'];

//ribbon breadcrumbs config
//array("Display Name" => "URL");
//if(@$_SESSION['logged']== 'yes')
//{ 
  //	$id_user=$_SESSION['su'];
    //$acc = $_SESSION['acc'];
	$breadcrumbs = array(
	"Inicio" => APP_URL
	);

	/*navigation array config

	ex:
	"dashboard" => array(
		"title" => "Display Title",
		"url" => "http://yoururl.com",
		"url_target" => "_self",
		"icon" => "fa-home",
		"label_htm" => "<span>Add your custom label/badge html here</span>",
		"sub" => array() //contains array of sub items with the same format as the parent
	)
	*/

	$page_nav['menu_ppal']['title']="<h6>Menú Principal</h6>";
	$page_nav['menu_ppal']['icon']="fa-home";
	$page_nav['menu_ppal']['url']="menu_ppal.php";

	if (in_array(39, @$acc))
	{
		$page_nav['Clientes']['title']="<h6>Clientes</h6>";
		$page_nav['Clientes']['icon']="fa-group";
		$page_nav['Clientes']['url']="busqueda_clientes.php";
	}
	if (in_array(11, @$acc))
	{
		$page_nav['eto']['title']="<h6>E T O</h6>";
		$page_nav['eto']['icon']="fa-share-alt";

		if (in_array(12, $acc))
		{
			$page_nav['eto']['sub']['cilindros']['title']="<h7>Cilindros-ETO</h7>";
			$page_nav['eto']['sub']['cilindros']['icon']="fa-fire-extinguisher";
			$page_nav['eto']['sub']['cilindros']['url']="busqueda_cilindros_eto.php";
		}
		if (in_array(15, $acc))
		{	
			$page_nav['eto']['sub']['ordenes']['title']="<h7>Ordenes Producción</h7>";
			$page_nav['eto']['sub']['ordenes']['icon']="fa-database";
			$page_nav['eto']['sub']['ordenes']['url']="busqueda_ordenes.php";
		}
		if (in_array(18, $acc))
		{	
			$page_nav['eto']['sub']['revision_preliminar']['title']="<h7>Revisión</h7>";
			$page_nav['eto']['sub']['revision_preliminar']['icon']="fa-database";

			$page_nav['eto']['sub']['revision_preliminar']['sub']['revision']['title']="<h7>Revisión Preliminar</h7>";
			$page_nav['eto']['sub']['revision_preliminar']['sub']['revision']['icon']="fa-database";
			$page_nav['eto']['sub']['revision_preliminar']['sub']['revision']['url']="revision_preliminar.php";
			$page_nav['eto']['sub']['revision_preliminar']['sub']['lavado']['title']="<h7>Lavado ETO</h7>";
			$page_nav['eto']['sub']['revision_preliminar']['sub']['lavado']['icon']="fa-flask";
			$page_nav['eto']['sub']['revision_preliminar']['sub']['lavado']['url']="lavado_eto.php";
			$page_nav['eto']['sub']['revision_preliminar']['sub']['ph']['title']="<h7>PH</h7>";
			$page_nav['eto']['sub']['revision_preliminar']['sub']['ph']['icon']="fa-gavel";
			$page_nav['eto']['sub']['revision_preliminar']['sub']['ph']['url']="ph.php";
		}
		if (in_array(76, $acc))
		{	
			$page_nav['eto']['sub']['cilindros_especiales']['title']="<h7>Cilindros Especiales</h7>";
			$page_nav['eto']['sub']['cilindros_especiales']['icon']="fa-database";

			if(in_array(77, $acc)){
				$page_nav['eto']['sub']['cilindros_especiales']['sub']['salida']['title']="<h7>Salida de Cilindros</h7>";
				$page_nav['eto']['sub']['cilindros_especiales']['sub']['salida']['icon']="fa-truck";
				$page_nav['eto']['sub']['cilindros_especiales']['sub']['salida']['url']="busqueda_ordenes_especiales.php";
			}
			if(in_array(78, $acc)){
				$page_nav['eto']['sub']['cilindros_especiales']['sub']['llegada']['title']="<h7>Llegada de Cilindros</h7>";
				$page_nav['eto']['sub']['cilindros_especiales']['sub']['llegada']['icon']="fa-truck";
				$page_nav['eto']['sub']['cilindros_especiales']['sub']['llegada']['url']="recepcion_especiales.php";
			}
			
		}
		if (in_array(43, $acc))
		{	
			$page_nav['eto']['sub']['produccion']['title']="<h7>Producción Mezclas</h7>";
			$page_nav['eto']['sub']['produccion']['icon']="fa-database";
			$page_nav['eto']['sub']['produccion']['url']="busqueda_mezclas.php";
		}
		if (in_array(23, $acc))
		{	
			$page_nav['eto']['sub']['calidad']['title']="<h7>Calidad</h7>";
			$page_nav['eto']['sub']['calidad']['icon']="fa-database";

			$page_nav['eto']['sub']['calidad']['sub']['r_calidad']['title']="<h7>Calidad</h7>";
			$page_nav['eto']['sub']['calidad']['sub']['r_calidad']['icon']="fa-thumbs-o-up";
			$page_nav['eto']['sub']['calidad']['sub']['r_calidad']['url']="busqueda_calidad.php";
			$page_nav['eto']['sub']['calidad']['sub']['lavado']['title']="<h7>No Conformidades</h7>";
			$page_nav['eto']['sub']['calidad']['sub']['lavado']['icon']="fa-crosshairs";
			$page_nav['eto']['sub']['calidad']['sub']['lavado']['url']="no_conformidades.php";
		}
		if (in_array(25, $acc))
		{	
			$page_nav['eto']['sub']['logistica']['title']="<h7>Logística</h7>";
			$page_nav['eto']['sub']['logistica']['icon']="fa-database";
			$page_nav['eto']['sub']['logistica']['url']="busqueda_logistica.php";
		}
		if (in_array(88, $acc))
		{	
			$page_nav['eto']['sub']['firma']['title']="<h7>Firma de Certificados</h7>";
			$page_nav['eto']['sub']['firma']['icon']="fa-pencil-square-o";
			$page_nav['eto']['sub']['firma']['url']="firma_certificados_eto.php";
		}
		if (in_array(28, $acc))
		{	
			$page_nav['eto']['sub']['transporte']['title']="<h7>Transporte</h7>";
			$page_nav['eto']['sub']['transporte']['icon']="fa-truck";
			$page_nav['eto']['sub']['transporte']['url']="busqueda_transporte_eto.php";
		}
			
		if (in_array(42, $acc))
		{
			$page_nav['eto']['sub']['lote_insumo']['title']="<h7>Lote Insumo</h7>";
			$page_nav['eto']['sub']['lote_insumo']['icon']="fa-share-alt";
			$page_nav['eto']['sub']['lote_insumo']['sub']['lote_eto']['title']="<h7>Lote ETO</h7>";
			$page_nav['eto']['sub']['lote_insumo']['sub']['lote_eto']['icon']="fa-database";
			$page_nav['eto']['sub']['lote_insumo']['sub']['lote_eto']['url']="busqueda_eto.php";
			$page_nav['eto']['sub']['lote_insumo']['sub']['lote_co2']['title']="<h7>Lote CO2</h7>";
			$page_nav['eto']['sub']['lote_insumo']['sub']['lote_co2']['icon']="fa-database";
			$page_nav['eto']['sub']['lote_insumo']['sub']['lote_co2']['url']="busqueda_co2.php";
		}
		
	}
	if (in_array(46, @$acc))
	{
		$page_nav['pev']['title']="<h6>P E V</h6>";
		$page_nav['pev']['icon']="fa-tasks";


		if (in_array(47, $acc))
		{
			$page_nav['pev']['sub']['transporte']['title']="<h7>Transporte PEV</h7>";
			$page_nav['pev']['sub']['transporte']['icon']="fa-truck";
			$page_nav['pev']['sub']['transporte']['url']="busqueda_transporte_pev.php";
		}
		if (in_array(50, $acc))
		{
			$page_nav['pev']['sub']['Asignacion']['title']="<h7>Asignación servicios</h7>";
			$page_nav['pev']['sub']['Asignacion']['icon']="fa-wrench";
			$page_nav['pev']['sub']['Asignacion']['url']="busqueda_ordenes_pev.php";
		}
		if (in_array(71, $acc))
		{	
			$page_nav['pev']['sub']['cilindros_pev']['title']="<h7>Cilindros PEV</h7>";
			$page_nav['pev']['sub']['cilindros_pev']['icon']="fa-fire-extinguisher";
			$page_nav['pev']['sub']['cilindros_pev']['url']="busqueda_cilindros_pev.php";
		}
		if (in_array(53, $acc))
		{	
			$page_nav['pev']['sub']['insp_visual']['title']="<h7>Inspección Visual</h7>";
			$page_nav['pev']['sub']['insp_visual']['icon']="fa-eye";
			$page_nav['pev']['sub']['insp_visual']['url']="busqueda_insp_visual.php";
		}
		if (in_array(72, $acc))
		{
			$page_nav['pev']['sub']['calibracion']['title']="<h7>Verificación del Método</h7>";
			$page_nav['pev']['sub']['calibracion']['icon']="fa-wrench";
			$page_nav['pev']['sub']['calibracion']['url']="busqueda_calibracion.php";
		}
		if (in_array(72, $acc))
		{	
			$page_nav['pev']['sub']['prueba']['title']="<h7>Prueba</h7>";
			$page_nav['pev']['sub']['prueba']['icon']="fa-dashboard";
			$page_nav['pev']['sub']['prueba']['url']="prueba_pev.php";
		}
		if (in_array(73, $acc)){
			$page_nav['pev']['sub']['ejecucion']['title']="<h7>Ejecucion de Servicios</h7>";
			$page_nav['pev']['sub']['ejecucion']['icon']="fa-gear";
			$page_nav['pev']['sub']['ejecucion']['url']="busqueda_ordenespev_ejecucion.php";
		}
		if (in_array(87, $acc)){
			$page_nav['pev']['sub']['firma']['title']="<h7>Firma de Certificados</h7>";
			$page_nav['pev']['sub']['firma']['icon']="fa-pencil-square-o";
			$page_nav['pev']['sub']['firma']['url']="firma_certificados_pev.php";
		}
		if (in_array(73, $acc))
		{	
			$page_nav['pev']['sub']['llenado']['title']="<h7>Manual PEV</h7>";
			$page_nav['pev']['sub']['llenado']['icon']="fa-database";
			$page_nav['pev']['sub']['llenado']['url']="manual_pev_ingegas.pdf";
		}
		
		
	}

	if(in_array(64, $acc)){

		$page_nav['informes']['title']="<h6>Informes</h6>";
		$page_nav['informes']['icon']="fa-sitemap";
		if(in_array(65, $acc)){
			$page_nav['informes']['sub']['produccion_me']['title']="<h7>Produccion de Mezclas</h7>";
			$page_nav['informes']['sub']['produccion_me']['icon']="fa-gear";
			$page_nav['informes']['sub']['produccion_me']['url']="informe_pro_mezclas.php";
		}
		if(in_array(66, $acc)){
			$page_nav['informes']['sub']['tipo_gas']['title']="<h7>Tipo Gas</h7>";
			$page_nav['informes']['sub']['tipo_gas']['icon']="fa-gear";
			$page_nav['informes']['sub']['tipo_gas']['url']="informe_proceso.php";
		}
		if(in_array(67, $acc)){
			$page_nav['informes']['sub']['estado_pro']['title']="<h7>Estado del Proceso</h7>";
			$page_nav['informes']['sub']['estado_pro']['icon']="fa-gear";
			$page_nav['informes']['sub']['estado_pro']['url']="informe_tipo_gas.php";
		}
		if(in_array(68, $acc)){
			$page_nav['informes']['sub']['por_cliente']['title']="<h7>Informe Por Cliente</h7>";
			$page_nav['informes']['sub']['por_cliente']['icon']="fa-gear";
			$page_nav['informes']['sub']['por_cliente']['url']="informe_por_busqueda_cliente.php";
		}
		if(in_array(69, $acc)){
			$page_nav['informes']['sub']['consumo']['title']="<h7>Informe Por Consumo</h7>";
			$page_nav['informes']['sub']['consumo']['icon']="fa-gear";
			$page_nav['informes']['sub']['consumo']['url']="informe_consumo.php";
		}
		if(in_array(70, $acc)){
			$page_nav['informes']['sub']['evacuado']['title']="<h7>Informe Por Gas Evacuado</h7>";
			$page_nav['informes']['sub']['evacuado']['icon']="fa-gear";
			$page_nav['informes']['sub']['evacuado']['url']="informe_gas_evacuado.php";
		}
		if(in_array(86, $acc)){
			$page_nav['informes']['sub']['Rutas']['title']="<h7>Informe Rutas Cliente</h7>";
			$page_nav['informes']['sub']['Rutas']['icon']="fa-gear";
			$page_nav['informes']['sub']['Rutas']['url']="informe_rutas_cliente.php";
		}
	}
		
	if (in_array(56, $acc))
	{		
		$page_nav['rutero']['title']="<h6>Rutero</h6>";
		$page_nav['rutero']['icon']="fa-calendar-o";
		if (in_array(57, $acc))
		{	
			$page_nav['rutero']['sub']['hoja_ruta']['title']="<h7>Hoja de Ruta</h7>";
			$page_nav['rutero']['sub']['hoja_ruta']['icon']="fa-calendar-o";
			$page_nav['rutero']['sub']['hoja_ruta']['url']="nueva_busqueda_hoja_ruta.php";
		}
		if (in_array(61, $acc))
		{	
			$page_nav['rutero']['sub']['informes']['title']="<h7>Informes</h7>";
			$page_nav['rutero']['sub']['informes']['icon']="fa-info-circle";
			$page_nav['rutero']['sub']['informes']['url']="informes_hoja_ruta.php";
		}
		if (in_array(82, $acc)){
			$page_nav['rutero']['sub']['verificacion']['title']="<h7>Verificación Mensual</h7>";
			$page_nav['rutero']['sub']['verificacion']['icon']="fa-truck";
			$page_nav['rutero']['sub']['verificacion']['url']="busqueda_verificacion_mensual_vehiculos.php";
		}
	}

	if (in_array(89, $acc))
	{		
		$page_nav['comisiones']['title']="<h6>Comisiones</h6>";
		$page_nav['comisiones']['icon']="fa-usd";

		$page_nav['comisiones']['sub']['messer']['title']="<h7>Messer</h7>";
		$page_nav['comisiones']['sub']['messer']['icon']="fa-database";

		$page_nav['comisiones']['sub']['messer']['sub']['cargue']['title']="<h7>Recibos de Caja</h7>";
		$page_nav['comisiones']['sub']['messer']['sub']['cargue']['icon']="fa-file-text";
		$page_nav['comisiones']['sub']['messer']['sub']['cargue']['url']="cargue_archivo_comisiones.php";

		$page_nav['comisiones']['sub']['messer']['sub']['consolidado']['title']="<h7>Consolidado Recibos de Caja</h7>";
		$page_nav['comisiones']['sub']['messer']['sub']['consolidado']['icon']="fa-file-text";
		$page_nav['comisiones']['sub']['messer']['sub']['consolidado']['url']="consolidado_comisiones.php";

		$page_nav['comisiones']['sub']['messer']['sub']['acumulado']['title']="<h7>Ventas Acumuladas</h7>";
		$page_nav['comisiones']['sub']['messer']['sub']['acumulado']['icon']="fa-file-text";
		$page_nav['comisiones']['sub']['messer']['sub']['acumulado']['url']="cargue_archivo_acumulados.php";

		$page_nav['comisiones']['sub']['messer']['sub']['producto']['title']="<h7>Ventas por Producto</h7>";
		$page_nav['comisiones']['sub']['messer']['sub']['producto']['icon']="fa-file-text";
		$page_nav['comisiones']['sub']['messer']['sub']['producto']['url']="cargue_producto.php";

		$page_nav['comisiones']['sub']['messer']['sub']['unneg']['title']="<h7>UNNEG</h7>";
		$page_nav['comisiones']['sub']['messer']['sub']['unneg']['icon']="fa-file-text";
		$page_nav['comisiones']['sub']['messer']['sub']['unneg']['url']="cargue_unneg.php";

		$page_nav['comisiones']['sub']['messer']['sub']['consolidadoComisiones']['title']="<h7>Consolidado de Comisiones</h7>";
		$page_nav['comisiones']['sub']['messer']['sub']['consolidadoComisiones']['icon']="fa-file-text";
		$page_nav['comisiones']['sub']['messer']['sub']['consolidadoComisiones']['url']="busquedaArchivoComisiones.php";

		$page_nav['comisiones']['sub']['vendedores']['title']="<h7>Vendedores</h7>";
		$page_nav['comisiones']['sub']['vendedores']['icon']="fa-database";

		$page_nav['comisiones']['sub']['vendedores']['sub']['movimiento_inventario']['title']="<h7>Cargue de Movimientos de Inventario</h7>";
		$page_nav['comisiones']['sub']['vendedores']['sub']['movimiento_inventario']['icon']="fa-file-text";
		$page_nav['comisiones']['sub']['vendedores']['sub']['movimiento_inventario']['url']="movimiento_inventario.php";

		$page_nav['comisiones']['sub']['vendedores']['sub']['asignacion_vendedores']['title']="<h7>Asignación Vendedores</h7>";
		$page_nav['comisiones']['sub']['vendedores']['sub']['asignacion_vendedores']['icon']="fa-file-text";
		$page_nav['comisiones']['sub']['vendedores']['sub']['asignacion_vendedores']['url']="asignacion_vendedores.php";

		/*$page_nav['comisiones']['sub']['vendedores']['sub']['consolidado_ventas']['title']="<h7>Consolidado de Ventas</h7>";
		$page_nav['comisiones']['sub']['vendedores']['sub']['consolidado_ventas']['icon']="fa-file-text";
		$page_nav['comisiones']['sub']['vendedores']['sub']['consolidado_ventas']['url']="consolidado_ventas.php";*/

		$page_nav['comisiones']['sub']['vendedores']['sub']['ventas_vendedor']['title']="<h7>Cargue de Ventas - Vendedor</h7>";
		$page_nav['comisiones']['sub']['vendedores']['sub']['ventas_vendedor']['icon']="fa-file-text";
		$page_nav['comisiones']['sub']['vendedores']['sub']['ventas_vendedor']['url']="ventas_vendedor.php";

		$page_nav['comisiones']['sub']['vendedores']['sub']['cargue_notas_credito']['title']="<h7>Cargue de Notas Credito</h7>";
		$page_nav['comisiones']['sub']['vendedores']['sub']['cargue_notas_credito']['icon']="fa-file-text";
		$page_nav['comisiones']['sub']['vendedores']['sub']['cargue_notas_credito']['url']="cargue_notas_credito.php";

		$page_nav['comisiones']['sub']['vendedores']['sub']['cargue_cobranza']['title']="<h7>Cargue de Cobranza</h7>";
		$page_nav['comisiones']['sub']['vendedores']['sub']['cargue_cobranza']['icon']="fa-file-text";
		$page_nav['comisiones']['sub']['vendedores']['sub']['cargue_cobranza']['url']="cargue_cobranza.php";

		$page_nav['comisiones']['sub']['vendedores']['sub']['cargue_anticipos']['title']="<h7>Cargue de Anticipos</h7>";
		$page_nav['comisiones']['sub']['vendedores']['sub']['cargue_anticipos']['icon']="fa-file-text";
		$page_nav['comisiones']['sub']['vendedores']['sub']['cargue_anticipos']['url']="cargue_anticipos.php";

		/*$page_nav['comisiones']['sub']['vendedores']['sub']['facturas_comisiones']['title']="<h7>Cargue de Facturas Comisiones</h7>";
		$page_nav['comisiones']['sub']['vendedores']['sub']['facturas_comisiones']['icon']="fa-file-text";
		$page_nav['comisiones']['sub']['vendedores']['sub']['facturas_comisiones']['url']="cargue_facturas_comisiones.php";*/

		/*$page_nav['comisiones']['sub']['vendedores']['sub']['rotacion_cilindros']['title']="<h7>Rotacion de Cilindros</h7>";
		$page_nav['comisiones']['sub']['vendedores']['sub']['rotacion_cilindros']['icon']="fa-file-text";
		$page_nav['comisiones']['sub']['vendedores']['sub']['rotacion_cilindros']['url']="cargue_rotacion_cilindros.php";*/

		$page_nav['comisiones']['sub']['vendedores']['sub']['comisiones_vendedores']['title']="<h7>Descargue Consolidado</h7>";
		$page_nav['comisiones']['sub']['vendedores']['sub']['comisiones_vendedores']['icon']="fa-file-text";
		$page_nav['comisiones']['sub']['vendedores']['sub']['comisiones_vendedores']['url']="comisiones_vendedores.php";

		/*$page_nav['comisiones']['sub']['cargue']['title']="<h7>Cargue de Recibos de Caja</h7>";
		$page_nav['comisiones']['sub']['cargue']['icon']="fa-file-text";
		$page_nav['comisiones']['sub']['cargue']['url']="cargue_archivo_comisiones.php";

		$page_nav['comisiones']['sub']['consolidado']['title']="<h7>Consolidado Comisiones</h7>";
		$page_nav['comisiones']['sub']['consolidado']['icon']="fa-file-text";
		$page_nav['comisiones']['sub']['consolidado']['url']="consolidado_comisiones.php";

		$page_nav['comisiones']['sub']['acumulado']['title']="<h7>Cargue de Ventas Acumulado</h7>";
		$page_nav['comisiones']['sub']['acumulado']['icon']="fa-file-text";
		$page_nav['comisiones']['sub']['acumulado']['url']="cargue_archivo_acumulados.php";

		$page_nav['comisiones']['sub']['producto']['title']="<h7>Cargue de Ventas por Producto</h7>";
		$page_nav['comisiones']['sub']['producto']['icon']="fa-file-text";
		$page_nav['comisiones']['sub']['producto']['url']="cargue_producto.php";

		$page_nav['comisiones']['sub']['unneg']['title']="<h7>Cargue de UNNEG</h7>";
		$page_nav['comisiones']['sub']['unneg']['icon']="fa-file-text";
		$page_nav['comisiones']['sub']['unneg']['url']="cargue_unneg.php";

		$page_nav['comisiones']['sub']['ventasProducto']['title']="<h7>Archivo de Comisiones</h7>";
		$page_nav['comisiones']['sub']['ventasProducto']['icon']="fa-file-text";
		$page_nav['comisiones']['sub']['ventasProducto']['url']="busquedaArchivoComisiones.php";*/
		
	}

	if (in_array(1, @$acc))
	{		
		$page_nav['configuracion']['title']="<h6>Configuración</h6>";
		$page_nav['configuracion']['icon']="fa-cogs";
		$page_nav['configuracion']['sub']['parametros']['title']="<h7>Parametros</h7>";
		$page_nav['configuracion']['sub']['parametros']['icon']="fa-gear";
		$page_nav['configuracion']['sub']['parametros']['url']="parametros.php";
		$page_nav['configuracion']['sub']['perfil']['title']="<h7>Perfil</h7>";
		$page_nav['configuracion']['sub']['perfil']['icon']="fa-unlock-alt";
		$page_nav['configuracion']['sub']['perfil']['url']="busqueda_perfil.php";
		$page_nav['configuracion']['sub']['usuario']['title']="<h7>Usuario</h7>";
		$page_nav['configuracion']['sub']['usuario']['icon']="fa-user";
		$page_nav['configuracion']['sub']['usuario']['url']="busqueda_usuario.php";
	}
	
	//configuration variables
	$page_title = "";
	$page_css = array();
	$no_main_header = false; //set true for lock.php and login.php
	$page_body_prop = array(); //optional properties for <body>
	$page_html_prop = array(); //optional properties for <html>
}
//}
?>