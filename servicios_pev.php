<?php
session_start();
date_default_timezone_set("America/Bogota");
require ("libraries/conexion.php");
require ("libraries/seguimiento_pev.php");
$hora= date ("h:i:s");
if($_SESSION['logged']== 'yes')
{ 
	require_once("inc/init.php");
	require_once("inc/config.ui.php");
	$page_title = "Asignación Servicios";
	$page_css[] = "your_style.css";
	include("inc/header.php");
	$page_nav[""][""][""][""] = true;
	include("inc/nav.php");

	//Se solicitan datos del transporte
  	$id_transporte_pev = isset($_REQUEST['id_transporte_pev']) ? $_REQUEST['id_transporte_pev'] : NULL;
  	//$orden_pev = isset($_REQUEST['orden_pev']) ? $_REQUEST['orden_pev'] : NULL;
    $id_user =$_SESSION['su'];
    $acc = $_SESSION['acc'];
    $opciones[] = '';

    if (strlen($id_transporte_pev) > 0)
    {	
    	$consulta3 = "SELECT * FROM transporte_pev 
    				  WHERE id_transporte_pev = '".$id_transporte_pev."'";    				  
    	$resultado3 = mysqli_query($con,$consulta3);

    	if (mysqli_num_rows($resultado3) > 0) 
    	{
    		$linea3 = mysqli_fetch_assoc($resultado3);
    		//$id_finalizar = $linea3['id_finalizar'];
    		$id_estado_transporte_pev = $linea3['id_estado_transporte_pev'];
    		$id_cliente = $linea3['id_cliente'];
    		$num_trans = $linea3['num_trans'];
    		$fecha = $linea3['fecha'];

    		$consulta8 = "UPDATE orden_servicio_pev 
    					  SET id_cliente = '".$id_cliente."', id_transporte_pev = '".$id_transporte_pev."' 
    					  WHERE orden_pev = '".$orden_pev."'";

    		$resultado8 = mysqli_query($con,$consulta8);
			if ($resultado8 == FALSE)
			{
				echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
			}

    		$consulta4 = "SELECT nombre FROM clientes WHERE id_cliente = '".$id_cliente."'";
    		$resultado4 = mysqli_query($con,$consulta4);
    		if (mysqli_num_rows($resultado4) > 0 )
    		{
    			$linea4 = mysqli_fetch_assoc($resultado4);
    			$nombre = $linea4['nombre'];
    		}
    		if(isset($_POST['guardar_servicios']))
    		{
    			$id_transporte_pev = $_POST['id_transporte_pev'];
    			//Se consulta si ya existe una orden de servicio asignada al transporte
    			$consulta7 = "SELECT id_orden_pev 
    						  FROM orden_servicio_pev 
    						  WHERE id_transporte_pev = '".$id_transporte_pev."'";
    			$resultado7 = mysqli_query($con,$consulta7);

    			if(mysqli_num_rows($resultado7) > 0) // Si existe, se guardan y actualizan servicios a la orden
    			{
    				$consulta6 = "UPDATE has_movimiento_cilindro_pev
							  	  SET ph_u = '0', llenado_u = '0', pintura_u = '0', valvula_u = '0', granallado_u = '0', cambio_serv_u = '0', lavado_especial_u = '0'
							  	  WHERE id_transporte_pev = '".$id_transporte_pev."'";
					$resultado6 = mysqli_query($con,$consulta6);
					if ($resultado6 == FALSE)
					{
						echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
					}

					$opcion = isset($_POST['opcion']) ? $_POST['opcion'] : NULL;
					
					if(count($opcion))
					{
						foreach ($opcion as $id_has_movimiento_cilindro_pev) 
						{
							$checklist = explode("~", $id_has_movimiento_cilindro_pev);
							
							$consulta5 = "UPDATE has_movimiento_cilindro_pev 
										SET ".$checklist[1]."_u = '1'
										WHERE id_has_movimiento_cilindro_pev = '".$checklist[0]."'";
							$resultado5 = mysqli_query($con,$consulta5);
							if ($resultado5 == FALSE)
							{
								echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
							}
							else
							{
								$contador = $contador + 1;
							}
						}
						$observacion_u = isset($_POST['observacion_u']) ? $_POST['observacion_u'] : NULL;
						$id_has = isset($_POST['id_has']) ? $_POST['id_has'] : NULL;
						$servicio = isset($_POST['servicio']) ? $_POST['servicio'] : NULL;
						if(count($observacion_u))
						{	
							if(count($id_has))
							{
								$contador = 0;
								foreach ($id_has as $key1) 
								{
									$consulta5 = "UPDATE has_movimiento_cilindro_pev 
												SET observacion_u = '".$observacion_u[$contador]."', material_cilindro_u = '".$servicio[$contador]."'
												WHERE id_has_movimiento_cilindro_pev = '".$key1."'";
									$resultado5 = mysqli_query($con,$consulta5);
									if ($resultado5 == FALSE)
									{
										echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
									}
									$contador = $contador + 1;
									
								}
							}
							$consulta9 = "UPDATE transporte_pev SET id_finalizar = '2' WHERE  id_transporte_pev = '".$id_transporte_pev."'";
							$resultado9 = mysqli_query($con,$consulta9);
							if($resultado9 == FALSE)
							{
								echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
							}
							$consulta11 = "SELECT * FROM has_movimiento_cilindro_pev WHERE id_transporte_pev = $id_transporte_pev";
							$resultado11 = mysqli_query($con,$consulta11);
							while ($linea11 = mysqli_fetch_array($resultado11)) {
								$num_cili3 = isset($linea11["num_cili"]) ? $linea11["num_cili"] : NULL;
								$ph_u3 = isset($linea11["ph_u"]) ? $linea11["ph_u"] : NULL;
								$llenado_u3 = isset($linea11["llenado_u"]) ? $linea11["llenado_u"] : NULL;
								$pintura_u3 = isset($linea11["pintura_u"]) ? $linea11["pintura_u"] : NULL;
								$valvula_u3 = isset($linea11["valvula_u"]) ? $linea11["valvula_u"] : NULL;
								$granallado_u3 = isset($linea11["granallado_u"]) ? $linea11["granallado_u"] : NULL;
								$cambio_serv_u3 = isset($linea11["cambio_serv_u"]) ? $linea11["cambio_serv_u"] : NULL;
								$lavado_especial_u3 = isset($linea11["lavado_especial_u"]) ? $linea11["lavado_especial_u"] : NULL;

								if($ph_u3==1){
									$consulta12 = "UPDATE cilindro_pev SET id_estado = 2 WHERE num_cili_pev = '$num_cili3'";
								}else if($llenado_u3 == 1){
									$consulta12 = "UPDATE cilindro_pev SET id_estado = 13 WHERE num_cili_pev = '$num_cili3'";
								}else if($pintura_u3 == 1){
									$consulta12 = "UPDATE cilindro_pev SET id_estado = 6 WHERE num_cili_pev = '$num_cili3'";
								}else if($valvula_u3 == 1){
									$consulta12 = "UPDATE cilindro_pev SET id_estado = 4 WHERE num_cili_pev = '$num_cili3'";
								}else if($granallado_u3 == 1){
									$consulta12 = "UPDATE cilindro_pev SET id_estado = 7 WHERE num_cili_pev = '$num_cili3'";
								}else if($cambio_serv_u3 == 1){
									$consulta12 = "UPDATE cilindro_pev SET id_estado = 8 WHERE num_cili_pev = '$num_cili3'";
								}else if($lavado_especial_u3 == 1){
									$consulta12 = "UPDATE cilindro_pev SET id_estado = 12 WHERE num_cili_pev = '$num_cili3'";
								}

								mysqli_query($con,$consulta12);
							}
							?>
							<script type="text/javascript">
								var id_transporte_pev = <?php echo $id_transporte_pev; ?>;
								alert("Los servicios han sido actualizados exitosamente. Presione aceptar para continuar");
								window.location = ("busqueda_ordenes_pev.php");
							</script>
							<?php
						}
					}
					else
					{
						?>
						<script type="text/javascript">
							alert("Debe seleccionar al menos un elemento de la lista");
						</script>
						<?php
					}
    			}
    			else // Si no existe, se crea la orden y se asignan servicios a la orden
    			{
    				$consulta8 = "SELECT orden_pev FROM orden_servicio_pev ORDER BY orden_pev DESC LIMIT 1";
					$resultado8 = mysqli_query($con,$consulta8);
					if (mysqli_num_rows($resultado8) > 0)
				    {
				        $linea8 = mysqli_fetch_assoc($resultado8);
				        $orden_pev = $linea8['orden_pev'];
				        $orden_pev = $orden_pev + 1;
				        $consulta9 = "INSERT INTO orden_servicio_pev (id_cliente, orden_pev, fecha_orden_pev, hora_orden_pev, id_transporte_pev, id_user) 
				        		      VALUES ('".$id_cliente."', '".$orden_pev."', '".$fecha."', '".$hora."', '".$id_transporte_pev."', '".$id_user."')";
				        if(mysqli_query($con,$consulta9))
				        {
				        	$consulta6 = "UPDATE has_movimiento_cilindro_pev
							  	  SET ph_u = '0', llenado_u = '0', pintura_u = '0', valvula_u = '0', granallado = '0'
							  	  WHERE id_transporte_pev = '".$id_transporte_pev."'";
							$resultado6 = mysqli_query($con,$consulta6);
							if ($resultado6 == FALSE)
							{
								echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
							}

							$opcion = isset($_POST['opcion']) ? $_POST['opcion'] : NULL;
							
							if(count($opcion))
							{
								foreach ($opcion as $id_has_movimiento_cilindro_pev) 
								{
									$checklist = explode("~", $id_has_movimiento_cilindro_pev);
									
									$consulta5 = "UPDATE has_movimiento_cilindro_pev 
												SET ".$checklist[1]."_u = '1'
												WHERE id_has_movimiento_cilindro_pev = '".$checklist[0]."'";
									$resultado5 = mysqli_query($con,$consulta5);

									if ($resultado5 == FALSE)
									{
										echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
									}
									else
									{
										$contador = $contador + 1;
									}
								}
								$observacion_u = isset($_POST['observacion_u']) ? $_POST['observacion_u'] : NULL;
								$id_has = isset($_POST['id_has']) ? $_POST['id_has'] : NULL;
								$servicio = isset($_POST['servicio']) ? $_POST['servicio'] : NULL;
								if(count($observacion_u))
								{	
									if(count($id_has))
									{
										$contador = 0;
										foreach ($id_has as $key1) 
										{
											$consulta5 = "UPDATE has_movimiento_cilindro_pev 
														SET observacion_u = '".$observacion_u[$contador]."', material_cilindro_u = '".$servicio[$contador]."'
														WHERE id_has_movimiento_cilindro_pev = '".$key1."'";
											$resultado5 = mysqli_query($con,$consulta5);
											seguimiento_pev(4,$id_cilindro_pev,$id_user,1,$id_has_movimiento_cilindro_pev);
											if ($resultado5 == FALSE)
											{
												echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
											}
											$contador = $contador + 1;
											
										}
									}

									$consulta9 = "UPDATE transporte_pev SET id_finalizar = '2' WHERE  id_transporte_pev = '".$id_transporte_pev."'";
									$resultado9 = mysqli_query($con,$consulta9);
									if($resultado9 == FALSE)
									{
										echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
									}
									$consulta11 = "SELECT * FROM has_movimiento_cilindro_pev WHERE id_transporte_pev = $id_transporte_pev";
									$resultado11 = mysqli_query($con,$consulta11);
									while ($linea11 = mysqli_fetch_array($resultado11)) {
										$num_cili3 = isset($linea11["num_cili"]) ? $linea11["num_cili"] : NULL;
										$ph_u3 = isset($linea11["ph_u"]) ? $linea11["ph_u"] : NULL;
										$llenado_u3 = isset($linea11["llenado_u"]) ? $linea11["llenado_u"] : NULL;
										$pintura_u3 = isset($linea11["pintura_u"]) ? $linea11["pintura_u"] : NULL;
										$valvula_u3 = isset($linea11["valvula_u"]) ? $linea11["valvula_u"] : NULL;
										$granallado_u3 = isset($linea11["granallado_u"]) ? $linea11["granallado_u"] : NULL;
										$cambio_serv_u3 = isset($linea11["cambio_serv_u"]) ? $linea11["cambio_serv_u"] : NULL;
										$lavado_especial_u3 = isset($linea11["lavado_especial_u"]) ? $linea11["lavado_especial_u"] : NULL;

										if($ph_u3==1){
											$consulta12 = "UPDATE cilindro_pev SET id_estado = 2 WHERE num_cili_pev = '$num_cili3'";
										}else if($llenado_u3 == 1){
											$consulta12 = "UPDATE cilindro_pev SET id_estado = 13 WHERE num_cili_pev = '$num_cili3'";
										}else if($pintura_u3 == 1){
											$consulta12 = "UPDATE cilindro_pev SET id_estado = 6 WHERE num_cili_pev = '$num_cili3'";
										}else if($valvula_u3 == 1){
											$consulta12 = "UPDATE cilindro_pev SET id_estado = 4 WHERE num_cili_pev = '$num_cili3'";
										}else if($granallado_u3 == 1){
											$consulta12 = "UPDATE cilindro_pev SET id_estado = 7 WHERE num_cili_pev = '$num_cili3'";
										}else if($cambio_serv_u3 == 1){
											$consulta12 = "UPDATE cilindro_pev SET id_estado = 8 WHERE num_cili_pev = '$num_cili3'";
										}else if($lavado_especial_u3 == 1){
											$consulta12 = "UPDATE cilindro_pev SET id_estado = 12 WHERE num_cili_pev = '$num_cili3'";
										}

										mysqli_query($con,$consulta12);
									}
									?>
									<script type="text/javascript">
										var id_transporte_pev = <?php echo $id_transporte_pev; ?>;
										alert("Los servicios han sido actualizados exitosamente. Presione aceptar para continuar");
										window.location = ("busqueda_ordenes_pev.php");
									</script>
									<?php
								}
							}
							else
							{
								?>
								<script type="text/javascript">
									alert("Debe seleccionar al menos un elemento de la lista");
								</script>
								<?php
							}
				        }

				    }
				    else //Si no existe ninguna orden, se crea la primera
				    {
				        $orden_pev = 1;
				        $consulta2 = "INSERT INTO orden_servicio_pev (id_cliente, orden_pev, fecha_orden_pev, hora_orden_pev, id_transporte_pev, id_user) 
				        		      VALUES ('".$id_cliente."', '".$orden_pev."', '".$fecha."', '".$hora."', '".$id_transporte_pev."', '".$id_user."')";
				        if(mysqli_query($con,$consulta2))
				        {
				        	$consulta6 = "UPDATE has_movimiento_cilindro_pev
							  	  		  SET ph_u = '0', llenado_u = '0', pintura_u = '0', valvula_u = '0', granallado_u = '0', cambio_serv_u = '0', lavado_especial_u = '0'
							  	  		  WHERE id_transporte_pev = '".$id_transporte_pev."'";
							$resultado6 = mysqli_query($con,$consulta6);
							if ($resultado6 == FALSE)
							{
								echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
							}

							$opcion = isset($_POST['opcion']) ? $_POST['opcion'] : NULL;
							
							if(count($opcion))
							{
								foreach ($opcion as $id_has_movimiento_cilindro_pev) 
								{
									$checklist = explode("~", $id_has_movimiento_cilindro_pev);
									
									$consulta5 = "UPDATE has_movimiento_cilindro_pev 
												SET ".$checklist[1]."_u = '1'
												WHERE id_has_movimiento_cilindro_pev = '".$checklist[0]."'";
									$resultado5 = mysqli_query($con,$consulta5);
									if ($resultado5 == FALSE)
									{
										echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
									}
									else
									{
										$contador = $contador + 1;
									}
								}
								$observacion_u = isset($_POST['observacion_u']) ? $_POST['observacion_u'] : NULL;
								$id_has = isset($_POST['id_has']) ? $_POST['id_has'] : NULL;
								$servicio = isset($_POST['servicio']) ? $_POST['servicio'] : NULL;
								if(count($observacion_u))
								{	
									if(count($id_has))
									{
										$contador = 0;
										foreach ($id_has as $key1) 
										{
											$consulta5 = "UPDATE has_movimiento_cilindro_pev 
														SET observacion_u = '".$observacion_u[$contador]."', material_cilindro_u = '".$servicio[$contador]."'
														WHERE id_has_movimiento_cilindro_pev = '".$key1."'";
											$resultado5 = mysqli_query($con,$consulta5);
											if ($resultado5 == FALSE)
											{
												echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
											}
											$contador = $contador + 1;
											
										}
									}
									$consulta9 = "UPDATE transporte_pev SET id_finalizar = '2' WHERE  id_transporte_pev = '".$id_transporte_pev."'";
									$resultado9 = mysqli_query($con,$consulta9);
									if($resultado9 == FALSE)
									{
										echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
									}
									$consulta11 = "SELECT * FROM has_movimiento_cilindro_pev WHERE id_transporte_pev = $id_transporte_pev";
									$resultado11 = mysqli_query($con,$consulta11);
									while ($linea11 = mysqli_fetch_array($resultado11)) {
										$num_cili3 = isset($linea11["num_cili"]) ? $linea11["num_cili"] : NULL;
										$ph_u3 = isset($linea11["ph_u"]) ? $linea11["ph_u"] : NULL;
										$llenado_u3 = isset($linea11["llenado_u"]) ? $linea11["llenado_u"] : NULL;
										$pintura_u3 = isset($linea11["pintura_u"]) ? $linea11["pintura_u"] : NULL;
										$valvula_u3 = isset($linea11["valvula_u"]) ? $linea11["valvula_u"] : NULL;
										$granallado_u3 = isset($linea11["granallado_u"]) ? $linea11["granallado_u"] : NULL;
										$cambio_serv_u3 = isset($linea11["cambio_serv_u"]) ? $linea11["cambio_serv_u"] : NULL;
										$lavado_especial_u3 = isset($linea11["lavado_especial_u"]) ? $linea11["lavado_especial_u"] : NULL;

										if($ph_u3==1){
											$consulta12 = "UPDATE cilindro_pev SET id_estado = 2 WHERE num_cili_pev = '$num_cili3'";
										}else if($llenado_u3 == 1){
											$consulta12 = "UPDATE cilindro_pev SET id_estado = 13 WHERE num_cili_pev = '$num_cili3'";
										}else if($pintura_u3 == 1){
											$consulta12 = "UPDATE cilindro_pev SET id_estado = 6 WHERE num_cili_pev = '$num_cili3'";
										}else if($valvula_u3 == 1){
											$consulta12 = "UPDATE cilindro_pev SET id_estado = 4 WHERE num_cili_pev = '$num_cili3'";
										}else if($granallado_u3 == 1){
											$consulta12 = "UPDATE cilindro_pev SET id_estado = 7 WHERE num_cili_pev = '$num_cili3'";
										}else if($cambio_serv_u3 == 1){
											$consulta12 = "UPDATE cilindro_pev SET id_estado = 8 WHERE num_cili_pev = '$num_cili3'";
										}else if($lavado_especial_u3 == 1){
											$consulta12 = "UPDATE cilindro_pev SET id_estado = 12 WHERE num_cili_pev = '$num_cili3'";
										}

										mysqli_query($con,$consulta12);
									}
									?>
									<script type="text/javascript">
										var id_transporte_pev = <?php echo $id_transporte_pev; ?>;
										alert("Los servicios han sido actualizados exitosamente. Presione aceptar para continuar");
										window.location = ("busqueda_ordenes_pev.php");
									</script>
									<?php
								}
							}
							else
							{
								?>
								<script type="text/javascript">
									alert("Debe seleccionar al menos un elemento de la lista");
								</script>
								<?php
							}
				        }
				    }
    			}
    		}
    	}
    }
    

?>
<div id="main" role="main">
	<div id="content">
		<section id="widget-grid" class="">
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-10">
					<div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h6>Cilindros que pertenecen a la orden </h6>				
						</header>
						<div>
						<form id="form1" name="form1" class="smart-form" action="servicios_pev.php" method="POST">
						<input type="hidden" name="id_transporte_pev" value="<?php echo isset($_REQUEST['id_transporte_pev']) ? $_REQUEST['id_transporte_pev'] : NULL; ?>">
						<input type="hidden" name="orden_pev" value="<?php echo isset($_REQUEST['orden_pev']) ? $_REQUEST['orden_pev'] : NULL; ?>">
							<fieldset>
								<legend>Empresa: <?php echo $nombre; ?> | CME: <?php echo $num_trans; ?> | Fecha Transporte:<?php echo $fecha; ?></legend>
								<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">

										<thead>
											<tr>
												<th>#</th>
					                            <th>Cilindro Serial</th>
					                            <th>Material del cilindro</th>
					                            <th>Tipo de Gas</th>
					                            <th>PEV-IVA</th>
					                            <th>Llenado</th>
					                            <th>Pintura</th>
					                            <th>Cambio Válvula</th>
					                            <th>Granallado</th>
					                            <th>Cambio Servicio</th>
					                            <th>Lavado Especial</th>						                            
					                            <th>Observaciones</th>
											</tr>
										</thead>
										<tbody>													
										  <?php
				                          $contador = "0";	                
				                          $consulta1 = "SELECT * FROM has_movimiento_cilindro_pev WHERE id_transporte_pev = '".$id_transporte_pev."'";
				                          $resultado1 = mysqli_query($con,$consulta1);
				                          while ($linea1 = mysqli_fetch_array($resultado1))
				                          {
				                            $contador = $contador + 1;
				                            $id_has_movimiento_cilindro_pev = $linea1["id_has_movimiento_cilindro_pev"];
				                            $id_movimiento_pev = $linea1["id_movimiento_pev"];
				                            $num_cili = $linea1["num_cili"];
				                            $num_cili2 = $linea1["num_cili2"];
				                            $material_cilindro_u = $linea1["material_cilindro_u"];

				                            $id_tapa = $linea1["id_tapa"];
				                            $id_tipo_gas_pev = $linea1["id_tipo_gas_pev"];
				                            $id_cliente = $linea1["id_cliente"];
				                            $observacion_u = $linea1["observacion_u"];
				                            $ph_u = $linea1['ph_u'] ;
				                            $llenado_u = $linea1['llenado_u'];
				                            $pintura_u = $linea1['pintura_u'];
				                            $valvula_u = $linea1['valvula_u'];
				                            $granallado_u = $linea1['granallado_u'];
				                            $cambio_serv_u = $linea1['cambio_serv_u'];
				                            $lavado_especial_u = $linea1['lavado_especial_u'];

				                            $consulta2 = "SELECT nombre FROM tipo_gas_pev WHERE id_tipo_gas_pev = '".$id_tipo_gas_pev."'";
				                            $resultado2 = mysqli_query($con,$consulta2) ;
				                            if(mysqli_num_rows($resultado2) > 0)
				                            {
				                            	$linea2 = mysqli_fetch_array($resultado2);
				                            	$nombre_gas = $linea2["nombre"];
				                            }

				                            $consulta10 = "SELECT id_cilindro_pev, id_estado FROM cilindro_pev WHERE num_cili_pev = '$num_cili'";
				                            $resultado10 = mysqli_query($con,$consulta10);
				                            $linea10 = mysqli_fetch_assoc($resultado10);
 
				                            $id_cilindro_pev = isset($linea10["id_cilindro_pev"]) ? $linea10["id_cilindro_pev"] : NULL;
				                            $id_estado = isset($linea10["id_estado"]) ? $linea10["id_estado"] : NULL;
				                            

				                            ?>
				                            <tr>
				                              	<td width="10"><?php echo $contador; ?></td>
				                              	<td width="30"><?php echo $num_cili; ?></td>
				                              	<td width="10">
				                              		<?php

														$consulta7 = "SELECT * FROM material_cilindro";
							                            $resultado7 = mysqli_query($con,$consulta7) ;		
														
														echo "<select name='servicio[]' onclick='validar_material(this.value)'";
														echo "<option value='0'>Seleccione...</option>";
														while($linea7 = mysqli_fetch_array($resultado7))
					                                    {
					                                      	$id_material = $linea7['id_material'];
					                                      	$nombre = $linea7['nombre'];
					                                      	if($id_material == $material_cilindro_u)
					                                      	{
					                                      		echo "<option value='$id_material'  selected>$nombre</option>";
					                                      	}
					                                      	else
					                                      	{
					                                      		echo "<option value='$id_material'  >$nombre</option>";
					                                      	}
					                                    }//fin while
														echo "</select>";
														echo "<i></i>";
													?>
				                              	</td>
				                              	<td><?php echo $nombre_gas; ?></td>
				                              	<td align="center" width="40">
				                              		<input type="checkbox" value="<?php echo $id_has_movimiento_cilindro_pev."~ph"; ?>" name="opcion[]" id="opc_1" <?php if($ph_u != 0){echo 'checked="checked"';} ?>>
				                              	</td>
				                              	<td align="center" width="40">
				                              		<input type="checkbox" value="<?php echo $id_has_movimiento_cilindro_pev."~llenado"; ?>" name="opcion[]" id="opc_2" <?php if($llenado_u != 0){echo 'checked="checked"';} ?>>
				                              	</td>
				                              	<td align="center" width="40">
				                              		<input type="checkbox" value="<?php echo $id_has_movimiento_cilindro_pev."~pintura"; ?>" name="opcion[]" id="opc_3" <?php if($pintura_u != 0){echo 'checked="checked"';} ?>>
				                              	</td>
				                              	<td align="center" width="40">
				                              		<input type="checkbox" value="<?php echo $id_has_movimiento_cilindro_pev."~valvula"; ?>" name="opcion[]" id="opc_4" <?php if($valvula_u != 0){echo 'checked="checked"';} ?>>
				                              	</td>
				                              	<td align="center" width="40">
				                              		<input type="checkbox" value="<?php echo $id_has_movimiento_cilindro_pev."~granallado"; ?>" name="opcion[]" id="opc_5" <?php if($granallado_u != 0){echo 'checked="checked"';} ?>>
				                              	</td>
				                              	<td align="center" width="40">
				                              		<input type="checkbox" value="<?php echo $id_has_movimiento_cilindro_pev."~cambio_serv"; ?>" name="opcion[]" id="opc_6" <?php if($cambio_serv_u != 0){echo 'checked="checked"';} ?>>
				                              	</td>
				                              	<td align="center" width="40">
				                              		<input type="checkbox" value="<?php echo $id_has_movimiento_cilindro_pev."~lavado_especial"; ?>" name="opcion[]" id="opc_7" <?php if($lavado_especial_u != 0){echo 'checked="checked"';} ?>>
				                              	</td>
				                              	<td width="8" style="vertical-align:bottom;" align="center">
				                              		<input type="text" size="19" name="observacion_u[]" value="<?php echo isset($observacion_u) ? $observacion_u :NULL; ?>">
				                              		<input type="hidden" name="id_has[]" value="<?php echo isset($id_has_movimiento_cilindro_pev) ? $id_has_movimiento_cilindro_pev :NULL; ?>">
				                              	</td>
				                           	</tr>                           
				                            <?php
				                            }mysqli_free_result($resultado1);	                                            
				                  		    ?>
										</tbody>
								</table>
							</fieldset>
							<footer>
								<input type="submit" name="guardar_servicios" value="GUARDAR SERVICIOS" class="btn btn-primary">
							</footer>
						</form>
						</div>
					</div>
				</article>
			</div>
		</section>
	</div>
</div>
<script type="text/javascript">
	function validar_material(id_material){

		var material = id_material;

		if(material == 2){

			document.getElementById("opc_5").disabled = true;

		}else{

			document.getElementById("opc_5").disabled = false;

		}

	}
</script>
<!-- <script type="text/javascript">
	function confirmar()
	{
		var id = document.getElementById("opc_1").value;
		//var numberChecked = $('input:checkbox:checked').length;
		alert("el valor es:"+id);
		/*if ($("#opc_1:checked").length)
		{       
		    
		} 
		else 
		{
		    if($("#opc_2:checked").length)
		    {
		    	
		    }
		    else
		    {
		    	if($("#opc_3:checked").length)
		    	{
		    		
		    	}
		    	else
		    	{
		    		if($("#opc_4:checked").length )
					{
						submit.form1();
					}
					else
					{
						alert("Debe seleccionar al menos un servicio para cada cilindro");
						return false;
					}
		    	}
		    }
		}*/
	}
</script> -->
<?php
	include("inc/footer.php");
	include("inc/scripts.php"); 
?>
<?php 
	//include footer
	include("inc/google-analytics.php"); 
?>
<?php
}
else
{
    header("Location:index.php");
}
?>
