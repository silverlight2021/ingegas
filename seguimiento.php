<?php
require ("libraries/conexion.php");
require ("libraries/seguimiento.php");
session_start();
if(@$_SESSION['logged']== 'yes')
{ 
	$id_user=$_SESSION['su'];
    $acc = $_SESSION['acc'];
    $id_cilindro_eto = isset($_REQUEST['id_cilindro_eto']) ? $_REQUEST['id_cilindro_eto'] : NULL;

	require_once("inc/init.php");
	require_once("inc/config.ui.php");
	$page_title = "Seguimiento";
	$page_css[] = "your_style.css";
	include("inc/header.php");
	$page_nav[""][""][""][""] = true;
	include("inc/nav.php");
	$id_num_cilindro1 = "";
?>
<!-- MAIN PANEL -->
<!-- Modal -->

<div class="modal fade" id="modal_ph" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">	
	<div class="modal-dialog">
		<form name="mezcla" action="ph.php" method="POST" id="mezcla" >
			<input type="hidden" name="id_cilindro_eto_1"  id="id_cilindro_eto_1" value="">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					&times;
					</button>
					<h4 class="modal-title" id="myModalLabel">Prueba Hidrostatica</h4>
				</div>
				<div class="modal-body">
					<div class="well well-sm well-primary">
						<div class="row">
							<div class="col-md-8">
								<div class="form-group">
									<label for="category">Usuario :</label>
									<input type="text" class="form-control" placeholder="Usuario" name="usuario_ph" readonly required value=""/>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="category">Fecha :</label>
									<input type="text" class="form-control" placeholder="Fecha" name="fecha_ph" readonly value=""/>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
						<input type="submit" value="Guardar" name="ph_cilindro" id="ph_cilindro" class="btn btn-primary" />
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</form>
	</div><!-- /.modal -->
	<!-- Fin Modal -->
</div>
<div class="modal fade" id="modal_lavar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">	
	<div class="modal-dialog">
		<form name="mezcla" action="lavado_eto.php" method="POST" id="mezcla" >
			<input type="hidden" name="id_cilindro_eto_1"  id="id_cilindro_eto_1" value="">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					&times;
					</button>
					<h4 class="modal-title" id="myModalLabel">Lavado de Cilindros</h4>
				</div>
				<div class="modal-body">
					<div class="well well-sm well-primary">
						<div class="row">
							<div class="col-md-8">
								<div class="form-group">
									<label for="category">Usuario :</label>
									<input type="text" class="form-control" placeholder="Usuario" name="usuario" readonly required value=""/>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="category">Fecha :</label>
									<input type="text" class="form-control" placeholder="Fecha" name="fecha_h" readonly value=""/>
								</div>
							</div>
						</div>
					</div>
					<div class="well well-sm well-primary">	
					<label for="category">Cambio de Valvulas</label>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<input type="text" class="form-control" placeholder="" name="cambio" readonly value=""/>
								</div>
							</div>
						</div>						
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</form>
	</div><!-- /.modal -->
	<!-- Fin Modal -->
</div>
<!-- Modal -->
<div class="modal fade" id="modal_orden1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">
	<form name="mezcla" action="mezcla.php" method="POST" id="mezcla">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" id ="cerrar"aria-hidden="true">
					&times;
					</button>
					<h4 class="modal-title" id="myModalLabel">Orden</h4>
				</div>
				<div class="modal-body">
				<div class="well well-sm well-primary">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="category">Numero Cilindro :</label>
								<input type="text" class="form-control" placeholder="Numero Cilindro" name="num_cili_eto" readonly required />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="category">Prueba Hidrostatica :</label>
								<input type="date" class="form-control" placeholder="Prueba Hidrostatica" name="prue_hidro" readonly required />
							</div>
						</div>
					</div>
				</div>
				<div class="well well-sm well-primary">	
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label for="category">Presión Inicial :</label>
								<input type="num" class="form-control" placeholder="Presión Inicial" name="pre_inicial" readonly required />
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="category">Gas Evacuado :</label>
								<input type="text" class="form-control" placeholder="Gas Evacuado " name="gas_eva" readonly required />
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="category">Peso Inicial (Kg):</label>
								<input type="num" class="form-control" placeholder="Peso Inicial (Kg)" name="peso_inicial" readonly required />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="category">Peso Final (Kg) :</label>
								<input type="num" class="form-control" placeholder="Peso Final (Kg)" onKeyUp="igualar()" name="peso_final" required />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="category">Presion de Vacio (in Hg):</label>
								<input type="num" class="form-control" placeholder="Presion de Vacio (in Hg)" name="pre_vacio" readonly required />
							</div>
						</div>
					</div>
				</div>
				<div class="well well-sm well-primary">	
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label for="category">Lote Produccion :</label>
								<input type="text" class="form-control" placeholder="Lote Produccion" name="lote_pro" readonly required />
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="category">Nombre Mezcla :</label>
								<input type="text" class="form-control" placeholder="Nombre Mezcla" name="nombre_mezcla" readonly required />
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="category">Tara Vacio (Kg) :</label>
								<input type="text" class="form-control" placeholder="Tara Vacio (Kg)" name="tara_vacio" readonly required />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="category">Peso Esperado (Kg):</label>
								<input type="text" class="form-control" placeholder="Peso Esperado (Kg)" name="peso_esperado" readonly required />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="category">Presion Inicial (in Hg) :</label>
								<input type="text" class="form-control" placeholder="Presion Inicial (in Hg)" name="pre_inicial_1" required />
							</div>
						</div>
					</div>
				</div>
				<div class="well well-sm well-primary">	
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="category">Esperado ETO:</label>
								<input type="text" class="form-control" placeholder="Esperado" name="esperado_eto" readonly required />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="category">Real ETO:</label>
								<input type="text" class="form-control" placeholder="Real" name="real_eto" required />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="category">Esperado CO2:</label>
								<input type="text" class="form-control" placeholder="Esperado" name="esperado_co" readonly required />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="category">Real CO2:</label>
								<input type="text" class="form-control" placeholder="Real" name="real_co" required />
							</div>
						</div>
					</div>
				</div>
				<div class="well well-sm well-primary">	
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label for="category">Peso Final (Kg) :</label>
								<input type="text" class="form-control" placeholder="Peso Final (Kg)" name="peso_final_1" readonly required />
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="category">Desviacion (%) :</label>
								<input type="text" class="form-control" placeholder="Desviacion (%) " name="desviacion" readonly required />
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="category">Presión Final (Psi) :</label>
								<input type="text" class="form-control" placeholder="Presión Final (Psi)" name="pre_final" required />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<?php
									$consulta7 ="SELECT * FROM lote_co2";
									$resultado7 = mysqli_query($con,$consulta7) ;
								?>	
								<label for="category">Lote CO2 :</label>
								<select class="form-control" name='id_num_cilindro1' >
								<option value='0'>Seleccione...</option>
								<?php
								while($linea7 = mysqli_fetch_array($resultado7))
								{
									$id_num_cilindro = $linea7['id_num_cilindro'];
									$num_cilindro_co2 = $linea7['num_cilindro_co2'];
									if ($id_num_cilindro==$id_num_cilindro1)
									{
										echo "<option value='$id_num_cilindro' selected >$num_cilindro_co2</option>"; 
									}
									else 
									{
										echo "<option value='$id_num_cilindro'>$num_cilindro_co2</option>"; 
									} 
								}//fin while 
								?>
								</select>								
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<?php
									$consulta7 ="SELECT * FROM lote_eto";
									$resultado7 = mysqli_query($con,$consulta7) ;
								?>	
								<label for="category">Cilindro ETO :</label>
								<select class="form-control" name='id_lote_eto1' >
								<option value='0'>Seleccione...</option>
								<?php
								while($linea7 = mysqli_fetch_array($resultado7))
								{
									$id_lote_eto = $linea7['id_lote_eto'];
									$num_cilindro_eto = $linea7['num_cilindro_eto'];
									$id_lote_eto1 = $id_lote_eto;
									if ($id_lote_eto==$id_lote_eto1)
									{
										echo "<option value='$id_lote_eto' selected >$num_cilindro_eto</option>"; 
									}
									else 
									{
										echo "<option value='$id_lote_eto'>$num_cilindro_eto</option>"; 
									} 
								}//fin while 
								?>
								</select>								
							</div>
						</div>						
					</div>
					<div class="row">							
						<div class="col-md-6">
							<div class="form-group">
								<label for="category">Vencimiento Mezcla :</label>
								<input type="date" class="form-control" placeholder="Vencimiento Mezcla" name="fecha_ven_mezcla" readonly required value="<?php echo isset($fecha_ven) ? $fecha_ven : NULL; ?>">
							</div>
						</div>						
					</div>					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" id="Cancelar" data-dismiss="modal">Cancelar</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</form>
</div><!-- /.modal -->
</div><!-- /.modal -->
<!-- Fin Modal -->
<div class="modal fade" id="modal_orden" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">	
	<div class="modal-dialog">
		<form name="mezcla" action="revision_preliminar.php" method="POST" id="mezcla" >
			<input type="hidden" name="id_cilindro_eto"  value="id_cilindro_eto">
			<input type="hidden" name="id_estado"  value="id_estado">
			<input type="hidden" name="id_orden" id="id_orden" value="id_orden">
			<input type="hidden" name="id_pre_produccion_mezcla" id="id_pre_produccion_mezcla" value="<?php echo isset($$id_pre_produccion_mezcla) ? $$id_pre_produccion_mezcla : NULL; ?>">
			<input type="hidden" name="fecha_lav_eto" value="fecha_lav_eto">

			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					&times;
					</button>
					<h4 class="modal-title" id="myModalLabel">Evacuación y Vacío de Cilindros</h4>
				</div>
				<div class="modal-body">
					<div class="well well-sm well-primary">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="category">Numero Cilindro :</label>
									<input type="text" class="form-control" placeholder="Numero Cilindro" name="num_cili_eto" readonly required />
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="category">Prueba Hidrostatica :</label>
									<input type="date" class="form-control" placeholder="Prueba Hidrostatica" name="prue_hidro" readonly required />
								</div>
							</div>
						</div>
					</div>
					<div class="well well-sm well-primary">	
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="category">Presión Inicial Llegada:</label>
									<input type="num" class="form-control" placeholder="Presión Inicial Llegada" name="pre_inicial" required />
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="category">Gas Evacuado :</label>
									<input type="text" class="form-control" placeholder="Presión Inicial Llegada" name="gas_eva" readonly required />
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="category">Peso Inicial Llegada (Kg):</label>
									<input type="num" class="form-control" placeholder="Peso Inicial (Kg)" onKeyUp='resta()' name="peso_inicial" required />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="category">Tara de Vacío (Kg) :</label>
									<input type="num" class="form-control" placeholder="Tara de Vacío (Kg)" onKeyUp='resta()' name="peso_final"  />
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="category">Presion de Vacio (Psi):</label>
									<input type="num" class="form-control" placeholder="Presion de Vacio (Psi)" name="pre_vacio"  />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="category">Cantidad a Evacuar :</label>
									<input type="num" class="form-control" placeholder="Cantidad a Evacuar" name="g_evacuar" readonly  />
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</form>
	</div><!-- /.modal -->
	<!-- Fin Modal -->
</div>
<div id="main" role="main">
<?php
if (in_array(12, $acc))
{
?>
	<div id="content">
		<section id="widget-grid" class="">
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-6">
					<div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">	
						<header>
							<span class="widget-icon"> <i class="fa fa-table"></i> </span>
							<h2>Seguimiento</h2>				
						</header>
						<div>
							<div class="jarviswidget-editbox"></div>
							<div class="widget-body no-padding">
								<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
									<form action="busqueda_cilindros_eto.php" method="POST" name="form1">
										<thead>
											<tr>
												<th>#</th>
												<th>Número Cilindro</th>
												<th>Seguimiento</th>
												<th>Usuario</th>
												<th>Fecha</th>
												<th>Detalle</th>										
											</tr>
										</thead>
										<tbody>
											<?php
			                                    
				                    			$contador = "0";
				                    			$contador_pev = "0";
				                    	        $consulta = "SELECT * FROM seguimiento_cilindro WHERE id_cilindro_eto = $id_cilindro_eto ORDER BY fecha_hora DESC ";
				                                $resultado = mysqli_query($con,$consulta) ;
				                                while ($linea = mysqli_fetch_array($resultado))
				                                {	                                    
				                                    $contador = $contador + 1;
				                                    $id_cilindro_eto = $linea["id_cilindro_eto"];
				                                    $id_seguimiento = $linea["id_seguimiento"];
				                                    $id_user = $linea["id_user"];
				                                    $id_proceso = $linea["id_proceso"];
				                                    $id_has_proceso = $linea["id_has_proceso"];
				                                    $fecha_hora = $linea["fecha_hora"];
				                                    $id_seguimiento_cilindro = $linea["id_seguimiento_cilindro"];

				                                    $consulta1 = "SELECT num_cili_eto FROM cilindro_eto WHERE id_cilindro_eto = $id_cilindro_eto";
				                                    $resultado1 = mysqli_query($con,$consulta1) ;
				                                    while ($linea4 = mysqli_fetch_array($resultado1))
				                                    {
				                                    	$num_cili_eto = $linea4["num_cili_eto"];		                                                	
				                                    }mysqli_free_result($resultado1);

				                                    $consulta3 = "SELECT * FROM seguimiento WHERE id_seguimiento =".$id_seguimiento;
				                                    $resultado3 = mysqli_query($con,$consulta3) ;
				                                    while ($linea3 = mysqli_fetch_array($resultado3))
				                                    {
				                                    	$seguimiento = $linea3["seguimiento"];	                                    	
				                                    }mysqli_free_result($resultado3);

				                                    $consulta4 = "SELECT * FROM user WHERE idUser = $id_user";
				                                    $resultado4 = mysqli_query($con,$consulta4) ;
				                                    while ($linea2 = mysqli_fetch_array($resultado4))
				                                    {
				                                    	$Name = $linea2["Name"];
				                                    	$LastName = $linea2["LastName"];		
				                                    	$usuario = $Name." ".$LastName;                                             	
				                                    }mysqli_free_result($resultado4);
			                         
				                                ?>
			                                    <tr>
			                                        <td width="5"><?php echo $contador; ?></td>
			                                        <td><?php echo $num_cili_eto; ?></td>
			                                        <td><?php echo $seguimiento; ?></td>
			                                        <td><?php echo $usuario; ?></td>
			                                        <td><?php echo $fecha_hora; ?></td>        
			                                        <td><input type="image" onclick="editar(<?php echo $id_seguimiento; ?> , <?php echo $id_cilindro_eto; ?>, <?php echo $id_has_proceso; ?>, <?php echo $id_seguimiento_cilindro; ?>)" src="img/lupa.png" width="20" height="20"></td>                                               
			                                	</tr>    
			                                <?php
			                                }
			                                mysqli_free_result($resultado);   
			                            	
			                            	?>
			                            	<?php
			                            		if($contador == 0){

			                            			$consulta8 = "SELECT * FROM seguimiento_pev WHERE id_cilindro_eto = $id_cilindro_eto ORDER BY fecha_hora DESC";
			                            			$resultado8 = mysqli_query($con,$consulta8);

			                            			if(mysqli_num_rows($resultado8)){

			                            				while ($linea8 = mysqli_fetch_assoc($resultado8)) {
			                            					
			                            					$contador_pev++;

			                            					$id_cilindro_pev = isset($linea8["id_cilindro_eto"]) ? $linea8["id_cilindro_eto"] : NULL;
			                            					$id_seguimiento_pev = isset($linea8["id_seguimiento"]) ? $linea8["id_seguimiento"] : NULL;
			                            					$id_user = isset($linea8["id_user"]) ? $linea8["id_user"] : NULL;
			                            					$id_proceso_pev = isset($linea8["id_proceso"]) ? $linea8["id_proceso"] : NULL;
			                            					$id_has_proceso_pev = isset($linea8["id_has_proceso"]) ? $linea8["id_has_proceso"] : NULL;
			                            					$fecha_hora_pev = isset($linea8["fecha_hora"]) ? $linea8["fecha_hora"] : NULL;
			                            					$id_seguimiento_cilindro_pev = isset($linea8["id_seguimiento_pev"]) ? $linea8["id_seguimiento_pev"] : NULL;

			                            					$consulta9 = "SELECT num_cili_eto FROM cilindro_eto WHERE id_cilindro_eto = $id_cilindro_pev";
			                            					$resultado9 = mysqli_query($con,$consulta9);

			                            					while ($linea9 = mysqli_fetch_assoc($resultado9)) {
			                            						$num_cili_pev = isset($linea9["num_cili_eto"]) ? $linea9["num_cili_eto"] : NULL;
			                            					}mysqli_free_result($resultado9);

			                            					$consulta10 = "SELECT seguimiento FROM seguimiento WHERE id_seguimiento = '$id_seguimiento_pev'";
			                            					$resultado10 = mysqli_query($con,$consulta10);

			                            					while ($linea10 = mysqli_fetch_assoc($resultado10)) {
			                            						$seguimiento_pev = isset($linea10["seguimiento"]) ? $linea10["seguimiento"] : NULL;
			                            					}mysqli_free_result($resultado10);

			                            					$consulta11 = "SELECT * FROM user WHERE idUser = '$id_user'";
			                            					$resultado11 = mysqli_query($con,$consulta11);

			                            					while ($linea11 = mysqli_fetch_assoc($resultado11)) {
			                            						$Name = $linea11["Name"];
						                                    	$LastName = $linea11["LastName"];		
						                                    	$usuario = $Name." ".$LastName;
			                            					}mysqli_free_result($resultado11);

			                            					?>
			                            					<tr>
						                                        <td width="5"><?php echo $contador_pev; ?></td>
						                                        <td><?php echo $num_cili_pev; ?></td>
						                                        <td><?php echo $seguimiento_pev; ?></td>
						                                        <td><?php echo $usuario; ?></td>
						                                        <td><?php echo $fecha_hora_pev; ?></td>        
						                                        <td><input type="image" onclick="editar_pev(<?php echo $id_seguimiento; ?> , <?php echo $id_cilindro_pev; ?>, <?php echo $id_has_proceso_pev; ?>, <?php echo $id_seguimiento_cilindro_pev; ?>)" src="img/lupa.png" width="20" height="20"></td>                                               
						                                	</tr>
						                                	<?php
			                            				}
			                            			}
			                            			
			                            		}
			                            	?>
										</tbody>
									</form>
								</table>
							</div>
						</div>
					</div>
				</article>
			</div>
		</section>
	</div>
	<!-- Modal no conformidad-->
<div class="modal fade" id="modal_conformidad" role="dialog">
    <form id="tracking_client" method="POST">
        <input type="hidden" name="id_cilindro_eto_2"  id="id_cilindro_eto_2"  value="id_cilindro_eto_2">
        <div class="modal-dialog">
          	<div class="modal-content">
            	<div class="modal-header">
              		<button type="button" class="close" data-dismiss="modal">&times;</button>
              		<h4 class="modal-title">Seguimiento Conformidad Cilindro</h4>
            	</div>
            	<div style="overflow-y: scroll;height: 200px; ">          
              		<div class='modal-body' id="id02"></div>
            	</div>
            	<div class="modal-footer">
              		<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              		<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="guardar_trk_client()">Guardar</button>
            	</div>
          	</div>

        </div>
    </form>
</div>
<!-- Fin Modal no conformidad--> 
	<!-- END MAIN CONTENT -->
<?php
}										
?>

</div>
<!-- ==========================CONTENT ENDS HERE ========================== -->
<?php
	include("inc/footer.php");
	include("inc/scripts.php"); 
?>

<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<script>
function editar(id_seguimiento,id_cilindro_eto,id_has_proceso,id_seguimiento_cilindro) 
{
	//alert(id_seguimiento);
	if (id_seguimiento==1) 
	{
		//window.location.href="cilindro_eto.php?id_cilindro_eto="+id_cilindro_eto;
		window.open("cilindro_eto.php?id_cilindro_eto="+id_cilindro_eto+"","Cilindro","width=auto,height=auto,menubar=no")
	}
	if (id_seguimiento==2) 
	{
		//window.location.href="cilindro_eto.php?id_cilindro_eto="+id_cilindro_eto;
		window.open("cilindro_eto.php?id_cilindro_eto="+id_cilindro_eto+"","Cilindro","width=auto,height=auto,menubar=no") 
	}
	if (id_seguimiento==3) 
	{
		$("#modal_orden").modal();
		$.ajax({
			url: 'edit_cili3.php',
			type: 'POST',
			data: 'dato='+id_cilindro_eto,
		})
		.done(function(data) {
			var objeto = JSON.parse(data);							
			$("input[name=id_pre_produccion_mezcla]").val(objeto.id);
			$("input[name=id_cilindro_eto]").val(objeto.id_cilindro_eto);			
			$("input[name=num_cili_eto]").val(objeto.id_cilindro);
			$("input[name=prue_hidro]").val(objeto.prue_hidro);
			$("input[name=pre_inicial]").val(objeto.pre_inicial);
			$("input[name=gas_eva]").val(objeto.gas_eva);
			$("input[name=peso_inicial]").val(objeto.peso_inicial);
			$("input[name=peso_final]").val(objeto.peso_final);
			$("input[name=pre_vacio]").val(objeto.pre_vacio);
			$("input[name=g_evacuar]").val(objeto.g_evacuar);
			$("input[name=fecha_lav_eto]").val(objeto.fecha_lav_eto);					
		})
		.fail(function() {
			console.log("error");
		});
	}
	if (id_seguimiento==4) 
	{
		$("#modal_orden").modal();
		$.ajax({
			url: 'edit_cili3.php',
			type: 'POST',
			data: 'dato='+id_cilindro_eto,
		})
		.done(function(data) {
			var objeto = JSON.parse(data);							
			$("input[name=id_pre_produccion_mezcla]").val(objeto.id);
			$("input[name=id_cilindro_eto]").val(objeto.id_cilindro_eto);			
			$("input[name=num_cili_eto]").val(objeto.id_cilindro);
			$("input[name=prue_hidro]").val(objeto.prue_hidro);
			$("input[name=pre_inicial]").val(objeto.pre_inicial);
			$("input[name=gas_eva]").val(objeto.gas_eva);
			$("input[name=peso_inicial]").val(objeto.peso_inicial);
			$("input[name=peso_final]").val(objeto.peso_final);
			$("input[name=pre_vacio]").val(objeto.pre_vacio);
			$("input[name=g_evacuar]").val(objeto.g_evacuar);
			$("input[name=fecha_lav_eto]").val(objeto.fecha_lav_eto);					
		})
		.fail(function() {
			console.log("error");
		});
	}
	if (id_seguimiento==5) 
	{
		
	}
	if (id_seguimiento==6) 
	{
		//alert(id_has_proceso);
		$("#modal_orden1").modal();
		$.ajax({
			url: 'edit_cili.php',
			type: 'POST',
			data: 'dato='+id_has_proceso,
		})
		.done(function(data) {
			var objeto = JSON.parse(data);							
			$("input[name=id_produccion_mezclas]").val(objeto.id);
			$("input[name=id_cilindro_eto]").val(objeto.id_cilindro_eto);
			$("input[name=id_orden]").val(objeto.id_orden);
			$("input[name=num_cili_eto]").val(objeto.id_cilindro);
			$("input[name=prue_hidro]").val(objeto.prue_hidro);
			$("input[name=pre_inicial]").val(objeto.pre_inicial);
			$("input[name=gas_eva]").val(objeto.gas_eva);
			$("input[name=peso_inicial]").val(objeto.peso_inicial);
			$("input[name=peso_final]").val(objeto.peso_final);
			$("input[name=pre_vacio]").val(objeto.pre_vacio);
			$("input[name=lote_pro]").val(objeto.lote_pro);
			$("input[name=nombre_mezcla]").val(objeto.nombre_mezcla);
			$("input[name=tara_vacio]").val(objeto.tara_vacio);
			$("input[name=peso_esperado]").val(objeto.peso_esperado);
			$("input[name=pre_inicial_1]").val(objeto.pre_inicial_1);
			$("input[name=esperado_eto]").val(objeto.esperado_eto);
			$("input[name=real_eto]").val(objeto.real_eto);
			$("input[name=esperado_co]").val(objeto.esperado_co);
			$("input[name=real_co]").val(objeto.real_co);
			$("input[name=peso_final_1]").val(objeto.peso_final_1);
			$("input[name=desviacion]").val(objeto.desviacion);
			$("input[name=pre_final]").val(objeto.pre_final);
			$("select[name=id_num_cilindro1]").val(objeto.lote_co);
			$("select[name=id_lote_eto1]").val(objeto.cilindro_eto);
			$("input[name=fecha_ven_mezcla]").val(objeto.fecha_ven_mezcla);	

			
			campo=objeto.gas_eva;
			if (campo=="ETO-10") 
			{
				valor= 10;
			}
			if (campo=="ETO-20") 
			{
				valor= 20;
			}
			if (campo=="ETO-90") 
			{
				valor= 90;
			}
			if (campo=="ETO-100") 
			{
				valor= 100;
			}

			var restante = 100-valor;
			var por_valor=valor*(0.01);
			var por_res=restante*(0.01);

		    var form = document.forms.mezcla;
		    form.oninput = function() 
		    {  
		        form.esperado_eto.value = ((parseInt(form.peso_esperado.value))*por_valor).toFixed(2);
		        form.esperado_co.value = ((parseInt(form.peso_esperado.value))*por_res).toFixed(2);
		       // form.tara_vacio.value = form.peso_final.value;
		        form.peso_final_1.value = ((parseFloat(form.real_eto.value))+(parseFloat(form.real_co.value))).toFixed(2);
		        form.desviacion.value = (Math.abs(((parseFloat(form.peso_esperado.value))-(parseFloat(form.peso_final_1.value)))/(parseFloat(form.peso_esperado.value)) *(100))).toFixed(2);
		    }
		})
		.fail(function() {
			console.log("error");
		});
	}	
	if (id_seguimiento==7) 
	{
		//alert(id_has_proceso);
		$.ajax({
			url: 'id_logistica.php',
			type: 'POST',
			data: 'dato='+id_has_proceso,
		})
		.done(function(data) {
			//alert();	
			//window.location.href="logistica.php?id_logistica_eto="+data;	
			//window.open(""logistica.php?id_logistica_eto="+data"); 
			window.open("logistica.php?id_logistica_eto="+data+"","logistica","width=auto,height=auto,menubar=no") 	
		})
		.fail(function() {
			console.log("error");
		});
	}
	if (id_seguimiento==8) 
	{
		$.ajax({
			url: 'id_transporte.php',
			type: 'POST',
			data: 'dato='+id_has_proceso,
		})
		.done(function(data) {
			//alert();	
			//window.location.href="logistica.php?id_logistica_eto="+data;	
			//window.open(""logistica.php?id_logistica_eto="+data"); 
			window.open("transporte_eto.php?id_transporte="+data+"","logistica","width=auto,height=auto,menubar=no") 	
		})
		.fail(function() {
			console.log("error");
		});	
	}
	if (id_seguimiento==9) 
	{
		$("#modal_lavar").modal();
		$("#si_no").prop("checked", true); 
		$.ajax({
			url: 'lavado_cili.php',
			type: 'POST',
			data: 'dato='+id_seguimiento_cilindro,
		})
		.done(function(data) {
			var objeto = JSON.parse(data);							
			$("input[name=usuario]").val(objeto.usuario);
			$("input[name=fecha_h]").val(objeto.fecha_h);
			$("input[name=cambio]").val("Si");
			
		})
		.fail(function() {
			console.log("error");
		});
	}
	if (id_seguimiento==10) 
	{
		$("#modal_lavar").modal();
		//$("#si_no").prop("checked", false); 
		$.ajax({
			url: 'lavado_cili.php',
			type: 'POST',
			data: 'dato='+id_seguimiento_cilindro,
		})
		.done(function(data) {
			var objeto = JSON.parse(data);							
			$("input[name=usuario]").val(objeto.usuario);
			$("input[name=fecha_h]").val(objeto.fecha_h);
			$("input[name=cambio]").val("No");
			
		})
		.fail(function() {
			console.log("error");
		});
	}
	if (id_seguimiento==11) 
	{
		$("#modal_ph").modal();
		$.ajax({
			url: 'lavado_cili.php',
			type: 'POST',
			data: 'dato='+id_seguimiento_cilindro,
		})
		.done(function(data) {
			var objeto = JSON.parse(data);							
			$("input[name=usuario_ph]").val(objeto.usuario);
			$("input[name=fecha_ph]").val(objeto.fecha_h);
			
		})
		.fail(function() {
			console.log("error");
		});
	}
	if (id_seguimiento==12) 
	{
		$("#modal_conformidad").modal();
		$.ajax({
			url: 'id_conformidad_seguimiento.php',
			type: 'POST',
			data: 'dato='+id_has_proceso,
		})
		.done(function(data) {

		var objeto1 = JSON.parse(data);					
		var i;
		var tabla = "<table class='table table-striped table-bordered table-hover' width='100%'><colgroup> <col width='50%'><col width='50%'></colgroup>";

		for(i = 0; i < objeto1.length; i++) {
			tabla += "<tr><td colspan='1'>Fecha : " + 
			objeto1[i].fecha_hora +
			"</td><td colspan='1'>Usuario : " +
			objeto1[i].id_user +
			"</td></tr><tr><td colspan='6'>" +
			objeto1[i].obs_con +
			"</td></tr>";
		}
		tabla += "</table>";

		document.getElementById("id02").innerHTML = tabla;
	})
	.fail(function() {
		console.log("error");
	});
	}
	if (id_seguimiento==13) 
	{
		//alert(id_has_proceso);
		
			window.open("calidad.php?id_orden="+id_has_proceso+"","calidad","width=auto,height=auto,menubar=no") 	
		
	}
	if (id_seguimiento==14) 
	{
		//alert(id_has_proceso);
		
			window.open("calidad.php?id_orden="+id_has_proceso+"","calidad","width=auto,height=auto,menubar=no") 	
		
	}

}
</script>
<script type="text/javascript">
function editar_pev(id_seguimiento,id_cilindro_eto,id_has_proceso,id_seguimiento_cilindro) 
{
	//alert(id_seguimiento);
	if (id_seguimiento==1) 
	{
		//window.location.href="cilindro_eto.php?id_cilindro_eto="+id_cilindro_eto;
		window.open("cilindro_eto.php?id_cilindro_eto="+id_cilindro_eto+"","Cilindro","width=auto,height=auto,menubar=no")
	}
	if (id_seguimiento==2) 
	{
		//window.location.href="cilindro_eto.php?id_cilindro_eto="+id_cilindro_eto;
		window.open("cilindro_eto.php?id_cilindro_eto="+id_cilindro_eto+"","Cilindro","width=auto,height=auto,menubar=no") 
	}
	if (id_seguimiento==3) 
	{
		$("#modal_orden").modal();
		$.ajax({
			url: 'edit_cili3.php',
			type: 'POST',
			data: 'dato='+id_cilindro_eto,
		})
		.done(function(data) {
			var objeto = JSON.parse(data);							
			$("input[name=id_pre_produccion_mezcla]").val(objeto.id);
			$("input[name=id_cilindro_eto]").val(objeto.id_cilindro_eto);			
			$("input[name=num_cili_eto]").val(objeto.id_cilindro);
			$("input[name=prue_hidro]").val(objeto.prue_hidro);
			$("input[name=pre_inicial]").val(objeto.pre_inicial);
			$("input[name=gas_eva]").val(objeto.gas_eva);
			$("input[name=peso_inicial]").val(objeto.peso_inicial);
			$("input[name=peso_final]").val(objeto.peso_final);
			$("input[name=pre_vacio]").val(objeto.pre_vacio);
			$("input[name=g_evacuar]").val(objeto.g_evacuar);
			$("input[name=fecha_lav_eto]").val(objeto.fecha_lav_eto);					
		})
		.fail(function() {
			console.log("error");
		});
	}
	if (id_seguimiento==4) 
	{
		$("#modal_orden").modal();
		$.ajax({
			url: 'edit_cili3.php',
			type: 'POST',
			data: 'dato='+id_cilindro_eto,
		})
		.done(function(data) {
			var objeto = JSON.parse(data);							
			$("input[name=id_pre_produccion_mezcla]").val(objeto.id);
			$("input[name=id_cilindro_eto]").val(objeto.id_cilindro_eto);			
			$("input[name=num_cili_eto]").val(objeto.id_cilindro);
			$("input[name=prue_hidro]").val(objeto.prue_hidro);
			$("input[name=pre_inicial]").val(objeto.pre_inicial);
			$("input[name=gas_eva]").val(objeto.gas_eva);
			$("input[name=peso_inicial]").val(objeto.peso_inicial);
			$("input[name=peso_final]").val(objeto.peso_final);
			$("input[name=pre_vacio]").val(objeto.pre_vacio);
			$("input[name=g_evacuar]").val(objeto.g_evacuar);
			$("input[name=fecha_lav_eto]").val(objeto.fecha_lav_eto);					
		})
		.fail(function() {
			console.log("error");
		});
	}
	if (id_seguimiento==5) 
	{
		
	}
	if (id_seguimiento==6) 
	{
		//alert(id_has_proceso);
		$("#modal_orden1").modal();
		$.ajax({
			url: 'edit_cili.php',
			type: 'POST',
			data: 'dato='+id_has_proceso,
		})
		.done(function(data) {
			var objeto = JSON.parse(data);							
			$("input[name=id_produccion_mezclas]").val(objeto.id);
			$("input[name=id_cilindro_eto]").val(objeto.id_cilindro_eto);
			$("input[name=id_orden]").val(objeto.id_orden);
			$("input[name=num_cili_eto]").val(objeto.id_cilindro);
			$("input[name=prue_hidro]").val(objeto.prue_hidro);
			$("input[name=pre_inicial]").val(objeto.pre_inicial);
			$("input[name=gas_eva]").val(objeto.gas_eva);
			$("input[name=peso_inicial]").val(objeto.peso_inicial);
			$("input[name=peso_final]").val(objeto.peso_final);
			$("input[name=pre_vacio]").val(objeto.pre_vacio);
			$("input[name=lote_pro]").val(objeto.lote_pro);
			$("input[name=nombre_mezcla]").val(objeto.nombre_mezcla);
			$("input[name=tara_vacio]").val(objeto.tara_vacio);
			$("input[name=peso_esperado]").val(objeto.peso_esperado);
			$("input[name=pre_inicial_1]").val(objeto.pre_inicial_1);
			$("input[name=esperado_eto]").val(objeto.esperado_eto);
			$("input[name=real_eto]").val(objeto.real_eto);
			$("input[name=esperado_co]").val(objeto.esperado_co);
			$("input[name=real_co]").val(objeto.real_co);
			$("input[name=peso_final_1]").val(objeto.peso_final_1);
			$("input[name=desviacion]").val(objeto.desviacion);
			$("input[name=pre_final]").val(objeto.pre_final);
			$("select[name=id_num_cilindro1]").val(objeto.lote_co);
			$("select[name=id_lote_eto1]").val(objeto.cilindro_eto);
			$("input[name=fecha_ven_mezcla]").val(objeto.fecha_ven_mezcla);	

			
			campo=objeto.gas_eva;
			if (campo=="ETO-10") 
			{
				valor= 10;
			}
			if (campo=="ETO-20") 
			{
				valor= 20;
			}
			if (campo=="ETO-90") 
			{
				valor= 90;
			}
			if (campo=="ETO-100") 
			{
				valor= 100;
			}

			var restante = 100-valor;
			var por_valor=valor*(0.01);
			var por_res=restante*(0.01);

		    var form = document.forms.mezcla;
		    form.oninput = function() 
		    {  
		        form.esperado_eto.value = ((parseInt(form.peso_esperado.value))*por_valor).toFixed(2);
		        form.esperado_co.value = ((parseInt(form.peso_esperado.value))*por_res).toFixed(2);
		       // form.tara_vacio.value = form.peso_final.value;
		        form.peso_final_1.value = ((parseFloat(form.real_eto.value))+(parseFloat(form.real_co.value))).toFixed(2);
		        form.desviacion.value = (Math.abs(((parseFloat(form.peso_esperado.value))-(parseFloat(form.peso_final_1.value)))/(parseFloat(form.peso_esperado.value)) *(100))).toFixed(2);
		    }
		})
		.fail(function() {
			console.log("error");
		});
	}	
	if (id_seguimiento==7) 
	{
		//alert(id_has_proceso);
		$.ajax({
			url: 'id_logistica.php',
			type: 'POST',
			data: 'dato='+id_has_proceso,
		})
		.done(function(data) {
			//alert();	
			//window.location.href="logistica.php?id_logistica_eto="+data;	
			//window.open(""logistica.php?id_logistica_eto="+data"); 
			window.open("logistica.php?id_logistica_eto="+data+"","logistica","width=auto,height=auto,menubar=no") 	
		})
		.fail(function() {
			console.log("error");
		});
	}
	if (id_seguimiento==8) 
	{
		$.ajax({
			url: 'id_transporte.php',
			type: 'POST',
			data: 'dato='+id_has_proceso,
		})
		.done(function(data) {
			//alert();	
			//window.location.href="logistica.php?id_logistica_eto="+data;	
			//window.open(""logistica.php?id_logistica_eto="+data"); 
			window.open("transporte_eto.php?id_transporte="+data+"","logistica","width=auto,height=auto,menubar=no") 	
		})
		.fail(function() {
			console.log("error");
		});	
	}
	if (id_seguimiento==9) 
	{
		$("#modal_lavar").modal();
		$("#si_no").prop("checked", true); 
		$.ajax({
			url: 'lavado_cili.php',
			type: 'POST',
			data: 'dato='+id_seguimiento_cilindro,
		})
		.done(function(data) {
			var objeto = JSON.parse(data);							
			$("input[name=usuario]").val(objeto.usuario);
			$("input[name=fecha_h]").val(objeto.fecha_h);
			$("input[name=cambio]").val("Si");
			
		})
		.fail(function() {
			console.log("error");
		});
	}
	if (id_seguimiento==10) 
	{
		$("#modal_lavar").modal();
		//$("#si_no").prop("checked", false); 
		$.ajax({
			url: 'lavado_cili.php',
			type: 'POST',
			data: 'dato='+id_seguimiento_cilindro,
		})
		.done(function(data) {
			var objeto = JSON.parse(data);							
			$("input[name=usuario]").val(objeto.usuario);
			$("input[name=fecha_h]").val(objeto.fecha_h);
			$("input[name=cambio]").val("No");
			
		})
		.fail(function() {
			console.log("error");
		});
	}
	if (id_seguimiento==11) 
	{
		$("#modal_ph").modal();
		$.ajax({
			url: 'lavado_cili.php',
			type: 'POST',
			data: 'dato='+id_seguimiento_cilindro,
		})
		.done(function(data) {
			var objeto = JSON.parse(data);							
			$("input[name=usuario_ph]").val(objeto.usuario);
			$("input[name=fecha_ph]").val(objeto.fecha_h);
			
		})
		.fail(function() {
			console.log("error");
		});
	}
	if (id_seguimiento==12) 
	{
		$("#modal_conformidad").modal();
		$.ajax({
			url: 'id_conformidad_seguimiento.php',
			type: 'POST',
			data: 'dato='+id_has_proceso,
		})
		.done(function(data) {

		var objeto1 = JSON.parse(data);					
		var i;
		var tabla = "<table class='table table-striped table-bordered table-hover' width='100%'><colgroup> <col width='50%'><col width='50%'></colgroup>";

		for(i = 0; i < objeto1.length; i++) {
			tabla += "<tr><td colspan='1'>Fecha : " + 
			objeto1[i].fecha_hora +
			"</td><td colspan='1'>Usuario : " +
			objeto1[i].id_user +
			"</td></tr><tr><td colspan='6'>" +
			objeto1[i].obs_con +
			"</td></tr>";
		}
		tabla += "</table>";

		document.getElementById("id02").innerHTML = tabla;
	})
	.fail(function() {
		console.log("error");
	});
	}
	if (id_seguimiento==13) 
	{
		//alert(id_has_proceso);
		
			window.open("calidad.php?id_orden="+id_has_proceso+"","calidad","width=auto,height=auto,menubar=no") 	
		
	}
	if (id_seguimiento==14) 
	{
		//alert(id_has_proceso);
		
			window.open("calidad.php?id_orden="+id_has_proceso+"","calidad","width=auto,height=auto,menubar=no") 	
		
	}

}
</script>
<script type="text/javascript">



// DO NOT REMOVE : GLOBAL FUNCTIONS!

$(document).ready(function() {
	
	/* // DOM Position key index //
		
	l - Length changing (dropdown)
	f - Filtering input (search)
	t - The Table! (datatable)
	i - Information (records)
	p - Pagination (paging)
	r - pRocessing 
	< and > - div elements
	<"#id" and > - div with an id
	<"class" and > - div with a class
	<"#id.class" and > - div with an id and class
	
	Also see: http://legacy.datatables.net/usage/features
	*/	

	/* BASIC ;*/
		var responsiveHelper_dt_basic = undefined;
		var responsiveHelper_datatable_fixed_column = undefined;
		var responsiveHelper_datatable_col_reorder = undefined;
		var responsiveHelper_datatable_tabletools = undefined;
		
		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};

		$('#dt_basic').dataTable({
			"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
			"autoWidth" : true,
			"preDrawCallback" : function() {
				// Initialize the responsive datatables helper once.
				if (!responsiveHelper_dt_basic) {
					responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
				}
			},
			"rowCallback" : function(nRow) {
				responsiveHelper_dt_basic.createExpandIcon(nRow);
			},
			"drawCallback" : function(oSettings) {
				responsiveHelper_dt_basic.respond();
			}
		});

	/* END BASIC */
	
	/* COLUMN FILTER  */
    var otable = $('#datatable_fixed_column').DataTable({
    	//"bFilter": false,
    	//"bInfo": false,
    	//"bLengthChange": false
    	//"bAutoWidth": false,
    	//"bPaginate": false,
    	//"bStateSave": true // saves sort state using localStorage
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_fixed_column) {
				responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_fixed_column.respond();
		}		
	
    });
    
    // custom toolbar
    $("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');
    	   
    // Apply the filter
    $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {
    	
        otable
            .column( $(this).parent().index()+':visible' )
            .search( this.value )
            .draw();
            
    } );
    /* END COLUMN FILTER */   

	/* COLUMN SHOW - HIDE */
	$('#datatable_col_reorder').dataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_col_reorder) {
				responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_col_reorder.respond();
		}			
	});
	
	/* END COLUMN SHOW - HIDE */

	/* TABLETOOLS */
	$('#datatable_tabletools').dataTable({
		
		// Tabletools options: 
		//   https://datatables.net/extensions/tabletools/button_options
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        "oTableTools": {
        	 "aButtons": [
             "copy",
             "csv",
             "xls",
                {
                    "sExtends": "pdf",
                    "sTitle": "SmartAdmin_PDF",
                    "sPdfMessage": "SmartAdmin PDF Export",
                    "sPdfSize": "letter"
                },
             	{
                	"sExtends": "print",
                	"sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>"
            	}
             ],
            "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
        },
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_tabletools) {
				responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_tabletools.respond();
		}
	});
	
	/* END TABLETOOLS */

})

</script>

<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script type="text/javascript">

 
		
		$(document).ready(function() {
			
			// menu
			$("#menu").menu();
		
			/*
			 * AUTO COMPLETE AJAX
			 */
		
			function log(message) {
				$("<div>").text(message).prependTo("#log");
				$("#log").scrollTop(0);
			}
		
			$("#city").autocomplete({
				source : function(request, response) {
					$.ajax({
						url : "http://ws.geonames.org/searchJSON",
						dataType : "jsonp",
						data : {
							featureClass : "P",
							style : "full",
							maxRows : 12,
							name_startsWith : request.term
						},
						success : function(data) {
							response($.map(data.geonames, function(item) {
								return {
									label : item.name + (item.adminName1 ? ", " + item.adminName1 : "") + ", " + item.countryName,
									value : item.name
								}
							}));
						}
					});
				},
				minLength : 2,
				select : function(event, ui) {
					log(ui.item ? "Selected: " + ui.item.label : "Nothing selected, input was " + this.value);
				}
			});
		
			/*
			 * Spinners
			 */
			$("#spinner").spinner();
			$("#spinner-decimal").spinner({
				step : 0.01,
				numberFormat : "n"
			});
		
			$("#spinner-currency").spinner({
				min : 5,
				max : 2500,
				step : 25,
				start : 1000,
				numberFormat : "C"
			});
		
			/*
			 * CONVERT DIALOG TITLE TO HTML
			 * REF: http://stackoverflow.com/questions/14488774/using-html-in-a-dialogs-title-in-jquery-ui-1-10
			 */
			$.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
				_title : function(title) {
					if (!this.options.title) {
						title.html("&#160;");
					} else {
						title.html(this.options.title);
					}
				}
			}));
		
		
			/*
			* DIALOG SIMPLE
			*/
		
			// Dialog click
			$('#dialog_link').click(function() {
				$('#dialog_simple').dialog('open');
				return false;
		
			});
		
			$('#dialog_simple').dialog({
				autoOpen : false,
				width : 600,
				resizable : false,
				modal : true,
				title : "<div class='widget-header'><h4><i class='fa fa-warning'></i> Empty the recycle bin?</h4></div>",
				buttons : [{
					html : "<i class='fa fa-trash-o'></i>&nbsp; Delete all items",
					"class" : "btn btn-danger",
					click : function() {
						$(this).dialog("close");
					}
				}, {
					html : "<i class='fa fa-times'></i>&nbsp; Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
					}
				}]
			});
		
			/*
			* DIALOG HEADER ICON
			*/
		
			// Modal Link
			$('#modal_link').click(function() {
				$('#dialog-message').dialog('open');
				return false;
			});
		
			$("#dialog-message").dialog({
				autoOpen : false,
				modal : true,
				title : "<div class='widget-header'><h4><i class='icon-ok'></i> jQuery UI Dialog</h4></div>",
				buttons : [{
					html : "Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
					}
				}]
		
			});
		
			/*
			 * Remove focus from buttons
			 */
			$('.ui-dialog :button').blur();
		
			/*
			 * Just Tabs
			 */
		
			$('#tabs').tabs();
		
			/*
			 *  Simple tabs adding and removing
			 */
		
			$('#tabs2').tabs();
		
			// Dynamic tabs
			var tabTitle = $("#tab_title"), tabContent = $("#tab_content"), tabTemplate = "<li style='position:relative;'> <span class='air air-top-left delete-tab' style='top:7px; left:7px;'><button class='btn btn-xs font-xs btn-default hover-transparent'><i class='fa fa-times'></i></button></span></span><a href='#{href}'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; #{label}</a></li>", tabCounter = 2;
		
			var tabs = $("#tabs2").tabs();
		
			// modal dialog init: custom buttons and a "close" callback reseting the form inside
			var dialog = $("#addtab").dialog({
				autoOpen : false,
				width : 600,
				resizable : false,
				modal : true,
				buttons : [{
					html : "<i class='fa fa-times'></i>&nbsp; Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
		
					}
				}]
			});
		
			// addTab form: calls addTab function on submit and closes the dialog
			var form = dialog.find("form").submit(function(event) {
				addTab();
				dialog.dialog("close");
				
			});
		
			// actual addTab function: adds new tab using the input from the form above

			
			function addTab() 
			{
				
				var label = tabTitle.val() || "Tab " + tabCounter, id = "tabs-" + tabCounter, li = $(tabTemplate.replace(/#\{href\}/g, "#" + id).replace(/#\{label\}/g, label)), tabContentHtml = tabContent.val() || "Tab " + tabCounter + " content.";
		
				tabs.find(".ui-tabs-nav").append(li);
				tabs.append("<div id='" + id + "'><p>" + tabContentHtml + "</p></div>");
				tabs.tabs("refresh");
				tabCounter++;
		
				// clear fields
				$("#tab_title").val("");
				$("#tab_content").val("");
			}
		
			// addTab button: just opens the dialog
			$("#add_tab").button().click(function() {
				var idd = document.getElementById('num').firstChild.nodeValue;

				
				dialog.dialog("open");
			});
		
			// close icon: removing the tab on click
					
			/*
			* ACCORDION
			*/
			//jquery accordion
			
		     var accordionIcons = {
		         header: "fa fa-plus",    // custom icon class
		         activeHeader: "fa fa-minus" // custom icon class
		     };
		     
			$("#accordion").accordion({
				autoHeight : false,
				heightStyle : "content",
				collapsible : true,
				animate : 300,
				icons: accordionIcons,
				header : "h4",
			})
		
			/*
			 * PROGRESS BAR
			 */
			$("#progressbar").progressbar({
		     	value: 25,
		     	create: function( event, ui ) {
		     		$(this).removeClass("ui-corner-all").addClass('progress').find(">:first-child").removeClass("ui-corner-left").addClass('progress-bar progress-bar-success');
				}
			});			

		})

	</script>
	


<?php 

	include("inc/google-analytics.php"); 


}
else
{
    header("Location:index.php");
}
?>