<?php
date_default_timezone_set("America/Bogota");

require ("libraries/conexion.php");
require ("libraries/seguimiento.php");
session_start();

if(@$_SESSION['logged']== 'yes')
{ 	
	
	$id_pre_produccion_mezcla = isset($_REQUEST['id_pre_produccion_mezcla']) ? $_REQUEST['id_pre_produccion_mezcla'] : NULL;
	$id_cilindro_eto1 = isset($_REQUEST['id_cilindro_eto']) ? $_REQUEST['id_cilindro_eto'] : NULL;
  	$id_orden = isset($_REQUEST['id_orden']) ? $_REQUEST['id_orden'] : NULL;
    $id_user =$_SESSION['su'];
    $acc = $_SESSION['acc'];
    $id_estado1 = '';

	if(isset($_POST['g_cilindro']))
	{
		$id_cilindro_eto1 = $_POST['id_cilindro_eto'];
		$prue_hidro1 = $_POST['prue_hidro'];
		$pre_inicial1 = $_POST['pre_inicial'];
		$peso_inicial1 = $_POST['peso_inicial'];  
		$peso_final1 = $_POST['peso_final'];
		$pre_vacio1 = $_POST['pre_vacio'];
		$g_evacuar1 = $_POST['g_evacuar'];
		$fecha_ult_eto = $prue_hidro1;
		$fecha_lav_eto = $_POST['fecha_lav_eto'];

		$fecha_actual = date("Y/m/d");
		$fecha_act_array = explode('/', $fecha_actual);
		$yyyy = $fecha_act_array[0];//año actual
		$mmmm = $fecha_act_array[1];//mes actual

		$año_cili = explode('/', $fecha_ult_eto);
		$mes_cili = explode('/', $fecha_lav_eto);			
		$año = $año_cili[0];//año prueba hidrostatica
		$mes_año = $mes_cili[0];//año de lavado
		$mes = $mes_cili[0];//mes lavado

		$aux = $mmmm-6;			
		if ($mes_año<$yyyy ) 
		{
			if ($aux<=0) 
			{
				$aux1 = $aux*(-1);
				$mes_mes = 12-$aux1;
				if ($mes<$mes_mes) 
				{
					?>
					<script type="text/javascript">
						alert("Este cilindro se encuentra con la prueba de lavado vencida 1");	
					</script>					
					<?php
					$lavado1 = 1;
				}
				else
				{
					$lavado1 = 2;
				}					
			}
			/*
			{
				?>
				<script type="text/javascript">
					alert("Este cilindro se encuentra con la prueba de lavado vencida 2");	
				</script>					
				<?php
				$lavado1 = 1;
			}*/				
		}elseif ($yyyy==$mes_año && $aux>$mes) 
		{
			?>
			<script type="text/javascript">
				alert("Este cilindro se encuentra con la prueba de lavado vencida 3");	
			</script>					
			<?php
			$lavado1 = 1;			
		}
		else
		{
			$lavado1 = 2;
		}	
		$año_actual = $yyyy-5;
		if ($año<$año_actual) 
		{
			?>
			<script type="text/javascript">
				alert("Este cilindro se encuentra con la prueba de Expansión volumetrica vencida");
			</script>					
			<?php
			$prueba=1;			
		}
		else
		{
			$prueba=2;	
		}	
		///////////////////////////////////
		if ($lavado1==1 && $prueba==1) 
		{
			?>
			<script type="text/javascript">
				alert("A Este Cilindro se le debe realizar Prueba de Expansión Volumetrica");
			</script>					
			<?php
			$id_estado1 = 8;
		}
		if ($lavado1==1 && $prueba==2) 
		{
			?>
			<script type="text/javascript">
				alert("A este cilindro se le debe realizar Lavado");
			</script>					
			<?php
			$id_estado1 = 7;
		}
		
		if ($lavado1==2 && $prueba==1) 
		{
			?>
			<script type="text/javascript">
				alert("A Este Cilindro se le debe realizar Prueba de Expansión Volumetrica");
			</script>
			<?php
			$id_estado1 = 8;
		}
		if ($id_estado1 == 8 || $id_estado1 == 7) 
		{
		}	
		else
		{
			if ($peso_final1>0 && $pre_vacio1>0) 
			{
				$id_estado1 = 2;
			}
			else
			{
				$id_estado1 = 10;
			}
		}	
		//echo $id_estado1;
		//echo $lavado1;
	
      	if($id_pre_produccion_mezcla > 0)
		{

			/*?>
				<script type="text/javascript"> 
		        window.alert(<?php echo $id_pre_produccion_mezcla; ?>)
		    </script>
		  <?php*/

			$consulta= "UPDATE pre_produccion_mezcla SET             
            id_cilindro_eto = '".$id_cilindro_eto1."',
      		id_estado_cilindro = '".$id_estado1."', 	          		
      		pre_inicial = '".$pre_inicial1."', 
      		peso_inicial = '".$peso_inicial1."',
      		peso_final = '".$peso_final1."',
      		pre_vacio = '".$pre_vacio1."',
      		g_evacuar = '".$g_evacuar1."',
      		fecha_pre = '".date("Y/m/d")."'

            WHERE id_pre_produccion_mezcla = $id_pre_produccion_mezcla ";
	    	$resultado = mysqli_query($con,$consulta) ;

		    if ($resultado == FALSE)
		    {
		      	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    }
		    else
		    {
		    	seguimiento(3,$id_cilindro_eto1,$id_user,2,$id_pre_produccion_mezcla);//id_seguimiento,id_cilindro_eto,id_user,id_proceso,id_has_proceso
		    	$consulta= "UPDATE cilindro_eto SET 	                
            	id_estado = $id_estado1   
                WHERE id_cilindro_eto = $id_cilindro_eto1 ";
			    $resultado = mysqli_query($con,$consulta) ;
			    if ($resultado == FALSE)
			    {
			      	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
			    }
			    else
			    {
			    	//header('Location: revision_preliminar.php');
			    	?>
		            <script type="text/javascript"> 
		              window.location ="revision_preliminar.php";
		            </script>
		            <?php
			    }	
		    }
		}
		else
		{ 	
		    $consulta1  = "SELECT * FROM cilindro_eto WHERE id_cilindro_eto= $id_cilindro_eto1";
		  	$resultado1 = mysqli_query($con,$consulta1) ;
		  	$linea1 = mysqli_fetch_array($resultado1);
			$id_estado = isset($linea1["id_estado"]) ? $linea1["id_estado"] : NULL;

			if ($id_estado==7) 
			{

			/*	?>
				<script type="text/javascript"> 
		              window.alert("2.")
		           </script>
		    <?php*/

				$consulta = "INSERT INTO pre_produccion_mezcla(		    			
		          		id_cilindro_eto,
		          		id_estado_cilindro, 		          		
		          		pre_inicial, 		          		 
		          		peso_inicial, 
		          		peso_final,
		          		pre_vacio,
		          		g_evacuar,
		          		fecha_pre) 
		          		VALUES(
		          		'".$id_cilindro_eto1."', 
		          		'".$id_estado."',  
		          		'".$pre_inicial1."',		          		
		          		'".$peso_inicial1."', 
		          		'".$peso_final1."',
		          		'".$pre_vacio1."',
		          		'".$g_evacuar1."',
		          		'".date("Y/m/d")."')";
			    $resultado = mysqli_query($con,$consulta) ;
			    if ($resultado == FALSE)
			    {
			      echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
			    }
			    else
			    {			    	
				    $id_pre_produccion_mezcla22 = mysqli_insert_id($con);
				    seguimiento(4,$id_cilindro_eto1,$id_user,2,$id_pre_produccion_mezcla22);//id_seguimiento,id_cilindro_eto,id_user,id_proceso,id_has_proceso
				    ?>
		            <script type="text/javascript"> 

		              window.location ="revision_preliminar.php";

		            </script>
		            <?php			    	
			    }
			}
			else
			{	

				/*?>
				<script type="text/javascript"> 
		              window.alert("3.")
		           </script>
		    <?php*/

			 	$consulta = "INSERT INTO pre_produccion_mezcla(		    			
		          		id_cilindro_eto,
		          		id_estado_cilindro, 		          		
		          		pre_inicial, 		          		 
		          		peso_inicial, 
		          		peso_final,
		          		pre_vacio,
		          		g_evacuar,
		          		fecha_pre) 
		          		VALUES(
		          		'".$id_cilindro_eto1."', 
		          		'".$id_estado1."',  
		          		'".$pre_inicial1."',		          		
		          		'".$peso_inicial1."', 
		          		'".$peso_final1."',
		          		'".$pre_vacio1."',
		          		'".$g_evacuar1."',
		          		'".date("Y/m/d")."')";
			    $resultado = mysqli_query($con,$consulta) ;
			    if ($resultado == FALSE)
			    {
			      echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
			    }
			    else
			    {
			    	$id_pre_produccion_mezcla22 = mysqli_insert_id($con);
				    seguimiento(4,$id_cilindro_eto1,$id_user,2,$id_pre_produccion_mezcla22);//id_seguimiento,id_cilindro_eto,id_user,id_proceso,id_has_proceso

			    	$consulta= "UPDATE cilindro_eto SET 	                
	            	id_estado = $id_estado1    
	                WHERE id_cilindro_eto = $id_cilindro_eto1 ";
				    $resultado = mysqli_query($con,$consulta) ;

				    if ($resultado == FALSE)
				    {
				      	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
				    }
				    else
				    {
				    	//header('Location: revision_preliminar.php');
				    	?>
		            	<script type="text/javascript"> 
		              		window.location ="revision_preliminar.php";
		            	</script>
		            	<?php
				    }	
			    }
			}
		} 
	}
	require_once("inc/init.php");
	require_once("inc/config.ui.php");
	$page_title = "Revisión Preliminar";
	$page_css[] = "your_style.css";
	include("inc/header.php");
	$page_nav[""][""][""][""] = true;
	include("inc/nav.php");
?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<!-- Modal -->

<div class="modal fade" id="modal_orden" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">	
	<div class="modal-dialog">
		<form name="mezcla" action="revision_preliminar.php" method="POST" id="mezcla" >
			<input type="hidden" name="id_cilindro_eto"  value="id_cilindro_eto">
			<input type="hidden" name="id_estado"  value="id_estado">
			<input type="hidden" name="id_orden" id="id_orden" value="id_orden">
			<input type="hidden" name="id_pre_produccion_mezcla" id="id_pre_produccion_mezcla" value="<?php echo $id_pre_produccion_mezcla; ?>">
			<input type="hidden" name="fecha_lav_eto" value="fecha_lav_eto">

			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					&times;
					</button>
					<h4 class="modal-title" id="myModalLabel">Evacuación y Vacío de Cilindros</h4>
				</div>
				<div class="modal-body">
					<div class="well well-sm well-primary">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="category">Numero Cilindro :</label>
									<input type="text" class="form-control" placeholder="Numero Cilindro" name="num_cili_eto" readonly required />
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="category">Prueba Hidrostatica :</label>
									<input type="date" class="form-control" placeholder="Prueba Hidrostatica" name="prue_hidro" readonly required />
								</div>
							</div>
						</div>
					</div>
					<div class="well well-sm well-primary">	
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="category">Presión Inicial Llegada:</label>
									<input type="num" class="form-control" placeholder="Presión Inicial Llegada" name="pre_inicial" required />
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="category">Gas Evacuado :</label>
									<input type="text" class="form-control" placeholder="Presión Inicial Llegada" name="gas_eva" readonly required />
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="category">Peso Inicial Llegada (Kg):</label>
									<input type="num" class="form-control" placeholder="Peso Inicial (Kg)" onKeyUp='resta()' name="peso_inicial" required />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="category">Tara de Vacío (Kg) :</label>
									<input type="num" class="form-control" placeholder="Tara de Vacío (Kg)" onKeyUp='resta()' name="peso_final"  />
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="category">Presion de Vacio (Psi):</label>
									<input type="num" class="form-control" placeholder="Presion de Vacio (Psi)" name="pre_vacio"  />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="category">Cantidad a Evacuar :</label>
									<input type="num" class="form-control" placeholder="Cantidad a Evacuar" name="g_evacuar" readonly  />
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="category">Fecha de Lavado :</label>
									<input type="text"  class="form-control" placeholder="Fecha de Lavado" value="<?php isset($fecha_lav_eto) ? $fecha_lav_eto : NULL ?>" name="g_lavado" readonly />
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
						<input type="submit" value="Guardar" name="g_cilindro" id="g_cilindro" class="btn btn-primary" />
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</form>
	</div><!-- /.modal -->
	<!-- Fin Modal -->
</div>
<div id="main" role="main">
<?php
if (in_array(18, $acc))
{
?>
	<div id="content">
	<!-- widget grid -->
		<section id="widget-grid" class="">
			<!-- START ROW -->
			<div class="row">
				<!-- NEW COL START -->
				<article class="col-sm-12 col-md-12 col-lg-6">		
					<div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h2>Revisión Preliminar</h2>				
						</header>
						<div>				
						<div class="jarviswidget-editbox"></div>
							<div class="widget-body no-padding">												
								<input type="hidden" name="id_orden"  value="<?php echo $id_orden; ?>">
								<input type="hidden" name="id_tipo_cilindro1"  value="<?php echo isset($id_tipo_cilindro1) ? $id_tipo_cilindro1 : NULL; ?>">
								<input type="hidden" name="id_tipo_envace1"  value="<?php echo isset($id_tipo_envace1) ? $id_tipo_envace1 : NULL; ?>">							
								<div class="row">
									<?php
									if (in_array(19, $acc))
									{
									?>					
										<div class="col-md-4">
											<div class="form-group">
												<label class="font-lg">Num cilindro :</label>											 
												<input type="text" class="input-lg" name="num_cili" id="cilindro" placeholder="Num cilindro" value="" />											
											</div>
										</div>	
									<?php
									}										
									?>						
								</div>																
							</div>
						</div>					
					</div>			
				<!-- Widget ID (each widget will need unique ID)-->
					<div class="jarviswidget" id="wid-id-2" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h2>Cilindros en Planta</h2>			
						</header>
						<div>				
							<div class="jarviswidget-editbox"></div>																	
							<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">						
								<thead>
									<tr>
										<th>#</th>                                            
			              <th>Cilindro</th>
			              <th>Estado</th>
			              <th>Tipo Gas</th>
										<th>Capacidad (Kg)</th>
			              <th>Acción</th>			                            
									</tr>
								</thead>
								<tbody>													
								  <?php
			              $contador1 = 0;
			              $consulta1 = "SELECT cilindro_eto.num_cili_eto,cilindro_eto.id_cilindro_eto,cilindro_eto.id_estado,pre_produccion_mezcla.id_pre_produccion_mezcla  FROM cilindro_eto INNER JOIN pre_produccion_mezcla ON pre_produccion_mezcla.id_cilindro_eto = cilindro_eto.id_cilindro_eto WHERE cilindro_eto.id_estado IN(2,8,10,7,9)";
			              $resultado1 = mysqli_query($con,$consulta1) ;
			              while ($linea1 = mysqli_fetch_array($resultado1))
			              {
			               	$contador1 = $contador1 + 1;
			               	$num_cili_eto = $linea1["num_cili_eto"];
											$id_cilindro_eto = $linea1["id_cilindro_eto"];
											$id_pre_produccion_mezcla22 = $linea1["id_pre_produccion_mezcla"];
											$id_estado = $linea1["id_estado"];
											
											$consulta = "SELECT * FROM cilindro_eto WHERE id_cilindro_eto =".$id_cilindro_eto;
			                $resultado = mysqli_query($con,$consulta) ;
			                while ($linea = mysqli_fetch_array($resultado))
			                {
			                  $id_tipo_cilindro = $linea["id_tipo_cilindro"];
			                  $id_tipo_envace = $linea["id_tipo_envace"];
			                }mysqli_free_result($resultado);	

											$consulta3 = "SELECT * FROM tipo_cilindro WHERE id_tipo_cilindro =".$id_tipo_cilindro;
		                  $resultado3 = mysqli_query($con,$consulta3) ;
		                  while ($linea3 = mysqli_fetch_array($resultado3))
		                  {
		                    $tipo_cili = $linea3["tipo_cili"];	                                    	
		                  }mysqli_free_result($resultado3);

		                  $consulta4 = "SELECT * FROM tipo_envace WHERE id_tipo_envace =".$id_tipo_envace;
		                  $resultado4 = mysqli_query($con,$consulta4) ;
		                  while ($linea4 = mysqli_fetch_array($resultado4))
		                  {
		                    $tipo = $linea4["tipo"];
		                  }mysqli_free_result($resultado4);	

											$consulta5 = "SELECT * FROM cilindro_eto WHERE id_cilindro_eto =".$id_cilindro_eto;
											$resultado5 = mysqli_query($con,$consulta5) ;
											while ($linea5 = mysqli_fetch_array($resultado5))
											{
											  	$num_cili_eto = $linea5["num_cili_eto"];
											  	//$id_estado = $linea5["id_estado"];
											  	
											}mysqli_free_result($resultado5); 
											$consulta6 = "SELECT * FROM estado_cilindro_eto WHERE id_estado_cilindro = $id_estado";
		                  $resultado6 = mysqli_query($con,$consulta6) ;
		                  while ($linea6 = mysqli_fetch_array($resultado6))
		                  {
		                    $estado_cili_eto = $linea6["estado_cili_eto"];		                                                	
											}mysqli_free_result($resultado6);
											
											$consulta7 = "SELECT * FROM pre_produccion_mezcla WHERE id_pre_produccion_mezcla = $id_pre_produccion_mezcla22";
											$resultado7 = mysqli_query($con, $consulta7);

											$linea7 = mysqli_fetch_array($resultado7);

											$pre_inicial2 = $linea7["pre_inicial"];
											$g_evacuar2 = $linea7["g_evacuar"];

											if($pre_inicial2 != 0){
												?>
												<tr class="odd gradeX">
													<td width="5"><?php echo $contador1; ?></td> 
													<td><?php echo $num_cili_eto; ?></td>
													<td><?php echo $estado_cili_eto; ?></td>
													<td><?php echo $tipo_cili; ?></td>
													<td><?php echo $tipo; ?></td>
													<?php
													if (in_array(20, $acc))
													{
														?>
														<td width="100" style="vertical-align:bottom;">			                              	
															<input type="image" onclick="editar(<?php echo $id_pre_produccion_mezcla22; ?>)" src="img/edit.png" width="30" height="30">			                              				                              
														</td>
														<?php
													}										
													?>   
												</tr>
												<?php
											}
			                            			                            		                            
			                ?>
				              
			                <?php
			              }mysqli_free_result($resultado1);	                                            
		                ?>
								</tbody>							
							</table>
						</div>            
         			</div>
         		</article>
        	</div>      
   		</section> 
    </div>
<?php
}										
?>
</div>

<?php
  include("inc/footer.php");
  include("inc/scripts.php"); 
?>
<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/jquery-form/jquery-form.min.js"></script>
<script type="text/javascript">
function resta()
{ 
    mezcla.g_evacuar.value = ((parseFloat(mezcla.peso_inicial.value))-(parseFloat(mezcla.peso_final.value))).toFixed(2);      	    
}
</script>
<script type="text/javascript">
$(document).ready(function(){
	$("#cilindro").keypress(function(e) {
       if(e.which == 13) 
       {        
	    	document.getElementById("mezcla").reset();
	    	var num_cili1 = $('input:text[name=num_cili]').val();    	
			validar(num_cili1);     
       }
    });
    function validar(dato)
	{	
		//alert(dato);
		$.ajax({
			url: 'id_cili.php',
			type: 'POST',
			data: 'dato='+dato,
		})
		.done(function(data) {			
			var objeto = JSON.parse(data);							
			var id_cili= objeto.id;
			if (id_cili>0) 
			{												
				form_cili(id_cili);								
			}
			else
			{				
				alert("Por Favor seleccione un cilindro valido para la mezcla");
			}						
		})
		.fail(function() {
			console.log("error");
		});
	}
	function form_cili(id_cili)
	{	
		$.ajax({
			url: 'id_cili1.php',
			type: 'POST',
			data: 'dato='+id_cili,
		})
		.done(function(data) {

			var objeto = JSON.parse(data);
			$("input[name=id_pre_produccion_mezcla]").val(objeto.id_pre_produccion_mezcla);
			$("input[name=id_cilindro_eto]").val(objeto.id);
			$("input[name=num_cili_eto]").val(objeto.num_cili_eto);
			$("input[name=prue_hidro]").val(objeto.fecha_ult_eto);
			$("input[name=gas_eva]").val(objeto.tipo_cili);
			$("input[name=nombre_mezcla]").val(objeto.tipo_cili);
			$("input[name=peso_esperado]").val(objeto.tipo_envace);
			$("input[name=id_estado]").val(objeto.id_estado);
			$("input[name=peso_final]").val(objeto.peso_final);
			$("input[name=g_evacuar]").val(objeto.g_evacuar);
			$("input[name=g_lavado]").val(objeto.fecha_lav_eto);
			$("input[name=pre_inicial]").val(objeto.pre_inicial);
			$("input[name=pre_vacio]").val(objeto.pre_vacio);
			$("input[name=peso_inicial]").val(objeto.peso_inicial);


			
			/*
			var id_cilindro_eto= objeto.id;
			var fecha = new Date();
		
			var yyyy = fecha.getFullYear();//año actual
			var mmmm = fecha.getMonth();//mes actual

			var fecha_lav_eto= objeto.fecha_lav_eto;
			var fecha_ult_eto= objeto.fecha_ult_eto;							
			var año_cili = fecha_ult_eto.split('-');
			var mes_cili = fecha_lav_eto.split('-');			
			var año = año_cili[0];//año prueba hidrostatica
			var mes_año = mes_cili[0];//año de lavado
			var mes = mes_cili[1];//mes lavado
			
			var aux = mmmm-4;			
			var lavado1=2 ;
			var prueba ;

			if (mes_año<yyyy ) 
			{
				if (aux<=0) 
				{
					var aux1 = aux*(-1);
					var mes_mes = 12-aux1;
					if (mes<mes_mes) 
					{
						alert("Este cilindro se encuentra con la prueba de lavado vencida ");
						lavado1 = 1;
						modal(id_cilindro_eto);
					}
					else
					{
						lavado1 = 2;
					}					
				}
				else
				{
					alert("Este cilindro se encuentra con la prueba de lavado vencida ");
					lavado1 = 1;
					modal(id_cilindro_eto);
				}				
			}
			if (yyyy==mes_año && aux>mes) 
			{
				alert("Este cilindro se encuentra con la prueba de lavado vencida ");
				lavado1 = 1;
				modal(id_cilindro_eto);				
			}
			else
			{
				lavado1 = 2;
			}

			año_actual = yyyy-4;			
			
			if (año<año_actual) 
			{
				//alert("Este cilindro excede la fecha ("+fecha_ult_eto+") maxima permitida de la prueba de expancion volumetrica")
				alert("Este cilindro se encuentra con la prueba de expansión volumétrica vencida");
				prueba=1;
				modal(id_cilindro_eto);
				//alert(fecha_ult_eto);
			}
			else
			{
				prueba=2;
			}	

			if (prueba==2&&lavado1==2) 
			{
				*/

			
				$("#modal_orden").modal();	
		})
		.fail(function() {
			console.log("error");
		});
	}
	function modal(id_cilindro_eto) 
	{
		$.ajax({
			url: 'id_cili5.php',
			type: 'POST',
			data: 'dato='+id_cilindro_eto,
		})
		.done(function(data) {
			if (data==1) 
			{
				//alert("Bien");
			}
			if (data==2) 
			{
				alert("Error en la consulta");
			}
		})
		.fail(function() {
			console.log("error");
		});
	}
});
	function editar(cili)
	{
		$("#modal_orden").modal();
		$.ajax({
			url: 'edit_cili1.php',
			type: 'POST',
			data: 'dato='+cili,
		})
		.done(function(data) {
			var objeto = JSON.parse(data);							
			$("input[name=id_pre_produccion_mezcla]").val(objeto.id);
			$("input[name=id_cilindro_eto]").val(objeto.id_cilindro_eto);			
			$("input[name=num_cili_eto]").val(objeto.id_cilindro);
			$("input[name=prue_hidro]").val(objeto.prue_hidro);
			$("input[name=pre_inicial]").val(objeto.pre_inicial);
			$("input[name=gas_eva]").val(objeto.gas_eva);
			$("input[name=peso_inicial]").val(objeto.peso_inicial);
			$("input[name=peso_final]").val(objeto.peso_final);
			$("input[name=pre_vacio]").val(objeto.pre_vacio);
			$("input[name=g_evacuar]").val(objeto.g_evacuar);
			$("input[name=fecha_lav_eto]").val(objeto.fecha_lav_eto);
			$("input[name=g_lavado]").val(objeto.fecha_lav_eto);					
		})
		.fail(function() {
			console.log("error");
		});
	}
</script>
<script type="text/javascript">
window.onload = function()
{
    var form = document.forms.mezcla;
    form.oninput = function() 
    {  
        form.esperado_eto.value = ((parseInt(form.peso_esperado.value))*0.90).toFixed(1);
        form.esperado_co.value = ((parseInt(form.peso_esperado.value))*0.10).toFixed(1);
        form.tara_vacio.value = form.peso_final.value;
        form.peso_final_1.value = ((parseFloat(form.real_eto.value))+(parseFloat(form.real_co.value))).toFixed(1);
        form.desviacion.value = ((parseFloat(form.peso_esperado.value))-(parseFloat(form.peso_final_1.value))).toFixed(1);
    }
}
function vaciar()
{
document.getElementById("myTable").innerHTML=""
        
}
</script>
<script type="text/javascript">

 
		
		$(document).ready(function() {
			
			// menu
			$("#menu").menu();
		
			/*
			 * AUTO COMPLETE AJAX
			 */
		
			function log(message) {
				$("<div>").text(message).prependTo("#log");
				$("#log").scrollTop(0);
			}
		
			$("#city").autocomplete({
				source : function(request, response) {
					$.ajax({
						url : "http://ws.geonames.org/searchJSON",
						dataType : "jsonp",
						data : {
							featureClass : "P",
							style : "full",
							maxRows : 12,
							name_startsWith : request.term
						},
						success : function(data) {
							response($.map(data.geonames, function(item) {
								return {
									label : item.name + (item.adminName1 ? ", " + item.adminName1 : "") + ", " + item.countryName,
									value : item.name
								}
							}));
						}
					});
				},
				minLength : 2,
				select : function(event, ui) {
					log(ui.item ? "Selected: " + ui.item.label : "Nothing selected, input was " + this.value);
				}
			});
		
			/*
			 * Spinners
			 */
			$("#spinner").spinner();
			$("#spinner-decimal").spinner({
				step : 0.01,
				numberFormat : "n"
			});
		
			$("#spinner-currency").spinner({
				min : 5,
				max : 2500,
				step : 25,
				start : 1000,
				numberFormat : "C"
			});
		
			/*
			 * CONVERT DIALOG TITLE TO HTML
			 * REF: http://stackoverflow.com/questions/14488774/using-html-in-a-dialogs-title-in-jquery-ui-1-10
			 */
			$.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
				_title : function(title) {
					if (!this.options.title) {
						title.html("&#160;");
					} else {
						title.html(this.options.title);
					}
				}
			}));
		
		
			/*
			* DIALOG SIMPLE
			*/
		
			// Dialog click
			$('#dialog_link').click(function() {
				$('#dialog_simple').dialog('open');
				return false;
		
			});
		
			$('#dialog_simple').dialog({
				autoOpen : false,
				width : 600,
				resizable : false,
				modal : true,
				title : "<div class='widget-header'><h4><i class='fa fa-warning'></i> Empty the recycle bin?</h4></div>",
				buttons : [{
					html : "<i class='fa fa-trash-o'></i>&nbsp; Delete all items",
					"class" : "btn btn-danger",
					click : function() {
						$(this).dialog("close");
					}
				}, {
					html : "<i class='fa fa-times'></i>&nbsp; Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
					}
				}]
			});
		
			/*
			* DIALOG HEADER ICON
			*/
		
			// Modal Link
			$('#modal_link').click(function() {
				$('#dialog-message').dialog('open');
				return false;
			});
		
			$("#dialog-message").dialog({
				autoOpen : false,
				modal : true,
				title : "<div class='widget-header'><h4><i class='icon-ok'></i> jQuery UI Dialog</h4></div>",
				buttons : [{
					html : "Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
					}
				}]
		
			});
		
			/*
			 * Remove focus from buttons
			 */
			$('.ui-dialog :button').blur();
		
			/*
			 * Just Tabs
			 */
		
			$('#tabs').tabs();
		
			/*
			 *  Simple tabs adding and removing
			 */
		
			$('#tabs2').tabs();
		
			// Dynamic tabs
			var tabTitle = $("#tab_title"), tabContent = $("#tab_content"), tabTemplate = "<li style='position:relative;'> <span class='air air-top-left delete-tab' style='top:7px; left:7px;'><button class='btn btn-xs font-xs btn-default hover-transparent'><i class='fa fa-times'></i></button></span></span><a href='#{href}'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; #{label}</a></li>", tabCounter = 2;
		
			var tabs = $("#tabs2").tabs();
		
			// modal dialog init: custom buttons and a "close" callback reseting the form inside
			var dialog = $("#addtab").dialog({
				autoOpen : false,
				width : 600,
				resizable : false,
				modal : true,
				buttons : [{
					html : "<i class='fa fa-times'></i>&nbsp; Cancel",
					"class" : "btn btn-default",
					click : function() {
						$(this).dialog("close");
		
					}
				}]
			});
		
			// addTab form: calls addTab function on submit and closes the dialog
			var form = dialog.find("form").submit(function(event) {
				addTab();
				dialog.dialog("close");
				
			});
		
			// actual addTab function: adds new tab using the input from the form above

			
			function addTab() 
			{
				
				var label = tabTitle.val() || "Tab " + tabCounter, id = "tabs-" + tabCounter, li = $(tabTemplate.replace(/#\{href\}/g, "#" + id).replace(/#\{label\}/g, label)), tabContentHtml = tabContent.val() || "Tab " + tabCounter + " content.";
		
				tabs.find(".ui-tabs-nav").append(li);
				tabs.append("<div id='" + id + "'><p>" + tabContentHtml + "</p></div>");
				tabs.tabs("refresh");
				tabCounter++;
		
				// clear fields
				$("#tab_title").val("");
				$("#tab_content").val("");
			}
		
			// addTab button: just opens the dialog
			$("#add_tab").button().click(function() {
				var idd = document.getElementById('num').firstChild.nodeValue;

				
				dialog.dialog("open");
			});
		
			// close icon: removing the tab on click
					
			/*
			* ACCORDION
			*/
			//jquery accordion
			
		     var accordionIcons = {
		         header: "fa fa-plus",    // custom icon class
		         activeHeader: "fa fa-minus" // custom icon class
		     };
		     
			$("#accordion").accordion({
				autoHeight : false,
				heightStyle : "content",
				collapsible : true,
				animate : 300,
				icons: accordionIcons,
				header : "h4",
			})
		
			/*
			 * PROGRESS BAR
			 */
			$("#progressbar").progressbar({
		     	value: 25,
		     	create: function( event, ui ) {
		     		$(this).removeClass("ui-corner-all").addClass('progress').find(">:first-child").removeClass("ui-corner-left").addClass('progress-bar progress-bar-success');
				}
			});			

		})

	</script>
	<script src="js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<script type="text/javascript">
// DO NOT REMOVE : GLOBAL FUNCTIONS!

$(document).ready(function() {
	
	/* // DOM Position key index //
		
	l - Length changing (dropdown)
	f - Filtering input (search)
	t - The Table! (datatable)
	i - Information (records)
	p - Pagination (paging)
	r - pRocessing 
	< and > - div elements
	<"#id" and > - div with an id
	<"class" and > - div with a class
	<"#id.class" and > - div with an id and class
	
	Also see: http://legacy.datatables.net/usage/features
	*/	

	/* BASIC ;*/
		var responsiveHelper_dt_basic = undefined;
		var responsiveHelper_datatable_fixed_column = undefined;
		var responsiveHelper_datatable_col_reorder = undefined;
		var responsiveHelper_datatable_tabletools = undefined;
		
		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};

		$('#dt_basic').dataTable({
			"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
			"autoWidth" : true,
			"preDrawCallback" : function() {
				// Initialize the responsive datatables helper once.
				if (!responsiveHelper_dt_basic) {
					responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
				}
			},
			"rowCallback" : function(nRow) {
				responsiveHelper_dt_basic.createExpandIcon(nRow);
			},
			"drawCallback" : function(oSettings) {
				responsiveHelper_dt_basic.respond();
			}
		});

	/* END BASIC */
	
	/* COLUMN FILTER  */
    var otable = $('#datatable_fixed_column').DataTable({
    	//"bFilter": false,
    	//"bInfo": false,
    	//"bLengthChange": false
    	//"bAutoWidth": false,
    	//"bPaginate": false,
    	//"bStateSave": true // saves sort state using localStorage
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_fixed_column) {
				responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_fixed_column.respond();
		}		
	
    });
    
    // custom toolbar
    $("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');
    	   
    // Apply the filter
    $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {
    	
        otable
            .column( $(this).parent().index()+':visible' )
            .search( this.value )
            .draw();
            
    } );
    /* END COLUMN FILTER */   

	/* COLUMN SHOW - HIDE */
	$('#datatable_col_reorder').dataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_col_reorder) {
				responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_col_reorder.respond();
		}			
	});
	
	/* END COLUMN SHOW - HIDE */

	/* TABLETOOLS */
	$('#datatable_tabletools').dataTable({
		
		// Tabletools options: 
		//   https://datatables.net/extensions/tabletools/button_options
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        "oTableTools": {
        	 "aButtons": [
             "copy",
             "csv",
             "xls",
                {
                    "sExtends": "pdf",
                    "sTitle": "SmartAdmin_PDF",
                    "sPdfMessage": "SmartAdmin PDF Export",
                    "sPdfSize": "letter"
                },
             	{
                	"sExtends": "print",
                	"sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>"
            	}
             ],
            "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
        },
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_tabletools) {
				responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_tabletools.respond();
		}
	});
	
	/* END TABLETOOLS */

})

</script>
<?php 
include("inc/google-analytics.php"); 
}
else
{
    header("Location:index.php");
}
?>
					                       
										