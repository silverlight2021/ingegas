<?php

date_default_timezone_set("America/Bogota");

require ("libraries/conexion.php");
session_start();
if(@$_SESSION['logged']== 'yes')
{ 
     $acc = $_SESSION['acc'];
     $letra = isset($_REQUEST['letra']) ? $_REQUEST['letra'] : NULL;
  


//initilize the page
require_once("inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("inc/config.ui.php");

/*---------------- PHP Custom Scripts ---------

YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */

$page_title = "User Search";

/* ---------------- END PHP Custom Scripts ------------- */

//include header
//you can add your custom css in $page_css array.
//Note: all css files are inside css/ folder
$page_css[] = "your_style.css";
include("inc/header.php");

//include left panel (navigation)
//follow the tree in inc/config.ui.php
$page_nav[""][""][""][""] = true;
include("inc/nav.php");

?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
	<?php
		//configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
		//$breadcrumbs["New Crumb"] => "http://url.com"
		include("inc/ribbon.php");
	?>
<style type="text/css">
  h2 {display:inline}
</style>
	<!-- MAIN CONTENT -->
	<div id="content">

		<div class="row">
		<div align="right" >
			<form>
				<input type="text" placeholder="Client Search">
			</form>
		</div>
			<div class="">
				<h1  class="page-title txt-color-blueDark"> <?php echo $page_title; ?>: 
					          <a href="busqueda_usuario.php?letra=a"><h2>A</h2></a>
                    <a href="busqueda_usuario.php?letra=b"><h2>B</h2></a>
                    <a href="busqueda_usuario.php?letra=c"><h2>C</h2></a>
                    <a href="busqueda_usuario.php?letra=d"><h2>D</h2></a>
                    <a href="busqueda_usuario.php?letra=e"><h2>E</h2></a>
                    <a href="busqueda_usuario.php?letra=f"><h2>F</h2></a>
                    <a href="busqueda_usuario.php?letra=g"><h2>G</h2></a>
                    <a href="busqueda_usuario.php?letra=h"><h2>H</h2></a>
                    <a href="busqueda_usuario.php?letra=i"><h2>I</h2></a>
                    <a href="busqueda_usuario.php?letra=j"><h2>J</h2></a>
                    <a href="busqueda_usuario.php?letra=k"><h2>K</h2></a>
                    <a href="busqueda_usuario.php?letra=l"><h2>L</h2></a>
                    <a href="busqueda_usuario.php?letra=m"><h2>M</h2></a>
                    <a href="busqueda_usuario.php?letra=n"><h2>N</h2></a>
                    <a href="busqueda_usuario.php?letra=o"><h2>O</h2></a>
                    <a href="busqueda_usuario.php?letra=p"><h2>P</h2></a>
                    <a href="busqueda_usuario.php?letra=q"><h2>Q</h2></a>
                    <a href="busqueda_usuario.php?letra=r"><h2>R</h2></a>
                    <a href="busqueda_usuario.php?letra=s"><h2>S</h2></a>
                    <a href="busqueda_usuario.php?letra=t"><h2>T</h2></a>
                    <a href="busqueda_usuario.php?letra=u"><h2>U</h2></a>
                    <a href="busqueda_usuario.php?letra=v"><h2>V</h2></a>
                    <a href="busqueda_usuario.php?letra=w"><h2>W</h2></a>
                    <a href="busqueda_usuario.php?letra=x"><h2>X</h2></a>
                    <a href="busqueda_usuario.php?letra=y"><h2>Y</h2></a>
                    <a href="busqueda_usuario.php?letra=z"><h2>Z</h2></a>                 
				
				</h1>
	
	
				  				
			</div>
			
					
		</div>
		<!-- widget grid -->
         
		
			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">
						<!-- widget options:
						usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

						data-widget-colorbutton="false"
						data-widget-editbutton="false"
						data-widget-togglebutton="false"
						data-widget-deletebutton="false"
						data-widget-fullscreenbutton="false"
						data-widget-custombutton="false"
						data-widget-collapsed="true"
						data-widget-sortable="false"

						-->
						<header>
							<span class="widget-icon"> <i class="fa fa-table"></i> </span>
							<h2>User</h2>
							 
                              

						</header>
						
							
						

						<!-- widget div-->
						<div>

							<!-- widget edit box -->
							<div class="jarviswidget-editbox">
								<!-- This area used as dropdown edit box -->

							</div>
							<!-- end widget edit box -->

							<!-- widget content -->
							<div class="widget-body">
								
								<table class="table table-bordered">
									<thead>
										<tr>
											<th>#</th>
											<th>Name</th>											
											<th>Last Name</th>																					
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										
										<?php
                                        if ( !empty($_GET['letra'])) 
                                        {
                                            $contador = "0";
                                            if (strlen($letra) > 0)
                                            {           
                                                $temp = explode(" ", $letra) ;
                                                $dato1 = isset($temp[0]) ? $temp[0] : NULL;
                                               
                                                
                                                  $consulta = "SELECT idUser,LastName,Name FROM user WHERE Name like '%".$dato1."%'";
                                                  $resultado = mysql_query($consulta) ;
                                                  while ($linea = mysql_fetch_array($resultado))
                                                  {
                                                    $contador = $contador + 1;
                                                    $idUser = $linea["idUser"];
                                                    $LastName = $linea["LastName"];
                                                    $Name = $linea["Name"];
                                                    ?>
                                                      <tr class="odd gradeX">
                                                                    <td width="40"><?php echo $contador; ?></td>
                                                                    <td><?php echo $Name; ?></td>
                                                                    <td><?php echo $LastName; ?></td>
                                                                    
                                                                    
                                                                    <td>

                                                                    <?php                                                                   
                                                                      if (in_array(22, $acc))
                                                                      {
                                                                      ?>
                                                                        <a class="btn btn-info btn-block" href="usuario.php?idUser=<?php echo $idUser; ?>">update</a>
                                                                      <?php
                                                                      }
                                                                      ?>


                                                                        
                                                                        
                                                                    </td>
                                                      </tr>   
                                                      <?php
                                                    }
                                                    mysql_free_result($resultado);
                                            }
                                            
                                        }
                                        else
                                        {			
                                        			$contador = "0";
                                        	        $temp = explode(" ", $dato) ;
                                                $dato1 = isset($temp[0]) ? $temp[0] : NULL;
                                                $dato2 = isset($temp[1]) ? $temp[1] : NULL;
                                                
                                                  $consulta = "SELECT idUser,LastName,Name FROM user";
                                                  $resultado = mysql_query($consulta) ;
                                                  while ($linea = mysql_fetch_array($resultado))
                                                  {
                                                    $contador = $contador + 1;
                                                    $idUser = $linea["idUser"];
                                                    $LastName = $linea["LastName"];
                                                    $Name = $linea["Name"];
                                                    ?>
                                                      <tr class="odd gradeX">
                                                                    <td width="40"><?php echo $contador; ?></td>
                                                                    <td><?php echo $Name; ?></td>
                                                                    <td><?php echo $LastName; ?></td>
                                                                    
                                                                    
                                                                    <td>

                                                                    <?php                                                                   
                                                                      if (in_array(22, $acc))
                                                                      {
                                                                      ?>
                                                                        <a class="btn btn-info btn-block" href="usuario.php?idUser=<?php echo $idUser; ?>">update</a>
                                                                      <?php
                                                                      }
                                                                      ?>


                                                                        
                                                                        
                                                                    </td>
                                                      </tr>   
                                                      <?php
                                                    }
                                                    mysql_free_result($resultado);
                                                   
                                        }	
                                        ?>


									</tbody>
								</table>

							</div>
							<!-- end widget content -->

						</div>
						<!-- end widget div -->

					</div>












		

		<!-- end widget grid -->

	</div>
	<!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->

<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->
<?php
	include("inc/footer.php");
?>
<!-- END PAGE FOOTER -->

<?php 
	//include required scripts
	include("inc/scripts.php"); 
?>
<script>

	$(document).ready(function() {
		// PAGE RELATED SCRIPTS
	})

</script>

<!-- PAGE RELATED PLUGIN(S) 
<script src="..."></script>-->
<!-- Flot Chart Plugin: Flot Engine, Flot Resizer, Flot Tooltip -->
<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>

<!-- Vector Maps Plugin: Vectormap engine, Vectormap language -->
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>

<!-- Full Calendar -->
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>


<?php 
	//include footer
	include("inc/google-analytics.php"); 
?>
<?php

}
else
{
    header("Location:index.php");
}
?>