<?php
date_default_timezone_set("America/Bogota");
require ("libraries/conexion.php");
session_start();

if($_SESSION['logged']== 'yes')
{ 
  	$id_cliente = isset($_REQUEST['id_cliente']) ? $_REQUEST['id_cliente'] : NULL;
    $User_idUser =$_SESSION['su'];
    $acc = $_SESSION['acc'];

	if(isset($_POST['guardar_cliente']))
	{
		$nombre1 = $_POST['nombre'] ;
		$telefono_fijo1=$_POST['telefono_fijo'] ;
		$nit1=$_POST['nit'] ;
		$telefono_celular1=$_POST['telefono_celular'] ;
		$persona_contacto1=$_POST['persona_contacto'] ;
		$correo1=$_POST['correo'] ;
		$observacion1=$_POST['observacion'] ;
		$direccion1=$_POST['direccion'] ;
		$tel_per1=$_POST['tel_per'] ;
		$mail_per1=$_POST['mail_per'] ;
		$dogito_v1=$_POST['dogito_v'] ;
		$estadoCli=$_POST['estadoCli'] ;

		if(strlen($id_cliente) > 0)
		{
	   		$consulta= "UPDATE clientes SET 
	            nombre = '$nombre1' ,
	            telefono_fijo = '$telefono_fijo1' ,
	            nit = '$nit1' , 
	            telefono_celular = '$telefono_celular1' , 
	            persona_contacto = '$persona_contacto1' ,
	            observacion = '$observacion1',
	            direccion = '$direccion1',
	            correo = '$correo1', 
	            tel_per = '$tel_per1',
	            mail_per = '$mail_per1',
	            dogito_v = '$dogito_v1',
							estado = '$estadoCli' 
	            WHERE id_cliente = $id_cliente ";
	    	$resultado = mysqli_query($con,$consulta) ;
		    if ($resultado == FALSE)
		    {
		      	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    }
		    else
		    {
		     	 header('Location: busqueda_clientes.php');
		    }
		}    
	  	
	 	else
	 	{
	 		$consulta2  = "SELECT nit FROM clientes WHERE nit like '".$nit1."' ";
		  	$resultado2 = mysqli_query($con,$consulta2) ;
		  	$linea2 = mysqli_fetch_array($resultado2);
		  	$nit2 = isset($linea2["nit"]) ? $linea2["nit"] : NULL;	
		  	if ($nit2==$nit1) 
		  	{
		  		?>
		      	<script type="text/javascript">
		         	alert("Ya existe este Cliente ");
		     	</script>
		     	<?php
		  	}
		  	else		  		# code...
		  	{ 
		   		$consulta  = "INSERT INTO clientes
		          	(nombre,nit, telefono_fijo, telefono_celular, persona_contacto,observacion,correo,direccion,tel_per,mail_per,dogito_v,estado) 
		          	VALUES ('".$nombre1."','".$nit1."', '".$telefono_fijo1."', '".$telefono_celular1."','".$persona_contacto1."' ,'".$observacion1."' ,'".$correo1."','".$direccion1."','".$tel_per1."','".$mail_per1."','".$dogito_v1."','".$estadoCli."' )";
		    	$resultado = mysqli_query($con,$consulta) ;
		    	if ($resultado == FALSE)
		    	{
		    	  	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
		    	}
		    	else
		    	{
		     		$id_cliente = mysqli_insert_id($con);
		      		header('Location: busqueda_clientes.php');
		    	}
		    }
	  	}    
	}
	if(strlen($id_cliente) > 0)
	{ 
	  	$consulta  = "SELECT * FROM clientes WHERE id_cliente= $id_cliente";
	  	$resultado = mysqli_query($con,$consulta) ;
	  	$linea = mysqli_fetch_array($resultado);

		$id_cliente1 = isset($linea["id_cliente"]) ? $linea["id_cliente"] : NULL;  
		$nombre = isset($linea["nombre"]) ? $linea["nombre"] : NULL;
		$telefono_fijo = isset($linea["telefono_fijo"]) ? $linea["telefono_fijo"] : NULL;
		$telefono_celular = isset($linea["telefono_celular"]) ? $linea["telefono_celular"] : NULL;
		$persona_contacto = isset($linea["persona_contacto"]) ? $linea["persona_contacto"] : NULL;
		$observacion = isset($linea["observacion"]) ? $linea["observacion"] : NULL;
		$correo = isset($linea["correo"]) ? $linea["correo"] : NULL;
		$direccion = isset($linea["direccion"]) ? $linea["direccion"] : NULL;
		$nit = isset($linea["nit"]) ? $linea["nit"] : NULL;
		$tel_per = isset($linea["tel_per"]) ? $linea["tel_per"] : NULL;
		$mail_per = isset($linea["mail_per"]) ? $linea["mail_per"] : NULL;
		$dogito_v = isset($linea["dogito_v"]) ? $linea["dogito_v"] : NULL;
		$estadoCli1 = isset($linea["estado"]) ? $linea["estado"] : NULL;
	    mysqli_free_result($resultado);
	}
require_once("inc/init.php");
require_once("inc/config.ui.php");
$page_title = "Cliente";
$page_css[] = "your_style.css";
include("inc/header.php");
$page_nav[""][""][""][""] = true;
include("inc/nav.php");

if ($estadoCli1 == 1) {
	$muetraNombre = 'ACTIVO';
	$cambiaEstadi = 'INACTIVO';
}elseif ($estadoCli1 == 2) {
	$muetraNombre = 'INACTIVO';
	$cambiaEstadi = 'INACTIVO';
}

?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
	<div id="content">
		<section id="widget-grid" class="">
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-6">			
					<div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">			
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h2>Cliente </h2>											
						</header>
						<div>
							<div class="jarviswidget-editbox"></div>	
							<div class="widget-body no-padding">								
								<form id="checkout-form" name="form1" class="smart-form" novalidate="novalidate" action="cliente.php" method="POST" onsubmit="confirmar();return false">
								<input type="hidden" name="id_cliente" id="id_cliente" value="<?php echo $id_cliente; ?>">
									<fieldset>
										<div class="row">
											<section class="col col-4">
												<label class="label">Nombre / Razón Social :</label>
												<label class="input"> 
													<input type="text" name="nombre" id="nombre" placeholder="Nombre / Razón Social" value="<?php echo isset($nombre) ? $nombre : NULL; ?>">
												</label>
											</section>
											<section class="col col-4">
												<label class="label">CC / Nit :</label>
												<label class="input"> 
													<input type="tel" name="nit" placeholder="Cc / Nit"  value="<?php echo isset($nit) ? $nit : NULL; ?>">
												</label>
											</section>
											<section class="col col-2">
												<label class="label">D V :</label>
												<label class="input"> 
													<input type="tel" name="dogito_v" placeholder="D V" value="<?php echo isset($dogito_v) ? $dogito_v : NULL; ?>">
												</label>
											</section>
											<section class="col col-2">
												<label class="label">Estado :</label>
												<label class="input">													
													<select name="estadoCli" class="col-10">													
														<option value="<?php echo $estadoCli1; ?>" selected ><?php echo $muetraNombre; ?></option>
														<option value="1">Cambiar a ACTIVO</option>	
														<option value="2">Cambiar a INACTIVO</option>													
													</select>													
												</label>
											</section>																							
										</div>
									</fieldset>	
									<fieldset>	
										<div class="row">
											<section class="col col-6">
												<label class="label">Teléfono Fijo :</label>
												<label class="input"> 
													<input type="tel" name="telefono_fijo" placeholder="Teléfono Fijo" data-mask="(9) 999-9999" value="<?php echo isset($telefono_fijo) ? $telefono_fijo : NULL; ?>">
												</label>
											</section>
					                        <section class="col col-6">
					                        <label class="label">Celular :</label>
					                        <label class="input"> 
					                          <input type="tel" name="telefono_celular" placeholder="Celular" data-mask="(999) 999-9999" value="<?php echo isset($telefono_celular) ? $telefono_celular : NULL; ?>">
					                        </label>
					                      </section>									
										</div>
									</fieldset>
									<fieldset>
										<div class="row">
											<section class="col col-6">
												<label class="label">E-Mail :</label>
												<label class="input"> 
													<input type="email" name="correo" placeholder="Correo Electronico" value="<?php echo isset($correo) ? $correo : NULL; ?>">
												</label>
											</section>
											<section class="col col-6">
												<label class="label">Dirección :</label>
												<label class="input"> 
													<input type="text" name="direccion" placeholder="Dirección" value="<?php echo isset($direccion) ? $direccion : NULL; ?>">
												</label>
											</section>											
										</div>
									</fieldset>
									<fieldset>	
										<div class="row">
											<section class="col col-6">
												<label class="label">Persona Contacto :</label>
												<label class="input"> 
													<input type="text" name="persona_contacto" placeholder="Persona Contacto " value="<?php echo isset($persona_contacto) ? $persona_contacto : NULL; ?>">
												</label>
											</section>
					                        <section class="col col-6">
					                        <label class="label">Teléfono Celular :</label>
					                        <label class="input"> 
					                          <input type="tel" name="tel_per" placeholder="Teléfono" data-mask="(999) 999-9999"  value="<?php echo isset($tel_per) ? $tel_per : NULL; ?>">
					                        </label>
					                      </section>									
										</div>
										<div class="row">
											<section class="col col-6">
												<label class="label">Correo :</label>
												<label class="input"> 
													<input type="email" name="mail_per" title="Ingrese un correo Electrónico valido" placeholder="Correo "  value="<?php echo isset($mail_per) ? $mail_per : NULL; ?>" >
												</label>
											</section>
					                        <section class="col col-6">
					                        <label class="label">Observación :</label>
					                        <label class="input"> 
					                          <input type="text" name="observacion" placeholder="Observación" value="<?php echo isset($observacion) ? $observacion : NULL; ?>">
					                        </label>
					                      </section>									
										</div>
									</fieldset>
									<?php
									if (in_array(41, $acc))
									{
									?>
									<footer>										
										<input type="submit" value="Guardar" name="guardar_cliente" id="guardar_cliente" class="btn btn-primary" />
									</footer>
									<?php
									}										
									?>
								</form>
							</div>						
						</div>				
					</div>	
				</article>
			</div>
		</section>
	</div>
</div>
<?php
	include("inc/footer.php");
	include("inc/scripts.php"); 
?>

<script type="text/javascript">
function confirmar()
{
	//return false;
	var txt;
    var r = confirm("¿ Seguro que desea continuar ?");
    if (r == true) {
        submit.form1();
        txt = "You pressed OK!";
    } else {
        return false;
    }
}
</script>
<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/jquery-form/jquery-form.min.js"></script>


<script type="text/javascript">

	$(document).ready(function() 
	{

		var $checkoutForm = $('#checkout-form').validate(
		{
		// Rules for form validation
			rules : 
			 {
				nombre : 
				{
					required : true
				},
				nit : 
				{
					required : true
				},
				teléfono_fijo : 
				{
					required : true				
				},
		        correo : 
		        {
		          required : true,
		          email : true
		          
		        },
		        direccion : 
		        {
		          required : true
		          
		        }
			},

			// Messages for form validation
			messages : {
				nombre : {
					required : 'Ingrese el nombre de la empresa'
				},
				nit : {
					required : 'Ingrese el NIT de la empresa'
				},
		        telefono_fijo : {
		          required : 'Ingrese un numero de telefono fijo'
		        },
				correo : {
					required : 'Ingrese un correo Electronico',
					email : 'Ingrese un correo Electronico valido'
				}
				,
				direccion : {
					required : 'Ingrese una direccion'
				}
			},

			// Do not change code below
			errorPlacement : function(error, element) {
				error.insertAfter(element.parent());
			}
		});
		
		
		
				
	})

</script>


<?php 
	//include footer
	include("inc/google-analytics.php"); 
}
else
{
    header("Location:index.php");
}
?>