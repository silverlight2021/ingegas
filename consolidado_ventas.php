<?php
date_default_timezone_set("America/Bogota");
require ("libraries/conexion.php");
session_start();

if(@$_SESSION['logged']== 'yes')
{ 
	$valor ="";
  $acc = $_SESSION['acc'];    
    
  if(isset($_POST['descargar_excel'])){
    $sucursal = $_POST["sucursal"];
		$tipo_informe = $_POST["tipo_informe"];

		if($sucursal == '051'){
			if($tipo_informe == '1'){
				header("Location: descargar_excel/comisiones/descargar_comisiones.php");
			}else if($tipo_informe == '2'){
				header("Location: descargar_excel/comisiones/descargar_comisiones_hh.php");
			}
		}else if($sucursal == '060'){
			if($tipo_informe == '1'){
				header("Location: descargar_excel/comisiones/descargar_comisiones_060.php");
			}else if($tipo_informe == '2'){
				header("Location: descargar_excel/comisiones/descargar_comisiones_hh_060.php");
			}
		}else if($sucursal == '370'){
			if($tipo_informe == '1'){
				header("Location: descargar_excel/comisiones/descargar_comisiones_370.php");
			}else if($tipo_informe == '2'){
				header("Location: descargar_excel/comisiones/descargar_comisiones_hh_370.php");
			}
		}else if($sucursal == '590'){
			header("Location: descargar_excel/comisiones/descargar_comisiones_590.php");
		}
		
		
  }
    

require_once("inc/init.php");
require_once("inc/config.ui.php");
$page_title = "Consolidado de Ventas";
$page_css[] = "your_style.css";
include("inc/header.php");
include("inc/nav.php");

?>
<style type="text/css">
  h2 {display:inline}
</style>
<style type="text/css">
	.center-row {
	display:table;
	}
	.center {
		display:table-cell;
	    vertical-align:middle;
	    float:none;
	}
</style>	
<div id="main" role="main">
	<div id="content">
		<div class="row">
			<div class="" align="center">
				<h1  class="page-title txt-color-blueDark"> <?php echo $page_title; ?></h1>
			</div>	      	
		</div>	
		<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">		
			<header>
				<span class="widget-icon"> <i class="fa fa-table"></i> </span>
				<h2>Consolidado de Ventas</h2>				
			</header>
			<div>
				<div class="jarviswidget-editbox"></div>
				<div class="widget-body no-padding">
					<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
						<form action="busqueda_cilindros_eto.php" method="POST" name="form1">
							<thead>
								<tr>
									<th>#</th>
									<th>Fecha</th>
									<th>Movimientos de Inventario</th>
									<th>Presenta Anomalias?</th>
									<th>Descargar</th>

								</tr>
							</thead>
							<tbody>
								<?php 
								$contador = 0;
								$contador_anomalias = 0;
								$consulta = "SELECT * FROM cargue_comisiones_vendedores";
								$resultado = mysqli_query($con, $consulta);
								$ids_anomalias_messer = [];
								$pos_messer = 0;
								$ids_anomalias_ingegas = [];
								$pos_ingegas = 0;

								while($linea = mysqli_fetch_array($resultado)){
									$contador++;
									$fecha = $linea["fecha"];
									$mes = $linea["mes"];
									$anio = date("Y");
									?>
									<tr>
										<td><?php echo $contador ?></td>
										<td><?php echo $fecha ?></td>
										<td>Si</td>
										<td>
											<?php
											$consulta1 = "SELECT cliente, id_ventas_producto FROM ventas_producto WHERE anio = '$anio' AND mes = '$mes'";
											$resultado1 = mysqli_query($con, $consulta1);

											while($linea1 = mysqli_fetch_array($resultado1)){
												$cliente = $linea1["cliente"];
												$id_ventas_producto = $linea1["id_ventas_producto"];
												
												$consulta2 = "SELECT vendedor FROM asignacion_vendedores WHERE codigo = '$cliente'";
												$resultado2 = mysqli_query($con, $consulta2);

												if(mysqli_num_rows($resultado2)== 0){

													$cliente = intval($cliente);

													$consulta8 = "SELECT vendedor FROM asignacion_vendedores WHERE codigo = '$cliente'";
													$resultado8 = mysqli_query($con, $consulta8);

													if(mysqli_num_rows($resultado8) == 0){
														$contador_anomalias++;
														$ids_anomalias_messer[$pos_messer] = $id_ventas_producto;
														$pos_messer++;
													}
												}
											}

											$consulta3 = "SELECT tercero, id_vt_ing FROM vt_ing";
											$resultado3 = mysqli_query($con, $consulta3);

											/*while($linea3 = mysqli_fetch_array($resultado3)){
												$tercero = $linea3["tercero"];
												$id_vt_ing = $linea3["id_vt_ing"];

												$consulta4 = "SELECT vendedor FROM asignacion_vendedores WHERE cliente LIKE '%$tercero%'";
												$resultado4 = mysqli_query($con, $consulta4);

												if(mysqli_num_rows($resultado4)== 0){
													$contador_anomalias++;
													$ids_anomalias_ingegas[$pos_ingegas] = $id_vt_ing;
													$pos_ingegas++;
												}
											}*/

											if($contador_anomalias > 0){
												echo "Si";
											}else{
												echo "No";
											}
											?>
										</td>
										<td>
											<a href="descargaDemostracion_1.php"><img src="img/excel_icon.png" width="30" height="30"></a>
										</td>
									</tr>
									<?php
								}
								?>
							</tbody>
						</form>
					</table>
				</div>
			</div>
		</div>

		<?php
		if($contador_anomalias > 0){
			?>
			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">		
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>Anomalias</h2>				
				</header>
				<div>
					<div class="jarviswidget-editbox"></div>
					<div class="widget-body no-padding">
						<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
							<form action="busqueda_cilindros_eto.php" method="POST" name="form1">
								<thead>
									<tr>
										<th>#</th>
										<th>Fecha</th>
										<th>Cliente</th>
										<th>Agencia</th>
										<th>Factura</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										$contador1 = 0;
										$consulta5 = "SELECT * FROM cargue_comisiones_vendedores ORDER BY id_cargue_comisiones_vendedores DESC LIMIT 1";
										$resultado5 = mysqli_query($con, $consulta5);

										$linea5 = mysqli_fetch_array($resultado5);

										$fecha = $linea5["fecha"];

										$cantidad_anomalias_messer = count($ids_anomalias_messer);
										$cantidad_anomalias_ingegas = count($ids_anomalias_ingegas);

										for($i = 0; $i<$cantidad_anomalias_messer; $i++){
											$contador++;
											$id_ventas_producto = $ids_anomalias_messer[$i];

											$consulta6 = "SELECT nombre_cliente, numero_legal FROM ventas_producto WHERE id_ventas_producto = $id_ventas_producto";
											$resultado6 = mysqli_query($con, $consulta6);

											$linea6 = mysqli_fetch_array($resultado6);

											$nombre_cliente = $linea6["nombre_cliente"];
											$numero_legal = $linea6["numero_legal"];
											?>
											<tr>
												<td><?php echo $contador ?></td>
												<td><?php echo $fecha; ?></td>
												<td><?php echo $nombre_cliente; ?></td>
												<td>Messer</td>
												<td><?php echo $numero_legal; ?></td>
											</tr>
											<?php
										}

										/*for($a = 0; $a<$cantidad_anomalias_ingegas; $a++){
											$contador++;
											$id_vt_ing = $ids_anomalias_ingegas[$a];

											$consulta7 = "SELECT tercero, numero_documento FROM vt_ing WHERE id_vt_ing = $id_vt_ing";
											$resultado7 = mysqli_query($con, $consulta7);

											$linea7 = mysqli_fetch_array($resultado7);

											$tercero = $linea7["tercero"];
											$numero_documento = $linea7["numero_documento"];
											?>
											<tr>
												<td><?php echo $contador ?></td>
												<td><?php echo $fecha; ?></td>
												<td><?php echo $tercero; ?></td>
												<td>Ingegas</td>
												<td><?php echo $numero_documento; ?></td>
											</tr>
											<?php
										}*/
									?>	
								</tbody>
							</form>
						</table>
					</div>
				</div>
			</div>
			<?php
		}
		?>
	</div>
	
</div>
<?php
	include("inc/footer.php");
	include("inc/scripts.php"); 
?>
<script>
    function mostrarId(id){
        document.getElementById("id_cliente").value = id;
        valor = document.getElementById("id_cliente").value;
        
        fecha_inicial = document.getElementById("fecha_inicial").value;
        fecha_final = document.getElementById("fecha_final").value;
        
        if(valor != "" && fecha_inicial != "" && fecha_final != ""){
            document.getElementById("descargar_excel").disabled = false;
        }
    }
    
    function validarCampos(){
        valor = document.getElementById("id_cliente").value;
        
        fecha_inicial = document.getElementById("fecha_inicial").value;
        fecha_final = document.getElementById("fecha_final").value;
        
        if(valor != "" && fecha_inicial != "" && fecha_final != ""){
            document.getElementById("descargar_excel").disabled = false;
        }
    }
</script>
<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<script type="text/javascript">
// DO NOT REMOVE : GLOBAL FUNCTIONS!

$(document).ready(function() {
	
	/* // DOM Position key index //
		
	l - Length changing (dropdown)
	f - Filtering input (search)
	t - The Table! (datatable)
	i - Information (records)
	p - Pagination (paging)
	r - pRocessing 
	< and > - div elements
	<"#id" and > - div with an id
	<"class" and > - div with a class
	<"#id.class" and > - div with an id and class
	
	Also see: http://legacy.datatables.net/usage/features
	*/	

	/* BASIC ;*/
		var responsiveHelper_dt_basic = undefined;
		var responsiveHelper_datatable_fixed_column = undefined;
		var responsiveHelper_datatable_col_reorder = undefined;
		var responsiveHelper_datatable_tabletools = undefined;
		
		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};

		$('#dt_basic').dataTable({
			"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
			"autoWidth" : true,
			"preDrawCallback" : function() {
				// Initialize the responsive datatables helper once.
				if (!responsiveHelper_dt_basic) {
					responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
				}
			},
			"rowCallback" : function(nRow) {
				responsiveHelper_dt_basic.createExpandIcon(nRow);
			},
			"drawCallback" : function(oSettings) {
				responsiveHelper_dt_basic.respond();
			}
		});

	/* END BASIC */
	
	/* COLUMN FILTER  */
    var otable = $('#datatable_fixed_column').DataTable({
    	//"bFilter": false,
    	//"bInfo": false,
    	//"bLengthChange": false
    	//"bAutoWidth": false,
    	//"bPaginate": false,
    	//"bStateSave": true // saves sort state using localStorage
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_fixed_column) {
				responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_fixed_column.respond();
		}		
	
    });
    
    // custom toolbar
    $("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');
    	   
    // Apply the filter
    $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {
    	
        otable
            .column( $(this).parent().index()+':visible' )
            .search( this.value )
            .draw();
            
    } );
    /* END COLUMN FILTER */   

	/* COLUMN SHOW - HIDE */
	$('#datatable_col_reorder').dataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_col_reorder) {
				responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_col_reorder.respond();
		}			
	});
	
	/* END COLUMN SHOW - HIDE */

	/* TABLETOOLS */
	$('#datatable_tabletools').dataTable({
		
		// Tabletools options: 
		//   https://datatables.net/extensions/tabletools/button_options
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        "oTableTools": {
        	 "aButtons": [
             "copy",
             "csv",
             "xls",
                {
                    "sExtends": "pdf",
                    "sTitle": "SmartAdmin_PDF",
                    "sPdfMessage": "SmartAdmin PDF Export",
                    "sPdfSize": "letter"
                },
             	{
                	"sExtends": "print",
                	"sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>"
            	}
             ],
            "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
        },
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_tabletools) {
				responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_tabletools.respond();
		}
	});
	
	/* END TABLETOOLS */

})

</script>
<script type="text/javascript">
    
</script>

<?php 

	include("inc/google-analytics.php"); 
}
else
{
    header("Location:index.php");
}
?>