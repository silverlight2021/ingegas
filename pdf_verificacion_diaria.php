<?php
// William Aguirre Carpeta
// PDF HOJA DE RUTA INGEGAS
header('Content-Type: text/html; charset=UTF-8');
set_time_limit(0);
session_start();
date_default_timezone_set("America/Bogota");
require ("libraries/conexion.php");
require('libraries/fpdf/fpdf.php');
$fecha = date("Y-m-d");
$hora = date("G:i:s",time());

////////////////VARIABLES POST///////////////


	$logotipo_empresa = "img/logo-ingegas.png";
	$idHoja_ruta = isset($_REQUEST['idHoja_ruta']) ? $_REQUEST['idHoja_ruta'] : NULL;

	$id_verificacion_mensual = 5;
	
	$consulta1 = "SELECT * FROM hoja_ruta WHERE idHoja_ruta = '$idHoja_ruta'";
	$resultado1 = mysqli_query($con,$consulta1);

	$linea = mysqli_fetch_array($resultado1);

	$id_usuario = isset($linea["User_idUser_conductor"]) ? $linea["User_idUser_conductor"] : NULL;
	$id_vehiculo = isset($linea["placa"]) ? $linea["placa"] : NULL;
	$fecha = isset($linea["fecha"]) ? $linea["fecha"] : NULL;
	$odometro_salida = isset($linea["odometro_salida"]) ? $linea["odometro_salida"] : NULL;
	$agencia_salida = isset($linea["agencia_salida"]) ? $linea["agencia_salida"] : NULL;

	if($agencia_salida==1){
		$salida = "TOBERÍN";
	}else if($agencia_salida == 2){
		$salida = "CAZUCÁ";
	}else if($agencia_salida == 3){
		$salida = "MANIZALEZ";
	}

	$consulta2 = "SELECT * FROM user WHERE idUser = '$id_usuario'";
	$resultado2 = mysqli_query($con,$consulta2);

	$linea2 = mysqli_fetch_array($resultado2);

	$Name = isset($linea2["Name"]) ? $linea2["Name"] : NULL;
	$LastName = isset($linea2["LastName"]) ? $linea2["LastName"] : NULL;

	$nombre_usuario = $Name." ".$LastName;

	$consulta3 = "SELECT * FROM placa_camion WHERE id_placa_camion = '$id_vehiculo'";
	$resultado3 = mysqli_query($con,$consulta3);

	$linea3 = mysqli_fetch_array($resultado3);

	$placa_camion = isset($linea3["placa_camion"]) ? $linea3["placa_camion"] : NULL;


	$consulta4 = "SELECT * FROM verificacion_diaria_rutero WHERE idHoja_ruta = $idHoja_ruta";
	$resultado4 = mysqli_query($con,$consulta4);

	$linea4 = mysqli_fetch_array($resultado4);
	$apagado_encendido = $linea4["apagado_encendido"];
	$master = $linea4["master"];
	$fugas_vehiculo = $linea4["fugas_vehiculo"];
	$llantas = $linea4["llantas"];
	$frenos = $linea4["frenos"];
	$elementos_emergencia = $linea4["elementos_emergencia"];
	$listado_telefonos = $linea4["listado_telefonos"];
	$identificacion_vehiculo = $linea4["identificacion_vehiculo"];
	$hojas_seguridad = $linea4["hojas_seguridad"];
	$fugas_carga = $linea4["fugas_carga"];
	$carga_embalada = $linea4["carga_embalada"];
	$observaciones_apagado_encendido = $linea4["observaciones_apagado_encendido"];
	$observaciones_master = $linea4["observaciones_master"];
	$observaciones_fugas_vehiculo = $linea4["observaciones_fugas_vehiculo"];
	$observaciones_llantas = $linea4["observaciones_llantas"];
	$observaciones_frenos = $linea4["observaciones_frenos"];
	$observaciones_elementos_emergencia = $linea4["observaciones_elementos_emergencia"];
	$observaciones_listado_telefonos = $linea4["observaciones_listado_telefonos"];
	$observaciones_identificacion_vehiculo = $linea4["observaciones_identificacion_vehiculo"];
	$observaciones_hojas_seguridad = $linea4["observaciones_hojas_seguridad"];
	$observaciones_fugas_carga = $linea4["observaciones_fugas_carga"];
	$observaciones_carga_embalada = $linea4["observaciones_carga_embalada"];
/**
*  Clase para la maquetación de Factura
*/
class PDF extends FPDF
{
	function _Arc($x1, $y1, $x2, $y2, $x3, $y3) //Curvatura del rectángulo
	{
		$h = $this->h;
		$this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
							$x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
	}
	// private functions
	function RoundedRect($x, $y, $w, $h, $r, $style = '') //Rectángulo con bordes redondeados 
	{
		$k = $this->k;
		$hp = $this->h;
		if($style=='F')
			$op='f';
		elseif($style=='FD' || $style=='DF')
			$op='B';
		else
			$op='S';
		$MyArc = 4/3 * (sqrt(2) - 1);
		$this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));
		$xc = $x+$w-$r ;
		$yc = $y+$r;
		$this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));

		$this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);
		$xc = $x+$w-$r ;
		$yc = $y+$h-$r;
		$this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
		$this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);
		$xc = $x+$r ;
		$yc = $y+$h-$r;
		$this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
		$this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);
		$xc = $x+$r ;
		$yc = $y+$r;
		$this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
		$this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
		$this->_out($op);
	}
	// public functions
	function sizeOfText( $texte, $largeur )
	{
		$index    = 0;
		$nb_lines = 0;
		$loop     = TRUE;
		while ( $loop )
		{
			$pos = strpos($texte, "\n");
			if (!$pos)
			{
				$loop  = FALSE;
				$ligne = $texte;
			}
			else
			{
				$ligne  = substr( $texte, $index, $pos);
				$texte = substr( $texte, $pos+1 );
			}
			$length = floor( $this->GetStringWidth( $ligne ) );
			$res = 1 + floor( $length / $largeur) ;
			$nb_lines += $res;
		}
		return $nb_lines;
	}
	//ENCABEZADO DE PÁGINA
	function Header()
	{
		$x1 = 140;
		$y1 = 6;
		//Variables de encabezado
		global $logotipo_empresa, $nombre_empresa;
		global $reviso, $aprobo,$fecha_reviso,$fecha_aprobo,$codigo,$version,$pagina;

	    $this->SetFont('Arial','B',10);
		$this->SetTextColor(0,0,0);
		$this->SetLineWidth(0);	
		//.....Logo de la compañia
		
		
	}
	//PIE DE PÁGINA
	function Footer()
	{
		$this->SetFont('Arial','B',8);

	}
}
$pdf = new PDF('P','mm', 'A4');
$pdf->SetAutoPageBreak(TRUE,40);
$pdf->AliasNbPages();
$pdf->AddPage();

//CONTENIDO DEL CUERPO DE LA HOJA DE RUTA

		$pdf->SetXY(15,15);
		$pdf->Cell(180,10,'LISTA DE CHEQUEO PREOPERACIONAL',0,0,'C');

		$pdf->SetFont('Arial','',9);
		
		$pdf->SetXY(15,40);
		$pdf->Cell(35,5,utf8_decode('FECHA:'),1,1);
		$pdf->SetXY(50,40);
		$pdf->Cell(40,5,utf8_decode($fecha),1,1);
		$pdf->SetXY(15,45);
		$pdf->Cell(35,5,utf8_decode('PLACA VEHICULO:'),1,1);
		$pdf->SetXY(50,45);
		$pdf->Cell(40,5,utf8_decode($placa_camion),1,1);
		$pdf->SetXY(15,50);
		$pdf->Cell(35,5,utf8_decode('CONDUCTOR:'),1,1);
		$pdf->SetXY(50,50);
		$pdf->Cell(40,5,utf8_decode($nombre_usuario),1,1);
		$pdf->SetXY(15,55);
		$pdf->Cell(35,5,utf8_decode('AGENCIA DE SALIDA:'),1,1);
		$pdf->SetXY(50,55);
		$pdf->Cell(40,5,utf8_decode($salida),1,1);
		$pdf->SetXY(15,60);
		$pdf->Cell(35,5,utf8_decode('ODOMETRO SALIDA:'),1,1);
		$pdf->SetXY(50,60);
		$pdf->Cell(40,5,utf8_decode($odometro_salida),1,1);

		$pdf->SetFont('Arial','B',8);
	
		$pdf->SetXY(15,70);
		$pdf->Cell(112,5,'ASPECTOS DEL VEHICULO',1,1,'C');

		$pdf->SetXY(127,70);
		$pdf->Cell(10,5,'SI',1,1,'C');

		$pdf->SetXY(137,70);
		$pdf->Cell(10,5,'NO',1,1,'C');

		$pdf->SetXY(147,70);
		$pdf->Cell(47,5,'OBSERVACIONES',1,1,'C');

		$pdf->SetFont('Arial','',8);

		$pdf->SetXY(15,75);
		$pdf->Cell(10,10,'1.',1,1,'C');		

		$pdf->SetXY(25,75);
		$pdf->MultiCell(102,4,utf8_decode('El sistema electrico de apagado y encendido del Vehiculo funciona correctamente.'),0,1);

		$pdf->SetXY(25,75);
		$pdf->Cell(102,10,utf8_decode(''),1,1);
			
		$pdf->SetXY(127,75);
		if($apagado_encendido==1){
			$pdf->Cell(10,10,'X',1,1,'C');
		}else{
			$pdf->Cell(10,10,'',1,1,'C');
		}

		$pdf->SetXY(137,75);
		if($apagado_encendido==2){
			$pdf->Cell(10,10,'X',1,1,'C');
		}else{
			$pdf->Cell(10,10,'',1,1,'C');
		}

		$pdf->SetXY(147,75);
		$pdf->MultiCell(47,4,utf8_decode($observaciones_apagado_encendido),0,1);

		$pdf->SetXY(147,75);
		$pdf->Cell(47,10,'',1,1);

		$pdf->SetXY(15,85);
		$pdf->Cell(10,5,'2.',1,1,'C');		

		$pdf->SetXY(25,85);
		$pdf->MultiCell(102,4,utf8_decode('El interruptor electrico central(Master) funciona correctamente.'),0,1);

		$pdf->SetXY(25,85);
		$pdf->Cell(102,5,utf8_decode(''),1,1);
			
		$pdf->SetXY(127,85);
		if($master==1){
			$pdf->Cell(10,5,'X',1,1,'C');
		}else{
			$pdf->Cell(10,5,'',1,1,'C');
		}

		$pdf->SetXY(137,85);
		if($master==2){
			$pdf->Cell(10,5,'X',1,1,'C');
		}else{
			$pdf->Cell(10,5,'',1,1,'C');
		}

		$pdf->SetXY(147,85);
		$pdf->MultiCell(47,4,utf8_decode($observaciones_master),0,1);

		$pdf->SetXY(147,85);
		$pdf->Cell(47,5,'',1,1);



		$pdf->SetXY(15,90);
		$pdf->Cell(10,5,'3.',1,1,'C');		

		$pdf->SetXY(25,90);
		$pdf->MultiCell(102,4,utf8_decode('El vehículo presenta fugas de aceite, liquido de frenos o combustible.'),0,1);

		$pdf->SetXY(25,90);
		$pdf->Cell(102,5,utf8_decode(''),1,1);
			
		$pdf->SetXY(127,90);
		if($fugas_vehiculo==1){
			$pdf->Cell(10,5,'X',1,1,'C');
		}else{
			$pdf->Cell(10,5,'',1,1,'C');
		}

		$pdf->SetXY(137,90);
		if($fugas_vehiculo==2){
			$pdf->Cell(10,5,'X',1,1,'C');
		}else{
			$pdf->Cell(10,5,'',1,1,'C');
		}

		$pdf->SetXY(147,90);
		$pdf->MultiCell(47,4,utf8_decode($observaciones_fugas_vehiculo),0,1);

		$pdf->SetXY(147,90);
		$pdf->Cell(47,5,'',1,1);


		$pdf->SetXY(15,95);
		$pdf->Cell(10,5,'4.',1,1,'C');		

		$pdf->SetXY(25,95);
		$pdf->MultiCell(102,4,utf8_decode('Las llantas del vehículo estan en buen estado.'),0,1);

		$pdf->SetXY(25,95);
		$pdf->Cell(102,5,utf8_decode(''),1,1);
			
		$pdf->SetXY(127,95);
		if($llantas==1){
			$pdf->Cell(10,5,'X',1,1,'C');
		}else{
			$pdf->Cell(10,5,'',1,1,'C');
		}

		$pdf->SetXY(137,95);
		if($llantas==2){
			$pdf->Cell(10,5,'X',1,1,'C');
		}else{
			$pdf->Cell(10,5,'',1,1,'C');
		}

		$pdf->SetXY(147,95);
		$pdf->MultiCell(47,4,utf8_decode($observaciones_llantas),0,1);

		$pdf->SetXY(147,95);
		$pdf->Cell(47,5,'',1,1);

		
		$pdf->SetXY(15,100);
		$pdf->Cell(10,5,'5.',1,1,'C');		

		$pdf->SetXY(25,100);
		$pdf->MultiCell(102,4,utf8_decode('El sistema de frenos del Vehiculo funciona adecuadamente.'),0,1);

		$pdf->SetXY(25,100);
		$pdf->Cell(102,5,utf8_decode(''),1,1);
			
		$pdf->SetXY(127,100);
		if($frenos==1){
			$pdf->Cell(10,5,'X',1,1,'C');
		}else{
			$pdf->Cell(10,5,'',1,1,'C');
		}

		$pdf->SetXY(137,100);
		if($frenos==2){
			$pdf->Cell(10,5,'X',1,1,'C');
		}else{
			$pdf->Cell(10,5,'',1,1,'C');
		}

		$pdf->SetXY(147,100);
		$pdf->MultiCell(47,4,utf8_decode($observaciones_frenos),0,1);

		$pdf->SetXY(147,100);
		$pdf->Cell(47,5,'',1,1);

		$pdf->SetXY(15,105);
		$pdf->Cell(10,15,'6.',1,1,'C');		

		$pdf->SetXY(25,105);
		$pdf->MultiCell(102,4,utf8_decode('El vehiculo cuenta con los equipos y elementos de proteccion para atención de emergencias: extintor de incendios, linterna botiquin de primeros auxilios, control de derrames, gato, cuñas, conos y herramientas.'),0,1);

		$pdf->SetXY(25,105);
		$pdf->Cell(102,15,utf8_decode(''),1,1);
			
		$pdf->SetXY(127,105);
		if($elementos_emergencia==1){
			$pdf->Cell(10,15,'X',1,1,'C');
		}else{
			$pdf->Cell(10,15,'',1,1,'C');
		}

		$pdf->SetXY(137,105);
		if($elementos_emergencia==2){
			$pdf->Cell(10,15,'X',1,1,'C');
		}else{
			$pdf->Cell(10,15,'',1,1,'C');
		}

		$pdf->SetXY(147,105);
		$pdf->MultiCell(47,4,utf8_decode($observaciones_elementos_emergencia),0,1);

		$pdf->SetXY(147,105);
		$pdf->Cell(47,15,'',1,1);


		$pdf->SetXY(15,120);
		$pdf->Cell(10,10,'7.',1,1,'C');		

		$pdf->SetXY(25,120);
		$pdf->MultiCell(102,4,utf8_decode('El vehiculo cuenta con un listado de telefonos para notificacion de emergencias.'),0,1);

		$pdf->SetXY(25,120);
		$pdf->Cell(102,10,utf8_decode(''),1,1);
			
		$pdf->SetXY(127,120);
		if($listado_telefonos==1){
			$pdf->Cell(10,10,'X',1,1,'C');
		}else{
			$pdf->Cell(10,10,'',1,1,'C');
		}

		$pdf->SetXY(137,120);
		if($listado_telefonos==2){
			$pdf->Cell(10,10,'X',1,1,'C');
		}else{
			$pdf->Cell(10,10,'',1,1,'C');
		}

		$pdf->SetXY(147,120);
		$pdf->MultiCell(47,4,utf8_decode($observaciones_listado_telefonos),0,1);

		$pdf->SetXY(147,120);
		$pdf->Cell(47,10,'',1,1);

		$pdf->SetXY(15,130);
		$pdf->Cell(10,10,'8.',1,1,'C');		

		$pdf->SetXY(25,130);
		$pdf->MultiCell(102,4,utf8_decode('El vehiculo y las unidades de transporte estan debidamente identificados de acuerdo al tipo de mercancia que transporta (etiquetas, rombos UN).'),0,1);

		$pdf->SetXY(25,130);
		$pdf->Cell(102,10,utf8_decode(''),1,1);
			
		$pdf->SetXY(127,130);
		if($identificacion_vehiculo==1){
			$pdf->Cell(10,10,'X',1,1,'C');
		}else{
			$pdf->Cell(10,10,'',1,1,'C');
		}

		$pdf->SetXY(137,130);
		if($identificacion_vehiculo==2){
			$pdf->Cell(10,10,'X',1,1,'C');
		}else{
			$pdf->Cell(10,10,'',1,1,'C');
		}

		$pdf->SetXY(147,130);
		$pdf->MultiCell(47,4,utf8_decode($observaciones_identificacion_vehiculo),0,1);

		$pdf->SetXY(147,130);
		$pdf->Cell(47,10,'',1,1);

		$pdf->SetFont('Arial','B',8);
	
		$pdf->SetXY(15,140);
		$pdf->Cell(112,5,'ASPECTOS DE LA CARGA',1,1,'C');

		$pdf->SetXY(127,140);
		$pdf->Cell(10,5,'SI',1,1,'C');

		$pdf->SetXY(137,140);
		$pdf->Cell(10,5,'NO',1,1,'C');

		$pdf->SetXY(147,140);
		$pdf->Cell(47,5,'OBSERVACIONES',1,1,'C');

		$pdf->SetFont('Arial','',8);

		$pdf->SetXY(15,145);
		$pdf->Cell(10,10,'1.',1,1,'C');		

		$pdf->SetXY(25,145);
		$pdf->MultiCell(102,4,utf8_decode('Se cuenta con las hojas de seguridad y tarjetas de emergencias de las mercancias transportadas'),0,1);

		$pdf->SetXY(25,145);
		$pdf->Cell(102,10,utf8_decode(''),1,1);
			
		$pdf->SetXY(127,145);
		if($hojas_seguridad==1){
			$pdf->Cell(10,10,'X',1,1,'C');
		}else{
			$pdf->Cell(10,10,'',1,1,'C');
		}

		$pdf->SetXY(137,145);
		if($hojas_seguridad==2){
			$pdf->Cell(10,10,'X',1,1,'C');
		}else{
			$pdf->Cell(10,10,'',1,1,'C');
		}

		$pdf->SetXY(147,145);
		$pdf->MultiCell(47,4,utf8_decode($observaciones_hojas_seguridad),0,1);

		$pdf->SetXY(147,145);
		$pdf->Cell(47,10,'',1,1);

		$pdf->SetXY(15,155);
		$pdf->Cell(10,5,'2.',1,1,'C');		

		$pdf->SetXY(25,155);
		$pdf->MultiCell(102,4,utf8_decode('La carga ha sido inspeccionada previamente en ausencia de fugas.'),0,1);

		$pdf->SetXY(25,155);
		$pdf->Cell(102,5,utf8_decode(''),1,1);
			
		$pdf->SetXY(127,155);
		if($fugas_carga==1){
			$pdf->Cell(10,5,'X',1,1,'C');
		}else{
			$pdf->Cell(10,5,'',1,1,'C');
		}

		$pdf->SetXY(137,155);
		if($fugas_carga==2){
			$pdf->Cell(10,5,'X',1,1,'C');
		}else{
			$pdf->Cell(10,5,'',1,1,'C');
		}

		$pdf->SetXY(147,155);
		$pdf->MultiCell(47,4,utf8_decode($observaciones_fugas_carga),0,1);

		$pdf->SetXY(147,155);
		$pdf->Cell(47,5,'',1,1);

		$pdf->SetXY(15,160);
		$pdf->Cell(10,10,'3.',1,1,'C');		

		$pdf->SetXY(25,160);
		$pdf->MultiCell(102,4,utf8_decode('La carga se encuentra debidamente embalada, ubicada y ajustada en el vehiculo de transporte de acuerdo con el tipo de mercancia.'),0,1);

		$pdf->SetXY(25,160);
		$pdf->Cell(102,10,utf8_decode(''),1,1);
			
		$pdf->SetXY(127,160);
		if($carga_embalada==1){
			$pdf->Cell(10,10,'X',1,1,'C');
		}else{
			$pdf->Cell(10,10,'',1,1,'C');
		}

		$pdf->SetXY(137,160);
		if($carga_embalada==2){
			$pdf->Cell(10,10,'X',1,1,'C');
		}else{
			$pdf->Cell(10,10,'',1,1,'C');
		}

		$pdf->SetXY(147,160);
		$pdf->MultiCell(47,4,utf8_decode($observaciones_carga_embalada),0,1);

		$pdf->SetXY(147,160);
		$pdf->Cell(47,10,'',1,1);

		/*

		$pdf->SetFont('Arial','B',8);

		$pdf->SetXY(15,75);
		$pdf->Cell(180,10,'',1,1);

		$pdf->SetXY(15,75);
		$pdf->Cell(72,10,utf8_decode('ELEMENTOS QUE SE INSPECCIONAN'),1,1,'C');	

		$pdf->SetXY(87,75);
		$pdf->Cell(50,10,utf8_decode('CRITERIO'),1,1,'C');

		$pdf->SetXY(137,75);
		$pdf->Cell(58,5,'ESTADO(Conforme/No Conforme)',1,1,'C');

		$pdf->SetXY(137,80);
		$pdf->Cell(29,5,'CONFORME',1,1,'C');

		$pdf->SetXY(166,80);
		$pdf->Cell(29,5,'NO CONFORME',1,1,'C');		

		$pdf->SetFont('Arial','',8);

		$pdf->SetXY(15,85);
		$pdf->Cell(36,66,'DOCUMENTOS',1,1);

		$pdf->SetXY(51,85);
		$pdf->MultiCell(36,3,utf8_decode('LICENCIA DE CONDUCCIÓN'),1,1);

		$pdf->SetXY(51,91);
		$pdf->Cell(36,5,utf8_decode('SOAT'),1,1);

		$pdf->SetXY(51,96);
		$pdf->Cell(36,5,utf8_decode('RTM'),1,1);

		$pdf->SetXY(51,101);
		$pdf->MultiCell(36,3,utf8_decode('SEGURO DE DAÑOS Y RC'),1,1);

		$pdf->SetXY(51,107);
		$pdf->MultiCell(36,4,utf8_decode('MANUAL DE SEGURIDAD DE TRANSPORTE DE CILINDROS'),1,1);

		$pdf->SetXY(51,123);
		$pdf->MultiCell(36,4,utf8_decode('TARJETAS DE EMERGENCIA DE LOS GASES TRANSPORTADOS'),1,1);

		$pdf->SetXY(51,139);
		$pdf->MultiCell(36,4,utf8_decode('TELÉFONOS DE EMERGENCIA'),0,1);

		$pdf->SetXY(51,139);
		$pdf->Cell(36,12,utf8_decode(''),1,1);

		$pdf->SetXY(87,85);
		$pdf->MultiCell(50,4,utf8_decode('Los documentos se encuentran en físico y la fecha está vigente'),0,1);

		$pdf->SetXY(87,85);
		$pdf->MultiCell(50,22,utf8_decode(''),1,1);

		$pdf->SetXY(87,107);
		$pdf->MultiCell(50,4,utf8_decode('Constatar que existe un documento informativo sobre la manipulación y transporte de los cilindros'),0,1);

		$pdf->SetXY(87,107);
		$pdf->MultiCell(50,16,utf8_decode(''),1,1);

		$pdf->SetXY(87,123);
		$pdf->MultiCell(50,4,utf8_decode('Verificar que se encuentren las fichas correspondientes y actualizadas de los gases transportados'),0,1);

		$pdf->SetXY(87,123);
		$pdf->MultiCell(50,16,utf8_decode(''),1,1);

		$pdf->SetXY(87,139);
		$pdf->MultiCell(50,4,utf8_decode('Constatar que la lista cuenta con los contactos de los principales agentes de atención de emergencias'),0,1);

		$pdf->SetXY(87,139);
		$pdf->MultiCell(50,12,utf8_decode(''),1,1);

		$pdf->SetXY(137,85);
		if($licencia==1){
			$pdf->Cell(29,6,'X',1,1,'C');
		}else{
			$pdf->Cell(29,6,'',1,1,'C');
		}

		$pdf->SetXY(166,85);
		if($licencia==2){
			$pdf->Cell(29,6,'X',1,1,'C');
		}else{
			$pdf->Cell(29,6,'',1,1,'C');
		}

		$pdf->SetXY(137,91);
		if($soat==1){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(166,91);
		if($soat==2){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(137,96);
		if($rtm==1){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(166,96);
		if($rtm==2){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(137,101);
		if($seguro_dano_rc==1){
			$pdf->Cell(29,6,'X',1,1,'C');
		}else{
			$pdf->Cell(29,6,'',1,1,'C');
		}

		$pdf->SetXY(166,101);
		if($seguro_dano_rc==2){
			$pdf->Cell(29,6,'X',1,1,'C');
		}else{
			$pdf->Cell(29,6,'',1,1,'C');
		}

		$pdf->SetXY(137,107);
		if($manual_seguridad==1){
			$pdf->Cell(29,16,'X',1,1,'C');
		}else{
			$pdf->Cell(29,16,'',1,1,'C');
		}

		$pdf->SetXY(166,107);
		if($manual_seguridad==2){
			$pdf->Cell(29,16,'X',1,1,'C');
		}else{
			$pdf->Cell(29,16,'',1,1,'C');
		}
		
		$pdf->SetXY(137,123);
		if($tarjetas_emergencia==1){
			$pdf->Cell(29,16,'X',1,1,'C');
		}else{
			$pdf->Cell(29,16,'',1,1,'C');
		}

		$pdf->SetXY(166,123);
		if($tarjetas_emergencia==2){
			$pdf->Cell(29,16,'X',1,1,'C');
		}else{
			$pdf->Cell(29,16,'',1,1,'C');
		}

		$pdf->SetXY(137,139);
		if($telefonos_emergencia==1){
			$pdf->Cell(29,12,'X',1,1,'C');
		}else{
			$pdf->Cell(29,12,'',1,1,'C');
		}

		$pdf->SetXY(166,139);
		if($telefonos_emergencia==2){
			$pdf->Cell(29,12,'X',1,1,'C');
		}else{
			$pdf->Cell(29,12,'',1,1,'C');
		}


		$pdf->SetXY(15,151);
		$pdf->Cell(36,10,'DIRECCIONALES',1,1);

		$pdf->SetXY(51,151);
		$pdf->Cell(36,5,utf8_decode('DELANTERAS'),1,1);

		$pdf->SetXY(51,156);
		$pdf->Cell(36,5,utf8_decode('TRASERAS'),1,1);

		$pdf->SetXY(87,151);
		$pdf->MultiCell(50,4,utf8_decode('Funcionamiento adecuado, respuesta inmediata'),0,1);

		$pdf->SetXY(87,151);
		$pdf->MultiCell(50,10,utf8_decode(''),1,1);

		$pdf->SetXY(137,151);
		if($direccionales_delanteras==1){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(166,151);
		if($direccionales_delanteras==2){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(137,156);
		if($direccionales_traseras==1){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(166,156);
		if($direccionales_traseras==2){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(15,161);
		$pdf->Cell(36,25,'LUCES',1,1);

		$pdf->SetXY(51,161);
		$pdf->Cell(36,5,utf8_decode('ALTAS'),1,1);

		$pdf->SetXY(51,166);
		$pdf->Cell(36,5,utf8_decode('BAJAS'),1,1);

		$pdf->SetXY(51,171);
		$pdf->Cell(36,5,utf8_decode('STOPS'),1,1);

		$pdf->SetXY(51,176);
		$pdf->Cell(36,5,utf8_decode('REVERSA'),1,1);

		$pdf->SetXY(51,181);
		$pdf->Cell(36,5,utf8_decode('PARQUEO'),1,1);

		$pdf->SetXY(87,161);
		$pdf->MultiCell(50,4,utf8_decode('Funcionamiento de bombillas, cubierta sin rotura, leds no fundidos'),0,1);

		$pdf->SetXY(87,161);
		$pdf->MultiCell(50,25,utf8_decode(''),1,1);

		$pdf->SetXY(137,161);
		if($luces_altas==1){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(166,161);
		if($luces_altas==2){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(137,166);
		if($luces_bajas==1){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(166,166);
		if($luces_bajas==2){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(137,171);
		if($luces_stops==1){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(166,171);
		if($luces_stops==2){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(137,176);
		if($luces_reversa==1){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(166,176);
		if($luces_reversa==2){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(137,181);
		if($luces_parqueo==1){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(166,181);
		if($luces_parqueo==2){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}


		$pdf->SetXY(15,186);
		$pdf->Cell(36,10,'LIMPIABRISAS',1,1);

		$pdf->SetXY(51,186);
		$pdf->Cell(36,10,utf8_decode('DER/IZQ/ATRÁS'),1,1);

		$pdf->SetXY(87,186);
		$pdf->MultiCell(50,4,utf8_decode('Plumilla en buen estado,(limpieza y estructura)'),0,1);

		$pdf->SetXY(87,186);
		$pdf->MultiCell(50,10,utf8_decode(''),1,1);

		$pdf->SetXY(137,186);
		if($limpiabrisas==1){
			$pdf->Cell(29,10,'X',1,1,'C');
		}else{
			$pdf->Cell(29,10,'',1,1,'C');
		}

		$pdf->SetXY(166,186);
		if($limpiabrisas==2){
			$pdf->Cell(29,10,'X',1,1,'C');
		}else{
			$pdf->Cell(29,10,'',1,1,'C');
		}

		$pdf->SetXY(15,196);
		$pdf->Cell(36,10,'FRENOS',1,1);

		$pdf->SetXY(51,196);
		$pdf->Cell(36,5,utf8_decode('PRINCIPAL'),1,1);

		$pdf->SetXY(51,201);
		$pdf->Cell(36,5,utf8_decode('EMERGENCIA'),1,1);

		$pdf->SetXY(87,196);
		$pdf->MultiCell(50,4,utf8_decode('Verificar cada dia al momento de comenzar la marcha'),0,1);

		$pdf->SetXY(87,196);
		$pdf->MultiCell(50,10,utf8_decode(''),1,1);

		$pdf->SetXY(137,196);
		if($frenos_principal==1){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(166,196);
		if($frenos_principal==2){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(137,201);
		if($frenos_emergencia==1){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(166,201);
		if($frenos_emergencia==2){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(15,206);
		$pdf->Cell(36,15,'LLANTAS',1,1);

		$pdf->SetXY(51,206);
		$pdf->Cell(36,5,utf8_decode('DELANTERAS'),1,1);

		$pdf->SetXY(51,211);
		$pdf->Cell(36,5,utf8_decode('TRASERAS'),1,1);

		$pdf->SetXY(51,216);
		$pdf->Cell(36,5,utf8_decode('REPUESTO'),1,1);

		$pdf->SetXY(87,206);
		$pdf->MultiCell(50,4,utf8_decode('Cada dia, antes de iniciar la marcha, verificar estado, presión y profundidad del labrado'),0,1);

		$pdf->SetXY(87,206);
		$pdf->MultiCell(50,15,utf8_decode(''),1,1);

		$pdf->SetXY(137,206);
		if($llantas_delanteras==1){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(166,206);
		if($llantas_delanteras==2){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(137,211);
		if($llantas_traseras==1){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(166,211);
		if($llantas_traseras==2){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(137,216);
		if($llantas_repuesto==1){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(166,216);
		if($llantas_repuesto==2){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(15,221);
		$pdf->Cell(36,10,'ESPEJOS',1,1);

		$pdf->SetXY(51,221);
		$pdf->Cell(36,5,utf8_decode('LATERALES DER/IZQ'),1,1);

		$pdf->SetXY(51,226);
		$pdf->Cell(36,5,utf8_decode('RETROVISOR'),1,1);

		$pdf->SetXY(87,221);
		$pdf->MultiCell(50,4,utf8_decode('Verificar estado(limpieza, sin rotura, ni opacidad)'),0,1);

		$pdf->SetXY(87,221);
		$pdf->MultiCell(50,10,utf8_decode(''),1,1);

		$pdf->SetXY(137,221);
		if($laterales_der_izq==1){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(166,221);
		if($laterales_der_izq==2){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(137,226);
		if($retrovisor==1){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(166,226);
		if($retrovisor==2){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(15,231);
		$pdf->Cell(72,20,'PLATAFORMA',1,1,'C');

		$pdf->SetXY(87,231);
		$pdf->MultiCell(50,4,utf8_decode('Verificar el buen funcionamiento del sistema mecánico de la plataforma y el buen estado de las cadenas y barras para sujetar los cilindros'),0,1);

		$pdf->SetXY(87,231);
		$pdf->MultiCell(50,20,utf8_decode(''),1,1);

		$pdf->SetXY(137,231);
		if($plataforma==1){
			$pdf->Cell(29,20,'X',1,1,'C');
		}else{
			$pdf->Cell(29,20,'',1,1,'C');
		}

		$pdf->SetXY(166,231);
		if($plataforma==2){
			$pdf->Cell(29,20,'X',1,1,'C');
		}else{
			$pdf->Cell(29,20,'',1,1,'C');
		}

		$pdf->AddPage();

		$pdf->SetXY(15,15);
		$pdf->Cell(72,10,'PITO',1,1,'C');

		$pdf->SetXY(87,15);
		$pdf->MultiCell(50,4,utf8_decode('Accionar antes de iniciar la marcha, debe responder en forma adecuada'),0,1);

		$pdf->SetXY(87,15);
		$pdf->MultiCell(50,10,utf8_decode(''),1,1);

		$pdf->SetXY(137,15);
		if($pito==1){
			$pdf->Cell(29,10,'X',1,1,'C');
		}else{
			$pdf->Cell(29,10,'',1,1,'C');
		}

		$pdf->SetXY(166,15);
		if($pito==2){
			$pdf->Cell(29,10,'X',1,1,'C');
		}else{
			$pdf->Cell(29,10,'',1,1,'C');
		}

		$pdf->SetXY(15,25);
		$pdf->Cell(36,15,'NIVELES DE FLUIDOS',1,1);

		$pdf->SetXY(51,25);
		$pdf->Cell(36,5,utf8_decode('FRENOS'),1,1);

		$pdf->SetXY(51,30);
		$pdf->Cell(36,5,utf8_decode('ACEITE'),1,1);

		$pdf->SetXY(51,35);
		$pdf->Cell(36,5,utf8_decode('REFRIGERANTE'),1,1);	

		$pdf->SetXY(87,25);
		$pdf->MultiCell(50,4,utf8_decode('Verificar que los niveles de los fluidos sean adecuados(reportar fugas)'),0,1);

		$pdf->SetXY(87,25);
		$pdf->MultiCell(50,15,utf8_decode(''),1,1);	

		$pdf->SetXY(137,25);
		if($frenos==1){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(166,25);
		if($frenos==2){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(137,30);
		if($aceite==1){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(166,30);
		if($aceite==2){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(137,35);
		if($refrigerante==1){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(166,35);
		if($refrigerante==2){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(15,40);
		$pdf->Cell(36,10,'APOYA CABEZAS',1,1);

		$pdf->SetXY(51,40);
		$pdf->Cell(36,5,utf8_decode('DELANTEROS'),1,1);

		$pdf->SetXY(51,45);
		$pdf->Cell(36,5,utf8_decode('TRASEROS'),1,1);

		$pdf->SetXY(87,40);
		$pdf->MultiCell(50,4,utf8_decode('Verificar su estado'),0,1);

		$pdf->SetXY(87,40);
		$pdf->MultiCell(50,10,utf8_decode(''),1,1);

		$pdf->SetXY(137,40);
		if($apoya_cabezas_delantero==1){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(166,40);
		if($apoya_cabezas_delantero==2){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(137,45);
		if($apoya_cabezas_trasero==1){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(166,45);
		if($apoya_cabezas_trasero==2){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(15,50);
		$pdf->Cell(72,10,'CINTURONES DE SEGURIDAD DEL / TRAS',1,1,'C');

		$pdf->SetXY(87,50);
		$pdf->MultiCell(50,4,utf8_decode('Verificar estado de las partes (hebillas, tela) y ajuste'),0,1);

		$pdf->SetXY(87,50);
		$pdf->MultiCell(50,10,utf8_decode(''),1,1);

		$pdf->SetXY(137,50);
		if($cinturon_seguridad==1){
			$pdf->Cell(29,10,'X',1,1,'C');
		}else{
			$pdf->Cell(29,10,'',1,1,'C');
		}

		$pdf->SetXY(166,50);
		if($cinturon_seguridad==2){
			$pdf->Cell(29,10,'X',1,1,'C');
		}else{
			$pdf->Cell(29,10,'',1,1,'C');
		}

		$pdf->SetXY(15,65);
		$pdf->MultiCell(36,4,'ULTIMA FECHA DE MANTENIMIENTO',0,1);		


		$pdf->SetXY(15,60);
		$pdf->MultiCell(36,20,'',1,1);

		$pdf->SetXY(51,60);
		$pdf->Cell(36,5,utf8_decode('CAMBIO DE ACEITE'),1,1);

		$pdf->SetXY(51,65);
		$pdf->Cell(36,5,utf8_decode('SINCRONIZACIÓN'),1,1);

		$pdf->SetXY(51,70);
		$pdf->Cell(36,5,utf8_decode('ALINEACIÓN-BALANCEO'),1,1);

		$pdf->SetXY(51,75);
		$pdf->Cell(36,5,utf8_decode('CAMBIO DE LLANTAS'),1,1);

		$pdf->SetXY(87,60);
		$pdf->MultiCell(50,5,utf8_decode('Fecha'),1,1);
		$pdf->SetXY(87,65);
		$pdf->MultiCell(50,5,utf8_decode('Fecha'),1,1);
		$pdf->SetXY(87,70);
		$pdf->MultiCell(50,5,utf8_decode('Fecha'),1,1);
		$pdf->SetXY(87,75);
		$pdf->MultiCell(50,5,utf8_decode('Fecha'),1,1);

		$pdf->SetXY(137,60);
		$pdf->Cell(58,5,utf8_decode($cambio_aceite),1,1,'C');
		$pdf->SetXY(137,65);
		$pdf->Cell(58,5,utf8_decode($sincronizacion),1,1,'C');
		$pdf->SetXY(137,70);
		$pdf->Cell(58,5,utf8_decode($alineacion_balanceo),1,1,'C');
		$pdf->SetXY(137,75);
		$pdf->Cell(58,5,utf8_decode($cambio_llantas),1,1,'C');

		$pdf->SetXY(15,82.5);
		$pdf->MultiCell(36,4,'FECHAs DE VENCIMIENTO',0,1);

		$pdf->SetXY(15,80);
		$pdf->Cell(36,15,'',1,1);

		$pdf->SetXY(51,80);
		$pdf->MultiCell(36,4,utf8_decode('REVISIÓN TÉCNICOMECANICA'),0,1);

		$pdf->SetXY(51,80);
		$pdf->Cell(36,10,'',1,1);

		$pdf->SetXY(51,90);
		$pdf->Cell(36,5,utf8_decode('SOAT'),1,1);

		$pdf->SetXY(87,80);
		$pdf->MultiCell(50,10,utf8_decode('Fecha'),1,1);
		$pdf->SetXY(87,90);
		$pdf->MultiCell(50,5,utf8_decode('Fecha'),1,1);

		$pdf->SetXY(137,80);
		$pdf->Cell(58,10,utf8_decode($fecha_tecnicomecanica),1,1,'C');
		$pdf->SetXY(137,90);
		$pdf->Cell(58,5,utf8_decode($fecha_soat),1,1,'C');

		$pdf->SetXY(15,95);
		$pdf->Cell(72,7.5,'CARRO PORTA CILINDROS',1,1,'C');

		$pdf->SetXY(15,102.5);
		$pdf->Cell(72,7.5,'CARRO PORTA TERMOS',1,1,'C');

		$pdf->SetXY(87,95);
		$pdf->MultiCell(50,4,utf8_decode('Verificar estado de las llantas y condiciones generales de seguridad y limpieza'),0,1);

		$pdf->SetXY(87,95);
		$pdf->MultiCell(50,15,utf8_decode(''),1,1);

		$pdf->SetXY(137,95);
		if($carro_portacilindros==1){
			$pdf->Cell(29,7.5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,7.5,'',1,1,'C');
		}

		$pdf->SetXY(166,95);
		if($carro_portacilindros==2){
			$pdf->Cell(29,7.5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,7.5,'',1,1,'C');
		}

		$pdf->SetXY(137,102.5);
		if($carro_portatermo==1){
			$pdf->Cell(29,7.5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,7.5,'',1,1,'C');
		}

		$pdf->SetXY(166,102.5);
		if($carro_portatermo==2){
			$pdf->Cell(29,7.5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,7.5,'',1,1,'C');
		}

		$pdf->SetXY(15,110);
		$pdf->MultiCell(72,4,'CADENAS EN PLATAFORMA PARA SUJETAR CILINDROS',0,1);

		$pdf->SetXY(15,110);
		$pdf->MultiCell(72,10,'',1,1);

		$pdf->SetXY(87,110);
		$pdf->MultiCell(50,4,utf8_decode('Verificar estado y cantidad'),0,1);

		$pdf->SetXY(87,110);
		$pdf->MultiCell(50,10,utf8_decode(''),1,1);

		$pdf->SetXY(137,110);
		if($cadenas==1){
			$pdf->Cell(29,10,'X',1,1,'C');
		}else{
			$pdf->Cell(29,10,'',1,1,'C');
		}

		$pdf->SetXY(166,110);
		if($cadenas==2){
			$pdf->Cell(29,10,'X',1,1,'C');
		}else{
			$pdf->Cell(29,10,'',1,1,'C');
		}

		$pdf->SetXY(15,120);
		$pdf->MultiCell(72,4,'RIATAS PARA SUJETAR CILINDROS',0,1);

		$pdf->SetXY(15,120);
		$pdf->MultiCell(72,10,'',1,1);

		$pdf->SetXY(87,120);
		$pdf->MultiCell(50,4,utf8_decode('Verificar estado y cantidad'),0,1);

		$pdf->SetXY(87,120);
		$pdf->MultiCell(50,10,utf8_decode(''),1,1);

		$pdf->SetXY(137,120);
		if($cadenas==1){
			$pdf->Cell(29,10,'X',1,1,'C');
		}else{
			$pdf->Cell(29,10,'',1,1,'C');
		}

		$pdf->SetXY(166,120);
		if($cadenas==2){
			$pdf->Cell(29,10,'X',1,1,'C');
		}else{
			$pdf->Cell(29,10,'',1,1,'C');
		}

		$pdf->SetFont('Arial','B',9);

		$pdf->SetXY(15,135);
		$pdf->Cell(30,5,'EQUIPO DE SEGURIDAD',0,1);

		$pdf->SetFont('Arial','',9);

		$pdf->SetXY(15,145);
		$pdf->MultiCell(36,4,'EQUIPOS DE CARRETERA',0,1);

		$pdf->SetXY(15,145);
		$pdf->Cell(36,10,'',1,1);

		$pdf->SetXY(51,145);
		$pdf->Cell(86,10,'CRITERIO',1,1,'C');

		$pdf->SetXY(137,145);
		$pdf->Cell(58,5,'ESTADO(Conforme/No Conforme)',1,1,'C');

		$pdf->SetXY(137,150);
		$pdf->Cell(29,5,'CONFORME',1,1,'C');

		$pdf->SetXY(166,150);
		$pdf->Cell(29,5,'NO CONFORME',1,1,'C');	

		$pdf->SetXY(15,155);
		$pdf->MultiCell(36,10,'EXTINTOR',1,1);

		$pdf->SetXY(51,155);
		$pdf->Cell(86,5,utf8_decode('Vencimiento: '.$vencimiento_extintor),1,1,'C');

		$pdf->SetXY(51,160);
		$pdf->Cell(86,5,utf8_decode('Capacidad: '.$capacidad_extintor),1,1,'C');

		$pdf->SetXY(137,155);
		if($conforme_vencimiento_extintor==1){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(166,155);
		if($conforme_vencimiento_extintor==2){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(137,160);
		if($conforme_capacidad_extintor==1){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(166,160);
		if($conforme_capacidad_extintor==2){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(15,165);
		$pdf->MultiCell(36,10,'HERRAMIENTAS',1,1);

		$pdf->SetXY(51,165);
		$pdf->Cell(86,5,utf8_decode('Alicate, destornilladores y llaves fijas'),1,1,'C');

		$pdf->SetXY(51,170);
		$pdf->Cell(86,5,utf8_decode('Llave Fija'),1,1,'C');

		$pdf->SetXY(137,165);
		if($alicates_destornilladores==1){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(166,165);
		if($alicates_destornilladores==2){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(137,170);
		if($llave_fija==1){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(166,170);
		if($llave_fija==2){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(15,175);
		$pdf->MultiCell(36,5,'CRUCETA',1,1);

		$pdf->SetXY(51,175);
		$pdf->Cell(86,5,utf8_decode('Apta para el vehículo'),1,1,'C');

		$pdf->SetXY(137,175);
		if($cruceta==1){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(166,175);
		if($cruceta==2){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(15,180);
		$pdf->MultiCell(36,5,'GATO',1,1);

		$pdf->SetXY(51,180);
		$pdf->Cell(86,5,utf8_decode('Con capacidad para elevar el vehículo'),1,1,'C');

		$pdf->SetXY(137,180);
		if($gato==1){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(166,180);
		if($gato==2){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(15,185);
		$pdf->MultiCell(36,5,'CONOS',1,1);

		$pdf->SetXY(51,185);
		$pdf->Cell(86,5,utf8_decode('Dos conos con cinta reflectiva '),1,1,'C');

		$pdf->SetXY(137,185);
		if($conos==1){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(166,185);
		if($conos==2){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(15,190);
		$pdf->MultiCell(36,5,'TACOS',1,1);

		$pdf->SetXY(51,190);
		$pdf->Cell(86,5,utf8_decode('Dos tacos para bloquear el vehículo'),1,1,'C');

		$pdf->SetXY(137,190);
		if($conos==1){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(166,190);
		if($conos==2){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}		

		$pdf->SetXY(15,195);
		$pdf->Cell(36,10,utf8_decode('SEÑALES'),1,1);

		$pdf->SetXY(51,195);
		$pdf->MultiCell(86,4,utf8_decode('Rombo de seguridad, señal UN, banda amarilla con franjas negras, roja con franjas blancas de material reflectivo'),0,1);

		$pdf->SetXY(51,195);
		$pdf->Cell(86,10,utf8_decode(''),1,1);

		$pdf->SetXY(137,195);
		if($senales==1){
			$pdf->Cell(29,10,'X',1,1,'C');
		}else{
			$pdf->Cell(29,10,'',1,1,'C');
		}

		$pdf->SetXY(166,195);
		if($senales==2){
			$pdf->Cell(29,10,'X',1,1,'C');
		}else{
			$pdf->Cell(29,10,'',1,1,'C');
		}

		$pdf->SetXY(15,205);
		$pdf->MultiCell(36,5,'CHALECO',1,1);

		$pdf->SetXY(51,205);
		$pdf->Cell(86,5,utf8_decode('Debe ser reflectivo'),1,1,'C');

		$pdf->SetXY(137,205);
		if($chaleco==1){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(166,205);
		if($chaleco==2){
			$pdf->Cell(29,5,'X',1,1,'C');
		}else{
			$pdf->Cell(29,5,'',1,1,'C');
		}

		$pdf->SetXY(15,210);
		$pdf->Cell(36,20,utf8_decode('BOTIQUIN'),1,1);

		$pdf->SetXY(51,210);
		$pdf->MultiCell(86,4,utf8_decode('Yodopividona solución antiséptico bolsa (120 ml), jabón, gasas, curas, venda elástica, rollo micropore, algodón, sales de rehidratación oral, baja lenguas, bolsa de suero fisiológico, guantes de látex desechables, toallas higiénicas, tijeras y termómetro oral'),0,1);

		$pdf->SetXY(51,210);
		$pdf->Cell(86,20,utf8_decode(''),1,1);

		$pdf->SetXY(137,210);
		if($botiquin==1){
			$pdf->Cell(29,20,'X',1,1,'C');
		}else{
			$pdf->Cell(29,20,'',1,1,'C');
		}

		$pdf->SetXY(166,210);
		if($botiquin==2){
			$pdf->Cell(29,20,'X',1,1,'C');
		}else{
			$pdf->Cell(29,20,'',1,1,'C');
		}*/
			
//Datos de archivo de descarga
$file_name = str_replace(' ','_',$nombre_empresa_ajustado)."_Factura_No._".$num_factura.".pdf";
$file_destination = $path_pdf.$file_name;
$pdf->Output($file_destination,'F');
$pdf->Output($file_name,'I');
?>