<?php
date_default_timezone_set("America/Bogota");
require ("libraries/conexion.php");
require ("libraries/seguimiento.php");
$fecha = date("Y-m-d");
$hora = date("G:i:s",time());
session_start();

$cilindros_completos = 0;

if(@$_SESSION['logged']== 'yes')
{ 		
	//$id_produccion_mezclas = isset($_REQUEST['id_produccion_mezclas']) ? $_REQUEST['id_produccion_mezclas'] : NULL;
	$id_cilindro_eto1 = isset($_REQUEST['id_cilindro_eto']) ? $_REQUEST['id_cilindro_eto'] : NULL;
  	$id_orden_especial = isset($_REQUEST['id_orden_especial']) ? $_REQUEST['id_orden_especial'] : NULL;
  	$abrir = isset($_REQUEST['abrir']) ? $_REQUEST['abrir'] : NULL;
  	$cerrar = isset($_REQUEST['cerrar']) ? $_REQUEST['cerrar'] : NULL;
  	$facturar = isset($_REQUEST['facturar']) ? $_REQUEST['facturar'] : NULL;
    $User_idUser =$_SESSION['su'];
    $acc = $_SESSION['acc'];
    $orden_fini="";

    /*if ($abrir==1) 
    {
    	$consulta5  = "UPDATE ordenes_especiales 
					SET id_abrir = 1
    				WHERE id_orden_especial =".$id_orden_especial;
	    $resultado5 = mysqli_query($con,$consulta5) ;
	    if ($resultado5 == FALSE)
	    {
	       	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
	    }
	    else
	    {
	    	?>
	    	<script type="text/javascript">
	    		window.location = ("produccion_especial.php?id_orden_especial="+<?php echo $id_orden_especial; ?>+"");
	    	</script>
	    	
	    	<?php
	    }
    }*/
    if ($cerrar==1) 
    {
    	$consulta5  = "UPDATE ordenes_especiales
						SET id_estado='5'
    					WHERE id_orden_especial =".$id_orden_especial;
	    $resultado5 = mysqli_query($con,$consulta5) ;
	    if ($resultado5 == FALSE)
	    {
	       	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
	    }
	    else
	    {
	    	?>
	    	<script type="text/javascript">
	    		window.location = ("produccion_especial.php?id_orden_especial="+<?php echo $id_orden_especial; ?>+"");
	    	</script>
	    	
	    	<?php
	    }
    }
    /*if ($facturar==1) 
    {
    	$Fecha = date("Y/m/d");
    	$consulta5  = "UPDATE ordenes_especiales 
						SET fecha_factura='$Fecha'
    					WHERE id_orden_especial =".$id_orden_especial;
	    $resultado5 = mysqli_query($con,$consulta5) ;
	    if ($resultado5 == FALSE)
	    {
	       	echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
	    }
	    else
	    {
	    	?>
	    	<script type="text/javascript">
	    		window.location = ("produccion_especial.php?id_orden_especial="+<?php echo $id_orden_especial; ?>+"");
	    	</script>
	    	
	    	<?php
	    }
    }*/
	
	if(isset($_POST['g_cilindro'])){

		$fecha_hoy = date("Y-m-d");

		$id_orden_especial1 = $_POST['id_orden_especial'];
		
		$id_cilindro_eto1 = $_POST['id_cilindro_eto'];

		$consulta6 = "SELECT * FROM has_orden_especial_cilindro WHERE id_orden_especial = '".$id_orden_especial1."'";
		$resultado6 = mysqli_query($con,$consulta6);

		if(mysqli_num_rows($resultado6)>1){

			$consulta7 = "SELECT SUM(  `cantidad` ) AS total FROM  `has_orden_especial_cilindro` WHERE  `id_orden_especial` ='".$id_orden_especial."'";
			$resultado7 = mysqli_query($con,$consulta7);

			$linea7 = mysqli_fetch_array($resultado7);

			$total_cilindros_llenar = $linea7["total"];

			$consulta8 = "SELECT COUNT(  `id_produccion_especial` ) AS llenados FROM  `produccion_especiales` WHERE  `id_orden_especial` ='".$id_orden_especial."'";
			$resultado8 = mysqli_query($con,$consulta8);

			$linea8 = mysqli_fetch_array($resultado8);

			$total_cilindros_llenados = $linea8["llenados"];

			if($total_cilindros_llenados<$total_cilindros_llenar){

				$consulta9 = "SELECT id_tipo_cilindro FROM cilindro_eto WHERE id_cilindro_eto = '".$id_cilindro_eto1."'";
				$resultado9 = mysqli_query($con,$consulta9);

				$linea9 = mysqli_fetch_array($resultado9);

				$id_tipo_cilindro = $linea9["id_tipo_cilindro"];


				$consulta10 = "SELECT cantidad FROM has_orden_especial_cilindro WHERE id_orden_especial = '".$id_orden_especial."' AND id_tipo_cilindro = '".$id_tipo_cilindro."'";
				$resultado10 = mysqli_query($con,$consulta10);

				$linea10 = mysqli_fetch_array($resultado10);

				$cantidad_llenar_por_tipo = $linea10["cantidad"];


				$consulta11 = "SELECT COUNT(  `id_produccion_especial` ) AS llenados_por_tipo FROM  `produccion_especiales` WHERE  `id_orden_especial` ='".$id_orden_especial."' AND `id_tipo_cilindro` = '".$id_tipo_cilindro."'";
				$resultado11 = mysqli_query($con,$consulta11);

				$linea11 = mysqli_fetch_array($resultado11);

				$cantidad_llenada_por_tipo = $linea11["llenados_por_tipo"];

				if($cantidad_llenada_por_tipo<$cantidad_llenar_por_tipo){
					$consulta9 = "UPDATE cilindro_eto SET id_estado = 12 WHERE id_cilindro_eto = '".$id_cilindro_eto1."'";
					$resultado9 = mysqli_query($con,$consulta9);

					$consulta10 = "INSERT INTO produccion_especiales (id_orden_especial, id_cilindro_eto, fecha_creacion) 
									VALUES ($id_orden_especial1, $id_cilindro_eto1,'$fecha_hoy')";
					$resultado10 = mysqli_query($con,$consulta10);

					if($resultado10){
						?>
						<script type="text/javascript">
							window.location = ("produccion_especial.php?id_orden_especial="+<?php echo $id_orden_especial1; ?>+"");
						</script>
						<?php
					}
				}

				
			}
		}else if(mysqli_num_rows($resultado6)==1){
			

			$consulta12 = "SELECT cantidad FROM has_orden_especial_cilindro WHERE id_orden_especial = '".$id_orden_especial."'";
			$resultado12 = mysqli_query($con,$consulta12);

			$linea12 = mysqli_fetch_array($resultado12);

			$cilindros_a_llenar = $linea12["cantidad"];

			$consulta13 = "SELECT COUNT(  `id_produccion_especial` ) AS llenados FROM  `produccion_especiales` WHERE  `id_orden_especial` ='".$id_orden_especial."'";
			$resultado13 = mysqli_query($con,$consulta13);

			$linea13 = mysqli_fetch_array($resultado13);

			$cilindros_llenados = $linea13["llenados"];
			

			if($cilindros_a_llenar>$cilindros_llenados){

				$consulta14 = "UPDATE cilindro_eto SET id_estado = 12 WHERE id_cilindro_eto = '".$id_cilindro_eto1."'";
				$resultado14 = mysqli_query($con,$consulta14);

				$consulta15 = "INSERT INTO produccion_especiales (id_orden_especial, id_cilindro_eto, fecha_creacion) VALUES ($id_orden_especial1, $id_cilindro_eto1, $fecha)";
				$resultado15 = mysqli_query($con,$consulta15);

				if($resultado15){
					?>
					<script type="text/javascript">
						window.location = ("produccion_especial.php?id_orden_especial="+<?php echo $id_orden_especial1; ?>+"");
					</script>
					<?php
				}
			}

		}


	}
	
	if(strlen($id_orden_especial) > 0)
	{ 
		$id_tipo_cilindro2 = "";
		$contador_completados = 0;
	  	$consulta  = "SELECT * FROM has_orden_especial_cilindro WHERE id_orden_especial= '".$id_orden_especial."'";
	  	$resultado = mysqli_query($con,$consulta) ;
	  	$linea = mysqli_fetch_array($resultado);
	  	
		$fecha = isset($linea["fecha"]) ? $linea["fecha"] : NULL;
		$id_orden_especial = isset($linea["id_orden_especial"]) ? $linea["id_orden_especial"] : NULL;
		$id_tipo_cilindro1 = isset($linea["id_tipo_cilindro"]) ? $linea["id_tipo_cilindro"] : NULL;	
		$id_tipo_envace1 = isset($linea["id_tipo_envace"]) ? $linea["id_tipo_envace"] : NULL;
		$id_finalizar = isset($linea["id_finalizar"]) ? $linea["id_finalizar"] : NULL;
		$total = isset($linea["cantidad"]) ? $linea["cantidad"] : NULL;	
		$cantidad = isset($linea["cantidad"]) ? $linea["cantidad"] : NULL;

		mysqli_free_result($resultado);

	  	$consulta2  = "SELECT * FROM tipo_cilindro WHERE id_tipo_cilindro = '".$id_tipo_cilindro1."'";
	  	$resultado2 = mysqli_query($con,$consulta2) ;
	  	$linea2 = mysqli_fetch_array($resultado2);
	  	$id_tipo_cilindro = isset($linea2["tipo_cili"]) ? $linea2["tipo_cili"] : NULL;
	  	mysqli_free_result($resultado2);

	  	$consulta3  = "SELECT * FROM tipo_envace WHERE id_tipo_envace= $id_tipo_envace1";
	  	$resultado3 = mysqli_query($con,$consulta3) ;
	  	$linea3 = mysqli_fetch_array($resultado3);
	  	$id_tipo_envace = isset($linea3["tipo"]) ? $linea3["tipo"] : NULL;		
	    mysqli_free_result($resultado3);

	    $consulta4 = "SELECT SUM(  `cantidad` ) AS total FROM  `has_orden_especial_cilindro` WHERE  `id_orden_especial` ='".$id_orden_especial."'";
	    $resultado4 = mysqli_query($con,$consulta4);

	    $linea4 = mysqli_fetch_assoc($resultado4);

	    $cantidad_total = isset($linea4["total"]) ? $linea4["total"] : NULL;

	    $consulta5 = "SELECT * FROM has_orden_especial_cilindro WHERE id_orden_especial = '".$id_orden_especial."'";
	    $resultado5 = mysqli_query($con,$consulta5);

	    if(mysqli_num_rows($resultado5)>1){
	    	
	    	while ($linea5=mysqli_fetch_array($resultado5)) {
	    		
	    		$id_tipo_cilindro3 = isset($linea5["id_tipo_cilindro"]) ? $linea5["id_tipo_cilindro"] : NULL;
	    		$id_tipo_cilindro2 = $id_tipo_cilindro2.$id_tipo_cilindro3.";";
	    	}
	    }

	    $consulta11 = "SELECT COUNT(  `id_produccion_especial` ) AS llenados FROM  `produccion_especiales` WHERE  `id_orden_especial` ='".$id_orden_especial."'";
	    $resultado11 = mysqli_query($con,$consulta11);

	    $linea11 = mysqli_fetch_assoc($resultado11);

	    $llenados = $linea11["llenados"];

	    if($llenados==$cantidad_total){
	    	$contador_completados++;
	    }


  		
	}
	if(strlen($id_orden_especial) > 0)
	{ 
	  	$consulta  = "SELECT * FROM ordenes_especiales WHERE id_orden_especial= $id_orden_especial";
	  	$resultado = mysqli_query($con,$consulta) ;
	  	$linea = mysqli_fetch_array($resultado);
	  	$num_ord = isset($linea["num_ord"]) ? $linea["num_ord"] : NULL;
		$fecha_ven = isset($linea["fecha_ven"]) ? $linea["fecha_ven"] : NULL;
		$id_estado = isset($linea["id_estado"]) ? $linea["id_estado"] : NULL;	
		$fecha_factura = isset($linea["fecha_factura"]) ? $linea["fecha_factura"] : NULL;
		$fecha_factura = isset($linea["fecha_factura"]) ? $linea["fecha_factura"] : NULL;	
		$id_abrir1 = isset($linea["id_abrir"]) ? $linea["id_abrir"] : NULL;
			
	    mysqli_free_result($resultado);
	}  
	require_once("inc/init.php");
	require_once("inc/config.ui.php");
	$page_title = "Produccion de Mezclas";
	$page_css[] = "your_style.css";
	include("inc/header.php");
	$page_nav[""][""][""][""] = true;
	include("inc/nav.php");
?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<!-- Modal -->
<div class="modal fade" id="modal_orden" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-den="true">
	<form name="mezcla" action="produccion_especial.php" method="POST" id="mezcla">
	<input type="hidden" name="id_cilindro_eto"  value="id_cilindro_eto">
	<input type="hidden" name="id_orden" id="id_orden" value="<?php echo $id_orden; ?>">
	<input type="hidden" name="id_produccion_especial" id="id_produccion_especial" value="<?php echo $id_produccion_especial; ?>">
	<input type="hidden" name="id_num_cilindro" id="id_num_cilindro" value="<?php echo $id_num_cilindro; ?>">
	<input type="hidden" name="total" id="total" value="<?php echo $total; ?>">
	<input type="hidden" name="alerta" id="alerta" value="<?php echo $alerta; ?>">
	<input type="hidden" name="uso_actual" id="uso_actual" value="<?php echo $uso_actual; ?>">
	<input type="hidden" name="alerta_eto" id="alerta_eto" value="<?php echo $alerta_eto; ?>">
	<input type="hidden" name="uso_actual_eto" id="uso_actual_eto" value="<?php echo $uso_actual_eto; ?>">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" id ="cerrar"aria-hidden="true">
					&times;
					</button>
					<h4 class="modal-title" id="myModalLabel">Orden</h4>
				</div>
				<div class="modal-body">
				<div class="well well-sm well-primary">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="category">Numero Cilindro :</label>
								<input type="text" class="form-control" placeholder="Numero Cilindro" name="num_cili_eto" readonly required />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="category">Prueba Hidrostatica :</label>
								<input type="date" class="form-control" placeholder="Prueba Hidrostatica" name="prue_hidro" readonly required />
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" id="Cancelar" data-dismiss="modal">	Cancelar</button>
						<input type="hidden" name="id_orden_especial" value="<?php echo $id_orden_especial; ?>">
						<input type="submit" value="Guardar" name="g_cilindro" id="g_cilindro" class="btn btn-primary" />
					
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</form>
</div><!-- /.modal -->
<!-- Fin Modal -->
 </div>
<div id="main" role="main">
	<div id="content">
		<section id="widget-grid" class="">
			<div class="row">
				<article class="col-sm-12 col-md-12 col-lg-6">
					<div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h6 id="eg1">Producción Mezclas</h6>				
						</header>
						<div>				
						<div class="jarviswidget-editbox"></div>
							<div class="widget-body no-padding">												
								<input type="hidden" name="id_orden"  value="<?php echo $id_orden; ?>">
								<input type="hidden" name="id_tipo_cilindro1"  value="<?php echo $id_tipo_cilindro1; ?>">
								<input type="hidden" name="id_tipo_cilindro2" value="<?php echo $id_tipo_cilindro2; ?>">
								<div class="row">					
									<div class="col-md-3">
										<div class="form-group">
											<label for="category">Num Orden :</label>											 
											<input type="text" readonly class="form-control"  placeholder="Num Orden" value="<?php echo isset($num_ord) ? $num_ord : NULL; ?>" />											
										</div>
									</div>	
									<div class="col-md-3">	
										<div class="form-group">
											<label for="category">Tipo de Orden :</label>											 
											<input type="text" readonly class="form-control" placeholder="Tipo Gas" value="Especial" />											
										</div>
									</div>	
									<div class="col-md-4">	
										<div class="form-group">
											<label for="category">Cantidad Total de Cilindros :</label>											 
											<input type="text" readonly class="form-control" placeholder="Cantidad" value="<?php echo isset($cantidad_total) ? $cantidad_total : NULL; ?>" />											
										</div>
									</div>							
								</div>
								<?php
								if($contador_completados==0){
									?>
									<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label for="category">Número cilindro :</label>											 
												<input type="text" class="form-control" name="num_cili" id="cilindro" placeholder="Número cilindro" value="" />				
											</div>
										</div>
									</div>
									<?php
								}else if(($contador_completados==1)&&($id_estado != 5)){
									?>
									<!---->
									<div class="col-md-3">
										<div class="form-group">
											<a class="btn btn-danger" href="produccion_especial.php?cerrar=1&id_orden_especial=<?php echo $id_orden_especial; ?>">Cerrar Orden</a>
										</div>
									</div>
									<?php
								}else if($id_estado == 5){
									?>
									<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label for="category">Orden Completa</label>										 
												<!--<a href="produccion_especial_pdf.php?id_orden_especial=<?php echo $id_orden_especial; ?>" target="_blank"><img src="img/pdf-icon.png" width="50" height="50"></a>-->
												<a href="javascript:imprSelec('muestra')"><img src="/img/pdf-icon.png" width="50" height="50"></a>			
											</div>
										</div>
									</div>
									<?php
								}
								?>
							</div>
						</div>					
					</div>
					<div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
							<h6>Cilindros Llenos</h6>			
						</header>
						<div>				
						<div class="jarviswidget-editbox"></div>																	
							<table class="table table-bordered">							
								<thead>
									<tr>
										<th>#</th>                                                
			                            <th>Cilindro</th>
			                            <th>Tipo Cilindro</th>
			                            <?php
			                            if ($id_estado!=5) {
			                            	?>
			                            	<th>Eliminar</th>    
			                            	<?php
			                            }
			                            ?>
			                                 
									</tr>
								</thead>
								<tbody>													
								  <?php
			                          $contador = "0";	                
			                          $consulta = "SELECT * FROM produccion_especiales WHERE id_orden_especial=".$id_orden_especial;
			                          $resultado = mysqli_query($con,$consulta) ;
			                          while ($linea = mysqli_fetch_array($resultado))
			                          {
			                            $contador = $contador + 1;
			                            $id_orden_especial = $linea["id_orden_especial"];
			                            $id_cilindro_eto = $linea["id_cilindro_eto"];
			                            $id_produccion_especial = $linea["id_produccion_especial"];
													                   		                            

			                            $consulta1 = "SELECT * FROM  cilindro_eto WHERE id_cilindro_eto = $id_cilindro_eto";
			                            $resultado1 = mysqli_query($con,$consulta1) ;
			                            while ($linea1 = mysqli_fetch_array($resultado1))
			                            {
			                            	$num_cili_eto = $linea1["num_cili_eto"];
			                            	$id_tipo_cilindro = $linea1["id_tipo_cilindro"];

			                            	$consulta2 = "SELECT * FROM tipo_cilindro WHERE id_tipo_cilindro = '".$id_tipo_cilindro."'";
			                            	$resultado2 = mysqli_query($con,$consulta2);

			                            	$linea2 = mysqli_fetch_array($resultado2);

			                            	$tipo_cili = $linea2["tipo_cili"];
			                            }			                            		                            
			                            ?>
			                            <tr class="odd gradeX">
			                              	<td width="5"><?php echo $contador; ?></td>                                  
			                              	<td><?php echo $num_cili_eto; ?></td>
			                              	<td><?php echo $tipo_cili; ?></td>
			                              	<?php
											if ($id_estado!=5) 
											{
											?>
			                              	<td width="100" style="vertical-align:bottom;">
			                              		<a href="eliminar_cili.php?id_produccion_mezclas=<?php echo $id_produccion_mezclas; ?>&id_cilindro_eto=<?php echo $id_cilindro; ?>&id_orden=<?php echo $id_orden; ?>"><input type="image"  src="img/eliminar.png" width="35" height="35"></a>
			                              	</td>
			                              	<?php
											}											
											?>                       
			                            </tr>                           
			                            <?php
			                            }mysqli_free_result($resultado);                                   
		                  		    ?>
								</tbody>							
							</table>												
						</div>           
         			</div>
         		</article>
     		 </div>
   		</section> 
    </div>
</div>
<div id="muestra" style="display:none">
	<?php
	$fecha = date("Y-m-d");
	$id_tipo_cilindro1 = "";
	$tipo_envace1 = "";
	$consulta16 = "SELECT * FROM has_orden_especial_cilindro WHERE id_orden_especial = '".$id_orden_especial."'";
	$resultado16 = mysqli_query($con,$consulta16);

	if(mysqli_num_rows($resultado16)>1){
		while ($linea16=mysqli_fetch_array($resultado16)) {
			$id_tipo_cilindro = isset($linea16["id_tipo_cilindro"]) ? $linea16["id_tipo_cilindro"] : NULL;
			$id_tipo_envace = isset($linea16["id_tipo_envace"]) ? $linea16["id_tipo_envace"] : NULL;

			$consulta17 = "SELECT * FROM tipo_cilindro WHERE id_tipo_cilindro = '".$id_tipo_cilindro."'";
			$resultado17 = mysqli_query($con,$consulta17);

			$linea17 = mysqli_fetch_array($resultado17);

			$tipo_cilindro = $linea17["tipo_cili"];

			$id_tipo_cilindro1 .= $tipo_cilindro." - ";

			if($id_tipo_cilindro == 5){
				$tipo_envace = 25;
			}else if($id_tipo_cilindro == 6){
				$tipo_envace = 5.8;
			}else if($id_tipo_cilindro == 7){
				$tipo_envace = 6.4;
			}else if($id_tipo_cilindro == 8){
				$tipo_envace = 6;
			}else if($id_tipo_cilindro == 9){
				$tipo_envace = 6.3;
			}

			$tipo_envace1 .= $tipo_envace." - ";

		}
	}else{
		$linea16=mysqli_fetch_array($resultado16);

		$id_tipo_cilindro = isset($linea16["id_tipo_cilindro"]) ? $linea16["id_tipo_cilindro"] : NULL;
		$id_tipo_envace = isset($linea16["id_tipo_envace"]) ? $linea16["id_tipo_envace"] : NULL;

		$consulta17 = "SELECT * FROM tipo_cilindro WHERE id_tipo_cilindro = '".$id_tipo_cilindro."'";
		$resultado17 = mysqli_query($con,$consulta17);
		$linea17 = mysqli_fetch_array($resultado17);

		$tipo_cilindro = $linea17["tipo_cili"];

		$id_tipo_cilindro1 = $tipo_cilindro;

		if($id_tipo_cilindro == 5){
			$tipo_envace = 25;
		}else if($id_tipo_cilindro == 6){
			$tipo_envace = 5.8;
		}else if($id_tipo_cilindro == 7){
			$tipo_envace = 6.4;
		}else if($id_tipo_cilindro == 8){
			$tipo_envace = 6;
		}else if($id_tipo_cilindro == 9){
			$tipo_envace = 6.3;
		}

		$tipo_envace1 = $tipo_envace;
	}
	?>
		<p>INGEGAS-INGENIERIA Y GASES LTDA</p>
		<h2 align="center">Informe Producción de Cilindros Especiales</h2>
		<table class="table table-bordered" style="border: 1px solid black;border-collapse: collapse;" align="center">							
			<thead>
				<tr>                                               
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Num Orden </th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Tipo Gas</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Capacidad</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Cantidad</th>
                    <th style="border: 1px solid black;border-collapse: collapse;" align="center">Fecha</th>         
				</tr>
			</thead>
			<tbody>
                <tr class="odd gradeX">                                
                  	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $num_ord; ?></td>
                  	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $id_tipo_cilindro1; ?></td>
                  	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $tipo_envace1; ?></td>  
                  	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $cantidad; ?></td>  
                  	<td style="border: 1px solid black;border-collapse: collapse;" align="center"><?php echo $fecha; ?></td>
                </tr>
			</tbody>							
		</table>		
		<br>
		<br>		
		<br>
		<br>			
	</div>
<?php
  include("inc/footer.php");
  include("inc/scripts.php"); 
?>
<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/jquery-form/jquery-form.min.js"></script>
<script src="js/plugin/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<script type="text/javascript">
	function imprSelec(muestra)
	{
		var ficha=document.getElementById(muestra);
		var ventimp=window.open(' ','popimpr');
		ventimp.document.write(ficha.innerHTML);
		ventimp.document.close();
		ventimp.print();
		//ventimp.close();
	}
</script>
<script>
	$('#mezcla').on('keyup keypress', function(e) {
	  	var keyCode = e.keyCode || e.which;
	  	if (keyCode === 13) 
	  	{ 
	    	e.preventDefault();
	    	return false;
	  	}
	});
</script>
<script>
$(document).ready(function(){
    $("#g_cilindro").click(function(){
        $('#g_cilindro').hide();
        alert("Por Favor Espere,su informacion se esta transmitiendo");
    });
});
</script>
<script>
$(document).ready(function(){
    $("#cancelar").click(function(){
        $('#g_cilindro').show();
    });
});
</script>
<script type="text/javascript">
	function igualar()
	{
		mezcla.tara_vacio.value = (parseFloat(mezcla.peso_final.value));
	}
</script>

<script type="text/javascript">
	/*$(document).ready(function() 
	{

	  	var uso = '<?php echo $uso_actual_eto; ?>';
	  	var alerta = '<?php echo $alerta_eto; ?>';
	    
	    if(parseInt(uso)<=parseInt(alerta)) 
	    {
	      $.bigBox({
	        title : "Alerta Insumo ETO",
	        content : "El contenido del Cilindro se está agotando. Por favor tenga en cuenta el restante para las siguientes órdenes de producción.",
	        color : "#C46A69",
	        //timeout: 6000,
	        icon : "fa fa-warning shake animated",
	        number : "1",
	        timeout : 60000
	      });
	    }
	    else
	    {
	      $.bigBox({
	        title : "Alerta Insumo ETO",
	        content : "Contenido disponible.",
	        color : "#28B463",
	        //timeout: 6000,
	        icon : "fa fa-check-square-o shake animated",
	        number : "1",
	        timeout : 60000
	      });
	    }
	});*/
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#cilindro").keypress(function(e) {
	       if(e.which == 13) 
	       {        
		    	document.getElementById("mezcla").reset();
		    	var num_cili1 = $('input:text[name=num_cili]').val();
		    	//var num_cili1 = document.checkout_form.num_cili.value;	    	
				validar(num_cili1);	          
	       }
	    });		    
	    function validar(dato)
		{	
			//alert(dato);
			var id_tipo_cilindro1 = $('input:hidden[name=id_tipo_cilindro1]').val();
			var id_tipo_cilindro2 = $('input:hidden[name=id_tipo_cilindro2]').val();
			var id_orden = $('input:hidden[name=id_orden]').val();
			var contador = 0;
			var contador2 = 0;
			//alert(id_orden);
			
			/*if($id_tipo_cilindro2 != ""){

			}*/

			$.ajax({
				url: 'id_cili2.php',
				type: 'POST',
				data: 'dato='+dato+'&dato1='+id_orden,
			})
			.done(function(data) {
				var objeto = JSON.parse(data);							
				var id_cili= objeto.id;
				var tipo_cili= objeto.tipo_cili;
				var tipo_envace= objeto.tipo_envace;
				var variable= objeto.variable;
				//alert(id_tipo_cilindro1); 			
				if (variable==2) 
				{	
					if(id_tipo_cilindro2 != ""){

						arrayTiposdeCilindro = id_tipo_cilindro2.split(";");
						for(i=0;i<(arrayTiposdeCilindro.length-1);i++){
							id_tipo_cilindro3 = arrayTiposdeCilindro[i];
							if (tipo_cili==id_tipo_cilindro3) 
							{							
								form_cili(id_cili);
								$("#modal_orden").modal(); 
								contador2++
							}
							contador++;
						}
						if((contador==(arrayTiposdeCilindro.length-1))&&(contador2==0)){
							alert("Por Favor seleccione un cilindro de otro tipo de mezcla ");
						}


					}else{
						if (tipo_cili==id_tipo_cilindro1) 
						{							
							form_cili(id_cili);
							$("#modal_orden").modal(); 
						}
						else
						{
							alert("Por Favor seleccione un cilindro de otro tipo de mezcla ");
						}
					}
					
						
				}
				if(variable==3)
				{				
					alert("Por Favor seleccione un cilindro valido para la mezcla");
				}
				if (variable==1) 
				{
					alert("Orden ya esta terminada ");
				}
				if (variable==4) 
				{
					alert("Orden se termino");
				}
							
			})
			.fail(function() {
				console.log("error");
			});
		}

		function form_cili(id_cili)
		{	
			//alert(tipo_envace);
			$.ajax({
				url: 'id_cili3_especial.php',
				type: 'POST',
				data: 'dato='+id_cili,
			})
			.done(function(data) {
				var objeto = JSON.parse(data);							
				$("input[name=id_cilindro_eto]").val(objeto.id);
				$("input[name=num_cili_eto]").val(objeto.num_cili_eto);
				$("input[name=prue_hidro]").val(objeto.fecha_ult_eto);

				campo=objeto.tipo_cili;
				if (campo=="ETO-10") 
				{
					valor= 10;
				}
				if (campo=="ETO-20") 
				{
					valor= 20;
				}
				if (campo=="ETO-90") 
				{
					valor= 90;
				}
				if (campo=="ETO-100") 
				{
					valor= 100;
				}

				var restante = 100-valor;
				var por_valor=valor*(0.01);
				var por_res=restante*(0.01);

			    var form = document.forms.mezcla;
			    form.oninput = function() 
			    {  
			        form.esperado_eto.value = ((parseFloat(form.peso_esperado.value))*por_valor).toFixed(2);
			        form.esperado_co.value = ((parseFloat(form.peso_esperado.value))*por_res).toFixed(2);
			        // form.tara_vacio.value = form.peso_final.value;
			        form.peso_final_1.value = ((parseFloat(form.real_eto.value))+(parseFloat(form.real_co.value))).toFixed(2);
			        form.desviacion.value = (Math.abs(((parseFloat(form.peso_esperado.value))-(parseFloat(form.peso_final_1.value)))/(parseFloat(form.peso_esperado.value)) *(100))).toFixed(2);
			        
			    }			
			})
			.fail(function() {
				console.log("error");
			});
		}		
	});

	function editar(cili)
	{
		//var cili=1;
		//alert(cili);
		$("#modal_orden").modal();
		$.ajax({
			url: 'edit_cili.php',
			type: 'POST',
			data: 'dato='+cili,
		})
		.done(function(data) {
			var objeto = JSON.parse(data);							
			$("input[name=id_produccion_mezclas]").val(objeto.id);
			$("input[name=id_cilindro_eto]").val(objeto.id_cilindro_eto);
			$("input[name=id_orden]").val(objeto.id_orden);
			$("input[name=num_cili_eto]").val(objeto.id_cilindro);
			$("input[name=prue_hidro]").val(objeto.prue_hidro);
			$("input[name=pre_inicial]").val(objeto.pre_inicial);
			$("input[name=gas_eva]").val(objeto.gas_eva);
			$("input[name=peso_inicial]").val(objeto.peso_inicial);
			$("input[name=peso_final]").val(objeto.peso_final);
			$("input[name=pre_vacio]").val(objeto.pre_vacio);
			$("input[name=lote_pro]").val(objeto.lote_pro);
			$("input[name=nombre_mezcla]").val(objeto.nombre_mezcla);
			$("input[name=tara_vacio]").val(objeto.tara_vacio);
			$("input[name=peso_esperado]").val(objeto.peso_esperado);
			$("input[name=pre_inicial_1]").val(objeto.pre_inicial_1);
			$("input[name=esperado_eto]").val(objeto.esperado_eto);
			$("input[name=real_eto]").val(objeto.real_eto);
			$("input[name=esperado_co]").val(objeto.esperado_co);
			$("input[name=real_co]").val(objeto.real_co);
			$("input[name=peso_final_1]").val(objeto.peso_final_1);
			$("input[name=desviacion]").val(objeto.desviacion);
			$("input[name=pre_final]").val(objeto.pre_final);
			$("input[name=lote_co]").val(objeto.lote_co);
			$("select[name=id_lote_eto1]").val(objeto.cilindro_eto);
			$("input[name=fecha_ven_mezcla]").val(objeto.fecha_ven_mezcla);	

			campo=objeto.gas_eva;
			if (campo=="ETO-10") 
			{
				valor= 10;
			}
			if (campo=="ETO-20") 
			{
				valor= 20;
			}
			if (campo=="ETO-90") 
			{
				valor= 90;
			}
			if (campo=="ETO-100") 
			{
				valor= 100;
			}

			var restante = 100-valor;
			var por_valor=valor*(0.01);
			var por_res=restante*(0.01);

		    var form = document.forms.mezcla;
		    form.oninput = function() 
		    {  
		        form.esperado_eto.value = ((parseInt(form.peso_esperado.value))*por_valor).toFixed(2);
		        form.esperado_co.value = ((parseInt(form.peso_esperado.value))*por_res).toFixed(2);
		       // form.tara_vacio.value = form.peso_final.value;
		        form.peso_final_1.value = ((parseFloat(form.real_eto.value))+(parseFloat(form.real_co.value))).toFixed(2);
		        form.desviacion.value = (Math.abs(((parseFloat(form.peso_esperado.value))-(parseFloat(form.peso_final_1.value)))/(parseFloat(form.peso_esperado.value)) *(100))).toFixed(2);
		    }
		})
		.fail(function() {
			console.log("error");
		});
	}
</script>

<?php 
include("inc/google-analytics.php"); 
}
else
{
    header("Location:index.php");
}
?>
					                       
										