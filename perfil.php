<?php
//header('Content-Type: text/html; charset=ISO-8859-1');
header('Content-Type: text/html; charset=UTF-8');

date_default_timezone_set("America/Bogota");

require ("libraries/conexion.php");
session_start();
if(@$_SESSION['logged']== 'yes')
{ 
  $id_creditor = isset($_REQUEST['id_creditor']) ? $_REQUEST['id_creditor'] : NULL;
    $User_idUser =$_SESSION['su'];
    $acc = $_SESSION['acc'];
 $idprofile = isset($_REQUEST['idprofile']) ? $_REQUEST['idprofile'] : NULL;

if(isset($_POST['guardar_perfil']))
{
    $profile = $_POST['profile'] ;
    $description = $_POST['description'] ;
    
    if(strlen($idprofile) > 0)
    {
        $consulta  = "UPDATE profile
                    SET profile = '".$profile."', description = '".$description."' 
                    WHERE idprofile = ".$idprofile;
        $resultado = mysqli_query($con,$consulta) ;
        if ($resultado == FALSE)
        {
            echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
        }
        else
        {
            header('Location: busqueda_perfil.php');
        }
    }
    else
    {
        $consulta  = "INSERT INTO profile
                    (profile, description) 
                    VALUES ('".$profile."', '".$description."')";
        $resultado = mysqli_query($con,$consulta) ;
        if ($resultado == FALSE)
        {
            echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";
        }
        else
        {
            $idprofile = mysqli_insert_id($con);
            ?>
            <script type="text/javascript">
                alert("Nuevo Perfil creado correctamente");
            </script>
            <?php
        }
    }
}
else if(isset($_GET['idpermission']))
{
    $idpermission = $_GET['idpermission'] ;
    $estado1 = $_GET['estado1'];
    
    if($estado1 == 0)
    {
        $consulta  = "DELETE FROM profile_has_permission 
                WHERE permission_idpermission = ".$idpermission." 
                AND profile_idprofile = ".$idprofile;
        $resultado = mysqli_query($con,$consulta) ;
        if ($resultado == FALSE)
        {
            echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";   
        }
    }
    else
    {
        $consulta  = "INSERT INTO profile_has_permission 
                    (profile_idprofile, permission_idpermission) 
                    VALUES (".$idprofile.", ".$idpermission.")";
        $resultado = mysqli_query($con,$consulta) ;
        if ($resultado == FALSE)
        {
            echo mysqli_errno($con) . ": " . mysqli_error($con) . "\n";   
        }
    }
}

if(strlen($idprofile) > 0)
{   
    $consulta  = "SELECT * FROM profile WHERE idprofile = ".$idprofile;
    $resultado = mysqli_query($con,$consulta) ;
    $linea = mysqli_fetch_array($resultado);
    $profile = isset($linea["profile"]) ? $linea["profile"] : NULL;
    if(strlen($profile) > 0)
    {
        $description = isset($linea["description"]) ? $linea["description"] : NULL;
    }
    mysqli_free_result($resultado);
}


//initilize the page
require_once("inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("inc/config.ui.php");

/*---------------- PHP Custom Scripts ---------

YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */

$page_title = "Perfil";

/* ---------------- END PHP Custom Scripts ------------- */

//include header
//you can add your custom css in $page_css array.
//Note: all css files are inside css/ folder
$page_css[] = "your_style.css";
include("inc/header.php");

//include left panel (navigation)
//follow the tree in inc/config.ui.php
$page_nav[""][""][""][""] = true;
include("inc/nav.php");

?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
  <?php
    //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
    //$breadcrumbs["New Crumb"] => "http://url.com"
    include("inc/ribbon.php");
  ?>
  <!-- MAIN CONTENT -->
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
  <div id="content">

        <!-- widget grid -->

    <section id="widget-grid" class="">


      <!-- START ROW -->

      <div class="row">

        <!-- NEW COL START -->
        <article class="col-sm-12 col-md-12 col-lg-6">
          
          <!-- Widget ID (each widget will need unique ID)-->
          <div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
          
              
          
            <header>
              <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
              <h2>Profile </h2>        
              
            </header>

            <!-- widget div-->
            <div>
              
              <!-- widget edit box -->
              <div class="jarviswidget-editbox">
                <!-- This area used as dropdown edit box -->
                
              </div>
              <!-- end widget edit box -->
              
              <!-- widget content -->
              <div class="widget-body no-padding">
                
                <form id="checkout-form" class="smart-form" novalidate="novalidate" action="perfil.php" method="POST">
                <input type="hidden" name="idprofile" id="idprofile" value="<?php echo $idprofile; ?>">

                  <fieldset>
                    <div class="row">
                      <section class="col col-6">
                        <label class="label">Perfil :</label>
                        <label class="input"> <i class="icon-append fa fa-briefcase"></i>
                          <input type="text" name="profile" placeholder="Perfil" value="<?php echo isset($profile) ? $profile : NULL; ?>">
                        </label>
                      </section>
                      <section class="col col-6">
                        <label class="label">Descripción :</label>
                        <label class="input"> <i class="icon-append fa fa-lock"></i>
                          <input type="text" name="description" placeholder="Descripción" value="<?php echo isset($description) ? $description : NULL; ?>">
                        </label>
                      </section>
                    </div>                  
                  </fieldset>             

                  <?php
                  if (in_array(4, $acc))
                  {
                  ?>
                    <footer>                    
                      <input type="submit" value="Enviar" name="guardar_perfil" id="guardar_perfil" class="btn btn-primary" />
                    </footer>
                  <?php
                  }                   
                  ?>
                </form>
                 <?php
                                    if(strlen($idprofile) > 0)
                                    {
                                    ?>
                <div class="widget-body">
                      
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                             <th>#</th>
                            <th>Módulo principal</th>
                            <th>Módulo</th>
                            <th>Accion</th>
                            <th>Descripción</th>
                            <th>Estado </th>
                          </tr>
                        </thead>
                        <tbody>
                          
                          <?php
                          $cant = 0;
                          $consulta  = "SELECT *  
                                      FROM permission 
                                      ORDER BY modulo_ppal ASC, modulo ASC, accion DESC";
                          $resultado = mysqli_query($con,$consulta) ;
                          while ($linea = mysqli_fetch_array($resultado))
                          {
                              $idpermiso = $linea["idpermission"];
                              $modulo_ppal = $linea["modulo_ppal"];
                              $modulo = $linea["modulo"];
                              $accion = $linea["accion"];
                              $description = $linea["descripcion"];
                              
                              $consulta1  = "SELECT *  
                                          FROM profile_has_permission  
                                          WHERE  profile_idprofile = ".$idprofile." 
                                          AND permission_idpermission = ".$idpermiso;
                              $resultado1 = mysqli_query($con,$consulta1) ;
                              if(mysqli_num_rows($resultado1) > 0)
                              {
                                  $imgestado = "<img src=\"img/activo.png\" alt=\"Remove Permission\" title=\"Remove Permission\" onClick=\"if(confirm('¿Está seguro de que desea eliminar el permiso otorgado al perfil ?'))document.location='perfil.php?idpermission=".$idpermiso."&idprofile=".$idprofile."&estado1=0'\"/>";
                              }
                              else
                              {
                                  $imgestado = "<img src=\"img/inactivo.png\" alt=\"Add Permission\" title=\"Add Permission\" onClick=\"if(confirm('¿Está seguro de que desea agregar este permiso al perfil ?'))document.location='perfil.php?idpermission=".$idpermiso."&idprofile=".$idprofile."&estado1=1'\"/>";
                              }
                              $cant++;
                                          
                                  ?>
                                  <tr class="odd gradeX">
                                      <td><?php echo $cant; ?></td>
                                      <td><?php echo $modulo_ppal; ?></td>
                                      <td><?php echo $modulo; ?></td>
                                      <td><?php echo $accion; ?></td>
                                      <td><?php echo $description; ?></td>
                                      <td><?php echo $imgestado; ?></td>
                                       
                                  </tr>
                               
                                  <?php
                              }
                          
                      ?>

                      </tbody>
                    </table>

                    </div>
                    <?php
                                    }
                                    ?>

              </div>
              <!-- end widget content -->
              
            </div>
            <!-- end widget div -->
            
          </div>

          
          
          
        
        


        </article>
        <!-- END COL -->
    

      </div>

      <!-- END ROW -->

    </section>
    
      
  

    <!-- end widget grid -->

  </div>
  <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->

<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->
<?php
  include("inc/footer.php");
?>
<!-- END PAGE FOOTER -->

<?php 
  //include required scripts
  include("inc/scripts.php"); 
?>


<!-- PAGE RELATED PLUGIN(S) 
<script src="..."></script>-->
<!-- Flot Chart Plugin: Flot Engine, Flot Resizer, Flot Tooltip -->
<script src="js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="js/plugin/flot/jquery.flot.tooltip.min.js"></script>

<!-- Vector Maps Plugin: Vectormap engine, Vectormap language -->
<script src="js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>

<!-- Full Calendar -->
<script src="js/plugin/moment/moment.min.js"></script>
<script src="js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="js/plugin/jquery-form/jquery-form.min.js"></script>


<script type="text/javascript">

  $(document).ready(function() 
  {

    var $checkoutForm = $('#checkout-form').validate({
    // Rules for form validation
      rules : {
        company_name : {
          required : true
        },
        interest_percent : {
          required : true
        },
        minimum_payment : {
          required : true,
          
        },
        new_interest : {
          required : true,
          
        }
      },

      // Messages for form validation
      messages : {
        company_name : {
          required : 'Please enter the company name'
        },
        interest_percent : {
          required : 'Please enter the interest percent'
        },
        minimum_payment : {
          required : 'Please enter the minimum payment'
        },
        new_interest : {
          required : 'Please enter the new interest'
        }
      },

      // Do not change code below
      errorPlacement : function(error, element) {
        error.insertAfter(element.parent());
      }
    });
    
    
    
        
  })

</script>


<?php 
  //include footer
  include("inc/google-analytics.php"); 
?>
<?php

}
else
{
    header("Location:index.php");
}
?>